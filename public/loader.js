(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.U = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgqA2QgRgQAAgeIAAhOIAfAAIAABNQABAQAHAKQAIAKAMAAQAOAAAHgKQAIgKAAgQIAAhNIAfAAIAABOQAAAegRAQQgRARgaAAQgZAAgRgRg");
	this.shape.setTransform(-6,14.4);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.U, new cjs.Rectangle(-15.5,0,31.2,28.4), null);


(lib.T = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgPBGIAAhwIgnAAIAAgbIBtAAIAAAbIgnAAIAABwg");
	this.shape.setTransform(-6.1,14.4);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.T, new cjs.Rectangle(-14.1,0,28.4,28.4), null);


(lib.R = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAXBGIgegsIgWAAIAAAsIgfAAIAAiLIA1AAQAgAAANALQAOALAAAYQAAAhgaAKIAjAygAgdAAIAXAAQAPgBAGgFQAGgFAAgMQAAgKgGgFQgGgEgOAAIgYAAg");
	this.shape.setTransform(-5.5,14.4);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.R, new cjs.Rectangle(-15.3,0,30.8,28.4), null);


(lib.O = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgzAzQgWgUABgfQgBgeAWgVQAVgUAeAAQAfAAAWAUQAUAVAAAeQAAAfgUAUQgWAVgfAAQgeAAgVgVgAgcgfQgNANAAASQAAATANANQAMANAQAAQASAAALgNQANgNAAgTQAAgSgNgNQgLgNgSAAQgQAAgMANg");
	this.shape.setTransform(-5.4,14.3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.O, new cjs.Rectangle(-15.7,0,32.7,28.4), null);


(lib.N = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAhBGIhBhXIAABXIgfAAIAAiLIAdAAIBDBZIAAhZIAgAAIAACLg");
	this.shape.setTransform(-6.1,14.4);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.N, new cjs.Rectangle(-16.2,0,32.6,28.4), null);


(lib.M = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAvBGIAAhXIgmBLIgRAAIgmhLIAABXIgfAAIAAiLIAqAAIAjBMIAkhMIAqAAIAACLg");
	this.shape.setTransform(-6.1,14.4);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.M, new cjs.Rectangle(-17.6,0,35.3,28.4), null);


(lib.ligne_zigzag = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#333333").ss(1,1,1).p("EgAahM/UAEuAsMgEsAorUgDKAkxADMAgX");
	this.shape.setTransform(2.3,319.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#333333").ss(1,1,1).p("EgAZhM/UAEqAsLgEoAosUgDHAkxADIAgX");
	this.shape_1.setTransform(2.3,319.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#333333").ss(1,1,1).p("EgAZhM/UAEeAsJgEcAouUgC9AkuAC/Aga");
	this.shape_2.setTransform(2.2,319.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#333333").ss(1,1,1).p("EgAYhM/UAEKAsGgEIAoxUgCtAkrACvAgd");
	this.shape_3.setTransform(2.1,319.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#333333").ss(1,1,1).p("EgAWhM/UADtAsBgDrAo2UgCXAkmACYAgi");
	this.shape_4.setTransform(2,319.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#333333").ss(1,1,1).p("EgAUhM/UADJAr7gDHAo8UgB7AkgAB8Ago");
	this.shape_5.setTransform(1.8,319.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#333333").ss(1,1,1).p("EgAShM/UACcAr0gCaApDUgBXAkYABYAgw");
	this.shape_6.setTransform(1.6,319.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#333333").ss(1,1,1).p("EgAPhM/UABoArsgBmApLUgAuAkPAAvAg5");
	this.shape_7.setTransform(1.3,319.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#333333").ss(1,1,1).p("EgANhM/UAA0ArjgAyApUUgAEAkGAAGAhC");
	this.shape_8.setTransform(1,319.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#333333").ss(1,1,1).p("EgAIhM/UAAIArcgAGApbUAAeAj/gAcAhJ");
	this.shape_9.setTransform(0.5,319.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#333333").ss(1,1,1).p("EgAIhM/UgAcArWAAeAphUAA6Aj4gA5AhQ");
	this.shape_10.setTransform(0.6,319.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#333333").ss(1,1,1).p("EgAHhM/UgA4ArRAA6ApmUABRAjzgBPAhV");
	this.shape_11.setTransform(0.4,319.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#333333").ss(1,1,1).p("EgAGhM/UgBMArOABOAppUABhAjwgBfAhY");
	this.shape_12.setTransform(0.3,319.3);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#333333").ss(1,1,1).p("EgAFhM/UgBZArMABbAprUABqAjugBpAha");
	this.shape_13.setTransform(0.3,319.3);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#333333").ss(1,1,1).p("EgAFhM/UgBcArMABeAprUABuAjtgBtAhb");
	this.shape_14.setTransform(0.3,319.3);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#333333").ss(1,1,1).p("EgAGhM/UgBOArOABQAppUABjAjwgBhAhY");
	this.shape_15.setTransform(0.3,319.3);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#333333").ss(1,1,1).p("EgAHhM/UgA9ArQAA/ApnUABVAjzgBTAhV");
	this.shape_16.setTransform(0.4,319.3);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#333333").ss(1,1,1).p("EgAIhM/UgAkArUAAmApjUABBAj3gA/AhR");
	this.shape_17.setTransform(0.5,319.3);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#333333").ss(1,1,1).p("EgAKhM/UgAEAraAAGApdUAAoAj8gAmAhM");
	this.shape_18.setTransform(0.7,319.3);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#333333").ss(1,1,1).p("EgAIhM/UAAhArggAfApXUAAKAkDgAIAhF");
	this.shape_19.setTransform(0.5,319.3);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#333333").ss(1,1,1).p("EgAOhM/UABPArngBNApQUgAaAkLAAbAg9");
	this.shape_20.setTransform(1.2,319.3);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#333333").ss(1,1,1).p("EgARhM/UACCArwgCAApHUgBCAkUABEAg0");
	this.shape_21.setTransform(1.4,319.3);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#333333").ss(1,1,1).p("EgAThM/UACvAr3gCtApAUgBmAkbABnAgt");
	this.shape_22.setTransform(1.7,319.3);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#333333").ss(1,1,1).p("EgAVhM/UADWAr9gDUAo6UgCFAkiACGAgm");
	this.shape_23.setTransform(1.9,319.3);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#333333").ss(1,1,1).p("EgAXhM/UAD2AsDgD0Ao0UgCdAkoACfAgg");
	this.shape_24.setTransform(2,319.3);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#333333").ss(1,1,1).p("EgAYhM/UAEOAsHgEMAowUgCxAksACyAgc");
	this.shape_25.setTransform(2.2,319.3);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#333333").ss(1,1,1).p("EgAZhM/UAEgAsJgEeAouUgC/AkvADBAgZ");
	this.shape_26.setTransform(2.2,319.3);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#333333").ss(1,1,1).p("EgAahM/UAErAsLgEpAosUgDHAkxADJAgX");
	this.shape_27.setTransform(2.3,319.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[]},1).wait(11));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-11.2,-174.5,27.1,987.5);


(lib.H = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAcBGIAAg3Ig2AAIAAA3IggAAIAAiLIAgAAIAAA7IA2AAIAAg7IAfAAIAACLg");
	this.shape.setTransform(-6.1,14.4);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.H, new cjs.Rectangle(-15.7,0,31.5,28.4), null);


(lib.G = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgrA0QgVgUAAggQAAgeAVgUQAWgVAdAAQAeAAAWAUIgQAXQgKgIgIgDQgHgDgKAAQgQAAgNAMQgMAMAAATQAAATAMAMQAMAMAPAAQAQAAALgGIAAgmIAfAAIAAAxQgVAXgkAAQgeAAgVgUg");
	this.shape.setTransform(-6.2,14.3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.G, new cjs.Rectangle(-15.6,0,31.2,28.4), null);


(lib.E2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgzBGIAAiLIBlAAIAAAcIhFAAIAAAdIA+AAIAAAZIg+AAIAAAeIBHAAIAAAbg");
	this.shape.setTransform(-5.8,14.4);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.E2, new cjs.Rectangle(-14.6,0,29.3,28.4), null);


(lib.E = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgzBGIAAiLIBlAAIAAAcIhFAAIAAAdIA+AAIAAAZIg+AAIAAAeIBHAAIAAAbg");
	this.shape.setTransform(-5.8,14.4);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.E, new cjs.Rectangle(-14.6,0,29.3,28.4), null);


(lib.C = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgsA0QgVgUAAggQABgeAUgUQAWgVAdAAQAjAAAYAaIgUAWQgPgSgWAAQgRAAgMALQgMAMAAASQAAAUALALQAMAMAQAAQAXAAAOgSIAVAVQgYAaghAAQgfAAgVgUg");
	this.shape.setTransform(-5.9,14.3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.C, new cjs.Rectangle(-15.4,0,30.9,28.4), null);


(lib.A = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAqBGIgNgeIg6AAIgNAeIghAAIA9iLIAdAAIA8CLgAgQAMIAiAAIgSgng");
	this.shape.setTransform(-6.1,14.4);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.A, new cjs.Rectangle(-15.5,0,31.1,28.4), null);


(lib.lignes_all = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_29 = function() {
		this.gotoAndPlay(1);
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(29).call(this.frame_29).wait(1));

	// ligne_zigzag
	this.instance = new lib.ligne_zigzag("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(-457.9,469.9,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(30));

	// ligne_zigzag
	this.instance_1 = new lib.ligne_zigzag("synched",4);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-229.9,469.9,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(30));

	// ligne_zigzag
	this.instance_2 = new lib.ligne_zigzag("synched",9);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-2.9,469.9,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(30));

	// ligne_zigzag
	this.instance_3 = new lib.ligne_zigzag("synched",14);
	this.instance_3.parent = this;
	this.instance_3.setTransform(224.2,469.9,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(30));

	// ligne_zigzag
	this.instance_4 = new lib.ligne_zigzag("synched",19);
	this.instance_4.parent = this;
	this.instance_4.setTransform(449.1,469.9,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(30));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-469.1,-110.6,920.7,987.5);


(lib.chargement_texte = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// T
	this.instance = new lib.T();
	this.instance.parent = this;
	this.instance.setTransform(0,251.4,1,1,0,0,0,0,14.2);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(18).to({_off:false},0).to({x:-30},13,cjs.Ease.quartOut).to({x:0},13,cjs.Ease.cubicIn).to({_off:true},1).wait(136));

	// N
	this.instance_1 = new lib.N();
	this.instance_1.parent = this;
	this.instance_1.setTransform(0,225,1,1,0,0,0,0,14.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(16).to({_off:false},0).to({x:-30},13,cjs.Ease.quartOut).to({x:0},13,cjs.Ease.cubicIn).to({_off:true},1).wait(138));

	// E2
	this.instance_2 = new lib.E2();
	this.instance_2.parent = this;
	this.instance_2.setTransform(0,198.7,1,1,0,0,0,0,14.2);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(14).to({_off:false},0).to({x:-30},13,cjs.Ease.quartOut).to({x:0},13,cjs.Ease.cubicIn).to({_off:true},1).wait(140));

	// M
	this.instance_3 = new lib.M();
	this.instance_3.parent = this;
	this.instance_3.setTransform(0,172.3,1,1,0,0,0,0,14.2);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(12).to({_off:false},0).to({x:-30},13,cjs.Ease.quartOut).to({x:0},13,cjs.Ease.cubicIn).to({_off:true},1).wait(142));

	// E
	this.instance_4 = new lib.E();
	this.instance_4.parent = this;
	this.instance_4.setTransform(0,146,1,1,0,0,0,0,14.2);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(10).to({_off:false},0).to({x:-30},13,cjs.Ease.quartOut).to({x:0},13,cjs.Ease.cubicIn).to({_off:true},1).wait(144));

	// G
	this.instance_5 = new lib.G();
	this.instance_5.parent = this;
	this.instance_5.setTransform(0,119.6,1,1,0,0,0,0,14.2);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(8).to({_off:false},0).to({x:-30},13,cjs.Ease.quartOut).to({x:0},13,cjs.Ease.cubicIn).to({_off:true},1).wait(146));

	// R
	this.instance_6 = new lib.R();
	this.instance_6.parent = this;
	this.instance_6.setTransform(0,93.3,1,1,0,0,0,0,14.2);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off:false},0).to({x:-30},13,cjs.Ease.quartOut).to({x:0},13,cjs.Ease.cubicIn).to({_off:true},1).wait(148));

	// A
	this.instance_7 = new lib.A();
	this.instance_7.parent = this;
	this.instance_7.setTransform(0,66.9,1,1,0,0,0,0,14.2);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(4).to({_off:false},0).to({x:-30},13,cjs.Ease.quartOut).to({x:0},13,cjs.Ease.cubicIn).to({_off:true},1).wait(150));

	// H
	this.instance_8 = new lib.H();
	this.instance_8.parent = this;
	this.instance_8.setTransform(0,40.6,1,1,0,0,0,0,14.2);
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(2).to({_off:false},0).to({x:-30},13,cjs.Ease.quartOut).to({x:0},13,cjs.Ease.cubicIn).to({_off:true},1).wait(152));

	// C
	this.instance_9 = new lib.C();
	this.instance_9.parent = this;
	this.instance_9.setTransform(0,14.2,1,1,0,0,0,0,14.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_9).to({x:-30},13,cjs.Ease.quartOut).to({x:0},13,cjs.Ease.cubicIn).to({_off:true},1).wait(154));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-15.4,0,30.9,28.4);


(lib.ca_va_texte = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// A
	this.instance = new lib.U();
	this.instance.parent = this;
	this.instance.setTransform(0,156.9,1,1,0,0,0,0,14.2);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(10).to({_off:false},0).to({x:-30},13,cjs.Ease.quartOut).to({x:0},13,cjs.Ease.cubicIn).to({_off:true},1).wait(144));

	// H
	this.instance_1 = new lib.O();
	this.instance_1.parent = this;
	this.instance_1.setTransform(0,130.6,1,1,0,0,0,0,14.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(8).to({_off:false},0).to({x:-30},13,cjs.Ease.quartOut).to({x:0},13,cjs.Ease.cubicIn).to({_off:true},1).wait(146));

	// C
	this.instance_2 = new lib.C();
	this.instance_2.parent = this;
	this.instance_2.setTransform(0,104.2,1,1,0,0,0,0,14.2);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(6).to({_off:false},0).to({x:-30},13,cjs.Ease.quartOut).to({x:0},13,cjs.Ease.cubicIn).to({_off:true},1).wait(148));

	// A
	this.instance_3 = new lib.U();
	this.instance_3.parent = this;
	this.instance_3.setTransform(0,66.9,1,1,0,0,0,0,14.2);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(4).to({_off:false},0).to({x:-30},13,cjs.Ease.quartOut).to({x:0},13,cjs.Ease.cubicIn).to({_off:true},1).wait(150));

	// H
	this.instance_4 = new lib.O();
	this.instance_4.parent = this;
	this.instance_4.setTransform(0,40.6,1,1,0,0,0,0,14.2);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(2).to({_off:false},0).to({x:-30},13,cjs.Ease.quartOut).to({x:0},13,cjs.Ease.cubicIn).to({_off:true},1).wait(152));

	// C
	this.instance_5 = new lib.C();
	this.instance_5.parent = this;
	this.instance_5.setTransform(0,14.2,1,1,0,0,0,0,14.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).to({x:-30},13,cjs.Ease.quartOut).to({x:0},13,cjs.Ease.cubicIn).to({_off:true},1).wait(154));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-15.4,0,30.9,28.4);


// stage content:
(lib.loader_fullscreen = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.ca_va_texte();
	this.instance.parent = this;
	this.instance.setTransform(408.8,411.2,1,1,0,0,0,-0.1,14.2);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(294).to({_off:false},0).wait(105));

	// Layer_3
	this.instance_1 = new lib.ca_va_texte();
	this.instance_1.parent = this;
	this.instance_1.setTransform(859.5,402,1,1,0,0,0,-0.1,14.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(151).to({_off:false},0).wait(248));

	// Layer_5
	this.instance_2 = new lib.chargement_texte();
	this.instance_2.parent = this;
	this.instance_2.setTransform(1087.7,561.6,1,1,0,0,0,0.1,132.9);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(226).to({_off:false},0).wait(173));

	// Layer_6
	this.instance_3 = new lib.chargement_texte();
	this.instance_3.parent = this;
	this.instance_3.setTransform(864.2,247.6,0.7,0.7,0,0,0,0,132.8);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(93).to({_off:false},0).wait(306));

	// Layer_7
	this.instance_4 = new lib.chargement_texte();
	this.instance_4.parent = this;
	this.instance_4.setTransform(195.5,166.4,0.9,0.9,0,0,0,0,132.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(399));

	// Layer_1
	this.lignes_mc = new lib.lignes_all();
	this.lignes_mc.name = "lignes_mc";
	this.lignes_mc.parent = this;
	this.lignes_mc.setTransform(640,406.3,1,1,0,0,0,0,406.3);

	this.timeline.addTween(cjs.Tween.get(this.lignes_mc).wait(399));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(811.4,273.9,923.7,986.5);
// library properties:
lib.properties = {
	id: 'BB31E8F48565D749B0A887649043155E',
	width: 1280,
	height: 768,
	fps: 60,
	color: "#000000",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['BB31E8F48565D749B0A887649043155E'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;