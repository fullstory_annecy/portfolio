(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.txt_hashtag = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CC9933").s().p("AgjAxIAAhhIBGAAIAAATIgwAAIAAAVIAqAAIAAARIgqAAIAAAVIAxAAIAAATg");
	this.shape.setTransform(40.1,115.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#CC9933").s().p("AAQAxIgVgfIgPAAIAAAfIgWAAIAAhhIAlAAQAWAAAKAIQAKAIAAAQQAAAXgTAHIAZAjgAgUAAIAQAAQAKAAAEgDQAEgFAAgIQAAgHgEgDQgEgDgKAAIgQAAg");
	this.shape_1.setTransform(30.6,115.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#CC9933").s().p("AgjAkQgPgOAAgWQAAgUAPgPQAOgPAVAAQAWAAAPAPQAOAPAAAUQAAAWgOAOQgPAOgWAAQgVAAgOgOgAgTgWQgJAKAAAMQAAANAJAJQAIAKALgBQAMABAJgKQAIgJAAgNQAAgMgIgKQgJgIgMgBQgLABgIAIg");
	this.shape_2.setTransform(19.3,115.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#CC9933").s().p("AAhAxIAAg8IgbA0IgMAAIgag0IAAA8IgWAAIAAhhIAeAAIAYA1IAZg1IAeAAIAABhg");
	this.shape_3.setTransform(6.9,115.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CC9933").s().p("AgoAGIAAgLIBRAAIAAALg");
	this.shape_4.setTransform(-4.8,122.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#CC9933").s().p("AgrAxIAAhhIAiAAQAZAAAOANQAOANABAWQgBAWgOANQgNAOgbAAgAgWAeIAOAAQAOAAAIgIQAIgHAAgPQAAgNgIgIQgHgIgQAAIgNAAg");
	this.shape_5.setTransform(-15,115.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#CC9933").s().p("AAXAxIgtg8IAAA8IgWAAIAAhhIAUAAIAvA+IAAg+IAWAAIAABhg");
	this.shape_6.setTransform(-26.4,115.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#CC9933").s().p("AAeAxIgKgVIgoAAIgJAVIgXAAIAqhhIAVAAIAqBhgAgLAIIAXAAIgMgbg");
	this.shape_7.setTransform(-37.3,115.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#CC9933").s().p("AgIAJQgDgEAAgFQAAgEADgEQAEgDAEAAQAFAAAEADQADAEAAAEQAAAFgDAEQgEADgFAAQgEAAgEgDg");
	this.shape_8.setTransform(-44.6,119.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#CC9933").s().p("AgIAJQgDgEAAgFQAAgEADgEQAEgDAEAAQAFAAAEADQADAEAAAEQAAAFgDAEQgEADgFAAQgEAAgEgDg");
	this.shape_9.setTransform(-48.6,119.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#CC9933").s().p("AgIAJQgDgEAAgFQAAgEADgEQAEgDAEAAQAFAAAEADQADAEAAAEQAAAFgDAEQgEADgFAAQgEAAgEgDg");
	this.shape_10.setTransform(-52.7,119.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AAXAxIgtg9IAAA9IgWAAIAAhhIAUAAIAvA+IAAg+IAWAAIAABhg");
	this.shape_11.setTransform(76.5,73.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgeAlQgPgPAAgWQABgUAPgPQAOgPAVAAQAUAAAQAOIgLARQgHgGgGgCQgFgCgGAAQgMgBgIAJQgJAJAAAMQAAAOAIAIQAJAJAKAAQALAAAIgEIAAgbIAVAAIAAAjQgOAQgZAAQgWAAgOgOg");
	this.shape_12.setTransform(65.5,73.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgKAxIAAhhIAVAAIAABhg");
	this.shape_13.setTransform(58,73.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgmAiIANgQQAQANANAAQAGAAADgDQADgCAAgFQAAgDgDgDQgEgDgKgDQgRgDgIgGQgIgGAAgOQAAgOALgIQAKgIANAAQAKAAAKAEQAKADAHAHIgLAQQgNgLgNAAQgFAAgDADQgDADAAAEQAAAEAEADQAEACANAEQAOADAHAGQAIAHAAANQAAANgKAJQgKAHgQABQgWAAgTgRg");
	this.shape_14.setTransform(51.1,73.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBFAAIAAATIgvAAIAAAVIArAAIAAASIgrAAIAAAUIAxAAIAAATg");
	this.shape_15.setTransform(42.4,73.8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgrAxIAAhhIAiAAQAaAAANANQAPANAAAWQAAAWgPANQgNAOgbAAgAgWAeIAOAAQAPAAAHgIQAJgHgBgPQABgOgJgHQgHgIgRAAIgMAAg");
	this.shape_16.setTransform(32.5,73.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgoAGIAAgLIBRAAIAAALg");
	this.shape_17.setTransform(21.8,80);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AAXAxIgtg9IAAA9IgWAAIAAhhIAUAAIAvA+IAAg+IAWAAIAABhg");
	this.shape_18.setTransform(11,73.8);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgjAkQgPgPAAgVQAAgUAPgPQAOgPAVAAQAWAAAPAPQAOAPAAAUQAAAVgOAPQgPAPgWAAQgVAAgOgPgAgTgWQgJAKAAAMQAAANAJAJQAIAJALAAQAMAAAJgJQAIgJAAgNQAAgMgIgKQgJgIgMgBQgLABgIAIg");
	this.shape_19.setTransform(-0.5,73.8);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgKAxIAAhhIAVAAIAABhg");
	this.shape_20.setTransform(-8.5,73.8);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgKAxIAAhOIgcAAIAAgTIBMAAIAAATIgbAAIAABOg");
	this.shape_21.setTransform(-15.1,73.8);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgjAkQgPgPAAgVQAAgUAPgPQAOgPAVAAQAWAAAPAPQAOAPAAAUQAAAVgOAPQgPAPgWAAQgVAAgOgPgAgTgWQgJAKAAAMQAAANAJAJQAIAJALAAQAMAAAJgJQAIgJAAgNQAAgMgIgKQgJgIgMgBQgLABgIAIg");
	this.shape_22.setTransform(-25.1,73.8);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AAhAxIAAg9IgbA1IgMAAIgag1IAAA9IgWAAIAAhhIAeAAIAYA1IAZg1IAeAAIAABhg");
	this.shape_23.setTransform(-37.6,73.8);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgUIANAAIADgQIgPAAIADgUIAPAAIAEgWIAUAAIgDAWIAMAAIADgWIAWAAIgEAWIAOAAIgDAUIgPAAIgDAQIARAAIgEAUIgQAAIgDAVgAgHAJIALAAIAEgQIgMAAg");
	this.shape_24.setTransform(-49.5,73.7);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgKAxIAAhOIgcAAIAAgTIBNAAIAAATIgcAAIAABOg");
	this.shape_25.setTransform(85,52.8);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AAXAxIgtg9IAAA9IgWAAIAAhhIAUAAIAvA+IAAg+IAWAAIAABhg");
	this.shape_26.setTransform(75,52.8);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBFAAIAAATIgvAAIAAAVIArAAIAAASIgrAAIAAAUIAxAAIAAATg");
	this.shape_27.setTransform(64.9,52.8);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AgKAxIAAhOIgbAAIAAgTIBLAAIAAATIgbAAIAABOg");
	this.shape_28.setTransform(55.9,52.8);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AAXAxIgtg9IAAA9IgWAAIAAhhIAUAAIAvA+IAAg+IAWAAIAABhg");
	this.shape_29.setTransform(45.9,52.8);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AgjAkQgPgPAAgVQAAgUAPgPQAOgPAVAAQAWAAAPAPQAOAPAAAUQAAAVgOAPQgPAPgWAAQgVAAgOgPgAgTgVQgJAJAAAMQAAANAJAKQAIAIALABQAMgBAJgIQAIgKAAgNQAAgMgIgJQgJgJgMAAQgLAAgIAJg");
	this.shape_30.setTransform(34.4,52.7);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AgeAkQgPgOAAgWQAAgVAPgOQAPgPAVAAQAYAAAQASIgNAQQgLgMgPAAQgMgBgIAJQgJAHAAANQgBANAJAJQAJAIAKAAQAQAAALgMIANAOQgQASgXABQgVAAgPgPg");
	this.shape_31.setTransform(23.6,52.7);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AgoAGIAAgLIBRAAIAAALg");
	this.shape_32.setTransform(13.3,58.9);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AgrAxIAAhhIAiAAQAaAAAOANQANAMAAAXQAAAWgNAOQgOANgbAAgAgWAeIAOAAQAPgBAHgHQAJgIAAgOQAAgOgJgHQgHgIgRAAIgMAAg");
	this.shape_33.setTransform(3.2,52.8);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AAXAxIgtg9IAAA9IgWAAIAAhhIAUAAIAvA+IAAg+IAWAAIAABhg");
	this.shape_34.setTransform(-8.2,52.8);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AAeAxIgKgVIgoAAIgJAVIgXAAIAqhhIAVAAIAqBhgAgLAJIAXAAIgMgcg");
	this.shape_35.setTransform(-19.2,52.8);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AAQAxIgVgfIgPAAIAAAfIgWAAIAAhhIAlAAQAWAAAKAIQAKAIAAARQAAAWgTAHIAZAjgAgUAAIAQAAQAKAAAEgEQAEgEAAgHQAAgIgEgDQgEgDgKAAIgQAAg");
	this.shape_36.setTransform(-29.1,52.8);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AgmAxIAAhhIAmAAQAJAAAHACQAIADADAEQAGAHAAAKQABALgIAGIgDACIgDACQAIABAGAGQAFAHAAAJQAAAKgHAIQgIAJgUAAgAgQAeIAPAAQAIAAAFgCQAFgCgBgHQAAgHgEgCQgGgCgJAAIgNAAgAgQgIIAKAAQAIAAAFgCQAEgCAAgHQAAgGgEgCQgEgCgIgBIgLAAg");
	this.shape_37.setTransform(-39.1,52.8);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgTIANAAIADgRIgPAAIADgUIAPAAIAEgWIAUAAIgDAWIAMAAIADgWIAWAAIgEAWIAOAAIgDAUIgPAAIgDARIARAAIgEATIgQAAIgDAVgAgHAKIALAAIAEgRIgMAAg");
	this.shape_38.setTransform(-49.5,52.7);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBGAAIAAATIgwAAIAAAVIAqAAIAAASIgqAAIAAATIAxAAIAAAUg");
	this.shape_39.setTransform(95.6,31.7);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("AAQAxIgVgfIgPAAIAAAfIgWAAIAAhhIAlAAQAWAAAKAIQAKAHAAASQAAAWgTAHIAZAjgAgUAAIAQAAQAKAAAEgEQAEgEAAgHQAAgIgEgDQgEgDgKAAIgQAAg");
	this.shape_40.setTransform(86.1,31.7);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("AgdAmQgMgMAAgUIAAg2IAWAAIAAA1QAAAMAFAGQAGAHAIAAQAJAAAGgHQAFgGAAgMIAAg1IAWAAIAAA2QAAAUgMAMQgLAMgTAAQgRAAgMgMg");
	this.shape_41.setTransform(75.4,31.8);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("AgmAiIANgRQAQAPANAAQAGAAADgEQADgCAAgEQAAgFgDgCQgEgDgKgDQgRgEgIgFQgIgHAAgNQAAgPALgHQAKgIANABQAKAAAKADQAKADAHAGIgLARQgNgKgNAAQgFAAgDACQgDADAAAEQAAAFAEACQAEACANAEQAOADAHAGQAIAHAAANQAAANgKAJQgKAHgQABQgWgBgTgQg");
	this.shape_42.setTransform(65.5,31.7);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBFAAIAAATIgvAAIAAAVIArAAIAAASIgrAAIAAATIAxAAIAAAUg");
	this.shape_43.setTransform(56.8,31.7);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFFFFF").s().p("AAhAxIAAg9IgbA1IgMAAIgag1IAAA9IgWAAIAAhhIAeAAIAYA1IAZg1IAeAAIAABhg");
	this.shape_44.setTransform(45.3,31.7);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFFFFF").s().p("AgoAGIAAgLIBRAAIAAALg");
	this.shape_45.setTransform(33.6,37.9);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FFFFFF").s().p("AAQAxIgVgfIgPAAIAAAfIgWAAIAAhhIAlAAQAWAAAKAIQAKAHAAASQAAAWgTAHIAZAjgAgUAAIAQAAQAKAAAEgEQAEgEAAgHQAAgIgEgDQgEgDgKAAIgQAAg");
	this.shape_46.setTransform(23.8,31.7);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFFFFF").s().p("AgdAmQgMgMAAgUIAAg2IAWAAIAAA1QAAAMAFAGQAGAHAIAAQAJAAAGgHQAFgGAAgMIAAg1IAWAAIAAA2QAAAUgMAMQgMAMgSAAQgRAAgMgMg");
	this.shape_47.setTransform(13.1,31.8);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FFFFFF").s().p("AgmAiIANgRQAQAPANAAQAGAAADgEQADgCAAgEQAAgFgDgCQgEgDgKgDQgRgEgIgFQgIgHAAgNQAAgPALgHQAKgIANABQAKAAAKADQAKADAHAGIgLARQgNgKgNAAQgFAAgDACQgDADAAAEQAAAFAEACQAEACANAEQAOADAHAGQAIAHAAANQAAANgKAJQgKAHgQABQgWgBgTgQg");
	this.shape_48.setTransform(3.3,31.7);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FFFFFF").s().p("AgoAGIAAgLIBRAAIAAALg");
	this.shape_49.setTransform(-6.1,37.9);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FFFFFF").s().p("AgmAxIAAhhIAmAAQAJAAAHADQAIACADAEQAGAHAAAKQABAMgIAFIgDACIgDACQAIABAGAGQAFAHAAAJQAAAKgHAIQgIAJgUAAgAgQAeIAPAAQAIABAFgDQAFgCgBgHQAAgGgEgDQgGgCgJAAIgNAAgAgQgIIAKAAQAIAAAFgCQAEgCAAgGQAAgHgEgCQgEgDgIAAIgLAAg");
	this.shape_50.setTransform(-15.7,31.7);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBFAAIAAATIgvAAIAAAVIArAAIAAASIgrAAIAAATIAxAAIAAAUg");
	this.shape_51.setTransform(-25.2,31.7);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FFFFFF").s().p("AAVAxIgVhDIgUBDIgQAAIgjhhIAYAAIATA4IASg4IAVAAIASA4IATg4IAYAAIgiBhg");
	this.shape_52.setTransform(-37.1,31.7);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgTIANAAIADgRIgPAAIADgTIAPAAIAEgXIAUAAIgDAXIAMAAIADgXIAWAAIgEAXIAOAAIgDATIgPAAIgDARIARAAIgEATIgQAAIgDAVgAgHAKIALAAIAEgRIgMAAg");
	this.shape_53.setTransform(-49.5,31.6);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBGAAIAAAUIgwAAIAAAUIAqAAIAAASIgqAAIAAATIAxAAIAAAUg");
	this.shape_54.setTransform(113.7,10.7);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBFAAIAAAUIgvAAIAAAUIAqAAIAAASIgqAAIAAATIAxAAIAAAUg");
	this.shape_55.setTransform(104.6,10.7);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FFFFFF").s().p("AgKAxIAAhOIgcAAIAAgTIBNAAIAAATIgcAAIAABOg");
	this.shape_56.setTransform(95.5,10.7);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FFFFFF").s().p("AAXAxIgtg9IAAA9IgWAAIAAhhIAUAAIAvA+IAAg+IAWAAIAABhg");
	this.shape_57.setTransform(85.5,10.7);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBGAAIAAAUIgwAAIAAAUIAqAAIAAASIgqAAIAAATIAxAAIAAAUg");
	this.shape_58.setTransform(75.5,10.7);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#FFFFFF").s().p("AAhAxIAAg9IgbA1IgMAAIgag1IAAA9IgWAAIAAhhIAeAAIAYA1IAZg1IAeAAIAABhg");
	this.shape_59.setTransform(64,10.7);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#FFFFFF").s().p("AgeAkQgPgOAAgWQABgUAPgPQAOgOAVAAQAUAAAQANIgLARQgHgGgGgDQgFgBgHAAQgLAAgIAIQgJAIAAANQAAAOAIAJQAJAIAKAAQALAAAIgFIAAgaIAVAAIAAAiQgOAQgZABQgWgBgOgOg");
	this.shape_60.setTransform(52,10.6);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#FFFFFF").s().p("AgdAmQgMgLAAgVIAAg2IAWAAIAAA1QAAAMAGAGQAFAHAIAAQAJAAAGgHQAFgGAAgMIAAg1IAWAAIAAA2QAAAVgMALQgMAMgSAAQgRAAgMgMg");
	this.shape_61.setTransform(41.6,10.7);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#FFFFFF").s().p("AAeAxIgKgVIgoAAIgJAVIgXAAIAqhhIAVAAIAqBhgAgLAJIAXAAIgMgcg");
	this.shape_62.setTransform(31.1,10.7);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#FFFFFF").s().p("AgoAGIAAgLIBRAAIAAALg");
	this.shape_63.setTransform(20.8,16.8);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBFAAIAAAUIgvAAIAAAUIAqAAIAAASIgqAAIAAATIAxAAIAAAUg");
	this.shape_64.setTransform(11.4,10.7);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#FFFFFF").s().p("AgKAxIAAhOIgcAAIAAgTIBNAAIAAATIgcAAIAABOg");
	this.shape_65.setTransform(2.4,10.7);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#FFFFFF").s().p("AgKAxIAAhhIAVAAIAABhg");
	this.shape_66.setTransform(-4.2,10.7);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#FFFFFF").s().p("AgfAxIAAhhIAWAAIAABNIApAAIAAAUg");
	this.shape_67.setTransform(-10.1,10.7);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#FFFFFF").s().p("AAeAxIgKgVIgoAAIgJAVIgXAAIAqhhIAVAAIAqBhgAgLAJIAXAAIgMgcg");
	this.shape_68.setTransform(-19.7,10.7);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBFAAIAAAUIgvAAIAAAUIArAAIAAASIgrAAIAAATIAxAAIAAAUg");
	this.shape_69.setTransform(-29.2,10.7);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#FFFFFF").s().p("AAQAxIgVgfIgPAAIAAAfIgWAAIAAhhIAlAAQAWAAAKAIQAKAHAAARQAAAXgTAHIAZAjgAgUAAIAQAAQAKAAAEgEQAEgDAAgJQAAgHgEgDQgEgDgKAAIgQAAg");
	this.shape_70.setTransform(-38.8,10.7);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgTIANAAIADgRIgPAAIADgTIAPAAIAEgXIAUAAIgDAXIAMAAIADgXIAWAAIgEAXIAOAAIgDATIgPAAIgDARIARAAIgEATIgQAAIgDAVgAgHAKIALAAIAEgRIgMAAg");
	this.shape_71.setTransform(-49.5,10.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.txt_hashtag, new cjs.Rectangle(-56.7,0,177,126.3), null);


(lib.traitfullstorycontinu = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AxOAUIAAgnMAidAAAIAAAng");
	this.shape.setTransform(0,2);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.traitfullstorycontinu, new cjs.Rectangle(-110.2,0,220.5,4), null);


(lib.masqueTexte_1_ligne = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("EgicAEJIAAoRMBE5AAAIAAIRg");
	this.shape.setTransform(0,24.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.masqueTexte_1_ligne, new cjs.Rectangle(-220.5,-1.9,441,53), null);


(lib.masque_generique = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CC9933").s().p("AsQCqIAAlTIYhAAIAAFTg");
	this.shape.setTransform(78.5,17);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.masque_generique, new cjs.Rectangle(0,0,157,34), null);


(lib.ligne = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		//this.stop();
	}
	this.frame_48 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(48).call(this.frame_48).wait(1));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#333333").ss(2,1,1).p("EgABhM/MAADCZ/");
	this.shape.setTransform(-0.2,319.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#333333").ss(2,1,1).p("EAAghM7Qg1OzgbRBQgbUYAEVMQADWBAkVVQAlTkBERl");
	this.shape_1.setTransform(-7,319.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#333333").ss(2,1,1).p("EAA7hM5QhfN/gxR5QgxUHAHVhQAGWSA/VCQBDUPB4Qw");
	this.shape_2.setTransform(-12.4,319.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#333333").ss(2,1,1).p("EABQhM3Qh/NXhDSkQhAT5AIVyQAIWfBUU0QBZUwChQG");
	this.shape_3.setTransform(-16.5,320.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#333333").ss(2,1,1).p("EABfhM1QiXM4hPTEQhMTvAKV+QAJWpBjUqQBqVIC+Pn");
	this.shape_4.setTransform(-19.6,320.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#333333").ss(2,1,1).p("EABqhM0QioMjhYTaQhVToALWHQALWwBuUiQB1VaDUPR");
	this.shape_5.setTransform(-21.8,320.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#333333").ss(2,1,1).p("EABxhMzQizMUheTqQhbTjAMWNQALW1B2UdQB9VmDjPB");
	this.shape_6.setTransform(-23.3,320.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#333333").ss(2,1,1).p("EAB2hMzQi7MKhiT1QhfTgAMWRQAMW3B7UaQCDVvDsO3");
	this.shape_7.setTransform(-24.2,320.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#333333").ss(2,1,1).p("EAB5hMyQjAMEhkT7QhhTeAMWTQAMW5B+UYQCGVzDyOx");
	this.shape_8.setTransform(-24.8,320.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#333333").ss(2,1,1).p("EAB7hMyQjCMBhmT+QhjTdANWVQAMW6CAUWQCHV2D2Ou");
	this.shape_9.setTransform(-25.2,320.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#333333").ss(2,1,1).p("EAB8hMyQjEL/hmUAQhjTcAMWWQANW7CAUWQCIV3D3Os");
	this.shape_10.setTransform(-25.3,320.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#333333").ss(2,1,1).p("EAB8hMyQjEL/hnUAQhjTcANWWQAMW7CAUWQCJV3D4Os");
	this.shape_11.setTransform(-25.4,320.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#333333").ss(2,1,1).p("EAB8hMyQjEL/hnUAQhjTcAMWWQANW7CAUWQCJV3D4Os");
	this.shape_12.setTransform(-25.4,320.5);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#333333").ss(2,1,1).p("EAB8hMyQjEL/hnUAQhjTcAMWWQANW7CAUWQCJV4D4Or");
	this.shape_13.setTransform(-25.4,320.5);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#333333").ss(2,1,1).p("EAB8hMyQjEL/hmUAQhkTcANWWQAMW7CBUWQCIV3D4Os");
	this.shape_14.setTransform(-25.4,320.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#333333").ss(2,1,1).p("EAB7hMyQjDMAhmT/QhjTdANWVQAMW6CAUWQCIV3D2Ot");
	this.shape_15.setTransform(-25.2,320.5);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#333333").ss(2,1,1).p("EAB6hMyQjBMChmT9QhiTdAMWVQANW6B/UXQCHV1D1Ou");
	this.shape_16.setTransform(-25.1,320.5);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#333333").ss(2,1,1).p("EAB5hMyQjAMEhkT7QhiTeANWTQAMW5B+UYQCGV0DzOw");
	this.shape_17.setTransform(-24.9,320.5);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#333333").ss(2,1,1).p("EAB4hMzQi+MHhjT5QhgTfAMWRQAMW5B8UZQCFVxDvO0");
	this.shape_18.setTransform(-24.6,320.5);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#333333").ss(2,1,1).p("EAB2hMzQi7MLhhT0QhfTgAMWRQAMW3B6UaQCDVtDrO5");
	this.shape_19.setTransform(-24.2,320.5);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#333333").ss(2,1,1).p("EABzhMzQi2MQhgTuQhcTiALWPQAMW1B4UcQB/VqDnO9");
	this.shape_20.setTransform(-23.7,320.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#333333").ss(2,1,1).p("EABwhMzQiyMWhcToQhbTkAMWMQALWzB1UeQB8VlDgPD");
	this.shape_21.setTransform(-23,320.4);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#333333").ss(2,1,1).p("EABshM0QisMehZTgQhXTmALWJQALWxBwUhQB4VeDZPM");
	this.shape_22.setTransform(-22.2,320.4);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#333333").ss(2,1,1).p("EABohM0QilMnhVTWQhUTpALWFQAKWuBsUlQBzVWDQPV");
	this.shape_23.setTransform(-21.3,320.3);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#333333").ss(2,1,1).p("EABihM1QibMyhSTKQhPTuAKWAQAKWrBnUoQBsVNDFPh");
	this.shape_24.setTransform(-20.2,320.3);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#333333").ss(2,1,1).p("EABchM1QiSM/hMS8QhKTyAKV7QAJWnBgUsQBmVDC4Pt");
	this.shape_25.setTransform(-18.9,320.2);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#333333").ss(2,1,1).p("EABUhM2QiGNNhGStQhET3AJV1QAJWiBYUyQBeU3CpP8");
	this.shape_26.setTransform(-17.4,320.1);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#333333").ss(2,1,1).p("EABMhM3Qh5Neg/ScQg9T8AIVuQAHWdBQU2QBVUqCYQO");
	this.shape_27.setTransform(-15.7,320);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#333333").ss(2,1,1).p("EABChM4QhqNxg3SIQg2UCAHVnQAHWWBGU9QBKUbCGQh");
	this.shape_28.setTransform(-13.8,319.9);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#333333").ss(2,1,1).p("EAA3hM5QhZOGguRxQguUKAGVeQAGWPA7VFQA/UJBxQ3");
	this.shape_29.setTransform(-11.7,319.8);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#333333").ss(2,1,1).p("EAAuhM6QhKOZgnRdQgmUQAFVWQAGWKAxVLQA0T6BfRK");
	this.shape_30.setTransform(-9.8,319.7);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#333333").ss(2,1,1).p("EAAlhM7Qg8OqggRLQgfUWAEVPQAEWFApVQQArTtBORb");
	this.shape_31.setTransform(-8.1,319.7);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#333333").ss(2,1,1).p("EAAehM7QgxO4gaQ8QgZUaADVJQAEWAAhVWQAjTgA/Rq");
	this.shape_32.setTransform(-6.6,319.6);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#333333").ss(2,1,1).p("EAAXhM8QgnPFgUQuQgUUeADVFQADV8AaVaQAcTWAzR3");
	this.shape_33.setTransform(-5.3,319.5);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#333333").ss(2,1,1).p("EAAShM9QgePQgRQjQgPUiACVAQADV4AVVeQAVTNAoSD");
	this.shape_34.setTransform(-4.2,319.5);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#333333").ss(2,1,1).p("EAANhM9QgXPZgMQZQgMUlACU9QACV1AQVhQARTFAeSM");
	this.shape_35.setTransform(-3.2,319.4);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#333333").ss(2,1,1).p("EAAJhM9QgRPggJQRQgJUoACU5QABVyANVkQAMS/AXSU");
	this.shape_36.setTransform(-2.4,319.4);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#333333").ss(2,1,1).p("EAAGhM+QgMPogGQKQgHUpACU3QABVwAJVmQAJS6ARSb");
	this.shape_37.setTransform(-1.8,319.3);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#333333").ss(2,1,1).p("EAADhM+QgIPsgEQFQgEUrABU1QABVuAGVoQAGS2AMSg");
	this.shape_38.setTransform(-1.3,319.3);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#333333").ss(2,1,1).p("EAABhM+QgFPwgCQAQgDUtABUzQABVtAEVqQAESyAISk");
	this.shape_39.setTransform(-0.9,319.3);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#333333").ss(2,1,1).p("EAAAhM+QgCPzgCP9QgBUuAAUxQABVtADVqQACSwAFSn");
	this.shape_40.setTransform(-0.6,319.3);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#333333").ss(2,1,1).p("EAAAhM+QgCP1gBP7QAAUuAAUxQABVsACVrQABSuADSp");
	this.shape_41.setTransform(-0.4,319.3);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#333333").ss(2,1,1).p("EgABhM/QgBP3AAP5QAAUvABUxQAAVsABVrQABStACSr");
	this.shape_42.setTransform(-0.2,319.3);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#333333").ss(2,1,1).p("EgABhM/QAAP4AAP4QAAUwABUvQAAVsAAVrQABStABSs");
	this.shape_43.setTransform(-0.2,319.3);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#333333").ss(2,1,1).p("EgABhM/QABP4AAP4QAAUwAAUvQAAVsABVrQABStAASs");
	this.shape_44.setTransform(-0.2,319.3);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#333333").ss(2,1,1).p("EgABhM/QABP4AAP4QAAUwAAUvQABVsAAVrQABStAASs");
	this.shape_45.setTransform(-0.2,319.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.3,-174.5,2.4,987.5);


(lib.hastaggraphicbranche = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AjQAuIAShbIDlAAICqAAIgSBbg");
	this.shape.setTransform(0,4.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.hastaggraphicbranche, new cjs.Rectangle(-20.8,0,41.7,9.2), null);


(lib.Btn_cliquable_area = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CC9933").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape.setTransform(0,30);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Btn_cliquable_area, new cjs.Rectangle(-113,0,226,60), null);


(lib.bouclefullStoryotherside = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AggBAQgagIgaAAQgXAAgWAGIAAh8QAWgDAXgBQAnAAAmAKQAYAGAYAKQAxAVAoAjQhLAdhNAVIgKgCg");
	this.shape.setTransform(7.5,68.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAuBXQgQgkgcgcQgagaglgQQglgQgpAAQgXAAgWAFIAAh9QAXgDAWAAQBCAAA8AaQA5AYAuAtQAsAtAZA6QAXA0ADA6Qg+AFg9AEQgBglgPgjg");
	this.shape_1.setTransform(13,77.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgNDbIAKgLQBBhNAAhdQAAgngQgmQgQgkgcgcQgagbglgQQglgQgpAAQgXAAgWAFIAAh9QAWgDAXAAQBCAAA8AaQA5AYAuAtQAsAtAZA7QAaA7AABBQAABrg5BaQgQAagVAZg");
	this.shape_2.setTransform(13,90.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AitD9IAggRQAAAAAAgBQABAAAAAAQAAAAABAAQAAAAAAAAIAGgDQBRgtAxg5QBBhNAAhcQAAgpgQglQgQgkgcgcQgagcglgPQglgQgpAAQgXAAgWAFIAAh9QAWgDAXAAQBCAAA8AZQA5AZAuAtQAsAtAZA6QAaA8AABCQAABpg5BbQgtBIhPA7IgEAEIgDABQg0ArhBAcg");
	this.shape_3.setTransform(13,98.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AlWElIAwgHQBTgOBLgXIAFgCIAMgDIAmgMQAigLA/gkQAAAAABAAQAAgBAAAAQABAAAAAAQAAAAABAAIAGgDQBQgtAxg5QBChNAAhcQAAgpgQglQgPgkgcgcQgcgcgkgPQgmgQgoAAQgWAAgWAFIAAh9QAWgDAWAAQBCAAA8AZQA6AZAtAtQAtAtAZA6QAZA8AABCQAABpg4BbQgtBIhQA7IgEAEIgDABQg8AxhMAeQhuAziGAbQgyAKgyAGg");
	this.shape_4.setTransform(-2.9,103.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AoWGkIgDh7IEBAAQBXAABegPQBTgOBLgYIAEgBIAMgEIAmgMQAjgLA/gjQABgBAAAAQAAgBABAAQAAAAAAAAQABAAAAABIAGgEQBRgtAwg5QBChMABhdQgBgogQgmQgPgkgcgcQgcgbgkgQQglgQgpAAQgXAAgWAFIAAh9QAWgDAXAAQBCAAA8AaQA6AYAuAtQAsAtAZA7QAZA7AABCQAABqg4BaQgtBIhPA8IgFADIgDACQg8AwhMAfQhvAyiFAbQg+ANhBAHQg/AGg8AAg");
	this.shape_5.setTransform(-22.4,104.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("ArAGkIAsh7IIiAAQBYAABcgPQBTgOBMgYIAFgBIALgEIAngMQAigLBAgjQAAgBAAAAQABgBAAAAQAAAAABAAQAAAAAAABIAGgEQBRgtAxg5QBChMAAhdQAAgogQgmQgQgkgcgcQgbgbglgQQglgQgpAAQgXAAgWAFIAAh9QAWgDAXAAQBCAAA8AaQA6AYAuAtQAsAtAZA7QAaA7AABCQAABqg5BaQgtBIhPA8IgFADIgDACQg7AwhNAfQhvAyiGAbQg+ANhAAHQg/AGg8AAg");
	this.shape_6.setTransform(-39,104.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AtoGkIAwh7INuAAQBXAABegPQBTgOBMgYIAEgBIAMgEIAmgMQAjgLA/gjQABgBAAAAQAAgBABAAQAAAAAAAAQABAAAAABIAGgEQBRgtAwg5QBChMAAhdQAAgogQgmQgPgkgcgcQgcgbgkgQQgmgQgoAAQgXAAgWAFIAAh9QAWgDAXAAQBCAAA8AaQA6AYAtAtQAtAtAZA7QAZA7AABCQAABqg4BaQgtBIhPA8IgFADIgDACQg8AwhMAfQhvAyiGAbQg/ANhAAHQg/AGg8AAg");
	this.shape_7.setTransform(-55.9,104.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AwXGkIB9h7IR/AAQBXAABegPQBTgOBMgYIAEgBIAMgEIAmgMQAigLBAgjQABgBAAAAQAAgBAAAAQABAAAAAAQAAAAABABIAGgEQBRgtAwg5QBChMAAhdQAAgogQgmQgPgkgcgcQgcgbgkgQQgmgQgoAAQgXAAgWAFIAAh9QAWgDAXAAQBCAAA8AaQA6AYAtAtQAtAtAZA7QAZA7AABCQAABqg4BaQgtBIhQA8IgEADIgDACQg8AwhMAfQhvAyiGAbQg/ANhAAHQg/AGg8AAg");
	this.shape_8.setTransform(-73.4,104.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},2).to({state:[{t:this.shape_2}]},2).to({state:[{t:this.shape_3}]},2).to({state:[{t:this.shape_4}]},2).to({state:[{t:this.shape_5}]},2).to({state:[{t:this.shape_6}]},2).to({state:[{t:this.shape_7}]},2).to({state:[{t:this.shape_8}]},2).wait(25));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-5.5,62.1,26.1,13.3);


(lib.bouclefullStory1side = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AjuA+IB+h7IFeAAIgyB7g");
	this.shape.setTransform(-154.3,139.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("Am5A+IB9h7IL0AAIACB7g");
	this.shape_1.setTransform(-134,139.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("ApjA+IB9h7IRKAAIgBB7g");
	this.shape_2.setTransform(-117,139.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AsiBNIB9h6IR/AAQBXAABegPQAogHAmgJIBGBsQgmALgpAIQg/ANhAAHQg/AGg8AAg");
	this.shape_3.setTransform(-97.9,138.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("Au2B6IB9h6IR/AAQBXAABegPQBTgOBMgYIAEgBIAMgEIAmgMQAjgLA/gjQABgBAAAAQAAAAABgBQAAAAAAAAQABAAAAABIAGgEIABAAIB7BCQg5AuhJAcQhvAyiGAbQg+ANhBAHQg/AGg8AAg");
	this.shape_4.setTransform(-83.1,133.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AvsCpIB9h7IR/AAQBYAABdgPQBTgOBMgXIAEgBIAMgEIAngMQAigLBAgjQAAgBAAAAQAAAAABAAQAAgBAAABQABAAAAAAIAGgEQBMgqAvg0IBtA+QgnAvg3AqIgFADIgDACQg7AwhNAeQhvAyiGAbQg+ANhAAHQhAAGg8AAg");
	this.shape_5.setTransform(-77.7,129.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AwVDkIB+h7IR/AAQBXAABdgQQBUgOBLgXIAFgCIALgDIAngNQAigKBAgjQAAAAABgBQAAAAAAAAQAAAAABAAQAAAAABAAIAGgDQBQgtAxg5QAsg0AOg6IB9AEQgLBGgnA/QgtBIhQA6IgFAEIgDABQg7AxhMAeQhvAziGAbQg/ANhAAGQhAAHg7AAg");
	this.shape_6.setTransform(-73.6,123.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AwXFFIB9h7IR/AAQBXAABegQQBTgOBMgXIAEgCIAMgDIAmgMQAigLBAgkQABAAAAAAQAAgBAAAAQABAAAAAAQAAAAABAAIAGgDQBRgtAwg4QBChNAAhdQAAgngOgkIBsg7IAEAIQAZA8AABCQAABqg4BaQgtBIhQA7IgEAEIgDABQg8AxhMAeQhvAziGAbQg/ANhAAGQg/AHg8AAg");
	this.shape_7.setTransform(-73.4,113.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AwXGcIB9h7IR/AAQBXAABegQQBTgOBMgXIAEgCIAMgDIAmgMQAigLBAgkQABAAAAAAQAAgBAAAAQABAAAAAAQAAAAABAAIAGgDQBRgtAwg5QBChMAAhdQAAgpgQglQgPgkgcgcQgcgcgkgPQgPgGgOgEIA2hxIAXAJQA6AZAtAtQAtAtAZA6QAZA8AABCQAABpg4BbQgtBIhQA7IgEAEIgDABQg8AxhMAeQhvAziGAbQg/ANhAAGQg/AHg8AAg");
	this.shape_8.setTransform(-73.4,104.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AwXGkIB9h7IR/AAQBXAABegPQBTgOBMgYIAEgBIAMgEIAmgMQAigLBAgjQABgBAAAAQAAgBAAAAQABAAAAAAQAAAAABABIAGgEQBRgtAwg5QBChMAAhdQAAgogQgmQgPgkgcgcQgcgbgkgQQgmgQgoAAQgXAAgWAFIAAh9QAWgDAXAAQBCAAA8AaQA6AYAtAtQAtAtAZA7QAZA7AABCQAABqg4BaQgtBIhQA8IgEADIgDACQg8AwhMAfQhvAyiGAbQg/ANhAAHQg/AGg8AAg");
	this.shape_9.setTransform(-73.4,104.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},2).to({state:[{t:this.shape_2}]},2).to({state:[{t:this.shape_3}]},2).to({state:[{t:this.shape_4}]},2).to({state:[{t:this.shape_5}]},2).to({state:[{t:this.shape_6}]},2).to({state:[{t:this.shape_7}]},2).to({state:[{t:this.shape_8}]},2).to({state:[{t:this.shape_9}]},2).wait(65));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-178.2,133.8,47.7,12.3);


(lib.masqueTexte = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// masqueTexte_1_ligne
	this.instance = new lib.masqueTexte_1_ligne();
	this.instance.parent = this;
	this.instance.setTransform(-218.8,303.5,0.008,1,0,0,0,0,25.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(10).to({_off:false},0).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.01,x:218.8},14).wait(1));

	// masqueTexte_1_ligne
	this.instance_1 = new lib.masqueTexte_1_ligne();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-218.8,247.5,0.008,1,0,0,0,0,25.5);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(8).to({_off:false},0).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.01,x:218.8},14).to({_off:true},1).wait(2));

	// masqueTexte_1_ligne
	this.instance_2 = new lib.masqueTexte_1_ligne();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-218.8,191.5,0.008,1,0,0,0,0,25.5);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(6).to({_off:false},0).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.01,x:218.8},14).to({_off:true},1).wait(4));

	// masqueTexte_1_ligne
	this.instance_3 = new lib.masqueTexte_1_ligne();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-218.8,136.1,0.008,1,0,0,0,0,25.5);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(4).to({_off:false},0).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.01,x:218.8},14).to({_off:true},1).wait(6));

	// masqueTexte_1_ligne
	this.instance_4 = new lib.masqueTexte_1_ligne();
	this.instance_4.parent = this;
	this.instance_4.setTransform(-218.8,80.8,0.008,1,0,0,0,0,25.5);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(2).to({_off:false},0).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.01,x:218.8},14).to({_off:true},1).wait(8));

	// masqueTexte_1_ligne
	this.instance_5 = new lib.masqueTexte_1_ligne();
	this.instance_5.parent = this;
	this.instance_5.setTransform(-218.8,25.5,0.008,1,0,0,0,0,25.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.01,x:218.8},14).to({_off:true},1).wait(10));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-220.5,-1.9,3.5,53);


(lib.logofullstoryboucleanim = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.instance = new lib.bouclefullStoryotherside("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(-40.4,51.2,1,1,0,0,180,-73.4,104);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(22).to({_off:false},0).wait(28));

	// Layer_3
	this.instance_1 = new lib.bouclefullStory1side("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-186,51.2,1,1,0,0,0,-73.4,104);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(50));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-290.8,80.9,47.7,12.3);


(lib.logofullStory = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.traitfullstorycontinu();
	this.instance.parent = this;
	this.instance.setTransform(238.8,139.8,0.912,3.007,0,0,0,-0.5,2.1);

	this.instance_1 = new lib.traitfullstorycontinu();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-250.9,140.3,0.912,3.007,0,0,0,-0.5,2.1);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AidDcIAAm3IEzAAIAABHIjpAAIAAB0IDRAAIAABAIjRAAIAAB1IDxAAIAABHg");
	this.shape.setTransform(138,176.3,0.251,0.251);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgoDcIixm3IBTAAICGFIICHlIIBTAAIixG3g");
	this.shape_1.setTransform(120.1,176.3,0.251,0.251);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgkDcIAAm3IBJAAIAAG3g");
	this.shape_2.setTransform(105.2,176.3,0.251,0.251);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgkDcIAAlyIiFAAIAAhFIFTAAIAABFIiFAAIAAFyg");
	this.shape_3.setTransform(91,176.3,0.251,0.251);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AiJCjQhBhAAAhiQAAhhBChBQBFhBBjAAQBnAABEBLIgvA0QgggfgcgLQgbgMgmAAQhCAAgsArQguAqAABDQAABCAtAtQAtAtA8AAQAnAAAbgNQAbgMAfgdIAvAxQhGBNhiAAQhjAAhChAg");
	this.shape_4.setTransform(73.7,176.2,0.251,0.251);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AidDcIAAm3IE0AAIAABHIjqAAIAAB0IDSAAIAABAIjSAAIAAB1IDxAAIAABHg");
	this.shape_5.setTransform(56,176.3,0.251,0.251);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AiIDcIAAm3IBKAAIAAFwIDHAAIAABHg");
	this.shape_6.setTransform(39.6,176.3,0.251,0.251);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AiIDcIAAm3IBKAAIAAFwIDHAAIAABHg");
	this.shape_7.setTransform(23.8,176.3,0.251,0.251);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AijCiQhDhBAAhhQAAhgBDhBQBChBBhAAQBjAABBBBQBDBBAABgQAABhhDBBQhCBBhiAAQhhAAhChBgAhthvQgtAuAABBQAABCAtAvQAtAuBAAAQBBAAAtguQAtgvAAhCQAAhBgtguQgtgvhBAAQhAAAgtAvg");
	this.shape_8.setTransform(5.3,176.2,0.251,0.251);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AiICjQhChAAAhiQAAhhBChBQBDhBBmAAQBlAABFBLIgvA0QgfgfgdgLQgbgMgmAAQhCAAgtArQgtArAABCQAABCAtAtQAtAtA8AAQAnAAAbgNQAcgMAegdIAvAxQhFBNhkAAQhiAAhBhAg");
	this.shape_9.setTransform(-13.7,176.2,0.251,0.251);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AiIDcIAAm3IBKAAIAAFwIDHAAIAABHg");
	this.shape_10.setTransform(-41.7,176.3,0.251,0.251);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("ACaDcIgshkIjbAAIgsBkIhPAAIDBm3IBOAAIDCG3gAhPAzICfAAIhQi0g");
	this.shape_11.setTransform(-59.5,176.3,0.251,0.251);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgkDcIAAlyIiFAAIAAhFIFTAAIAABFIiFAAIAAFyg");
	this.shape_12.setTransform(-76.1,176.3,0.251,0.251);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgkDcIAAm3IBJAAIAAG3g");
	this.shape_13.setTransform(-90.3,176.3,0.251,0.251);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AiJCjQhBhAAAhiQAAhhBChBQBEhBBdAAQBfAAA/A3IgnA4QgbgWgZgIQgagIgjAAQhCAAgtArQgtArAABFQAABGAsAqQAsAqA9AAQA/AAAqgbIAAh4IBKAAIAACWQg+BEh0AAQhgAAhChAg");
	this.shape_14.setTransform(-105.7,176.2,0.251,0.251);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgkDcIAAm3IBJAAIAAG3g");
	this.shape_15.setTransform(-121,176.3,0.251,0.251);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AjFDcIAAm3ICXAAQB0ABBAA5QBAA6AABmQAABlg+A7Qg9A9h+AAgAh7CVIBSAAQBPABArgmQAqgmAAhKQAAiWisAAIhKAAg");
	this.shape_16.setTransform(-136.2,176.3,0.251,0.251);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("ApbIcIgjggIEbkaIAfAaQDRCvCvAAQBMAAAngeQAigaAAgsQAAgsgkgZQgwgiiZgmQkMhAh/hoQiIhvAAjnQAAjoCpiAQCjh7DwAAQCaAACcA1QCEAtBmBNIAqAfIkTETIgegSQiQhUiTAAQhBAAgkAdQgfAaAAAqQAAApAmAYQAzAiDKAyQDdA2B8BvQCEB0AADbQAADbilCJQijCGkCAAQlrAAklkMg");
	this.shape_17.setTransform(-127.5,89,0.251,0.251);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AjQMPIAApoIogu1IHMAAIEkH5IEln5IHMAAIogO1IAAJog");
	this.shape_18.setTransform(122.8,89.2,0.251,0.251);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("ADDMPIlGnXIigAAIAAHXImjAAIAA4dIJcAAQFnAACfB+QCkCBAAEYQAAFekBB+IGIIqgAkjg+IDEAAQCWAAAxgrQAvgqAAhjQAAhggtgfQg0gkiPAAIjKAAg");
	this.shape_19.setTransform(66.8,89.2,0.251,0.251);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AjRMPIAAymImhAAIAAl3ITlAAIAAF3ImhAAIAASmg");
	this.shape_20.setTransform(-70.3,89.2,0.251,0.251);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AoLMPIAA4dIGjAAIAASdIJ0AAIAAGAg");
	this.shape_21.setTransform(85.5,19.6,0.251,0.251);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AoLMPIAA4dIGjAAIAASdIJ0AAIAAGAg");
	this.shape_22.setTransform(33.3,19.6,0.251,0.251);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AnkJcQjAi6AAlSIAAtlIGjAAIAANaQAACiBIBaQBFBWB1AAQB2AABEhVQBHhbAAiiIAAtaIGjAAIAANlQAAFUi/C6Qi+C4knAAQklAAjAi6g");
	this.shape_23.setTransform(-26.7,19.8,0.251,0.251);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AomMPIAA4dIRNAAIAAF7IqrAAIAADlIKJAAIAAF6IqJAAIAAJDg");
	this.shape_24.setTransform(-84.8,19.6,0.251,0.251);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AeOaJQnqAAn7hnQoXhtm5jIQm7DJoWBsQj5Azj9AaQj+AajwAAMhPfAAAIHznqMBHsAAAQFdAAF1g9QF0g9FPhwQlRj0i8kuQjhlpAAmlQAAkHBmjwQBijoCzizQCzizDohiQDxhmEGAAQEGAADwBmQDoBiC0CzQCzCzBiDoQBmDwAAEHQAAGmjhFoQi8EtlRD1QFQBwF0A9QF1A9FeAAMBI/AAAIHvHqgAlgxfQiQA9hvBwQhvBvg9CQQg/CVAACjQAAFvEGEzQDJDsFUC5QFSi6DJjrQEGkzAAlvQAAijg/iVQg9iQhvhvQhvhwiQg9QiWg/iiAAQijAAiVA/g");
	this.shape_25.setTransform(0,104,0.251,0.251);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.logofullStory, new cjs.Rectangle(-350.9,0,690.7,182), null);


(lib.hashtag_graphic = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(64));

	// hastag graphic branche
	this.instance = new lib.hastaggraphicbranche();
	this.instance.parent = this;
	this.instance.setTransform(37.4,51.9,1.084,0.995,0,-78.3,102.1,0.1,4.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({x:6.5,y:50.1},17,cjs.Ease.quartOut).to({_off:true},5).wait(3).to({_off:false},0).to({_off:true},4).wait(2).to({_off:false,x:26.5},0).to({_off:true},5).wait(28));

	// hastag graphic branche
	this.instance_1 = new lib.hastaggraphicbranche();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-41.6,49.9,1.084,0.995,0,-78.3,102.1,0.1,4.6);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off:false},0).to({x:-7.4,y:49.8},18,cjs.Ease.quartOut).to({_off:true},4).wait(5).to({_off:false},0).to({_off:true},4).wait(31));

	// hastag graphic branche
	this.instance_2 = new lib.hastaggraphicbranche();
	this.instance_2.parent = this;
	this.instance_2.setTransform(2.7,21.7,1,1,0,0,0,0,4.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({x:-2.3,y:58.7},13,cjs.Ease.quartOut).to({_off:true},11).wait(3).to({_off:false,scaleX:2.33,scaleY:1.03,skewX:-12.8,x:25.5},0).to({_off:true},4).wait(33));

	// hastag graphic branche
	this.instance_3 = new lib.hastaggraphicbranche();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-7.3,77.7,1,1,0,0,0,0,4.6);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({x:0.7,y:41.7},14,cjs.Ease.quartOut).to({_off:true},4).wait(7).to({_off:false,regX:0.1,scaleX:1.98,scaleY:1.01,skewX:-9.8,x:19.9,y:49.7},0).to({_off:true},4).wait(4).to({_off:false,regX:0,scaleX:1,scaleY:1,skewX:0,x:0.7,y:41.7},0).to({_off:true},4).wait(21));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-18.1,17.1,64.6,57.7);


(lib.Btn_next = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{rollOver:55,rollOut:70});

	// timeline functions:
	this.frame_54 = function() {
		this.stop();
	}
	this.frame_69 = function() {
		this.stop();
	}
	this.frame_84 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(54).call(this.frame_54).wait(15).call(this.frame_69).wait(15).call(this.frame_84).wait(1));

	// ZONE
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.008)").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape.setTransform(0,29.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(85));

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AANAqIgRgbIgNAAIAAAbIgSAAIAAhTIAfAAQASAAAJAGQAJAIgBAOQAAATgPAGIAUAegAgRAAIANAAQAJAAAEgDQAEgDgBgHQAAgHgDgCQgEgDgIAAIgOAAg");
	this.shape_1.setTransform(57.1,29.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgeAqIAAhTIA8AAIAAARIgpAAIAAARIAkAAIAAAPIgkAAIAAARIAqAAIAAARg");
	this.shape_2.setTransform(40.7,29.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAOAqIgSgbIgNAAIAAAbIgTAAIAAhTIAgAAQATAAAIAGQAJAIAAAOQgBATgPAGIAUAegAgRAAIAOAAQAJAAADgDQAEgDAAgHQAAgHgFgCQgDgDgIAAIgOAAg");
	this.shape_3.setTransform(24.6,29.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgeAfQgNgMAAgTQAAgRANgNQAMgNASAAQATAAANANQAMANAAARQAAATgMAMQgNANgTAAQgSAAgMgNgAgRgSQgHAIAAAKQAAAMAHAHQAIAIAJAAQAKAAAIgIQAHgHAAgMQAAgKgHgIQgIgIgKAAQgJAAgIAIg");
	this.shape_4.setTransform(6.9,29.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgaAqIAAhTIASAAIAABCIAjAAIAAARg");
	this.shape_5.setTransform(-9.1,29.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AggAqIAAhTIAdAAQATAAAIAIQAJAHAAAPQAAAPgJAIQgJAGgSAAIgLAAIAAAYgAgOADIANAAQAIAAAEgEQADgDAAgHQAAgIgEgCQgFgEgIAAIgLAAg");
	this.shape_6.setTransform(-24.6,29.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AASAqIgSgcIgRAcIgWAAIAcgqIgbgpIAXAAIAPAZIAQgZIAXAAIgaAoIAcArg");
	this.shape_7.setTransform(-41.1,29.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgeAqIAAhTIA8AAIAAARIgpAAIAAARIAkAAIAAAPIgkAAIAAARIAqAAIAAARg");
	this.shape_8.setTransform(-57,29.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#333333").ss(1,1,1).p("AxpAAMAjTAAA");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(85));

	// Calque_5
	this.instance = new lib.Btn_cliquable_area();
	this.instance.parent = this;
	this.instance.setTransform(0,29.5,1,1,0,0,0,0,30);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({alpha:0.012},46).wait(6).to({y:30},0).to({alpha:1},14).wait(1).to({alpha:0.012},13).wait(2));

	// BG
	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("rgba(0,0,0,0.098)").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape_10.setTransform(0,29.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_10).wait(85));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-114,-1,228,60.5);


(lib.bloc_NEXT_btn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{close:17});

	// timeline functions:
	this.frame_16 = function() {
		this.stop();
	}
	this.frame_33 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(16).call(this.frame_16).wait(17).call(this.frame_33).wait(1));

	// CTA DETAIL
	this.btnNext = new lib.Btn_next();
	this.btnNext.name = "btnNext";
	this.btnNext.parent = this;
	this.btnNext.setTransform(-340.5,411.5,1,1,0,0,0,0,26.5);

	this.timeline.addTween(cjs.Tween.get(this.btnNext).to({y:350.5},16,cjs.Ease.quartInOut).wait(2).to({y:411.5},14,cjs.Ease.quartInOut).to({_off:true},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-454,384.5,227,60);


(lib.bloc_lignes = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{close_OLD:116,"close":159});

	// timeline functions:
	this.frame_115 = function() {
		this.stop();
	}
	this.frame_158 = function() {
		this.stop();
	}
	this.frame_179 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(115).call(this.frame_115).wait(43).call(this.frame_158).wait(21).call(this.frame_179).wait(1));

	// ligne
	this.instance = new lib.ligne();
	this.instance.parent = this;
	this.instance.setTransform(539.5,406,1,1,0,0,0,0,406);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(4).to({_off:false},0).to({x:-454},49,cjs.Ease.quartOut).wait(63).to({x:-883},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:-454},0).wait(21));

	// ligne
	this.instance_1 = new lib.ligne();
	this.instance_1.parent = this;
	this.instance_1.setTransform(533.7,406.5,1,1,0,0,0,0,406);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(9).to({_off:false},0).to({x:-227},49,cjs.Ease.quartOut).wait(58).to({x:-883.2},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:-227},0).wait(21));

	// ligne
	this.instance_2 = new lib.ligne();
	this.instance_2.parent = this;
	this.instance_2.setTransform(537,406.5,1,1,0,0,0,0,406);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(14).to({_off:false},0).to({x:0},49,cjs.Ease.quartOut).wait(53).to({x:-883.4},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:0},0).wait(21));

	// ligne
	this.instance_3 = new lib.ligne();
	this.instance_3.parent = this;
	this.instance_3.setTransform(540.2,406,1,1,0,0,0,0,406);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(19).to({_off:false},0).to({x:227},49,cjs.Ease.quartOut).wait(6).to({y:320},40,cjs.Ease.quartInOut).to({_off:true},2).wait(43).to({_off:false},0).to({y:406},17,cjs.Ease.quartInOut).wait(4));

	// ligne
	this.instance_4 = new lib.ligne();
	this.instance_4.parent = this;
	this.instance_4.setTransform(543.6,406,1,1,0,0,0,0,406);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(24).to({_off:false},0).to({x:454.1},49,cjs.Ease.quartOut).wait(43).to({x:-883.4},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:454.1},0).wait(21));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.bloc_hashtag = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_126 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(126).call(this.frame_126).wait(1));

	// Layer_1
	this.instance = new lib.hashtag_graphic("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(-456,243.2,2.317,2.317,0,0,0,-0.1,50.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true},61).wait(66));

	// Layer_5 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_28 = new cjs.Graphics().p("AtWBpIAAjRIatAAIAADRg");
	var mask_graphics_32 = new cjs.Graphics().p("AtWDSIAAmjIatAAIAAGjg");
	var mask_graphics_40 = new cjs.Graphics().p("AudExIAAphIc7AAIAAJhg");
	var mask_graphics_42 = new cjs.Graphics().p("AuiHPIAAueIdFAAIAAOeg");
	var mask_graphics_95 = new cjs.Graphics().p("AuiPTIAA+lIdFAAIAAelg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(28).to({graphics:mask_graphics_28,x:-400.4,y:265.5}).wait(4).to({graphics:mask_graphics_32,x:-400.4,y:255}).wait(8).to({graphics:mask_graphics_40,x:-393.4,y:245.6}).wait(2).to({graphics:mask_graphics_42,x:-392.9,y:261.4}).wait(53).to({graphics:mask_graphics_95,x:-392.9,y:313.1}).wait(32));

	// Layer_3
	this.instance_1 = new lib.txt_hashtag();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-421.5,244.1,1,1,0,0,0,0,31.6);
	this.instance_1._off = true;

	var maskedShapeInstanceList = [this.instance_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(28).to({_off:false},0).wait(99));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-497.8,166.1,149.7,129.4);


(lib.blocInfostextes = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":117});

	// timeline functions:
	this.frame_116 = function() {
		this.stop();
	}
	this.frame_159 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(116).call(this.frame_116).wait(43).call(this.frame_159).wait(1));

	// Bloc Hashtag
	this.instance = new lib.bloc_hashtag();
	this.instance.parent = this;
	this.instance.setTransform(-391.5,355.2,0.9,0.9,0,0,0,-412.9,244.7);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(94).to({_off:false},0).to({_off:true},23).wait(43));

	// masque Titre
	this.instance_1 = new lib.masque_generique();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-527.9,-13.9,0.007,0.471,0,0,0,0,17.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(66).to({_off:false},0).to({regX:0.5,scaleX:1.92,x:-527.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.01,x:-228.9},10,cjs.Ease.quartInOut).to({_off:true},1).wait(31).to({_off:false},0).to({regX:0.5,scaleX:1.92,x:-527.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.01,x:-528.7},10,cjs.Ease.quartInOut).to({_off:true},1).wait(23));

	// titre
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CC9933").s().p("AAWA4IAAgsIgrAAIAAAsIgZAAIAAhvIAZAAIAAAvIArAAIAAgvIAZAAIAABvg");
	this.shape.setTransform(-380.7,-14.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#CC9933").s().p("AgiApQgSgQABgYQgBgZASgQQAQgRAYAAQAcAAASAVIgPASQgMgPgSAAQgNAAgJAJQgKAJAAAPQgBAPAKAKQAJAKAMAAQATAAALgPIAQARQgSAVgaAAQgZAAgQgRg");
	this.shape_1.setTransform(-404.5,-14.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#CC9933").s().p("AgMA4IAAhZIgfAAIAAgWIBXAAIAAAWIggAAIAABZg");
	this.shape_2.setTransform(-427.4,-14.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#CC9933").s().p("AgMA4IAAhvIAZAAIAABvg");
	this.shape_3.setTransform(-446.8,-14.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CC9933").s().p("AgrA4IAAhvIAnAAQAZAAAMAKQALAKAAAVQAAAUgMAJQgMAKgYAAIgOAAIAAAfgAgSADIAQAAQAMAAAEgEQAFgFAAgKQAAgJgGgEQgGgEgLgBIgOAAg");
	this.shape_4.setTransform(-466.5,-14.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},75).to({state:[]},51).wait(34));

	// Lettres aléatoires
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#CC9933").s().p("AkEECQhqhoAAiaQAAiZBqhnQBqhoCaAAQCbAABqBoQBqBnAACZQAACahqBoQhqBnibAAQiaAAhqhngAiSieQg8BCAABcQAABeA8BBQA8BCBXAAQBXAAA8hCQA8hBAAheQAAhcg8hCQg8hChXAAQhXAAg8BCg");
	this.shape_5.setTransform(-319.5,131.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#CC9933").s().p("AjiFdIAAq5ICcAAIAAIvIEpAAIAACKg");
	this.shape_6.setTransform(-398.3,131.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#CC9933").s().p("AjiFeIAAq6ICcAAIAAIuIEpAAIAACMg");
	this.shape_7.setTransform(-513.5,7.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#CC9933").s().p("AkAFdIAAq5IH3AAIAACKIlbAAIAACQIE4AAIAACEIk4AAIAACRIFmAAIAACKg");
	this.shape_8.setTransform(-345.4,-116);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#CC9933").s().p("ACLFdIAAkSIkVAAIAAESIicAAIAAq5ICcAAIAAEjIEVAAIAAkjICcAAIAAK5g");
	this.shape_9.setTransform(-506.7,-116);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#CC9933").s().p("AkEEBQhqhnAAiaQAAiZBqhoQBqhnCaAAQCbAABqBnQBqBoAACZQAACahqBnQhqBoibAAQiaAAhqhogAiSieQg8BCAABdQAABdA8BBQA8BCBXAAQBXAAA8hCQA8hBAAhdQAAhdg8hCQg8hChXAAQhXAAg8BCg");
	this.shape_10.setTransform(-204.3,7.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#CC9933").s().p("AjiFdIAAq5ICcAAIAAIuIEpAAIAACLg");
	this.shape_11.setTransform(-309.5,-116);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#CC9933").s().p("AkAFdIAAq5IH3AAIAACKIlcAAIAACQIE4AAIAACEIk4AAIAACRIFmAAIAACKg");
	this.shape_12.setTransform(-383.8,-116);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#CC9933").s().p("AkEEBQhqhnAAiaQAAiZBqhoQBqhnCaAAQCbAABqBnQBqBoAACZQAACahqBnQhqBoibAAQiaAAhqhogAiSieQg8BCAABdQAABdA8BBQA8BCBWAAQBYAAA8hCQA8hBAAhdQAAhdg8hCQg8hChYAAQhWAAg8BCg");
	this.shape_13.setTransform(-426.7,7.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#CC9933").s().p("AjiFdIAAq5ICcAAIAAIuIEpAAIAACLg");
	this.shape_14.setTransform(-271.1,-116);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#CC9933").s().p("AkBFdIAAq5IH3AAIAACKIlbAAIAACQIE4AAIAACEIk4AAIAACRIFmAAIAACKg");
	this.shape_15.setTransform(-363.8,-116);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_9,p:{x:-506.7}},{t:this.shape_8},{t:this.shape_7,p:{x:-513.5}},{t:this.shape_6},{t:this.shape_5}]},46).to({state:[{t:this.shape_9,p:{x:-506.7}},{t:this.shape_12},{t:this.shape_11,p:{x:-309.5}},{t:this.shape_7,p:{x:-283.1}},{t:this.shape_10}]},6).to({state:[{t:this.shape_9,p:{x:-506.7}},{t:this.shape_12},{t:this.shape_14,p:{x:-271.1}},{t:this.shape_11,p:{x:-202.3}},{t:this.shape_13,p:{x:-426.7,y:7.4}}]},5).to({state:[{t:this.shape_9,p:{x:-486.7}},{t:this.shape_15},{t:this.shape_14,p:{x:-289.5}},{t:this.shape_11,p:{x:-143.9}},{t:this.shape_13,p:{x:-65.1,y:-116.4}}]},5).to({state:[{t:this.shape_11,p:{x:-433.5}},{t:this.shape_13,p:{x:-354.7,y:-116.4}}]},6).to({state:[]},7).wait(85));

	// elements geometriques
	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#FFFFFF").ss(1,1,1).p("AW3ISI4mONI1HzBILk58IcQC+g");
	this.shape_16.setTransform(-22.7,68.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#FFFFFF").ss(1,1,1).p("AnFmSICWm8ISbU+I7XFfIAAgB");
	this.shape_17.setTransform(-240,-49.6);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AqGJxIGnzhIAAAAINmPeI0MEDg");
	this.shape_18.setTransform(-262.9,-27.5);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#FFFFFF").ss(1,1,1).p("ALnNZI3NtWIXJtbg");
	this.shape_19.setTransform(-577,114.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_16}]},118).to({state:[{t:this.shape_18},{t:this.shape_17}]},4).to({state:[{t:this.shape_19}]},3).to({state:[]},6).wait(29));

	// Masque Logo 2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_43 = new cjs.Graphics().p("AnyHXIAAutIPlAAIAAOtg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(43).to({graphics:mask_graphics_43,x:-254.9,y:75.9}).wait(103).to({graphics:null,x:0,y:0}).wait(14));

	// logo Full Story 2
	this.instance_2 = new lib.logofullStory();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-308,77,0.387,0.387,0,0,0,-0.7,90.9);
	this.instance_2._off = true;

	var maskedShapeInstanceList = [this.instance_2];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(43).to({_off:false},0).to({y:67},10,cjs.Ease.quartOut).to({_off:true},1).wait(106));

	// Masque Logo (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	var mask_1_graphics_22 = new cjs.Graphics().p("EgkmAhwMAAAhDfMBJNAAAMAAABDfg");
	var mask_1_graphics_28 = new cjs.Graphics().p("EgoOARRMAAAgihMBQdAAAMAAAAihg");
	var mask_1_graphics_43 = new cjs.Graphics().p("AmhHXIAAutINDAAIAAOtg");
	var mask_1_graphics_149 = new cjs.Graphics().p("EgjBAD/IAAtGMAjWAAAIAANGg");
	var mask_1_graphics_150 = new cjs.Graphics().p("EgjBAD/IAAtGIdhAAIAANGg");
	var mask_1_graphics_151 = new cjs.Graphics().p("EgjBAD/IAAtGIXrAAIAANGg");
	var mask_1_graphics_152 = new cjs.Graphics().p("EgjBAD/IAAtGIR1AAIAANGg");
	var mask_1_graphics_153 = new cjs.Graphics().p("EgjBAD/IAAtGIL/AAIAANGg");
	var mask_1_graphics_154 = new cjs.Graphics().p("EgjBAD/IAAtGIGJAAIAANGg");
	var mask_1_graphics_155 = new cjs.Graphics().p("EgjBAD/IAAtGIASAAIAANGg");

	this.timeline.addTween(cjs.Tween.get(mask_1).to({graphics:null,x:0,y:0}).wait(22).to({graphics:mask_1_graphics_22,x:-47.6,y:159}).wait(6).to({graphics:mask_1_graphics_28,x:-235,y:-5}).wait(6).to({graphics:null,x:0,y:0}).wait(9).to({graphics:mask_1_graphics_43,x:-347.8,y:75.9}).wait(11).to({graphics:null,x:0,y:0}).wait(95).to({graphics:mask_1_graphics_149,x:-224.2,y:-58.4}).wait(1).to({graphics:mask_1_graphics_150,x:-224.2,y:-58.4}).wait(1).to({graphics:mask_1_graphics_151,x:-224.2,y:-58.4}).wait(1).to({graphics:mask_1_graphics_152,x:-224.2,y:-58.4}).wait(1).to({graphics:mask_1_graphics_153,x:-224.2,y:-58.4}).wait(1).to({graphics:mask_1_graphics_154,x:-224.2,y:-58.4}).wait(1).to({graphics:mask_1_graphics_155,x:-224.2,y:-58.4}).wait(1).to({graphics:null,x:0,y:0}).wait(4));

	// logo Full Story
	this.instance_3 = new lib.logofullStory();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-279.4,172.6,1.708,1.708,0,0,0,0,91);
	this.instance_3._off = true;

	var maskedShapeInstanceList = [this.instance_3];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(22).to({_off:false},0).wait(6).to({regX:-0.1,regY:90.9,scaleX:1.88,scaleY:1.88,x:-102.1,y:99.1},0).wait(6).to({regX:-0.7,regY:90.7,scaleX:0.72,scaleY:0.72,x:-440.5,y:29.8},0).wait(9).to({regY:90.9,scaleX:0.39,scaleY:0.39,x:-308,y:77},0).to({y:87},10,cjs.Ease.quartOut).wait(1).to({x:-328,y:57},0).wait(2).to({regX:-0.5,regY:90.8,scaleX:0.33,scaleY:0.33,x:-269.6,y:119.1},0).wait(9).to({regX:-0.6,regY:91.2,scaleX:0.35,scaleY:0.35,x:-389.2,y:69.1},0).to({regY:90.5,y:-140.3},31,cjs.Ease.quartOut).wait(21).to({regY:90.6},0).to({regX:-0.5,regY:90.8,scaleX:0.33,scaleY:0.33,x:-333.6,y:-73.2},17,cjs.Ease.quartOut).wait(8).to({_off:true},14).wait(4));

	// segment droite
	this.instance_4 = new lib.traitfullstorycontinu();
	this.instance_4.parent = this;
	this.instance_4.setTransform(74.8,74.6,2.045,1.165,0,0,0,0.1,2.3);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(43).to({_off:false},0).wait(7).to({scaleX:2.51,x:230.8,y:110.1},0).wait(6).to({regX:-0.1,regY:2,scaleX:2.05,scaleY:1,x:230.3,y:85},0).wait(9).to({x:76.3,y:86},0).to({y:293},31,cjs.Ease.quartOut).wait(21).to({y:-57},17,cjs.Ease.quartOut).to({regX:0.6,scaleX:0.27,x:34.5,y:-57.1},10).to({_off:true},1).wait(15));

	// segment centre
	this.instance_5 = new lib.traitfullstorycontinu();
	this.instance_5.parent = this;
	this.instance_5.setTransform(60.2,117,2.919,2.211,0,0,0,0,2.4);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(37).to({_off:false},0).wait(6).to({regY:2.3,scaleX:0.95,scaleY:1.17,x:-100,y:95.2},0).wait(5).to({regX:-0.1,regY:2.2,scaleX:1.01,x:-85.4,y:76.1},0).wait(6).to({regY:2,scaleX:1.03,scaleY:1,x:-109.1,y:85},0).wait(11).to({regX:-0.4,scaleX:0.54,x:-210.3,y:86},0).to({y:156},12,cjs.Ease.quartOut).to({_off:true},2).wait(38).to({_off:false,regX:-0.3,scaleX:1.03,x:-109.8,y:-356.7},0).to({y:-56.7},17,cjs.Ease.quartOut).wait(10).to({scaleX:0.17,x:-204.7},6).to({_off:true},1).wait(9));

	// segment gauche
	this.instance_6 = new lib.traitfullstorycontinu();
	this.instance_6.parent = this;
	this.instance_6.setTransform(-544.7,228.3,0.775,2.323,0,0,0,-0.3,2);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(35).to({_off:false},0).wait(9).to({regX:-0.1,scaleX:0.37,scaleY:1.11,x:-431.1,y:96.2},0).wait(11).to({x:-504.1,y:75.9},0).wait(2).to({regX:0,scaleX:0.2,scaleY:1,x:-246.5,y:242},0).to({x:-470.5},8,cjs.Ease.quartOut).to({y:43.1},21,cjs.Ease.quartOut).wait(31).to({y:-57.1},17,cjs.Ease.quartOut).to({_off:true},22).wait(4));

	// boucle masque (mask)
	var mask_2 = new cjs.Shape();
	mask_2._off = true;
	var mask_2_graphics_0 = new cjs.Graphics().p("AzwS1MAAAglpMAnhAAAMAAAAlpg");

	this.timeline.addTween(cjs.Tween.get(mask_2).to({graphics:mask_2_graphics_0,x:-218.5,y:185.5}).wait(22).to({graphics:null,x:0,y:0}).wait(138));

	// Boucle animation
	this.instance_7 = new lib.logofullstoryboucleanim("synched",14);
	this.instance_7.parent = this;
	this.instance_7.setTransform(-447.9,204.5,1.787,1.787,0,0,0,-238.3,72.1);

	var maskedShapeInstanceList = [this.instance_7];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_2;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_7).to({_off:true},22).wait(138));

	// Layer_11
	this.instance_8 = new lib.masqueTexte("synched",0);
	this.instance_8.parent = this;
	this.instance_8.setTransform(-141.8,99.2,1.387,0.79,0,0,0,-0.1,109);
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(74).to({_off:false},0).to({_off:true},40).wait(3).to({_off:false,regX:0.3,regY:108.7,scaleX:1.39,scaleY:0.77,rotation:180,x:-142.9,y:182.4},0).to({_off:true},39).wait(4));

	// Layer_12
	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgVAWQgKgJAAgNQAAgMAKgKQAJgJAMAAQANAAAKAJQAJAKAAAMQAAANgJAJQgKAKgNAAQgMAAgJgKg");
	this.shape_20.setTransform(-221.5,259.5);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgmBcQgXgHgTgQIAYghQAfAXAgAAQALAAAGgEQAGgFAAgGQAAgHgIgGQgJgGgPgFQgQgEgJgEQgIgEgLgHQgVgNAAgaQgBgbAWgRQAVgRAiAAQAiAAAiAXIgUAkQgZgRgZAAQgYAAAAAOQAAAHAIAEQAHAEASAGIAaAJQAIADAKAHQAVALAAAdQAAAdgVARQgWARggAAQgVAAgXgIg");
	this.shape_21.setTransform(-236.3,252.7);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AhEBJQgcgbAAguQAAgtAdgbQAdgbAnAAQAoAAAcAYQAcAZAAAqIAAAcIiMAAQADAPANAKQAOAKASAAQAcAAASgTIAeAhQggAfgsAAQgrAAgegbgAgagsQgOAJgCARIBVAAQgCgRgMgKQgKgJgRAAQgOAAgOAKg");
	this.shape_22.setTransform(-256.4,252.7);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgbBhIhMjBIA4AAIAvB5IAxh5IA3AAIhNDBg");
	this.shape_23.setTransform(-278.2,252.7);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgaCIIAAjAIA1AAIAADAgAgWhRQgIgJgBgOQABgNAIgJQAJgJANAAQAOAAAIAJQAJAJAAANQAAAOgJAJQgIAJgOAAQgNAAgJgJg");
	this.shape_24.setTransform(-294,248.7);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgcBtQgTgTgBgfIAAhZIgWAAIAAgmIAWAAIAAg6IA2AAIAAA6IAvAAIAAAmIgvAAIAABWQAAALAGAHQAFAGAJAAQAPAAAKgOIAUAmQgZAXgcAAQgbAAgTgSg");
	this.shape_25.setTransform(-306.7,249.9);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("Ag9BJQgegcAAgsQAAgsAegcQAegcApAAQAVAAAWAJQAVAIAQARIgcAlQgJgLgOgGQgNgHgOABQgUgBgPAOQgPAOAAAYQAAAZAPAOQAPANAUAAQAbAAAVgbIAgAkQglAngsAAQgqAAgegbg");
	this.shape_26.setTransform(-325.4,252.7);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AhCBSQgUgRgBgeQAAgeAWgOQAWgOAlgBIAnAAIAAAAQABgfghgBQgNAAgQAGQgQAFgKAJIgYgjQAmgcAwAAQAjAAAWASQAWARAAAnIAAB6IgyAAIAAgXQgVAageAAQgfAAgVgSgAggAiQAAAKAIAGQAHAFAPABQANAAALgJQALgJAAgPIAAgJIghAAQggAAAAAUg");
	this.shape_27.setTransform(-347.2,252.7);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("Ag3BjIAAjBIA2AAIAAAZQAJgMAQgIQAQgIAQgBIAAA0IgKAAQgYgBgMASQgLARAAAbIAABUg");
	this.shape_28.setTransform(-364.1,252.5);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AhEBJQgcgbAAguQAAgtAdgbQAegbAnAAQAoAAAbAYQAcAZAAAqIAAAcIiMAAQACAPAPAKQAOAKAQAAQAcAAATgTIAfAhQgiAfgsAAQgrAAgdgbgAgagsQgOAJgBARIBTAAQgBgRgLgKQgMgJgPAAQgPAAgOAKg");
	this.shape_29.setTransform(-383.3,252.7);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AgcBtQgUgTAAgfIAAhZIgWAAIAAgmIAWAAIAAg6IA2AAIAAA6IAvAAIAAAmIgvAAIAABWQAAALAGAHQAGAGAHAAQAPAAALgOIAUAmQgZAXgcAAQgbAAgTgSg");
	this.shape_30.setTransform(-401.9,249.9);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AAkBjIAAhtQABgsghAAQgPAAgMALQgNAMAAAXIAABrIg2AAIAAjBIA2AAIAAAWQAZgaAdAAQAfAAAVAWQAUAWAAAkIAAB1g");
	this.shape_31.setTransform(-421.7,252.5);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AgaCIIAAjAIA1AAIAADAgAgVhRQgJgJAAgOQAAgNAJgJQAIgJANAAQAOAAAJAJQAIAJABANQgBAOgIAJQgJAJgOAAQgNAAgIgJg");
	this.shape_32.setTransform(-438.6,248.7);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AglBcQgYgIgTgPIAYghQAfAXAgAAQALAAAGgFQAGgEAAgGQAAgHgIgGQgJgGgPgFQgQgEgJgFQgIgDgLgHQgWgMABgbQAAgbAVgRQAVgRAiAAQAiAAAiAXIgUAkQgZgRgZAAQgYAAAAAOQAAAHAIAEQAHAFASAFIAaAJQAIAEALAGQAUALAAAcQAAAegVAQQgVASggAAQgVAAgXgIg");
	this.shape_33.setTransform(-230.2,208.8);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AAkBiIAAhsQAAgtggAAQgPAAgNAMQgMAMABAWIAABrIg2AAIAAjAIA2AAIAAAVQAYgZAdAAQAfABAVAVQAVAWgBAjIAAB1g");
	this.shape_34.setTransform(-250.7,208.6);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AhIBHQgdgcAAgrQAAgpAdgdQAdgdArAAQAsAAAdAdQAdAdAAApQAAArgdAcQgdAdgsAAQgrAAgdgdgAghglQgOAPAAAWQAAAYAOAPQANAPAUAAQAVAAAOgPQANgPAAgYQAAgWgNgPQgOgQgVAAQgUAAgNAQg");
	this.shape_35.setTransform(-274,208.8);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AgaCIIAAjAIA1AAIAADAgAgVhRQgKgJABgOQgBgNAKgJQAIgJANAAQANAAAKAJQAIAJABANQgBAOgIAJQgKAJgNAAQgNAAgIgJg");
	this.shape_36.setTransform(-290.9,204.8);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AgcBtQgTgTAAgfIAAhZIgXAAIAAgmIAXAAIAAg6IA1AAIAAA6IAvAAIAAAmIgvAAIAABWQAAALAGAHQAFAGAIAAQAQAAAKgOIAUAmQgZAXgcAAQgbAAgTgSg");
	this.shape_37.setTransform(-303.6,206);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("AhBBSQgWgSAAgdQABgeAVgOQAWgPAlAAIAoAAIAAgBQAAgeghAAQgNgBgQAGQgPAFgLAJIgZgjQAmgcAxAAQAjAAAWASQAWARABAmIAAB7IgzAAIAAgYQgVAbgeAAQgeAAgVgSgAggAiQAAALAHAFQAJAFAOAAQANABALgKQALgIAAgPIAAgJIghAAQggAAAAAUg");
	this.shape_38.setTransform(-323.1,208.8);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AgaCFIAAkJIA1AAIAAEJg");
	this.shape_39.setTransform(-338.6,205.1);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("AgaCFIAAkJIA1AAIAAEJg");
	this.shape_40.setTransform(-349.1,205.1);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("AhCBSQgUgSgBgdQAAgeAWgOQAWgPAlAAIAoAAIAAgBQAAgeghAAQgNgBgQAGQgQAFgKAJIgZgjQAngcAwAAQAjAAAWASQAWARABAmIAAB7IgzAAIAAgYQgVAbgeAAQgfAAgVgSgAggAiQAAALAHAFQAJAFAOAAQANABALgKQALgIAAgPIAAgJIghAAQggAAAAAUg");
	this.shape_41.setTransform(-365.5,208.8);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("AgcBtQgTgTAAgfIAAhZIgXAAIAAgmIAXAAIAAg6IA1AAIAAA6IAvAAIAAAmIgvAAIAABWQAAALAGAHQAFAGAIAAQAQAAAKgOIAUAmQgZAXgcAAQgbAAgTgSg");
	this.shape_42.setTransform(-383.2,206);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("AgmBcQgXgIgTgPIAYghQAfAXAgAAQALAAAGgFQAGgEAAgGQAAgHgJgGQgIgGgPgFQgQgEgIgFQgJgDgLgHQgVgMgBgbQAAgbAWgRQAVgRAiAAQAiAAAiAXIgUAkQgZgRgZAAQgYAAAAAOQAAAHAIAEQAIAFARAFIAaAJQAIAEAKAGQAVALAAAcQAAAegVAQQgVASghAAQgUAAgYgIg");
	this.shape_43.setTransform(-401.3,208.8);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFFFFF").s().p("AAkBiIAAhsQABgtghAAQgPAAgMAMQgNAMAAAWIAABrIg2AAIAAjAIA2AAIAAAVQAZgZAdAAQAfABAVAVQAUAWAAAjIAAB1g");
	this.shape_44.setTransform(-421.7,208.6);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFFFFF").s().p("AgaCIIAAjAIA1AAIAADAgAgVhRQgJgJAAgOQAAgNAJgJQAIgJANAAQAOAAAJAJQAIAJABANQgBAOgIAJQgJAJgOAAQgNAAgIgJg");
	this.shape_45.setTransform(-438.6,204.8);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FFFFFF").s().p("AglBcQgYgIgTgPIAYgiQAeAYAiAAQAKAAAGgFQAGgDAAgIQAAgGgJgGQgIgFgPgGQgQgEgIgFQgJgDgKgHQgXgNAAgaQABgbAVgRQAWgRAhAAQAiAAAiAXIgUAkQgZgRgZAAQgYAAAAANQAAAIAIAFQAIADARAHIAaAJQAIADALAGQAUALAAAcQAAAdgVARQgVASggAAQgWAAgWgIg");
	this.shape_46.setTransform(-173.7,164.9);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFFFFF").s().p("AhEBIQgcgbAAgtQAAgtAdgbQAegbAnAAQAoAAAbAYQAcAZAAApIAAAcIiMAAQACAQAPAKQAOAKAQAAQAcAAATgTIAfAhQgiAfgsAAQgqAAgegcgAgagtQgOAKgBARIBTAAQgBgSgLgJQgMgJgPAAQgPAAgOAJg");
	this.shape_47.setTransform(-193.8,164.9);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FFFFFF").s().p("AhIBqQgbgeAAgrQAAgqAagcQAbgbAjAAQAiAAAWAZIAAhfIA3AAIAAEKIg3AAIAAgaQgWAdgiAAQgiAAgbgdgAgfgCQgOAOAAAXQAAAYAOAPQAOAPASAAQATAAANgPQANgPAAgYQAAgXgNgPQgNgQgTAAQgSABgOAQg");
	this.shape_48.setTransform(-217.6,161.4);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FFFFFF").s().p("AgcBtQgTgTAAgfIAAhZIgXAAIAAgmIAXAAIAAg6IA1AAIAAA6IAvAAIAAAmIgvAAIAABWQAAALAGAHQAFAGAIAAQAQAAAKgOIAUAmQgZAXgcAAQgbAAgTgSg");
	this.shape_49.setTransform(-246,162.1);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FFFFFF").s().p("AhEBIQgcgbAAgtQAAgtAdgbQAdgbAnAAQAoAAAcAYQAcAZAAApIAAAcIiMAAQADAQANAKQAOAKASAAQAcAAASgTIAeAhQggAfgsAAQgrAAgegcgAgagtQgOAKgCARIBVAAQgCgSgMgJQgKgJgRAAQgOAAgOAJg");
	this.shape_50.setTransform(-265.4,164.9);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FFFFFF").s().p("AglBcQgYgIgTgPIAYgiQAeAYAiAAQAKAAAGgFQAGgDAAgIQAAgGgJgGQgIgFgPgGQgQgEgIgFQgJgDgKgHQgXgNAAgaQABgbAVgRQAWgRAhAAQAiAAAiAXIgUAkQgZgRgZAAQgYAAAAANQAAAIAIAFQAIADARAHIAaAJQAIADALAGQAUALAAAcQAAAdgVARQgWASgfAAQgWAAgWgIg");
	this.shape_51.setTransform(-295.8,164.9);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FFFFFF").s().p("AhDBIQgdgbAAgtQAAgtAdgbQAegbAnAAQAoAAAbAYQAcAZAAApIAAAcIiMAAQACAQAOAKQAOAKARAAQAcAAATgTIAfAhQgiAfgsAAQgqAAgdgcgAgagtQgOAKgBARIBTAAQgBgSgLgJQgMgJgPAAQgPAAgOAJg");
	this.shape_52.setTransform(-315.9,164.9);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FFFFFF").s().p("AgaCFIAAkJIA1AAIAAEJg");
	this.shape_53.setTransform(-332.3,161.2);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FFFFFF").s().p("AhCBSQgUgSAAgdQgBgeAWgOQAWgPAmAAIAmAAIAAgBQABgeghAAQgNAAgQAFQgPAFgMAIIgXgjQAmgbAvAAQAkAAAWARQAXASgBAmIAAB7IgyAAIAAgYQgVAbgeAAQgeAAgWgSgAggAiQAAALAIAFQAHAGAPgBQANAAALgJQALgIAAgPIAAgJIghAAQggAAAAAUg");
	this.shape_54.setTransform(-348.7,164.9);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FFFFFF").s().p("AgcBtQgTgTgBgfIAAhZIgWAAIAAgmIAWAAIAAg6IA2AAIAAA6IAvAAIAAAmIgvAAIAABWQAAALAGAHQAFAGAJAAQAOAAALgOIAUAmQgZAXgcAAQgbAAgTgSg");
	this.shape_55.setTransform(-366.4,162.1);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FFFFFF").s().p("AgaCIIAAjAIA1AAIAADAgAgVhRQgJgJAAgOQAAgNAJgJQAIgJANAAQAOAAAJAJQAIAJABANQgBAOgIAJQgJAJgOAAQgNAAgIgJg");
	this.shape_56.setTransform(-380.1,160.9);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FFFFFF").s().p("AgvCCQgWgIgRgMIAUgpQAbAUAeAAQAdAAARgPQARgOAAggQgZAcglAAQgmAAgbgaQgagZAAgrQAAgrAbgbQAbgcAjAAQAhAAAXAdIAAgaIA2AAIAACoQAAAagJAUQgJAUgPAMQgeAYgoAAQgVAAgXgHgAgehMQgOANAAAXQAAAXANAOQAOAOASAAQAUAAANgNQANgOAAgYQAAgXgNgNQgNgOgTAAQgTAAgNAOg");
	this.shape_57.setTransform(-397.9,168.5);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#FFFFFF").s().p("AgaCIIAAjAIA1AAIAADAgAgWhRQgIgJgBgOQABgNAIgJQAJgJANAAQAOAAAIAJQAJAJAAANQAAAOgJAJQgIAJgOAAQgNAAgJgJg");
	this.shape_58.setTransform(-414.6,160.9);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#FFFFFF").s().p("AhIBqQgbgeAAgrQAAgqAagcQAbgbAjAAQAiAAAWAZIAAhfIA3AAIAAEKIg3AAIAAgaQgWAdgiAAQgiAAgbgdgAgfgCQgOAOAAAXQAAAYAOAPQAOAPASAAQATAAANgPQANgPAAgYQAAgXgNgPQgNgQgTAAQgSABgOAQg");
	this.shape_59.setTransform(-432.4,161.4);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#FFFFFF").s().p("AgmBcQgXgHgTgQIAYgiQAfAYAgAAQALAAAGgFQAGgDAAgIQAAgGgIgGQgJgFgPgFQgQgGgJgDQgIgEgLgHQgVgMAAgbQgBgbAWgRQAVgRAiAAQAiAAAiAXIgUAkQgZgRgZAAQgYAAAAANQAAAIAIAFQAHADASAHIAaAJQAIADAKAGQAVALAAAcQAAAegVARQgVARghAAQgUAAgYgIg");
	this.shape_60.setTransform(-157,121);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#FFFFFF").s().p("AhDBIQgdgbAAgtQAAgtAdgbQAdgbAnAAQAoAAAcAYQAcAYAAAqIAAAdIiMAAQACAPAOAKQAOAKASAAQAcAAASgTIAeAhQggAfgsAAQgsAAgcgcgAgagtQgNAKgDARIBVAAQgCgSgMgJQgKgJgRAAQgOAAgOAJg");
	this.shape_61.setTransform(-177.1,121);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#FFFFFF").s().p("Ag9BIQgegbAAgtQAAgrAegcQAegcApAAQAVAAAWAIQAVAJAQARIgcAlQgJgLgOgHQgNgFgOAAQgUAAgPANQgPAOAAAYQAAAZAPAOQAPANAUAAQAbAAAVgbIAgAkQglAngsAAQgqAAgegcg");
	this.shape_62.setTransform(-198.6,121);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#FFFFFF").s().p("AAkBjIAAhtQABgsghAAQgQgBgLAMQgMAMgBAXIAABrIg2AAIAAjBIA2AAIAAAWQAZgZAdAAQAfgBAVAWQAUAWAAAkIAAB1g");
	this.shape_63.setTransform(-220.7,120.8);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#FFFFFF").s().p("AhEBIQgcgbAAgtQAAgtAdgbQAegbAnAAQAoAAAbAYQAcAYAAAqIAAAdIiMAAQACAPAPAKQAOAKAQAAQAcAAATgTIAfAhQgiAfgsAAQgqAAgegcgAgagtQgOAKgBARIBTAAQgBgSgLgJQgMgJgPAAQgPAAgOAJg");
	this.shape_64.setTransform(-243.3,121);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#FFFFFF").s().p("AgaCIIAAjAIA1AAIAADAgAgWhRQgIgJgBgOQABgNAIgJQAJgJANAAQAOAAAIAJQAJAJAAANQAAAOgJAJQgIAJgOAAQgNAAgJgJg");
	this.shape_65.setTransform(-259.8,117);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#FFFFFF").s().p("Ag3BjIAAjBIA2AAIAAAZQAJgLAQgJQAQgIAQAAIAAAyIgKAAQgYAAgMARQgLASAAAaIAABVg");
	this.shape_66.setTransform(-271.7,120.8);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#FFFFFF").s().p("AhDB0QgdgbAAguQAAgtAdgbQAegbAmAAQAoAAAcAYQAcAZAAApIAAAdIiMAAQACAPAOAKQAOAKASAAQAcAAASgTIAeAhQggAfgsAAQgrAAgdgbgAgagBQgNAIgCARIBTAAQgCgRgKgJQgLgJgQAAQgQAAgNAKgAgghPIAtg/IA3AXIgyAog");
	this.shape_67.setTransform(-290.9,116.6);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#FFFFFF").s().p("AhjCFIAAkGIA2AAIAAAWQAZgZAhAAQAigBAaAdQAbAcAAArQAAAqgbAdQgaAcgkAAQgiAAgWgcIAABfgAgghHQgNAQAAAXQAAAXANAOQANAPATABQARgBAOgPQAOgOAAgXQAAgXgNgQQgOgQgSAAQgTAAgNAQg");
	this.shape_68.setTransform(-313.6,124.3);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#FFFFFF").s().p("AAkBhIglg2IgkA2Ig/AAIBEhiIhAhfIBBAAIAgA0IAig0IA/AAIhCBfIBFBig");
	this.shape_69.setTransform(-336.6,121);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#FFFFFF").s().p("AhDBIQgdgbAAgtQAAgtAdgbQAegbAnAAQAoAAAbAYQAcAYAAAqIAAAdIiMAAQACAPAOAKQAOAKARAAQAcAAATgTIAfAhQgiAfgsAAQgqAAgdgcgAgagtQgOAKgBARIBTAAQgBgSgLgJQgMgJgPAAQgPAAgOAJg");
	this.shape_70.setTransform(-358.2,121);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#FFFFFF").s().p("AgmBcQgXgHgTgQIAYgiQAfAYAgAAQALAAAGgFQAGgDAAgIQAAgGgIgGQgJgFgPgFQgQgGgJgDQgIgEgLgHQgVgMAAgbQAAgbAVgRQAVgRAiAAQAiAAAiAXIgUAkQgZgRgZAAQgYAAAAANQAAAIAIAFQAHADASAHIAaAJQAIADAKAGQAVALAAAcQAAAegVARQgWARggAAQgVAAgXgIg");
	this.shape_71.setTransform(-388.5,121);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#FFFFFF").s().p("AhEBIQgcgbAAgtQAAgtAdgbQAdgbAnAAQAoAAAcAYQAcAYAAAqIAAAdIiMAAQADAPAOAKQAOAKAQAAQAdAAASgTIAeAhQggAfgtAAQgrAAgdgcgAgagtQgOAKgCARIBVAAQgCgSgMgJQgLgJgQAAQgPAAgNAJg");
	this.shape_72.setTransform(-408.6,121);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#FFFFFF").s().p("AhIBqQgbgdAAgsQAAgqAagcQAbgbAjAAQAiAAAWAZIAAhfIA3AAIAAEKIg3AAIAAgaQgWAdgiAAQgiAAgbgdgAgfgDQgOAPAAAXQAAAYAOAPQAOAPASAAQATAAANgPQANgPAAgYQAAgXgNgPQgNgQgTAAQgSAAgOAQg");
	this.shape_73.setTransform(-432.4,117.5);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#FFFFFF").s().p("AglBcQgYgHgTgQIAYghQAeAXAiAAQAKAAAGgEQAGgFAAgHQAAgGgJgGQgIgGgPgEQgQgFgIgEQgJgEgKgHQgXgMAAgbQABgbAVgRQAWgRAhAAQAiAAAiAXIgUAkQgZgRgZAAQgYAAAAAOQAAAHAIAEQAIAFARAFIAaAJQAIADALAHQAUALAAAdQAAAdgVARQgWARgfAAQgWAAgWgIg");
	this.shape_74.setTransform(-242.7,77.1);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#FFFFFF").s().p("AAlBjIAAhtQAAgsghAAQgQAAgLALQgMAMAAAXIAABrIg2AAIAAjBIA2AAIAAAWQAYgaAeAAQAeAAAVAWQAUAWABAkIAAB1g");
	this.shape_75.setTransform(-263.1,76.9);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#FFFFFF").s().p("AhIBHQgdgdAAgqQAAgpAdgdQAdgdArAAQAsAAAdAdQAdAdAAApQAAAqgdAdQgdAdgsAAQgrAAgdgdgAghgmQgOAPAAAXQAAAYAOAPQANAPAUAAQAVAAAOgPQANgPAAgYQAAgXgNgPQgOgPgVAAQgUAAgNAPg");
	this.shape_76.setTransform(-286.4,77.1);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#FFFFFF").s().p("AglBcQgYgHgTgQIAYghQAfAXAhAAQAKAAAGgEQAGgFAAgHQAAgGgJgGQgIgGgPgEQgQgFgJgEQgIgEgKgHQgXgMAAgbQABgbAVgRQAVgRAiAAQAiAAAiAXIgUAkQgZgRgZAAQgYAAAAAOQAAAHAIAEQAIAFARAFIAaAJQAIADALAHQAUALAAAdQAAAdgVARQgVARggAAQgWAAgWgIg");
	this.shape_77.setTransform(-307.7,77.1);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("#FFFFFF").s().p("AgaCIIAAjAIA1AAIAADAgAgVhRQgJgJAAgOQAAgNAJgJQAIgJANAAQAOAAAJAJQAIAJABANQgBAOgIAJQgJAJgOAAQgNAAgIgJg");
	this.shape_78.setTransform(-322,73.1);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("#FFFFFF").s().p("AgaCGIAAkLIA1AAIAAELg");
	this.shape_79.setTransform(-332.5,73.4);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f("#FFFFFF").s().p("AhBBSQgWgRABgfQAAgdAVgOQAWgOAmAAIAmAAIAAgBQAAggggAAQgNAAgQAGQgQAGgLAHIgYgiQAmgcAwAAQAkAAAWARQAXATgBAmIAAB6IgyAAIAAgXQgVAageAAQgfAAgUgSgAggAiQAAAKAHAGQAIAFAPABQANgBAMgIQAKgJAAgPIAAgJIghAAQggAAAAAUg");
	this.shape_80.setTransform(-348.9,77.1);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("#FFFFFF").s().p("AhEB0QgcgbAAguQAAgtAdgbQAegbAnAAQAoAAAbAYQAcAZAAApIAAAdIiMAAQACAPAPAKQAOAKAQAAQAcAAATgTIAfAhQgiAfgsAAQgrAAgdgbgAgagBQgOAIgBARIBTAAQgBgRgLgJQgMgJgPAAQgPAAgOAKgAgghPIAsg/IA4AXIgyAog");
	this.shape_81.setTransform(-370.2,72.7);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f("#FFFFFF").s().p("Ag3BjIAAjBIA2AAIAAAZQAJgMAQgIQAQgIAQgBIAAA0IgKAAQgYgBgMASQgLAQAAAcIAABUg");
	this.shape_82.setTransform(-388.1,76.9);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("#FFFFFF").s().p("AgcBtQgUgTAAgfIAAhZIgWAAIAAgmIAWAAIAAg6IA2AAIAAA6IAvAAIAAAmIgvAAIAABWQAAALAGAHQAGAGAHAAQAPAAALgOIAUAmQgZAXgcAAQgbAAgTgSg");
	this.shape_83.setTransform(-413.2,74.3);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f("#FFFFFF").s().p("AhDBJQgdgcAAgtQAAgtAdgbQAegbAmAAQAoAAAcAYQAcAYAAArIAAAcIiMAAQACAPAOAKQAOAKASAAQAcAAASgTIAeAhQggAfgsAAQgrAAgdgbgAgagsQgNAJgCARIBTAAQgCgRgKgKQgLgJgQAAQgQAAgNAKg");
	this.shape_84.setTransform(-432.6,77.1);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f("#FFFFFF").s().p("AglBcQgYgIgTgPIAYghQAfAXAgAAQALAAAGgEQAGgEAAgHQAAgHgIgGQgJgGgPgFQgQgFgJgEQgIgDgLgHQgWgNABgaQAAgbAVgRQAVgRAiAAQAiAAAiAXIgUAkQgZgRgZAAQgYAAAAAOQAAAHAIAEQAHAFASAFIAaAJQAIAEALAGQAUALAAAdQAAAdgVAQQgVASggAAQgVAAgXgIg");
	this.shape_85.setTransform(-152.8,33.2);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f("#FFFFFF").s().p("AAkBiIAAhsQAAgtggAAQgPAAgNAMQgMAMABAWIAABrIg2AAIAAjAIA2AAIAAAVQAYgZAdAAQAfABAVAVQAVAWgBAjIAAB1g");
	this.shape_86.setTransform(-173.3,33);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f("#FFFFFF").s().p("AhIBHQgdgdAAgqQAAgpAdgdQAdgdArAAQAsAAAdAdQAdAdAAApQAAAqgdAdQgdAdgsAAQgrAAgdgdgAghglQgOAPAAAWQAAAYAOAPQANAPAUAAQAVAAAOgPQANgPAAgYQAAgWgNgPQgOgQgVAAQgUAAgNAQg");
	this.shape_87.setTransform(-196.6,33.2);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f("#FFFFFF").s().p("AgbBhIhMjBIA4AAIAvB5IAxh5IA3AAIhNDBg");
	this.shape_88.setTransform(-218.8,33.2);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f("#FFFFFF").s().p("AhDBJQgdgbAAguQAAgtAdgbQAegbAnAAQAoAAAbAYQAcAZAAAqIAAAbIiMAAQACAQAOAKQAPAKARAAQAbAAATgTIAeAhQghAfgrAAQgrAAgdgbgAgagsQgNAJgCARIBTAAQgCgRgKgKQgLgJgQAAQgQAAgNAKg");
	this.shape_89.setTransform(-240.4,33.2);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f("#FFFFFF").s().p("Ag9BJQgegcAAgsQAAgsAegcQAegcApAAQAVAAAWAJQAVAIAQARIgcAlQgJgLgOgGQgNgHgOAAQgUABgPAOQgPAOAAAXQAAAYAPAOQAPAOAUAAQAbAAAVgbIAgAkQglAngsAAQgqAAgegbg");
	this.shape_90.setTransform(-261.9,33.2);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f("#FFFFFF").s().p("AAlBiIAAhsQAAgtghAAQgPAAgMAMQgNAMAAAWIAABrIg2AAIAAjAIA2AAIAAAVQAZgZAeAAQAeABAVAVQAUAWABAjIAAB1g");
	this.shape_91.setTransform(-283.9,33);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f("#FFFFFF").s().p("AhIBHQgdgdAAgqQAAgpAdgdQAdgdArAAQAsAAAdAdQAdAdAAApQAAAqgdAdQgdAdgsAAQgrAAgdgdgAghglQgOAPAAAWQAAAYAOAPQANAPAUAAQAVAAAOgPQANgPAAgYQAAgWgNgPQgOgQgVAAQgUAAgNAQg");
	this.shape_92.setTransform(-307.2,33.2);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.f("#FFFFFF").s().p("Ag9BJQgegcAAgsQAAgsAegcQAegcApAAQAVAAAWAJQAVAIAQARIgcAlQgJgLgOgGQgNgHgOAAQgUABgPAOQgPAOAAAXQAAAYAPAOQAPAOAUAAQAbAAAVgbIAgAkQglAngsAAQgqAAgegbg");
	this.shape_93.setTransform(-329.2,33.2);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.f("#FFFFFF").s().p("AgmBcQgXgIgTgPIAYghQAfAXAgAAQALAAAGgEQAGgEAAgHQAAgHgJgGQgIgGgPgFQgQgFgIgEQgJgDgLgHQgVgNgBgaQAAgbAWgRQAVgRAiAAQAiAAAiAXIgUAkQgZgRgZAAQgYAAAAAOQAAAHAIAEQAIAFARAFIAaAJQAIAEAKAGQAVALAAAdQAAAdgVAQQgVASghAAQgUAAgYgIg");
	this.shape_94.setTransform(-359,33.2);

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.f("#FFFFFF").s().p("AhEBOQgWgUAAgnIAAh0IA2AAIAABoQAAAtAhAAQAPAAAMgMQAMgMAAgWIAAhnIA3AAIAADAIg3AAIAAgZQgUAcgfAAQggAAgVgUg");
	this.shape_95.setTransform(-379.7,33.3);

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.f("#FFFFFF").s().p("AhIBHQgdgdAAgqQAAgpAdgdQAdgdArAAQAsAAAdAdQAdAdAAApQAAAqgdAdQgdAdgsAAQgrAAgdgdgAghglQgOAPAAAWQAAAYAOAPQANAPAUAAQAVAAAOgPQANgPAAgYQAAgWgNgPQgOgQgVAAQgUAAgNAQg");
	this.shape_96.setTransform(-402.8,33.2);

	this.shape_97 = new cjs.Shape();
	this.shape_97.graphics.f("#FFFFFF").s().p("AA8B+Ih3idIAACdIg4AAIAAj7IA0AAIB7ChIAAihIA4AAIAAD7g");
	this.shape_97.setTransform(-429.1,30.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_97},{t:this.shape_96},{t:this.shape_95},{t:this.shape_94},{t:this.shape_93},{t:this.shape_92},{t:this.shape_91},{t:this.shape_90},{t:this.shape_89},{t:this.shape_88},{t:this.shape_87},{t:this.shape_86},{t:this.shape_85},{t:this.shape_84},{t:this.shape_83},{t:this.shape_82},{t:this.shape_81},{t:this.shape_80},{t:this.shape_79},{t:this.shape_78},{t:this.shape_77},{t:this.shape_76},{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20}]},94).to({state:[]},44).wait(22));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-345,126.2,177.8,116);


// stage content:
(lib.FS_projet_intro_mobile = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":231});

	// timeline functions:
	this.frame_99 = function() {
		//var frequency = 60;
		//stage.enableMouseOver(frequency);
		
		// EVENT DE CLIC POUR LA SUITE
		this.next_btn.addEventListener("click", fl_ClickToGoToAndPlayFromFrame.bind(this));
		this.next_btn.addEventListener("mouseover", fl_MouseOverHandler.bind(this));
		this.next_btn.addEventListener("mouseout", fl_MouseOutHandler.bind(this));
		
		
		function fl_ClickToGoToAndPlayFromFrame()
		{
			//this.gotoAndPlay("close");
			
			var event = new Event('next');
			this.dispatchEvent(event);		
			event = null;
			
			this.next_btn.btnNext.gotoAndPlay('rollOut');
			
		}
		function fl_MouseOverHandler(){	this.next_btn.btnNext.gotoAndPlay('rollOver'); }
		function fl_MouseOutHandler(){	this.next_btn.btnNext.gotoAndPlay('rollOut'); }
	}
	this.frame_230 = function() {
		//PAUSE
		this.stop();
	}
	this.frame_232 = function() {
		this.next_btn.gotoAndPlay("close");
		this.textes_mc.gotoAndPlay("close");
		//this.lignes_mc.gotoAndPlay("close_lines");
	}
	this.frame_251 = function() {
		this.lignes_mc.gotoAndPlay("close");
	}
	this.frame_280 = function() {
		// EVENT DE FIN D'ANIM
		var event = new Event('closed');
		this.dispatchEvent(event);		
		event = null;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(99).call(this.frame_99).wait(131).call(this.frame_230).wait(2).call(this.frame_232).wait(19).call(this.frame_251).wait(29).call(this.frame_280).wait(1));

	// CTA NEXT
	this.next_btn = new lib.bloc_NEXT_btn();
	this.next_btn.name = "next_btn";
	this.next_btn.parent = this;
	this.next_btn.setTransform(359.1,861.2,1.06,1.06,0,0,0,-340.4,403.9);
	this.next_btn._off = true;

	this.timeline.addTween(cjs.Tween.get(this.next_btn).wait(89).to({_off:false},0).to({_off:true},191).wait(1));

	// BLOC INFOS TEXTE
	this.textes_mc = new lib.blocInfostextes();
	this.textes_mc.name = "textes_mc";
	this.textes_mc.parent = this;
	this.textes_mc.setTransform(732.1,132,1,1,0,0,0,222.1,-164);
	this.textes_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.textes_mc).wait(17).to({_off:false},0).to({_off:true},263).wait(1));

	// lignes
	this.lignes_mc = new lib.bloc_lignes();
	this.lignes_mc.name = "lignes_mc";
	this.lignes_mc.parent = this;
	this.lignes_mc.setTransform(240.8,453.9,0.525,1,0,0,0,0.7,405.9);

	this.timeline.addTween(cjs.Tween.get(this.lignes_mc).to({_off:true},280).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;
// library properties:
lib.properties = {
	id: '9A88C8A9F23C274EBAFB53F563A7F48D',
	width: 480,
	height: 840,
	fps: 60,
	color: "#000000",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['9A88C8A9F23C274EBAFB53F563A7F48D'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;