(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.casquetteHeadict = function() {
	this.initialize(img.casquetteHeadict);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,844,450);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.video = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#080808").s().p("EgtTBLAMAAAiV/MBanAAAMAAACV/g");
	this.shape.setTransform(290,480);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.video, new cjs.Rectangle(0,0,580,960), null);


(lib.txt_hashtag = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgiAmIAJgRQAOAKALAAQAFgBAEgDQAFgDAAgGQAAgGgFgEQgFgDgHAAQgFAAgJACIAAgPIASgVIgdAAIAAgTIA7AAIAAAPIgVAXQAMABAGAIQAHAIAAAKQAAAPgKAJQgLAJgPAAQgRAAgQgMg");
	this.shape.setTransform(95.6,52.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AghAKIAAgTIBDggIAAAVIgsAUIAsAVIAAAVg");
	this.shape_1.setTransform(87.1,52.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AAVAxIgVggIgUAgIgaAAIAhgxIgfgwIAaAAIASAdIASgdIAbAAIgfAvIAhAyg");
	this.shape_2.setTransform(70.9,52.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgjAkQgPgPAAgVQAAgUAPgPQAOgPAVAAQAWAAAPAPQAOAPAAAUQAAAVgOAPQgPAPgWAAQgVAAgOgPgAgTgVQgJAJAAAMQAAANAJAKQAIAIALABQAMgBAJgIQAIgKAAgNQAAgMgIgJQgJgJgMAAQgLAAgIAJg");
	this.shape_3.setTransform(60.2,52.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgmAxIAAhhIAmAAQAJAAAIACQAGADAEAEQAHAHAAAKQgBALgGAGIgEACIgEACQAKABAFAGQAFAHAAAJQAAAKgHAIQgIAJgUAAgAgQAeIAPAAQAIAAAFgCQAEgCABgHQAAgHgGgCQgEgCgKAAIgNAAgAgQgIIAKAAQAIAAAFgCQAEgCAAgHQAAgGgEgCQgEgCgJgBIgKAAg");
	this.shape_4.setTransform(49.9,52.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBGAAIAAATIgwAAIAAAVIAqAAIAAASIgqAAIAAAUIAxAAIAAATg");
	this.shape_5.setTransform(40.5,52.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAUAxIAAgmIgmAAIAAAmIgWAAIAAhhIAWAAIAAApIAmAAIAAgpIAVAAIAABhg");
	this.shape_6.setTransform(30.3,52.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgKAxIAAhOIgbAAIAAgTIBLAAIAAATIgbAAIAABOg");
	this.shape_7.setTransform(20.8,52.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AAXAxIgtg9IAAA9IgWAAIAAhhIAUAAIAvA+IAAg+IAWAAIAABhg");
	this.shape_8.setTransform(10.8,52.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgKAxIAAhhIAVAAIAABhg");
	this.shape_9.setTransform(2.8,52.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgKAxIAAgmIgig7IAYAAIAUAkIAVgkIAYAAIgiA7IAAAmg");
	this.shape_10.setTransform(-7.6,52.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgmAxIAAhhIAmAAQAJAAAIACQAGADAEAEQAHAHgBAKQAAALgGAGIgEACIgDACQAJABAFAGQAFAHAAAJQAAAKgHAIQgIAJgUAAgAgQAeIAPAAQAIAAAFgCQAFgCAAgHQgBgHgFgCQgEgCgKAAIgNAAgAgQgIIAKAAQAJAAAEgCQAEgCAAgHQAAgGgEgCQgEgCgJgBIgKAAg");
	this.shape_11.setTransform(-16.4,52.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgsAxIAAhhIAjAAQAaAAAOANQAOAMgBAXQABAWgOAOQgOANgbAAgAgVAeIANAAQAOgBAJgHQAHgIABgOQgBgOgHgHQgJgIgQAAIgLAAg");
	this.shape_12.setTransform(-30.3,52.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgiAmIAJgRQAOAKALAAQAFgBAEgDQAFgDAAgGQAAgGgFgEQgFgDgHAAQgFAAgJACIAAgPIASgVIgeAAIAAgTIA8AAIAAAPIgVAXQAMABAGAIQAHAIAAAKQAAAPgLAJQgKAJgPAAQgRAAgQgMg");
	this.shape_13.setTransform(-40.2,52.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgTIANAAIADgRIgPAAIADgUIAPAAIAEgWIAUAAIgDAWIAMAAIADgWIAWAAIgEAWIAOAAIgDAUIgPAAIgDARIARAAIgEATIgQAAIgDAVgAgHAKIALAAIAEgRIgMAAg");
	this.shape_14.setTransform(-49.5,52.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBGAAIAAATIgwAAIAAAVIAqAAIAAASIgqAAIAAATIAxAAIAAAUg");
	this.shape_15.setTransform(54.7,31.7);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgeAkQgPgOAAgWQAAgUAPgPQAPgOAVAAQAYgBAQATIgNAPQgLgMgPAAQgMgBgIAJQgJAHAAANQgBANAJAJQAIAIALAAQAQAAALgNIANAPQgQATgXAAQgVgBgPgOg");
	this.shape_16.setTransform(44.8,31.7);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AAQAxIgVgfIgPAAIAAAfIgWAAIAAhhIAlAAQAWAAAKAIQAKAHAAASQAAAWgTAHIAZAjgAgUAAIAQAAQAKAAAEgEQAEgEAAgHQAAgIgEgDQgEgDgKAAIgQAAg");
	this.shape_17.setTransform(34.9,31.7);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBGAAIAAATIgwAAIAAAVIAqAAIAAASIgqAAIAAATIAxAAIAAAUg");
	this.shape_18.setTransform(25.1,31.7);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AAhAxIAAg9IgbA1IgMAAIgag1IAAA9IgWAAIAAhhIAeAAIAYA1IAZg1IAeAAIAABhg");
	this.shape_19.setTransform(13.6,31.7);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AAhAxIAAg9IgbA1IgMAAIgag1IAAA9IgWAAIAAhhIAeAAIAYA1IAZg1IAeAAIAABhg");
	this.shape_20.setTransform(0.2,31.7);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgjAkQgPgPAAgVQAAgVAPgOQAOgOAVAAQAWAAAPAOQAOAOAAAVQAAAVgOAPQgPAPgWAAQgVAAgOgPgAgTgVQgJAJAAAMQAAANAJAKQAIAIALABQAMgBAJgIQAIgKAAgNQAAgMgIgJQgJgJgMAAQgLAAgIAJg");
	this.shape_21.setTransform(-12.2,31.7);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgeAkQgPgOAAgWQAAgUAPgPQAPgOAUAAQAZgBAQATIgOAPQgKgMgQAAQgLgBgIAJQgKAHABANQAAANAIAJQAJAIAKAAQAQAAALgNIANAPQgRATgWAAQgVgBgPgOg");
	this.shape_22.setTransform(-23,31.7);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgYAKIAAgTIAxAAIAAATg");
	this.shape_23.setTransform(-31.7,32.4);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBFAAIAAATIgvAAIAAAVIAqAAIAAASIgqAAIAAATIAxAAIAAAUg");
	this.shape_24.setTransform(-39.4,31.7);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgTIANAAIADgRIgPAAIADgTIAPAAIAEgXIAUAAIgDAXIAMAAIADgXIAWAAIgEAXIAOAAIgDATIgPAAIgDARIARAAIgEATIgQAAIgDAVgAgHAKIALAAIAEgRIgMAAg");
	this.shape_25.setTransform(-49.5,31.6);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgfAxIAAhhIAWAAIAABNIApAAIAAAUg");
	this.shape_26.setTransform(3.7,10.7);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgeAkQgOgOAAgWQgBgUAPgPQAPgOAUAAQAWAAAPANIgMARQgGgGgFgDQgGgBgGAAQgMAAgJAIQgIAIAAANQAAAOAIAJQAJAIAJAAQAMAAAHgFIAAgaIAXAAIAAAiQgPAQgaABQgUgBgPgOg");
	this.shape_27.setTransform(-6,10.6);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AgmAxIAAhhIAmAAQAJAAAHADQAIACADAEQAGAHAAAKQABALgIAGIgDACIgDACQAIABAGAHQAFAFAAAKQAAAKgHAIQgIAJgUAAgAgQAeIAPAAQAIABAFgDQAFgCgBgHQAAgGgEgDQgGgCgJAAIgNAAgAgQgIIAKAAQAIAAAFgCQAEgCAAgGQAAgHgEgCQgEgDgIAAIgLAAg");
	this.shape_28.setTransform(-15.7,10.7);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBFAAIAAAUIgvAAIAAAUIArAAIAAASIgrAAIAAATIAxAAIAAAUg");
	this.shape_29.setTransform(-25.2,10.7);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AAVAxIgVhDIgUBDIgQAAIgjhhIAYAAIATA4IASg4IAVAAIASA4IATg4IAYAAIgiBhg");
	this.shape_30.setTransform(-37.1,10.7);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgTIANAAIADgRIgPAAIADgTIAPAAIAEgXIAUAAIgDAXIAMAAIADgXIAWAAIgEAXIAOAAIgDATIgPAAIgDARIARAAIgEATIgQAAIgDAVgAgHAKIALAAIAEgRIgMAAg");
	this.shape_31.setTransform(-49.5,10.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.txt_hashtag, new cjs.Rectangle(-56.7,0,162.4,63.2), null);


(lib.project_illustration_placeHolder = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.casquetteHeadict();
	this.instance.parent = this;
	this.instance.setTransform(-115.6,-65.1,0.64,0.64,50.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.project_illustration_placeHolder, new cjs.Rectangle(-336.6,-65.1,567,599), null);


(lib.playArrow = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(2,1,1).p("AiOiMIAAEZIEdiIg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AiOiMIEdCRIkdCIg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.playArrow, new cjs.Rectangle(-15.3,-15.1,30.7,30.2), null);


(lib.masqueTexte_1_ligne = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("EgicAEJIAAoRMBE5AAAIAAIRg");
	this.shape.setTransform(0,24.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.masqueTexte_1_ligne, new cjs.Rectangle(-220.5,-1.9,441,53), null);


(lib.masque_generique = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF8D72").s().p("AsQCqIAAlTIYhAAIAAFTg");
	this.shape.setTransform(78.5,17);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.masque_generique, new cjs.Rectangle(0,0,157,34), null);


(lib.line_explode = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_15 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(15).call(this.frame_15).wait(1));

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(1,1,1).p("AgHAAIAPAA");
	this.shape.setTransform(0.8,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#FFFFFF").ss(1,1,1).p("AgRAAIAjAA");
	this.shape_1.setTransform(2.9,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#FFFFFF").ss(1,1,1).p("AgaAAIA2AA");
	this.shape_2.setTransform(5.1,0);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#FFFFFF").ss(1,1,1).p("AglAAIBLAA");
	this.shape_3.setTransform(7.3,0);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#FFFFFF").ss(1,1,1).p("AgnAAIBPAA");
	this.shape_4.setTransform(8.8,0);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#FFFFFF").ss(1,1,1).p("AgpAAIBTAA");
	this.shape_5.setTransform(10.4,0);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#FFFFFF").ss(1,1,1).p("AgrAAIBXAA");
	this.shape_6.setTransform(12,0);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#FFFFFF").ss(1,1,1).p("AgtAAIBbAA");
	this.shape_7.setTransform(13.6,0);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#FFFFFF").ss(1,1,1).p("AgvAAIBfAA");
	this.shape_8.setTransform(15.1,0);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#FFFFFF").ss(1,1,1).p("AgxAAIBjAA");
	this.shape_9.setTransform(16.7,0);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#FFFFFF").ss(1,1,1).p("Ag0AAIBpAA");
	this.shape_10.setTransform(18.3,0);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#FFFFFF").ss(1,1,1).p("AgWAAIAtAA");
	this.shape_11.setTransform(25.8,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape,p:{x:0.8}}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3,p:{x:7.3}}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_3,p:{x:22}}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape,p:{x:29.5}}]},1).to({state:[]},1).to({state:[]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1,-1,13,2);


(lib.ligne = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(49));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#333333").ss(2,1,1).p("EgABhM/MAADCZ/");
	this.shape.setTransform(-0.2,319.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#333333").ss(2,1,1).p("EAAghM7Qg1OzgbRBQgbUYAEVMQADWBAkVVQAlTkBERl");
	this.shape_1.setTransform(-7,319.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#333333").ss(2,1,1).p("EAA7hM5QhfN/gxR5QgxUHAHVhQAGWSA/VCQBDUPB4Qw");
	this.shape_2.setTransform(-12.4,319.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#333333").ss(2,1,1).p("EABQhM3Qh/NXhDSkQhAT5AIVyQAIWfBUU0QBZUwChQG");
	this.shape_3.setTransform(-16.5,320.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#333333").ss(2,1,1).p("EABfhM1QiXM4hPTEQhMTvAKV+QAJWpBjUqQBqVIC+Pn");
	this.shape_4.setTransform(-19.6,320.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#333333").ss(2,1,1).p("EABqhM0QioMjhYTaQhVToALWHQALWwBuUiQB1VaDUPR");
	this.shape_5.setTransform(-21.8,320.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#333333").ss(2,1,1).p("EABxhMzQizMUheTqQhbTjAMWNQALW1B2UdQB9VmDjPB");
	this.shape_6.setTransform(-23.3,320.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#333333").ss(2,1,1).p("EAB2hMzQi7MKhiT1QhfTgAMWRQAMW3B7UaQCDVvDsO3");
	this.shape_7.setTransform(-24.2,320.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#333333").ss(2,1,1).p("EAB5hMyQjAMEhkT7QhhTeAMWTQAMW5B+UYQCGVzDyOx");
	this.shape_8.setTransform(-24.8,320.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#333333").ss(2,1,1).p("EAB7hMyQjCMBhmT+QhjTdANWVQAMW6CAUWQCHV2D2Ou");
	this.shape_9.setTransform(-25.2,320.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#333333").ss(2,1,1).p("EAB8hMyQjEL/hmUAQhjTcAMWWQANW7CAUWQCIV3D3Os");
	this.shape_10.setTransform(-25.3,320.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#333333").ss(2,1,1).p("EAB8hMyQjEL/hnUAQhjTcANWWQAMW7CAUWQCJV3D4Os");
	this.shape_11.setTransform(-25.4,320.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#333333").ss(2,1,1).p("EAB8hMyQjEL/hnUAQhjTcAMWWQANW7CAUWQCJV3D4Os");
	this.shape_12.setTransform(-25.4,320.5);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#333333").ss(2,1,1).p("EAB8hMyQjEL/hnUAQhjTcAMWWQANW7CAUWQCJV4D4Or");
	this.shape_13.setTransform(-25.4,320.5);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#333333").ss(2,1,1).p("EAB8hMyQjEL/hmUAQhkTcANWWQAMW7CBUWQCIV3D4Os");
	this.shape_14.setTransform(-25.4,320.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#333333").ss(2,1,1).p("EAB7hMyQjDMAhmT/QhjTdANWVQAMW6CAUWQCIV3D2Ot");
	this.shape_15.setTransform(-25.2,320.5);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#333333").ss(2,1,1).p("EAB6hMyQjBMChmT9QhiTdAMWVQANW6B/UXQCHV1D1Ou");
	this.shape_16.setTransform(-25.1,320.5);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#333333").ss(2,1,1).p("EAB5hMyQjAMEhkT7QhiTeANWTQAMW5B+UYQCGV0DzOw");
	this.shape_17.setTransform(-24.9,320.5);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#333333").ss(2,1,1).p("EAB4hMzQi+MHhjT5QhgTfAMWRQAMW5B8UZQCFVxDvO0");
	this.shape_18.setTransform(-24.6,320.5);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#333333").ss(2,1,1).p("EAB2hMzQi7MLhhT0QhfTgAMWRQAMW3B6UaQCDVtDrO5");
	this.shape_19.setTransform(-24.2,320.5);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#333333").ss(2,1,1).p("EABzhMzQi2MQhgTuQhcTiALWPQAMW1B4UcQB/VqDnO9");
	this.shape_20.setTransform(-23.7,320.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#333333").ss(2,1,1).p("EABwhMzQiyMWhcToQhbTkAMWMQALWzB1UeQB8VlDgPD");
	this.shape_21.setTransform(-23,320.4);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#333333").ss(2,1,1).p("EABshM0QisMehZTgQhXTmALWJQALWxBwUhQB4VeDZPM");
	this.shape_22.setTransform(-22.2,320.4);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#333333").ss(2,1,1).p("EABohM0QilMnhVTWQhUTpALWFQAKWuBsUlQBzVWDQPV");
	this.shape_23.setTransform(-21.3,320.3);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#333333").ss(2,1,1).p("EABihM1QibMyhSTKQhPTuAKWAQAKWrBnUoQBsVNDFPh");
	this.shape_24.setTransform(-20.2,320.3);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#333333").ss(2,1,1).p("EABchM1QiSM/hMS8QhKTyAKV7QAJWnBgUsQBmVDC4Pt");
	this.shape_25.setTransform(-18.9,320.2);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#333333").ss(2,1,1).p("EABUhM2QiGNNhGStQhET3AJV1QAJWiBYUyQBeU3CpP8");
	this.shape_26.setTransform(-17.4,320.1);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#333333").ss(2,1,1).p("EABMhM3Qh5Neg/ScQg9T8AIVuQAHWdBQU2QBVUqCYQO");
	this.shape_27.setTransform(-15.7,320);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#333333").ss(2,1,1).p("EABChM4QhqNxg3SIQg2UCAHVnQAHWWBGU9QBKUbCGQh");
	this.shape_28.setTransform(-13.8,319.9);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#333333").ss(2,1,1).p("EAA3hM5QhZOGguRxQguUKAGVeQAGWPA7VFQA/UJBxQ3");
	this.shape_29.setTransform(-11.7,319.8);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#333333").ss(2,1,1).p("EAAuhM6QhKOZgnRdQgmUQAFVWQAGWKAxVLQA0T6BfRK");
	this.shape_30.setTransform(-9.8,319.7);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#333333").ss(2,1,1).p("EAAlhM7Qg8OqggRLQgfUWAEVPQAEWFApVQQArTtBORb");
	this.shape_31.setTransform(-8.1,319.7);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#333333").ss(2,1,1).p("EAAehM7QgxO4gaQ8QgZUaADVJQAEWAAhVWQAjTgA/Rq");
	this.shape_32.setTransform(-6.6,319.6);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#333333").ss(2,1,1).p("EAAXhM8QgnPFgUQuQgUUeADVFQADV8AaVaQAcTWAzR3");
	this.shape_33.setTransform(-5.3,319.5);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#333333").ss(2,1,1).p("EAAShM9QgePQgRQjQgPUiACVAQADV4AVVeQAVTNAoSD");
	this.shape_34.setTransform(-4.2,319.5);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#333333").ss(2,1,1).p("EAANhM9QgXPZgMQZQgMUlACU9QACV1AQVhQARTFAeSM");
	this.shape_35.setTransform(-3.2,319.4);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#333333").ss(2,1,1).p("EAAJhM9QgRPggJQRQgJUoACU5QABVyANVkQAMS/AXSU");
	this.shape_36.setTransform(-2.4,319.4);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#333333").ss(2,1,1).p("EAAGhM+QgMPogGQKQgHUpACU3QABVwAJVmQAJS6ARSb");
	this.shape_37.setTransform(-1.8,319.3);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#333333").ss(2,1,1).p("EAADhM+QgIPsgEQFQgEUrABU1QABVuAGVoQAGS2AMSg");
	this.shape_38.setTransform(-1.3,319.3);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#333333").ss(2,1,1).p("EAABhM+QgFPwgCQAQgDUtABUzQABVtAEVqQAESyAISk");
	this.shape_39.setTransform(-0.9,319.3);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#333333").ss(2,1,1).p("EAAAhM+QgCPzgCP9QgBUuAAUxQABVtADVqQACSwAFSn");
	this.shape_40.setTransform(-0.6,319.3);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#333333").ss(2,1,1).p("EAAAhM+QgCP1gBP7QAAUuAAUxQABVsACVrQABSuADSp");
	this.shape_41.setTransform(-0.4,319.3);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#333333").ss(2,1,1).p("EgABhM/QgBP3AAP5QAAUvABUxQAAVsABVrQABStACSr");
	this.shape_42.setTransform(-0.2,319.3);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#333333").ss(2,1,1).p("EgABhM/QAAP4AAP4QAAUwABUvQAAVsAAVrQABStABSs");
	this.shape_43.setTransform(-0.2,319.3);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#333333").ss(2,1,1).p("EgABhM/QABP4AAP4QAAUwAAUvQAAVsABVrQABStAASs");
	this.shape_44.setTransform(-0.2,319.3);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#333333").ss(2,1,1).p("EgABhM/QABP4AAP4QAAUwAAUvQABVsAAVrQABStAASs");
	this.shape_45.setTransform(-0.2,319.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.3,-174.5,2.4,987.5);


(lib.lettres_FAT = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF8D72").s().p("ADUFeIhCiXIkjAAIhBCXIinAAIEvq6ICVAAIEuK6gAhXA+ICvAAIhYjLg");
	this.shape.setTransform(13.1,311.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF8D72").s().p("AkBFeIAAq7IH3AAIAACMIlaAAIAACQIE3AAIAACDIk3AAIAACSIFlAAIAACKg");
	this.shape_1.setTransform(10.2,187.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF8D72").s().p("ACLFdIAAkSIkVAAIAAESIicAAIAAq5ICcAAIAAEjIEVAAIAAkjICcAAIAAK5g");
	this.shape_2.setTransform(-62.8,63.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.lettres_FAT, new cjs.Rectangle(-102.9,0,167.5,373.7), null);


(lib.hastaggraphicbranche = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AjQAuIAShbIDlAAICqAAIgSBbg");
	this.shape.setTransform(0,4.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.hastaggraphicbranche, new cjs.Rectangle(-20.8,0,41.7,9.2), null);


(lib.Btn_cliquable_area = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF8D72").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape.setTransform(0,30);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Btn_cliquable_area, new cjs.Rectangle(-113,0,226,60), null);


(lib.prject_illustration = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.project_illustration_placeHolder();
	this.instance.parent = this;
	this.instance.setTransform(0,316.5,1,1,0,0,0,0,316.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:-0.1,regY:316.6,rotation:-1.5,x:-9.6,y:335.9},59,cjs.Ease.quadInOut).to({regX:0,regY:316.5,rotation:0,x:0,y:316.5},60,cjs.Ease.quadInOut).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-336.6,-65.1,567,599);


(lib.masqueTexte = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// masqueTexte_1_ligne
	this.instance = new lib.masqueTexte_1_ligne();
	this.instance.parent = this;
	this.instance.setTransform(-218.8,191.5,0.008,1,0,0,0,0,25.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(6).to({_off:false},0).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.01,x:218.8},14).wait(1));

	// masqueTexte_1_ligne
	this.instance_1 = new lib.masqueTexte_1_ligne();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-218.8,136.1,0.008,1,0,0,0,0,25.5);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.01,x:218.8},14).to({_off:true},1).wait(2));

	// masqueTexte_1_ligne
	this.instance_2 = new lib.masqueTexte_1_ligne();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-218.8,80.8,0.008,1,0,0,0,0,25.5);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(2).to({_off:false},0).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.01,x:218.8},14).to({_off:true},1).wait(4));

	// masqueTexte_1_ligne
	this.instance_3 = new lib.masqueTexte_1_ligne();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-218.8,25.5,0.008,1,0,0,0,0,25.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.01,x:218.8},14).to({_off:true},1).wait(6));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-220.5,-1.9,3.5,53);


(lib.hashtag_graphic = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(64));

	// hastag graphic branche
	this.instance = new lib.hastaggraphicbranche();
	this.instance.parent = this;
	this.instance.setTransform(37.4,51.9,1.084,0.995,0,-78.3,102.1,0.1,4.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({x:6.5,y:50.1},17,cjs.Ease.quartOut).to({_off:true},5).wait(3).to({_off:false},0).to({_off:true},4).wait(2).to({_off:false,x:26.5},0).to({_off:true},5).wait(28));

	// hastag graphic branche
	this.instance_1 = new lib.hastaggraphicbranche();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-41.6,49.9,1.084,0.995,0,-78.3,102.1,0.1,4.6);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off:false},0).to({x:-7.4,y:49.8},18,cjs.Ease.quartOut).to({_off:true},4).wait(5).to({_off:false},0).to({_off:true},4).wait(31));

	// hastag graphic branche
	this.instance_2 = new lib.hastaggraphicbranche();
	this.instance_2.parent = this;
	this.instance_2.setTransform(2.7,21.7,1,1,0,0,0,0,4.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({x:-2.3,y:58.7},13,cjs.Ease.quartOut).to({_off:true},11).wait(3).to({_off:false,scaleX:2.33,scaleY:1.03,skewX:-12.8,x:25.5},0).to({_off:true},4).wait(33));

	// hastag graphic branche
	this.instance_3 = new lib.hastaggraphicbranche();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-7.3,77.7,1,1,0,0,0,0,4.6);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({x:0.7,y:41.7},14,cjs.Ease.quartOut).to({_off:true},4).wait(7).to({_off:false,regX:0.1,scaleX:1.98,scaleY:1.01,skewX:-9.8,x:19.9,y:49.7},0).to({_off:true},4).wait(4).to({_off:false,regX:0,scaleX:1,scaleY:1,skewX:0,x:0.7,y:41.7},0).to({_off:true},4).wait(21));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-18.1,17.1,64.6,57.7);


(lib.btn_playVideo = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{open:1,close:97});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_96 = function() {
		this.stop();
	}
	this.frame_135 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(96).call(this.frame_96).wait(39).call(this.frame_135).wait(1));

	// ZONE
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.004)").s().p("Am7G8IAAt3IN3AAIAAN3g");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(136));

	// Calque_16
	this.instance = new lib.line_explode();
	this.instance.parent = this;
	this.instance.setTransform(2.7,0,1,1,120);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(127).to({_off:false},0).wait(9));

	// Calque_15
	this.instance_1 = new lib.line_explode();
	this.instance_1.parent = this;
	this.instance_1.setTransform(2.8,0,0.778,1,-155.3);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(125).to({_off:false},0).wait(11));

	// Calque_14
	this.instance_2 = new lib.line_explode();
	this.instance_2.parent = this;
	this.instance_2.setTransform(2.8,0.1,1,1,-80.3);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(126).to({_off:false},0).wait(10));

	// Calque_13
	this.instance_3 = new lib.line_explode();
	this.instance_3.parent = this;
	this.instance_3.setTransform(2.9,0.1,0.807,1,69.7,0,0,0.1,-0.1);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(124).to({_off:false},0).wait(12));

	// Calque_12
	this.instance_4 = new lib.line_explode();
	this.instance_4.parent = this;
	this.instance_4.setTransform(2.8,0);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(123).to({_off:false},0).wait(13));

	// Calque_5
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgGADIANgF");
	this.shape_1.setTransform(-10.5,13.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgVALIArgU");
	this.shape_2.setTransform(-8.9,12.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgkASIBJgj");
	this.shape_3.setTransform(-7.4,12);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgzAZIBngx");
	this.shape_4.setTransform(-5.9,11.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhDAgICHg/");
	this.shape_5.setTransform(-4.4,10.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhSAnIClhN");
	this.shape_6.setTransform(-2.9,9.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhhAvIDDhc");
	this.shape_7.setTransform(-1.4,9.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhwA2IDhhr");
	this.shape_8.setTransform(0.2,8.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#FFFFFF").ss(2,1,1).p("Ah/A9ID/h5");
	this.shape_9.setTransform(1.7,7.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#FFFFFF").ss(2,1,1).p("AiOBEIEdiH");
	this.shape_10.setTransform(3.2,7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_1}]},54).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[]},34).wait(39));

	// Calque_6
	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAAgFIAAAL");
	this.shape_11.setTransform(-11.1,-13.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAAATIAAgl");
	this.shape_12.setTransform(-11.1,-12.5);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAAAhIAAhB");
	this.shape_13.setTransform(-11.1,-11.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAAAvIAAhc");
	this.shape_14.setTransform(-11.1,-9.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAAA8IAAh3");
	this.shape_15.setTransform(-11.1,-8.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAABKIAAiT");
	this.shape_16.setTransform(-11.1,-7.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAABXIAAit");
	this.shape_17.setTransform(-11.1,-5.7);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAABlIAAjJ");
	this.shape_18.setTransform(-11.1,-4.4);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAAByIAAjj");
	this.shape_19.setTransform(-11.1,-3);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAACAIAAj/");
	this.shape_20.setTransform(-11.1,-1.7);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAAiMIAAEZ");
	this.shape_21.setTransform(-11.1,-0.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_11}]},44).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[]},43).wait(39));

	// Calque_4
	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#FFFFFF").ss(2,1,1).p("AANAHIgZgM");
	this.shape_22.setTransform(16.2,-0.5);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgbgNIA3Ab");
	this.shape_23.setTransform(14.8,-1.2);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgpgUIBTAp");
	this.shape_24.setTransform(13.3,-2);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#FFFFFF").ss(2,1,1).p("Ag3gcIBwA5");
	this.shape_25.setTransform(11.9,-2.7);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhGgjICNBH");
	this.shape_26.setTransform(10.4,-3.4);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhUgrICpBX");
	this.shape_27.setTransform(9,-4.2);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhjgyIDHBl");
	this.shape_28.setTransform(7.5,-4.9);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#FFFFFF").ss(2,1,1).p("Ahxg5IDjBz");
	this.shape_29.setTransform(6.1,-5.7);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#FFFFFF").ss(2,1,1).p("AiAhBIEBCD");
	this.shape_30.setTransform(4.6,-6.4);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#FFFFFF").ss(2,1,1).p("ACPBJIkdiR");
	this.shape_31.setTransform(3.2,-7.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_22}]},35).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[]},53).wait(39));

	// Calque_3
	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAVAAIgpAA");
	this.shape_32.setTransform(-318.9,0);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgVAAIArAA");
	this.shape_33.setTransform(-318.6,0);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgZAAIAzAA");
	this.shape_34.setTransform(-317.5,0);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgfAAIA/AA");
	this.shape_35.setTransform(-315.8,0);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgnAAIBPAA");
	this.shape_36.setTransform(-313.3,0);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgyAAIBkAA");
	this.shape_37.setTransform(-310.1,0);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#FFFFFF").ss(2,1,1).p("Ag/AAIB+AA");
	this.shape_38.setTransform(-306.3,0);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhOAAICdAA");
	this.shape_39.setTransform(-301.7,0);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhfAAIC/AA");
	this.shape_40.setTransform(-296.5,0);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhzAAIDnAA");
	this.shape_41.setTransform(-290.5,0);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#FFFFFF").ss(2,1,1).p("AiJAAIETAA");
	this.shape_42.setTransform(-283.8,0);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#FFFFFF").ss(2,1,1).p("AiiAAIFFAA");
	this.shape_43.setTransform(-276.4,0);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#FFFFFF").ss(2,1,1).p("Ai9AAIF7AA");
	this.shape_44.setTransform(-268.4,0);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#FFFFFF").ss(2,1,1).p("AjaAAIG2AA");
	this.shape_45.setTransform(-259.6,0);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().s("#FFFFFF").ss(2,1,1).p("Aj6AAIH1AA");
	this.shape_46.setTransform(-250.1,0);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().s("#FFFFFF").ss(2,1,1).p("AkcAAII5AA");
	this.shape_47.setTransform(-239.9,0);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f().s("#FFFFFF").ss(2,1,1).p("AlAAAIKBAA");
	this.shape_48.setTransform(-229.1,0);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f().s("#FFFFFF").ss(2,1,1).p("AlnAAILPAA");
	this.shape_49.setTransform(-217.5,0);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f().s("#FFFFFF").ss(2,1,1).p("AmQAAIMhAA");
	this.shape_50.setTransform(-205.2,0);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f().s("#FFFFFF").ss(2,1,1).p("AmUAAIMpAA");
	this.shape_51.setTransform(-194.2,0);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f().s("#FFFFFF").ss(2,1,1).p("AmZAAIMzAA");
	this.shape_52.setTransform(-183.3,0);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f().s("#FFFFFF").ss(2,1,1).p("AmdAAIM7AA");
	this.shape_53.setTransform(-172.3,0);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f().s("#FFFFFF").ss(2,1,1).p("AmhAAINDAA");
	this.shape_54.setTransform(-161.3,0);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f().s("#FFFFFF").ss(2,1,1).p("AmmAAINNAA");
	this.shape_55.setTransform(-150.4,0);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f().s("#FFFFFF").ss(2,1,1).p("AmqAAINVAA");
	this.shape_56.setTransform(-139.4,0);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f().s("#FFFFFF").ss(2,1,1).p("AmvAAINeAA");
	this.shape_57.setTransform(-128.4,0);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f().s("#FFFFFF").ss(2,1,1).p("AmzAAINnAA");
	this.shape_58.setTransform(-117.5,0);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f().s("#FFFFFF").ss(2,1,1).p("Am4AAINxAA");
	this.shape_59.setTransform(-106.5,0);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f().s("#FFFFFF").ss(2,1,1).p("Am8AAIN5AA");
	this.shape_60.setTransform(-95.5,0);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f().s("#FFFFFF").ss(2,1,1).p("AnAAAIOBAA");
	this.shape_61.setTransform(-84.6,0);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f().s("#FFFFFF").ss(2,1,1).p("AnFAAIOLAA");
	this.shape_62.setTransform(-73.6,0);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f().s("#FFFFFF").ss(2,1,1).p("AnJAAIOTAA");
	this.shape_63.setTransform(-62.6,0);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f().s("#FFFFFF").ss(2,1,1).p("AnOAAIOdAA");
	this.shape_64.setTransform(-51.7,0);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f().s("#FFFFFF").ss(2,1,1).p("AnSAAIOlAA");
	this.shape_65.setTransform(-40.7,0);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f().s("#FFFFFF").ss(2,1,1).p("AHXAAIutAA");
	this.shape_66.setTransform(-29.8,0);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f().s("#FFFFFF").ss(2,1,1).p("AmfAAIM/AA");
	this.shape_67.setTransform(-24.2,0);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f().s("#FFFFFF").ss(2,1,1).p("AlrAAILXAA");
	this.shape_68.setTransform(-19,0);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f().s("#FFFFFF").ss(2,1,1).p("Ak7AAIJ3AA");
	this.shape_69.setTransform(-14.2,0);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f().s("#FFFFFF").ss(2,1,1).p("AkOAAIIdAA");
	this.shape_70.setTransform(-9.7,0);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f().s("#FFFFFF").ss(2,1,1).p("AjlAAIHLAA");
	this.shape_71.setTransform(-5.6,0);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f().s("#FFFFFF").ss(2,1,1).p("Ai/AAIF/AA");
	this.shape_72.setTransform(-1.8,0);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f().s("#FFFFFF").ss(2,1,1).p("AidAAIE7AA");
	this.shape_73.setTransform(1.6,0);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f().s("#FFFFFF").ss(2,1,1).p("Ah/AAID/AA");
	this.shape_74.setTransform(4.6,0);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhkAAIDJAA");
	this.shape_75.setTransform(7.3,0);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhNAAICbAA");
	this.shape_76.setTransform(9.6,0);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f().s("#FFFFFF").ss(2,1,1).p("Ag5AAIBzAA");
	this.shape_77.setTransform(11.6,0);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgpAAIBTAA");
	this.shape_78.setTransform(13.2,0);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgcAAIA5AA");
	this.shape_79.setTransform(14.5,0);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgTAAIAnAA");
	this.shape_80.setTransform(15.3,0);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgOAAIAdAA");
	this.shape_81.setTransform(15.9,0);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f().s("#FFFFFF").ss(2,1,1).p("AANAAIgZAA");
	this.shape_82.setTransform(16.1,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_51}]},1).to({state:[{t:this.shape_52}]},1).to({state:[{t:this.shape_53}]},1).to({state:[{t:this.shape_54}]},1).to({state:[{t:this.shape_55}]},1).to({state:[{t:this.shape_56}]},1).to({state:[{t:this.shape_57}]},1).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_59}]},1).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_63}]},1).to({state:[{t:this.shape_64}]},1).to({state:[{t:this.shape_65}]},1).to({state:[{t:this.shape_66}]},1).to({state:[{t:this.shape_67}]},1).to({state:[{t:this.shape_68}]},1).to({state:[{t:this.shape_69}]},1).to({state:[{t:this.shape_70}]},1).to({state:[{t:this.shape_71}]},1).to({state:[{t:this.shape_72}]},1).to({state:[{t:this.shape_73}]},1).to({state:[{t:this.shape_74}]},1).to({state:[{t:this.shape_75}]},1).to({state:[{t:this.shape_76}]},1).to({state:[{t:this.shape_77}]},1).to({state:[{t:this.shape_78}]},1).to({state:[{t:this.shape_79}]},1).to({state:[{t:this.shape_80}]},1).to({state:[{t:this.shape_81}]},1).to({state:[{t:this.shape_82}]},1).to({state:[]},1).wait(84));

	// Calque_8 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_62 = new cjs.Graphics().p("Ai/DUIAAmoIF/AAIAAGog");
	var mask_graphics_63 = new cjs.Graphics().p("Ai/DUIAAmoIF/AAIAAGog");
	var mask_graphics_64 = new cjs.Graphics().p("Ai/DUIAAmoIF/AAIAAGog");
	var mask_graphics_65 = new cjs.Graphics().p("Ai/DUIAAmoIF/AAIAAGog");
	var mask_graphics_66 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_67 = new cjs.Graphics().p("AjADUIAAmoIGAAAIAAGog");
	var mask_graphics_68 = new cjs.Graphics().p("Ai/DUIAAmoIGAAAIAAGog");
	var mask_graphics_69 = new cjs.Graphics().p("Ai/DUIAAmoIF/AAIAAGog");
	var mask_graphics_70 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_71 = new cjs.Graphics().p("Ai/DUIAAmoIGAAAIAAGog");
	var mask_graphics_72 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_73 = new cjs.Graphics().p("Ai/DUIAAmoIF/AAIAAGog");
	var mask_graphics_74 = new cjs.Graphics().p("AjADUIAAmoIGAAAIAAGog");
	var mask_graphics_75 = new cjs.Graphics().p("AjADUIAAmoIGAAAIAAGog");
	var mask_graphics_76 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_77 = new cjs.Graphics().p("Ai/DUIAAmoIF/AAIAAGog");
	var mask_graphics_78 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_79 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_80 = new cjs.Graphics().p("Ai/DUIAAmoIF/AAIAAGog");
	var mask_graphics_81 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_82 = new cjs.Graphics().p("Ai/DUIAAmoIF/AAIAAGog");
	var mask_graphics_83 = new cjs.Graphics().p("AjADUIAAmoIGAAAIAAGog");
	var mask_graphics_84 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_85 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_86 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_87 = new cjs.Graphics().p("Ai/DUIAAmoIGAAAIAAGog");
	var mask_graphics_88 = new cjs.Graphics().p("AjADUIAAmoIGAAAIAAGog");
	var mask_graphics_89 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_90 = new cjs.Graphics().p("Ai/DUIAAmoIGAAAIAAGog");
	var mask_graphics_91 = new cjs.Graphics().p("AjADUIAAmoIGAAAIAAGog");
	var mask_graphics_92 = new cjs.Graphics().p("Ai/DUIAAmoIF/AAIAAGog");
	var mask_graphics_93 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_94 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_95 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_96 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_97 = new cjs.Graphics().p("Ai/DUIAAmoIGAAAIAAGog");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(62).to({graphics:mask_graphics_62,x:38.2,y:0.7}).wait(1).to({graphics:mask_graphics_63,x:38.2,y:0.7}).wait(1).to({graphics:mask_graphics_64,x:38.2,y:0.7}).wait(1).to({graphics:mask_graphics_65,x:38.2,y:0.7}).wait(1).to({graphics:mask_graphics_66,x:38.2,y:0.7}).wait(1).to({graphics:mask_graphics_67,x:38.1,y:0.7}).wait(1).to({graphics:mask_graphics_68,x:38,y:0.7}).wait(1).to({graphics:mask_graphics_69,x:37.8,y:0.7}).wait(1).to({graphics:mask_graphics_70,x:37.5,y:0.7}).wait(1).to({graphics:mask_graphics_71,x:37.1,y:0.7}).wait(1).to({graphics:mask_graphics_72,x:36.5,y:0.7}).wait(1).to({graphics:mask_graphics_73,x:35.6,y:0.7}).wait(1).to({graphics:mask_graphics_74,x:34.5,y:0.7}).wait(1).to({graphics:mask_graphics_75,x:33.2,y:0.7}).wait(1).to({graphics:mask_graphics_76,x:31.4,y:0.7}).wait(1).to({graphics:mask_graphics_77,x:29.2,y:0.7}).wait(1).to({graphics:mask_graphics_78,x:26.6,y:0.7}).wait(1).to({graphics:mask_graphics_79,x:23.4,y:0.7}).wait(1).to({graphics:mask_graphics_80,x:20.2,y:0.7}).wait(1).to({graphics:mask_graphics_81,x:17.6,y:0.7}).wait(1).to({graphics:mask_graphics_82,x:15.4,y:0.7}).wait(1).to({graphics:mask_graphics_83,x:13.6,y:0.7}).wait(1).to({graphics:mask_graphics_84,x:12.3,y:0.7}).wait(1).to({graphics:mask_graphics_85,x:11.2,y:0.7}).wait(1).to({graphics:mask_graphics_86,x:10.4,y:0.7}).wait(1).to({graphics:mask_graphics_87,x:9.7,y:0.7}).wait(1).to({graphics:mask_graphics_88,x:9.3,y:0.7}).wait(1).to({graphics:mask_graphics_89,x:9,y:0.7}).wait(1).to({graphics:mask_graphics_90,x:8.8,y:0.7}).wait(1).to({graphics:mask_graphics_91,x:8.7,y:0.7}).wait(1).to({graphics:mask_graphics_92,x:8.6,y:0.7}).wait(1).to({graphics:mask_graphics_93,x:8.6,y:0.7}).wait(1).to({graphics:mask_graphics_94,x:8.6,y:0.7}).wait(1).to({graphics:mask_graphics_95,x:8.6,y:0.7}).wait(1).to({graphics:mask_graphics_96,x:8.6,y:0.7}).wait(1).to({graphics:mask_graphics_97,x:4.8,y:0.7}).wait(39));

	// Calque_7
	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("#FFFFFF").s().p("AiOiMIEdCRIkdCIg");
	this.shape_83.setTransform(3.2,-0.3);

	this.instance_5 = new lib.playArrow();
	this.instance_5.parent = this;
	this.instance_5.setTransform(3.2,-0.3);
	this.instance_5._off = true;

	var maskedShapeInstanceList = [this.shape_83,this.instance_5];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_83}]},62).to({state:[{t:this.instance_5}]},35).to({state:[{t:this.instance_5}]},3).to({state:[{t:this.instance_5}]},18).to({state:[]},1).wait(17));
	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(97).to({_off:false},0).wait(3).to({scaleX:0.07,scaleY:0.07},18,cjs.Ease.quartIn).to({_off:true},1).wait(17));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-44.3,-44.4,88.8,88.8);


(lib.Btn_next = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{rollOver:55,rollOut:70});

	// timeline functions:
	this.frame_54 = function() {
		this.stop();
	}
	this.frame_69 = function() {
		this.stop();
	}
	this.frame_84 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(54).call(this.frame_54).wait(15).call(this.frame_69).wait(15).call(this.frame_84).wait(1));

	// Calque_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.008)").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape.setTransform(0,29.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(85));

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgJAqIAAhDIgXAAIAAgQIBBAAIAAAQIgYAAIAABDg");
	this.shape_1.setTransform(46.1,29.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AASAqIgSgcIgRAcIgWAAIAcgqIgagpIAWAAIAPAZIAQgZIAWAAIgaApIAdAqg");
	this.shape_2.setTransform(30.2,29.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgeAqIAAhTIA8AAIAAARIgpAAIAAARIAkAAIAAAPIgkAAIAAASIAqAAIAAAQg");
	this.shape_3.setTransform(14.3,29.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAUAqIgngzIAAAzIgTAAIAAhTIASAAIAoA1IAAg1IATAAIAABTg");
	this.shape_4.setTransform(-2.7,29.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgKAQIAEgOQgEgCAAgFQAAgEADgDQADgDAEAAQAFAAADADQADADAAAEQAAAEgDAEIgIANg");
	this.shape_5.setTransform(-28.5,32.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAPAqIgYghIgIAKIAAAXIgTAAIAAhTIATAAIAAAjIAegjIAYAAIgiAlIAQAWIASAYg");
	this.shape_6.setTransform(-38,29.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgfAfQgMgNAAgSQAAgSAMgMQAOgNARAAQASAAANANQANAMAAASQAAASgNANQgNAMgSAAQgRAAgOgMgAgQgSQgIAIAAAKQAAALAIAIQAGAIAKAAQALAAAHgIQAHgIAAgLQAAgKgHgIQgHgIgLAAQgKAAgGAIg");
	this.shape_7.setTransform(-55.7,29.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#333333").ss(1,1,1).p("AxpAAMAjTAAA");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(85));

	// Layer_2
	this.instance = new lib.Btn_cliquable_area();
	this.instance.parent = this;
	this.instance.setTransform(0,29.5,1,1,0,0,0,0,30);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({alpha:0.012},46).wait(6).to({y:30},0).to({alpha:1},14).wait(1).to({alpha:0.012},13).wait(2));

	// BG
	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("rgba(0,0,0,0.098)").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape_9.setTransform(0,29.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_9).wait(85));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-114,-1,228,60.5);


(lib.Btn_detail = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"rollOver":55,"rollOut":70});

	// timeline functions:
	this.frame_54 = function() {
		this.stop();
	}
	this.frame_69 = function() {
		this.stop();
	}
	this.frame_84 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(54).call(this.frame_54).wait(15).call(this.frame_69).wait(15).call(this.frame_84).wait(1));

	// Calque_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.008)").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape.setTransform(0,29.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(85));

	// content
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgOAyIAAhkIAdAAIAABkg");
	this.shape_1.setTransform(-85.4,29.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgyAOIAAgcIBlAAIAAAcg");
	this.shape_2.setTransform(-85.4,29.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgaAqIAAhTIASAAIAABDIAjAAIAAAQg");
	this.shape_3.setTransform(48.4,29.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgIAqIAAhTIARAAIAABTg");
	this.shape_4.setTransform(34.7,29.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AAZAqIgHgSIgjAAIgHASIgUAAIAkhTIARAAIAkBTgAgKAIIAUAAIgKgYg");
	this.shape_5.setTransform(20.2,29.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgJAqIAAhDIgXAAIAAgQIBBAAIAAAQIgYAAIAABDg");
	this.shape_6.setTransform(4.1,29.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgeAqIAAhTIA8AAIAAARIgpAAIAAARIAkAAIAAAPIgkAAIAAASIAqAAIAAAQg");
	this.shape_7.setTransform(-11.3,29.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AglAqIAAhTIAeAAQAVAAAMALQAMALAAATQAAATgMALQgMAMgWAAgAgTAaIANAAQAMAAAGgHQAHgGAAgNQAAgLgHgHQgGgGgOgBIgLAAg");
	this.shape_8.setTransform(-27.8,29.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#333333").ss(1,1,1).p("AxpAAMAjTAAA");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(85));

	// Calque_7
	this.instance = new lib.Btn_cliquable_area();
	this.instance.parent = this;
	this.instance.setTransform(0,29.5,1,1,0,0,0,0,30);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({alpha:0.012},46).wait(6).to({y:30},0).to({alpha:1},14).wait(1).to({alpha:0.012},13).wait(2));

	// BG
	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("rgba(0,0,0,0.004)").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape_10.setTransform(0,29.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_10).wait(85));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-114,-1,228,60.5);


(lib.bloc_visuel = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":63});

	// timeline functions:
	this.frame_62 = function() {
		this.stop();
	}
	this.frame_99 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(62).call(this.frame_62).wait(37).call(this.frame_99).wait(1));

	// masque VISUEL (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_20 = new cjs.Graphics().p("EgjsBDvMAAAiHdMBHZAAAMAAACHdg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(20).to({graphics:mask_graphics_20,x:-228.7,y:-19}).wait(80));

	// VISUEL
	this.instance = new lib.prject_illustration();
	this.instance.parent = this;
	this.instance.setTransform(552.6,26.5,1,1,175,0,0,0,316.5);
	this.instance.alpha = 0.012;

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:0.1,regY:316.6,rotation:354.5,x:-159.3,y:6.6,alpha:1},20,cjs.Ease.cubicOut).to({regX:0,rotation:347.3,x:-39.5},28,cjs.Ease.cubicInOut).wait(20).to({x:-719.5},30,cjs.Ease.quartInOut).to({_off:true},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(304.3,-219.2,616.8,645.8);


(lib.bloc_video = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"open":1,"close":42});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_34 = function() {
		this.btn_playVideo.gotoAndPlay("open");
	}
	this.frame_41 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(34).call(this.frame_34).wait(7).call(this.frame_41).wait(16));

	// PLAY btn
	this.btn_playVideo = new lib.btn_playVideo();
	this.btn_playVideo.name = "btn_playVideo";
	this.btn_playVideo.parent = this;
	this.btn_playVideo.setTransform(358.3,-255.7);

	this.timeline.addTween(cjs.Tween.get(this.btn_playVideo).wait(57));

	// masque video (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_43 = new cjs.Graphics().p("Ega8A8AMAAAh3/MA7rAAAMAAAB3/g");
	var mask_graphics_44 = new cjs.Graphics().p("EgYrA8AMAAAh3/MA3HAAAMAAAB3/g");
	var mask_graphics_45 = new cjs.Graphics().p("EgWaA8AMAAAh3/MAyiAAAMAAAB3/g");
	var mask_graphics_46 = new cjs.Graphics().p("EgUJA8AMAAAh3/MAt+AAAMAAAB3/g");
	var mask_graphics_47 = new cjs.Graphics().p("EgR4A8AMAAAh3/MApaAAAMAAAB3/g");
	var mask_graphics_48 = new cjs.Graphics().p("EgPnA8AMAAAh3/MAk1AAAMAAAB3/g");
	var mask_graphics_49 = new cjs.Graphics().p("EgNWA8AMAAAh3/MAgRAAAMAAAB3/g");
	var mask_graphics_50 = new cjs.Graphics().p("EgLFA8AMAAAh3/IbtAAMAAAB3/g");
	var mask_graphics_51 = new cjs.Graphics().p("EgI0A8AMAAAh3/IXIAAMAAAB3/g");
	var mask_graphics_52 = new cjs.Graphics().p("EgGjA8AMAAAh3/ISkAAMAAAB3/g");
	var mask_graphics_53 = new cjs.Graphics().p("EgESA8AMAAAh3/IOAAAMAAAB3/g");
	var mask_graphics_54 = new cjs.Graphics().p("EgCBA8AMAAAh3/IJbAAMAAAB3/g");
	var mask_graphics_55 = new cjs.Graphics().p("EAAPA8AMAAAh3/IE4AAMAAAB3/g");
	var mask_graphics_56 = new cjs.Graphics().p("EACgA8AMAAAh3/IAUAAMAAAB3/g");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(43).to({graphics:mask_graphics_43,x:209.5,y:0}).wait(1).to({graphics:mask_graphics_44,x:194.8,y:0}).wait(1).to({graphics:mask_graphics_45,x:180,y:0}).wait(1).to({graphics:mask_graphics_46,x:165.3,y:0}).wait(1).to({graphics:mask_graphics_47,x:150.6,y:0}).wait(1).to({graphics:mask_graphics_48,x:135.8,y:0}).wait(1).to({graphics:mask_graphics_49,x:121.1,y:0}).wait(1).to({graphics:mask_graphics_50,x:106.4,y:0}).wait(1).to({graphics:mask_graphics_51,x:91.6,y:0}).wait(1).to({graphics:mask_graphics_52,x:76.9,y:0}).wait(1).to({graphics:mask_graphics_53,x:62.2,y:0}).wait(1).to({graphics:mask_graphics_54,x:47.4,y:0}).wait(1).to({graphics:mask_graphics_55,x:32.7,y:0}).wait(1).to({graphics:mask_graphics_56,x:18,y:0}).wait(1));

	// VIDEO
	this.videoContainer = new lib.video();
	this.videoContainer.name = "videoContainer";
	this.videoContainer.parent = this;
	this.videoContainer.setTransform(36.7,765,0.659,0.659,0,0,0,0.3,480.2);

	var maskedShapeInstanceList = [this.videoContainer];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.videoContainer).wait(1).to({y:1},40,cjs.Ease.quartInOut).wait(16));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(36.5,-300.1,382.1,1381.2);


(lib.bloc_NEXT_btn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":40});

	// timeline functions:
	this.frame_39 = function() {
		this.stop();
	}
	this.frame_56 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(39).call(this.frame_39).wait(17).call(this.frame_56).wait(1));

	// CTA DETAIL
	this.btnNext = new lib.Btn_next();
	this.btnNext.name = "btnNext";
	this.btnNext.parent = this;
	this.btnNext.setTransform(-340.5,411.5,1,1,0,0,0,0,26.5);

	this.timeline.addTween(cjs.Tween.get(this.btnNext).to({y:350.5},39,cjs.Ease.quartInOut).wait(2).to({y:411.5},14,cjs.Ease.quartInOut).to({_off:true},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-454,384.5,227,60);


(lib.bloc_lignes = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{close_OLD:166,"close":209});

	// timeline functions:
	this.frame_165 = function() {
		this.stop();
	}
	this.frame_208 = function() {
		this.stop();
	}
	this.frame_229 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(165).call(this.frame_165).wait(43).call(this.frame_208).wait(21).call(this.frame_229).wait(1));

	// ligne
	this.instance = new lib.ligne();
	this.instance.parent = this;
	this.instance.setTransform(-454,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(166).to({x:-883},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:-454},0).wait(21));

	// ligne
	this.instance_1 = new lib.ligne();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-227,406.5,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(119).to({y:320.5},40,cjs.Ease.quartInOut).wait(7).to({y:406.5},0).to({x:-883.2},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:-227,y:320.5},0).to({y:406.5},17,cjs.Ease.quartInOut).wait(4));

	// ligne
	this.instance_2 = new lib.ligne();
	this.instance_2.parent = this;
	this.instance_2.setTransform(0,406.5,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(166).to({x:-883.4},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:0},0).wait(21));

	// ligne
	this.instance_3 = new lib.ligne();
	this.instance_3.parent = this;
	this.instance_3.setTransform(227,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(124).to({y:320},40,cjs.Ease.quartInOut).to({_off:true},2).wait(43).to({_off:false},0).to({y:406},17,cjs.Ease.quartInOut).wait(4));

	// ligne
	this.instance_4 = new lib.ligne();
	this.instance_4.parent = this;
	this.instance_4.setTransform(454.1,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(166).to({x:-883.4},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:454.1},0).wait(21));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-455.4,-174.5,910.5,988);


(lib.bloc_hashtag = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_61 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(61).call(this.frame_61).wait(2));

	// Layer_1
	this.instance = new lib.hashtag_graphic("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(-456,243.2,2.317,2.317,0,0,0,-0.1,50.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true},61).wait(2));

	// Layer_5 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_28 = new cjs.Graphics().p("AtWBpIAAjRIatAAIAADRg");
	var mask_graphics_32 = new cjs.Graphics().p("AtWDSIAAmjIatAAIAAGjg");
	var mask_graphics_40 = new cjs.Graphics().p("AtWGYIAAsvIatAAIAAMvg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(28).to({graphics:mask_graphics_28,x:-400.4,y:265.5}).wait(4).to({graphics:mask_graphics_32,x:-400.4,y:255}).wait(8).to({graphics:mask_graphics_40,x:-400.4,y:255.9}).wait(23));

	// Layer_3
	this.instance_1 = new lib.txt_hashtag();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-421.5,244.1,1,1,0,0,0,0,31.6);
	this.instance_1._off = true;

	var maskedShapeInstanceList = [this.instance_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(28).to({_off:false},0).to({_off:true},34).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-497.8,166.1,149.7,129.4);


(lib.bloc_CTA = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":40});

	// timeline functions:
	this.frame_39 = function() {
		this.stop();
	}
	this.frame_56 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(39).call(this.frame_39).wait(17).call(this.frame_56).wait(1));

	// CTA DETAIL
	this.btnCta = new lib.Btn_detail();
	this.btnCta.name = "btnCta";
	this.btnCta.parent = this;
	this.btnCta.setTransform(-340.5,411.5,1,1,0,0,0,0,26.5);

	this.timeline.addTween(cjs.Tween.get(this.btnCta).to({y:350.5},39,cjs.Ease.quartInOut).wait(2).to({y:411.5},14,cjs.Ease.quartInOut).to({_off:true},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-454,384.5,227,60);


(lib.blocInfostextes = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":80});

	// timeline functions:
	this.frame_79 = function() {
		this.stop();
	}
	this.frame_120 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(79).call(this.frame_79).wait(41).call(this.frame_120).wait(1));

	// Bloc Hashtag
	this.instance = new lib.bloc_hashtag();
	this.instance.parent = this;
	this.instance.setTransform(-391.5,261.2,0.9,0.9,0,0,0,-412.9,244.7);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(62).to({_off:false},0).to({_off:true},18).wait(41));

	// masque Titre
	this.instance_1 = new lib.masque_generique();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-527.9,-13.9,0.007,0.471,0,0,0,0,17.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(34).to({_off:false},0).to({regX:0.5,scaleX:1.92,x:-527.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.01,x:-228.9},10,cjs.Ease.quartInOut).to({_off:true},1).wait(26).to({_off:false},0).to({regX:0.5,scaleX:1.92,x:-527.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.01,x:-528.7},10,cjs.Ease.quartInOut).to({_off:true},1).wait(21));

	// _NOM-du-CLIENT
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF8D72").s().p("AgMA4IAAhZIgfAAIAAgWIBXAAIAAAWIggAAIAABZg");
	this.shape.setTransform(-385.7,-14.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF8D72").s().p("AgiApQgRgQgBgYQABgZARgQQAQgRAZAAQAbAAATAVIgQASQgMgPgRAAQgNAAgLAJQgKAJABAPQAAAPAJAKQAJAKAMAAQATAAAMgPIAQARQgUAVgZAAQgZAAgQgRg");
	this.shape_1.setTransform(-408.3,-14.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF8D72").s().p("AgMA4IAAhvIAZAAIAABvg");
	this.shape_2.setTransform(-428.8,-14.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF8D72").s().p("AgyA4IAAhvIAoAAQAcAAARAPQAPAOAAAaQAAAZgPAQQgPAPggAAgAgYAiIAPAAQAQAAAKgJQAIgJABgQQgBgPgIgJQgKgJgSAAIgNAAg");
	this.shape_3.setTransform(-449.3,-14.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FF8D72").s().p("AAiA4IgLgYIguAAIgJAYIgbAAIAwhvIAXAAIAwBvgAgNAKIAbAAIgOgfg");
	this.shape_4.setTransform(-473.7,-14.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FF8D72").s().p("AgoA4IAAhvIBQAAIAAAXIg3AAIAAAWIAxAAIAAAUIgxAAIAAAYIA4AAIAAAWg");
	this.shape_5.setTransform(-496.6,-14.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FF8D72").s().p("AAWA4IAAgsIgrAAIAAAsIgZAAIAAhvIAZAAIAAAvIArAAIAAgvIAZAAIAABvg");
	this.shape_6.setTransform(-520.2,-14.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},34).to({state:[]},55).to({state:[]},1).wait(31));

	// masque Titre (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_17 = new cjs.Graphics().p("EgpOArJMAAAhWRMAvJAAAMAAABWRg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(17).to({graphics:mask_graphics_17,x:-263.9,y:-6.1}).wait(24).to({graphics:null,x:0,y:0}).wait(80));

	// _LETTRES_ANARCHIQUES
	this.instance_2 = new lib.lettres_FAT();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-433.8,-73,1,1,0,0,0,0,186.8);
	this.instance_2._off = true;

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FF8D72").s().p("AhNFeIAAozIjGAAIAAiHIInAAIAACHIjGAAIAAIzg");
	this.shape_7.setTransform(-259.2,7.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FF8D72").s().p("AjeEDQhphlAAicQAAibBrhnQBrhpCYAAQCtAABzCEIhhBuQhJhchvAAQhXgBg+A6Qg/A6AABgQABBhA7A8QA7A6BTAAQByABBKhcIBkBoQh4CFihAAQihABhohng");
	this.shape_8.setTransform(-337.6,7.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FF8D72").s().p("AhNFeIAAq6ICbAAIAAK6g");
	this.shape_9.setTransform(-403.2,7.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FF8D72").s().p("AixBCIAAiDIFjAAIAACDg");
	this.shape_10.setTransform(-360.4,-111.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FF8D72").s().p("AixBCIAAiDIFjAAIAACDg");
	this.shape_11.setTransform(-421.4,-111.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FF8D72").s().p("Ak8FdIAAq5ID3AAQC6AABkBbQBkBbAACjQAACihiBgQhhBejHAAgAigDTIBjAAQBqAAA5g2QA6g2AAhmQAAhmg6g4Qg5g4h2ABIhXAAg");
	this.shape_12.setTransform(-494.5,-116);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FF8D72").s().p("AhNFdIAAoyIjGAAIAAiHIInAAIAACHIjGAAIAAIyg");
	this.shape_13.setTransform(-219.6,106.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FF8D72").s().p("AjeEEQhohngBibQAAibBrhnQBrhoCYgBQCtAABzCEIhhBuQhJhchuAAQhYAAg+A5Qg/A5AABhQAABhA8A8QA6A6BUAAQByABBKhcIBkBnQh3CGiiAAQihABhohmg");
	this.shape_14.setTransform(-435.8,106.4);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FF8D72").s().p("AhNFdIAAq5ICbAAIAAK5g");
	this.shape_15.setTransform(-501.4,106.8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FF8D72").s().p("Ak8FeIAAq7ID3AAQC6AABkBcQBkBbAACjQAACjhiBeQhhBgjHAAgAigDTIBjAAQBqAAA5g2QA6g1AAhnQAAhng6g3Qg5g3h2gBIhXAAg");
	this.shape_16.setTransform(-300.4,-141);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FF8D72").s().p("Ak8FeIAAq7ID3AAQC6AABkBcQBkBbAACjQAACjhiBeQhhBgjHAAgAigDTIBjAAQBqAAA5g2QA6g1AAhnQAAhng6g3Qg5g3h2gBIhXAAg");
	this.shape_17.setTransform(-389.3,-141);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FF8D72").s().p("ACLFeIAAkUIkVAAIAAEUIicAAIAAq7ICcAAIAAEkIEVAAIAAkkICcAAIAAK7g");
	this.shape_18.setTransform(-479.7,-141);

	var maskedShapeInstanceList = [this.instance_2,this.shape_7,this.shape_8,this.shape_9,this.shape_10,this.shape_11,this.shape_12,this.shape_13,this.shape_14,this.shape_15,this.shape_16,this.shape_17,this.shape_18];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_2}]},17).to({state:[{t:this.instance_2}]},14).to({state:[{t:this.shape_12},{t:this.shape_11},{t:this.shape_10,p:{x:-360.4,y:-111.1}},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7}]},1).to({state:[{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_10,p:{x:-286.5,y:111.7}},{t:this.shape_13}]},5).to({state:[]},4).to({state:[]},39).wait(41));
	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(17).to({_off:false},0).to({x:-276.8},14,cjs.Ease.quartOut).to({_off:true},1).wait(89));

	// masque Titre
	this.instance_3 = new lib.masque_generique();
	this.instance_3.parent = this;
	this.instance_3.setTransform(221.1,-163.9,0.013,0.471,0,0,0,0,17.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({regX:0.5,scaleX:1.92,x:-75.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.01,x:-68.7},10,cjs.Ease.quartInOut).to({_off:true},1).wait(101));

	// DECO 2
	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FF8D72").s().p("AgDAKIgHgKQgCgDABgEQABgDADgCQAGgEAEAHIAIAKQACADgBAEQgBADgDACIgFABQgDAAgDgEg");
	this.shape_19.setTransform(-36.7,-68,2.741,2.741);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FF8D72").s().p("AgDAKIgGgLQgFgHAIgFQAHgEADAHIAGAMQAFAHgIAEIgFACQgDAAgCgFg");
	this.shape_20.setTransform(-25.9,-50.4,2.741,2.741);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FF8D72").s().p("AAAAPQgHgCACgIQAAgDgCgCQgGgGAGgGQAGgGAFAGQAJAJgDANQgBAGgGAAIgDgBg");
	this.shape_21.setTransform(-17.6,-29.9,2.741,2.741);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FF8D72").s().p("AgHAHIAAgOQAAgHAHAAQAJAAAAAHIAAAOQAAAJgJgBQgHABAAgJg");
	this.shape_22.setTransform(-13.6,-11.9,2.741,2.741);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FF8D72").s().p("AgHAKIAAgTQAAgHAHAAQAIAAAAAHIAAATQAAAHgIAAQgHAAAAgHg");
	this.shape_23.setTransform(-13,8.4,2.741,2.741);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FF8D72").s().p("AgFAUQgIgCACgIIAHgYQADgIAHADQAIACgCAIIgIAYQgBAGgFAAIgDgBg");
	this.shape_24.setTransform(-16.2,28.9,2.741,2.741);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FF8D72").s().p("AgEAIQgHAAgBgIQABgHAHAAIAIAAQAIAAABAHQgBAIgIAAg");
	this.shape_25.setTransform(-78.3,-81,2.741,2.741);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FF8D72").s().p("AgMADQgCgHAIgCIAJgCQAIgCACAIQADAHgJACIgJADIgCAAQgHAAgBgHg");
	this.shape_26.setTransform(-94.8,-79.5,2.741,2.741);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FF8D72").s().p("AgJAGQgCgDABgDQAAgDAEgCIAGgDQAGgDAEAGQACADgBADQgBADgDACIgGADIgDABQgFAAgCgEg");
	this.shape_27.setTransform(-108.7,-74.9,2.741,2.741);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FF8D72").s().p("AgJAPQgEgBgCgDQgDgHAGgEIAQgNQADgCAEACQADABACACQADAHgGAEIgQANIgEABIgCAAg");
	this.shape_28.setTransform(-122.9,-68.8,2.741,2.741);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FF8D72").s().p("AgHALQgCgBgBgEQgBgDACgDIAGgHQADgIAIAEQACACABAEQABADgCACIgGAIQgDAFgDAAIgFgCg");
	this.shape_29.setTransform(-136.4,-58.5,2.741,2.741);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FF8D72").s().p("AgJAJQAAgJADgJQADgJAGADQADABACADQACADgBADQgDAGABAIQABADgDADQgDACgCAAQgIAAgBgIg");
	this.shape_30.setTransform(-142.8,-40.6,2.741,2.741);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FF8D72").s().p("AgHAEIAAgIQgBgHAIAAQAJAAAAAHIAAAIQAAAJgJgBQgIABABgJg");
	this.shape_31.setTransform(-144.9,-22.6,2.741,2.741);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FF8D72").s().p("AgDAQQgHgDACgIIACgOQABgDADgCQACgBADABQAIABgCAJIgCAOQgBADgDACIgDABIgDAAg");
	this.shape_32.setTransform(-146.3,-4.4,2.741,2.741);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FF8D72").s().p("ACHBrQgGgCAAgGIgBgEQgKAFgJgBQgJgBgDgJQgYAGgTAAQgFABgCgFQgCgEACgEIADgGQgoAKgXgCQgigCALgaQg8AQgigGQgrgHADgoQAGg9A7grQBAguA1AkQAHAEgFAIQgEAHgHgFQgZgRgkAKQghAIgXAYQgUAUgHAQQgNAbABARQADAcAogDQAtgDAqgLQAVgdAcgLQAhgNAdAPQAEACAAAEQAAAGgEACQgmATg8ATQgPAWAUgBQAlgCAmgPQAGgCADAGQADAGgEAEQgEADgFAGQAPgCASgFQAEgBAEACQADADAAAEIAQgCQAEgDAEABQAFABABAEIAEAMQADADAKgBQAIgCADAIQACAJgIABIgPACQgEAAgLgEgAAJAaQAYgHAcgNIgHgBQgXAAgWAVg");
	this.shape_33.setTransform(-80.1,-17.4,2.741,2.741);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FF8D72").s().p("AgSGxQgkgFg4gMQhagUg/gkQhVgxgRhHQgJgjAKgmQAHgcAVgpQAvhdAsgxQAAgKACgPIAEgUIALhzQAHhBAigpQAVglAtghQAtghAogJQA4gLAiACQADgEAEAAQBJgFBAAbQBBAbAuA5QAeAlANApQAPAsgHApQADADAAADQALBhgGArQgKBLg3AlQACBNgdBIQgdBJg3A0QgeAcgYAPQgiAWgeADQgRADgUAAQgaAAgdgEgAk7BeQgbA1gHAbQgMAvAVAlQBABwC6AhIBYAQQA2AHAhgLQBAgWAxg6QBXhlABiCQgSAKgdAEQgcBIgWAnQghA5gsAjQhTBCh4gsQg/gXgtgfQg6gngWgwQgQghAWhEQASg7Aig1IgNgIQgDgCgCgEQgkAqgoBNgAkFBwQgVBMAnAnQBMBMBdAcQB+AlBOhVQAdggAXgtQARggAVg2IgTACIAAABQgoBfhHA4QhVBEhZghQgvgSgjggIgkghQgWgVgIgRQgTgkAOg7QAEgRAbhNIgBgBQgoA/gOAzgAjeBqQgKA9AeAgQApArAUAQQAmAfAkAHQBSAPBHg3QA7gwAlhVIgTABQgmBRg1ArQhFA3hIggQg7gbg3hLQgVgdAGguQADgTASg7IgPgJQgZBHgFAcgAixBYQgGAdAGAUQAFAUASAWQAXAbAPAOQAWAVAXAKQBFAfA9guQAwgjAlhMQhgAChAgbQhBgbhRgwQgLAogEAXgAieAFQBNAvAsAVQAtAVA4AHQArAGBAgBIAZgBIAegBQADgEAFgBQAEAAADADQAggFANgQIADgEQAFgIABgGIAQhOQAMhBgDgpQgEg0gdgwQgegygPgUQgeglgigOQgKAJgXgCQgUgBgNgHQgPgJgFgNIgFACQhBABghACQg6ADglAOQg2AUgaAsQgWAmgGA/IgIBZIgDAWQgCAOABAJQABALANAJIAWALIAWANIACAAQAHAAABAFgAFShrIAAAAQAAAmgLA6IgPBLQAVgUAKgaQAIgXABgiQAAg2gDgkQgFAMgGAKgAB+mVIABACQAQAAAPAJQAFADgBAEQAbAMgDASQBCAdA5BvQAUAnAGAnQAUgygSg4QgQg0grgoQhOhKhcgHQAKAGAIAHgAB1mBQgMAFADAJQADAJAKAEQAUAJAUgEQAJgCgDgHQgCgHgHgCIgKgDQgGgCABgHQgJgDgIAAQgFAAgEABgAARmfQg0AFghASQgfARgZAVQArgQA9gFIBrgFIAEABQABgIAGgGQAEgEAEgCQgXgRgrAAIgXABg");
	this.shape_34.setTransform(-90.5,14.6,2.741,2.741);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FF8D72").s().p("AgYAZQgDgCABgDIADgFIAkgmQAFgEAFABQABAAAAAAQAAAAABAAQABAAAAABQABAAABABQAAAFgEAEIgjAlQgFAFgEAAQgBAAAAAAQgBAAAAAAQgBgBAAAAQgBAAAAgBg");
	this.shape_35.setTransform(-84.3,114,3.176,3.176,18);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FF8D72").s().p("AgmANQgBgFAHgCQATgEAogSQAGgDAEAFQAEADgGAEQgeARggAIQgJAAgCgFg");
	this.shape_36.setTransform(-231.2,156.7,3.176,3.176,18);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FF8D72").s().p("AgiATQgDgCAEgEIAPgJQAJgDAMgIQAOgKAHgDQAGgEAEAEQADACgBADIgEAFQgaAUgbAKIgIADIgFgEg");
	this.shape_37.setTransform(-192.5,150.6,3.176,3.176,18);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FF8D72").s().p("AAXASQgmgJgPgJQgDAAgFgIQgEgEAEgDQAEgEAFAEQATANAZAFIASADQAIACAAAEQAAADgHADQgFgBgGABg");
	this.shape_38.setTransform(-315.8,145.2,3.176,3.176,18);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FF8D72").s().p("AggAbQgHgCADgGQAAgFAJgEQAbgOAWgRIAGgFQAEgBAEADQAFAFgJAGIg1AkQgFAEgEAAIgCAAg");
	this.shape_39.setTransform(-154.3,141.2,3.176,3.176,18);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FF8D72").s().p("AgjAcQgDgFAFgEIAVgPQAVgPAJgLIAIgHQAFgCADACQAFADgEAGIgFAFQgVATgdAVQgGAGgEAAIgFgDg");
	this.shape_40.setTransform(-119.1,128.3,3.176,3.176,18);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FF8D72").s().p("AADAoQgKgLgBgFQgHgfADgVQAAgEADgDQAEgGAFABQADABAAAFIgCAXIABAUQACAMAHAIIACAEQADAHgFABIgDABQgDAAgCgCg");
	this.shape_41.setTransform(-334.6,112.3,3.176,3.176,18);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FF8D72").s().p("AgsAPQgIgCACgFQAAgEAHAAQANAAASgEIAegHIAZgHQAFgBADAEQACAEgFADQgGAEgEAAQgpAMglADIgEAAg");
	this.shape_42.setTransform(-275.8,155.8,3.176,3.176,18);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FF8D72").s().p("AgVAmQgHgCACgHIAFgLQAKgYAZgbQAFgFAFABQAEAEAAAFQAAADgIAIQgUASgFAVQgFAQgIAAIgDAAg");
	this.shape_43.setTransform(-289.7,46.2,3.176,3.176,18);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FF8D72").s().p("AgQAlQgDgFAAgIIAJgeQAFgTAHgMQADgHAIADQAKACgEAJIgEAJQgOAVAAAbIAAAJQgBAFgHACQgFAAgEgGg");
	this.shape_44.setTransform(-320.7,77.3,3.176,3.176,18);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FF8D72").s().p("AlVGzQgdgKgkgWQgigVgQghQgOgbgDgoQgEg1AcgyIAeg6IAdg5QAHgPATgYQARgZADggQAEg/ASgtQAXg5AigyQAkg3BLgwQBLgxA/gUQBMgaBCADQBKADA/AnIAhATQATAMAMAKQAVARAUAcIAiAxQA1BSASBzQADAXgFAkQgDAJgCADQgcAngQASQhFBNhMBAIgmAiQgXAUgRAMQgbASgoAXIg6AhIg7AfQgYAMgYAJQg7AWhGASQgqAKhEAAIgRABQgZAAgbgJgAlKAIQgEABgEAFQgbAegOAfQgOAgglBDQgpBNAWBLQAMAmAfAXQA/AvBPgCQBagCBigjQAygSA1gbQAigRBBgnQBDglAhgfIBCg8IAugpQAcgYAPgUIAogvQgGgCgLACQhJANhKAFQghgBgRADQgaAEglACIhAACQghACgugDIhPgFQhcgEg0gNIgygLQgegIgTgKQgDgDgEAAIgCABgAgBjOQACACgBAEIgCAGIgEAHQgoA6ggBOQgIATgFAaIgKAuIAjACQByAIBFgFIBxgHQBEgFAsgFQAkgEAsgNQAggJACgZQACgQgFgvQgCgagDgHQgRgrgDgJQgMgpgmg+QgyhRhMgmQgwgYgdgBIgBACIACAFQAIANgCAFQgBAFgPAHQgIAFgJABQgQAAgJAEQgNAEgGAMQghAVgcAvIgWAfQgDAFgHgDQgFgCACgFQABgGADgFQAig9AlgbQALgJACgEQgBgCgHgCQgNgFgCgJQgBgJAKgIQAJgIAQgFQAHgCANAAIAVgBIgIgCQgPgEgXABQgZADgMAAQgrACgpASQhVAlgwAfQg2AlgWAbQgsA2gZA5QgNAcgIAjQgFAWgHArQgDATASALQAZANAUAFIBNASQAPADAsAFQAPABABgMQAGgkAQgyQAFgPAKgWIARgkQAKgYAIgNQAMgUAPgNIAIgGIAEgBQADAAACACgABlmOQgFAEABACQABAEAGAAQAcAFAbgKQgMgMgQgBIgEgBQgOAAgMAJg");
	this.shape_45.setTransform(-182.2,56,3.176,3.176,18);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().s("#FFFFFF").ss(1,1,1).p("AW3ISI4mONI1HzBILk58IcQC+g");
	this.shape_46.setTransform(-22.7,68.8);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().s("#FFFFFF").ss(1,1,1).p("AnFmSICWm8ISbU+I7XFfIAAgB");
	this.shape_47.setTransform(-240,-49.6);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FFFFFF").s().p("AqGJxIGnzhIAAAAINmPeI0MEDg");
	this.shape_48.setTransform(-262.9,-27.5);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f().s("#FFFFFF").ss(1,1,1).p("ALnNZI3NtWIXJtbg");
	this.shape_49.setTransform(-577,114.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_34,p:{x:-90.5,y:14.6}},{t:this.shape_33,p:{x:-80.1,y:-17.4}},{t:this.shape_32,p:{x:-146.3,y:-4.4}},{t:this.shape_31,p:{x:-144.9,y:-22.6}},{t:this.shape_30,p:{x:-142.8,y:-40.6}},{t:this.shape_29,p:{x:-136.4,y:-58.5}},{t:this.shape_28,p:{x:-122.9,y:-68.8}},{t:this.shape_27,p:{x:-108.7,y:-74.9}},{t:this.shape_26,p:{x:-94.8,y:-79.5}},{t:this.shape_25,p:{x:-78.3,y:-81}},{t:this.shape_24,p:{x:-16.2,y:28.9}},{t:this.shape_23,p:{x:-13,y:8.4}},{t:this.shape_22,p:{x:-13.6,y:-11.9}},{t:this.shape_21,p:{x:-17.6,y:-29.9}},{t:this.shape_20,p:{x:-25.9,y:-50.4}},{t:this.shape_19,p:{x:-36.7,y:-68}}]},17).to({state:[{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35}]},6).to({state:[{t:this.shape_34,p:{x:-196.1,y:-53.7}},{t:this.shape_33,p:{x:-185.7,y:-85.7}},{t:this.shape_32,p:{x:-251.9,y:-72.7}},{t:this.shape_31,p:{x:-250.5,y:-90.9}},{t:this.shape_30,p:{x:-248.4,y:-108.9}},{t:this.shape_29,p:{x:-242,y:-126.8}},{t:this.shape_28,p:{x:-228.5,y:-137.1}},{t:this.shape_27,p:{x:-214.3,y:-143.2}},{t:this.shape_26,p:{x:-200.4,y:-147.8}},{t:this.shape_25,p:{x:-183.9,y:-149.3}},{t:this.shape_24,p:{x:-121.8,y:-39.4}},{t:this.shape_23,p:{x:-118.6,y:-59.9}},{t:this.shape_22,p:{x:-119.2,y:-80.2}},{t:this.shape_21,p:{x:-123.2,y:-98.2}},{t:this.shape_20,p:{x:-131.5,y:-118.7}},{t:this.shape_19,p:{x:-142.3,y:-136.3}}]},10).to({state:[]},10).to({state:[{t:this.shape_46}]},50).to({state:[{t:this.shape_48},{t:this.shape_47}]},4).to({state:[{t:this.shape_49}]},3).to({state:[]},6).to({state:[]},1).wait(14));

	// masque Titre
	this.instance_4 = new lib.masque_generique();
	this.instance_4.parent = this;
	this.instance_4.setTransform(-228.9,-13.9,0.013,0.471,0,0,0,0,17.2);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(4).to({_off:false},0).to({regX:0.5,scaleX:1.92,x:-527.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.01,x:-528.7},10,cjs.Ease.quartInOut).to({_off:true},1).wait(97));

	// _LETTRES_CLIENT
	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FF8D72").s().p("AgJA0QgEgEAAgGQAAgFAEgFQAEgEAFAAQAGAAAEAEQAEAFAAAFQAAAGgEAEQgEAFgGAAQgFAAgEgFgAgJAVIgEg5IAAgUIAbAAIAAAUIgFA5g");
	this.shape_50.setTransform(-357.9,-14.1);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FF8D72").s().p("AAbA4Ig1hFIAABFIgZAAIAAhvIAYAAIA2BHIAAhHIAZAAIAABvg");
	this.shape_51.setTransform(-378.9,-14.1);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FF8D72").s().p("AgoA4IAAhvIBPAAIAAAXIg2AAIAAAWIAxAAIAAAUIgxAAIAAAYIA4AAIAAAWg");
	this.shape_52.setTransform(-402.4,-14.1);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FF8D72").s().p("AgMA4IAAhvIAZAAIAABvg");
	this.shape_53.setTransform(-422.5,-14.1);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FF8D72").s().p("AgCA5IADgYIgNAAIgEAYIgZAAIAFgYIgQAAIAFgWIAPAAIADgTIgRAAIAEgWIARAAIAEgaIAXAAIgDAaIANAAIAEgaIAZAAIgFAaIARAAIgEAWIgRAAIgDATIASAAIgEAWIgSAAIgEAYgAgJALIAOAAIAEgTIgOAAg");
	this.shape_54.setTransform(-459.4,-14.3);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FF8D72").s().p("AgjA4IAAhvIAZAAIAABYIAuAAIAAAXg");
	this.shape_55.setTransform(-497.6,-14.1);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FF8D72").s().p("AgjApQgRgQAAgYQAAgZARgQQASgRAXAAQAcAAASAVIgPASQgMgPgSAAQgNAAgKAJQgJAJgBAPQAAAPAKAKQAJAKANAAQASAAALgPIAQARQgTAVgZAAQgZAAgRgRg");
	this.shape_56.setTransform(-520.4,-14.2);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FF8D72").s().p("AgMA4IAAhZIgfAAIAAgWIBXAAIAAAWIggAAIAABZg");
	this.shape_57.setTransform(-411.9,-14.1);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#FF8D72").s().p("AAbA4Ig0hFIAABFIgaAAIAAhvIAYAAIA2BHIAAhHIAZAAIAABvg");
	this.shape_58.setTransform(-435.3,-14.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53,p:{x:-422.5}},{t:this.shape_52,p:{x:-402.4}},{t:this.shape_51},{t:this.shape_50,p:{x:-357.9}}]},13).to({state:[{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_50,p:{x:-422.7}}]},5).to({state:[{t:this.shape_56},{t:this.shape_55},{t:this.shape_53,p:{x:-478.9}},{t:this.shape_52,p:{x:-458.8}},{t:this.shape_58},{t:this.shape_57}]},5).to({state:[]},11).to({state:[]},46).wait(41));

	// Layer_11
	this.instance_5 = new lib.masqueTexte("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(-340,105.8,0.85,0.85,0,0,0,0.1,108.8);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(37).to({_off:false},0).to({_off:true},40).wait(3).to({_off:false,rotation:180},0).to({_off:true},40).wait(1));

	// _TEXTE_PRESENTATION
	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#FFFFFF").s().p("Ah+CMIAAkXIBjAAQBKAAAoAlQAoAkAABCQAABAgnAmQgnAmhPAAgAg/BUIAnAAQAqAAAXgVQAXgWAAgpQAAgogXgWQgXgWgvAAIgiAAg");
	this.shape_59.setTransform(-423.3,179);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#FFFFFF").s().p("AhjBtIAagyQAnAcAfAAQASAAAMgJQANgJAAgSQAAgRgPgKQgNgLgYAAQgOAAgXAIIAAgtIA0g/IhWAAIAAg1ICqAAIAAArIg6BBQAiAGASAWQATAWAAAdQAAAsgeAZQgeAZgtAAQguAAgwggg");
	this.shape_60.setTransform(-451.6,179.1);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#FFFFFF").s().p("AAoBtIAAh4QAAgygkAAQgRAAgNANQgOANAAAZIAAB3Ig7AAIAAjWIA7AAIAAAYQAbgbAhAAQAjAAAWAYQAYAYAAAnIAACCg");
	this.shape_61.setTransform(-486.2,182);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAhgeQAggeArAAQAtAAAfAbQAfAbAAAuIAAAfIicAAQADASAQALQAPALATAAQAfAAAVgVIAhAlQgkAigxAAQgwAAgggegAgdgyQgPALgDATIBeAAQgCgUgMgKQgNgKgRAAQgRAAgPAKg");
	this.shape_62.setTransform(-511.4,182.2);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#FFFFFF").s().p("AgqBmQgagIgVgRIAagmQAiAaAlAAQAMAAAGgEQAIgFgBgHQAAgIgJgGQgJgHgRgFIgbgKQgKgEgLgIQgZgOAAgeQAAgdAYgTQAYgTAlAAQAmAAAmAaIgXAoQgcgUgcAAQgaAAAAAQQAAAIAJAFQAIAEAUAHIAdAKQAJADAMAHQAWANAAAgQAAAggXATQgYATgkAAQgXAAgagJg");
	this.shape_63.setTransform(-247.2,133.4);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAggeQAhgeAsAAQAsAAAfAbQAfAbAAAuIAAAfIicAAQADASAPALQAQALATAAQAfAAAVgVIAiAlQglAigxAAQgwAAgggegAgdgyQgPALgDATIBeAAQgCgUgNgKQgMgKgRAAQgSAAgOAKg");
	this.shape_64.setTransform(-269.5,133.4);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#FFFFFF").s().p("AgfB5QgWgUAAgkIAAhiIgZAAIAAgrIAZAAIAAhAIA7AAIAABAIA0AAIAAArIg0AAIAABgQAAAMAHAHQAGAHAJAAQARAAAMgPIAWAqQgcAZgfAAQgeAAgVgUg");
	this.shape_65.setTransform(-290.2,130.4);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#FFFFFF").s().p("AggB5QgVgUAAgkIAAhiIgZAAIAAgrIAZAAIAAhAIA8AAIAABAIAzAAIAAArIgzAAIAABgQgBAMAHAHQAGAHAJAAQARAAALgPIAXAqQgcAZgfAAQgeAAgWgUg");
	this.shape_66.setTransform(-307.8,130.4);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAhgeQAggeArAAQAtAAAfAbQAfAbAAAuIAAAfIicAAQADASAQALQAPALATAAQAfAAAVgVIAhAlQgkAigxAAQgwAAgggegAgdgyQgPALgCATIBdAAQgCgUgMgKQgNgKgSAAQgRAAgOAKg");
	this.shape_67.setTransform(-329.4,133.4);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#FFFFFF").s().p("AhMBXQgYgWAAgsIAAiBIA9AAIAAB0QAAAxAkAAQARAAANgMQANgNAAgZIAAhzIA9AAIAADWIg9AAIAAgcQgWAfgiAAQgkAAgYgWg");
	this.shape_68.setTransform(-354.8,133.6);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#FFFFFF").s().p("AAyCUIAAhfQgZAVggAAQgpAAgfgfQgfggAAgvQAAgxAegfQAegfAlAAQAkAAAbAbIAAgYIA9AAIAAEkgAgihQQgPASAAAaQAAAaAPAQQAQARATAAQAVAAAPgRQAOgQAAgaQAAgagOgSQgPgRgVAAQgUAAgPARg");
	this.shape_69.setTransform(-381.4,137.2);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#FFFFFF").s().p("AgqBmQgagIgVgRIAbgmQAhAaAmAAQALAAAHgEQAGgFAAgHQAAgIgJgGQgKgHgQgFIgcgKQgJgEgLgIQgZgOAAgeQAAgdAYgTQAYgTAlAAQAmAAAmAaIgWAoQgcgUgdAAQgaAAAAAQQAAAIAJAFQAJAEATAHIAdAKQAIADAMAHQAXANAAAgQAAAggYATQgXATgkAAQgXAAgagJg");
	this.shape_70.setTransform(-404.9,133.4);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#FFFFFF").s().p("AhJBbQgXgUAAghQAAghAYgPQAYgQAqAAIArAAIAAgCQAAgigkAAQgPAAgRAGQgSAGgLAKIgbgnQAqgfA2AAQAnAAAZAUQAYATAAArIAACIIg4AAIAAgaQgYAdggAAQgjAAgXgUgAgkAmQAAALAJAHQAIAGAQAAQAQAAALgKQANgKAAgQIAAgLIglAAQgkAAAAAXg");
	this.shape_71.setTransform(-427.3,133.4);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#FFFFFF").s().p("AhEBQQgigeAAgyQAAgwAigfQAigfAtAAQAXAAAYAKQAYAJASATIgfApQgLgNgPgGQgOgHgPAAQgXAAgRAPQgRAQAAAaQAAAbARAQQARAPAWAAQAeAAAYgeIAkAoQgqArgwAAQgwAAghgfg");
	this.shape_72.setTransform(-450.1,133.4);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAggeQAhgeAsAAQAsAAAfAbQAfAbAAAuIAAAfIicAAQADASAPALQAQALATAAQAfAAAVgVIAhAlQgkAigxAAQgwAAgggegAgdgyQgPALgDATIBeAAQgCgUgNgKQgMgKgRAAQgSAAgOAKg");
	this.shape_73.setTransform(-484.7,133.4);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#FFFFFF").s().p("AhQB2QgeggAAgwQAAgwAdgeQAegeAngBQAmAAAZAcIAAhqIA8AAIAAEoIg8AAIAAgcQgaAfglAAQgmAAgegggAgigDQgPARAAAaQAAAaAPARQAQAQATAAQAVAAAPgQQAOgRAAgaQAAgagOgRQgPgSgVAAQgUAAgPASg");
	this.shape_74.setTransform(-511.1,129.5);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#FFFFFF").s().p("AAoBtIAAh4QABgyglAAQgRAAgNANQgOANAAAZIAAB3Ig7AAIAAjWIA7AAIAAAYQAcgbAgAAQAjAAAXAYQAWAYAAAnIAACCg");
	this.shape_75.setTransform(-198.5,84.5);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#FFFFFF").s().p("AhQBPQghggAAgvQAAguAhggQAgggAwAAQAxAAAgAgQAhAgAAAuQAAAvghAgQggAggxAAQgwAAgggggAglgqQgPARAAAZQAAAbAPAQQAPARAWAAQAXAAAPgRQAPgQAAgbQAAgZgPgRQgPgRgXAAQgWAAgPARg");
	this.shape_76.setTransform(-224.4,84.7);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#FFFFFF").s().p("AgdCXIAAjWIA7AAIAADWgAgYhaQgKgKAAgPQAAgPAKgKQAKgKAOAAQAPAAAKAKQAKAKAAAPQAAAPgKAKQgKAKgPAAQgOAAgKgKg");
	this.shape_77.setTransform(-243.2,80.3);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("#FFFFFF").s().p("AgfB5QgWgUAAgkIAAhiIgZAAIAAgrIAZAAIAAhAIA8AAIAABAIA0AAIAAArIg0AAIAABgQAAAMAGAHQAHAHAIAAQARAAALgPIAXAqQgcAZgfAAQgeAAgVgUg");
	this.shape_78.setTransform(-257.2,81.6);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("#FFFFFF").s().p("AhJBbQgXgUAAghQAAghAYgPQAYgQApAAIAsAAIAAgCQAAgigkAAQgPAAgRAGQgRAGgNAKIgbgnQArgfA1AAQAnAAAZAUQAZATAAArIAACIIg4AAIAAgaQgYAdghAAQghAAgYgUgAgkAmQAAALAJAHQAIAGARAAQAPAAAMgKQAMgKAAgQIAAgLIglAAQgkAAAAAXg");
	this.shape_79.setTransform(-278.9,84.7);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f("#FFFFFF").s().p("AgqBmQgagIgVgRIAbgmQAhAaAmAAQALAAAHgEQAGgFABgHQAAgIgKgGQgKgHgQgFIgbgKQgKgEgMgIQgYgOAAgeQAAgdAYgTQAYgTAlAAQAmAAAmAaIgXAoQgbgUgcAAQgbAAAAAQQAAAIAJAFQAJAEATAHIAdAKQAJADALAHQAXANAAAgQAAAggYATQgXATgkAAQgXAAgagJg");
	this.shape_80.setTransform(-301.1,84.7);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("#FFFFFF").s().p("AgdCXIAAjWIA7AAIAADWgAgYhaQgKgKAAgPQAAgPAKgKQAKgKAOAAQAPAAAKAKQAKAKAAAPQAAAPgKAKQgKAKgPAAQgOAAgKgKg");
	this.shape_81.setTransform(-317,80.3);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f("#FFFFFF").s().p("AgdCVIAAkoIA7AAIAAEog");
	this.shape_82.setTransform(-328.6,80.6);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("#FFFFFF").s().p("AhJBbQgXgUAAghQAAghAYgPQAYgQApAAIAsAAIAAgCQAAgigkAAQgPAAgRAGQgRAGgNAKIgbgnQArgfA1AAQAnAAAZAUQAZATAAArIAACIIg4AAIAAgaQgYAdghAAQghAAgYgUgAgkAmQAAALAIAHQAKAGAQAAQAOAAANgKQAMgKAAgQIAAgLIglAAQgkAAAAAXg");
	this.shape_83.setTransform(-346.8,84.7);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f("#FFFFFF").s().p("AAoBtIAAh4QABgyglAAQgRAAgNANQgOANAAAZIAAB3Ig7AAIAAjWIA7AAIAAAYQAcgbAgAAQAjAAAXAYQAWAYAAAnIAACCg");
	this.shape_84.setTransform(-370.8,84.5);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f("#FFFFFF").s().p("AApBtIAAh4QAAgyglAAQgRAAgNANQgNANAAAZIAAB3Ig8AAIAAjWIA8AAIAAAYQAbgbAgAAQAjAAAXAYQAWAYAAAnIAACCg");
	this.shape_85.setTransform(-396.4,84.5);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f("#FFFFFF").s().p("AhQBPQghggAAgvQAAguAhggQAgggAwAAQAxAAAgAgQAhAgAAAuQAAAvghAgQggAggxAAQgwAAgggggAglgqQgPARAAAZQAAAbAPAQQAPARAWAAQAXAAAPgRQAPgQAAgbQAAgZgPgRQgPgRgXAAQgWAAgPARg");
	this.shape_86.setTransform(-422.3,84.7);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f("#FFFFFF").s().p("AgqBmQgagIgVgRIAbgmQAhAaAmAAQALAAAHgEQAGgFABgHQAAgIgKgGQgKgHgQgFIgbgKQgKgEgMgIQgYgOAAgeQAAgdAYgTQAYgTAlAAQAmAAAmAaIgXAoQgbgUgcAAQgbAAAAAQQAAAIAJAFQAJAEATAHIAdAKQAJADALAHQAXANAAAgQAAAggYATQgXATgkAAQgXAAgagJg");
	this.shape_87.setTransform(-446,84.7);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f("#FFFFFF").s().p("Ag9BtIAAjWIA8AAIAAAdQAKgOASgJQARgJASAAIAAA4IgLAAQgbAAgNATQgMATAAAeIAABdg");
	this.shape_88.setTransform(-463.4,84.5);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAggeQAhgeAsAAQAsAAAfAbQAfAbAAAuIAAAfIicAAQADASAPALQAQALATAAQAfAAAVgVIAhAlQgkAigxAAQgwAAgggegAgdgyQgPALgDATIBeAAQgCgUgNgKQgMgKgRAAQgSAAgOAKg");
	this.shape_89.setTransform(-484.7,84.7);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f("#FFFFFF").s().p("AhuCUIAAkjIA8AAIAAAXQAcgbAkAAQAmAAAdAfQAeAgAAAvQAAAvgeAgQgdAfgnABQgngBgYgfIAABqgAgkhPQgPARAAAaQAAAbAPAPQAPAQAUAAQAUAAAQgQQAPgPAAgaQAAgbgPgRQgPgSgUAAQgVAAgPASg");
	this.shape_90.setTransform(-509.9,88.4);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAggeQAhgeAsAAQAsAAAfAbQAfAbAAAuIAAAfIicAAQADASAPALQAQALATAAQAfAAAVgVIAiAlQglAigxAAQgwAAgggegAgdgyQgPALgDATIBeAAQgCgUgNgKQgMgKgRAAQgRAAgPAKg");
	this.shape_91.setTransform(-269.9,35.9);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f("#FFFFFF").s().p("AhQB2QgeghAAgwQAAguAdggQAegeAnAAQAmABAZAbIAAhqIA8AAIAAEoIg8AAIAAgdQgaAgglAAQgmAAgegggAgigDQgPARAAAaQAAAaAPAQQAQARATAAQAVAAAPgRQAOgQAAgbQAAgZgOgRQgPgRgVAAQgUAAgPARg");
	this.shape_92.setTransform(-296.3,32);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.f("#FFFFFF").s().p("AgyB2IAAAdIg8AAIAAkoIA8AAIAABqQAZgbAmgBQAnAAAdAeQAeAgAAAuQAAAwgeAhQgeAggmAAQgmAAgZgggAgkgDQgPARAAAZQAAAbAPAQQAPARAUAAQAUAAAQgRQAPgQAAgaQAAgagPgRQgPgRgUAAQgVAAgPARg");
	this.shape_93.setTransform(-332.2,32);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAhgeQAggeArAAQAtAAAfAbQAfAbAAAuIAAAfIicAAQADASAQALQAPALATAAQAfAAAVgVIAhAlQgkAigxAAQgwAAgggegAgdgyQgPALgDATIBeAAQgCgUgMgKQgNgKgRAAQgRAAgPAKg");
	this.shape_94.setTransform(-358.4,35.9);

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.f("#FFFFFF").s().p("AAoBsIgoh0IgnB0Ig+AAIhJjXIA+AAIApCBIAqiBIA6AAIApCBIAriBIA+AAIhJDXg");
	this.shape_95.setTransform(-388.9,36);

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.f("#FFFFFF").s().p("AgdCXIAAjWIA7AAIAADWgAgYhaQgKgKAAgPQAAgPAKgKQAKgKAOAAQAPAAAKAKQAKAKAAAPQAAAPgKAKQgKAKgPAAQgOAAgKgKg");
	this.shape_96.setTransform(-423.3,31.6);

	this.shape_97 = new cjs.Shape();
	this.shape_97.graphics.f("#FFFFFF").s().p("AgdCVIAAkoIA7AAIAAEog");
	this.shape_97.setTransform(-434.9,31.9);

	this.shape_98 = new cjs.Shape();
	this.shape_98.graphics.f("#FFFFFF").s().p("AhuCUIAAkkIA8AAIAAAZQAcgcAkAAQAmAAAdAfQAeAgAAAvQAAAvgeAgQgdAggngBQgnABgYggIAABqgAgkhQQgPASAAAaQAAAaAPAQQAPAQAUABQAUgBAQgQQAPgPAAgbQAAgagPgSQgPgRgUAAQgVAAgPARg");
	this.shape_98.setTransform(-453.5,39.7);

	this.shape_99 = new cjs.Shape();
	this.shape_99.graphics.f("#FFFFFF").s().p("AhuCUIAAkkIA8AAIAAAZQAcgcAkAAQAmAAAdAfQAeAgAAAvQAAAvgeAgQgdAggngBQgnABgYggIAABqgAgkhQQgPASAAAaQAAAaAPAQQAPAQAUABQAUgBAQgQQAPgPAAgbQAAgagPgSQgPgRgUAAQgVAAgPARg");
	this.shape_99.setTransform(-480.1,39.7);

	this.shape_100 = new cjs.Shape();
	this.shape_100.graphics.f("#FFFFFF").s().p("ABVCMIgbg9Ih0AAIgaA9IhCAAIB5kXIA8AAIB4EXgAgiAZIBFAAIgjhRg");
	this.shape_100.setTransform(-508.9,32.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_100},{t:this.shape_99},{t:this.shape_98},{t:this.shape_97},{t:this.shape_96},{t:this.shape_95},{t:this.shape_94},{t:this.shape_93},{t:this.shape_92},{t:this.shape_91},{t:this.shape_90},{t:this.shape_89},{t:this.shape_88},{t:this.shape_87},{t:this.shape_86},{t:this.shape_85},{t:this.shape_84},{t:this.shape_83},{t:this.shape_82},{t:this.shape_81},{t:this.shape_80},{t:this.shape_79},{t:this.shape_78},{t:this.shape_77},{t:this.shape_76},{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59}]},57).to({state:[]},43).to({state:[]},1).wait(20));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(221.1,-172,2,16);


// stage content:
(lib.FS_projet_headict_mobile = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{video:179,"close":515});

	// timeline functions:
	this.frame_0 = function() {
		//variable necessaire pour la fermeture des textes
		this.hiddentexts = false;
		
		//variable necessaire pour la fermeture de la vidéo
		this.hiddenvideo = true;
		
		//variable necessaire pour la fermeture du visuel de fond
		this.hiddenvisuel = false;
		
		//variable necessaire pour la fermeture du bouton play
		this.hiddenplay = true;
	}
	this.frame_139 = function() {
		//var frequency = 60;
		//stage.enableMouseOver(frequency);
		
		// EVENT DE CLIC POUR LA SUITE
		this.next_btn.addEventListener("click", fl_ClickToGoToAndPlayFromFrame.bind(this));
		this.next_btn.addEventListener("mouseover", fl_MouseOverHandler.bind(this));
		this.next_btn.addEventListener("mouseout", fl_MouseOutHandler.bind(this));
		
		
		function fl_ClickToGoToAndPlayFromFrame()
		{
			//this.gotoAndPlay("close");
			
			var event = new Event('next');
			this.dispatchEvent(event);		
			event = null;
			
			this.next_btn.btnNext.gotoAndPlay('rollOut');
			this.lignes_mc.gotoAndPlay("close");
			this.next_btn.gotoAndPlay("close");
			this.cta_mc.gotoAndPlay("close");
			
		}
		function fl_MouseOverHandler(){	this.next_btn.btnNext.gotoAndPlay('rollOver'); }
		function fl_MouseOutHandler(){	this.next_btn.btnNext.gotoAndPlay('rollOut'); }
		
		
		// EVENT DE CLIC POUR LE LIEN
		this.cta_mc.addEventListener("click", projectDetails.bind(this));
		this.cta_mc.addEventListener("mouseover", pDetails_MouseOverHandler.bind(this));
		this.cta_mc.addEventListener("mouseout", pDetails_MouseOutHandler.bind(this));
		
		function projectDetails()
		{	
			var event = new Event('details');
			this.dispatchEvent(event);		
			event = null;
			
			this.cta_mc.btnCta.gotoAndPlay('rollOut');
			
		}
		function pDetails_MouseOverHandler(){	this.cta_mc.btnCta.gotoAndPlay('rollOver'); }
		function pDetails_MouseOutHandler(){	this.cta_mc.btnCta.gotoAndPlay('rollOut'); }
		
		
		// EVENT DE CLIC POUR LA VIDEO
		this.video_mc.btn_playVideo.addEventListener("click", playVideo.bind(this));
		
		function playVideo()
		{	
			var event = new Event('video');
			this.dispatchEvent(event);		
			event = null;
			
			this.hiddenplay = true;	
			this.video_mc.btn_playVideo.gotoAndPlay('close');
		}
	}
	this.frame_179 = function() {
		this.visuel_mc.gotoAndPlay("close");
		this.video_mc.gotoAndPlay("open");
		
		this.hiddenvideo = false;
		this.hiddenvisuel = true;
		this.hiddenplay = false;
		
		//On envoie l'information de l'affichage de la video
		var event = new Event('videoStarter');
		this.dispatchEvent(event);		
		event = null;
	}
	this.frame_339 = function() {
		this.textes_mc.gotoAndPlay("close");
		this.hiddentexts = true;
	}
	this.frame_514 = function() {
		//PAUSE
		this.stop();
	}
	this.frame_516 = function() {
		this.cta_mc.gotoAndPlay("close");
		this.next_btn.gotoAndPlay("close");
		
		
		if(this.hiddenvisuel==false){
			this.visuel_mc.gotoAndPlay("close");
		}
		
		if(this.hiddenvideo==false){
			this.video_mc.gotoAndPlay("close");
		}
		
		if(this.hiddentexts==false){
			this.textes_mc.gotoAndPlay("close");
			this.hiddentexts = true;
		}
		
		if(this.hiddenplay==false){
			this.video_mc.btn_playVideo.gotoAndPlay('close');
		}
	}
	this.frame_535 = function() {
		//this.lignes_mc.gotoAndPlay("close");
	}
	this.frame_562 = function() {
		// EVENT DE FIN D'ANIM
		var event = new Event('closed');
		this.dispatchEvent(event);		
		event = null;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(139).call(this.frame_139).wait(40).call(this.frame_179).wait(160).call(this.frame_339).wait(175).call(this.frame_514).wait(2).call(this.frame_516).wait(19).call(this.frame_535).wait(27).call(this.frame_562).wait(1));

	// CTA NEXT
	this.next_btn = new lib.bloc_NEXT_btn();
	this.next_btn.name = "next_btn";
	this.next_btn.parent = this;
	this.next_btn.setTransform(359.2,861.2,1.06,1.06,0,0,0,-340.4,403.9);
	this.next_btn._off = true;

	this.timeline.addTween(cjs.Tween.get(this.next_btn).wait(128).to({_off:false},0).wait(435));

	// CTA DETAIL
	this.cta_mc = new lib.bloc_CTA();
	this.cta_mc.name = "cta_mc";
	this.cta_mc.parent = this;
	this.cta_mc.setTransform(120.2,861.4,1.06,1.06,0,0,0,-340.4,404.1);
	this.cta_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.cta_mc).wait(119).to({_off:false},0).to({_off:true},420).wait(24));

	// BLOC INFOS TEXTE
	this.textes_mc = new lib.blocInfostextes();
	this.textes_mc.name = "textes_mc";
	this.textes_mc.parent = this;
	this.textes_mc.setTransform(788.2,242,1,1,0,0,0,222.1,-164);
	this.textes_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.textes_mc).wait(17).to({_off:false},0).wait(546));

	// VIDEO
	this.video_mc = new lib.bloc_video();
	this.video_mc.name = "video_mc";
	this.video_mc.parent = this;
	this.video_mc.setTransform(238.9,1185.2,1.05,1.05,0,0,0,227.2,765);

	this.timeline.addTween(cjs.Tween.get(this.video_mc).to({_off:true},539).wait(24));

	// VISUEL
	this.visuel_mc = new lib.bloc_visuel();
	this.visuel_mc.name = "visuel_mc";
	this.visuel_mc.parent = this;
	this.visuel_mc.setTransform(1030.6,381.5,1,1,0,0,0,552.5,26.5);
	this.visuel_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.visuel_mc).wait(17).to({_off:false},0).wait(546));

	// lignes
	this.lignes_mc = new lib.bloc_lignes();
	this.lignes_mc.name = "lignes_mc";
	this.lignes_mc.parent = this;
	this.lignes_mc.setTransform(240.8,453.9,0.525,1,0,0,0,0.7,405.9);

	this.timeline.addTween(cjs.Tween.get(this.lignes_mc).wait(563));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(241.3,293.5,478,1643.4);
// library properties:
lib.properties = {
	id: 'B3A90A39D0EB79409035B03CFE084EDF',
	width: 480,
	height: 840,
	fps: 60,
	color: "#000000",
	opacity: 1.00,
	manifest: [
		{src:"images/casquetteHeadict.png?1540809349081", id:"casquetteHeadict"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['B3A90A39D0EB79409035B03CFE084EDF'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;