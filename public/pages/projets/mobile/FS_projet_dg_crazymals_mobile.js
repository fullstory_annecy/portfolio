(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.crazymals_1 = function() {
	this.initialize(img.crazymals_1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,241,452);


(lib.crazymals_cordon = function() {
	this.initialize(img.crazymals_cordon);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,608,618);


(lib.crazymals_singe = function() {
	this.initialize(img.crazymals_singe);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,285,361);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.video = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#080808").s().p("EgtTBLAMAAAiV/MBanAAAMAAACV/g");
	this.shape.setTransform(290,480);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.video, new cjs.Rectangle(0,0,580,960), null);


(lib.txt_hashtag = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAhAxIAAg9IgbA1IgMAAIgag1IAAA9IgWAAIAAhhIAeAAIAYA1IAZg1IAeAAIAABhg");
	this.shape.setTransform(37.5,52.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAeAxIgKgVIgoAAIgJAVIgXAAIAqhhIAVAAIAqBhgAgLAJIAXAAIgMgcg");
	this.shape_1.setTransform(25.6,52.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AAQAxIgVgfIgPAAIAAAfIgWAAIAAhhIAlAAQAWAAAKAIQAKAIAAARQAAAWgTAHIAZAjgAgUAAIAQAAQAKAAAEgEQAEgEAAgHQAAgIgEgDQgEgDgKAAIgQAAg");
	this.shape_2.setTransform(15.7,52.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgeAkQgOgOAAgWQgBgVAQgOQAOgPAVAAQAUAAAQAOIgMARQgGgGgFgCQgGgCgGAAQgMgBgJAJQgIAJAAAMQAAAOAIAJQAJAIAJAAQAMAAAIgFIAAgaIAVAAIAAAjQgOAQgZAAQgWAAgOgPg");
	this.shape_3.setTransform(4.9,52.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAeAxIgKgVIgoAAIgJAVIgXAAIAqhhIAVAAIAqBhgAgLAJIAXAAIgMgcg");
	this.shape_4.setTransform(-5.5,52.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgKAxIAAhOIgbAAIAAgTIBLAAIAAATIgbAAIAABOg");
	this.shape_5.setTransform(-15,52.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgmAiIANgQQAQAOANAAQAGgBADgDQADgCAAgFQAAgEgDgCQgEgDgKgDQgRgDgIgGQgIgGAAgOQAAgPALgHQAKgIANAAQAKAAAKAEQAKADAHAHIgLAQQgNgKgNAAQgFAAgDACQgDADAAAEQAAAEAEADQAEACANAEQAOADAHAGQAIAHAAANQAAANgKAJQgKAHgQABQgWAAgTgRg");
	this.shape_6.setTransform(-23.8,52.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AAXAxIgtg9IAAA9IgWAAIAAhhIAUAAIAvA+IAAg+IAWAAIAABhg");
	this.shape_7.setTransform(-33.9,52.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgKAxIAAhhIAVAAIAABhg");
	this.shape_8.setTransform(-41.9,52.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgTIANAAIADgRIgPAAIADgUIAPAAIAEgWIAUAAIgDAWIAMAAIADgWIAWAAIgEAWIAOAAIgDAUIgPAAIgDARIARAAIgEATIgQAAIgDAVgAgHAKIALAAIAEgRIgMAAg");
	this.shape_9.setTransform(-49.5,52.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AAXAxIgtg9IAAA9IgWAAIAAhhIAUAAIAvA+IAAg+IAWAAIAABhg");
	this.shape_10.setTransform(70.1,31.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgeAkQgPgOAAgWQABgUAPgPQAOgOAVAAQAUAAAQANIgLARQgHgGgGgDQgFgBgGAAQgMAAgIAIQgJAJAAAMQAAAOAIAJQAJAIAKAAQALAAAIgFIAAgaIAVAAIAAAiQgOAQgZABQgWgBgOgOg");
	this.shape_11.setTransform(59.1,31.7);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgKAxIAAhhIAVAAIAABhg");
	this.shape_12.setTransform(51.6,31.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgmAiIANgRQAQAPANAAQAGAAADgEQADgCAAgEQAAgFgDgCQgEgDgKgDQgRgEgIgFQgIgHAAgNQAAgPALgHQAKgIANABQAKAAAKADQAKADAHAGIgLARQgNgKgNAAQgFAAgDACQgDADAAAEQAAAFAEACQAEACANAEQAOADAHAGQAIAHAAANQAAANgKAJQgKAHgQABQgWgBgTgQg");
	this.shape_13.setTransform(44.7,31.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBFAAIAAATIgvAAIAAAVIArAAIAAASIgrAAIAAATIAxAAIAAAUg");
	this.shape_14.setTransform(36,31.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgrAxIAAhhIAiAAQAaAAAOANQANAMAAAXQAAAWgNAOQgOANgbAAgAgVAdIANAAQAPAAAHgHQAJgIAAgOQAAgOgJgHQgHgIgRAAIgLAAg");
	this.shape_15.setTransform(26.1,31.7);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AAXAxIgtg9IAAA9IgWAAIAAhhIAUAAIAvA+IAAg+IAWAAIAABhg");
	this.shape_16.setTransform(11,31.7);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgjAkQgPgPAAgVQAAgVAPgOQAOgOAVAAQAWAAAPAOQAOAOAAAVQAAAVgOAPQgPAPgWAAQgVAAgOgPgAgTgVQgJAJAAAMQAAANAJAKQAIAIALABQAMgBAJgIQAIgKAAgNQAAgMgIgJQgJgJgMAAQgLAAgIAJg");
	this.shape_17.setTransform(-0.5,31.7);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgKAxIAAhhIAVAAIAABhg");
	this.shape_18.setTransform(-8.5,31.7);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgKAxIAAhOIgcAAIAAgTIBMAAIAAATIgbAAIAABOg");
	this.shape_19.setTransform(-15.1,31.7);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgjAkQgPgPAAgVQAAgVAPgOQAOgOAVAAQAWAAAPAOQAOAOAAAVQAAAVgOAPQgPAPgWAAQgVAAgOgPgAgTgVQgJAJAAAMQAAANAJAKQAIAIALABQAMgBAJgIQAIgKAAgNQAAgMgIgJQgJgJgMAAQgLAAgIAJg");
	this.shape_20.setTransform(-25.1,31.7);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AAhAxIAAg9IgbA1IgMAAIgag1IAAA9IgWAAIAAhhIAeAAIAYA1IAZg1IAeAAIAABhg");
	this.shape_21.setTransform(-37.6,31.7);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgTIANAAIADgRIgPAAIADgTIAPAAIAEgXIAUAAIgDAXIAMAAIADgXIAWAAIgEAXIAOAAIgDATIgPAAIgDARIARAAIgEATIgQAAIgDAVgAgHAKIALAAIAEgRIgMAAg");
	this.shape_22.setTransform(-49.5,31.6);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgmAhIANgQQAQAOANABQAGAAADgEQADgCAAgEQAAgFgDgCQgEgDgKgDQgRgEgIgFQgIgHAAgNQAAgOALgIQAKgIANABQAKgBAKAEQAKADAHAGIgLARQgNgLgNABQgFgBgDADQgDADAAAEQAAAFAEACQAEADANADQAOADAHAGQAIAHAAANQAAANgKAIQgKAJgQAAQgWgBgTgRg");
	this.shape_23.setTransform(41,10.6);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgfAxIAAhhIAWAAIAABNIApAAIAAAUg");
	this.shape_24.setTransform(33,10.7);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AAeAxIgKgVIgoAAIgJAVIgXAAIAqhhIAVAAIAqBhgAgLAJIAXAAIgMgcg");
	this.shape_25.setTransform(23.4,10.7);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AAhAxIAAg9IgbA1IgMAAIgag1IAAA9IgWAAIAAhhIAeAAIAYA1IAZg1IAeAAIAABhg");
	this.shape_26.setTransform(11.5,10.7);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgKAxIAAgnIgig6IAYAAIAUAkIAVgkIAYAAIgiA6IAAAng");
	this.shape_27.setTransform(0.5,10.7);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AgoAxIAAgPIAyg/IgwAAIAAgTIBOAAIAAAPIgyA/IAzAAIAAATg");
	this.shape_28.setTransform(-8.6,10.7);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AAeAxIgKgVIgoAAIgJAVIgXAAIAqhhIAVAAIAqBhgAgLAJIAXAAIgMgcg");
	this.shape_29.setTransform(-18.6,10.7);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AAQAxIgVgfIgPAAIAAAfIgWAAIAAhhIAlAAQAWAAAKAIQAKAHAAARQAAAXgTAHIAZAjgAgUAAIAQAAQAKAAAEgEQAEgDAAgJQAAgHgEgDQgEgDgKAAIgQAAg");
	this.shape_30.setTransform(-28.5,10.7);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AgeAkQgPgOAAgWQAAgUAPgPQAPgOAUAAQAZgBAQATIgOAPQgKgMgQAAQgLgBgJAJQgJAHAAANQABAOAIAIQAJAIAKAAQAQAAAKgNIAPAPQgSATgWAAQgWgBgOgOg");
	this.shape_31.setTransform(-39,10.6);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgTIANAAIADgRIgPAAIADgTIAPAAIAEgXIAUAAIgDAXIAMAAIADgXIAWAAIgEAXIAOAAIgDATIgPAAIgDARIARAAIgEATIgQAAIgDAVgAgHAKIALAAIAEgRIgMAAg");
	this.shape_32.setTransform(-49.5,10.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.txt_hashtag, new cjs.Rectangle(-56.7,0,134.8,63.2), null);


(lib.project_illustration_placeHolder = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// crazymals_1.png
	this.instance = new lib.crazymals_1();
	this.instance.parent = this;
	this.instance.setTransform(-92,167,0.6,0.6);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(89));

	// crazymals_cordon.png
	this.instance_1 = new lib.crazymals_cordon();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-202,-172,0.6,0.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(89));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-202,-172,364.8,610.2);


(lib.playArrow = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(2,1,1).p("AiOiMIAAEZIEdiIg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AiOiMIEdCRIkdCIg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.playArrow, new cjs.Rectangle(-15.3,-15.1,30.7,30.2), null);


(lib.masqueTexte_1_ligne = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("EgicAEJIAAoRMBE5AAAIAAIRg");
	this.shape.setTransform(0,24.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.masqueTexte_1_ligne, new cjs.Rectangle(-220.5,-1.9,441,53), null);


(lib.masque_generique = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#00CD6A").s().p("AsQCqIAAlTIYhAAIAAFTg");
	this.shape.setTransform(78.5,17);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.masque_generique, new cjs.Rectangle(0,0,157,34), null);


(lib.line_explode = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_15 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(15).call(this.frame_15).wait(1));

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(1,1,1).p("AgHAAIAPAA");
	this.shape.setTransform(0.8,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#FFFFFF").ss(1,1,1).p("AgRAAIAjAA");
	this.shape_1.setTransform(2.9,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#FFFFFF").ss(1,1,1).p("AgaAAIA2AA");
	this.shape_2.setTransform(5.1,0);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#FFFFFF").ss(1,1,1).p("AglAAIBLAA");
	this.shape_3.setTransform(7.3,0);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#FFFFFF").ss(1,1,1).p("AgnAAIBPAA");
	this.shape_4.setTransform(8.8,0);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#FFFFFF").ss(1,1,1).p("AgpAAIBTAA");
	this.shape_5.setTransform(10.4,0);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#FFFFFF").ss(1,1,1).p("AgrAAIBXAA");
	this.shape_6.setTransform(12,0);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#FFFFFF").ss(1,1,1).p("AgtAAIBbAA");
	this.shape_7.setTransform(13.6,0);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#FFFFFF").ss(1,1,1).p("AgvAAIBfAA");
	this.shape_8.setTransform(15.1,0);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#FFFFFF").ss(1,1,1).p("AgxAAIBjAA");
	this.shape_9.setTransform(16.7,0);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#FFFFFF").ss(1,1,1).p("Ag0AAIBpAA");
	this.shape_10.setTransform(18.3,0);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#FFFFFF").ss(1,1,1).p("AgWAAIAtAA");
	this.shape_11.setTransform(25.8,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape,p:{x:0.8}}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3,p:{x:7.3}}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_3,p:{x:22}}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape,p:{x:29.5}}]},1).to({state:[]},1).to({state:[]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1,-1,13,2);


(lib.ligne = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(49));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#333333").ss(2,1,1).p("EgABhM/MAADCZ/");
	this.shape.setTransform(-0.2,319.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#333333").ss(2,1,1).p("EAAghM7Qg1OzgbRBQgbUYAEVMQADWBAkVVQAlTkBERl");
	this.shape_1.setTransform(-7,319.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#333333").ss(2,1,1).p("EAA7hM5QhfN/gxR5QgxUHAHVhQAGWSA/VCQBDUPB4Qw");
	this.shape_2.setTransform(-12.4,319.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#333333").ss(2,1,1).p("EABQhM3Qh/NXhDSkQhAT5AIVyQAIWfBUU0QBZUwChQG");
	this.shape_3.setTransform(-16.5,320.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#333333").ss(2,1,1).p("EABfhM1QiXM4hPTEQhMTvAKV+QAJWpBjUqQBqVIC+Pn");
	this.shape_4.setTransform(-19.6,320.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#333333").ss(2,1,1).p("EABqhM0QioMjhYTaQhVToALWHQALWwBuUiQB1VaDUPR");
	this.shape_5.setTransform(-21.8,320.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#333333").ss(2,1,1).p("EABxhMzQizMUheTqQhbTjAMWNQALW1B2UdQB9VmDjPB");
	this.shape_6.setTransform(-23.3,320.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#333333").ss(2,1,1).p("EAB2hMzQi7MKhiT1QhfTgAMWRQAMW3B7UaQCDVvDsO3");
	this.shape_7.setTransform(-24.2,320.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#333333").ss(2,1,1).p("EAB5hMyQjAMEhkT7QhhTeAMWTQAMW5B+UYQCGVzDyOx");
	this.shape_8.setTransform(-24.8,320.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#333333").ss(2,1,1).p("EAB7hMyQjCMBhmT+QhjTdANWVQAMW6CAUWQCHV2D2Ou");
	this.shape_9.setTransform(-25.2,320.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#333333").ss(2,1,1).p("EAB8hMyQjEL/hmUAQhjTcAMWWQANW7CAUWQCIV3D3Os");
	this.shape_10.setTransform(-25.3,320.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#333333").ss(2,1,1).p("EAB8hMyQjEL/hnUAQhjTcANWWQAMW7CAUWQCJV3D4Os");
	this.shape_11.setTransform(-25.4,320.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#333333").ss(2,1,1).p("EAB8hMyQjEL/hnUAQhjTcAMWWQANW7CAUWQCJV3D4Os");
	this.shape_12.setTransform(-25.4,320.5);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#333333").ss(2,1,1).p("EAB8hMyQjEL/hnUAQhjTcAMWWQANW7CAUWQCJV4D4Or");
	this.shape_13.setTransform(-25.4,320.5);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#333333").ss(2,1,1).p("EAB8hMyQjEL/hmUAQhkTcANWWQAMW7CBUWQCIV3D4Os");
	this.shape_14.setTransform(-25.4,320.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#333333").ss(2,1,1).p("EAB7hMyQjDMAhmT/QhjTdANWVQAMW6CAUWQCIV3D2Ot");
	this.shape_15.setTransform(-25.2,320.5);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#333333").ss(2,1,1).p("EAB6hMyQjBMChmT9QhiTdAMWVQANW6B/UXQCHV1D1Ou");
	this.shape_16.setTransform(-25.1,320.5);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#333333").ss(2,1,1).p("EAB5hMyQjAMEhkT7QhiTeANWTQAMW5B+UYQCGV0DzOw");
	this.shape_17.setTransform(-24.9,320.5);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#333333").ss(2,1,1).p("EAB4hMzQi+MHhjT5QhgTfAMWRQAMW5B8UZQCFVxDvO0");
	this.shape_18.setTransform(-24.6,320.5);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#333333").ss(2,1,1).p("EAB2hMzQi7MLhhT0QhfTgAMWRQAMW3B6UaQCDVtDrO5");
	this.shape_19.setTransform(-24.2,320.5);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#333333").ss(2,1,1).p("EABzhMzQi2MQhgTuQhcTiALWPQAMW1B4UcQB/VqDnO9");
	this.shape_20.setTransform(-23.7,320.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#333333").ss(2,1,1).p("EABwhMzQiyMWhcToQhbTkAMWMQALWzB1UeQB8VlDgPD");
	this.shape_21.setTransform(-23,320.4);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#333333").ss(2,1,1).p("EABshM0QisMehZTgQhXTmALWJQALWxBwUhQB4VeDZPM");
	this.shape_22.setTransform(-22.2,320.4);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#333333").ss(2,1,1).p("EABohM0QilMnhVTWQhUTpALWFQAKWuBsUlQBzVWDQPV");
	this.shape_23.setTransform(-21.3,320.3);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#333333").ss(2,1,1).p("EABihM1QibMyhSTKQhPTuAKWAQAKWrBnUoQBsVNDFPh");
	this.shape_24.setTransform(-20.2,320.3);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#333333").ss(2,1,1).p("EABchM1QiSM/hMS8QhKTyAKV7QAJWnBgUsQBmVDC4Pt");
	this.shape_25.setTransform(-18.9,320.2);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#333333").ss(2,1,1).p("EABUhM2QiGNNhGStQhET3AJV1QAJWiBYUyQBeU3CpP8");
	this.shape_26.setTransform(-17.4,320.1);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#333333").ss(2,1,1).p("EABMhM3Qh5Neg/ScQg9T8AIVuQAHWdBQU2QBVUqCYQO");
	this.shape_27.setTransform(-15.7,320);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#333333").ss(2,1,1).p("EABChM4QhqNxg3SIQg2UCAHVnQAHWWBGU9QBKUbCGQh");
	this.shape_28.setTransform(-13.8,319.9);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#333333").ss(2,1,1).p("EAA3hM5QhZOGguRxQguUKAGVeQAGWPA7VFQA/UJBxQ3");
	this.shape_29.setTransform(-11.7,319.8);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#333333").ss(2,1,1).p("EAAuhM6QhKOZgnRdQgmUQAFVWQAGWKAxVLQA0T6BfRK");
	this.shape_30.setTransform(-9.8,319.7);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#333333").ss(2,1,1).p("EAAlhM7Qg8OqggRLQgfUWAEVPQAEWFApVQQArTtBORb");
	this.shape_31.setTransform(-8.1,319.7);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#333333").ss(2,1,1).p("EAAehM7QgxO4gaQ8QgZUaADVJQAEWAAhVWQAjTgA/Rq");
	this.shape_32.setTransform(-6.6,319.6);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#333333").ss(2,1,1).p("EAAXhM8QgnPFgUQuQgUUeADVFQADV8AaVaQAcTWAzR3");
	this.shape_33.setTransform(-5.3,319.5);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#333333").ss(2,1,1).p("EAAShM9QgePQgRQjQgPUiACVAQADV4AVVeQAVTNAoSD");
	this.shape_34.setTransform(-4.2,319.5);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#333333").ss(2,1,1).p("EAANhM9QgXPZgMQZQgMUlACU9QACV1AQVhQARTFAeSM");
	this.shape_35.setTransform(-3.2,319.4);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#333333").ss(2,1,1).p("EAAJhM9QgRPggJQRQgJUoACU5QABVyANVkQAMS/AXSU");
	this.shape_36.setTransform(-2.4,319.4);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#333333").ss(2,1,1).p("EAAGhM+QgMPogGQKQgHUpACU3QABVwAJVmQAJS6ARSb");
	this.shape_37.setTransform(-1.8,319.3);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#333333").ss(2,1,1).p("EAADhM+QgIPsgEQFQgEUrABU1QABVuAGVoQAGS2AMSg");
	this.shape_38.setTransform(-1.3,319.3);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#333333").ss(2,1,1).p("EAABhM+QgFPwgCQAQgDUtABUzQABVtAEVqQAESyAISk");
	this.shape_39.setTransform(-0.9,319.3);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#333333").ss(2,1,1).p("EAAAhM+QgCPzgCP9QgBUuAAUxQABVtADVqQACSwAFSn");
	this.shape_40.setTransform(-0.6,319.3);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#333333").ss(2,1,1).p("EAAAhM+QgCP1gBP7QAAUuAAUxQABVsACVrQABSuADSp");
	this.shape_41.setTransform(-0.4,319.3);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#333333").ss(2,1,1).p("EgABhM/QgBP3AAP5QAAUvABUxQAAVsABVrQABStACSr");
	this.shape_42.setTransform(-0.2,319.3);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#333333").ss(2,1,1).p("EgABhM/QAAP4AAP4QAAUwABUvQAAVsAAVrQABStABSs");
	this.shape_43.setTransform(-0.2,319.3);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#333333").ss(2,1,1).p("EgABhM/QABP4AAP4QAAUwAAUvQAAVsABVrQABStAASs");
	this.shape_44.setTransform(-0.2,319.3);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#333333").ss(2,1,1).p("EgABhM/QABP4AAP4QAAUwAAUvQABVsAAVrQABStAASs");
	this.shape_45.setTransform(-0.2,319.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.3,-174.5,2.4,987.5);


(lib.lettres_FAT = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#00CD6A").s().p("AhNFeIAAkVIjymmICpAAICWEEICXkEICpAAIjyGmIAAEVg");
	this.shape.setTransform(94.1,187.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#00CD6A").s().p("AkjFeIAAhsIFsnIIlhAAIAAiHII2AAIAABsIlrHHIFyAAIAACIg");
	this.shape_1.setTransform(16.9,187.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#00CD6A").s().p("AkkAsIAAhWIJJAAIAABWg");
	this.shape_2.setTransform(-64.9,231.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#00CD6A").s().p("ABxFdIiZjfIhsAAIAADfIicAAIAAq5IEJAAQChAABGA3QBFA2ABB6QAACoiFAwICwD6gAiUgIIByAAQBOAAAegZQAdgaAAg5QAAg5gegUQgfgWhJAAIh1AAg");
	this.shape_3.setTransform(23.4,63.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#00CD6A").s().p("AjeEEQhohngBibQABibBqhoQBqhnCZAAQCsAAB0CCIhhBvQhJhdhuAAQhYAAg+A6Qg/A5AABhQAABiA8A6QA6A8BUAAQByAABJhbIBlBmQh3CGiiAAQigAAhphlg");
	this.shape_4.setTransform(-63.8,63.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.lettres_FAT, new cjs.Rectangle(-102.9,0,242.1,249.8), null);


(lib.hastaggraphicbranche = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AjQAuIAShbIDlAAICqAAIgSBbg");
	this.shape.setTransform(0,4.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.hastaggraphicbranche, new cjs.Rectangle(-20.8,0,41.7,9.2), null);


(lib.Btn_cliquable_area = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#00CD6A").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape.setTransform(0,30);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Btn_cliquable_area, new cjs.Rectangle(-113,0,226,60), null);


(lib.prject_illustration = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.project_illustration_placeHolder();
	this.instance.parent = this;
	this.instance.setTransform(-14.8,-268.3,1,1,5,0,0,-14.9,-268.3);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:-15,rotation:-5,x:-14.9},69,cjs.Ease.quadInOut).to({regX:-14.9,rotation:5,x:-14.8},70,cjs.Ease.quadInOut).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-241.9,-188.7,395.8,630.1);


(lib.masqueTexte = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// masqueTexte_1_ligne
	this.instance = new lib.masqueTexte_1_ligne();
	this.instance.parent = this;
	this.instance.setTransform(-218.8,191.5,0.008,1,0,0,0,0,25.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(6).to({_off:false},0).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.01,x:218.8},14).wait(1));

	// masqueTexte_1_ligne
	this.instance_1 = new lib.masqueTexte_1_ligne();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-218.8,136.1,0.008,1,0,0,0,0,25.5);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.01,x:218.8},14).to({_off:true},1).wait(2));

	// masqueTexte_1_ligne
	this.instance_2 = new lib.masqueTexte_1_ligne();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-218.8,80.8,0.008,1,0,0,0,0,25.5);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(2).to({_off:false},0).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.01,x:218.8},14).to({_off:true},1).wait(4));

	// masqueTexte_1_ligne
	this.instance_3 = new lib.masqueTexte_1_ligne();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-218.8,25.5,0.008,1,0,0,0,0,25.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.01,x:218.8},14).to({_off:true},1).wait(6));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-220.5,-1.9,3.5,53);


(lib.hashtag_graphic = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(64));

	// hastag graphic branche
	this.instance = new lib.hastaggraphicbranche();
	this.instance.parent = this;
	this.instance.setTransform(37.4,51.9,1.084,0.995,0,-78.3,102.1,0.1,4.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({x:6.5,y:50.1},17,cjs.Ease.quartOut).to({_off:true},5).wait(3).to({_off:false},0).to({_off:true},4).wait(2).to({_off:false,x:26.5},0).to({_off:true},5).wait(28));

	// hastag graphic branche
	this.instance_1 = new lib.hastaggraphicbranche();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-41.6,49.9,1.084,0.995,0,-78.3,102.1,0.1,4.6);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off:false},0).to({x:-7.4,y:49.8},18,cjs.Ease.quartOut).to({_off:true},4).wait(5).to({_off:false},0).to({_off:true},4).wait(31));

	// hastag graphic branche
	this.instance_2 = new lib.hastaggraphicbranche();
	this.instance_2.parent = this;
	this.instance_2.setTransform(2.7,21.7,1,1,0,0,0,0,4.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({x:-2.3,y:58.7},13,cjs.Ease.quartOut).to({_off:true},11).wait(3).to({_off:false,scaleX:2.33,scaleY:1.03,skewX:-12.8,x:25.5},0).to({_off:true},4).wait(33));

	// hastag graphic branche
	this.instance_3 = new lib.hastaggraphicbranche();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-7.3,77.7,1,1,0,0,0,0,4.6);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({x:0.7,y:41.7},14,cjs.Ease.quartOut).to({_off:true},4).wait(7).to({_off:false,regX:0.1,scaleX:1.98,scaleY:1.01,skewX:-9.8,x:19.9,y:49.7},0).to({_off:true},4).wait(4).to({_off:false,regX:0,scaleX:1,scaleY:1,skewX:0,x:0.7,y:41.7},0).to({_off:true},4).wait(21));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-18.1,17.1,64.6,57.7);


(lib.btn_playVideo = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{open:1,close:97});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_96 = function() {
		this.stop();
	}
	this.frame_135 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(96).call(this.frame_96).wait(39).call(this.frame_135).wait(1));

	// ZONE
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.004)").s().p("Am7G8IAAt3IN3AAIAAN3g");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(136));

	// Calque_16
	this.instance = new lib.line_explode();
	this.instance.parent = this;
	this.instance.setTransform(2.7,0,1,1,120);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(127).to({_off:false},0).wait(9));

	// Calque_15
	this.instance_1 = new lib.line_explode();
	this.instance_1.parent = this;
	this.instance_1.setTransform(2.8,0,0.778,1,-155.3);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(125).to({_off:false},0).wait(11));

	// Calque_14
	this.instance_2 = new lib.line_explode();
	this.instance_2.parent = this;
	this.instance_2.setTransform(2.8,0.1,1,1,-80.3);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(126).to({_off:false},0).wait(10));

	// Calque_13
	this.instance_3 = new lib.line_explode();
	this.instance_3.parent = this;
	this.instance_3.setTransform(2.9,0.1,0.807,1,69.7,0,0,0.1,-0.1);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(124).to({_off:false},0).wait(12));

	// Calque_12
	this.instance_4 = new lib.line_explode();
	this.instance_4.parent = this;
	this.instance_4.setTransform(2.8,0);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(123).to({_off:false},0).wait(13));

	// Calque_5
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgGADIANgF");
	this.shape_1.setTransform(-10.5,13.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgVALIArgU");
	this.shape_2.setTransform(-8.9,12.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgkASIBJgj");
	this.shape_3.setTransform(-7.4,12);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgzAZIBngx");
	this.shape_4.setTransform(-5.9,11.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhDAgICHg/");
	this.shape_5.setTransform(-4.4,10.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhSAnIClhN");
	this.shape_6.setTransform(-2.9,9.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhhAvIDDhc");
	this.shape_7.setTransform(-1.4,9.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhwA2IDhhr");
	this.shape_8.setTransform(0.2,8.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#FFFFFF").ss(2,1,1).p("Ah/A9ID/h5");
	this.shape_9.setTransform(1.7,7.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#FFFFFF").ss(2,1,1).p("AiOBEIEdiH");
	this.shape_10.setTransform(3.2,7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_1}]},54).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[]},34).wait(39));

	// Calque_6
	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAAgFIAAAL");
	this.shape_11.setTransform(-11.1,-13.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAAATIAAgl");
	this.shape_12.setTransform(-11.1,-12.5);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAAAhIAAhB");
	this.shape_13.setTransform(-11.1,-11.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAAAvIAAhc");
	this.shape_14.setTransform(-11.1,-9.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAAA8IAAh3");
	this.shape_15.setTransform(-11.1,-8.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAABKIAAiT");
	this.shape_16.setTransform(-11.1,-7.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAABXIAAit");
	this.shape_17.setTransform(-11.1,-5.7);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAABlIAAjJ");
	this.shape_18.setTransform(-11.1,-4.4);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAAByIAAjj");
	this.shape_19.setTransform(-11.1,-3);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAACAIAAj/");
	this.shape_20.setTransform(-11.1,-1.7);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAAiMIAAEZ");
	this.shape_21.setTransform(-11.1,-0.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_11}]},44).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[]},43).wait(39));

	// Calque_4
	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#FFFFFF").ss(2,1,1).p("AANAHIgZgM");
	this.shape_22.setTransform(16.2,-0.5);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgbgNIA3Ab");
	this.shape_23.setTransform(14.8,-1.2);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgpgUIBTAp");
	this.shape_24.setTransform(13.3,-2);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#FFFFFF").ss(2,1,1).p("Ag3gcIBwA5");
	this.shape_25.setTransform(11.9,-2.7);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhGgjICNBH");
	this.shape_26.setTransform(10.4,-3.4);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhUgrICpBX");
	this.shape_27.setTransform(9,-4.2);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhjgyIDHBl");
	this.shape_28.setTransform(7.5,-4.9);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#FFFFFF").ss(2,1,1).p("Ahxg5IDjBz");
	this.shape_29.setTransform(6.1,-5.7);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#FFFFFF").ss(2,1,1).p("AiAhBIEBCD");
	this.shape_30.setTransform(4.6,-6.4);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#FFFFFF").ss(2,1,1).p("ACPBJIkdiR");
	this.shape_31.setTransform(3.2,-7.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_22}]},35).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[]},53).wait(39));

	// Calque_3
	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAVAAIgpAA");
	this.shape_32.setTransform(-318.9,0);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgVAAIArAA");
	this.shape_33.setTransform(-318.6,0);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgZAAIAzAA");
	this.shape_34.setTransform(-317.5,0);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgfAAIA/AA");
	this.shape_35.setTransform(-315.8,0);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgnAAIBPAA");
	this.shape_36.setTransform(-313.3,0);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgyAAIBkAA");
	this.shape_37.setTransform(-310.1,0);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#FFFFFF").ss(2,1,1).p("Ag/AAIB+AA");
	this.shape_38.setTransform(-306.3,0);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhOAAICdAA");
	this.shape_39.setTransform(-301.7,0);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhfAAIC/AA");
	this.shape_40.setTransform(-296.5,0);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhzAAIDnAA");
	this.shape_41.setTransform(-290.5,0);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#FFFFFF").ss(2,1,1).p("AiJAAIETAA");
	this.shape_42.setTransform(-283.8,0);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#FFFFFF").ss(2,1,1).p("AiiAAIFFAA");
	this.shape_43.setTransform(-276.4,0);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#FFFFFF").ss(2,1,1).p("Ai9AAIF7AA");
	this.shape_44.setTransform(-268.4,0);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#FFFFFF").ss(2,1,1).p("AjaAAIG2AA");
	this.shape_45.setTransform(-259.6,0);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().s("#FFFFFF").ss(2,1,1).p("Aj6AAIH1AA");
	this.shape_46.setTransform(-250.1,0);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().s("#FFFFFF").ss(2,1,1).p("AkcAAII5AA");
	this.shape_47.setTransform(-239.9,0);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f().s("#FFFFFF").ss(2,1,1).p("AlAAAIKBAA");
	this.shape_48.setTransform(-229.1,0);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f().s("#FFFFFF").ss(2,1,1).p("AlnAAILPAA");
	this.shape_49.setTransform(-217.5,0);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f().s("#FFFFFF").ss(2,1,1).p("AmQAAIMhAA");
	this.shape_50.setTransform(-205.2,0);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f().s("#FFFFFF").ss(2,1,1).p("AmUAAIMpAA");
	this.shape_51.setTransform(-194.2,0);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f().s("#FFFFFF").ss(2,1,1).p("AmZAAIMzAA");
	this.shape_52.setTransform(-183.3,0);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f().s("#FFFFFF").ss(2,1,1).p("AmdAAIM7AA");
	this.shape_53.setTransform(-172.3,0);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f().s("#FFFFFF").ss(2,1,1).p("AmhAAINDAA");
	this.shape_54.setTransform(-161.3,0);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f().s("#FFFFFF").ss(2,1,1).p("AmmAAINNAA");
	this.shape_55.setTransform(-150.4,0);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f().s("#FFFFFF").ss(2,1,1).p("AmqAAINVAA");
	this.shape_56.setTransform(-139.4,0);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f().s("#FFFFFF").ss(2,1,1).p("AmvAAINeAA");
	this.shape_57.setTransform(-128.4,0);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f().s("#FFFFFF").ss(2,1,1).p("AmzAAINnAA");
	this.shape_58.setTransform(-117.5,0);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f().s("#FFFFFF").ss(2,1,1).p("Am4AAINxAA");
	this.shape_59.setTransform(-106.5,0);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f().s("#FFFFFF").ss(2,1,1).p("Am8AAIN5AA");
	this.shape_60.setTransform(-95.5,0);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f().s("#FFFFFF").ss(2,1,1).p("AnAAAIOBAA");
	this.shape_61.setTransform(-84.6,0);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f().s("#FFFFFF").ss(2,1,1).p("AnFAAIOLAA");
	this.shape_62.setTransform(-73.6,0);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f().s("#FFFFFF").ss(2,1,1).p("AnJAAIOTAA");
	this.shape_63.setTransform(-62.6,0);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f().s("#FFFFFF").ss(2,1,1).p("AnOAAIOdAA");
	this.shape_64.setTransform(-51.7,0);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f().s("#FFFFFF").ss(2,1,1).p("AnSAAIOlAA");
	this.shape_65.setTransform(-40.7,0);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f().s("#FFFFFF").ss(2,1,1).p("AHXAAIutAA");
	this.shape_66.setTransform(-29.8,0);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f().s("#FFFFFF").ss(2,1,1).p("AmfAAIM/AA");
	this.shape_67.setTransform(-24.2,0);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f().s("#FFFFFF").ss(2,1,1).p("AlrAAILXAA");
	this.shape_68.setTransform(-19,0);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f().s("#FFFFFF").ss(2,1,1).p("Ak7AAIJ3AA");
	this.shape_69.setTransform(-14.2,0);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f().s("#FFFFFF").ss(2,1,1).p("AkOAAIIdAA");
	this.shape_70.setTransform(-9.7,0);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f().s("#FFFFFF").ss(2,1,1).p("AjlAAIHLAA");
	this.shape_71.setTransform(-5.6,0);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f().s("#FFFFFF").ss(2,1,1).p("Ai/AAIF/AA");
	this.shape_72.setTransform(-1.8,0);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f().s("#FFFFFF").ss(2,1,1).p("AidAAIE7AA");
	this.shape_73.setTransform(1.6,0);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f().s("#FFFFFF").ss(2,1,1).p("Ah/AAID/AA");
	this.shape_74.setTransform(4.6,0);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhkAAIDJAA");
	this.shape_75.setTransform(7.3,0);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhNAAICbAA");
	this.shape_76.setTransform(9.6,0);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f().s("#FFFFFF").ss(2,1,1).p("Ag5AAIBzAA");
	this.shape_77.setTransform(11.6,0);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgpAAIBTAA");
	this.shape_78.setTransform(13.2,0);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgcAAIA5AA");
	this.shape_79.setTransform(14.5,0);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgTAAIAnAA");
	this.shape_80.setTransform(15.3,0);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgOAAIAdAA");
	this.shape_81.setTransform(15.9,0);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f().s("#FFFFFF").ss(2,1,1).p("AANAAIgZAA");
	this.shape_82.setTransform(16.1,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_51}]},1).to({state:[{t:this.shape_52}]},1).to({state:[{t:this.shape_53}]},1).to({state:[{t:this.shape_54}]},1).to({state:[{t:this.shape_55}]},1).to({state:[{t:this.shape_56}]},1).to({state:[{t:this.shape_57}]},1).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_59}]},1).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_63}]},1).to({state:[{t:this.shape_64}]},1).to({state:[{t:this.shape_65}]},1).to({state:[{t:this.shape_66}]},1).to({state:[{t:this.shape_67}]},1).to({state:[{t:this.shape_68}]},1).to({state:[{t:this.shape_69}]},1).to({state:[{t:this.shape_70}]},1).to({state:[{t:this.shape_71}]},1).to({state:[{t:this.shape_72}]},1).to({state:[{t:this.shape_73}]},1).to({state:[{t:this.shape_74}]},1).to({state:[{t:this.shape_75}]},1).to({state:[{t:this.shape_76}]},1).to({state:[{t:this.shape_77}]},1).to({state:[{t:this.shape_78}]},1).to({state:[{t:this.shape_79}]},1).to({state:[{t:this.shape_80}]},1).to({state:[{t:this.shape_81}]},1).to({state:[{t:this.shape_82}]},1).to({state:[]},1).wait(84));

	// Calque_8 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_62 = new cjs.Graphics().p("Ai/DUIAAmoIF/AAIAAGog");
	var mask_graphics_63 = new cjs.Graphics().p("Ai/DUIAAmoIF/AAIAAGog");
	var mask_graphics_64 = new cjs.Graphics().p("Ai/DUIAAmoIF/AAIAAGog");
	var mask_graphics_65 = new cjs.Graphics().p("Ai/DUIAAmoIF/AAIAAGog");
	var mask_graphics_66 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_67 = new cjs.Graphics().p("AjADUIAAmoIGAAAIAAGog");
	var mask_graphics_68 = new cjs.Graphics().p("Ai/DUIAAmoIGAAAIAAGog");
	var mask_graphics_69 = new cjs.Graphics().p("Ai/DUIAAmoIF/AAIAAGog");
	var mask_graphics_70 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_71 = new cjs.Graphics().p("Ai/DUIAAmoIGAAAIAAGog");
	var mask_graphics_72 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_73 = new cjs.Graphics().p("Ai/DUIAAmoIF/AAIAAGog");
	var mask_graphics_74 = new cjs.Graphics().p("AjADUIAAmoIGAAAIAAGog");
	var mask_graphics_75 = new cjs.Graphics().p("AjADUIAAmoIGAAAIAAGog");
	var mask_graphics_76 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_77 = new cjs.Graphics().p("Ai/DUIAAmoIF/AAIAAGog");
	var mask_graphics_78 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_79 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_80 = new cjs.Graphics().p("Ai/DUIAAmoIF/AAIAAGog");
	var mask_graphics_81 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_82 = new cjs.Graphics().p("Ai/DUIAAmoIF/AAIAAGog");
	var mask_graphics_83 = new cjs.Graphics().p("AjADUIAAmoIGAAAIAAGog");
	var mask_graphics_84 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_85 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_86 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_87 = new cjs.Graphics().p("Ai/DUIAAmoIGAAAIAAGog");
	var mask_graphics_88 = new cjs.Graphics().p("AjADUIAAmoIGAAAIAAGog");
	var mask_graphics_89 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_90 = new cjs.Graphics().p("Ai/DUIAAmoIGAAAIAAGog");
	var mask_graphics_91 = new cjs.Graphics().p("AjADUIAAmoIGAAAIAAGog");
	var mask_graphics_92 = new cjs.Graphics().p("Ai/DUIAAmoIF/AAIAAGog");
	var mask_graphics_93 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_94 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_95 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_96 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_97 = new cjs.Graphics().p("Ai/DUIAAmoIGAAAIAAGog");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(62).to({graphics:mask_graphics_62,x:38.2,y:0.7}).wait(1).to({graphics:mask_graphics_63,x:38.2,y:0.7}).wait(1).to({graphics:mask_graphics_64,x:38.2,y:0.7}).wait(1).to({graphics:mask_graphics_65,x:38.2,y:0.7}).wait(1).to({graphics:mask_graphics_66,x:38.2,y:0.7}).wait(1).to({graphics:mask_graphics_67,x:38.1,y:0.7}).wait(1).to({graphics:mask_graphics_68,x:38,y:0.7}).wait(1).to({graphics:mask_graphics_69,x:37.8,y:0.7}).wait(1).to({graphics:mask_graphics_70,x:37.5,y:0.7}).wait(1).to({graphics:mask_graphics_71,x:37.1,y:0.7}).wait(1).to({graphics:mask_graphics_72,x:36.5,y:0.7}).wait(1).to({graphics:mask_graphics_73,x:35.6,y:0.7}).wait(1).to({graphics:mask_graphics_74,x:34.5,y:0.7}).wait(1).to({graphics:mask_graphics_75,x:33.2,y:0.7}).wait(1).to({graphics:mask_graphics_76,x:31.4,y:0.7}).wait(1).to({graphics:mask_graphics_77,x:29.2,y:0.7}).wait(1).to({graphics:mask_graphics_78,x:26.6,y:0.7}).wait(1).to({graphics:mask_graphics_79,x:23.4,y:0.7}).wait(1).to({graphics:mask_graphics_80,x:20.2,y:0.7}).wait(1).to({graphics:mask_graphics_81,x:17.6,y:0.7}).wait(1).to({graphics:mask_graphics_82,x:15.4,y:0.7}).wait(1).to({graphics:mask_graphics_83,x:13.6,y:0.7}).wait(1).to({graphics:mask_graphics_84,x:12.3,y:0.7}).wait(1).to({graphics:mask_graphics_85,x:11.2,y:0.7}).wait(1).to({graphics:mask_graphics_86,x:10.4,y:0.7}).wait(1).to({graphics:mask_graphics_87,x:9.7,y:0.7}).wait(1).to({graphics:mask_graphics_88,x:9.3,y:0.7}).wait(1).to({graphics:mask_graphics_89,x:9,y:0.7}).wait(1).to({graphics:mask_graphics_90,x:8.8,y:0.7}).wait(1).to({graphics:mask_graphics_91,x:8.7,y:0.7}).wait(1).to({graphics:mask_graphics_92,x:8.6,y:0.7}).wait(1).to({graphics:mask_graphics_93,x:8.6,y:0.7}).wait(1).to({graphics:mask_graphics_94,x:8.6,y:0.7}).wait(1).to({graphics:mask_graphics_95,x:8.6,y:0.7}).wait(1).to({graphics:mask_graphics_96,x:8.6,y:0.7}).wait(1).to({graphics:mask_graphics_97,x:4.8,y:0.7}).wait(39));

	// Calque_7
	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("#FFFFFF").s().p("AiOiMIEdCRIkdCIg");
	this.shape_83.setTransform(3.2,-0.3);

	this.instance_5 = new lib.playArrow();
	this.instance_5.parent = this;
	this.instance_5.setTransform(3.2,-0.3);
	this.instance_5._off = true;

	var maskedShapeInstanceList = [this.shape_83,this.instance_5];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_83}]},62).to({state:[{t:this.instance_5}]},35).to({state:[{t:this.instance_5}]},3).to({state:[{t:this.instance_5}]},18).to({state:[]},1).wait(17));
	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(97).to({_off:false},0).wait(3).to({scaleX:0.07,scaleY:0.07},18,cjs.Ease.quartIn).to({_off:true},1).wait(17));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-44.3,-44.4,88.8,88.8);


(lib.Btn_next = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{rollOver:55,rollOut:70});

	// timeline functions:
	this.frame_54 = function() {
		this.stop();
	}
	this.frame_69 = function() {
		this.stop();
	}
	this.frame_84 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(54).call(this.frame_54).wait(15).call(this.frame_69).wait(15).call(this.frame_84).wait(1));

	// Calque_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.008)").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape.setTransform(0,29.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(85));

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgJAqIAAhDIgXAAIAAgQIBBAAIAAAQIgYAAIAABDg");
	this.shape_1.setTransform(46.1,29.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AASAqIgSgcIgRAcIgWAAIAcgqIgagpIAWAAIAPAZIAQgZIAWAAIgaApIAdAqg");
	this.shape_2.setTransform(30.2,29.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgeAqIAAhTIA8AAIAAARIgpAAIAAARIAkAAIAAAPIgkAAIAAASIAqAAIAAAQg");
	this.shape_3.setTransform(14.3,29.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAUAqIgngzIAAAzIgTAAIAAhTIASAAIAoA1IAAg1IATAAIAABTg");
	this.shape_4.setTransform(-2.7,29.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgKAQIAEgOQgEgCAAgFQAAgEADgDQADgDAEAAQAFAAADADQADADAAAEQAAAEgDAEIgIANg");
	this.shape_5.setTransform(-28.5,32.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAPAqIgYghIgIAKIAAAXIgTAAIAAhTIATAAIAAAjIAegjIAYAAIgiAlIAQAWIASAYg");
	this.shape_6.setTransform(-38,29.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgfAfQgMgNAAgSQAAgSAMgMQAOgNARAAQASAAANANQANAMAAASQAAASgNANQgNAMgSAAQgRAAgOgMgAgQgSQgIAIAAAKQAAALAIAIQAGAIAKAAQALAAAHgIQAHgIAAgLQAAgKgHgIQgHgIgLAAQgKAAgGAIg");
	this.shape_7.setTransform(-55.7,29.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#333333").ss(1,1,1).p("AxpAAMAjTAAA");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(85));

	// Layer_2
	this.instance = new lib.Btn_cliquable_area();
	this.instance.parent = this;
	this.instance.setTransform(0,29.5,1,1,0,0,0,0,30);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({alpha:0.012},46).wait(6).to({y:30},0).to({alpha:1},14).wait(1).to({alpha:0.012},13).wait(2));

	// BG
	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("rgba(0,0,0,0.098)").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape_9.setTransform(0,29.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_9).wait(85));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-114,-1,228,60.5);


(lib.Btn_detail = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"rollOver":55,"rollOut":70});

	// timeline functions:
	this.frame_54 = function() {
		this.stop();
	}
	this.frame_69 = function() {
		this.stop();
	}
	this.frame_84 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(54).call(this.frame_54).wait(15).call(this.frame_69).wait(15).call(this.frame_84).wait(1));

	// Calque_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.008)").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape.setTransform(0,29.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(85));

	// content
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgOAyIAAhkIAdAAIAABkg");
	this.shape_1.setTransform(-85.4,29.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgyAOIAAgcIBlAAIAAAcg");
	this.shape_2.setTransform(-85.4,29.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgaAqIAAhTIASAAIAABDIAjAAIAAAQg");
	this.shape_3.setTransform(48.4,29.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgIAqIAAhTIARAAIAABTg");
	this.shape_4.setTransform(34.7,29.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AAZAqIgHgSIgjAAIgHASIgUAAIAkhTIARAAIAkBTgAgKAIIAUAAIgKgYg");
	this.shape_5.setTransform(20.2,29.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgJAqIAAhDIgXAAIAAgQIBBAAIAAAQIgYAAIAABDg");
	this.shape_6.setTransform(4.1,29.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgeAqIAAhTIA8AAIAAARIgpAAIAAARIAkAAIAAAPIgkAAIAAASIAqAAIAAAQg");
	this.shape_7.setTransform(-11.3,29.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AglAqIAAhTIAeAAQAVAAAMALQAMALAAATQAAATgMALQgMAMgWAAgAgTAaIANAAQAMAAAGgHQAHgGAAgNQAAgLgHgHQgGgGgOgBIgLAAg");
	this.shape_8.setTransform(-27.8,29.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#333333").ss(1,1,1).p("AxpAAMAjTAAA");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(85));

	// Calque_7
	this.instance = new lib.Btn_cliquable_area();
	this.instance.parent = this;
	this.instance.setTransform(0,29.5,1,1,0,0,0,0,30);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({alpha:0.012},46).wait(6).to({y:30},0).to({alpha:1},14).wait(1).to({alpha:0.012},13).wait(2));

	// BG
	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("rgba(0,0,0,0.004)").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape_10.setTransform(0,29.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_10).wait(85));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-114,-1,228,60.5);


(lib.bloc_visuel = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":83});

	// timeline functions:
	this.frame_82 = function() {
		this.stop();
	}
	this.frame_119 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(82).call(this.frame_82).wait(37).call(this.frame_119).wait(1));

	// masque VISUEL (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_45 = new cjs.Graphics().p("EgjsBDvMAAAiHdMBHZAAAMAAACHdg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(45).to({graphics:mask_graphics_45,x:-228.7,y:-19}).wait(75));

	// VISUEL
	this.instance = new lib.prject_illustration();
	this.instance.parent = this;
	this.instance.setTransform(242.5,-697.6,1,1,-34,0,0,11.6,-308.4);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regY:-308.3,rotation:18.5,x:-172.5,y:-498.6},37,cjs.Ease.quadOut).to({regX:11.7,rotation:0,x:-85.5,y:-605.5},43,cjs.Ease.quadInOut).wait(9).to({regX:0,regY:316.6,rotation:-12.7,x:-39.5,y:6.6},0).to({x:-719.5},30,cjs.Ease.quartInOut).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(126,-651.5,518.4,657.2);


(lib.bloc_video = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"open":1,"close":42});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_34 = function() {
		this.btn_playVideo.gotoAndPlay("open");
	}
	this.frame_41 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(34).call(this.frame_34).wait(7).call(this.frame_41).wait(16));

	// PLAY btn
	this.btn_playVideo = new lib.btn_playVideo();
	this.btn_playVideo.name = "btn_playVideo";
	this.btn_playVideo.parent = this;
	this.btn_playVideo.setTransform(358.3,-255.7);

	this.timeline.addTween(cjs.Tween.get(this.btn_playVideo).wait(57));

	// masque video (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_43 = new cjs.Graphics().p("Ega8A8AMAAAh3/MA7rAAAMAAAB3/g");
	var mask_graphics_44 = new cjs.Graphics().p("EgYrA8AMAAAh3/MA3HAAAMAAAB3/g");
	var mask_graphics_45 = new cjs.Graphics().p("EgWaA8AMAAAh3/MAyiAAAMAAAB3/g");
	var mask_graphics_46 = new cjs.Graphics().p("EgUJA8AMAAAh3/MAt+AAAMAAAB3/g");
	var mask_graphics_47 = new cjs.Graphics().p("EgR4A8AMAAAh3/MApaAAAMAAAB3/g");
	var mask_graphics_48 = new cjs.Graphics().p("EgPnA8AMAAAh3/MAk1AAAMAAAB3/g");
	var mask_graphics_49 = new cjs.Graphics().p("EgNWA8AMAAAh3/MAgRAAAMAAAB3/g");
	var mask_graphics_50 = new cjs.Graphics().p("EgLFA8AMAAAh3/IbtAAMAAAB3/g");
	var mask_graphics_51 = new cjs.Graphics().p("EgI0A8AMAAAh3/IXIAAMAAAB3/g");
	var mask_graphics_52 = new cjs.Graphics().p("EgGjA8AMAAAh3/ISkAAMAAAB3/g");
	var mask_graphics_53 = new cjs.Graphics().p("EgESA8AMAAAh3/IOAAAMAAAB3/g");
	var mask_graphics_54 = new cjs.Graphics().p("EgCBA8AMAAAh3/IJbAAMAAAB3/g");
	var mask_graphics_55 = new cjs.Graphics().p("EAAPA8AMAAAh3/IE4AAMAAAB3/g");
	var mask_graphics_56 = new cjs.Graphics().p("EACgA8AMAAAh3/IAUAAMAAAB3/g");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(43).to({graphics:mask_graphics_43,x:209.5,y:0}).wait(1).to({graphics:mask_graphics_44,x:194.8,y:0}).wait(1).to({graphics:mask_graphics_45,x:180,y:0}).wait(1).to({graphics:mask_graphics_46,x:165.3,y:0}).wait(1).to({graphics:mask_graphics_47,x:150.6,y:0}).wait(1).to({graphics:mask_graphics_48,x:135.8,y:0}).wait(1).to({graphics:mask_graphics_49,x:121.1,y:0}).wait(1).to({graphics:mask_graphics_50,x:106.4,y:0}).wait(1).to({graphics:mask_graphics_51,x:91.6,y:0}).wait(1).to({graphics:mask_graphics_52,x:76.9,y:0}).wait(1).to({graphics:mask_graphics_53,x:62.2,y:0}).wait(1).to({graphics:mask_graphics_54,x:47.4,y:0}).wait(1).to({graphics:mask_graphics_55,x:32.7,y:0}).wait(1).to({graphics:mask_graphics_56,x:18,y:0}).wait(1));

	// VIDEO
	this.videoContainer = new lib.video();
	this.videoContainer.name = "videoContainer";
	this.videoContainer.parent = this;
	this.videoContainer.setTransform(36.7,765,0.659,0.659,0,0,0,0.3,480.2);

	var maskedShapeInstanceList = [this.videoContainer];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.videoContainer).wait(1).to({y:1},40,cjs.Ease.quartInOut).wait(16));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(36.5,-300.1,382.1,1381.2);


(lib.bloc_NEXT_btn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":40});

	// timeline functions:
	this.frame_39 = function() {
		this.stop();
	}
	this.frame_56 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(39).call(this.frame_39).wait(17).call(this.frame_56).wait(1));

	// CTA DETAIL
	this.btnNext = new lib.Btn_next();
	this.btnNext.name = "btnNext";
	this.btnNext.parent = this;
	this.btnNext.setTransform(-340.5,411.5,1,1,0,0,0,0,26.5);

	this.timeline.addTween(cjs.Tween.get(this.btnNext).to({y:350.5},39,cjs.Ease.quartInOut).wait(2).to({y:411.5},14,cjs.Ease.quartInOut).to({_off:true},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-454,384.5,227,60);


(lib.bloc_lignes = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{close_OLD:166,"close":209});

	// timeline functions:
	this.frame_165 = function() {
		this.stop();
	}
	this.frame_208 = function() {
		this.stop();
	}
	this.frame_229 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(165).call(this.frame_165).wait(43).call(this.frame_208).wait(21).call(this.frame_229).wait(1));

	// ligne
	this.instance = new lib.ligne();
	this.instance.parent = this;
	this.instance.setTransform(-454,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(166).to({x:-883},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:-454},0).wait(21));

	// ligne
	this.instance_1 = new lib.ligne();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-227,406.5,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(119).to({y:320.5},40,cjs.Ease.quartInOut).wait(7).to({y:406.5},0).to({x:-883.2},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:-227,y:320.5},0).to({y:406.5},17,cjs.Ease.quartInOut).wait(4));

	// ligne
	this.instance_2 = new lib.ligne();
	this.instance_2.parent = this;
	this.instance_2.setTransform(0,406.5,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(166).to({x:-883.4},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:0},0).wait(21));

	// ligne
	this.instance_3 = new lib.ligne();
	this.instance_3.parent = this;
	this.instance_3.setTransform(227,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(124).to({y:320},40,cjs.Ease.quartInOut).to({_off:true},2).wait(43).to({_off:false},0).to({y:406},17,cjs.Ease.quartInOut).wait(4));

	// ligne
	this.instance_4 = new lib.ligne();
	this.instance_4.parent = this;
	this.instance_4.setTransform(454.1,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(166).to({x:-883.4},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:454.1},0).wait(21));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-455.4,-174.5,910.5,988);


(lib.bloc_hashtag = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_61 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(61).call(this.frame_61).wait(2));

	// Layer_1
	this.instance = new lib.hashtag_graphic("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(-456,243.2,2.317,2.317,0,0,0,-0.1,50.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true},61).wait(2));

	// Layer_5 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_28 = new cjs.Graphics().p("AtWBpIAAjRIatAAIAADRg");
	var mask_graphics_32 = new cjs.Graphics().p("AtWDSIAAmjIatAAIAAGjg");
	var mask_graphics_40 = new cjs.Graphics().p("AtWGYIAAsvIatAAIAAMvg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(28).to({graphics:mask_graphics_28,x:-400.4,y:265.5}).wait(4).to({graphics:mask_graphics_32,x:-400.4,y:255}).wait(8).to({graphics:mask_graphics_40,x:-400.4,y:255.9}).wait(23));

	// Layer_3
	this.instance_1 = new lib.txt_hashtag();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-421.5,244.1,1,1,0,0,0,0,31.6);
	this.instance_1._off = true;

	var maskedShapeInstanceList = [this.instance_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(28).to({_off:false},0).to({_off:true},34).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-497.8,166.1,149.7,129.4);


(lib.bloc_CTA = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":40});

	// timeline functions:
	this.frame_39 = function() {
		this.stop();
	}
	this.frame_56 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(39).call(this.frame_39).wait(17).call(this.frame_56).wait(1));

	// CTA DETAIL
	this.btnCta = new lib.Btn_detail();
	this.btnCta.name = "btnCta";
	this.btnCta.parent = this;
	this.btnCta.setTransform(-340.5,411.5,1,1,0,0,0,0,26.5);

	this.timeline.addTween(cjs.Tween.get(this.btnCta).to({y:350.5},39,cjs.Ease.quartInOut).wait(2).to({y:411.5},14,cjs.Ease.quartInOut).to({_off:true},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-454,384.5,227,60);


(lib.blocInfostextes = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":80});

	// timeline functions:
	this.frame_79 = function() {
		this.stop();
	}
	this.frame_120 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(79).call(this.frame_79).wait(41).call(this.frame_120).wait(1));

	// Bloc Hashtag
	this.instance = new lib.bloc_hashtag();
	this.instance.parent = this;
	this.instance.setTransform(-391.5,261.2,0.9,0.9,0,0,0,-412.9,244.7);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(62).to({_off:false},0).to({_off:true},18).wait(41));

	// masque Titre
	this.instance_1 = new lib.masque_generique();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-527.9,-13.9,0.007,0.471,0,0,0,0,17.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(34).to({_off:false},0).to({regX:0.5,scaleX:1.92,x:-527.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.01,x:-228.9},10,cjs.Ease.quartInOut).to({_off:true},1).wait(26).to({_off:false},0).to({regX:0.5,scaleX:1.92,x:-527.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.01,x:-528.7},10,cjs.Ease.quartInOut).to({_off:true},1).wait(21));

	// _NOM-du-CLIENT
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#00CD6A").s().p("AgpApQgRgQAAgZQAAgXARgRQARgRAYAAQAZAAARARQARARAAAXQAAAZgRAQQgRARgZAAQgYAAgRgRgAgXgYQgJAKAAAOQAAAPAJALQAKAKANAAQAOAAAKgKQAJgLAAgPQAAgOgJgKQgKgLgOAAQgNAAgKALg");
	this.shape.setTransform(-264.2,-14.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#00CD6A").s().p("AAaA4IgzhFIAABFIgZAAIAAhvIAWAAIA2BHIAAhHIAZAAIAABvg");
	this.shape_1.setTransform(-289.3,-14.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#00CD6A").s().p("AgpApQgRgQAAgZQAAgXARgRQARgRAYAAQAZAAARARQARARAAAXQAAAZgRAQQgRARgZAAQgYAAgRgRgAgXgYQgJAKAAAOQAAAPAJALQAKAKANAAQAOAAAKgKQAJgLAAgPQAAgOgJgKQgKgLgOAAQgNAAgKALg");
	this.shape_2.setTransform(-314.5,-14.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#00CD6A").s().p("AgjApQgQgQAAgYQAAgZARgQQARgRAXAAQAYAAASAQIgNASQgIgGgGgDQgGgCgIAAQgNAAgKAKQgJAKgBAPQAAAPAKAKQAJAJANABQAMgBAJgFIAAgeIAZAAIAAAnQgQATgeAAQgXAAgSgRg");
	this.shape_3.setTransform(-339.1,-14.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#00CD6A").s().p("AgpApQgRgQAAgZQAAgXARgRQARgRAYAAQAZAAARARQARARAAAXQAAAZgRAQQgRARgZAAQgYAAgRgRgAgXgYQgJAKAAAOQAAAPAJALQAKAKANAAQAOAAAKgKQAJgLAAgPQAAgOgJgKQgKgLgOAAQgNAAgKALg");
	this.shape_4.setTransform(-363.7,-14.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#00CD6A").s().p("AgrAmIAOgSQATAQAOAAQAHAAAEgDQAEgDAAgGQAAgEgFgDQgEgDgLgDQgUgFgJgGQgJgIAAgQQAAgPAMgKQALgIAQAAQAMAAALAEQALAEAIAHIgMASQgPgLgPAAQgGAAgDADQgEADAAAFQAAAFAEADQAFACAPAFQAQAEAJAGQAIAIAAAPQAAAPgLAJQgLAKgTAAQgZAAgVgUg");
	this.shape_5.setTransform(-387.5,-14.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#00CD6A").s().p("AgLA4IAAhvIAYAAIAABvg");
	this.shape_6.setTransform(-407.1,-14.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#00CD6A").s().p("AASA4IgYgjIgRAAIAAAjIgZAAIAAhvIAqAAQAaAAALAJQALAJAAATQAAAagWAIIAdAogAgXgBIASAAQAMAAAFgDQAEgFAAgJQAAgIgFgEQgEgEgLAAIgTAAg");
	this.shape_7.setTransform(-427.1,-14.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#00CD6A").s().p("AgjApQgQgQAAgYQAAgZARgQQARgRAXAAQAYAAASAQIgNASQgHgGgHgDQgGgCgIAAQgNAAgJAKQgLAKAAAPQAAAPAKAKQAKAJALABQANgBAJgFIAAgeIAZAAIAAAnQgRATgdAAQgXAAgSgRg");
	this.shape_8.setTransform(-451.4,-14.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#00CD6A").s().p("AgoA4IAAhvIBPAAIAAAXIg2AAIAAAWIAxAAIAAAUIgxAAIAAAYIA4AAIAAAWg");
	this.shape_9.setTransform(-490.5,-14.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#00CD6A").s().p("AgxA4IAAhvIAnAAQAcAAAQAPQARAOgBAaQAAAZgPAQQgQAPgfAAgAgYAiIAPAAQAQAAAJgJQAJgJAAgQQAAgPgJgJQgJgJgSAAIgNAAg");
	this.shape_10.setTransform(-513.9,-14.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},34).to({state:[]},55).to({state:[]},1).wait(31));

	// masque Titre (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_17 = new cjs.Graphics().p("EgpOArJMAAAhWRMAvJAAAMAAABWRg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(17).to({graphics:mask_graphics_17,x:-263.9,y:-6.1}).wait(24).to({graphics:null,x:0,y:0}).wait(80));

	// DECO 2
	this.instance_2 = new lib.crazymals_singe();
	this.instance_2.parent = this;
	this.instance_2.setTransform(3.7,-348,0.49,0.49,0,0,180);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#FFFFFF").ss(1,1,1).p("AW3ISI4mONI1HzBILk58IcQC+g");
	this.shape_11.setTransform(-22.7,68.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#FFFFFF").ss(1,1,1).p("AnFmSICWm8ISbU+I7XFfIAAgB");
	this.shape_12.setTransform(-240,-49.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AqGJxIGnzhIAAAAINmPeI0MEDg");
	this.shape_13.setTransform(-262.9,-27.5);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#FFFFFF").ss(1,1,1).p("ALnNZI3NtWIXJtbg");
	this.shape_14.setTransform(-577,114.6);

	var maskedShapeInstanceList = [this.instance_2,this.shape_11,this.shape_12,this.shape_13,this.shape_14];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_2,p:{scaleX:0.49,scaleY:0.49,skewY:180,x:3.7,y:-348}}]},30).to({state:[{t:this.instance_2,p:{scaleX:1,scaleY:1,skewY:0,x:5,y:12}}]},19).to({state:[]},24).to({state:[{t:this.shape_11}]},20).to({state:[{t:this.shape_13},{t:this.shape_12}]},4).to({state:[{t:this.shape_14}]},3).to({state:[]},6).to({state:[]},1).wait(14));

	// _LETTRES_ANARCHIQUES
	this.instance_3 = new lib.lettres_FAT();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-433.8,-73,1,1,0,0,0,0,186.8);
	this.instance_3._off = true;

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#00CD6A").s().p("AixBCIAAiEIFjAAIAACEg");
	this.shape_15.setTransform(-282.9,136.7);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#00CD6A").s().p("AhNFdIAAkTIjymmICpAAICWEDICXkDICpAAIjyGmIAAETg");
	this.shape_16.setTransform(-350.3,131.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#00CD6A").s().p("AkjFeIAAhrIFtnIIliAAIAAiHII2AAIAABsIlrHFIFyAAIAACJg");
	this.shape_17.setTransform(-500.9,7.9);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#00CD6A").s().p("ADTFdIhBiWIkkAAIhBCWIilAAIEtq5ICWAAIEvK5gAhXA9ICuAAIhWjJg");
	this.shape_18.setTransform(-266.6,-116);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#00CD6A").s().p("ABxFdIiajeIhsAAIAADeIibAAIAAq5IEIAAQCiAABGA3QBGA3AAB6QAACmiGAyICyD5gAiVgHIBzAAQBPAAAdgaQAcgbAAg4QAAg4gdgWQgegVhLABIh1AAg");
	this.shape_19.setTransform(-349.4,-116);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#00CD6A").s().p("AixBCIAAiDIFjAAIAACDg");
	this.shape_20.setTransform(-425.1,-111.1);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#00CD6A").s().p("AjeEDQhohmAAibQAAibBqhnQBrhpCYAAQCtAABzCEIhgBuQhLhchuAAQhWAAg/A5Qg/A5ABBhQAABhA6A8QA7A6BTAAQB0ABBJhcIBjBnQh3CGihAAQigABhphng");
	this.shape_21.setTransform(-497.6,-116.4);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#00CD6A").s().p("AhNFeIAAkVIjymmICpAAICWEFICXkFICpAAIjyGmIAAEVg");
	this.shape_22.setTransform(-150.4,-141);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#00CD6A").s().p("AkjFeIAAhrIFsnJIlhAAIAAiHII2AAIAABsIlsHHIFzAAIAACIg");
	this.shape_23.setTransform(-227.5,-141);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#00CD6A").s().p("ADTFeIhBiXIkjAAIhCCXIilAAIEtq7ICWAAIEvK7gAhXA+ICvAAIhXjLg");
	this.shape_24.setTransform(-310.6,-141);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#00CD6A").s().p("ABxFeIiajfIhsAAIAADfIibAAIAAq7IEIAAQCjAABFA3QBGA4AAB6QAACmiGAyICyD6gAiVgHIBzAAQBPAAAcgbQAdgZAAg5QAAg4gdgWQgegUhLAAIh1AAg");
	this.shape_25.setTransform(-393.4,-141);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#00CD6A").s().p("AjeEDQhohlAAicQAAibBqhnQBrhpCYAAQCtAABzCEIhgBtQhLhchuABQhWgBg/A6Qg/A6AABgQABBhA6A8QA7A6BUAAQByAABKhbIBkBoQh4CGihAAQihAAhohng");
	this.shape_26.setTransform(-480.6,-141.4);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#00CD6A").s().p("AhNFeIAAkVIjymmICpAAICWEEICXkEICpAAIjyGmIAAEVg");
	this.shape_27.setTransform(-150.4,-17.1);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#00CD6A").s().p("ABxFeIiajgIhsAAIAADgIibAAIAAq7IEIAAQCjAABFA3QBGA3AAB7QAACmiGAxICyD7gAiVgIIBzAAQBPAAAcgaQAdgZAAg5QAAg4gdgWQgegUhLgBIh1AAg");
	this.shape_28.setTransform(-393.4,-17.1);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#00CD6A").s().p("AjeEDQhohlAAicQAAiaBqhoQBrhpCYAAQCtAABzCEIhgBtQhLhchuABQhWAAg/A5Qg/A6AABgQABBiA6A7QA7A6BUAAQByAABKhbIBkBoQh4CGihAAQihAAhohng");
	this.shape_29.setTransform(-480.6,-17.5);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#00CD6A").s().p("AkjFeIAAhrIFsnJIlhAAIAAiHII2AAIAABsIlsHHIFzAAIAACIg");
	this.shape_30.setTransform(-227.5,-141);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#00CD6A").s().p("ADTFeIhBiXIkjAAIhCCXIilAAIEtq7ICWAAIEvK7gAhXA+ICvAAIhXjLg");
	this.shape_31.setTransform(-310.6,-141);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#00CD6A").s().p("AhNFdIAAkTIjymmICpAAICWEEICXkEICpAAIjyGmIAAETg");
	this.shape_32.setTransform(-150.4,106.8);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#00CD6A").s().p("AkjFdIAAhqIFsnIIlhAAIAAiHII2AAIAABsIlsHFIFzAAIAACIg");
	this.shape_33.setTransform(-227.5,106.8);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#00CD6A").s().p("ADTFdIhBiWIkjAAIhCCWIilAAIEtq5ICWAAIEvK5gAhXA9ICvAAIhXjJg");
	this.shape_34.setTransform(-310.6,106.8);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#00CD6A").s().p("ABxFdIiajeIhsAAIAADeIibAAIAAq5IEIAAQCjAABFA3QBGA3AAB6QAACmiGAyICyD5gAiVgHIBzAAQBPAAAcgaQAdgbAAg4QAAg4gdgWQgegVhLABIh1AAg");
	this.shape_35.setTransform(-393.4,106.8);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#00CD6A").s().p("AjeEEQhohnAAibQAAibBqhnQBrhoCYgBQCtAABzCEIhgBuQhLhchuAAQhWAAg/A5Qg/A5AABhQABBhA6A8QA7A6BUAAQByABBKhcIBkBnQh4CGihAAQihABhohmg");
	this.shape_36.setTransform(-480.6,106.4);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#00CD6A").s().p("AhNFeIAAkVIjymlICpAAICWEEICXkEICpAAIjyGlIAAEVg");
	this.shape_37.setTransform(-150.4,663.2);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#00CD6A").s().p("AkjFeIAAhrIFsnIIlhAAIAAiHII2AAIAABrIlsHHIFzAAIAACIg");
	this.shape_38.setTransform(-227.5,663.2);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#00CD6A").s().p("ADTFeIhBiXIkjAAIhCCXIilAAIEtq6ICWAAIEvK6gAhXA+ICvAAIhXjLg");
	this.shape_39.setTransform(-310.6,663.2);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#00CD6A").s().p("ABxFeIiajfIhsAAIAADfIibAAIAAq6IEIAAQCjgBBFA3QBGA3AAB7QAACmiGAyICyD6gAiVgHIBzAAQBPAAAcgaQAdgaAAg5QAAg4gdgWQgegUhLAAIh1AAg");
	this.shape_40.setTransform(-393.4,663.2);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#00CD6A").s().p("AjeEDQhohlAAicQAAibBqhnQBrhpCYAAQCtAABzCEIhgBuQhLhdhuABQhWgBg/A6Qg/A6AABgQABBhA6A8QA7A6BUAAQByAABKhbIBkBoQh4CGihAAQihAAhohng");
	this.shape_41.setTransform(-480.6,662.8);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#00CD6A").s().p("AkjFeIAAhsIFsnIIlhAAIAAiHII2AAIAABsIlsHHIFzAAIAACIg");
	this.shape_42.setTransform(-227.5,539.3);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#00CD6A").s().p("ADTFeIhBiYIkjAAIhCCYIilAAIEtq7ICWAAIEvK7gAhXA9ICvAAIhXjKg");
	this.shape_43.setTransform(-310.6,539.3);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#00CD6A").s().p("ABxFeIiajgIhsAAIAADgIibAAIAAq7IEIAAQCjAABFA3QBGA4AAB5QAACoiGAwICyD7gAiVgIIBzAAQBPAAAcgaQAdgaAAg4QAAg5gdgUQgegWhLAAIh1AAg");
	this.shape_44.setTransform(-393.4,539.3);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#00CD6A").s().p("AjeEEQhohmAAicQAAiaBqhpQBrhoCYABQCtgBBzCDIhgBuQhLhbhugBQhWABg/A5Qg/A5AABhQABBhA6A7QA7A7BUABQBygBBKhaIBkBmQh4CHihAAQihgBhohlg");
	this.shape_45.setTransform(-480.6,538.9);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#00CD6A").s().p("AkjFdIAAhrIFsnHIlhAAIAAiHII2AAIAABsIlsHFIFzAAIAACIg");
	this.shape_46.setTransform(-227.5,415.4);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#00CD6A").s().p("ABxFdIiajeIhsAAIAADeIibAAIAAq5IEIAAQCjAABFA3QBGA2AAB6QAACoiGAxICyD5gAiVgHIBzAAQBPAAAcgaQAdgbAAg4QAAg5gdgUQgegWhLABIh1AAg");
	this.shape_47.setTransform(-393.4,415.4);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#00CD6A").s().p("AjeEEQhohnAAibQAAibBqhoQBrhnCYAAQCtAABzCCIhgBvQhLhdhuAAQhWAAg/A6Qg/A6AABgQABBiA6A6QA7A8BUgBQByABBKhbIBkBmQh4CGihAAQihAAhohlg");
	this.shape_48.setTransform(-480.6,415);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#00CD6A").s().p("AhNFeIAAkVIjymlICpAAICWEEICXkEICpAAIjyGlIAAEVg");
	this.shape_49.setTransform(-150.4,291.5);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#00CD6A").s().p("AkjFeIAAhrIFsnIIlhAAIAAiHII2AAIAABrIlsHHIFzAAIAACIg");
	this.shape_50.setTransform(-227.5,291.5);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#00CD6A").s().p("ADTFeIhBiXIkjAAIhCCXIilAAIEtq6ICWAAIEvK6gAhXA+ICvAAIhXjLg");
	this.shape_51.setTransform(-310.6,291.5);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#00CD6A").s().p("ABxFeIiajfIhsAAIAADfIibAAIAAq6IEIAAQCjgBBFA3QBGA4AAB6QAACmiGAyICyD6gAiVgHIBzAAQBPAAAcgaQAdgaAAg5QAAg4gdgWQgegUhLAAIh1AAg");
	this.shape_52.setTransform(-393.4,291.5);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#00CD6A").s().p("AhNFdIAAkUIjymmICpAAICWEEICXkEICpAAIjyGmIAAEUg");
	this.shape_53.setTransform(-150.4,167.6);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#00CD6A").s().p("AkjFdIAAhrIFsnIIlhAAIAAiHII2AAIAABsIlsHHIFzAAIAACHg");
	this.shape_54.setTransform(-227.5,167.6);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#00CD6A").s().p("ADTFdIhBiXIkjAAIhCCXIilAAIEtq6ICWAAIEvK6gAhXA9ICvAAIhXjKg");
	this.shape_55.setTransform(-310.6,167.6);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#00CD6A").s().p("ABxFdIiajfIhsAAIAADfIibAAIAAq6IEIAAQCjAABFA3QBGA4AAB5QAACoiGAwICyD6gAiVgIIBzAAQBPAAAcgaQAdgaAAg4QAAg5gdgUQgegWhLAAIh1AAg");
	this.shape_56.setTransform(-393.4,167.6);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#00CD6A").s().p("AjeEEQhohmAAicQAAiaBqhpQBrhoCYABQCtgBBzCDIhgBuQhLhbhugBQhWABg/A5Qg/A5AABhQABBhA6A7QA7A7BUABQBygBBKhaIBkBmQh4CHihAAQihgBhohlg");
	this.shape_57.setTransform(-480.6,167.2);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#00CD6A").s().p("AhNFdIAAkTIjymmICpAAICWEEICXkEICpAAIjyGmIAAETg");
	this.shape_58.setTransform(-150.4,43.7);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#00CD6A").s().p("AkjFdIAAhrIFsnHIlhAAIAAiHII2AAIAABsIlsHFIFzAAIAACIg");
	this.shape_59.setTransform(-227.5,43.7);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#00CD6A").s().p("ADTFdIhBiWIkjAAIhCCWIilAAIEtq5ICWAAIEvK5gAhXA9ICvAAIhXjJg");
	this.shape_60.setTransform(-310.6,43.7);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#00CD6A").s().p("ABxFdIiajeIhsAAIAADeIibAAIAAq5IEIAAQCjAABFA3QBGA2AAB6QAACoiGAxICyD5gAiVgHIBzAAQBPAAAcgaQAdgbAAg4QAAg5gdgUQgegWhLABIh1AAg");
	this.shape_61.setTransform(-393.4,43.7);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#00CD6A").s().p("AjeEEQhohnAAibQAAibBqhoQBrhnCYAAQCtAABzCCIhgBvQhLhdhuAAQhWAAg/A6Qg/A6AABgQABBiA6A6QA7A8BUgBQByABBKhbIBkBmQh4CGihAAQihAAhohlg");
	this.shape_62.setTransform(-480.6,43.3);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#00CD6A").s().p("AkjFdIAAhrIFsnHIlhAAIAAiHII2AAIAABsIlsHFIFzAAIAACIg");
	this.shape_63.setTransform(-227.5,-80.2);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#00CD6A").s().p("ADTFdIhBiXIkjAAIhCCXIilAAIEtq5ICWAAIEvK5gAhXA9ICvAAIhXjJg");
	this.shape_64.setTransform(-310.6,-80.2);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#00CD6A").s().p("ABxFdIiajfIhsAAIAADfIibAAIAAq5IEIAAQCjAABFA3QBGA2AAB6QAACoiGAwICyD6gAiVgIIBzAAQBPAAAcgZQAdgaAAg5QAAg5gdgUQgegWhLAAIh1AAg");
	this.shape_65.setTransform(-393.4,-80.2);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#00CD6A").s().p("AjeEEQhohnAAibQAAibBqhoQBrhnCYAAQCtAABzCCIhgBvQhLhdhuAAQhWAAg/A6Qg/A5AABhQABBiA6A6QA7A8BUAAQByAABKhbIBkBmQh4CGihAAQihAAhohlg");
	this.shape_66.setTransform(-480.6,-80.6);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#00CD6A").s().p("AhNFeIAAkUIjymmICpAAICWEEICXkEICpAAIjyGmIAAEUg");
	this.shape_67.setTransform(-150.4,-204.1);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#00CD6A").s().p("AkjFeIAAhrIFsnIIlhAAIAAiHII2AAIAABsIlsHFIFzAAIAACJg");
	this.shape_68.setTransform(-227.5,-204.1);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#00CD6A").s().p("ADTFeIhBiXIkjAAIhCCXIilAAIEtq6ICWAAIEvK6gAhXA+ICvAAIhXjLg");
	this.shape_69.setTransform(-310.6,-204.1);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#00CD6A").s().p("ABxFeIiajfIhsAAIAADfIibAAIAAq6IEIAAQCjAABFA2QBGA4AAB6QAACmiGAyICyD6gAiVgHIBzAAQBPAAAcgaQAdgbAAg4QAAg4gdgWQgegUhLAAIh1AAg");
	this.shape_70.setTransform(-393.4,-204.1);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#00CD6A").s().p("AjeEDQhohlAAicQAAibBqhnQBrhpCYAAQCtAABzCEIhgBuQhLhchuAAQhWgBg/A6Qg/A6AABgQABBhA6A8QA7A6BUAAQByABBKhcIBkBoQh4CFihAAQihABhohng");
	this.shape_71.setTransform(-480.6,-204.5);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#00CD6A").s().p("AhNFeIAAkVIjymmICpAAICWEEICXkEICpAAIjyGmIAAEVg");
	this.shape_72.setTransform(-150.4,-328);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#00CD6A").s().p("AkjFeIAAhsIFsnIIlhAAIAAiHII2AAIAABsIlsHHIFzAAIAACIg");
	this.shape_73.setTransform(-227.5,-328);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#00CD6A").s().p("ADTFeIhBiYIkjAAIhCCYIilAAIEtq7ICWAAIEvK7gAhXA+ICvAAIhXjLg");
	this.shape_74.setTransform(-310.6,-328);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#00CD6A").s().p("ABxFeIiajgIhsAAIAADgIibAAIAAq7IEIAAQCjAABFA3QBGA3AAB6QAACniGAxICyD7gAiVgIIBzAAQBPAAAcgaQAdgaAAg4QAAg5gdgVQgegUhLgBIh1AAg");
	this.shape_75.setTransform(-393.4,-328);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#00CD6A").s().p("AjeEDQhohlAAicQAAiaBqhpQBrhoCYAAQCtAABzCDIhgBuQhLhchuAAQhWABg/A5Qg/A5AABhQABBiA6A6QA7A7BUABQByAABKhcIBkBoQh4CGihAAQihAAhohng");
	this.shape_76.setTransform(-480.6,-328.4);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#00CD6A").s().p("AkjFeIAAhrIFsnIIlgAAIAAiHII2AAIAABrIltHHIFzAAIAACIg");
	this.shape_77.setTransform(613.1,663.2);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("#00CD6A").s().p("ABxFeIiajfIhsAAIAADfIibAAIAAq6IEIAAQCjgBBFA3QBGA3AAB7QAACmiGAyICyD6gAiVgHIBzAAQBPAAAdgaQAcgaAAg5QAAg4gdgWQgegUhLAAIh1AAg");
	this.shape_78.setTransform(447.2,663.2);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("#00CD6A").s().p("AkjFeIAAhsIFsnIIlgAAIAAiHII2AAIAABsIltHHIFzAAIAACIg");
	this.shape_79.setTransform(613.1,539.3);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f("#00CD6A").s().p("ABxFeIiajgIhsAAIAADgIibAAIAAq7IEIAAQCjAABFA3QBGA4AAB5QAACoiGAwICyD7gAiVgIIBzAAQBPAAAdgaQAcgaAAg4QAAg5gdgUQgegWhLAAIh1AAg");
	this.shape_80.setTransform(447.2,539.3);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("#00CD6A").s().p("AkjFdIAAhrIFsnHIlgAAIAAiHII2AAIAABsIltHFIFzAAIAACIg");
	this.shape_81.setTransform(613.1,415.4);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f("#00CD6A").s().p("ABxFdIiajeIhsAAIAADeIibAAIAAq5IEIAAQCjAABFA3QBGA2AAB6QAACoiGAxICyD5gAiVgHIBzAAQBPAAAdgaQAcgbAAg4QAAg5gdgUQgegWhLABIh1AAg");
	this.shape_82.setTransform(447.2,415.4);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("#00CD6A").s().p("AkjFeIAAhrIFsnIIlgAAIAAiHII2AAIAABrIltHHIFzAAIAACIg");
	this.shape_83.setTransform(613.1,291.5);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f("#00CD6A").s().p("ABxFeIiajfIhsAAIAADfIibAAIAAq6IEIAAQCjgBBFA3QBGA4AAB6QAACmiGAyICyD6gAiVgHIBzAAQBPAAAdgaQAcgaAAg5QAAg4gdgWQgegUhLAAIh1AAg");
	this.shape_84.setTransform(447.2,291.5);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f("#00CD6A").s().p("AkjFdIAAhrIFsnIIlgAAIAAiHII2AAIAABsIltHHIFzAAIAACHg");
	this.shape_85.setTransform(613.1,167.6);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f("#00CD6A").s().p("ABxFdIiajfIhsAAIAADfIibAAIAAq6IEIAAQCjAABFA3QBGA4AAB5QAACoiGAwICyD6gAiVgIIBzAAQBPAAAdgaQAcgaAAg4QAAg5gdgUQgegWhLAAIh1AAg");
	this.shape_86.setTransform(447.2,167.6);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f("#00CD6A").s().p("AkjFdIAAhrIFsnHIlgAAIAAiHII2AAIAABsIltHFIFzAAIAACIg");
	this.shape_87.setTransform(613.1,43.7);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f("#00CD6A").s().p("ABxFdIiajeIhsAAIAADeIibAAIAAq5IEIAAQCjAABFA3QBGA2AAB6QAACoiGAxICyD5gAiVgHIBzAAQBPAAAdgaQAcgbAAg4QAAg5gdgUQgegWhLABIh1AAg");
	this.shape_88.setTransform(447.2,43.7);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f("#00CD6A").s().p("AkjFdIAAhrIFsnHIlgAAIAAiHII2AAIAABsIltHFIFzAAIAACIg");
	this.shape_89.setTransform(613.1,-80.2);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f("#00CD6A").s().p("ABxFdIiajfIhsAAIAADfIibAAIAAq5IEIAAQCjAABFA3QBGA2AAB6QAACoiGAwICyD6gAiVgIIBzAAQBPAAAdgZQAcgaAAg5QAAg5gdgUQgegWhLAAIh1AAg");
	this.shape_90.setTransform(447.2,-80.2);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f("#00CD6A").s().p("AkjFeIAAhrIFsnIIlgAAIAAiHII2AAIAABsIltHFIFzAAIAACJg");
	this.shape_91.setTransform(613.1,-204.1);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f("#00CD6A").s().p("ABxFeIiajfIhsAAIAADfIibAAIAAq6IEIAAQCjAABFA2QBGA4AAB6QAACmiGAyICyD6gAiVgHIBzAAQBPAAAdgaQAcgbAAg4QAAg4gdgWQgegUhLAAIh1AAg");
	this.shape_92.setTransform(447.2,-204.1);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.f("#00CD6A").s().p("AkjFeIAAhsIFsnIIlgAAIAAiHII2AAIAABsIltHHIFzAAIAACIg");
	this.shape_93.setTransform(613.1,-328);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.f("#00CD6A").s().p("ABxFeIiajgIhsAAIAADgIibAAIAAq7IEIAAQCjAABFA3QBGA3AAB6QAACniGAxICyD7gAiVgIIBzAAQBPAAAdgaQAcgaAAg4QAAg5gdgVQgegUhLgBIh1AAg");
	this.shape_94.setTransform(447.2,-328);

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.f("#00CD6A").s().p("AhNFeIAAkVIjymlICpAAICWEEICXkEICpAAIjyGlIAAEVg");
	this.shape_95.setTransform(264.1,663.2);

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.f("#00CD6A").s().p("AkkFeIAAhrIFunIIlhAAIAAiHII2AAIAABrIltHHIFyAAIAACIg");
	this.shape_96.setTransform(187,663.2);

	this.shape_97 = new cjs.Shape();
	this.shape_97.graphics.f("#00CD6A").s().p("ADUFeIhBiXIklAAIhACXIinAAIEuq6ICXAAIEtK6gAhWA+ICtAAIhXjLg");
	this.shape_97.setTransform(103.9,663.2);

	this.shape_98 = new cjs.Shape();
	this.shape_98.graphics.f("#00CD6A").s().p("ABxFeIiZjfIhsAAIAADfIidAAIAAq6IEKAAQCigBBFA3QBFA3AAB7QABCmiFAyICwD6gAiUgHIBxAAQBPAAAegaQAdgaAAg5QAAg4gfgWQgegUhJAAIh1AAg");
	this.shape_98.setTransform(21.1,663.2);

	this.shape_99 = new cjs.Shape();
	this.shape_99.graphics.f("#00CD6A").s().p("AjeEDQhphlAAicQABibBqhnQBqhpCaAAQCrAAB1CEIhiBuQhJhdhuABQhYgBg+A6Qg+A6gBBgQAABhA8A8QA6A6BTAAQB0AABIhbIBkBoQh2CGiiAAQigAAhphng");
	this.shape_99.setTransform(-66.1,662.8);

	this.shape_100 = new cjs.Shape();
	this.shape_100.graphics.f("#00CD6A").s().p("AhNFeIAAkVIjymmICpAAICWEEICXkEICpAAIjyGmIAAEVg");
	this.shape_100.setTransform(264.1,539.3);

	this.shape_101 = new cjs.Shape();
	this.shape_101.graphics.f("#00CD6A").s().p("AkkFeIAAhsIFunIIlhAAIAAiHII2AAIAABsIltHHIFyAAIAACIg");
	this.shape_101.setTransform(187,539.3);

	this.shape_102 = new cjs.Shape();
	this.shape_102.graphics.f("#00CD6A").s().p("ADUFeIhBiYIklAAIhACYIinAAIEuq7ICXAAIEtK7gAhWA9ICtAAIhXjKg");
	this.shape_102.setTransform(103.9,539.3);

	this.shape_103 = new cjs.Shape();
	this.shape_103.graphics.f("#00CD6A").s().p("ABxFeIiZjgIhsAAIAADgIidAAIAAq7IEKAAQCiAABFA3QBFA4AAB5QABCoiFAwICwD7gAiUgIIBxAAQBPAAAegaQAdgaAAg4QAAg5gfgUQgegWhJAAIh1AAg");
	this.shape_103.setTransform(21.1,539.3);

	this.shape_104 = new cjs.Shape();
	this.shape_104.graphics.f("#00CD6A").s().p("AjeEEQhphmAAicQABiaBqhpQBqhoCaABQCrgBB1CDIhiBuQhJhbhugBQhYABg+A5Qg+A5gBBhQAABhA8A7QA6A7BTABQB0gBBIhaIBkBmQh2CHiiAAQiggBhphlg");
	this.shape_104.setTransform(-66.1,538.9);

	this.shape_105 = new cjs.Shape();
	this.shape_105.graphics.f("#00CD6A").s().p("AhNFdIAAkTIjymmICpAAICWEEICXkEICpAAIjyGmIAAETg");
	this.shape_105.setTransform(264.1,415.4);

	this.shape_106 = new cjs.Shape();
	this.shape_106.graphics.f("#00CD6A").s().p("AkkFdIAAhrIFunHIlhAAIAAiHII2AAIAABsIltHFIFyAAIAACIg");
	this.shape_106.setTransform(187,415.4);

	this.shape_107 = new cjs.Shape();
	this.shape_107.graphics.f("#00CD6A").s().p("ADUFdIhBiWIklAAIhACWIinAAIEuq5ICXAAIEtK5gAhWA9ICtAAIhXjJg");
	this.shape_107.setTransform(103.9,415.4);

	this.shape_108 = new cjs.Shape();
	this.shape_108.graphics.f("#00CD6A").s().p("ABxFdIiZjeIhsAAIAADeIidAAIAAq5IEKAAQCiAABFA3QBFA2AAB6QABCoiFAxICwD5gAiUgHIBxAAQBPAAAegaQAdgbAAg4QAAg5gfgUQgegWhJABIh1AAg");
	this.shape_108.setTransform(21.1,415.4);

	this.shape_109 = new cjs.Shape();
	this.shape_109.graphics.f("#00CD6A").s().p("AjeEEQhphnAAibQABibBqhoQBqhnCaAAQCrAAB1CCIhiBvQhJhdhuAAQhYAAg+A6Qg+A6gBBgQAABiA8A6QA6A8BTgBQB0ABBIhbIBkBmQh2CGiiAAQigAAhphlg");
	this.shape_109.setTransform(-66.1,415);

	this.shape_110 = new cjs.Shape();
	this.shape_110.graphics.f("#00CD6A").s().p("AhNFeIAAkVIjymlICpAAICWEEICXkEICpAAIjyGlIAAEVg");
	this.shape_110.setTransform(264.1,291.5);

	this.shape_111 = new cjs.Shape();
	this.shape_111.graphics.f("#00CD6A").s().p("AkkFeIAAhrIFunIIlhAAIAAiHII2AAIAABrIltHHIFyAAIAACIg");
	this.shape_111.setTransform(187,291.5);

	this.shape_112 = new cjs.Shape();
	this.shape_112.graphics.f("#00CD6A").s().p("ADUFeIhBiXIklAAIhACXIinAAIEuq6ICXAAIEtK6gAhWA+ICtAAIhXjLg");
	this.shape_112.setTransform(103.9,291.5);

	this.shape_113 = new cjs.Shape();
	this.shape_113.graphics.f("#00CD6A").s().p("ABxFeIiZjfIhsAAIAADfIidAAIAAq6IEKAAQCigBBFA3QBFA4AAB6QABCmiFAyICwD6gAiUgHIBxAAQBPAAAegaQAdgaAAg5QAAg4gfgWQgegUhJAAIh1AAg");
	this.shape_113.setTransform(21.1,291.5);

	this.shape_114 = new cjs.Shape();
	this.shape_114.graphics.f("#00CD6A").s().p("AjeEDQhphlAAicQABibBqhnQBqhpCaAAQCrAAB1CEIhiBtQhJhchuABQhYgBg+A6Qg+A6gBBgQAABhA8A8QA6A6BTAAQB0AABIhbIBkBoQh2CGiiAAQigAAhphng");
	this.shape_114.setTransform(-66.1,291.1);

	this.shape_115 = new cjs.Shape();
	this.shape_115.graphics.f("#00CD6A").s().p("AhNFdIAAkUIjymmICpAAICWEEICXkEICpAAIjyGmIAAEUg");
	this.shape_115.setTransform(264.1,167.6);

	this.shape_116 = new cjs.Shape();
	this.shape_116.graphics.f("#00CD6A").s().p("AkkFdIAAhrIFunIIlhAAIAAiHII2AAIAABsIltHHIFyAAIAACHg");
	this.shape_116.setTransform(187,167.6);

	this.shape_117 = new cjs.Shape();
	this.shape_117.graphics.f("#00CD6A").s().p("ADUFdIhBiXIklAAIhACXIinAAIEuq6ICXAAIEtK6gAhWA9ICtAAIhXjKg");
	this.shape_117.setTransform(103.9,167.6);

	this.shape_118 = new cjs.Shape();
	this.shape_118.graphics.f("#00CD6A").s().p("ABxFdIiZjfIhsAAIAADfIidAAIAAq6IEKAAQCiAABFA3QBFA4AAB5QABCoiFAwICwD6gAiUgIIBxAAQBPAAAegaQAdgaAAg4QAAg5gfgUQgegWhJAAIh1AAg");
	this.shape_118.setTransform(21.1,167.6);

	this.shape_119 = new cjs.Shape();
	this.shape_119.graphics.f("#00CD6A").s().p("AjeEEQhphmAAicQABiaBqhpQBqhoCaABQCrgBB1CDIhiBuQhJhbhugBQhYABg+A5Qg+A5gBBhQAABhA8A7QA6A7BTABQB0gBBIhaIBkBmQh2CHiiAAQiggBhphlg");
	this.shape_119.setTransform(-66.1,167.2);

	this.shape_120 = new cjs.Shape();
	this.shape_120.graphics.f("#00CD6A").s().p("AhNFdIAAkTIjymmICpAAICWEEICXkEICpAAIjyGmIAAETg");
	this.shape_120.setTransform(264.1,43.7);

	this.shape_121 = new cjs.Shape();
	this.shape_121.graphics.f("#00CD6A").s().p("AkkFdIAAhrIFunHIlhAAIAAiHII2AAIAABsIltHFIFyAAIAACIg");
	this.shape_121.setTransform(187,43.7);

	this.shape_122 = new cjs.Shape();
	this.shape_122.graphics.f("#00CD6A").s().p("ADUFdIhBiWIklAAIhACWIinAAIEuq5ICXAAIEtK5gAhWA9ICtAAIhXjJg");
	this.shape_122.setTransform(103.9,43.7);

	this.shape_123 = new cjs.Shape();
	this.shape_123.graphics.f("#00CD6A").s().p("ABxFdIiZjeIhsAAIAADeIidAAIAAq5IEKAAQCiAABFA3QBFA2AAB6QABCoiFAxICwD5gAiUgHIBxAAQBPAAAegaQAdgbAAg4QAAg5gfgUQgegWhJABIh1AAg");
	this.shape_123.setTransform(21.1,43.7);

	this.shape_124 = new cjs.Shape();
	this.shape_124.graphics.f("#00CD6A").s().p("AjeEEQhphnAAibQABibBqhoQBqhnCaAAQCrAAB1CCIhiBvQhJhdhuAAQhYAAg+A6Qg+A6gBBgQAABiA8A6QA6A8BTgBQB0ABBIhbIBkBmQh2CGiiAAQigAAhphlg");
	this.shape_124.setTransform(-66.1,43.3);

	this.shape_125 = new cjs.Shape();
	this.shape_125.graphics.f("#00CD6A").s().p("AhNFdIAAkTIjymmICpAAICWEDICXkDICpAAIjyGmIAAETg");
	this.shape_125.setTransform(264.1,-80.2);

	this.shape_126 = new cjs.Shape();
	this.shape_126.graphics.f("#00CD6A").s().p("AkkFdIAAhrIFunHIlhAAIAAiHII2AAIAABsIltHFIFyAAIAACIg");
	this.shape_126.setTransform(187,-80.2);

	this.shape_127 = new cjs.Shape();
	this.shape_127.graphics.f("#00CD6A").s().p("ADUFdIhBiXIklAAIhACXIinAAIEuq5ICXAAIEtK5gAhWA9ICtAAIhXjJg");
	this.shape_127.setTransform(103.9,-80.2);

	this.shape_128 = new cjs.Shape();
	this.shape_128.graphics.f("#00CD6A").s().p("ABxFdIiZjfIhsAAIAADfIidAAIAAq5IEKAAQCiAABFA3QBFA2AAB6QABCoiFAwICwD6gAiUgIIBxAAQBPAAAegZQAdgaAAg5QAAg5gfgUQgegWhJAAIh1AAg");
	this.shape_128.setTransform(21.1,-80.2);

	this.shape_129 = new cjs.Shape();
	this.shape_129.graphics.f("#00CD6A").s().p("AjeEEQhphnAAibQABibBqhoQBqhnCaAAQCrAAB1CCIhiBvQhJhdhuAAQhYAAg+A6Qg+A5gBBhQAABiA8A6QA6A8BTAAQB0AABIhbIBkBmQh2CGiiAAQigAAhphlg");
	this.shape_129.setTransform(-66.1,-80.6);

	this.shape_130 = new cjs.Shape();
	this.shape_130.graphics.f("#00CD6A").s().p("AhNFeIAAkUIjymmICpAAICWEEICXkEICpAAIjyGmIAAEUg");
	this.shape_130.setTransform(264.1,-204.1);

	this.shape_131 = new cjs.Shape();
	this.shape_131.graphics.f("#00CD6A").s().p("AkkFeIAAhrIFunIIlhAAIAAiHII2AAIAABsIltHFIFyAAIAACJg");
	this.shape_131.setTransform(187,-204.1);

	this.shape_132 = new cjs.Shape();
	this.shape_132.graphics.f("#00CD6A").s().p("ADUFeIhBiXIklAAIhACXIinAAIEuq6ICXAAIEtK6gAhWA+ICtAAIhXjLg");
	this.shape_132.setTransform(103.9,-204.1);

	this.shape_133 = new cjs.Shape();
	this.shape_133.graphics.f("#00CD6A").s().p("ABxFeIiZjfIhsAAIAADfIidAAIAAq6IEKAAQCiAABFA2QBFA4AAB6QABCmiFAyICwD6gAiUgHIBxAAQBPAAAegaQAdgbAAg4QAAg4gfgWQgegUhJAAIh1AAg");
	this.shape_133.setTransform(21.1,-204.1);

	this.shape_134 = new cjs.Shape();
	this.shape_134.graphics.f("#00CD6A").s().p("AjeEDQhphlAAicQABibBqhnQBqhpCaAAQCrAAB1CEIhiBuQhJhchuAAQhYgBg+A6Qg+A6gBBgQAABhA8A8QA6A6BTAAQB0ABBIhcIBkBoQh2CFiiAAQigABhphng");
	this.shape_134.setTransform(-66.1,-204.5);

	this.shape_135 = new cjs.Shape();
	this.shape_135.graphics.f("#00CD6A").s().p("AhNFeIAAkVIjymmICpAAICWEEICXkEICpAAIjyGmIAAEVg");
	this.shape_135.setTransform(264.1,-328);

	this.shape_136 = new cjs.Shape();
	this.shape_136.graphics.f("#00CD6A").s().p("AkkFeIAAhsIFunIIlhAAIAAiHII2AAIAABsIltHHIFyAAIAACIg");
	this.shape_136.setTransform(187,-328);

	this.shape_137 = new cjs.Shape();
	this.shape_137.graphics.f("#00CD6A").s().p("ADUFeIhBiYIklAAIhACYIinAAIEuq7ICXAAIEtK7gAhWA+ICtAAIhXjLg");
	this.shape_137.setTransform(103.9,-328);

	this.shape_138 = new cjs.Shape();
	this.shape_138.graphics.f("#00CD6A").s().p("ABxFeIiZjgIhsAAIAADgIidAAIAAq7IEKAAQCiAABFA3QBFA3AAB6QABCniFAxICwD7gAiUgIIBxAAQBPAAAegaQAdgaAAg4QAAg5gfgVQgegUhJgBIh1AAg");
	this.shape_138.setTransform(21.1,-328);

	this.shape_139 = new cjs.Shape();
	this.shape_139.graphics.f("#00CD6A").s().p("AjeEDQhphlAAicQABiaBqhpQBqhoCaAAQCrAAB1CDIhiBuQhJhchuAAQhYABg+A5Qg+A5gBBhQAABiA8A6QA6A7BTABQB0AABIhcIBkBoQh2CGiiAAQigAAhphng");
	this.shape_139.setTransform(-66.1,-328.4);

	this.shape_140 = new cjs.Shape();
	this.shape_140.graphics.f("#00CD6A").s().p("AhNFeIAAkVIjymlICpAAICWEEICXkEICpAAIjyGlIAAEVg");
	this.shape_140.setTransform(-150.4,663.2);

	this.shape_141 = new cjs.Shape();
	this.shape_141.graphics.f("#00CD6A").s().p("ADTFeIhBiXIkjAAIhCCXIilAAIEtq6ICWAAIEvK6gAhXA+ICvAAIhXjLg");
	this.shape_141.setTransform(-310.6,663.2);

	this.shape_142 = new cjs.Shape();
	this.shape_142.graphics.f("#00CD6A").s().p("AjeEDQhohlAAicQAAibBqhnQBrhpCYAAQCtAABzCEIhgBuQhLhdhuABQhWgBg/A6Qg/A6AABgQABBhA6A8QA7A6BUAAQByAABKhbIBkBoQh4CGihAAQihAAhohng");
	this.shape_142.setTransform(-480.6,662.8);

	this.shape_143 = new cjs.Shape();
	this.shape_143.graphics.f("#00CD6A").s().p("AhNFeIAAkVIjymmICpAAICWEEICXkEICpAAIjyGmIAAEVg");
	this.shape_143.setTransform(-150.4,539.3);

	this.shape_144 = new cjs.Shape();
	this.shape_144.graphics.f("#00CD6A").s().p("ADTFeIhBiYIkjAAIhCCYIilAAIEtq7ICWAAIEvK7gAhXA9ICvAAIhXjKg");
	this.shape_144.setTransform(-310.6,539.3);

	this.shape_145 = new cjs.Shape();
	this.shape_145.graphics.f("#00CD6A").s().p("AjeEEQhohmAAicQAAiaBqhpQBrhoCYABQCtgBBzCDIhgBuQhLhbhugBQhWABg/A5Qg/A5AABhQABBhA6A7QA7A7BUABQBygBBKhaIBkBmQh4CHihAAQihgBhohlg");
	this.shape_145.setTransform(-480.6,538.9);

	this.shape_146 = new cjs.Shape();
	this.shape_146.graphics.f("#00CD6A").s().p("AhNFdIAAkTIjymmICpAAICWEEICXkEICpAAIjyGmIAAETg");
	this.shape_146.setTransform(-150.4,415.4);

	this.shape_147 = new cjs.Shape();
	this.shape_147.graphics.f("#00CD6A").s().p("ADTFdIhBiWIkjAAIhCCWIilAAIEtq5ICWAAIEvK5gAhXA9ICvAAIhXjJg");
	this.shape_147.setTransform(-310.6,415.4);

	this.shape_148 = new cjs.Shape();
	this.shape_148.graphics.f("#00CD6A").s().p("AjeEEQhohnAAibQAAibBqhoQBrhnCYAAQCtAABzCCIhgBvQhLhdhuAAQhWAAg/A6Qg/A6AABgQABBiA6A6QA7A8BUgBQByABBKhbIBkBmQh4CGihAAQihAAhohlg");
	this.shape_148.setTransform(-480.6,415);

	this.shape_149 = new cjs.Shape();
	this.shape_149.graphics.f("#00CD6A").s().p("AhNFeIAAkVIjymlICpAAICWEEICXkEICpAAIjyGlIAAEVg");
	this.shape_149.setTransform(-150.4,291.5);

	this.shape_150 = new cjs.Shape();
	this.shape_150.graphics.f("#00CD6A").s().p("ADTFeIhBiXIkjAAIhCCXIilAAIEtq6ICWAAIEvK6gAhXA+ICvAAIhXjLg");
	this.shape_150.setTransform(-310.6,291.5);

	this.shape_151 = new cjs.Shape();
	this.shape_151.graphics.f("#00CD6A").s().p("AjeEDQhohlAAicQAAibBqhnQBrhpCYAAQCtAABzCEIhgBtQhLhchuABQhWgBg/A6Qg/A6AABgQABBhA6A8QA7A6BUAAQByAABKhbIBkBoQh4CGihAAQihAAhohng");
	this.shape_151.setTransform(-480.6,291.1);

	this.shape_152 = new cjs.Shape();
	this.shape_152.graphics.f("#00CD6A").s().p("AhNFdIAAkUIjymmICpAAICWEEICXkEICpAAIjyGmIAAEUg");
	this.shape_152.setTransform(-150.4,167.6);

	this.shape_153 = new cjs.Shape();
	this.shape_153.graphics.f("#00CD6A").s().p("ADTFdIhBiXIkjAAIhCCXIilAAIEtq6ICWAAIEvK6gAhXA9ICvAAIhXjKg");
	this.shape_153.setTransform(-310.6,167.6);

	this.shape_154 = new cjs.Shape();
	this.shape_154.graphics.f("#00CD6A").s().p("AjeEEQhohmAAicQAAiaBqhpQBrhoCYABQCtgBBzCDIhgBuQhLhbhugBQhWABg/A5Qg/A5AABhQABBhA6A7QA7A7BUABQBygBBKhaIBkBmQh4CHihAAQihgBhohlg");
	this.shape_154.setTransform(-480.6,167.2);

	this.shape_155 = new cjs.Shape();
	this.shape_155.graphics.f("#00CD6A").s().p("AhNFdIAAkTIjymmICpAAICWEEICXkEICpAAIjyGmIAAETg");
	this.shape_155.setTransform(-150.4,43.7);

	this.shape_156 = new cjs.Shape();
	this.shape_156.graphics.f("#00CD6A").s().p("ADTFdIhBiWIkjAAIhCCWIilAAIEtq5ICWAAIEvK5gAhXA9ICvAAIhXjJg");
	this.shape_156.setTransform(-310.6,43.7);

	this.shape_157 = new cjs.Shape();
	this.shape_157.graphics.f("#00CD6A").s().p("AjeEEQhohnAAibQAAibBqhoQBrhnCYAAQCtAABzCCIhgBvQhLhdhuAAQhWAAg/A6Qg/A6AABgQABBiA6A6QA7A8BUgBQByABBKhbIBkBmQh4CGihAAQihAAhohlg");
	this.shape_157.setTransform(-480.6,43.3);

	this.shape_158 = new cjs.Shape();
	this.shape_158.graphics.f("#00CD6A").s().p("AhNFdIAAkTIjymmICpAAICWEDICXkDICpAAIjyGmIAAETg");
	this.shape_158.setTransform(-150.4,-80.2);

	this.shape_159 = new cjs.Shape();
	this.shape_159.graphics.f("#00CD6A").s().p("ADTFdIhBiXIkjAAIhCCXIilAAIEtq5ICWAAIEvK5gAhXA9ICvAAIhXjJg");
	this.shape_159.setTransform(-310.6,-80.2);

	this.shape_160 = new cjs.Shape();
	this.shape_160.graphics.f("#00CD6A").s().p("AjeEEQhohnAAibQAAibBqhoQBrhnCYAAQCtAABzCCIhgBvQhLhdhuAAQhWAAg/A6Qg/A5AABhQABBiA6A6QA7A8BUAAQByAABKhbIBkBmQh4CGihAAQihAAhohlg");
	this.shape_160.setTransform(-480.6,-80.6);

	this.shape_161 = new cjs.Shape();
	this.shape_161.graphics.f("#00CD6A").s().p("AhNFeIAAkUIjymmICpAAICWEEICXkEICpAAIjyGmIAAEUg");
	this.shape_161.setTransform(-150.4,-204.1);

	this.shape_162 = new cjs.Shape();
	this.shape_162.graphics.f("#00CD6A").s().p("ADTFeIhBiXIkjAAIhCCXIilAAIEtq6ICWAAIEvK6gAhXA+ICvAAIhXjLg");
	this.shape_162.setTransform(-310.6,-204.1);

	this.shape_163 = new cjs.Shape();
	this.shape_163.graphics.f("#00CD6A").s().p("AjeEDQhohlAAicQAAibBqhnQBrhpCYAAQCtAABzCEIhgBuQhLhchuAAQhWgBg/A6Qg/A6AABgQABBhA6A8QA7A6BUAAQByABBKhcIBkBoQh4CFihAAQihABhohng");
	this.shape_163.setTransform(-480.6,-204.5);

	this.shape_164 = new cjs.Shape();
	this.shape_164.graphics.f("#00CD6A").s().p("AhNFeIAAkVIjymmICpAAICWEEICXkEICpAAIjyGmIAAEVg");
	this.shape_164.setTransform(-150.4,-328);

	this.shape_165 = new cjs.Shape();
	this.shape_165.graphics.f("#00CD6A").s().p("ADTFeIhBiYIkjAAIhCCYIilAAIEtq7ICWAAIEvK7gAhXA+ICvAAIhXjLg");
	this.shape_165.setTransform(-310.6,-328);

	this.shape_166 = new cjs.Shape();
	this.shape_166.graphics.f("#00CD6A").s().p("AjeEDQhohlAAicQAAiaBqhpQBrhoCYAAQCtAABzCDIhgBuQhLhchuAAQhWABg/A5Qg/A5AABhQABBiA6A6QA7A7BUABQByAABKhcIBkBoQh4CGihAAQihAAhohng");
	this.shape_166.setTransform(-480.6,-328.4);

	var maskedShapeInstanceList = [this.instance_3,this.shape_15,this.shape_16,this.shape_17,this.shape_18,this.shape_19,this.shape_20,this.shape_21,this.shape_22,this.shape_23,this.shape_24,this.shape_25,this.shape_26,this.shape_27,this.shape_28,this.shape_29,this.shape_30,this.shape_31,this.shape_32,this.shape_33,this.shape_34,this.shape_35,this.shape_36,this.shape_37,this.shape_38,this.shape_39,this.shape_40,this.shape_41,this.shape_42,this.shape_43,this.shape_44,this.shape_45,this.shape_46,this.shape_47,this.shape_48,this.shape_49,this.shape_50,this.shape_51,this.shape_52,this.shape_53,this.shape_54,this.shape_55,this.shape_56,this.shape_57,this.shape_58,this.shape_59,this.shape_60,this.shape_61,this.shape_62,this.shape_63,this.shape_64,this.shape_65,this.shape_66,this.shape_67,this.shape_68,this.shape_69,this.shape_70,this.shape_71,this.shape_72,this.shape_73,this.shape_74,this.shape_75,this.shape_76,this.shape_77,this.shape_78,this.shape_79,this.shape_80,this.shape_81,this.shape_82,this.shape_83,this.shape_84,this.shape_85,this.shape_86,this.shape_87,this.shape_88,this.shape_89,this.shape_90,this.shape_91,this.shape_92,this.shape_93,this.shape_94,this.shape_95,this.shape_96,this.shape_97,this.shape_98,this.shape_99,this.shape_100,this.shape_101,this.shape_102,this.shape_103,this.shape_104,this.shape_105,this.shape_106,this.shape_107,this.shape_108,this.shape_109,this.shape_110,this.shape_111,this.shape_112,this.shape_113,this.shape_114,this.shape_115,this.shape_116,this.shape_117,this.shape_118,this.shape_119,this.shape_120,this.shape_121,this.shape_122,this.shape_123,this.shape_124,this.shape_125,this.shape_126,this.shape_127,this.shape_128,this.shape_129,this.shape_130,this.shape_131,this.shape_132,this.shape_133,this.shape_134,this.shape_135,this.shape_136,this.shape_137,this.shape_138,this.shape_139,this.shape_140,this.shape_141,this.shape_142,this.shape_143,this.shape_144,this.shape_145,this.shape_146,this.shape_147,this.shape_148,this.shape_149,this.shape_150,this.shape_151,this.shape_152,this.shape_153,this.shape_154,this.shape_155,this.shape_156,this.shape_157,this.shape_158,this.shape_159,this.shape_160,this.shape_161,this.shape_162,this.shape_163,this.shape_164,this.shape_165,this.shape_166];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_3}]},17).to({state:[{t:this.instance_3}]},14).to({state:[{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16,p:{x:-350.3,y:131.8}},{t:this.shape_15}]},1).to({state:[{t:this.shape_26,p:{y:-141.4,x:-480.6}},{t:this.shape_25},{t:this.shape_24,p:{y:-141}},{t:this.shape_23,p:{y:-141}},{t:this.shape_22}]},5).to({state:[{t:this.shape_26,p:{y:-141.4,x:-480.6}},{t:this.shape_25},{t:this.shape_31},{t:this.shape_30},{t:this.shape_22},{t:this.shape_29},{t:this.shape_28},{t:this.shape_24,p:{y:-17.1}},{t:this.shape_23,p:{y:-17.1}},{t:this.shape_27,p:{y:-17.1,x:-150.4}}]},5).to({state:[{t:this.shape_26,p:{y:-141.4,x:-480.6}},{t:this.shape_25},{t:this.shape_31},{t:this.shape_30},{t:this.shape_22},{t:this.shape_29},{t:this.shape_28},{t:this.shape_24,p:{y:-17.1}},{t:this.shape_23,p:{y:-17.1}},{t:this.shape_27,p:{y:-17.1,x:-150.4}},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34,p:{y:106.8,x:-310.6}},{t:this.shape_33},{t:this.shape_32,p:{y:106.8,x:-150.4}}]},6).to({state:[{t:this.shape_76,p:{x:-480.6}},{t:this.shape_75},{t:this.shape_74,p:{x:-310.6}},{t:this.shape_73},{t:this.shape_72,p:{x:-150.4}},{t:this.shape_71,p:{x:-480.6}},{t:this.shape_70},{t:this.shape_69,p:{x:-310.6}},{t:this.shape_68},{t:this.shape_67,p:{x:-150.4}},{t:this.shape_66,p:{x:-480.6}},{t:this.shape_65},{t:this.shape_64,p:{x:-310.6}},{t:this.shape_63},{t:this.shape_16,p:{x:-150.4,y:-80.2}},{t:this.shape_62,p:{x:-480.6}},{t:this.shape_61},{t:this.shape_60,p:{x:-310.6}},{t:this.shape_59},{t:this.shape_58,p:{x:-150.4}},{t:this.shape_57,p:{x:-480.6}},{t:this.shape_56},{t:this.shape_55,p:{x:-310.6}},{t:this.shape_54},{t:this.shape_53,p:{x:-150.4}},{t:this.shape_26,p:{y:291.1,x:-480.6}},{t:this.shape_52},{t:this.shape_51,p:{x:-310.6}},{t:this.shape_50},{t:this.shape_49,p:{x:-150.4}},{t:this.shape_48,p:{x:-480.6}},{t:this.shape_47},{t:this.shape_34,p:{y:415.4,x:-310.6}},{t:this.shape_46},{t:this.shape_32,p:{y:415.4,x:-150.4}},{t:this.shape_45,p:{x:-480.6}},{t:this.shape_44},{t:this.shape_43,p:{x:-310.6}},{t:this.shape_42},{t:this.shape_27,p:{y:539.3,x:-150.4}},{t:this.shape_41,p:{x:-480.6}},{t:this.shape_40},{t:this.shape_39,p:{x:-310.6}},{t:this.shape_38},{t:this.shape_37,p:{x:-150.4}}]},7).to({state:[{t:this.shape_166},{t:this.shape_75},{t:this.shape_165},{t:this.shape_73},{t:this.shape_164},{t:this.shape_163},{t:this.shape_70},{t:this.shape_162},{t:this.shape_68},{t:this.shape_161},{t:this.shape_160},{t:this.shape_65},{t:this.shape_159},{t:this.shape_63},{t:this.shape_158},{t:this.shape_157},{t:this.shape_61},{t:this.shape_156},{t:this.shape_59},{t:this.shape_155},{t:this.shape_154},{t:this.shape_56},{t:this.shape_153},{t:this.shape_54},{t:this.shape_152},{t:this.shape_151},{t:this.shape_52},{t:this.shape_150},{t:this.shape_50},{t:this.shape_149},{t:this.shape_148},{t:this.shape_47},{t:this.shape_147},{t:this.shape_46},{t:this.shape_146},{t:this.shape_145},{t:this.shape_44},{t:this.shape_144},{t:this.shape_42},{t:this.shape_143},{t:this.shape_142},{t:this.shape_40},{t:this.shape_141},{t:this.shape_38},{t:this.shape_140},{t:this.shape_139},{t:this.shape_138},{t:this.shape_137},{t:this.shape_136},{t:this.shape_135},{t:this.shape_134},{t:this.shape_133},{t:this.shape_132},{t:this.shape_131},{t:this.shape_130},{t:this.shape_129},{t:this.shape_128},{t:this.shape_127},{t:this.shape_126},{t:this.shape_125},{t:this.shape_124},{t:this.shape_123},{t:this.shape_122},{t:this.shape_121},{t:this.shape_120},{t:this.shape_119},{t:this.shape_118},{t:this.shape_117},{t:this.shape_116},{t:this.shape_115},{t:this.shape_114},{t:this.shape_113},{t:this.shape_112},{t:this.shape_111},{t:this.shape_110},{t:this.shape_109},{t:this.shape_108},{t:this.shape_107},{t:this.shape_106},{t:this.shape_105},{t:this.shape_104},{t:this.shape_103},{t:this.shape_102},{t:this.shape_101},{t:this.shape_100},{t:this.shape_99},{t:this.shape_98},{t:this.shape_97},{t:this.shape_96},{t:this.shape_95},{t:this.shape_76,p:{x:360}},{t:this.shape_94},{t:this.shape_74,p:{x:530}},{t:this.shape_93},{t:this.shape_72,p:{x:690.2}},{t:this.shape_71,p:{x:360}},{t:this.shape_92},{t:this.shape_69,p:{x:530}},{t:this.shape_91},{t:this.shape_67,p:{x:690.2}},{t:this.shape_66,p:{x:360}},{t:this.shape_90},{t:this.shape_64,p:{x:530}},{t:this.shape_89},{t:this.shape_16,p:{x:690.2,y:-80.2}},{t:this.shape_62,p:{x:360}},{t:this.shape_88},{t:this.shape_60,p:{x:530}},{t:this.shape_87},{t:this.shape_58,p:{x:690.2}},{t:this.shape_57,p:{x:360}},{t:this.shape_86},{t:this.shape_55,p:{x:530}},{t:this.shape_85},{t:this.shape_53,p:{x:690.2}},{t:this.shape_26,p:{y:291.1,x:360}},{t:this.shape_84},{t:this.shape_51,p:{x:530}},{t:this.shape_83},{t:this.shape_49,p:{x:690.2}},{t:this.shape_48,p:{x:360}},{t:this.shape_82},{t:this.shape_34,p:{y:415.4,x:530}},{t:this.shape_81},{t:this.shape_32,p:{y:415.4,x:690.2}},{t:this.shape_45,p:{x:360}},{t:this.shape_80},{t:this.shape_43,p:{x:530}},{t:this.shape_79},{t:this.shape_27,p:{y:539.3,x:690.2}},{t:this.shape_41,p:{x:360}},{t:this.shape_78},{t:this.shape_39,p:{x:530}},{t:this.shape_77},{t:this.shape_37,p:{x:690.2}}]},7).to({state:[{t:this.shape_76,p:{x:360}},{t:this.shape_94},{t:this.shape_74,p:{x:530}},{t:this.shape_93},{t:this.shape_72,p:{x:690.2}},{t:this.shape_71,p:{x:360}},{t:this.shape_92},{t:this.shape_69,p:{x:530}},{t:this.shape_91},{t:this.shape_67,p:{x:690.2}},{t:this.shape_66,p:{x:360}},{t:this.shape_90},{t:this.shape_64,p:{x:530}},{t:this.shape_89},{t:this.shape_16,p:{x:690.2,y:-80.2}},{t:this.shape_62,p:{x:360}},{t:this.shape_88},{t:this.shape_60,p:{x:530}},{t:this.shape_87},{t:this.shape_58,p:{x:690.2}},{t:this.shape_57,p:{x:360}},{t:this.shape_86},{t:this.shape_55,p:{x:530}},{t:this.shape_85},{t:this.shape_53,p:{x:690.2}},{t:this.shape_26,p:{y:291.1,x:360}},{t:this.shape_84},{t:this.shape_51,p:{x:530}},{t:this.shape_83},{t:this.shape_49,p:{x:690.2}},{t:this.shape_48,p:{x:360}},{t:this.shape_82},{t:this.shape_34,p:{y:415.4,x:530}},{t:this.shape_81},{t:this.shape_32,p:{y:415.4,x:690.2}},{t:this.shape_45,p:{x:360}},{t:this.shape_80},{t:this.shape_43,p:{x:530}},{t:this.shape_79},{t:this.shape_27,p:{y:539.3,x:690.2}},{t:this.shape_41,p:{x:360}},{t:this.shape_78},{t:this.shape_39,p:{x:530}},{t:this.shape_77},{t:this.shape_37,p:{x:690.2}}]},7).to({state:[]},8).to({state:[]},3).wait(41));
	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(17).to({_off:false},0).to({x:-276.8},14,cjs.Ease.quartOut).to({_off:true},1).wait(89));

	// masque Titre
	this.instance_4 = new lib.masque_generique();
	this.instance_4.parent = this;
	this.instance_4.setTransform(221.1,-163.9,0.013,0.471,0,0,0,0,17.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).to({regX:0.5,scaleX:1.92,x:-75.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.01,x:-68.7},10,cjs.Ease.quartInOut).to({_off:true},1).wait(101));

	// masque Titre
	this.instance_5 = new lib.masque_generique();
	this.instance_5.parent = this;
	this.instance_5.setTransform(-228.9,-13.9,0.013,0.471,0,0,0,0,17.2);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(4).to({_off:false},0).to({regX:0.5,scaleX:1.92,x:-527.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.01,x:-528.7},10,cjs.Ease.quartInOut).to({_off:true},1).wait(97));

	// _LETTRES_CLIENT
	this.shape_167 = new cjs.Shape();
	this.shape_167.graphics.f("#00CD6A").s().p("AgJA0QgEgEAAgGQAAgFAEgFQAEgEAFAAQAGAAAEAEQAEAFAAAFQAAAGgEAEQgEAFgGAAQgFAAgEgFgAgJAVIgEg5IAAgUIAbAAIAAAUIgFA5g");
	this.shape_167.setTransform(-357.9,-14.1);

	this.shape_168 = new cjs.Shape();
	this.shape_168.graphics.f("#00CD6A").s().p("AAbA4Ig1hFIAABFIgZAAIAAhvIAYAAIA2BHIAAhHIAZAAIAABvg");
	this.shape_168.setTransform(-378.9,-14.1);

	this.shape_169 = new cjs.Shape();
	this.shape_169.graphics.f("#00CD6A").s().p("AgoA4IAAhvIBPAAIAAAXIg2AAIAAAWIAxAAIAAAUIgxAAIAAAYIA4AAIAAAWg");
	this.shape_169.setTransform(-402.4,-14.1);

	this.shape_170 = new cjs.Shape();
	this.shape_170.graphics.f("#00CD6A").s().p("AgMA4IAAhvIAZAAIAABvg");
	this.shape_170.setTransform(-422.5,-14.1);

	this.shape_171 = new cjs.Shape();
	this.shape_171.graphics.f("#00CD6A").s().p("AgCA5IADgYIgNAAIgEAYIgZAAIAFgYIgQAAIAFgWIAPAAIADgTIgRAAIAEgWIARAAIAEgaIAXAAIgDAaIANAAIAEgaIAZAAIgFAaIARAAIgEAWIgRAAIgDATIASAAIgEAWIgSAAIgEAYgAgJALIAOAAIAEgTIgOAAg");
	this.shape_171.setTransform(-459.4,-14.3);

	this.shape_172 = new cjs.Shape();
	this.shape_172.graphics.f("#00CD6A").s().p("AgjA4IAAhvIAZAAIAABYIAuAAIAAAXg");
	this.shape_172.setTransform(-497.6,-14.1);

	this.shape_173 = new cjs.Shape();
	this.shape_173.graphics.f("#00CD6A").s().p("AgjApQgRgQAAgYQAAgZARgQQASgRAXAAQAcAAASAVIgPASQgMgPgSAAQgNAAgKAJQgJAJgBAPQAAAPAKAKQAJAKANAAQASAAALgPIAQARQgTAVgZAAQgZAAgRgRg");
	this.shape_173.setTransform(-520.4,-14.2);

	this.shape_174 = new cjs.Shape();
	this.shape_174.graphics.f("#00CD6A").s().p("AgMA4IAAhZIgfAAIAAgWIBXAAIAAAWIggAAIAABZg");
	this.shape_174.setTransform(-411.9,-14.1);

	this.shape_175 = new cjs.Shape();
	this.shape_175.graphics.f("#00CD6A").s().p("AAbA4Ig0hFIAABFIgaAAIAAhvIAYAAIA2BHIAAhHIAZAAIAABvg");
	this.shape_175.setTransform(-435.3,-14.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_173},{t:this.shape_172},{t:this.shape_171},{t:this.shape_170,p:{x:-422.5}},{t:this.shape_169,p:{x:-402.4}},{t:this.shape_168},{t:this.shape_167,p:{x:-357.9}}]},13).to({state:[{t:this.shape_173},{t:this.shape_172},{t:this.shape_171},{t:this.shape_167,p:{x:-422.7}}]},5).to({state:[{t:this.shape_173},{t:this.shape_172},{t:this.shape_170,p:{x:-478.9}},{t:this.shape_169,p:{x:-458.8}},{t:this.shape_175},{t:this.shape_174}]},5).to({state:[]},11).to({state:[]},46).wait(41));

	// Layer_11
	this.instance_6 = new lib.masqueTexte("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(-328.9,105.9,0.9,0.85,0,0,0,0.1,108.9);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(37).to({_off:false},0).to({_off:true},40).wait(3).to({_off:false,regX:0.2,regY:108.8,rotation:180,x:-329,y:105.8},0).to({_off:true},40).wait(1));

	// _TEXTE_PRESENTATION
	this.shape_176 = new cjs.Shape();
	this.shape_176.graphics.f("#FFFFFF").s().p("AAoBrIgpg6IgpA6IhFAAIBMhsIhIhqIBIAAIAkA6IAlg6IBHAAIhKBpIBMBtg");
	this.shape_176.setTransform(-145.6,133.5);

	this.shape_177 = new cjs.Shape();
	this.shape_177.graphics.f("#FFFFFF").s().p("AhMBXQgYgWAAgsIAAiBIA9AAIAAB0QAAAxAkAAQARAAANgMQANgNAAgZIAAhzIA9AAIAADWIg9AAIAAgcQgWAfgiAAQgkAAgYgWg");
	this.shape_177.setTransform(-170.2,133.6);

	this.shape_178 = new cjs.Shape();
	this.shape_178.graphics.f("#FFFFFF").s().p("AhJBbQgXgUAAghQAAghAYgPQAYgQApAAIAsAAIAAgCQAAgigkAAQgPAAgRAGQgRAGgNAKIgbgnQArgfA1AAQAnAAAZAUQAZATAAArIAACIIg4AAIAAgaQgYAdghAAQghAAgYgUgAgkAmQAAALAJAHQAIAGARAAQAPAAAMgKQAMgKAAgQIAAgLIglAAQgkAAAAAXg");
	this.shape_178.setTransform(-195.2,133.4);

	this.shape_179 = new cjs.Shape();
	this.shape_179.graphics.f("#FFFFFF").s().p("AAoBtIAAh4QAAgygkAAQgRAAgNANQgOANAAAZIAAB3Ig7AAIAAjWIA7AAIAAAYQAcgbAgAAQAjAAAWAYQAXAYAAAnIAACCg");
	this.shape_179.setTransform(-219.3,133.3);

	this.shape_180 = new cjs.Shape();
	this.shape_180.graphics.f("#FFFFFF").s().p("AgdCXIAAjWIA7AAIAADWgAgYhaQgKgKAAgPQAAgPAKgKQAKgKAOAAQAPAAAKAKQAKAKAAAPQAAAPgKAKQgKAKgPAAQgOAAgKgKg");
	this.shape_180.setTransform(-238,129.1);

	this.shape_181 = new cjs.Shape();
	this.shape_181.graphics.f("#FFFFFF").s().p("Ag1CQQgZgIgSgOIAXguQAdAXAhAAQAgAAAUgRQATgQAAgkQgdAggpAAQgqAAgdgdQgdgcAAgvQAAgwAegfQAegeAmAAQAlAAAaAgIAAgdIA8AAIAAC7QAAAdgKAWQgKAWgRANQghAbgtAAQgYAAgZgIgAgihVQgQAPAAAZQAAAaAPAQQAPAOAVAAQAWAAAOgOQAPgQAAgZQAAgagPgPQgOgPgVAAQgVAAgPAPg");
	this.shape_181.setTransform(-257.8,137.5);

	this.shape_182 = new cjs.Shape();
	this.shape_182.graphics.f("#FFFFFF").s().p("AgdCXIAAjWIA7AAIAADWgAgYhaQgKgKAAgPQAAgPAKgKQAKgKAOAAQAPAAAKAKQAKAKAAAPQAAAPgKAKQgKAKgPAAQgOAAgKgKg");
	this.shape_182.setTransform(-276.4,129.1);

	this.shape_183 = new cjs.Shape();
	this.shape_183.graphics.f("#FFFFFF").s().p("Ag9BtIAAjWIA8AAIAAAdQAKgOASgJQARgJASAAIAAA4IgLAAQgbAAgNATQgMATAAAeIAABdg");
	this.shape_183.setTransform(-289.6,133.3);

	this.shape_184 = new cjs.Shape();
	this.shape_184.graphics.f("#FFFFFF").s().p("AhQBPQghggAAgvQAAguAhggQAgggAwAAQAxAAAgAgQAhAgAAAuQAAAvghAgQggAggxAAQgwAAgggggAglgqQgPARAAAZQAAAbAPAQQAPARAWAAQAXAAAPgRQAPgQAAgbQAAgZgPgRQgPgRgXAAQgWAAgPARg");
	this.shape_184.setTransform(-311.6,133.4);

	this.shape_185 = new cjs.Shape();
	this.shape_185.graphics.f("#FFFFFF").s().p("AAoBrIgpg6IgpA6IhFAAIBMhsIhIhqIBIAAIAkA6IAlg6IBHAAIhKBpIBMBtg");
	this.shape_185.setTransform(-346.7,133.5);

	this.shape_186 = new cjs.Shape();
	this.shape_186.graphics.f("#FFFFFF").s().p("AhMBXQgYgWAAgsIAAiBIA9AAIAAB0QAAAxAkAAQARAAANgMQANgNAAgZIAAhzIA9AAIAADWIg9AAIAAgcQgWAfgiAAQgkAAgYgWg");
	this.shape_186.setTransform(-371.4,133.6);

	this.shape_187 = new cjs.Shape();
	this.shape_187.graphics.f("#FFFFFF").s().p("AhQBPQghggAAgvQAAguAhggQAgggAwAAQAxAAAgAgQAhAgAAAuQAAAvghAgQggAggxAAQgwAAgggggAglgqQgPARAAAZQAAAbAPAQQAPARAWAAQAXAAAPgRQAPgQAAgbQAAgZgPgRQgPgRgXAAQgWAAgPARg");
	this.shape_187.setTransform(-397,133.4);

	this.shape_188 = new cjs.Shape();
	this.shape_188.graphics.f("#FFFFFF").s().p("AhECoIAbgsQANANAOAAQAIAAAGgGQAEgGAAgMIAAjbIA8AAIAADgQAAAjgWAUQgVAVgeAAQgfAAgcgagAAJiFQgJgKAAgPQAAgPAJgKQAKgKAOAAQAQAAAJAKQALAKAAAPQAAAPgLAKQgJAKgQAAQgOAAgKgKg");
	this.shape_188.setTransform(-419.2,133.3);

	this.shape_189 = new cjs.Shape();
	this.shape_189.graphics.f("#FFFFFF").s().p("AgdCXIAAjWIA7AAIAADWgAgYhaQgKgKAAgPQAAgPAKgKQAKgKAOAAQAPAAAKAKQAKAKAAAPQAAAPgKAKQgKAKgPAAQgOAAgKgKg");
	this.shape_189.setTransform(-429.5,129.1);

	this.shape_190 = new cjs.Shape();
	this.shape_190.graphics.f("#FFFFFF").s().p("AgyB3IAAAcIg8AAIAAkoIA8AAIAABqQAZgcAmAAQAnABAdAeQAeAeAAAwQAAAwgeAgQgeAggmAAQgmAAgZgfgAgkgDQgPARAAAaQAAAaAPARQAPAQAUAAQAUAAAQgQQAPgRAAgaQAAgagPgRQgPgSgUAAQgVAAgPASg");
	this.shape_190.setTransform(-448.1,129.5);

	this.shape_191 = new cjs.Shape();
	this.shape_191.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAggeQAhgeAsAAQAsAAAfAbQAfAbAAAuIAAAfIicAAQADASAPALQAQALATAAQAfAAAVgVIAhAlQgkAigxAAQgwAAgggegAgdgyQgPALgDATIBeAAQgCgUgNgKQgMgKgRAAQgSAAgOAKg");
	this.shape_191.setTransform(-484.7,133.4);

	this.shape_192 = new cjs.Shape();
	this.shape_192.graphics.f("#FFFFFF").s().p("AhQB2QgeggAAgwQAAgwAdgeQAegeAngBQAmAAAZAcIAAhqIA8AAIAAEoIg8AAIAAgcQgaAfglAAQgmAAgegggAgigDQgPARAAAaQAAAaAPARQAQAQATAAQAVAAAPgQQAOgRAAgaQAAgagOgRQgPgSgVAAQgUAAgPASg");
	this.shape_192.setTransform(-511.1,129.5);

	this.shape_193 = new cjs.Shape();
	this.shape_193.graphics.f("#FFFFFF").s().p("AAoBtIAAh4QAAgygkAAQgRAAgNANQgOANAAAZIAAB3Ig7AAIAAjWIA7AAIAAAYQAcgbAgAAQAjAAAWAYQAXAYAAAnIAACCg");
	this.shape_193.setTransform(-143.7,84.5);

	this.shape_194 = new cjs.Shape();
	this.shape_194.graphics.f("#FFFFFF").s().p("AhQBPQghggAAgvQAAguAhggQAgggAwAAQAxAAAgAgQAhAgAAAuQAAAvghAgQggAggxAAQgwAAgggggAglgqQgPARAAAZQAAAbAPAQQAPARAWAAQAXAAAPgRQAPgQAAgbQAAgZgPgRQgPgRgXAAQgWAAgPARg");
	this.shape_194.setTransform(-169.6,84.7);

	this.shape_195 = new cjs.Shape();
	this.shape_195.graphics.f("#FFFFFF").s().p("AgdCXIAAjWIA7AAIAADWgAgYhaQgKgKAAgPQAAgPAKgKQAKgKAOAAQAPAAAKAKQAKAKAAAPQAAAPgKAKQgKAKgPAAQgOAAgKgKg");
	this.shape_195.setTransform(-188.4,80.3);

	this.shape_196 = new cjs.Shape();
	this.shape_196.graphics.f("#FFFFFF").s().p("AggB5QgVgUAAgkIAAhiIgZAAIAAgrIAZAAIAAhAIA8AAIAABAIA0AAIAAArIg0AAIAABgQAAAMAGAHQAGAHAJAAQARAAALgPIAXAqQgcAZgfAAQgeAAgWgUg");
	this.shape_196.setTransform(-202.4,81.6);

	this.shape_197 = new cjs.Shape();
	this.shape_197.graphics.f("#FFFFFF").s().p("AhEBQQgigeAAgyQAAgwAigfQAigfAtAAQAXAAAYAKQAYAJASATIgfApQgLgNgPgGQgOgHgPAAQgXAAgRAPQgRAQAAAaQAAAbARAQQARAPAWAAQAeAAAYgeIAkAoQgqArgwAAQgwAAghgfg");
	this.shape_197.setTransform(-223.2,84.7);

	this.shape_198 = new cjs.Shape();
	this.shape_198.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAhgeQAggeArAAQAtAAAfAbQAfAbAAAuIAAAfIicAAQADASAQALQAPALATAAQAfAAAVgVIAhAlQgkAigxAAQgwAAgggegAgdgyQgPALgCATIBdAAQgCgUgMgKQgNgKgSAAQgRAAgOAKg");
	this.shape_198.setTransform(-247.3,84.7);

	this.shape_199 = new cjs.Shape();
	this.shape_199.graphics.f("#FFFFFF").s().p("AgdCVIAAkoIA7AAIAAEog");
	this.shape_199.setTransform(-265.6,80.6);

	this.shape_200 = new cjs.Shape();
	this.shape_200.graphics.f("#FFFFFF").s().p("AgdCVIAAkoIA7AAIAAEog");
	this.shape_200.setTransform(-277.2,80.6);

	this.shape_201 = new cjs.Shape();
	this.shape_201.graphics.f("#FFFFFF").s().p("AhQBPQghggAAgvQAAguAhggQAgggAwAAQAxAAAgAgQAhAgAAAuQAAAvghAgQggAggxAAQgwAAgggggAglgqQgPARAAAZQAAAbAPAQQAPARAWAAQAXAAAPgRQAPgQAAgbQAAgZgPgRQgPgRgXAAQgWAAgPARg");
	this.shape_201.setTransform(-296.1,84.7);

	this.shape_202 = new cjs.Shape();
	this.shape_202.graphics.f("#FFFFFF").s().p("AhEBQQgigeAAgyQAAgwAigfQAigfAtAAQAXAAAYAKQAYAJASATIgfApQgLgNgPgGQgOgHgPAAQgXAAgRAPQgRAQAAAaQAAAbARAQQARAPAWAAQAeAAAYgeIAkAoQgqArgwAAQgwAAghgfg");
	this.shape_202.setTransform(-320.5,84.7);

	this.shape_203 = new cjs.Shape();
	this.shape_203.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAhgeQAggeArAAQAtAAAfAbQAfAbAAAuIAAAfIicAAQADASAQALQAPALATAAQAfAAAVgVIAhAlQgkAigxAAQgwAAgggegAgdgyQgPALgCATIBdAAQgCgUgMgKQgNgKgSAAQgQAAgPAKg");
	this.shape_203.setTransform(-355.1,84.7);

	this.shape_204 = new cjs.Shape();
	this.shape_204.graphics.f("#FFFFFF").s().p("AApBtIAAh4QAAgyglAAQgRAAgNANQgNANAAAZIAAB3Ig8AAIAAjWIA8AAIAAAYQAbgbAgAAQAjAAAXAYQAWAYAAAnIAACCg");
	this.shape_204.setTransform(-380.2,84.5);

	this.shape_205 = new cjs.Shape();
	this.shape_205.graphics.f("#FFFFFF").s().p("AhMBXQgYgWAAgsIAAiBIA9AAIAAB0QAAAxAkABQARAAANgOQANgNAAgYIAAhzIA9AAIAADWIg9AAIAAgcQgWAfgiAAQgkAAgYgWg");
	this.shape_205.setTransform(-406,84.9);

	this.shape_206 = new cjs.Shape();
	this.shape_206.graphics.f("#FFFFFF").s().p("Ag9BtIAAjWIA8AAIAAAdQAKgOASgJQARgJASAAIAAA4IgLAAQgbAAgNATQgMATAAAeIAABdg");
	this.shape_206.setTransform(-436.6,84.5);

	this.shape_207 = new cjs.Shape();
	this.shape_207.graphics.f("#FFFFFF").s().p("AhMBXQgYgWAAgsIAAiBIA9AAIAAB0QAAAxAkABQARAAANgOQANgNAAgYIAAhzIA9AAIAADWIg9AAIAAgcQgWAfgiAAQgkAAgYgWg");
	this.shape_207.setTransform(-458.6,84.9);

	this.shape_208 = new cjs.Shape();
	this.shape_208.graphics.f("#FFFFFF").s().p("AhQBPQghggAAgvQAAguAhggQAgggAwAAQAxAAAgAgQAhAgAAAuQAAAvghAgQggAggxAAQgwAAgggggAglgqQgPARAAAZQAAAbAPAQQAPARAWAAQAXAAAPgRQAPgQAAgbQAAgZgPgRQgPgRgXAAQgWAAgPARg");
	this.shape_208.setTransform(-484.2,84.7);

	this.shape_209 = new cjs.Shape();
	this.shape_209.graphics.f("#FFFFFF").s().p("AhuCUIAAkjIA8AAIAAAXQAcgbAkAAQAmAAAdAfQAeAgAAAvQAAAvgeAgQgdAfgnABQgngBgYgfIAABqgAgkhPQgPARAAAaQAAAbAPAPQAPAQAUAAQAUAAAQgQQAPgPAAgaQAAgbgPgRQgPgSgUAAQgVAAgPASg");
	this.shape_209.setTransform(-509.9,88.4);

	this.shape_210 = new cjs.Shape();
	this.shape_210.graphics.f("#FFFFFF").s().p("AgfB5QgWgUAAgkIAAhiIgZAAIAAgrIAZAAIAAhAIA7AAIAABAIA1AAIAAArIg1AAIAABgQABAMAGAHQAHAHAIAAQARAAAMgPIAWAqQgcAZgfAAQgeAAgVgUg");
	this.shape_210.setTransform(-241.4,32.9);

	this.shape_211 = new cjs.Shape();
	this.shape_211.graphics.f("#FFFFFF").s().p("AAoBtIAAh4QAAgygkAAQgRAAgNANQgOANAAAZIAAB3Ig7AAIAAjWIA7AAIAAAYQAcgbAgAAQAjAAAWAYQAXAYAAAnIAACCg");
	this.shape_211.setTransform(-263.4,35.8);

	this.shape_212 = new cjs.Shape();
	this.shape_212.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAggeQAhgeAsAAQAsAAAfAbQAfAbAAAuIAAAfIicAAQADASAPALQAQALATAAQAfAAAVgVIAhAlQgkAigxAAQgwAAgggegAgdgyQgPALgDATIBeAAQgCgUgNgKQgMgKgRAAQgSAAgOAKg");
	this.shape_212.setTransform(-288.5,35.9);

	this.shape_213 = new cjs.Shape();
	this.shape_213.graphics.f("#FFFFFF").s().p("AgfB5QgWgUAAgkIAAhiIgZAAIAAgrIAZAAIAAhAIA7AAIAABAIA1AAIAAArIg1AAIAABgQABAMAGAHQAGAHAJAAQARAAAMgPIAWAqQgcAZgfAAQgeAAgVgUg");
	this.shape_213.setTransform(-309.2,32.9);

	this.shape_214 = new cjs.Shape();
	this.shape_214.graphics.f("#FFFFFF").s().p("AAoBtIAAh4QABgyglAAQgRAAgNANQgOANAAAZIAAB3Ig7AAIAAjWIA7AAIAAAYQAcgbAgAAQAjAAAXAYQAWAYAAAnIAACCg");
	this.shape_214.setTransform(-331.2,35.8);

	this.shape_215 = new cjs.Shape();
	this.shape_215.graphics.f("#FFFFFF").s().p("AhQBPQghggAAgvQAAguAhggQAgggAwAAQAxAAAgAgQAhAgAAAuQAAAvghAgQggAggxAAQgwAAgggggAglgqQgPARAAAZQAAAbAPAQQAPARAWAAQAXAAAPgRQAPgQAAgbQAAgZgPgRQgPgRgXAAQgWAAgPARg");
	this.shape_215.setTransform(-357.1,35.9);

	this.shape_216 = new cjs.Shape();
	this.shape_216.graphics.f("#FFFFFF").s().p("AhEBQQgigeAAgyQAAgwAigfQAigfAtAAQAXAAAYAKQAYAJASATIgfApQgLgNgPgGQgOgHgPAAQgXAAgRAPQgRAQAAAaQAAAbARAQQARAPAWAAQAeAAAYgeIAkAoQgqArgwAAQgwAAghgfg");
	this.shape_216.setTransform(-381.5,35.9);

	this.shape_217 = new cjs.Shape();
	this.shape_217.graphics.f("#FFFFFF").s().p("AhQB2QgeghAAgwQAAguAdggQAegeAnAAQAmABAZAbIAAhqIA8AAIAAEoIg8AAIAAgdQgaAgglAAQgmAAgegggAgigDQgPARAAAaQAAAaAPAQQAQARATAAQAVAAAPgRQAOgQAAgbQAAgZgOgRQgPgRgVAAQgUAAgPARg");
	this.shape_217.setTransform(-417.8,32);

	this.shape_218 = new cjs.Shape();
	this.shape_218.graphics.f("#FFFFFF").s().p("AApBtIAAh4QAAgyglAAQgRAAgNANQgNANAAAZIAAB3Ig8AAIAAjWIA8AAIAAAYQAbgbAgAAQAjAAAXAYQAWAYAAAnIAACCg");
	this.shape_218.setTransform(-443.2,35.8);

	this.shape_219 = new cjs.Shape();
	this.shape_219.graphics.f("#FFFFFF").s().p("AhJBbQgXgUAAghQAAghAYgPQAYgQAqAAIArAAIAAgCQAAgigkAAQgPAAgRAGQgRAGgMAKIgbgnQAqgfA1AAQAnAAAaAUQAYATAAArIAACIIg4AAIAAgaQgYAdggAAQgiAAgYgUgAgkAmQAAALAIAHQAKAGAPAAQAPAAAMgKQANgKAAgQIAAgLIglAAQgkAAAAAXg");
	this.shape_219.setTransform(-468.4,35.9);

	this.shape_220 = new cjs.Shape();
	this.shape_220.graphics.f("#FFFFFF").s().p("Ag9BtIAAjWIA8AAIAAAdQAKgOASgJQARgJASAAIAAA4IgLAAQgbAAgNATQgMATAAAeIAABdg");
	this.shape_220.setTransform(-487.2,35.8);

	this.shape_221 = new cjs.Shape();
	this.shape_221.graphics.f("#FFFFFF").s().p("AhuCMIAAkXIBsAAQAbAAAVAHQAVAHAKALQATAWAAAbQAAAhgVAQQgHAGgEABIgJAFQAaAEAPASQAPASABAaQAAAdgVAXQgWAag6AAgAgwBXIAqAAQAaAAAOgHQANgGAAgUQAAgTgOgGQgOgGgeAAIglAAgAgwgaIAdAAQAaAAAMgFQANgGAAgTQAAgSgMgGQgLgGgbAAIgeAAg");
	this.shape_221.setTransform(-509.2,32.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_221},{t:this.shape_220},{t:this.shape_219},{t:this.shape_218},{t:this.shape_217},{t:this.shape_216},{t:this.shape_215},{t:this.shape_214},{t:this.shape_213},{t:this.shape_212},{t:this.shape_211},{t:this.shape_210},{t:this.shape_209},{t:this.shape_208},{t:this.shape_207},{t:this.shape_206},{t:this.shape_205},{t:this.shape_204},{t:this.shape_203},{t:this.shape_202},{t:this.shape_201},{t:this.shape_200},{t:this.shape_199},{t:this.shape_198},{t:this.shape_197},{t:this.shape_196},{t:this.shape_195},{t:this.shape_194},{t:this.shape_193},{t:this.shape_192},{t:this.shape_191},{t:this.shape_190},{t:this.shape_189},{t:this.shape_188},{t:this.shape_187},{t:this.shape_186},{t:this.shape_185},{t:this.shape_184},{t:this.shape_183},{t:this.shape_182},{t:this.shape_181},{t:this.shape_180},{t:this.shape_179},{t:this.shape_178},{t:this.shape_177},{t:this.shape_176}]},57).to({state:[]},43).to({state:[]},1).wait(20));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(221.1,-172,2,16);


// stage content:
(lib.FS_projet_dg_crazymals_mobile = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{video:179,"close":515});

	// timeline functions:
	this.frame_0 = function() {
		//variable necessaire pour la fermeture des textes
		this.hiddentexts = false;
		
		//variable necessaire pour la fermeture de la vidéo
		this.hiddenvideo = true;
		
		//variable necessaire pour la fermeture du visuel de fond
		this.hiddenvisuel = false;
		
		//variable necessaire pour la fermeture du bouton play
		this.hiddenplay = true;
	}
	this.frame_139 = function() {
		//var frequency = 60;
		//stage.enableMouseOver(frequency);
		
		// EVENT DE CLIC POUR LA SUITE
		this.next_btn.addEventListener("click", fl_ClickToGoToAndPlayFromFrame.bind(this));
		this.next_btn.addEventListener("mouseover", fl_MouseOverHandler.bind(this));
		this.next_btn.addEventListener("mouseout", fl_MouseOutHandler.bind(this));
		
		
		function fl_ClickToGoToAndPlayFromFrame()
		{
			//this.gotoAndPlay("close");
			
			var event = new Event('next');
			this.dispatchEvent(event);		
			event = null;
			
			this.next_btn.btnNext.gotoAndPlay('rollOut');
			this.lignes_mc.gotoAndPlay("close");
			this.next_btn.gotoAndPlay("close");
			this.cta_mc.gotoAndPlay("close");
			
		}
		function fl_MouseOverHandler(){	this.next_btn.btnNext.gotoAndPlay('rollOver'); }
		function fl_MouseOutHandler(){	this.next_btn.btnNext.gotoAndPlay('rollOut'); }
		
		
		// EVENT DE CLIC POUR LE LIEN
		this.cta_mc.addEventListener("click", projectDetails.bind(this));
		this.cta_mc.addEventListener("mouseover", pDetails_MouseOverHandler.bind(this));
		this.cta_mc.addEventListener("mouseout", pDetails_MouseOutHandler.bind(this));
		
		function projectDetails()
		{	
			var event = new Event('details');
			this.dispatchEvent(event);		
			event = null;
			
			this.cta_mc.btnCta.gotoAndPlay('rollOut');
			
		}
		function pDetails_MouseOverHandler(){	this.cta_mc.btnCta.gotoAndPlay('rollOver'); }
		function pDetails_MouseOutHandler(){	this.cta_mc.btnCta.gotoAndPlay('rollOut'); }
		
		
		// EVENT DE CLIC POUR LA VIDEO
		this.video_mc.btn_playVideo.addEventListener("click", playVideo.bind(this));
		
		function playVideo()
		{	
			var event = new Event('video');
			this.dispatchEvent(event);		
			event = null;
			
			this.hiddenplay = true;	
			this.video_mc.btn_playVideo.gotoAndPlay('close');
		}
	}
	this.frame_179 = function() {
		this.visuel_mc.gotoAndPlay("close");
		this.video_mc.gotoAndPlay("open");
		
		this.hiddenvideo = false;
		this.hiddenvisuel = true;
		this.hiddenplay = false;
		
		//On envoie l'information de l'affichage de la video
		var event = new Event('videoStarter');
		this.dispatchEvent(event);		
		event = null;
	}
	this.frame_339 = function() {
		this.textes_mc.gotoAndPlay("close");
		this.hiddentexts = true;
	}
	this.frame_514 = function() {
		//PAUSE
		this.stop();
	}
	this.frame_516 = function() {
		this.cta_mc.gotoAndPlay("close");
		this.next_btn.gotoAndPlay("close");
		
		
		if(this.hiddenvisuel==false){
			this.visuel_mc.gotoAndPlay("close");
		}
		
		if(this.hiddenvideo==false){
			this.video_mc.gotoAndPlay("close");
		}
		
		if(this.hiddentexts==false){
			this.textes_mc.gotoAndPlay("close");
			this.hiddentexts = true;
		}
		
		if(this.hiddenplay==false){
			this.video_mc.btn_playVideo.gotoAndPlay('close');
		}
	}
	this.frame_535 = function() {
		//this.lignes_mc.gotoAndPlay("close");
	}
	this.frame_562 = function() {
		// EVENT DE FIN D'ANIM
		var event = new Event('closed');
		this.dispatchEvent(event);		
		event = null;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(139).call(this.frame_139).wait(40).call(this.frame_179).wait(160).call(this.frame_339).wait(175).call(this.frame_514).wait(2).call(this.frame_516).wait(19).call(this.frame_535).wait(27).call(this.frame_562).wait(1));

	// CTA NEXT
	this.next_btn = new lib.bloc_NEXT_btn();
	this.next_btn.name = "next_btn";
	this.next_btn.parent = this;
	this.next_btn.setTransform(359.2,861.2,1.06,1.06,0,0,0,-340.4,403.9);
	this.next_btn._off = true;

	this.timeline.addTween(cjs.Tween.get(this.next_btn).wait(128).to({_off:false},0).wait(435));

	// CTA DETAIL
	this.cta_mc = new lib.bloc_CTA();
	this.cta_mc.name = "cta_mc";
	this.cta_mc.parent = this;
	this.cta_mc.setTransform(120.2,861.4,1.06,1.06,0,0,0,-340.4,404.1);
	this.cta_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.cta_mc).wait(119).to({_off:false},0).to({_off:true},420).wait(24));

	// BLOC INFOS TEXTE
	this.textes_mc = new lib.blocInfostextes();
	this.textes_mc.name = "textes_mc";
	this.textes_mc.parent = this;
	this.textes_mc.setTransform(788.2,242,1,1,0,0,0,222.1,-164);
	this.textes_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.textes_mc).wait(17).to({_off:false},0).wait(546));

	// VIDEO
	this.video_mc = new lib.bloc_video();
	this.video_mc.name = "video_mc";
	this.video_mc.parent = this;
	this.video_mc.setTransform(238.9,1185.2,1.05,1.05,0,0,0,227.2,765);

	this.timeline.addTween(cjs.Tween.get(this.video_mc).to({_off:true},539).wait(24));

	// VISUEL
	this.visuel_mc = new lib.bloc_visuel();
	this.visuel_mc.name = "visuel_mc";
	this.visuel_mc.parent = this;
	this.visuel_mc.setTransform(1030.6,381.5,1,1,0,0,0,552.5,26.5);
	this.visuel_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.visuel_mc).wait(17).to({_off:false},0).wait(546));

	// lignes
	this.lignes_mc = new lib.bloc_lignes();
	this.lignes_mc.name = "lignes_mc";
	this.lignes_mc.parent = this;
	this.lignes_mc.setTransform(240.8,453.9,0.525,1,0,0,0,0.7,405.9);

	this.timeline.addTween(cjs.Tween.get(this.lignes_mc).wait(563));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(241.3,293.5,478,1643.4);
// library properties:
lib.properties = {
	id: '186B19C0CD29F9448AD8E463788224C1',
	width: 480,
	height: 840,
	fps: 60,
	color: "#000000",
	opacity: 1.00,
	manifest: [
		{src:"images/crazymals_1.png?1540809552073", id:"crazymals_1"},
		{src:"images/crazymals_cordon.png?1540809552073", id:"crazymals_cordon"},
		{src:"images/crazymals_singe.png?1540809552073", id:"crazymals_singe"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['186B19C0CD29F9448AD8E463788224C1'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;