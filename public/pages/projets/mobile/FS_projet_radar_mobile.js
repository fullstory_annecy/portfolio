(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.radar = function() {
	this.initialize(img.radar);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,753,587);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.video = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#080808").s().p("EgtTBLAMAAAiV/MBanAAAMAAACV/g");
	this.shape.setTransform(290,480);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.video, new cjs.Rectangle(0,0,580,960), null);


(lib.txt_hashtag = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgKAxIAAhOIgbAAIAAgTIBLAAIAAATIgbAAIAABOg");
	this.shape.setTransform(-0.6,73.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAXAxIgtg9IAAA9IgWAAIAAhhIAUAAIAvA+IAAg+IAWAAIAABhg");
	this.shape_1.setTransform(-10.6,73.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBGAAIAAATIgwAAIAAAVIAqAAIAAASIgqAAIAAAUIAxAAIAAATg");
	this.shape_2.setTransform(-20.6,73.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgIAxIgohhIAYAAIAYA+IAZg+IAXAAIgmBhg");
	this.shape_3.setTransform(-30.3,73.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBFAAIAAATIgvAAIAAAVIAqAAIAAASIgqAAIAAAUIAxAAIAAATg");
	this.shape_4.setTransform(-39.4,73.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgUIANAAIADgQIgPAAIADgUIAPAAIAEgWIAUAAIgDAWIAMAAIADgWIAWAAIgEAWIAOAAIgDAUIgPAAIgDAQIARAAIgEAUIgQAAIgDAVgAgHAJIALAAIAEgQIgMAAg");
	this.shape_5.setTransform(-49.5,73.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAXAxIgtg9IAAA9IgWAAIAAhhIAUAAIAvA+IAAg+IAWAAIAABhg");
	this.shape_6.setTransform(38.5,52.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgjAkQgPgPAAgVQAAgUAPgPQAOgPAVAAQAWAAAPAPQAOAPAAAUQAAAVgOAPQgPAPgWAAQgVAAgOgPgAgTgVQgJAJAAAMQAAANAJAKQAIAIALABQAMgBAJgIQAIgKAAgNQAAgMgIgJQgJgJgMAAQgLAAgIAJg");
	this.shape_7.setTransform(27,52.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AAXAxIgtg9IAAA9IgWAAIAAhhIAUAAIAvA+IAAg+IAWAAIAABhg");
	this.shape_8.setTransform(15.5,52.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AAeAxIgKgVIgoAAIgJAVIgXAAIAqhhIAVAAIAqBhgAgLAJIAXAAIgMgcg");
	this.shape_9.setTransform(4.6,52.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgeAkQgPgOAAgWQAAgVAPgOQAPgPAVAAQAYAAAQASIgNAQQgLgMgPAAQgMgBgIAJQgJAHAAANQgBANAJAJQAJAIAKAAQAQAAALgMIANAOQgQASgXABQgVAAgPgPg");
	this.shape_10.setTransform(-5.7,52.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AASAxIgcgmIgKAKIAAAcIgWAAIAAhhIAWAAIAAApIAkgpIAbAAIgnAsIASAZIAVAcg");
	this.shape_11.setTransform(-19,52.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgsAxIAAhhIAjAAQAZAAAOANQAOAMAAAXQAAAWgOAOQgNANgbAAgAgVAeIANAAQAOgBAJgHQAHgIABgOQgBgOgHgHQgJgIgPAAIgMAAg");
	this.shape_12.setTransform(-29.6,52.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgmAiIANgQQAQAOANAAQAGgBADgDQADgCAAgFQAAgEgDgCQgEgDgKgDQgRgDgIgGQgIgGAAgOQAAgPALgHQAKgIANAAQAKAAAKAEQAKADAHAHIgLAQQgNgKgNAAQgFAAgDACQgDADAAAEQAAAEAEADQAEACANAEQAOADAHAGQAIAHAAANQAAANgKAJQgKAHgQABQgWAAgTgRg");
	this.shape_13.setTransform(-39.9,52.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgTIANAAIADgRIgPAAIADgUIAPAAIAEgWIAUAAIgDAWIAMAAIADgWIAWAAIgEAWIAOAAIgDAUIgPAAIgDARIARAAIgEATIgQAAIgDAVgAgHAKIALAAIAEgRIgMAAg");
	this.shape_14.setTransform(-49.5,52.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgmAiIANgRQAQAPANAAQAGAAADgEQADgCAAgEQAAgFgDgCQgEgDgKgDQgRgEgIgFQgIgHAAgNQAAgPALgHQAKgIANABQAKAAAKADQAKADAHAGIgLARQgNgKgNAAQgFAAgDACQgDADAAAEQAAAFAEACQAEACANAEQAOADAHAGQAIAHAAANQAAANgKAJQgKAHgQABQgWgBgTgQg");
	this.shape_15.setTransform(14.9,31.7);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgeAkIALgQQAJAKAJAAQAEAAADgEQADgEAAgHIAAgsIgcAAIAAgTIAzAAIAAA/QAAARgJAJQgKAIgNABQgRAAgNgOg");
	this.shape_16.setTransform(6.2,31.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgIAJQgDgEAAgFQAAgEADgEQAEgDAEAAQAFAAAEADQADAEAAAEQAAAFgDAEQgEADgFAAQgEAAgEgDg");
	this.shape_17.setTransform(0.7,35.5);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBFAAIAAATIgvAAIAAAVIAqAAIAAASIgqAAIAAATIAxAAIAAAUg");
	this.shape_18.setTransform(-5.7,31.7);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgsAxIAAhhIAjAAQAaAAAOANQAOAMgBAXQABAWgOAOQgOANgbAAgAgVAdIANAAQAOAAAJgHQAHgIABgOQgBgOgHgHQgJgIgQAAIgLAAg");
	this.shape_19.setTransform(-15.6,31.7);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgjAkQgPgPAAgVQAAgVAPgOQAOgOAVAAQAWAAAPAOQAOAOAAAVQAAAVgOAPQgPAPgWAAQgVAAgOgPgAgTgVQgJAJAAAMQAAANAJAKQAIAIALABQAMgBAJgIQAIgKAAgNQAAgMgIgJQgJgJgMAAQgLAAgIAJg");
	this.shape_20.setTransform(-27,31.7);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AAXAxIgtg9IAAA9IgWAAIAAhhIAUAAIAvA+IAAg+IAWAAIAABhg");
	this.shape_21.setTransform(-38.5,31.7);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgTIANAAIADgRIgPAAIADgTIAPAAIAEgXIAUAAIgDAXIAMAAIADgXIAWAAIgEAXIAOAAIgDATIgPAAIgDARIARAAIgEATIgQAAIgDAVgAgHAKIALAAIAEgRIgMAAg");
	this.shape_22.setTransform(-49.5,31.6);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AASAxIgdgnIgJALIAAAcIgWAAIAAhhIAWAAIAAApIAkgpIAbAAIgnArIASAZIAVAdg");
	this.shape_23.setTransform(94.3,10.7);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AAQAxIgVgfIgPAAIAAAfIgWAAIAAhhIAlAAQAWAAAKAIQAKAHAAARQAAAXgTAHIAZAjgAgUAAIAQAAQAKAAAEgEQAEgDAAgJQAAgHgEgDQgEgDgKAAIgQAAg");
	this.shape_24.setTransform(84,10.7);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgjAkQgPgPAAgVQAAgVAPgOQAOgOAVAAQAWAAAPAOQAOAOAAAVQAAAVgOAPQgPAPgWAAQgVAAgOgPgAgTgVQgJAJAAAMQAAANAJAKQAIAIALABQAMgBAJgIQAIgKAAgNQAAgMgIgJQgJgKgMABQgLgBgIAKg");
	this.shape_25.setTransform(72.8,10.6);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AAVAxIgVhDIgUBDIgQAAIgjhhIAYAAIATA4IASg4IAVAAIASA4IATg4IAYAAIgiBhg");
	this.shape_26.setTransform(59.9,10.7);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBFAAIAAAUIgvAAIAAAUIArAAIAAASIgrAAIAAATIAxAAIAAAUg");
	this.shape_27.setTransform(48.4,10.7);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AAhAxIAAg9IgbA1IgMAAIgag1IAAA9IgWAAIAAhhIAeAAIAYA1IAZg1IAeAAIAABhg");
	this.shape_28.setTransform(36.9,10.7);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AAeAxIgKgVIgoAAIgJAVIgXAAIAqhhIAVAAIAqBhgAgLAJIAXAAIgMgcg");
	this.shape_29.setTransform(25,10.7);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AAQAxIgVgfIgPAAIAAAfIgWAAIAAhhIAlAAQAWAAAKAIQAKAHAAARQAAAXgTAHIAZAjgAgUAAIAQAAQAKAAAEgEQAEgDAAgJQAAgHgEgDQgEgDgKAAIgQAAg");
	this.shape_30.setTransform(15.1,10.7);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AggAxIAAhhIBCAAIAAAUIgtAAIAAAVIArAAIAAARIgrAAIAAAng");
	this.shape_31.setTransform(5.7,10.7);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AAXAxIgtg9IAAA9IgWAAIAAhhIAUAAIAvA+IAAg+IAWAAIAABhg");
	this.shape_32.setTransform(-8.3,10.7);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBGAAIAAAUIgwAAIAAAUIAqAAIAAASIgqAAIAAATIAxAAIAAAUg");
	this.shape_33.setTransform(-18.4,10.7);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AgmAxIAAhhIAiAAQAWAAAKAJQALAJAAASQAAARgLAIQgLAJgUgBIgNAAIAAAcgAgQADIAPAAQAJAAAEgDQAFgFAAgJQAAgIgGgDQgFgEgKAAIgMAAg");
	this.shape_34.setTransform(-27.6,10.7);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AgjAkQgPgPAAgVQAAgVAPgOQAOgOAVAAQAWAAAPAOQAOAOAAAVQAAAVgOAPQgPAPgWAAQgVAAgOgPgAgTgVQgJAJAAAMQAAANAJAKQAIAIALABQAMgBAJgIQAIgKAAgNQAAgMgIgJQgJgKgMABQgLgBgIAKg");
	this.shape_35.setTransform(-38.5,10.6);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgTIANAAIADgRIgPAAIADgTIAPAAIAEgXIAUAAIgDAXIAMAAIADgXIAWAAIgEAXIAOAAIgDATIgPAAIgDARIARAAIgEATIgQAAIgDAVgAgHAKIALAAIAEgRIgMAAg");
	this.shape_36.setTransform(-49.5,10.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.txt_hashtag, new cjs.Rectangle(-56.7,0,157.6,84.2), null);


(lib.radar_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.instance = new lib.radar();
	this.instance.parent = this;
	this.instance.setTransform(-316.8,-247,0.842,0.842);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.radar_1, new cjs.Rectangle(-316.8,-247,633.7,494), null);


(lib.playArrow = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(2,1,1).p("AiOiMIAAEZIEdiIg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AiOiMIEdCRIkdCIg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.playArrow, new cjs.Rectangle(-15.3,-15.1,30.7,30.2), null);


(lib.masqueTexte_1_ligne = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("EgicAEJIAAoRMBE5AAAIAAIRg");
	this.shape.setTransform(0,24.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.masqueTexte_1_ligne, new cjs.Rectangle(-220.5,-1.9,441,53), null);


(lib.masque_generique = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0033").s().p("AsQCqIAAlTIYhAAIAAFTg");
	this.shape.setTransform(78.5,17);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.masque_generique, new cjs.Rectangle(0,0,157,34), null);


(lib.line_explode = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_15 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(15).call(this.frame_15).wait(1));

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(1,1,1).p("AgHAAIAPAA");
	this.shape.setTransform(0.8,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#FFFFFF").ss(1,1,1).p("AgRAAIAjAA");
	this.shape_1.setTransform(2.9,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#FFFFFF").ss(1,1,1).p("AgaAAIA2AA");
	this.shape_2.setTransform(5.1,0);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#FFFFFF").ss(1,1,1).p("AglAAIBLAA");
	this.shape_3.setTransform(7.3,0);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#FFFFFF").ss(1,1,1).p("AgnAAIBPAA");
	this.shape_4.setTransform(8.8,0);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#FFFFFF").ss(1,1,1).p("AgpAAIBTAA");
	this.shape_5.setTransform(10.4,0);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#FFFFFF").ss(1,1,1).p("AgrAAIBXAA");
	this.shape_6.setTransform(12,0);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#FFFFFF").ss(1,1,1).p("AgtAAIBbAA");
	this.shape_7.setTransform(13.6,0);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#FFFFFF").ss(1,1,1).p("AgvAAIBfAA");
	this.shape_8.setTransform(15.1,0);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#FFFFFF").ss(1,1,1).p("AgxAAIBjAA");
	this.shape_9.setTransform(16.7,0);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#FFFFFF").ss(1,1,1).p("Ag0AAIBpAA");
	this.shape_10.setTransform(18.3,0);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#FFFFFF").ss(1,1,1).p("AgWAAIAtAA");
	this.shape_11.setTransform(25.8,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape,p:{x:0.8}}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3,p:{x:7.3}}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_3,p:{x:22}}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape,p:{x:29.5}}]},1).to({state:[]},1).to({state:[]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1,-1,13,2);


(lib.ligne = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(49));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#333333").ss(2,1,1).p("EgABhM/MAADCZ/");
	this.shape.setTransform(-0.2,319.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#333333").ss(2,1,1).p("EAAghM7Qg1OzgbRBQgbUYAEVMQADWBAkVVQAlTkBERl");
	this.shape_1.setTransform(-7,319.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#333333").ss(2,1,1).p("EAA7hM5QhfN/gxR5QgxUHAHVhQAGWSA/VCQBDUPB4Qw");
	this.shape_2.setTransform(-12.4,319.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#333333").ss(2,1,1).p("EABQhM3Qh/NXhDSkQhAT5AIVyQAIWfBUU0QBZUwChQG");
	this.shape_3.setTransform(-16.5,320.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#333333").ss(2,1,1).p("EABfhM1QiXM4hPTEQhMTvAKV+QAJWpBjUqQBqVIC+Pn");
	this.shape_4.setTransform(-19.6,320.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#333333").ss(2,1,1).p("EABqhM0QioMjhYTaQhVToALWHQALWwBuUiQB1VaDUPR");
	this.shape_5.setTransform(-21.8,320.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#333333").ss(2,1,1).p("EABxhMzQizMUheTqQhbTjAMWNQALW1B2UdQB9VmDjPB");
	this.shape_6.setTransform(-23.3,320.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#333333").ss(2,1,1).p("EAB2hMzQi7MKhiT1QhfTgAMWRQAMW3B7UaQCDVvDsO3");
	this.shape_7.setTransform(-24.2,320.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#333333").ss(2,1,1).p("EAB5hMyQjAMEhkT7QhhTeAMWTQAMW5B+UYQCGVzDyOx");
	this.shape_8.setTransform(-24.8,320.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#333333").ss(2,1,1).p("EAB7hMyQjCMBhmT+QhjTdANWVQAMW6CAUWQCHV2D2Ou");
	this.shape_9.setTransform(-25.2,320.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#333333").ss(2,1,1).p("EAB8hMyQjEL/hmUAQhjTcAMWWQANW7CAUWQCIV3D3Os");
	this.shape_10.setTransform(-25.3,320.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#333333").ss(2,1,1).p("EAB8hMyQjEL/hnUAQhjTcANWWQAMW7CAUWQCJV3D4Os");
	this.shape_11.setTransform(-25.4,320.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#333333").ss(2,1,1).p("EAB8hMyQjEL/hnUAQhjTcAMWWQANW7CAUWQCJV3D4Os");
	this.shape_12.setTransform(-25.4,320.5);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#333333").ss(2,1,1).p("EAB8hMyQjEL/hnUAQhjTcAMWWQANW7CAUWQCJV4D4Or");
	this.shape_13.setTransform(-25.4,320.5);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#333333").ss(2,1,1).p("EAB8hMyQjEL/hmUAQhkTcANWWQAMW7CBUWQCIV3D4Os");
	this.shape_14.setTransform(-25.4,320.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#333333").ss(2,1,1).p("EAB7hMyQjDMAhmT/QhjTdANWVQAMW6CAUWQCIV3D2Ot");
	this.shape_15.setTransform(-25.2,320.5);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#333333").ss(2,1,1).p("EAB6hMyQjBMChmT9QhiTdAMWVQANW6B/UXQCHV1D1Ou");
	this.shape_16.setTransform(-25.1,320.5);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#333333").ss(2,1,1).p("EAB5hMyQjAMEhkT7QhiTeANWTQAMW5B+UYQCGV0DzOw");
	this.shape_17.setTransform(-24.9,320.5);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#333333").ss(2,1,1).p("EAB4hMzQi+MHhjT5QhgTfAMWRQAMW5B8UZQCFVxDvO0");
	this.shape_18.setTransform(-24.6,320.5);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#333333").ss(2,1,1).p("EAB2hMzQi7MLhhT0QhfTgAMWRQAMW3B6UaQCDVtDrO5");
	this.shape_19.setTransform(-24.2,320.5);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#333333").ss(2,1,1).p("EABzhMzQi2MQhgTuQhcTiALWPQAMW1B4UcQB/VqDnO9");
	this.shape_20.setTransform(-23.7,320.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#333333").ss(2,1,1).p("EABwhMzQiyMWhcToQhbTkAMWMQALWzB1UeQB8VlDgPD");
	this.shape_21.setTransform(-23,320.4);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#333333").ss(2,1,1).p("EABshM0QisMehZTgQhXTmALWJQALWxBwUhQB4VeDZPM");
	this.shape_22.setTransform(-22.2,320.4);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#333333").ss(2,1,1).p("EABohM0QilMnhVTWQhUTpALWFQAKWuBsUlQBzVWDQPV");
	this.shape_23.setTransform(-21.3,320.3);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#333333").ss(2,1,1).p("EABihM1QibMyhSTKQhPTuAKWAQAKWrBnUoQBsVNDFPh");
	this.shape_24.setTransform(-20.2,320.3);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#333333").ss(2,1,1).p("EABchM1QiSM/hMS8QhKTyAKV7QAJWnBgUsQBmVDC4Pt");
	this.shape_25.setTransform(-18.9,320.2);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#333333").ss(2,1,1).p("EABUhM2QiGNNhGStQhET3AJV1QAJWiBYUyQBeU3CpP8");
	this.shape_26.setTransform(-17.4,320.1);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#333333").ss(2,1,1).p("EABMhM3Qh5Neg/ScQg9T8AIVuQAHWdBQU2QBVUqCYQO");
	this.shape_27.setTransform(-15.7,320);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#333333").ss(2,1,1).p("EABChM4QhqNxg3SIQg2UCAHVnQAHWWBGU9QBKUbCGQh");
	this.shape_28.setTransform(-13.8,319.9);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#333333").ss(2,1,1).p("EAA3hM5QhZOGguRxQguUKAGVeQAGWPA7VFQA/UJBxQ3");
	this.shape_29.setTransform(-11.7,319.8);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#333333").ss(2,1,1).p("EAAuhM6QhKOZgnRdQgmUQAFVWQAGWKAxVLQA0T6BfRK");
	this.shape_30.setTransform(-9.8,319.7);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#333333").ss(2,1,1).p("EAAlhM7Qg8OqggRLQgfUWAEVPQAEWFApVQQArTtBORb");
	this.shape_31.setTransform(-8.1,319.7);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#333333").ss(2,1,1).p("EAAehM7QgxO4gaQ8QgZUaADVJQAEWAAhVWQAjTgA/Rq");
	this.shape_32.setTransform(-6.6,319.6);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#333333").ss(2,1,1).p("EAAXhM8QgnPFgUQuQgUUeADVFQADV8AaVaQAcTWAzR3");
	this.shape_33.setTransform(-5.3,319.5);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#333333").ss(2,1,1).p("EAAShM9QgePQgRQjQgPUiACVAQADV4AVVeQAVTNAoSD");
	this.shape_34.setTransform(-4.2,319.5);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#333333").ss(2,1,1).p("EAANhM9QgXPZgMQZQgMUlACU9QACV1AQVhQARTFAeSM");
	this.shape_35.setTransform(-3.2,319.4);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#333333").ss(2,1,1).p("EAAJhM9QgRPggJQRQgJUoACU5QABVyANVkQAMS/AXSU");
	this.shape_36.setTransform(-2.4,319.4);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#333333").ss(2,1,1).p("EAAGhM+QgMPogGQKQgHUpACU3QABVwAJVmQAJS6ARSb");
	this.shape_37.setTransform(-1.8,319.3);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#333333").ss(2,1,1).p("EAADhM+QgIPsgEQFQgEUrABU1QABVuAGVoQAGS2AMSg");
	this.shape_38.setTransform(-1.3,319.3);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#333333").ss(2,1,1).p("EAABhM+QgFPwgCQAQgDUtABUzQABVtAEVqQAESyAISk");
	this.shape_39.setTransform(-0.9,319.3);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#333333").ss(2,1,1).p("EAAAhM+QgCPzgCP9QgBUuAAUxQABVtADVqQACSwAFSn");
	this.shape_40.setTransform(-0.6,319.3);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#333333").ss(2,1,1).p("EAAAhM+QgCP1gBP7QAAUuAAUxQABVsACVrQABSuADSp");
	this.shape_41.setTransform(-0.4,319.3);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#333333").ss(2,1,1).p("EgABhM/QgBP3AAP5QAAUvABUxQAAVsABVrQABStACSr");
	this.shape_42.setTransform(-0.2,319.3);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#333333").ss(2,1,1).p("EgABhM/QAAP4AAP4QAAUwABUvQAAVsAAVrQABStABSs");
	this.shape_43.setTransform(-0.2,319.3);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#333333").ss(2,1,1).p("EgABhM/QABP4AAP4QAAUwAAUvQAAVsABVrQABStAASs");
	this.shape_44.setTransform(-0.2,319.3);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#333333").ss(2,1,1).p("EgABhM/QABP4AAP4QAAUwAAUvQABVsAAVrQABStAASs");
	this.shape_45.setTransform(-0.2,319.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.3,-174.5,2.4,987.5);


(lib.lettres_FAT = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0033").s().p("ABxFeIiajfIhsAAIAADfIibAAIAAq6IEIAAQCjAABFA2QBGA4AAB6QAACmiGAyICyD6gAiVgHIBzAAQBPAAAcgaQAdgbAAg4QAAg4gdgWQgegUhLAAIh1AAg");
	this.shape.setTransform(53.4,311.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF0033").s().p("ADTFeIhBiYIkkAAIhBCYIilAAIEtq7ICWAAIEvK7gAhXA+ICuAAIhWjLg");
	this.shape_1.setTransform(-63.7,187.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.lettres_FAT, new cjs.Rectangle(-102.9,0,204.1,373.7), null);


(lib.hastaggraphicbranche = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AjQAuIAShbIDlAAICqAAIgSBbg");
	this.shape.setTransform(0,4.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.hastaggraphicbranche, new cjs.Rectangle(-20.8,0,41.7,9.2), null);


(lib.glitchs = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0033").s().p("AO4ScIl7BQQABgBqJtQIn0C0UAABgABgjHgUaIVdohQVdojAAgGQRwCrABAJQABAFJgkDQJhkEAAAFQABAEEIBgQEIBfAAAFUAAVA/0gAPAAEg");
	this.shape.setTransform(-282.6,-85.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#FFFFFF").ss(2,1,1).p("EAr/AhhI9HvFIl7BQQABgBqJtQIn0C0UAABgABgjHgUaQKvkRKukQQVdojAAgGQRwCrABAJQABAFJgkDQJhkEAAAFQABAEEIBgQEIBfAAAFUAAVA/0gAPAAEg");
	this.shape_1.setTransform(-282.6,-85.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#FFFFFF").ss(2,1,1).p("AcpVzIy8p0Ij3A0QABAAmmooIlFB1I20tSQG+ixG+ixQN9lkAAgEQLjBwABAGQAAACGMioQGLipABADQAAADCsA+QCsA+AAAD");
	this.shape_2.setTransform(-282.8,-85.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF0033").s().p("AH+J4IjLArIlbnHIkLBgIy0q8ILgkkQLgklgBgDQJhBcAAAFQABACFGiLQFGiLAAADQAAACCOAzQCOAzAAADUAALAiMgAIAACg");
	this.shape_3.setTransform(-229.7,-85.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FF0033").s().p("AH+GhIjLAdIlbktIkLBAIy0nOILgjBQLgjCgBgCQJhA9AAADQABACFGhcQFGhcAAACQAAABCOAiQCOAiAAABQALWmgIABg");
	this.shape_4.setTransform(-229.7,-39.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},3).to({state:[{t:this.shape_2}]},6).to({state:[{t:this.shape_3},{t:this.shape_2}]},2).to({state:[]},2).to({state:[{t:this.shape_2}]},104).to({state:[{t:this.shape_1}]},6).to({state:[{t:this.shape}]},8).to({state:[]},5).to({state:[{t:this.shape_3},{t:this.shape_2}]},43).to({state:[{t:this.shape_4}]},4).to({state:[]},3).to({state:[{t:this.shape_1}]},68).to({state:[]},4).wait(22));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-564.8,-300,564.3,428.9);


(lib.Btn_cliquable_area = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0033").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape.setTransform(0,30);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Btn_cliquable_area, new cjs.Rectangle(-113,0,226,60), null);


(lib.masqueTexte = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// masqueTexte_1_ligne
	this.instance = new lib.masqueTexte_1_ligne();
	this.instance.parent = this;
	this.instance.setTransform(-218.8,191.5,0.008,1,0,0,0,0,25.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(6).to({_off:false},0).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.01,x:218.8},14).wait(1));

	// masqueTexte_1_ligne
	this.instance_1 = new lib.masqueTexte_1_ligne();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-218.8,136.1,0.008,1,0,0,0,0,25.5);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.01,x:218.8},14).to({_off:true},1).wait(2));

	// masqueTexte_1_ligne
	this.instance_2 = new lib.masqueTexte_1_ligne();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-218.8,80.8,0.008,1,0,0,0,0,25.5);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(2).to({_off:false},0).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.01,x:218.8},14).to({_off:true},1).wait(4));

	// masqueTexte_1_ligne
	this.instance_3 = new lib.masqueTexte_1_ligne();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-218.8,25.5,0.008,1,0,0,0,0,25.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.01,x:218.8},14).to({_off:true},1).wait(6));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-220.5,-1.9,3.5,53);


(lib.hashtag_graphic = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(64));

	// hastag graphic branche
	this.instance = new lib.hastaggraphicbranche();
	this.instance.parent = this;
	this.instance.setTransform(37.4,51.9,1.084,0.995,0,-78.3,102.1,0.1,4.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({x:6.5,y:50.1},17,cjs.Ease.quartOut).to({_off:true},5).wait(3).to({_off:false},0).to({_off:true},4).wait(2).to({_off:false,x:26.5},0).to({_off:true},5).wait(28));

	// hastag graphic branche
	this.instance_1 = new lib.hastaggraphicbranche();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-41.6,49.9,1.084,0.995,0,-78.3,102.1,0.1,4.6);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off:false},0).to({x:-7.4,y:49.8},18,cjs.Ease.quartOut).to({_off:true},4).wait(5).to({_off:false},0).to({_off:true},4).wait(31));

	// hastag graphic branche
	this.instance_2 = new lib.hastaggraphicbranche();
	this.instance_2.parent = this;
	this.instance_2.setTransform(2.7,21.7,1,1,0,0,0,0,4.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({x:-2.3,y:58.7},13,cjs.Ease.quartOut).to({_off:true},11).wait(3).to({_off:false,scaleX:2.33,scaleY:1.03,skewX:-12.8,x:25.5},0).to({_off:true},4).wait(33));

	// hastag graphic branche
	this.instance_3 = new lib.hastaggraphicbranche();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-7.3,77.7,1,1,0,0,0,0,4.6);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({x:0.7,y:41.7},14,cjs.Ease.quartOut).to({_off:true},4).wait(7).to({_off:false,regX:0.1,scaleX:1.98,scaleY:1.01,skewX:-9.8,x:19.9,y:49.7},0).to({_off:true},4).wait(4).to({_off:false,regX:0,scaleX:1,scaleY:1,skewX:0,x:0.7,y:41.7},0).to({_off:true},4).wait(21));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-18.1,17.1,64.6,57.7);


(lib.btn_playVideo = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{open:1,close:97});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_96 = function() {
		this.stop();
	}
	this.frame_135 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(96).call(this.frame_96).wait(39).call(this.frame_135).wait(1));

	// ZONE
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.004)").s().p("Am7G8IAAt3IN3AAIAAN3g");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(136));

	// Calque_16
	this.instance = new lib.line_explode();
	this.instance.parent = this;
	this.instance.setTransform(2.7,0,1,1,120);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(127).to({_off:false},0).wait(9));

	// Calque_15
	this.instance_1 = new lib.line_explode();
	this.instance_1.parent = this;
	this.instance_1.setTransform(2.8,0,0.778,1,-155.3);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(125).to({_off:false},0).wait(11));

	// Calque_14
	this.instance_2 = new lib.line_explode();
	this.instance_2.parent = this;
	this.instance_2.setTransform(2.8,0.1,1,1,-80.3);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(126).to({_off:false},0).wait(10));

	// Calque_13
	this.instance_3 = new lib.line_explode();
	this.instance_3.parent = this;
	this.instance_3.setTransform(2.9,0.1,0.807,1,69.7,0,0,0.1,-0.1);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(124).to({_off:false},0).wait(12));

	// Calque_12
	this.instance_4 = new lib.line_explode();
	this.instance_4.parent = this;
	this.instance_4.setTransform(2.8,0);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(123).to({_off:false},0).wait(13));

	// Calque_5
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgGADIANgF");
	this.shape_1.setTransform(-10.5,13.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgVALIArgU");
	this.shape_2.setTransform(-8.9,12.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgkASIBJgj");
	this.shape_3.setTransform(-7.4,12);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgzAZIBngx");
	this.shape_4.setTransform(-5.9,11.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhDAgICHg/");
	this.shape_5.setTransform(-4.4,10.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhSAnIClhN");
	this.shape_6.setTransform(-2.9,9.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhhAvIDDhc");
	this.shape_7.setTransform(-1.4,9.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhwA2IDhhr");
	this.shape_8.setTransform(0.2,8.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#FFFFFF").ss(2,1,1).p("Ah/A9ID/h5");
	this.shape_9.setTransform(1.7,7.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#FFFFFF").ss(2,1,1).p("AiOBEIEdiH");
	this.shape_10.setTransform(3.2,7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_1}]},54).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[]},34).wait(39));

	// Calque_6
	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAAgFIAAAL");
	this.shape_11.setTransform(-11.1,-13.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAAATIAAgl");
	this.shape_12.setTransform(-11.1,-12.5);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAAAhIAAhB");
	this.shape_13.setTransform(-11.1,-11.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAAAvIAAhc");
	this.shape_14.setTransform(-11.1,-9.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAAA8IAAh3");
	this.shape_15.setTransform(-11.1,-8.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAABKIAAiT");
	this.shape_16.setTransform(-11.1,-7.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAABXIAAit");
	this.shape_17.setTransform(-11.1,-5.7);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAABlIAAjJ");
	this.shape_18.setTransform(-11.1,-4.4);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAAByIAAjj");
	this.shape_19.setTransform(-11.1,-3);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAACAIAAj/");
	this.shape_20.setTransform(-11.1,-1.7);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAAiMIAAEZ");
	this.shape_21.setTransform(-11.1,-0.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_11}]},44).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[]},43).wait(39));

	// Calque_4
	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#FFFFFF").ss(2,1,1).p("AANAHIgZgM");
	this.shape_22.setTransform(16.2,-0.5);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgbgNIA3Ab");
	this.shape_23.setTransform(14.8,-1.2);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgpgUIBTAp");
	this.shape_24.setTransform(13.3,-2);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#FFFFFF").ss(2,1,1).p("Ag3gcIBwA5");
	this.shape_25.setTransform(11.9,-2.7);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhGgjICNBH");
	this.shape_26.setTransform(10.4,-3.4);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhUgrICpBX");
	this.shape_27.setTransform(9,-4.2);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhjgyIDHBl");
	this.shape_28.setTransform(7.5,-4.9);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#FFFFFF").ss(2,1,1).p("Ahxg5IDjBz");
	this.shape_29.setTransform(6.1,-5.7);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#FFFFFF").ss(2,1,1).p("AiAhBIEBCD");
	this.shape_30.setTransform(4.6,-6.4);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#FFFFFF").ss(2,1,1).p("ACPBJIkdiR");
	this.shape_31.setTransform(3.2,-7.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_22}]},35).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[]},53).wait(39));

	// Calque_3
	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAVAAIgpAA");
	this.shape_32.setTransform(-318.9,0);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgVAAIArAA");
	this.shape_33.setTransform(-318.6,0);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgZAAIAzAA");
	this.shape_34.setTransform(-317.5,0);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgfAAIA/AA");
	this.shape_35.setTransform(-315.8,0);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgnAAIBPAA");
	this.shape_36.setTransform(-313.3,0);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgyAAIBkAA");
	this.shape_37.setTransform(-310.1,0);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#FFFFFF").ss(2,1,1).p("Ag/AAIB+AA");
	this.shape_38.setTransform(-306.3,0);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhOAAICdAA");
	this.shape_39.setTransform(-301.7,0);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhfAAIC/AA");
	this.shape_40.setTransform(-296.5,0);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhzAAIDnAA");
	this.shape_41.setTransform(-290.5,0);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#FFFFFF").ss(2,1,1).p("AiJAAIETAA");
	this.shape_42.setTransform(-283.8,0);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#FFFFFF").ss(2,1,1).p("AiiAAIFFAA");
	this.shape_43.setTransform(-276.4,0);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#FFFFFF").ss(2,1,1).p("Ai9AAIF7AA");
	this.shape_44.setTransform(-268.4,0);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#FFFFFF").ss(2,1,1).p("AjaAAIG2AA");
	this.shape_45.setTransform(-259.6,0);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().s("#FFFFFF").ss(2,1,1).p("Aj6AAIH1AA");
	this.shape_46.setTransform(-250.1,0);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().s("#FFFFFF").ss(2,1,1).p("AkcAAII5AA");
	this.shape_47.setTransform(-239.9,0);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f().s("#FFFFFF").ss(2,1,1).p("AlAAAIKBAA");
	this.shape_48.setTransform(-229.1,0);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f().s("#FFFFFF").ss(2,1,1).p("AlnAAILPAA");
	this.shape_49.setTransform(-217.5,0);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f().s("#FFFFFF").ss(2,1,1).p("AmQAAIMhAA");
	this.shape_50.setTransform(-205.2,0);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f().s("#FFFFFF").ss(2,1,1).p("AmUAAIMpAA");
	this.shape_51.setTransform(-194.2,0);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f().s("#FFFFFF").ss(2,1,1).p("AmZAAIMzAA");
	this.shape_52.setTransform(-183.3,0);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f().s("#FFFFFF").ss(2,1,1).p("AmdAAIM7AA");
	this.shape_53.setTransform(-172.3,0);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f().s("#FFFFFF").ss(2,1,1).p("AmhAAINDAA");
	this.shape_54.setTransform(-161.3,0);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f().s("#FFFFFF").ss(2,1,1).p("AmmAAINNAA");
	this.shape_55.setTransform(-150.4,0);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f().s("#FFFFFF").ss(2,1,1).p("AmqAAINVAA");
	this.shape_56.setTransform(-139.4,0);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f().s("#FFFFFF").ss(2,1,1).p("AmvAAINeAA");
	this.shape_57.setTransform(-128.4,0);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f().s("#FFFFFF").ss(2,1,1).p("AmzAAINnAA");
	this.shape_58.setTransform(-117.5,0);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f().s("#FFFFFF").ss(2,1,1).p("Am4AAINxAA");
	this.shape_59.setTransform(-106.5,0);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f().s("#FFFFFF").ss(2,1,1).p("Am8AAIN5AA");
	this.shape_60.setTransform(-95.5,0);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f().s("#FFFFFF").ss(2,1,1).p("AnAAAIOBAA");
	this.shape_61.setTransform(-84.6,0);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f().s("#FFFFFF").ss(2,1,1).p("AnFAAIOLAA");
	this.shape_62.setTransform(-73.6,0);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f().s("#FFFFFF").ss(2,1,1).p("AnJAAIOTAA");
	this.shape_63.setTransform(-62.6,0);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f().s("#FFFFFF").ss(2,1,1).p("AnOAAIOdAA");
	this.shape_64.setTransform(-51.7,0);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f().s("#FFFFFF").ss(2,1,1).p("AnSAAIOlAA");
	this.shape_65.setTransform(-40.7,0);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f().s("#FFFFFF").ss(2,1,1).p("AHXAAIutAA");
	this.shape_66.setTransform(-29.8,0);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f().s("#FFFFFF").ss(2,1,1).p("AmfAAIM/AA");
	this.shape_67.setTransform(-24.2,0);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f().s("#FFFFFF").ss(2,1,1).p("AlrAAILXAA");
	this.shape_68.setTransform(-19,0);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f().s("#FFFFFF").ss(2,1,1).p("Ak7AAIJ3AA");
	this.shape_69.setTransform(-14.2,0);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f().s("#FFFFFF").ss(2,1,1).p("AkOAAIIdAA");
	this.shape_70.setTransform(-9.7,0);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f().s("#FFFFFF").ss(2,1,1).p("AjlAAIHLAA");
	this.shape_71.setTransform(-5.6,0);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f().s("#FFFFFF").ss(2,1,1).p("Ai/AAIF/AA");
	this.shape_72.setTransform(-1.8,0);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f().s("#FFFFFF").ss(2,1,1).p("AidAAIE7AA");
	this.shape_73.setTransform(1.6,0);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f().s("#FFFFFF").ss(2,1,1).p("Ah/AAID/AA");
	this.shape_74.setTransform(4.6,0);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhkAAIDJAA");
	this.shape_75.setTransform(7.3,0);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhNAAICbAA");
	this.shape_76.setTransform(9.6,0);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f().s("#FFFFFF").ss(2,1,1).p("Ag5AAIBzAA");
	this.shape_77.setTransform(11.6,0);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgpAAIBTAA");
	this.shape_78.setTransform(13.2,0);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgcAAIA5AA");
	this.shape_79.setTransform(14.5,0);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgTAAIAnAA");
	this.shape_80.setTransform(15.3,0);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgOAAIAdAA");
	this.shape_81.setTransform(15.9,0);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f().s("#FFFFFF").ss(2,1,1).p("AANAAIgZAA");
	this.shape_82.setTransform(16.1,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_51}]},1).to({state:[{t:this.shape_52}]},1).to({state:[{t:this.shape_53}]},1).to({state:[{t:this.shape_54}]},1).to({state:[{t:this.shape_55}]},1).to({state:[{t:this.shape_56}]},1).to({state:[{t:this.shape_57}]},1).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_59}]},1).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_63}]},1).to({state:[{t:this.shape_64}]},1).to({state:[{t:this.shape_65}]},1).to({state:[{t:this.shape_66}]},1).to({state:[{t:this.shape_67}]},1).to({state:[{t:this.shape_68}]},1).to({state:[{t:this.shape_69}]},1).to({state:[{t:this.shape_70}]},1).to({state:[{t:this.shape_71}]},1).to({state:[{t:this.shape_72}]},1).to({state:[{t:this.shape_73}]},1).to({state:[{t:this.shape_74}]},1).to({state:[{t:this.shape_75}]},1).to({state:[{t:this.shape_76}]},1).to({state:[{t:this.shape_77}]},1).to({state:[{t:this.shape_78}]},1).to({state:[{t:this.shape_79}]},1).to({state:[{t:this.shape_80}]},1).to({state:[{t:this.shape_81}]},1).to({state:[{t:this.shape_82}]},1).to({state:[]},1).wait(84));

	// Calque_8 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_62 = new cjs.Graphics().p("Ai/DUIAAmoIF/AAIAAGog");
	var mask_graphics_63 = new cjs.Graphics().p("Ai/DUIAAmoIF/AAIAAGog");
	var mask_graphics_64 = new cjs.Graphics().p("Ai/DUIAAmoIF/AAIAAGog");
	var mask_graphics_65 = new cjs.Graphics().p("Ai/DUIAAmoIF/AAIAAGog");
	var mask_graphics_66 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_67 = new cjs.Graphics().p("AjADUIAAmoIGAAAIAAGog");
	var mask_graphics_68 = new cjs.Graphics().p("Ai/DUIAAmoIGAAAIAAGog");
	var mask_graphics_69 = new cjs.Graphics().p("Ai/DUIAAmoIF/AAIAAGog");
	var mask_graphics_70 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_71 = new cjs.Graphics().p("Ai/DUIAAmoIGAAAIAAGog");
	var mask_graphics_72 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_73 = new cjs.Graphics().p("Ai/DUIAAmoIF/AAIAAGog");
	var mask_graphics_74 = new cjs.Graphics().p("AjADUIAAmoIGAAAIAAGog");
	var mask_graphics_75 = new cjs.Graphics().p("AjADUIAAmoIGAAAIAAGog");
	var mask_graphics_76 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_77 = new cjs.Graphics().p("Ai/DUIAAmoIF/AAIAAGog");
	var mask_graphics_78 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_79 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_80 = new cjs.Graphics().p("Ai/DUIAAmoIF/AAIAAGog");
	var mask_graphics_81 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_82 = new cjs.Graphics().p("Ai/DUIAAmoIF/AAIAAGog");
	var mask_graphics_83 = new cjs.Graphics().p("AjADUIAAmoIGAAAIAAGog");
	var mask_graphics_84 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_85 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_86 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_87 = new cjs.Graphics().p("Ai/DUIAAmoIGAAAIAAGog");
	var mask_graphics_88 = new cjs.Graphics().p("AjADUIAAmoIGAAAIAAGog");
	var mask_graphics_89 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_90 = new cjs.Graphics().p("Ai/DUIAAmoIGAAAIAAGog");
	var mask_graphics_91 = new cjs.Graphics().p("AjADUIAAmoIGAAAIAAGog");
	var mask_graphics_92 = new cjs.Graphics().p("Ai/DUIAAmoIF/AAIAAGog");
	var mask_graphics_93 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_94 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_95 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_96 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_97 = new cjs.Graphics().p("Ai/DUIAAmoIGAAAIAAGog");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(62).to({graphics:mask_graphics_62,x:38.2,y:0.7}).wait(1).to({graphics:mask_graphics_63,x:38.2,y:0.7}).wait(1).to({graphics:mask_graphics_64,x:38.2,y:0.7}).wait(1).to({graphics:mask_graphics_65,x:38.2,y:0.7}).wait(1).to({graphics:mask_graphics_66,x:38.2,y:0.7}).wait(1).to({graphics:mask_graphics_67,x:38.1,y:0.7}).wait(1).to({graphics:mask_graphics_68,x:38,y:0.7}).wait(1).to({graphics:mask_graphics_69,x:37.8,y:0.7}).wait(1).to({graphics:mask_graphics_70,x:37.5,y:0.7}).wait(1).to({graphics:mask_graphics_71,x:37.1,y:0.7}).wait(1).to({graphics:mask_graphics_72,x:36.5,y:0.7}).wait(1).to({graphics:mask_graphics_73,x:35.6,y:0.7}).wait(1).to({graphics:mask_graphics_74,x:34.5,y:0.7}).wait(1).to({graphics:mask_graphics_75,x:33.2,y:0.7}).wait(1).to({graphics:mask_graphics_76,x:31.4,y:0.7}).wait(1).to({graphics:mask_graphics_77,x:29.2,y:0.7}).wait(1).to({graphics:mask_graphics_78,x:26.6,y:0.7}).wait(1).to({graphics:mask_graphics_79,x:23.4,y:0.7}).wait(1).to({graphics:mask_graphics_80,x:20.2,y:0.7}).wait(1).to({graphics:mask_graphics_81,x:17.6,y:0.7}).wait(1).to({graphics:mask_graphics_82,x:15.4,y:0.7}).wait(1).to({graphics:mask_graphics_83,x:13.6,y:0.7}).wait(1).to({graphics:mask_graphics_84,x:12.3,y:0.7}).wait(1).to({graphics:mask_graphics_85,x:11.2,y:0.7}).wait(1).to({graphics:mask_graphics_86,x:10.4,y:0.7}).wait(1).to({graphics:mask_graphics_87,x:9.7,y:0.7}).wait(1).to({graphics:mask_graphics_88,x:9.3,y:0.7}).wait(1).to({graphics:mask_graphics_89,x:9,y:0.7}).wait(1).to({graphics:mask_graphics_90,x:8.8,y:0.7}).wait(1).to({graphics:mask_graphics_91,x:8.7,y:0.7}).wait(1).to({graphics:mask_graphics_92,x:8.6,y:0.7}).wait(1).to({graphics:mask_graphics_93,x:8.6,y:0.7}).wait(1).to({graphics:mask_graphics_94,x:8.6,y:0.7}).wait(1).to({graphics:mask_graphics_95,x:8.6,y:0.7}).wait(1).to({graphics:mask_graphics_96,x:8.6,y:0.7}).wait(1).to({graphics:mask_graphics_97,x:4.8,y:0.7}).wait(39));

	// Calque_7
	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("#FFFFFF").s().p("AiOiMIEdCRIkdCIg");
	this.shape_83.setTransform(3.2,-0.3);

	this.instance_5 = new lib.playArrow();
	this.instance_5.parent = this;
	this.instance_5.setTransform(3.2,-0.3);
	this.instance_5._off = true;

	var maskedShapeInstanceList = [this.shape_83,this.instance_5];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_83}]},62).to({state:[{t:this.instance_5}]},35).to({state:[{t:this.instance_5}]},3).to({state:[{t:this.instance_5}]},18).to({state:[]},1).wait(17));
	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(97).to({_off:false},0).wait(3).to({scaleX:0.07,scaleY:0.07},18,cjs.Ease.quartIn).to({_off:true},1).wait(17));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-44.3,-44.4,88.8,88.8);


(lib.Btn_next = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{rollOver:55,rollOut:70});

	// timeline functions:
	this.frame_54 = function() {
		this.stop();
	}
	this.frame_69 = function() {
		this.stop();
	}
	this.frame_84 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(54).call(this.frame_54).wait(15).call(this.frame_69).wait(15).call(this.frame_84).wait(1));

	// Calque_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.008)").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape.setTransform(0,29.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(85));

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgJAqIAAhDIgXAAIAAgQIBBAAIAAAQIgYAAIAABDg");
	this.shape_1.setTransform(46.1,29.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AASAqIgSgcIgRAcIgWAAIAcgqIgagpIAWAAIAPAZIAQgZIAWAAIgaApIAdAqg");
	this.shape_2.setTransform(30.2,29.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgeAqIAAhTIA8AAIAAARIgpAAIAAARIAkAAIAAAPIgkAAIAAASIAqAAIAAAQg");
	this.shape_3.setTransform(14.3,29.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAUAqIgngzIAAAzIgTAAIAAhTIASAAIAoA1IAAg1IATAAIAABTg");
	this.shape_4.setTransform(-2.7,29.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgKAQIAEgOQgEgCAAgFQAAgEADgDQADgDAEAAQAFAAADADQADADAAAEQAAAEgDAEIgIANg");
	this.shape_5.setTransform(-28.5,32.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAPAqIgYghIgIAKIAAAXIgTAAIAAhTIATAAIAAAjIAegjIAYAAIgiAlIAQAWIASAYg");
	this.shape_6.setTransform(-38,29.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgfAfQgMgNAAgSQAAgSAMgMQAOgNARAAQASAAANANQANAMAAASQAAASgNANQgNAMgSAAQgRAAgOgMgAgQgSQgIAIAAAKQAAALAIAIQAGAIAKAAQALAAAHgIQAHgIAAgLQAAgKgHgIQgHgIgLAAQgKAAgGAIg");
	this.shape_7.setTransform(-55.7,29.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#333333").ss(1,1,1).p("AxpAAMAjTAAA");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(85));

	// Layer_2
	this.instance = new lib.Btn_cliquable_area();
	this.instance.parent = this;
	this.instance.setTransform(0,29.5,1,1,0,0,0,0,30);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({alpha:0.012},46).wait(6).to({y:30},0).to({alpha:1},14).wait(1).to({alpha:0.012},13).wait(2));

	// BG
	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("rgba(0,0,0,0.098)").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape_9.setTransform(0,29.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_9).wait(85));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-114,-1,228,60.5);


(lib.Btn_detail = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"rollOver":55,"rollOut":70});

	// timeline functions:
	this.frame_54 = function() {
		this.stop();
	}
	this.frame_69 = function() {
		this.stop();
	}
	this.frame_84 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(54).call(this.frame_54).wait(15).call(this.frame_69).wait(15).call(this.frame_84).wait(1));

	// Calque_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.008)").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape.setTransform(0,29.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(85));

	// content
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgOAyIAAhkIAdAAIAABkg");
	this.shape_1.setTransform(-85.4,29.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgyAOIAAgcIBlAAIAAAcg");
	this.shape_2.setTransform(-85.4,29.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgaAqIAAhTIASAAIAABDIAjAAIAAAQg");
	this.shape_3.setTransform(48.4,29.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgIAqIAAhTIARAAIAABTg");
	this.shape_4.setTransform(34.7,29.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AAZAqIgHgSIgjAAIgHASIgUAAIAkhTIARAAIAkBTgAgKAIIAUAAIgKgYg");
	this.shape_5.setTransform(20.2,29.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgJAqIAAhDIgXAAIAAgQIBBAAIAAAQIgYAAIAABDg");
	this.shape_6.setTransform(4.1,29.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgeAqIAAhTIA8AAIAAARIgpAAIAAARIAkAAIAAAPIgkAAIAAASIAqAAIAAAQg");
	this.shape_7.setTransform(-11.3,29.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AglAqIAAhTIAeAAQAVAAAMALQAMALAAATQAAATgMALQgMAMgWAAgAgTAaIANAAQAMAAAGgHQAHgGAAgNQAAgLgHgHQgGgGgOgBIgLAAg");
	this.shape_8.setTransform(-27.8,29.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#333333").ss(1,1,1).p("AxpAAMAjTAAA");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(85));

	// Calque_7
	this.instance = new lib.Btn_cliquable_area();
	this.instance.parent = this;
	this.instance.setTransform(0,29.5,1,1,0,0,0,0,30);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({alpha:0.012},46).wait(6).to({y:30},0).to({alpha:1},14).wait(1).to({alpha:0.012},13).wait(2));

	// BG
	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("rgba(0,0,0,0.004)").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape_10.setTransform(0,29.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_10).wait(85));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-114,-1,228,60.5);


(lib.bmw = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_32 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(32).call(this.frame_32).wait(1));

	// Calque_2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("EAyIAdNImkBZQABgBrPurIopDIUAABgABgm1gWmIXupbQXvpdAAgGQTqC9ABAKQABAFKhkeQKhkgABAFQAAAGElBpQEkBqAAAFUAAXBGmgAQAAFg");
	var mask_graphics_1 = new cjs.Graphics().p("EAnvAcaImkBYQABAArPurIopDHUAABAAAgm1gWmIXvpbQXupdAAgGQTqC9ABAKQABAFKhkfQKhkfABAFQAAAFElBqQEkBpAAAFUAAXBGngAQAAEg");
	var mask_graphics_2 = new cjs.Graphics().p("AfdbxImkBZQABgBrPuqIopDHUAABgABgm1gWmIXvpbQXupcAAgHQTqC9ABAKQABAFKhkeQKhkfABAFQAAAFElBpQEkBqAAAFUAAXBGngAQAAEg");
	var mask_graphics_3 = new cjs.Graphics().p("AZLbSImkBZQABgBrPuqIooDHUAABgABgm2gWmIXvpbQXupcAAgHQTqC9ABAKQABAFKhkeQKhkfABAFQAAAFElBpQEkBqAAAFUAAXBGngAQAAEg");
	var mask_graphics_4 = new cjs.Graphics().p("AUaa7ImkBYQABAArPurIonDHUAABAAAgm2gWmIXvpbQXupdgBgGQTrC9ABAKQAAAFKikfQKhkfAAAFQABAFEkBqQEkBpABAFUAAXBGngARAAEg");
	var mask_graphics_5 = new cjs.Graphics().p("AQwapImkBYQABAArOurIooDHUAABAAAgm2gWmIXvpbQXvpdgBgGQTqC9ABAKQAAAFKikfQKhkfAAAFQABAFEkBqQEkBpABAFUAAXBGngARAAEg");
	var mask_graphics_6 = new cjs.Graphics().p("AQeabImkBYQABAArOurIopDHUAABAAAgm2gWmIXvpbQXvpdAAgGQTpC9ABAKQABAFKhkfQKhkfABAFQAAAFElBqQEkBpAAAFUAAXBGngAQAAEg");
	var mask_graphics_7 = new cjs.Graphics().p("AQeaPImkBZQABgBrOuqIopDHUAABgABgm2gWmIXvpbQXvpcAAgHQTpC9ABAKQABAFKhkeQKhkfABAFQAAAFElBpQEkBqAAAFUAAXBGngAQAAEg");
	var mask_graphics_8 = new cjs.Graphics().p("AQeaGImkBZQABgBrOuqIopDHUAABgABgm2gWmIXvpbQXvpcAAgHQTpC9ABAKQABAFKhkeQKhkfABAFQAAAFElBpQEkBqAAAFUAAXBGngAQAAEg");
	var mask_graphics_9 = new cjs.Graphics().p("AQeZ/ImkBYQABAArOurIopDHUAABAAAgm2gWmIXvpbQXvpdAAgGQTpC9ABAKQABAFKhkfQKhkfABAFQAAAFElBqQEkBpAAAFUAAXBGngAQAAEg");
	var mask_graphics_10 = new cjs.Graphics().p("AQeZ4ImkBZQABgBrOuqIopDHUAABgABgm2gWmIXvpbQXvpcAAgHQTpC9ABAKQABAFKhkeQKhkfABAFQAAAFElBpQEkBqAAAFUAAXBGngAQAAEg");
	var mask_graphics_11 = new cjs.Graphics().p("AQeZzImkBZQABgBrOuqIopDHUAABgABgm2gWmIXvpbQXvpcAAgHQTpC9ABAKQABAFKhkeQKhkfABAFQAAAFElBpQEkBqAAAFUAAXBGngAQAAEg");
	var mask_graphics_12 = new cjs.Graphics().p("AQeZvImkBYQABAArOurIopDHUAABAAAgm2gWmIXvpbQXvpdAAgGQTpC9ABAKQABAFKhkfQKhkfABAFQAAAFElBqQEkBpAAAFUAAXBGngAQAAEg");
	var mask_graphics_13 = new cjs.Graphics().p("AQeZrImkBZQABgBrOuqIopDHUAABgABgm2gWmIXvpbQXvpcAAgHQTpC9ABAKQABAFKhkeQKhkfABAFQAAAFElBpQEkBqAAAFUAAXBGngAQAAEg");
	var mask_graphics_14 = new cjs.Graphics().p("AQeZoImkBYQABAArOurIopDHUAABAAAgm2gWmIXvpbQXvpdAAgGQTpC9ABAKQABAFKhkfQKhkfABAFQAAAFElBqQEkBpAAAFUAAXBGngAQAAEg");
	var mask_graphics_15 = new cjs.Graphics().p("AQeZlImkBZQABgBrOuqIopDHUAABgABgm2gWmIXvpbQXvpcAAgHQTpC9ABAKQABAFKhkeQKhkfABAFQAAAFElBpQEkBqAAAFUAAXBGngAQAAEg");
	var mask_graphics_16 = new cjs.Graphics().p("AQeZjImkBYQABAArOurIopDHUAABAAAgm2gWmIXvpbQXvpdAAgGQTpC9ABAKQABAFKhkfQKhkfABAFQAAAFElBqQEkBpAAAFUAAXBGngAQAAEg");
	var mask_graphics_17 = new cjs.Graphics().p("AQeZhImkBZQABgBrOuqIopDHUAABgABgm2gWmIXvpbQXvpcAAgHQTpC9ABAKQABAFKhkeQKhkfABAFQAAAFElBpQEkBqAAAFUAAXBGngAQAAEg");
	var mask_graphics_18 = new cjs.Graphics().p("AQeZfImkBZQABgBrOuqIopDHUAABgABgm2gWmIXvpbQXvpcAAgHQTpC9ABAKQABAFKhkeQKhkfABAFQAAAFElBpQEkBqAAAFUAAXBGngAQAAEg");
	var mask_graphics_19 = new cjs.Graphics().p("AQeZeImkBZQABgBrOuqIopDHUAABgABgm2gWmIXvpbQXvpcAAgHQTpC9ABAKQABAFKhkeQKhkfABAFQAAAFElBpQEkBqAAAFUAAXBGngAQAAEg");
	var mask_graphics_20 = new cjs.Graphics().p("AQeZdImkBZQABgBrOuqIopDHUAABgABgm2gWmIXvpbQXvpcAAgHQTpC9ABAKQABAFKhkeQKhkfABAFQAAAFElBpQEkBqAAAFUAAXBGngAQAAEg");
	var mask_graphics_21 = new cjs.Graphics().p("AQeZcImkBZQABgBrOuqIopDHUAABgABgm2gWmIXvpbQXvpcAAgHQTpC9ABAKQABAFKhkeQKhkfABAFQAAAFElBpQEkBqAAAFUAAXBGngAQAAEg");
	var mask_graphics_22 = new cjs.Graphics().p("AQeZbImkBZQABgBrOuqIopDHUAABgABgm2gWmIXvpbQXvpcAAgHQTpC9ABAKQABAFKhkeQKhkfABAFQAAAFElBpQEkBqAAAFUAAXBGngAQAAEg");
	var mask_graphics_23 = new cjs.Graphics().p("AQeZbImkBYQABAArOurIopDHUAABAAAgm2gWmIXvpbQXvpdAAgGQTpC9ABAKQABAFKhkfQKhkfABAFQAAAFElBqQEkBpAAAFUAAXBGngAQAAEg");
	var mask_graphics_24 = new cjs.Graphics().p("AQeZaImkBZQABgBrOuqIopDHUAABgABgm2gWmIXvpbQXvpcAAgHQTpC9ABAKQABAFKhkeQKhkfABAFQAAAFElBpQEkBqAAAFUAAXBGngAQAAEg");
	var mask_graphics_25 = new cjs.Graphics().p("AQeZaImkBZQABgBrOuqIopDHUAABgABgm2gWmIXvpbQXvpcAAgHQTpC9ABAKQABAFKhkeQKhkfABAFQAAAFElBpQEkBqAAAFUAAXBGngAQAAEg");
	var mask_graphics_26 = new cjs.Graphics().p("AQeZaImkBZQABgBrOuqIopDHUAABgABgm2gWmIXvpbQXvpcAAgHQTpC9ABAKQABAFKhkeQKhkfABAFQAAAFElBpQEkBqAAAFUAAXBGngAQAAEg");
	var mask_graphics_27 = new cjs.Graphics().p("AQeZaImkBZQABgBrOurIopDIUAABgABgm2gWmIXvpbQXvpdAAgGQTpC9ABAKQABAFKhkeQKhkgABAFQAAAGElBpQEkBqAAAFUAAXBGmgAQAAFg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:527.5,y:293.7}).wait(1).to({graphics:mask_graphics_1,x:461,y:288.5}).wait(1).to({graphics:mask_graphics_2,x:408.1,y:284.5}).wait(1).to({graphics:mask_graphics_3,x:367.8,y:281.4}).wait(1).to({graphics:mask_graphics_4,x:337.4,y:279}).wait(1).to({graphics:mask_graphics_5,x:314,y:277.2}).wait(1).to({graphics:mask_graphics_6,x:279.1,y:275.8}).wait(1).to({graphics:mask_graphics_7,x:249.8,y:274.7}).wait(1).to({graphics:mask_graphics_8,x:225.9,y:273.8}).wait(1).to({graphics:mask_graphics_9,x:206.3,y:273}).wait(1).to({graphics:mask_graphics_10,x:190.1,y:272.4}).wait(1).to({graphics:mask_graphics_11,x:176.5,y:271.9}).wait(1).to({graphics:mask_graphics_12,x:165,y:271.4}).wait(1).to({graphics:mask_graphics_13,x:155.4,y:271.1}).wait(1).to({graphics:mask_graphics_14,x:147.2,y:270.7}).wait(1).to({graphics:mask_graphics_15,x:140.2,y:270.5}).wait(1).to({graphics:mask_graphics_16,x:134.4,y:270.2}).wait(1).to({graphics:mask_graphics_17,x:129.4,y:270.1}).wait(1).to({graphics:mask_graphics_18,x:125.3,y:269.9}).wait(1).to({graphics:mask_graphics_19,x:121.8,y:269.8}).wait(1).to({graphics:mask_graphics_20,x:119,y:269.7}).wait(1).to({graphics:mask_graphics_21,x:116.7,y:269.6}).wait(1).to({graphics:mask_graphics_22,x:114.8,y:269.5}).wait(1).to({graphics:mask_graphics_23,x:113.4,y:269.4}).wait(1).to({graphics:mask_graphics_24,x:112.4,y:269.4}).wait(1).to({graphics:mask_graphics_25,x:111.6,y:269.4}).wait(1).to({graphics:mask_graphics_26,x:111.2,y:269.4}).wait(1).to({graphics:mask_graphics_27,x:111.1,y:269.4}).wait(6));

	// Calque_1
	this.instance = new lib.radar_1();
	this.instance.parent = this;
	this.instance.setTransform(110.9,307);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(33));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.bloc_visuel = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":63});

	// timeline functions:
	this.frame_62 = function() {
		this.stop();
	}
	this.frame_99 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(62).call(this.frame_62).wait(37).call(this.frame_99).wait(1));

	// masque VISUEL (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("EgyHBDvMAAAiHdMBkOAAAMAAACHdg");
	var mask_graphics_63 = new cjs.Graphics().p("EgyHBDvMAAAiHdMBkPAAAMAAACHdg");
	var mask_graphics_64 = new cjs.Graphics().p("EgyEBDvMAAAiHdMBkJAAAMAAACHdg");
	var mask_graphics_65 = new cjs.Graphics().p("Egx8BDvMAAAiHdMBj5AAAMAAACHdg");
	var mask_graphics_66 = new cjs.Graphics().p("EgxvBDvMAAAiHdMBjfAAAMAAACHdg");
	var mask_graphics_67 = new cjs.Graphics().p("EgxdBDvMAAAiHdMBi7AAAMAAACHdg");
	var mask_graphics_68 = new cjs.Graphics().p("EgxGBDvMAAAiHdMBiNAAAMAAACHdg");
	var mask_graphics_69 = new cjs.Graphics().p("EgwpBDvMAAAiHdMBhTAAAMAAACHdg");
	var mask_graphics_70 = new cjs.Graphics().p("EgwIBDvMAAAiHdMBgRAAAMAAACHdg");
	var mask_graphics_71 = new cjs.Graphics().p("EgvhBDvMAAAiHdMBfDAAAMAAACHdg");
	var mask_graphics_72 = new cjs.Graphics().p("Egu1BDvMAAAiHdMBdrAAAMAAACHdg");
	var mask_graphics_73 = new cjs.Graphics().p("EguEBDvMAAAiHdMBcJAAAMAAACHdg");
	var mask_graphics_74 = new cjs.Graphics().p("EgtOBDvMAAAiHdMBadAAAMAAACHdg");
	var mask_graphics_75 = new cjs.Graphics().p("EgsSBDvMAAAiHdMBYlAAAMAAACHdg");
	var mask_graphics_76 = new cjs.Graphics().p("EgrSBDvMAAAiHdMBWlAAAMAAACHdg");
	var mask_graphics_77 = new cjs.Graphics().p("EgqMBDvMAAAiHdMBUZAAAMAAACHdg");
	var mask_graphics_78 = new cjs.Graphics().p("EgpBBDvMAAAiHdMBSDAAAMAAACHdg");
	var mask_graphics_79 = new cjs.Graphics().p("EgnxBDvMAAAiHdMBPjAAAMAAACHdg");
	var mask_graphics_80 = new cjs.Graphics().p("EgmcBDvMAAAiHdMBM5AAAMAAACHdg");
	var mask_graphics_81 = new cjs.Graphics().p("EglBBDvMAAAiHdMBKDAAAMAAACHdg");
	var mask_graphics_82 = new cjs.Graphics().p("EgjiBDvMAAAiHdMBHFAAAMAAACHdg");
	var mask_graphics_83 = new cjs.Graphics().p("Egh9BDvMAAAiHdMBD7AAAMAAACHdg");
	var mask_graphics_84 = new cjs.Graphics().p("EggTBDvMAAAiHdMBAnAAAMAAACHdg");
	var mask_graphics_85 = new cjs.Graphics().p("EgekBDvMAAAiHdMA9JAAAMAAACHdg");
	var mask_graphics_86 = new cjs.Graphics().p("EgcwBDvMAAAiHdMA5hAAAMAAACHdg");
	var mask_graphics_87 = new cjs.Graphics().p("Ega2BDvMAAAiHdMA1tAAAMAAACHdg");
	var mask_graphics_88 = new cjs.Graphics().p("EgY4BDvMAAAiHdMAxxAAAMAAACHdg");
	var mask_graphics_89 = new cjs.Graphics().p("EgW0BDvMAAAiHdMAtpAAAMAAACHdg");
	var mask_graphics_90 = new cjs.Graphics().p("EgUrBDvMAAAiHdMApXAAAMAAACHdg");
	var mask_graphics_91 = new cjs.Graphics().p("EgSdBDvMAAAiHdMAk7AAAMAAACHdg");
	var mask_graphics_92 = new cjs.Graphics().p("EgQKBDvMAAAiHdMAgVAAAMAAACHdg");
	var mask_graphics_93 = new cjs.Graphics().p("EgNxBDvMAAAiHdIbjAAMAAACHdg");
	var mask_graphics_94 = new cjs.Graphics().p("EgLUBDvMAAAiHdIWpAAMAAACHdg");
	var mask_graphics_95 = new cjs.Graphics().p("EgIxBDvMAAAiHdIRjAAMAAACHdg");
	var mask_graphics_96 = new cjs.Graphics().p("EgGJBDvMAAAiHdIMTAAMAAACHdg");
	var mask_graphics_97 = new cjs.Graphics().p("EgDcBDvMAAAiHdIG5AAMAAACHdg");
	var mask_graphics_98 = new cjs.Graphics().p("EgAqBDvMAAAiHdIBVAAMAAACHdg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:-321.2,y:-19}).wait(63).to({graphics:mask_graphics_63,x:-321.2,y:-19}).wait(1).to({graphics:mask_graphics_64,x:-321.5,y:-19}).wait(1).to({graphics:mask_graphics_65,x:-322.2,y:-19}).wait(1).to({graphics:mask_graphics_66,x:-323.5,y:-19}).wait(1).to({graphics:mask_graphics_67,x:-325.3,y:-19}).wait(1).to({graphics:mask_graphics_68,x:-327.7,y:-19}).wait(1).to({graphics:mask_graphics_69,x:-330.5,y:-19}).wait(1).to({graphics:mask_graphics_70,x:-333.9,y:-19}).wait(1).to({graphics:mask_graphics_71,x:-337.7,y:-19}).wait(1).to({graphics:mask_graphics_72,x:-342.1,y:-19}).wait(1).to({graphics:mask_graphics_73,x:-347,y:-19}).wait(1).to({graphics:mask_graphics_74,x:-352.5,y:-19}).wait(1).to({graphics:mask_graphics_75,x:-358.4,y:-19}).wait(1).to({graphics:mask_graphics_76,x:-364.9,y:-19}).wait(1).to({graphics:mask_graphics_77,x:-371.8,y:-19}).wait(1).to({graphics:mask_graphics_78,x:-379.3,y:-19}).wait(1).to({graphics:mask_graphics_79,x:-387.3,y:-19}).wait(1).to({graphics:mask_graphics_80,x:-395.9,y:-19}).wait(1).to({graphics:mask_graphics_81,x:-404.9,y:-19}).wait(1).to({graphics:mask_graphics_82,x:-414.5,y:-19}).wait(1).to({graphics:mask_graphics_83,x:-424.5,y:-19}).wait(1).to({graphics:mask_graphics_84,x:-435.1,y:-19}).wait(1).to({graphics:mask_graphics_85,x:-446.2,y:-19}).wait(1).to({graphics:mask_graphics_86,x:-457.9,y:-19}).wait(1).to({graphics:mask_graphics_87,x:-470,y:-19}).wait(1).to({graphics:mask_graphics_88,x:-482.7,y:-19}).wait(1).to({graphics:mask_graphics_89,x:-495.9,y:-19}).wait(1).to({graphics:mask_graphics_90,x:-509.5,y:-19}).wait(1).to({graphics:mask_graphics_91,x:-523.8,y:-19}).wait(1).to({graphics:mask_graphics_92,x:-538.5,y:-19}).wait(1).to({graphics:mask_graphics_93,x:-553.7,y:-19}).wait(1).to({graphics:mask_graphics_94,x:-569.5,y:-19}).wait(1).to({graphics:mask_graphics_95,x:-585.8,y:-19}).wait(1).to({graphics:mask_graphics_96,x:-602.6,y:-19}).wait(1).to({graphics:mask_graphics_97,x:-619.9,y:-19}).wait(1).to({graphics:mask_graphics_98,x:-637.7,y:-19}).wait(2));

	// Calque_3
	this.instance = new lib.glitchs();
	this.instance.parent = this;
	this.instance.setTransform(-278,-70.9,1,1,0,0,0,-282.7,-85.5);
	this.instance._off = true;

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(10).to({_off:false},0).to({_off:true},89).wait(1));

	// Calque_2
	this.instance_1 = new lib.bmw();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-382,-350.3,0.905,0.905,0,0,0,0,-0.3);
	this.instance_1._off = true;

	var maskedShapeInstanceList = [this.instance_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(24).to({_off:false},0).wait(76));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.bloc_video = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"open":1,"close":42});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_34 = function() {
		this.btn_playVideo.gotoAndPlay("open");
	}
	this.frame_41 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(34).call(this.frame_34).wait(7).call(this.frame_41).wait(16));

	// PLAY btn
	this.btn_playVideo = new lib.btn_playVideo();
	this.btn_playVideo.name = "btn_playVideo";
	this.btn_playVideo.parent = this;
	this.btn_playVideo.setTransform(358.3,-255.7);

	this.timeline.addTween(cjs.Tween.get(this.btn_playVideo).wait(57));

	// masque video (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_43 = new cjs.Graphics().p("Ega8A8AMAAAh3/MA7rAAAMAAAB3/g");
	var mask_graphics_44 = new cjs.Graphics().p("EgYrA8AMAAAh3/MA3HAAAMAAAB3/g");
	var mask_graphics_45 = new cjs.Graphics().p("EgWaA8AMAAAh3/MAyiAAAMAAAB3/g");
	var mask_graphics_46 = new cjs.Graphics().p("EgUJA8AMAAAh3/MAt+AAAMAAAB3/g");
	var mask_graphics_47 = new cjs.Graphics().p("EgR4A8AMAAAh3/MApaAAAMAAAB3/g");
	var mask_graphics_48 = new cjs.Graphics().p("EgPnA8AMAAAh3/MAk1AAAMAAAB3/g");
	var mask_graphics_49 = new cjs.Graphics().p("EgNWA8AMAAAh3/MAgRAAAMAAAB3/g");
	var mask_graphics_50 = new cjs.Graphics().p("EgLFA8AMAAAh3/IbtAAMAAAB3/g");
	var mask_graphics_51 = new cjs.Graphics().p("EgI0A8AMAAAh3/IXIAAMAAAB3/g");
	var mask_graphics_52 = new cjs.Graphics().p("EgGjA8AMAAAh3/ISkAAMAAAB3/g");
	var mask_graphics_53 = new cjs.Graphics().p("EgESA8AMAAAh3/IOAAAMAAAB3/g");
	var mask_graphics_54 = new cjs.Graphics().p("EgCBA8AMAAAh3/IJbAAMAAAB3/g");
	var mask_graphics_55 = new cjs.Graphics().p("EAAPA8AMAAAh3/IE4AAMAAAB3/g");
	var mask_graphics_56 = new cjs.Graphics().p("EACgA8AMAAAh3/IAUAAMAAAB3/g");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(43).to({graphics:mask_graphics_43,x:209.5,y:0}).wait(1).to({graphics:mask_graphics_44,x:194.8,y:0}).wait(1).to({graphics:mask_graphics_45,x:180,y:0}).wait(1).to({graphics:mask_graphics_46,x:165.3,y:0}).wait(1).to({graphics:mask_graphics_47,x:150.6,y:0}).wait(1).to({graphics:mask_graphics_48,x:135.8,y:0}).wait(1).to({graphics:mask_graphics_49,x:121.1,y:0}).wait(1).to({graphics:mask_graphics_50,x:106.4,y:0}).wait(1).to({graphics:mask_graphics_51,x:91.6,y:0}).wait(1).to({graphics:mask_graphics_52,x:76.9,y:0}).wait(1).to({graphics:mask_graphics_53,x:62.2,y:0}).wait(1).to({graphics:mask_graphics_54,x:47.4,y:0}).wait(1).to({graphics:mask_graphics_55,x:32.7,y:0}).wait(1).to({graphics:mask_graphics_56,x:18,y:0}).wait(1));

	// VIDEO
	this.videoContainer = new lib.video();
	this.videoContainer.name = "videoContainer";
	this.videoContainer.parent = this;
	this.videoContainer.setTransform(36.7,765,0.659,0.659,0,0,0,0.3,480.2);

	var maskedShapeInstanceList = [this.videoContainer];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.videoContainer).wait(1).to({y:1},40,cjs.Ease.quartInOut).wait(16));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(36.5,-300.1,382.1,1381.2);


(lib.bloc_NEXT_btn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":40});

	// timeline functions:
	this.frame_39 = function() {
		this.stop();
	}
	this.frame_56 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(39).call(this.frame_39).wait(17).call(this.frame_56).wait(1));

	// CTA DETAIL
	this.btnNext = new lib.Btn_next();
	this.btnNext.name = "btnNext";
	this.btnNext.parent = this;
	this.btnNext.setTransform(-340.5,411.5,1,1,0,0,0,0,26.5);

	this.timeline.addTween(cjs.Tween.get(this.btnNext).to({y:350.5},39,cjs.Ease.quartInOut).wait(2).to({y:411.5},14,cjs.Ease.quartInOut).to({_off:true},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-454,384.5,227,60);


(lib.bloc_lignes = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":166,close_lines:209});

	// timeline functions:
	this.frame_165 = function() {
		this.stop();
	}
	this.frame_208 = function() {
		this.stop();
	}
	this.frame_229 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(165).call(this.frame_165).wait(43).call(this.frame_208).wait(21).call(this.frame_229).wait(1));

	// ligne
	this.instance = new lib.ligne();
	this.instance.parent = this;
	this.instance.setTransform(-454,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(166).to({x:-883},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:-454},0).wait(21));

	// ligne
	this.instance_1 = new lib.ligne();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-227,406.5,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(119).to({y:320.5},40,cjs.Ease.quartInOut).wait(7).to({y:406.5},0).to({x:-883.2},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:-227,y:320.5},0).to({y:406.5},17,cjs.Ease.quartInOut).wait(4));

	// ligne
	this.instance_2 = new lib.ligne();
	this.instance_2.parent = this;
	this.instance_2.setTransform(0,406.5,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(166).to({x:-883.4},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:0},0).wait(21));

	// ligne
	this.instance_3 = new lib.ligne();
	this.instance_3.parent = this;
	this.instance_3.setTransform(227,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(124).to({y:320},40,cjs.Ease.quartInOut).to({_off:true},2).wait(43).to({_off:false},0).to({y:406},17,cjs.Ease.quartInOut).wait(4));

	// ligne
	this.instance_4 = new lib.ligne();
	this.instance_4.parent = this;
	this.instance_4.setTransform(454.1,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(166).to({x:-883.4},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:454.1},0).wait(21));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-455.4,-174.5,910.5,988);


(lib.bloc_hashtag = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_61 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(61).call(this.frame_61).wait(2));

	// Layer_1
	this.instance = new lib.hashtag_graphic("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(-456,243.2,2.317,2.317,0,0,0,-0.1,50.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true},61).wait(2));

	// Layer_5 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_28 = new cjs.Graphics().p("AtWBpIAAjRIatAAIAADRg");
	var mask_graphics_32 = new cjs.Graphics().p("AtWDSIAAmjIatAAIAAGjg");
	var mask_graphics_40 = new cjs.Graphics().p("AtWHVIAAupIatAAIAAOpg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(28).to({graphics:mask_graphics_28,x:-400.4,y:265.5}).wait(4).to({graphics:mask_graphics_32,x:-400.4,y:255}).wait(8).to({graphics:mask_graphics_40,x:-400.4,y:262}).wait(23));

	// Layer_3
	this.instance_1 = new lib.txt_hashtag();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-421.5,244.1,1,1,0,0,0,0,31.6);
	this.instance_1._off = true;

	var maskedShapeInstanceList = [this.instance_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(28).to({_off:false},0).to({_off:true},34).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-497.8,166.1,149.7,129.4);


(lib.bloc_CTA = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":40});

	// timeline functions:
	this.frame_39 = function() {
		this.stop();
	}
	this.frame_56 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(39).call(this.frame_39).wait(17).call(this.frame_56).wait(1));

	// CTA DETAIL
	this.btnCta = new lib.Btn_detail();
	this.btnCta.name = "btnCta";
	this.btnCta.parent = this;
	this.btnCta.setTransform(-340.5,411.5,1,1,0,0,0,0,26.5);

	this.timeline.addTween(cjs.Tween.get(this.btnCta).to({y:350.5},39,cjs.Ease.quartInOut).wait(2).to({y:411.5},14,cjs.Ease.quartInOut).to({_off:true},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-454,384.5,227,60);


(lib.blocInfostextes = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":80});

	// timeline functions:
	this.frame_79 = function() {
		this.stop();
	}
	this.frame_120 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(79).call(this.frame_79).wait(41).call(this.frame_120).wait(1));

	// Bloc Hashtag
	this.instance = new lib.bloc_hashtag();
	this.instance.parent = this;
	this.instance.setTransform(-391.5,251.2,0.9,0.9,0,0,0,-412.9,244.7);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(62).to({_off:false},0).to({_off:true},18).wait(41));

	// masque Titre
	this.instance_1 = new lib.masque_generique();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-527.9,-13.9,0.007,0.471,0,0,0,0,17.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(34).to({_off:false},0).to({regX:0.5,scaleX:1.92,x:-527.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.01,x:-228.9},10,cjs.Ease.quartInOut).to({_off:true},1).wait(26).to({_off:false},0).to({regX:0.5,scaleX:1.92,x:-527.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.01,x:-528.7},10,cjs.Ease.quartInOut).to({_off:true},1).wait(21));

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0033").s().p("AASA4IgYgjIgRAAIAAAjIgZAAIAAhvIAqAAQAaAAALAJQALAJAAATQAAAagWAIIAdAogAgXgBIASAAQAMAAAFgDQAEgFAAgJQAAgIgFgEQgEgEgLAAIgTAAg");
	this.shape.setTransform(-424.3,-14.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF0033").s().p("AAiA4IgLgYIguAAIgJAYIgbAAIAwhvIAXAAIAwBvgAgNAKIAbAAIgOgfg");
	this.shape_1.setTransform(-448.5,-14.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF0033").s().p("AgxA4IAAhvIAnAAQAdAAAPAPQAQAOABAaQAAAZgQAQQgQAPgeAAgAgZAiIAQAAQAQAAAJgJQAKgJgBgQQABgPgKgJQgJgJgSAAIgOAAg");
	this.shape_2.setTransform(-472.3,-14.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF0033").s().p("AAiA4IgKgYIgvAAIgKAYIgbAAIAxhvIAXAAIAxBvgAgNAKIAbAAIgOgfg");
	this.shape_3.setTransform(-496.7,-14.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FF0033").s().p("AASA4IgYgjIgRAAIAAAjIgZAAIAAhvIAqAAQAaAAALAJQALAJAAATQAAAagWAIIAdAogAgXgBIASAAQAMAAAFgDQAEgFAAgJQAAgIgFgEQgEgEgLAAIgTAAg");
	this.shape_4.setTransform(-520.1,-14.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},34).to({state:[]},55).to({state:[]},1).wait(31));

	// masque Titre (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_17 = new cjs.Graphics().p("EgpOArJMAAAhWRMA3SAAAMAAABWRg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(17).to({graphics:mask_graphics_17,x:-263.9,y:-6.1}).wait(24).to({graphics:null,x:0,y:0}).wait(80));

	// Layer_5
	this.instance_2 = new lib.lettres_FAT();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-433.8,-73,1,1,0,0,0,0,186.8);
	this.instance_2._off = true;

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FF0033").s().p("AByFdIibjfIhsAAIAADfIicAAIAAq5IEJAAQCjAABFA3QBFA2AAB6QAACoiFAwICyD6gAiVgIIByAAQBQAAAcgZQAdgaAAg5QAAg5gegUQgdgWhLAAIh1AAg");
	this.shape_5.setTransform(-226.8,131.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FF0033").s().p("ADTFeIhBiXIkjAAIhCCXIilAAIEtq6ICWAAIEvK6gAhXA+ICvAAIhXjLg");
	this.shape_6.setTransform(-459.1,7.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FF0033").s().p("Ak8FdIAAq5ID3AAQC6AABkBbQBkBbAACjQAACihhBgQhiBejHAAgAigDTIBjAAQBqAAA6g2QA5g2AAhmQAAhmg5g4Qg6g4h2ABIhXAAg");
	this.shape_7.setTransform(-333,-116);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FF0033").s().p("AByFdIiajeIhsAAIAADeIidAAIAAq5IEKAAQCiAABFA3QBFA3AAB6QAACmiEAyICwD5gAiUgHIBxAAQBPAAAdgaQAegbAAg4QAAg4gfgWQgegVhJABIh1AAg");
	this.shape_8.setTransform(-495.6,-116);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FF0033").s().p("Ak8FdIAAq5ID3AAQC6AABkBbQBkBbAACjQAACihiBgQhhBejHAAgAigDTIBjAAQBqAAA5g2QA6g2AAhmQAAhmg6g4Qg5g4h2ABIhXAAg");
	this.shape_9.setTransform(-323.9,106.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FF0033").s().p("ADTFeIhAiXIklAAIhBCXIilAAIEtq7ICXAAIEtK7gAhWA+ICtAAIhWjLg");
	this.shape_10.setTransform(-238,-17.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FF0033").s().p("Ak8FeIAAq7ID3AAQC6AABkBcQBkBbAACkQAACihiBeQhhBfjHABgAigDTIBjAAQBqAAA5g2QA6g1AAhoQAAhmg6g3Qg5g3h2gBIhXAAg");
	this.shape_11.setTransform(-477.5,-17.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FF0033").s().p("ADUFeIhCiXIkjAAIhBCXIinAAIEvq7ICVAAIEuK7gAhXA+ICvAAIhYjLg");
	this.shape_12.setTransform(-480.5,-141);

	var maskedShapeInstanceList = [this.instance_2,this.shape_5,this.shape_6,this.shape_7,this.shape_8,this.shape_9,this.shape_10,this.shape_11,this.shape_12];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_2}]},17).to({state:[{t:this.instance_2}]},14).to({state:[{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5}]},1).to({state:[{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9}]},5).to({state:[]},4).to({state:[]},39).wait(41));
	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(17).to({_off:false},0).to({x:-276.8},14,cjs.Ease.quartOut).to({_off:true},1).wait(89));

	// masque Titre
	this.instance_3 = new lib.masque_generique();
	this.instance_3.parent = this;
	this.instance_3.setTransform(221.1,-163.9,0.013,0.471,0,0,0,0,17.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({regX:0.5,scaleX:1.92,x:-75.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.01,x:-68.7},10,cjs.Ease.quartInOut).to({_off:true},1).wait(101));

	// Calque_2
	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("rgba(255,255,255,0.298)").ss(2,1,1).p("EAzrgc1QGcdImccjEAkhgc1MAAAA5rEgnugc1MAAAA5rEg24gc1MAAAA5rApG81MAAAA5rAF581MAAAA5r");
	this.shape_13.setTransform(-118.8,-17.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("rgba(255,255,255,0.298)").ss(2,1,1).p("AZF81QJKdcpKcPEAmrgc1MAAAA5rAia81MAAAA5rAuS81MAAAA5rEgmqgc1MAAAA5r");
	this.shape_14.setTransform(4.5,-17.4);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("rgba(255,255,255,0.298)").ss(2,1,1).p("AEs81QqxdJKxciArZ81MAAAA5rAPe81MAAAA5rAZ881MAAAA5rA5781MAAAA5r");
	this.shape_15.setTransform(146,-17.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("rgba(255,255,255,0.298)").ss(2,1,1).p("EA5SgP1MhyjAAAEA5SgDhMhyjAAAEA5SAP2MhyjAAA");
	this.shape_16.setTransform(-293.6,35.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_13}]},21).to({state:[{t:this.shape_14}]},3).to({state:[{t:this.shape_15}]},3).to({state:[{t:this.shape_16}]},3).to({state:[]},4).wait(87));

	// Layer_7
	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#FFFFFF").ss(2,1,1).p("Agl/gQACAAsbfgQsafgACABUAACAABAysgABUAABAAAgZ+g/Bg");
	this.shape_17.setTransform(-34.2,16);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#FFFFFF").ss(2,1,1).p("AYOK8Qhmg2hYhZQjnjmAAlHQAAlFDnjnQBYhYBmg3QCSFAAAF7QAAF8iSFAQh7EPjkDkQnxHyq+AAQq+AAnwnyQnxnwAAq/QAAq+HxnwQHwnyK+AAQK+AAHxHyQDkDkB7EP");
	this.shape_18.setTransform(-82.2,-18.5);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#FFFFFF").ss(2,1,1).p("AsSAeQgBgPAAgPQAAlFDnjnQDnjnFFAAQFGAADnDnQDnDnAAFFQAAFHjnDmQjnDnlGAAQgoAAgogEQkTgZjJjKQjcjcgKkzQDpBOC8C7QDWDXBHES");
	this.shape_19.setTransform(-350.5,-68.4);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#FFFFFF").ss(2,1,1).p("EAqNgCKQAWBVAABeQAAEdjKDKQjKDKkeAAQkeAAjKjKQjJjKAAkdQAAkdDJjKQDKjKEeAAQB3AABoAjQCTAxB2B2QCHCHAtCtQitgQh/h/QiLiLgGjBAxw5mQABAAsbZmQsZZmABABUAADAABAyrgABUAACAAAgZ+gzNg");
	this.shape_20.setTransform(-425.5,45.3);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#FFFFFF").ss(2,1,1).p("A1sA1QgBgaAAgbQAAo/GXmXQGXmYI/AAQI/AAGYGYQGXGXAAI/QAAJAmXGXQmYGYo/AAQhIAAhGgGQnlgtljllQmEmEgSoeQGbCJFLFLQF7F7B9Hl");
	this.shape_21.setTransform(-168.5,35.5);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#FFFFFF").ss(2,1,1).p("AyYAMQAAAASYD/QSZD+AAgBQABAAgBwPUAAAgABgkxAIUg");
	this.shape_22.setTransform(-584.1,16);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_17,p:{x:-34.2}}]},17).to({state:[{t:this.shape_18}]},4).to({state:[{t:this.shape_19}]},3).to({state:[{t:this.shape_20}]},6).to({state:[]},6).to({state:[{t:this.shape_21}]},57).to({state:[{t:this.shape_17,p:{x:-307.2}}]},4).to({state:[{t:this.shape_22}]},3).to({state:[]},6).to({state:[]},1).wait(14));

	// Layer_8
	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FF0033").s().p("ADuMzQABABgB24QDQAGAHgGQABABAAD4IgBD4QhQBOACAAIBiBOQABAAgvA0IguA0IBcBIQAAABAACPIAACQQiRGHABABgAnEKFQgBgBABj4IAAj4QBPhOAAAAIhihOQgBAAAug0IAug0IhbhIQgBgBAAiPIABiQQCQmHgBgBIBcAuQgBgBABW4QjRgGgHAGg");
	this.shape_23.setTransform(-95.6,91.3);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FF0033").s().p("AoIOnIAAAAIAAAAIAAAAgAm0JPIBUlYIhrhGQACABEng6IjhiWQABABBegxIBhgyQABABgWg7IgUg6QABAABnlZIBolZIBkFLQBjFLABAAICBBZQABAAhdAsIhbArIDxB1QACABiKBiIiJBhIDMAgQABAAByFLIB0FKIiBAMIjOiiQABAAgviwIguixIAAAAIAAAAQkGADACABQABAAgsC+IgrC9QABAAiGBJIiHBIQACgDBSlVg");
	this.shape_24.setTransform(-44.9,62.8);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FF0033").s().p("AizQ9UAABAABgABgjAQE9AJAMgJQABABgBF7IAAF8Qh6B3ACABICWB4QABABhHBPIhGBPICMBuQABABgBDbIAADcQjdJXACABg");
	this.shape_25.setTransform(61.9,122.4);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FF0033").s().p("AE2WdQu/ABAAgBIABovQAAowgBAAIiWhuIBLhVQBMhVgBAAIiqgeQgBgBCXhsQAC0ngCgBQPBgUDGAAQB8AAAuBfQAOAdAEAkIACAeIABJEQAAJEgBAAIgUAyQgTAxgBAAIC0BuIhzAtQhyAtgBAAIBuCWQACHMgCFTQgBDUjCA3Qg/AShAAAQgiAAgigFgAE8SFQBUgjAAh9MAABggMQgCgbgJgVQgfhGhjAAIqJAKQABAAAASSIgBSQQAAABJrgBQAsAIAqgSg");
	this.shape_26.setTransform(-37.1,57.2);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FF0033").s().p("ADmOnIAAAAIAAAAIAAAAgAE7JPIBUlYIhrhGQABABEng6IjhiWQABABBfgxIBggyQABABgVg7IgVg6QABAABnlZIBolZIBlFLQBkFLABAAICABZQABAAhcAsIhcArIDyB1QABABiKBiIiIBhIDMAgQABAAByFLIB0FKIiCAMIjOiiQACAAgwiwIguixIAAAAIAAAAQkGADABABQABAAgsC+IgqC9QABAAiHBJIiHBIQACgDBTlVgAp8NVIocAAIAAk7IAAk7IhVg+IArgwIAqgwIhggRQgBAABWg9IAArnQIdgLBwAAQBGAAAZA1QAIARADAUIABARIAAFGIAAFHIgMAbIgLAcIBmA+IhBAZIhBAaIA+BUIAAHCQgBB4htAfQgkAKgkAAQgTAAgTgDgAp5K3QAwgUAAhGIAAyHQAAgQgGgLQgRgog4AAIluAGIABKRIgBKTQAAABFdgBQAZAEAXgKgAlcBuIAAAAg");
	this.shape_27.setTransform(-228,52.8);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FF0033").s().p("ANfO4IpsAAIABlqIgBlqIhhhHIAwg3IAxg3IhugTQgBgBBihFIAAtWQJtgNCBAAQBRAAAdA+QAJATADAWIABAUIAAF3IAAF3IgNAgQgMAggBAAIB1BHQgBgBhKAeIhKAdIBIBhIAAIFQgCCJh9AkQgpAMgqAAQgWAAgWgEgANjMDQA3gXAAhRIAA0zQAAgSgHgNQgUgug/AAImlAHIAALzIAAL0QAAABGRgBQAcAFAbgLgAypOSIAAAAIAAAAIAAAAgAxVI6IBVlYIhrhGQABABEng6IjhiWQABABBegxIBhgyQABABgVg7IgVg6QABAABnlZIBolZIBlFLQBkFLABAAICABZQABAAhdAsIhbArIDyB1QABABiKBiIiJBhIDMAgQABAABzFLIB0FKIiCAMIjOiiQABAAgviwIguixIAAAAIAAAAQkHADACABQABAAgsC+IgqC9QAAAAiGBJIiHBIQACgCBSlWg");
	this.shape_28.setTransform(-462.6,54.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_23}]},8).to({state:[{t:this.shape_24}]},2).to({state:[{t:this.shape_25}]},3).to({state:[{t:this.shape_26}]},4).to({state:[]},2).to({state:[{t:this.shape_27}]},78).to({state:[{t:this.shape_28}]},3).to({state:[]},1).wait(20));

	// masque Titre
	this.instance_4 = new lib.masque_generique();
	this.instance_4.parent = this;
	this.instance_4.setTransform(-228.9,-13.9,0.013,0.471,0,0,0,0,17.2);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(4).to({_off:false},0).wait(1).to({regX:78.5,regY:17,scaleX:0.03,x:-229,y:-14},0).wait(1).to({scaleX:0.09,x:-233.7},0).wait(1).to({scaleX:0.24,x:-245.4},0).wait(1).to({scaleX:0.67,x:-279.1},0).wait(1).to({scaleX:1.48,x:-342.4},0).wait(1).to({scaleX:1.76,x:-363.9},0).wait(1).to({scaleX:1.87,x:-372.9},0).wait(1).to({regX:-0.1,regY:17.1,scaleX:1.92,x:-527.6,y:-13.9},0).to({regX:0.5,regY:17.2,scaleX:1.92,x:-527.8},1).to({regX:0,scaleX:0.01,x:-528.7},10,cjs.Ease.quartInOut).to({_off:true},1).wait(97));

	// Layer_10
	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#01FBFC").s().p("AgJA0QgEgEAAgGQAAgFAEgFQAEgEAFAAQAGAAAEAEQAEAFAAAFQAAAGgEAEQgEAFgGAAQgFAAgEgFgAgJAVIgEg5IAAgUIAbAAIAAAUIgFA5g");
	this.shape_29.setTransform(-357.9,-14.1);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#01FBFC").s().p("AAbA4Ig1hFIAABFIgZAAIAAhvIAYAAIA2BHIAAhHIAZAAIAABvg");
	this.shape_30.setTransform(-378.9,-14.1);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#01FBFC").s().p("AgoA4IAAhvIBPAAIAAAXIg2AAIAAAWIAxAAIAAAUIgxAAIAAAYIA4AAIAAAWg");
	this.shape_31.setTransform(-402.4,-14.1);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#01FBFC").s().p("AgMA4IAAhvIAZAAIAABvg");
	this.shape_32.setTransform(-422.5,-14.1);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#01FBFC").s().p("AgCA5IADgYIgNAAIgEAYIgZAAIAFgYIgQAAIAFgWIAPAAIADgTIgRAAIAEgWIARAAIAEgaIAXAAIgDAaIANAAIAEgaIAZAAIgFAaIARAAIgEAWIgRAAIgDATIASAAIgEAWIgSAAIgEAYgAgJALIAOAAIAEgTIgOAAg");
	this.shape_33.setTransform(-459.4,-14.3);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#01FBFC").s().p("AgjA4IAAhvIAZAAIAABYIAuAAIAAAXg");
	this.shape_34.setTransform(-497.6,-14.1);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#01FBFC").s().p("AgjApQgRgQAAgYQAAgZARgQQASgRAXAAQAcAAASAVIgPASQgMgPgSAAQgNAAgKAJQgJAJgBAPQAAAPAKAKQAJAKANAAQASAAALgPIAQARQgTAVgZAAQgZAAgRgRg");
	this.shape_35.setTransform(-520.4,-14.2);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FF0033").s().p("AgJA0QgEgEAAgGQAAgFAEgFQAEgEAFAAQAGAAAEAEQAEAFAAAFQAAAGgEAEQgEAFgGAAQgFAAgEgFgAgJAVIgEg5IAAgUIAbAAIAAAUIgFA5g");
	this.shape_36.setTransform(-422.7,-14.1);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FF0033").s().p("AgCA5IADgYIgNAAIgEAYIgZAAIAFgYIgQAAIAFgWIAPAAIADgTIgRAAIAEgWIARAAIAEgaIAXAAIgDAaIANAAIAEgaIAZAAIgFAaIARAAIgEAWIgRAAIgDATIASAAIgEAWIgSAAIgEAYgAgJALIAOAAIAEgTIgOAAg");
	this.shape_37.setTransform(-459.4,-14.3);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FF0033").s().p("AgjA4IAAhvIAZAAIAABYIAuAAIAAAXg");
	this.shape_38.setTransform(-497.6,-14.1);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FF0033").s().p("AgjApQgRgQAAgYQAAgZARgQQASgRAXAAQAcAAASAVIgPASQgMgPgSAAQgNAAgKAJQgJAJgBAPQAAAPAKAKQAJAKANAAQASAAALgPIAQARQgTAVgZAAQgZAAgRgRg");
	this.shape_39.setTransform(-520.4,-14.2);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FF0033").s().p("AgMA4IAAhZIgfAAIAAgWIBXAAIAAAWIggAAIAABZg");
	this.shape_40.setTransform(-411.9,-14.1);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FF0033").s().p("AAbA4Ig0hFIAABFIgaAAIAAhvIAYAAIA2BHIAAhHIAZAAIAABvg");
	this.shape_41.setTransform(-435.3,-14.1);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FF0033").s().p("AgoA4IAAhvIBPAAIAAAXIg2AAIAAAWIAxAAIAAAUIgxAAIAAAYIA4AAIAAAWg");
	this.shape_42.setTransform(-458.8,-14.1);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FF0033").s().p("AgMA4IAAhvIAZAAIAABvg");
	this.shape_43.setTransform(-478.9,-14.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29}]},13).to({state:[{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36}]},5).to({state:[{t:this.shape_39},{t:this.shape_38},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40}]},5).to({state:[]},11).to({state:[]},46).wait(41));

	// Layer_11
	this.instance_5 = new lib.masqueTexte("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(-340,105.8,0.85,0.85,0,0,0,0.1,108.8);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(37).to({_off:false},0).to({_off:true},40).wait(3).to({_off:false,rotation:180},0).to({_off:true},40).wait(1));

	// Layer_12
	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFFFFF").s().p("AApBtIAAh4QAAgyglAAQgRAAgNANQgNANAAAZIAAB3Ig9AAIAAjWIA9AAIAAAYQAagbAiAAQAhAAAXAYQAYAYAAAnIAACCg");
	this.shape_44.setTransform(-316,133.3);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFFFFF").s().p("AhJBbQgXgUAAghQAAghAYgPQAYgQApAAIAsAAIAAgCQAAgigkAAQgPAAgRAGQgSAGgMAKIgbgnQArgfA2AAQAnAAAZAUQAYATAAArIAACIIg4AAIAAgaQgXAdgiAAQghAAgYgUgAgkAmQAAALAJAHQAIAGAQAAQAQAAAMgKQAMgKAAgQIAAgLIglAAQgkAAAAAXg");
	this.shape_45.setTransform(-341.2,133.4);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FFFFFF").s().p("Ag9BtIAAjWIA8AAIAAAdQAKgOASgJQARgJASAAIAAA4IgLAAQgbAAgNATQgMATAAAeIAABdg");
	this.shape_46.setTransform(-360,133.3);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFFFFF").s().p("AhEBQQgigeAAgyQAAgwAigfQAigfAtAAQAXAAAYAKQAYAJASATIgfApQgLgNgPgGQgOgHgPAAQgXAAgRAPQgRAQAAAaQAAAbARAQQARAPAWAAQAeAAAYgeIAkAoQgqArgwAAQgwAAghgfg");
	this.shape_47.setTransform(-380.5,133.4);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FFFFFF").s().p("AhLCBQgggeAAgzQAAgyAggeQAhgeAsAAQAsAAAfAbQAfAbAAAuIAAAfIicAAQADATAPAKQAQAMATAAQAfAAAVgWIAhAlQgkAigxAAQgwAAgggegAgdgCQgPALgDASIBeAAQgCgUgNgJQgMgKgRAAQgSAAgOAKgAglhXIAzhHIA+AaIg4Atg");
	this.shape_48.setTransform(-404.6,128.6);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FFFFFF").s().p("AgdCUIAAkoIA7AAIAAEog");
	this.shape_49.setTransform(-422.9,129.4);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FFFFFF").s().p("AhLCBQgggeAAgzQAAgyAggeQAhgeAsAAQAsAAAfAbQAfAbAAAuIAAAfIicAAQADATAPAKQAQAMATAAQAfAAAVgWIAiAlQglAigxAAQgwAAgggegAgdgCQgPALgDASIBeAAQgCgUgNgJQgMgKgRAAQgSAAgOAKgAglhXIAyhHIA/AaIg4Atg");
	this.shape_50.setTransform(-441,128.6);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FFFFFF").s().p("AgfB5QgWgUAAgkIAAhiIgZAAIAAgrIAZAAIAAhAIA7AAIAABAIA0AAIAAArIg0AAIAABgQABAMAGAHQAGAHAJAAQARAAAMgPIAWAqQgcAZgfAAQgeAAgVgUg");
	this.shape_51.setTransform(-461.7,130.4);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FFFFFF").s().p("AgfB5QgWgUAAgkIAAhiIgZAAIAAgrIAZAAIAAhAIA7AAIAABAIA1AAIAAArIg1AAIAABgQABAMAGAHQAHAHAIAAQARAAAMgPIAWAqQgcAZgfAAQgeAAgVgUg");
	this.shape_52.setTransform(-489.8,130.4);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAhgeQAggeArAAQAtAAAfAbQAfAbAAAuIAAAfIicAAQADASAQALQAPALATAAQAfAAAVgVIAhAlQgkAigxAAQgwAAgggegAgdgyQgPALgDATIBeAAQgCgUgMgKQgNgKgRAAQgRAAgPAKg");
	this.shape_53.setTransform(-511.4,133.4);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FFFFFF").s().p("AgqBmQgagIgVgRIAagmQAjAaAkAAQAMAAAGgEQAIgFAAgHQAAgIgKgGQgJgHgRgFIgbgKQgKgEgMgIQgYgOAAgeQAAgdAYgTQAYgTAlAAQAmAAAmAaIgXAoQgcgUgcAAQgaAAAAAQQAAAIAJAFQAIAEAUAHIAdAKQAJADAMAHQAWANAAAgQAAAggXATQgYATgkAAQgXAAgagJg");
	this.shape_54.setTransform(-259.9,84.7);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FFFFFF").s().p("AAoBtIAAh4QABgyglAAQgRAAgNANQgOANAAAZIAAB3Ig7AAIAAjWIA7AAIAAAYQAcgbAgAAQAjAAAXAYQAWAYAAAnIAACCg");
	this.shape_55.setTransform(-282.6,84.5);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FFFFFF").s().p("AhQBPQghggAAgvQAAguAhggQAgggAwAAQAxAAAgAgQAhAgAAAuQAAAvghAgQggAggxAAQgwAAgggggAglgqQgPARAAAZQAAAbAPAQQAPARAWAAQAXAAAPgRQAPgQAAgbQAAgZgPgRQgPgRgXAAQgWAAgPARg");
	this.shape_56.setTransform(-308.5,84.7);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FFFFFF").s().p("AggB5QgVgUAAgkIAAhiIgZAAIAAgrIAZAAIAAhAIA7AAIAABAIA0AAIAAArIg0AAIAABgQAAAMAHAHQAHAHAIAAQARAAAMgPIAWAqQgcAZgfAAQgeAAgWgUg");
	this.shape_57.setTransform(-329.7,81.6);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#FFFFFF").s().p("AhJBbQgXgUAAghQAAghAYgPQAYgQAqAAIArAAIAAgCQAAgigkAAQgPAAgRAGQgRAGgMAKIgbgnQAqgfA1AAQAnAAAaAUQAYATAAArIAACIIg4AAIAAgaQgYAdggAAQgiAAgYgUgAgkAmQAAALAIAHQAKAGAPAAQAPAAAMgKQANgKAAgQIAAgLIglAAQgkAAAAAXg");
	this.shape_58.setTransform(-351.4,84.7);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#FFFFFF").s().p("ABtBtIAAh0QAAgygkAAQgSAAgNANQgOANAAAZIAABzIg7AAIAAh0QAAgagIgMQgIgMgRAAQgSAAgNANQgNANAAAZIAABzIg8AAIAAjWIA8AAIAAAYQAZgbAfAAQAVAAAQALQAQANAIARQANgUAVgLQAVgKAVAAQAmAAAXAWQAXAXAAAqIAACCg");
	this.shape_59.setTransform(-382.4,84.5);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#FFFFFF").s().p("AhQBPQghggAAgvQAAguAhggQAgggAwAAQAxAAAgAgQAhAgAAAuQAAAvghAgQggAggxAAQgwAAgggggAglgqQgPARAAAZQAAAbAPAQQAPARAWAAQAXAAAPgRQAPgQAAgbQAAgZgPgRQgPgRgXAAQgWAAgPARg");
	this.shape_60.setTransform(-415.1,84.7);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#FFFFFF").s().p("AgfB5QgWgUAAgkIAAhiIgZAAIAAgrIAZAAIAAhAIA8AAIAABAIA0AAIAAArIg0AAIAABgQAAAMAGAHQAHAHAIAAQARAAALgPIAXAqQgcAZgfAAQgeAAgVgUg");
	this.shape_61.setTransform(-436.3,81.6);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#FFFFFF").s().p("AhQBPQghggAAgvQAAguAhggQAgggAwAAQAxAAAgAgQAhAgAAAuQAAAvghAgQggAggxAAQgwAAgggggAglgqQgPARAAAZQAAAbAPAQQAPARAWAAQAXAAAPgRQAPgQAAgbQAAgZgPgRQgPgRgXAAQgWAAgPARg");
	this.shape_62.setTransform(-458.7,84.7);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#FFFFFF").s().p("AApCVIAAh2QAAgxglABQgRgBgNANQgNAMAAAaIAAB0Ig9AAIAAkoIA9AAIAABpQAagbAiAAQAhAAAXAYQAYAYAAAmIAACEg");
	this.shape_63.setTransform(-484.3,80.6);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#FFFFFF").s().p("AhuCUIAAkjIA8AAIAAAXQAcgbAkAAQAmAAAdAfQAeAgAAAvQAAAvgeAgQgdAfgnABQgngBgYgfIAABqgAgkhPQgPARAAAaQAAAbAPAPQAPAQAUAAQAUAAAQgQQAPgPAAgaQAAgbgPgRQgPgSgUAAQgVAAgPASg");
	this.shape_64.setTransform(-509.9,88.4);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#FFFFFF").s().p("AglA1IAQgsQgPgJAAgQQAAgQALgKQALgKAOAAQAQAAAKAKQAMAJAAAOQAAANgMASIgYApg");
	this.shape_65.setTransform(-142.3,45.3);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAggeQAhgeAsAAQAsAAAfAbQAfAbAAAuIAAAfIicAAQADASAPALQAQALATAAQAfAAAVgVIAiAlQglAigxAAQgwAAgggegAgdgyQgPALgDATIBeAAQgCgUgNgKQgMgKgSAAQgQAAgPAKg");
	this.shape_66.setTransform(-160.4,35.9);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#FFFFFF").s().p("AgdCVIAAkoIA7AAIAAEog");
	this.shape_67.setTransform(-178.7,31.9);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#FFFFFF").s().p("AhJBbQgXgUAAghQAAghAYgPQAYgQAqAAIArAAIAAgCQAAgigkAAQgPAAgRAGQgSAGgLAKIgbgnQAqgfA2AAQAnAAAZAUQAYATAAArIAACIIg4AAIAAgaQgYAdggAAQgjAAgXgUgAgkAmQAAALAJAHQAIAGAQAAQAQAAALgKQANgKAAgQIAAgLIglAAQgkAAAAAXg");
	this.shape_68.setTransform(-196.9,35.9);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#FFFFFF").s().p("AgfB5QgWgUAAgkIAAhiIgZAAIAAgrIAZAAIAAhAIA7AAIAABAIA1AAIAAArIg1AAIAABgQABAMAGAHQAGAHAJAAQARAAAMgPIAWAqQgcAZgfAAQgeAAgVgUg");
	this.shape_69.setTransform(-216.5,32.9);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#FFFFFF").s().p("AgdCXIAAjWIA7AAIAADWgAgYhaQgKgKAAgPQAAgPAKgKQAKgKAOAAQAPAAAKAKQAKAKAAAPQAAAPgKAKQgKAKgPAAQgOAAgKgKg");
	this.shape_70.setTransform(-231.7,31.6);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#FFFFFF").s().p("Ag1CQQgZgIgSgOIAXguQAdAXAhAAQAgAAAUgRQATgQAAgkQgdAggpAAQgqAAgdgdQgdgcAAgvQAAgwAegfQAegeAmAAQAlAAAaAgIAAgdIA8AAIAAC7QAAAdgKAWQgKAWgRANQghAbgtAAQgYAAgZgIgAgihVQgQAPAAAZQAAAaAPAQQAPAOAVAAQAWAAAOgOQAPgQAAgZQAAgagPgPQgOgPgVAAQgVAAgPAPg");
	this.shape_71.setTransform(-251.5,40);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#FFFFFF").s().p("AgdCXIAAjWIA7AAIAADWgAgYhaQgKgKAAgPQAAgPAKgKQAKgKAOAAQAPAAAKAKQAKAKAAAPQAAAPgKAKQgKAKgPAAQgOAAgKgKg");
	this.shape_72.setTransform(-270.1,31.6);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#FFFFFF").s().p("AhQB2QgeghAAgwQAAguAdggQAegeAnAAQAmABAZAbIAAhqIA8AAIAAEoIg8AAIAAgdQgaAgglAAQgmAAgegggAgigDQgPARAAAaQAAAaAPAQQAQARATAAQAVAAAPgRQAOgQAAgbQAAgZgOgRQgPgRgVAAQgUAAgPARg");
	this.shape_73.setTransform(-289.8,32);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#FFFFFF").s().p("AAoBtIAAh4QAAgygkAAQgRAAgNANQgOANAAAZIAAB3Ig8AAIAAjWIA8AAIAAAYQAbgbAiAAQAiAAAWAYQAYAYAAAnIAACCg");
	this.shape_74.setTransform(-325.7,35.8);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#FFFFFF").s().p("AhQBPQghggAAgvQAAguAhggQAgggAwAAQAxAAAgAgQAhAgAAAuQAAAvghAgQggAggxAAQgwAAgggggAglgqQgPARAAAZQAAAbAPAQQAPARAWAAQAXAAAPgRQAPgQAAgbQAAgZgPgRQgPgRgXAAQgWAAgPARg");
	this.shape_75.setTransform(-351.6,35.9);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#FFFFFF").s().p("AgdCXIAAjWIA7AAIAADWgAgYhaQgKgKAAgPQAAgPAKgKQAKgKAOAAQAPAAAKAKQAKAKAAAPQAAAPgKAKQgKAKgPAAQgOAAgKgKg");
	this.shape_76.setTransform(-370.4,31.6);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#FFFFFF").s().p("AggB5QgVgUAAgkIAAhiIgZAAIAAgrIAZAAIAAhAIA8AAIAABAIAzAAIAAArIgzAAIAABgQgBAMAHAHQAGAHAJAAQARAAALgPIAXAqQgcAZgfAAQgeAAgWgUg");
	this.shape_77.setTransform(-384.4,32.9);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("#FFFFFF").s().p("AhJBbQgXgUAAghQAAghAYgPQAYgQAqAAIArAAIAAgCQAAgigkAAQgPAAgRAGQgSAGgLAKIgbgnQAqgfA2AAQAnAAAZAUQAYATAAArIAACIIg4AAIAAgaQgYAdggAAQgjAAgXgUgAgkAmQAAALAJAHQAIAGAQAAQAQAAALgKQANgKAAgQIAAgLIglAAQgkAAAAAXg");
	this.shape_78.setTransform(-406.1,35.9);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("#FFFFFF").s().p("ABtBtIAAh0QAAgygkAAQgSAAgNANQgOANAAAZIAABzIg7AAIAAh0QAAgagIgMQgIgMgRAAQgSAAgNANQgNANAAAZIAABzIg8AAIAAjWIA8AAIAAAYQAZgbAfAAQAVAAAQALQAQANAIARQANgUAVgLQAVgKAVAAQAmAAAXAWQAXAXAAAqIAACCg");
	this.shape_79.setTransform(-437.1,35.8);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f("#FFFFFF").s().p("AgdCXIAAjWIA7AAIAADWgAgYhaQgKgKAAgPQAAgPAKgKQAKgKAOAAQAPAAAKAKQAKAKAAAPQAAAPgKAKQgKAKgPAAQgOAAgKgKg");
	this.shape_80.setTransform(-462.7,31.6);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("#FFFFFF").s().p("AApBtIAAh4QAAgyglAAQgRAAgNANQgNANAAAZIAAB3Ig9AAIAAjWIA9AAIAAAYQAagbAiAAQAhAAAXAYQAYAYAAAnIAACCg");
	this.shape_81.setTransform(-481.1,35.8);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f("#FFFFFF").s().p("ABVCMIgbg9Ih0AAIgaA9IhCAAIB5kXIA8AAIB4EXgAgiAZIBFAAIgjhRg");
	this.shape_82.setTransform(-508.9,32.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_82},{t:this.shape_81},{t:this.shape_80},{t:this.shape_79},{t:this.shape_78},{t:this.shape_77},{t:this.shape_76},{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44}]},57).to({state:[]},43).to({state:[]},1).wait(20));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(221.1,-172,2,16);


// stage content:
(lib.FS_projet_radar_mobile = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{video:179,"close":515});

	// timeline functions:
	this.frame_0 = function() {
		//variable necessaire pour la fermeture des textes
		this.hiddentexts = false;
		
		//variable necessaire pour la fermeture de la vidéo
		this.hiddenvideo = true;
		
		//variable necessaire pour la fermeture du visuel de fond
		this.hiddenvisuel = false;
		
		//variable necessaire pour la fermeture du bouton play
		this.hiddenplay = true;
	}
	this.frame_139 = function() {
		//var frequency = 60;
		//stage.enableMouseOver(frequency);
		
		// EVENT DE CLIC POUR LA SUITE
		this.next_btn.addEventListener("click", fl_ClickToGoToAndPlayFromFrame.bind(this));
		this.next_btn.addEventListener("mouseover", fl_MouseOverHandler.bind(this));
		this.next_btn.addEventListener("mouseout", fl_MouseOutHandler.bind(this));
		
		
		function fl_ClickToGoToAndPlayFromFrame()
		{
			//this.gotoAndPlay("close");
			
			var event = new Event('next');
			this.dispatchEvent(event);		
			event = null;
			
			this.next_btn.btnNext.gotoAndPlay('rollOut');
			this.lignes_mc.gotoAndPlay("close");
			this.next_btn.gotoAndPlay("close");
			this.cta_mc.gotoAndPlay("close");
			
		}
		function fl_MouseOverHandler(){	this.next_btn.btnNext.gotoAndPlay('rollOver'); }
		function fl_MouseOutHandler(){	this.next_btn.btnNext.gotoAndPlay('rollOut'); }
		
		
		// EVENT DE CLIC POUR LE LIEN
		this.cta_mc.addEventListener("click", projectDetails.bind(this));
		this.cta_mc.addEventListener("mouseover", pDetails_MouseOverHandler.bind(this));
		this.cta_mc.addEventListener("mouseout", pDetails_MouseOutHandler.bind(this));
		
		function projectDetails()
		{	
			var event = new Event('details');
			this.dispatchEvent(event);		
			event = null;
			
			this.cta_mc.btnCta.gotoAndPlay('rollOut');
			
		}
		function pDetails_MouseOverHandler(){	this.cta_mc.btnCta.gotoAndPlay('rollOver'); }
		function pDetails_MouseOutHandler(){	this.cta_mc.btnCta.gotoAndPlay('rollOut'); }
		
		
		// EVENT DE CLIC POUR LA VIDEO
		this.video_mc.btn_playVideo.addEventListener("click", playVideo.bind(this));
		
		function playVideo()
		{	
			var event = new Event('video');
			this.dispatchEvent(event);		
			event = null;
			
			this.hiddenplay = true;	
			this.video_mc.btn_playVideo.gotoAndPlay('close');
		}
	}
	this.frame_179 = function() {
		this.visuel_mc.gotoAndPlay("close");
		this.video_mc.gotoAndPlay("open");
		
		this.hiddenvideo = false;
		this.hiddenvisuel = true;
		this.hiddenplay = false;
		
		//On envoie l'information de l'affichage de la video
		var event = new Event('videoStarter');
		this.dispatchEvent(event);		
		event = null;
	}
	this.frame_339 = function() {
		this.textes_mc.gotoAndPlay("close");
		this.hiddentexts = true;
	}
	this.frame_514 = function() {
		//PAUSE
		this.stop();
	}
	this.frame_516 = function() {
		this.cta_mc.gotoAndPlay("close");
		this.next_btn.gotoAndPlay("close");
		
		
		if(this.hiddenvisuel==false){
			this.visuel_mc.gotoAndPlay("close");
		}
		
		if(this.hiddenvideo==false){
			this.video_mc.gotoAndPlay("close");
		}
		
		if(this.hiddentexts==false){
			this.textes_mc.gotoAndPlay("close");
			this.hiddentexts = true;
		}
		
		if(this.hiddenplay==false){
			this.video_mc.btn_playVideo.gotoAndPlay('close');
		}
	}
	this.frame_535 = function() {
		//this.lignes_mc.gotoAndPlay("close");
	}
	this.frame_562 = function() {
		// EVENT DE FIN D'ANIM
		var event = new Event('closed');
		this.dispatchEvent(event);		
		event = null;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(139).call(this.frame_139).wait(40).call(this.frame_179).wait(160).call(this.frame_339).wait(175).call(this.frame_514).wait(2).call(this.frame_516).wait(19).call(this.frame_535).wait(27).call(this.frame_562).wait(1));

	// CTA NEXT
	this.next_btn = new lib.bloc_NEXT_btn();
	this.next_btn.name = "next_btn";
	this.next_btn.parent = this;
	this.next_btn.setTransform(359.2,861.2,1.06,1.06,0,0,0,-340.4,403.9);
	this.next_btn._off = true;

	this.timeline.addTween(cjs.Tween.get(this.next_btn).wait(128).to({_off:false},0).wait(435));

	// CTA DETAIL
	this.cta_mc = new lib.bloc_CTA();
	this.cta_mc.name = "cta_mc";
	this.cta_mc.parent = this;
	this.cta_mc.setTransform(120.2,861.4,1.06,1.06,0,0,0,-340.4,404.1);
	this.cta_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.cta_mc).wait(119).to({_off:false},0).to({_off:true},420).wait(24));

	// BLOC INFOS TEXTE
	this.textes_mc = new lib.blocInfostextes();
	this.textes_mc.name = "textes_mc";
	this.textes_mc.parent = this;
	this.textes_mc.setTransform(788.2,242,1,1,0,0,0,222.1,-164);
	this.textes_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.textes_mc).wait(17).to({_off:false},0).wait(546));

	// VIDEO
	this.video_mc = new lib.bloc_video();
	this.video_mc.name = "video_mc";
	this.video_mc.parent = this;
	this.video_mc.setTransform(238.9,1185.2,1.05,1.05,0,0,0,227.2,765);

	this.timeline.addTween(cjs.Tween.get(this.video_mc).to({_off:true},539).wait(24));

	// VISUEL
	this.visuel_mc = new lib.bloc_visuel();
	this.visuel_mc.name = "visuel_mc";
	this.visuel_mc.parent = this;
	this.visuel_mc.setTransform(1030.6,381.5,1,1,0,0,0,552.5,26.5);
	this.visuel_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.visuel_mc).wait(17).to({_off:false},0).wait(546));

	// lignes
	this.lignes_mc = new lib.bloc_lignes();
	this.lignes_mc.name = "lignes_mc";
	this.lignes_mc.parent = this;
	this.lignes_mc.setTransform(240.8,453.9,0.525,1,0,0,0,0.7,405.9);

	this.timeline.addTween(cjs.Tween.get(this.lignes_mc).wait(563));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(241.3,293.5,478,1643.4);
// library properties:
lib.properties = {
	id: '20DA403F96B39849809587821B5BE6B7',
	width: 480,
	height: 840,
	fps: 60,
	color: "#000000",
	opacity: 1.00,
	manifest: [
		{src:"images/radar.jpg?1540567877061", id:"radar"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['20DA403F96B39849809587821B5BE6B7'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;