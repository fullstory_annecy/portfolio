(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.WRC_cloud = function() {
	this.initialize(img.WRC_cloud);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,612,352);


(lib.WRC_route = function() {
	this.initialize(img.WRC_route);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1024,500);


(lib.WRC_voiture = function() {
	this.initialize(img.WRC_voiture);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,292,254);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.WRC_voiture_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.WRC_voiture();
	this.instance.parent = this;
	this.instance.setTransform(-146,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.WRC_voiture_1, new cjs.Rectangle(-146,0,292,254), null);


(lib.WRC_cloud_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.WRC_cloud();
	this.instance.parent = this;
	this.instance.setTransform(-306,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.WRC_cloud_1, new cjs.Rectangle(-306,0,612,352), null);


(lib.video = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#080808").s().p("EgtTBLAMAAAiV/MBanAAAMAAACV/g");
	this.shape.setTransform(290,480);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.video, new cjs.Rectangle(0,0,580,960), null);


(lib.txt_hashtag = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgiAmIAJgRQAOAKALgBQAFAAAFgDQAEgDAAgGQAAgGgFgEQgFgDgIAAQgEAAgIADIAAgQIARgVIgeAAIAAgTIA7AAIAAAPIgTAXQALABAHAJQAGAGAAALQAAAPgLAJQgKAJgPAAQgQAAgRgMg");
	this.shape.setTransform(94.7,73.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AghAKIAAgTIBDggIAAAVIgsAUIAsAVIAAAVg");
	this.shape_1.setTransform(86.3,73.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgmAxIAAhhIAmAAQAJAAAHACQAIADADAEQAGAIABAJQAAAMgIAFIgDACIgEACQAJABAGAGQAFAHAAAJQAAAKgHAIQgIAJgUAAgAgQAfIAPAAQAIgBAFgCQAEgCAAgHQAAgGgEgDQgGgCgJAAIgNAAgAgQgIIAKAAQAIAAAFgCQAEgCAAgHQAAgGgEgCQgEgCgIgBIgLAAg");
	this.shape_2.setTransform(74.1,73.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBFAAIAAATIgvAAIAAAVIArAAIAAASIgrAAIAAAUIAxAAIAAATg");
	this.shape_3.setTransform(64.6,73.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgjAkQgPgPAAgVQAAgUAPgPQAOgPAVAAQAWAAAPAPQAOAPAAAUQAAAVgOAPQgPAPgWAAQgVAAgOgPgAgTgWQgJAKAAAMQAAANAJAJQAIAJALAAQAMAAAJgJQAIgJAAgNQAAgMgIgKQgJgIgMgBQgLABgIAIg");
	this.shape_4.setTransform(54,73.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgfAxIAAhhIAWAAIAABOIApAAIAAATg");
	this.shape_5.setTransform(44.7,73.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAXAxIgtg9IAAA9IgWAAIAAhhIAUAAIAvA+IAAg+IAWAAIAABhg");
	this.shape_6.setTransform(30.9,73.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBFAAIAAATIgvAAIAAAVIArAAIAAASIgrAAIAAAUIAxAAIAAATg");
	this.shape_7.setTransform(20.8,73.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgKAxIAAhhIAVAAIAABhg");
	this.shape_8.setTransform(13.7,73.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgKAxIAAhOIgbAAIAAgTIBLAAIAAATIgbAAIAABOg");
	this.shape_9.setTransform(7.2,73.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgmAiIANgQQAQANANAAQAGAAADgDQADgCAAgFQAAgDgDgDQgEgDgKgDQgRgDgIgGQgIgGAAgOQAAgOALgIQAKgIANAAQAKAAAKAEQAKADAHAHIgLAQQgNgLgNAAQgFAAgDADQgDADAAAEQAAAEAEADQAEACANAEQAOADAHAGQAIAHAAANQAAANgKAJQgKAHgQABQgWAAgTgRg");
	this.shape_10.setTransform(-1.7,73.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AAeAxIgKgVIgoAAIgJAVIgXAAIAqhhIAVAAIAqBhgAgLAIIAXAAIgMgbg");
	this.shape_11.setTransform(-11.3,73.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgmAxIAAhhIAmAAQAJAAAHACQAIADADAEQAHAIgBAJQAAAMgGAFIgEACIgDACQAIABAGAGQAFAHAAAJQAAAKgHAIQgIAJgUAAgAgQAfIAPAAQAIgBAFgCQAFgCgBgHQAAgGgEgDQgGgCgJAAIgNAAgAgQgIIAKAAQAJAAAEgCQAEgCAAgHQAAgGgEgCQgEgCgIgBIgLAAg");
	this.shape_12.setTransform(-21,73.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBFAAIAAATIgvAAIAAAVIArAAIAAASIgrAAIAAAUIAxAAIAAATg");
	this.shape_13.setTransform(-30.5,73.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgmAiIANgQQAQANANAAQAGAAADgDQADgCAAgFQAAgDgDgDQgEgDgKgDQgRgDgIgGQgIgGAAgOQAAgOALgIQAKgIANAAQAKAAAKAEQAKADAHAHIgLAQQgNgLgNAAQgFAAgDADQgDADAAAEQAAAEAEADQAEACANAEQAOADAHAGQAIAHAAANQAAANgKAJQgKAHgQABQgWAAgTgRg");
	this.shape_14.setTransform(-39.9,73.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgUIANAAIADgQIgPAAIADgUIAPAAIAEgWIAUAAIgDAWIAMAAIADgWIAWAAIgEAWIAOAAIgDAUIgPAAIgDAQIARAAIgEAUIgQAAIgDAVgAgHAJIALAAIAEgQIgMAAg");
	this.shape_15.setTransform(-49.5,73.7);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBFAAIAAATIgvAAIAAAVIArAAIAAASIgrAAIAAAUIAxAAIAAATg");
	this.shape_16.setTransform(44.6,52.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgIAxIgnhhIAXAAIAYA9IAZg9IAYAAIgoBhg");
	this.shape_17.setTransform(34.9,52.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgKAxIAAhhIAVAAIAABhg");
	this.shape_18.setTransform(27.8,52.8);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgmAiIANgQQAQAOANAAQAGgBADgDQADgCAAgFQAAgEgDgCQgEgDgKgDQgRgDgIgGQgIgGAAgOQAAgPALgHQAKgIANAAQAKAAAKAEQAKADAHAHIgLAQQgNgKgNAAQgFAAgDACQgDADAAAEQAAAEAEADQAEACANAEQAOADAHAGQAIAHAAANQAAANgKAJQgKAHgQABQgWAAgTgRg");
	this.shape_19.setTransform(20.9,52.7);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AAXAxIgtg9IAAA9IgWAAIAAhhIAUAAIAvA+IAAg+IAWAAIAABhg");
	this.shape_20.setTransform(10.8,52.8);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgjAkQgPgPAAgVQAAgUAPgPQAOgPAVAAQAWAAAPAPQAOAPAAAUQAAAVgOAPQgPAPgWAAQgVAAgOgPgAgTgVQgJAJAAAMQAAANAJAKQAIAIALABQAMgBAJgIQAIgKAAgNQAAgMgIgJQgJgJgMAAQgLAAgIAJg");
	this.shape_21.setTransform(-0.7,52.7);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgmAxIAAhhIAiAAQAWAAAKAJQALAIAAATQAAARgLAIQgKAJgVgBIgNAAIAAAcgAgQADIAPAAQAJAAAEgEQAFgEAAgJQgBgIgFgDQgFgEgKAAIgMAAg");
	this.shape_22.setTransform(-10.8,52.8);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgmAiIANgQQAQAOANAAQAGgBADgDQADgCAAgFQAAgEgDgCQgEgDgKgDQgRgDgIgGQgIgGAAgOQAAgPALgHQAKgIANAAQAKAAAKAEQAKADAHAHIgLAQQgNgKgNAAQgFAAgDACQgDADAAAEQAAAEAEADQAEACANAEQAOADAHAGQAIAHAAANQAAANgKAJQgKAHgQABQgWAAgTgRg");
	this.shape_23.setTransform(-20.5,52.7);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBFAAIAAATIgvAAIAAAVIArAAIAAASIgrAAIAAAUIAxAAIAAATg");
	this.shape_24.setTransform(-29.2,52.8);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AAQAxIgVgfIgPAAIAAAfIgWAAIAAhhIAlAAQAWAAAKAIQAKAIAAARQAAAWgTAHIAZAjgAgUAAIAQAAQAKAAAEgEQAEgEAAgHQAAgIgEgDQgEgDgKAAIgQAAg");
	this.shape_25.setTransform(-38.8,52.8);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgTIANAAIADgRIgPAAIADgUIAPAAIAEgWIAUAAIgDAWIAMAAIADgWIAWAAIgEAWIAOAAIgDAUIgPAAIgDARIARAAIgEATIgQAAIgDAVgAgHAKIALAAIAEgRIgMAAg");
	this.shape_26.setTransform(-49.5,52.7);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBFAAIAAATIgvAAIAAAVIArAAIAAASIgrAAIAAATIAxAAIAAAUg");
	this.shape_27.setTransform(7.7,31.7);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AAhAxIAAg9IgbA1IgMAAIgag1IAAA9IgWAAIAAhhIAeAAIAYA1IAZg1IAeAAIAABhg");
	this.shape_28.setTransform(-3.8,31.7);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AAeAxIgKgVIgoAAIgJAVIgXAAIAqhhIAVAAIAqBhgAgLAJIAXAAIgMgcg");
	this.shape_29.setTransform(-15.7,31.7);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AAQAxIgVgfIgPAAIAAAfIgWAAIAAhhIAlAAQAWAAAKAIQAKAHAAASQAAAWgTAHIAZAjgAgUAAIAQAAQAKAAAEgEQAEgEAAgHQAAgIgEgDQgEgDgKAAIgQAAg");
	this.shape_30.setTransform(-25.7,31.7);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AggAxIAAhhIBBAAIAAATIgsAAIAAAWIAqAAIAAASIgqAAIAAAmg");
	this.shape_31.setTransform(-35.1,31.7);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AgKAxIAAhhIAVAAIAABhg");
	this.shape_32.setTransform(-41.9,31.7);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgTIANAAIADgRIgPAAIADgTIAPAAIAEgXIAUAAIgDAXIAMAAIADgXIAWAAIgEAXIAOAAIgDATIgPAAIgDARIARAAIgEATIgQAAIgDAVgAgHAKIALAAIAEgRIgMAAg");
	this.shape_33.setTransform(-49.5,31.6);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AgKAxIAAhhIAVAAIAABhg");
	this.shape_34.setTransform(-22,10.7);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AgmAxIAAhhIAiAAQAWAAALAJQAKAJAAASQAAARgLAIQgLAJgUgBIgNAAIAAAcgAgQADIAOAAQALAAADgDQAEgFAAgJQABgIgGgDQgFgEgKAAIgMAAg");
	this.shape_35.setTransform(-28.7,10.7);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AAeAxIgKgVIgoAAIgJAVIgXAAIAqhhIAVAAIAqBhgAgLAJIAXAAIgMgcg");
	this.shape_36.setTransform(-39,10.7);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgTIANAAIADgRIgPAAIADgTIAPAAIAEgXIAUAAIgDAXIAMAAIADgXIAWAAIgEAXIAOAAIgDATIgPAAIgDARIARAAIgEATIgQAAIgDAVgAgHAKIALAAIAEgRIgMAAg");
	this.shape_37.setTransform(-49.5,10.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.txt_hashtag, new cjs.Rectangle(-56.7,0,157.9,84.2), null);


(lib.playArrow = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(2,1,1).p("AiOiMIAAEZIEdiIg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AiOiMIEdCRIkdCIg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.playArrow, new cjs.Rectangle(-15.3,-15.1,30.7,30.2), null);


(lib.masqueTexte_1_ligne = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("EgicAEJIAAoRMBE5AAAIAAIRg");
	this.shape.setTransform(0,24.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.masqueTexte_1_ligne, new cjs.Rectangle(-220.5,-1.9,441,53), null);


(lib.masque_generique = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#004FA0").s().p("AsQCqIAAlTIYhAAIAAFTg");
	this.shape.setTransform(78.5,17);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.masque_generique, new cjs.Rectangle(0,0,157,34), null);


(lib.line_explode = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_15 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(15).call(this.frame_15).wait(1));

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(1,1,1).p("AgHAAIAPAA");
	this.shape.setTransform(0.8,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#FFFFFF").ss(1,1,1).p("AgRAAIAjAA");
	this.shape_1.setTransform(2.9,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#FFFFFF").ss(1,1,1).p("AgaAAIA2AA");
	this.shape_2.setTransform(5.1,0);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#FFFFFF").ss(1,1,1).p("AglAAIBLAA");
	this.shape_3.setTransform(7.3,0);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#FFFFFF").ss(1,1,1).p("AgnAAIBPAA");
	this.shape_4.setTransform(8.8,0);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#FFFFFF").ss(1,1,1).p("AgpAAIBTAA");
	this.shape_5.setTransform(10.4,0);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#FFFFFF").ss(1,1,1).p("AgrAAIBXAA");
	this.shape_6.setTransform(12,0);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#FFFFFF").ss(1,1,1).p("AgtAAIBbAA");
	this.shape_7.setTransform(13.6,0);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#FFFFFF").ss(1,1,1).p("AgvAAIBfAA");
	this.shape_8.setTransform(15.1,0);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#FFFFFF").ss(1,1,1).p("AgxAAIBjAA");
	this.shape_9.setTransform(16.7,0);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#FFFFFF").ss(1,1,1).p("Ag0AAIBpAA");
	this.shape_10.setTransform(18.3,0);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#FFFFFF").ss(1,1,1).p("AgWAAIAtAA");
	this.shape_11.setTransform(25.8,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape,p:{x:0.8}}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3,p:{x:7.3}}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_3,p:{x:22}}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape,p:{x:29.5}}]},1).to({state:[]},1).to({state:[]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1,-1,13,2);


(lib.ligne = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(49));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#333333").ss(2,1,1).p("EgABhM/MAADCZ/");
	this.shape.setTransform(-0.2,319.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#333333").ss(2,1,1).p("EAAghM7Qg1OzgbRBQgbUYAEVMQADWBAkVVQAlTkBERl");
	this.shape_1.setTransform(-7,319.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#333333").ss(2,1,1).p("EAA7hM5QhfN/gxR5QgxUHAHVhQAGWSA/VCQBDUPB4Qw");
	this.shape_2.setTransform(-12.4,319.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#333333").ss(2,1,1).p("EABQhM3Qh/NXhDSkQhAT5AIVyQAIWfBUU0QBZUwChQG");
	this.shape_3.setTransform(-16.5,320.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#333333").ss(2,1,1).p("EABfhM1QiXM4hPTEQhMTvAKV+QAJWpBjUqQBqVIC+Pn");
	this.shape_4.setTransform(-19.6,320.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#333333").ss(2,1,1).p("EABqhM0QioMjhYTaQhVToALWHQALWwBuUiQB1VaDUPR");
	this.shape_5.setTransform(-21.8,320.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#333333").ss(2,1,1).p("EABxhMzQizMUheTqQhbTjAMWNQALW1B2UdQB9VmDjPB");
	this.shape_6.setTransform(-23.3,320.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#333333").ss(2,1,1).p("EAB2hMzQi7MKhiT1QhfTgAMWRQAMW3B7UaQCDVvDsO3");
	this.shape_7.setTransform(-24.2,320.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#333333").ss(2,1,1).p("EAB5hMyQjAMEhkT7QhhTeAMWTQAMW5B+UYQCGVzDyOx");
	this.shape_8.setTransform(-24.8,320.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#333333").ss(2,1,1).p("EAB7hMyQjCMBhmT+QhjTdANWVQAMW6CAUWQCHV2D2Ou");
	this.shape_9.setTransform(-25.2,320.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#333333").ss(2,1,1).p("EAB8hMyQjEL/hmUAQhjTcAMWWQANW7CAUWQCIV3D3Os");
	this.shape_10.setTransform(-25.3,320.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#333333").ss(2,1,1).p("EAB8hMyQjEL/hnUAQhjTcANWWQAMW7CAUWQCJV3D4Os");
	this.shape_11.setTransform(-25.4,320.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#333333").ss(2,1,1).p("EAB8hMyQjEL/hnUAQhjTcAMWWQANW7CAUWQCJV3D4Os");
	this.shape_12.setTransform(-25.4,320.5);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#333333").ss(2,1,1).p("EAB8hMyQjEL/hnUAQhjTcAMWWQANW7CAUWQCJV4D4Or");
	this.shape_13.setTransform(-25.4,320.5);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#333333").ss(2,1,1).p("EAB8hMyQjEL/hmUAQhkTcANWWQAMW7CBUWQCIV3D4Os");
	this.shape_14.setTransform(-25.4,320.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#333333").ss(2,1,1).p("EAB7hMyQjDMAhmT/QhjTdANWVQAMW6CAUWQCIV3D2Ot");
	this.shape_15.setTransform(-25.2,320.5);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#333333").ss(2,1,1).p("EAB6hMyQjBMChmT9QhiTdAMWVQANW6B/UXQCHV1D1Ou");
	this.shape_16.setTransform(-25.1,320.5);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#333333").ss(2,1,1).p("EAB5hMyQjAMEhkT7QhiTeANWTQAMW5B+UYQCGV0DzOw");
	this.shape_17.setTransform(-24.9,320.5);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#333333").ss(2,1,1).p("EAB4hMzQi+MHhjT5QhgTfAMWRQAMW5B8UZQCFVxDvO0");
	this.shape_18.setTransform(-24.6,320.5);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#333333").ss(2,1,1).p("EAB2hMzQi7MLhhT0QhfTgAMWRQAMW3B6UaQCDVtDrO5");
	this.shape_19.setTransform(-24.2,320.5);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#333333").ss(2,1,1).p("EABzhMzQi2MQhgTuQhcTiALWPQAMW1B4UcQB/VqDnO9");
	this.shape_20.setTransform(-23.7,320.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#333333").ss(2,1,1).p("EABwhMzQiyMWhcToQhbTkAMWMQALWzB1UeQB8VlDgPD");
	this.shape_21.setTransform(-23,320.4);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#333333").ss(2,1,1).p("EABshM0QisMehZTgQhXTmALWJQALWxBwUhQB4VeDZPM");
	this.shape_22.setTransform(-22.2,320.4);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#333333").ss(2,1,1).p("EABohM0QilMnhVTWQhUTpALWFQAKWuBsUlQBzVWDQPV");
	this.shape_23.setTransform(-21.3,320.3);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#333333").ss(2,1,1).p("EABihM1QibMyhSTKQhPTuAKWAQAKWrBnUoQBsVNDFPh");
	this.shape_24.setTransform(-20.2,320.3);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#333333").ss(2,1,1).p("EABchM1QiSM/hMS8QhKTyAKV7QAJWnBgUsQBmVDC4Pt");
	this.shape_25.setTransform(-18.9,320.2);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#333333").ss(2,1,1).p("EABUhM2QiGNNhGStQhET3AJV1QAJWiBYUyQBeU3CpP8");
	this.shape_26.setTransform(-17.4,320.1);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#333333").ss(2,1,1).p("EABMhM3Qh5Neg/ScQg9T8AIVuQAHWdBQU2QBVUqCYQO");
	this.shape_27.setTransform(-15.7,320);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#333333").ss(2,1,1).p("EABChM4QhqNxg3SIQg2UCAHVnQAHWWBGU9QBKUbCGQh");
	this.shape_28.setTransform(-13.8,319.9);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#333333").ss(2,1,1).p("EAA3hM5QhZOGguRxQguUKAGVeQAGWPA7VFQA/UJBxQ3");
	this.shape_29.setTransform(-11.7,319.8);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#333333").ss(2,1,1).p("EAAuhM6QhKOZgnRdQgmUQAFVWQAGWKAxVLQA0T6BfRK");
	this.shape_30.setTransform(-9.8,319.7);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#333333").ss(2,1,1).p("EAAlhM7Qg8OqggRLQgfUWAEVPQAEWFApVQQArTtBORb");
	this.shape_31.setTransform(-8.1,319.7);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#333333").ss(2,1,1).p("EAAehM7QgxO4gaQ8QgZUaADVJQAEWAAhVWQAjTgA/Rq");
	this.shape_32.setTransform(-6.6,319.6);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#333333").ss(2,1,1).p("EAAXhM8QgnPFgUQuQgUUeADVFQADV8AaVaQAcTWAzR3");
	this.shape_33.setTransform(-5.3,319.5);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#333333").ss(2,1,1).p("EAAShM9QgePQgRQjQgPUiACVAQADV4AVVeQAVTNAoSD");
	this.shape_34.setTransform(-4.2,319.5);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#333333").ss(2,1,1).p("EAANhM9QgXPZgMQZQgMUlACU9QACV1AQVhQARTFAeSM");
	this.shape_35.setTransform(-3.2,319.4);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#333333").ss(2,1,1).p("EAAJhM9QgRPggJQRQgJUoACU5QABVyANVkQAMS/AXSU");
	this.shape_36.setTransform(-2.4,319.4);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#333333").ss(2,1,1).p("EAAGhM+QgMPogGQKQgHUpACU3QABVwAJVmQAJS6ARSb");
	this.shape_37.setTransform(-1.8,319.3);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#333333").ss(2,1,1).p("EAADhM+QgIPsgEQFQgEUrABU1QABVuAGVoQAGS2AMSg");
	this.shape_38.setTransform(-1.3,319.3);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#333333").ss(2,1,1).p("EAABhM+QgFPwgCQAQgDUtABUzQABVtAEVqQAESyAISk");
	this.shape_39.setTransform(-0.9,319.3);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#333333").ss(2,1,1).p("EAAAhM+QgCPzgCP9QgBUuAAUxQABVtADVqQACSwAFSn");
	this.shape_40.setTransform(-0.6,319.3);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#333333").ss(2,1,1).p("EAAAhM+QgCP1gBP7QAAUuAAUxQABVsACVrQABSuADSp");
	this.shape_41.setTransform(-0.4,319.3);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#333333").ss(2,1,1).p("EgABhM/QgBP3AAP5QAAUvABUxQAAVsABVrQABStACSr");
	this.shape_42.setTransform(-0.2,319.3);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#333333").ss(2,1,1).p("EgABhM/QAAP4AAP4QAAUwABUvQAAVsAAVrQABStABSs");
	this.shape_43.setTransform(-0.2,319.3);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#333333").ss(2,1,1).p("EgABhM/QABP4AAP4QAAUwAAUvQAAVsABVrQABStAASs");
	this.shape_44.setTransform(-0.2,319.3);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#333333").ss(2,1,1).p("EgABhM/QABP4AAP4QAAUwAAUvQABVsAAVrQABStAASs");
	this.shape_45.setTransform(-0.2,319.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.3,-174.5,2.4,987.5);


(lib.lettres_FAT = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFF000").s().p("ACnFeIlMm2IAAG2IidAAIAAq7ICSAAIFXHBIAAnBICcAAIAAK7g");
	this.shape.setTransform(268.7,187.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFF000").s().p("AhNFeIAAq7ICbAAIAAK7g");
	this.shape_1.setTransform(199.5,187.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFF000").s().p("AjiFeIAAq7ICcAAIAAIvIEpAAIAACMg");
	this.shape_2.setTransform(107.2,187.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFF000").s().p("AkBFeIAAq7IH3AAIAACMIlaAAIAACQIE3AAIAACDIk3AAIAACSIFmAAIAACKg");
	this.shape_3.setTransform(32.8,187.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFF000").s().p("AixBDIAAiFIFjAAIAACFg");
	this.shape_4.setTransform(-38,192.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFF000").s().p("ACLFdIAAkSIkVAAIAAESIicAAIAAq5ICcAAIAAEjIEVAAIAAkjICcAAIAAK5g");
	this.shape_5.setTransform(174.6,63.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFF000").s().p("AjeEEQhohngBibQAAibBrhoQBrhnCYAAQCtAABzCCIhhBvQhJhdhuAAQhYAAg+A6Qg/A5AABhQAABiA8A6QA6A8BUAAQByAABKhbIBkBmQh3CGiiAAQihAAhohlg");
	this.shape_6.setTransform(88.4,63.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFF000").s().p("AhNFdIAAq5ICbAAIAAK5g");
	this.shape_7.setTransform(22.9,63.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFF000").s().p("ADrFdIAAm3Ii9F+IhcAAIi8l+IAAG3IicAAIAAq5IDTAAICzF+IC1l+IDSAAIAAK5g");
	this.shape_8.setTransform(-53.2,63.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.lettres_FAT, new cjs.Rectangle(-102.9,0,426.7,249.8), null);


(lib.hastaggraphicbranche = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AjQAuIAShbIDlAAICqAAIgSBbg");
	this.shape.setTransform(0,4.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.hastaggraphicbranche, new cjs.Rectangle(-20.8,0,41.7,9.2), null);


(lib.Btn_cliquable_area = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#004FA0").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape.setTransform(0,30);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Btn_cliquable_area, new cjs.Rectangle(-113,0,226,60), null);


(lib.project_illustration_placeHolder = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_100 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(100).call(this.frame_100).wait(11));

	// Layer_7 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("EAvbAo0QAJgGtShiQtthljDgDQi9gDj1ggQh9gRhqgTIpWgzQpOgygugGQgtgHg/gDQg3gCgJgCQgUgFlaAUQlyAUjdAWQlDAfpIg1QADgCjPo9QjOo8ADgDQADgCFtvQQFtvPACgDQADgCRNjfQRNjeADgDQADgCbREgQbSEfAEgCQAFgDhdaOQhcaLgGAAIAAAAg");
	mask.setTransform(-152,261.2);

	// WRC_voiture.png
	this.instance = new lib.WRC_voiture_1();
	this.instance.parent = this;
	this.instance.setTransform(64.4,585,0.64,0.64,0,0,0,-0.1,127);
	this.instance._off = true;

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(23).to({_off:false},0).wait(1).to({regX:0,scaleX:0.74,scaleY:0.74,x:21.9,y:524.4},0).wait(1).to({scaleX:0.78,scaleY:0.78,x:3.5,y:498.2},0).wait(1).to({scaleX:0.8,scaleY:0.8,x:-9.2,y:479.9},0).wait(1).to({scaleX:0.83,scaleY:0.83,x:-19.1,y:465.8},0).wait(1).to({scaleX:0.84,scaleY:0.84,x:-27.1,y:454.4},0).wait(1).to({scaleX:0.86,scaleY:0.86,x:-33.9,y:444.7},0).wait(1).to({scaleX:0.87,scaleY:0.87,x:-39.7,y:436.4},0).wait(1).to({scaleX:0.88,scaleY:0.88,x:-44.8,y:429.2},0).wait(1).to({scaleX:0.89,scaleY:0.89,x:-49.3,y:422.7},0).wait(1).to({scaleX:0.9,scaleY:0.9,x:-53.3,y:417.1},0).wait(1).to({scaleX:0.91,scaleY:0.91,x:-56.8,y:412},0).wait(1).to({scaleX:0.92,scaleY:0.92,x:-60.1,y:407.4},0).wait(1).to({scaleX:0.92,scaleY:0.92,x:-63,y:403.2},0).wait(1).to({scaleX:0.93,scaleY:0.93,x:-65.6,y:399.4},0).wait(1).to({scaleX:0.94,scaleY:0.94,x:-68.1,y:396},0).wait(1).to({scaleX:0.94,scaleY:0.94,x:-70.3,y:392.8},0).wait(1).to({scaleX:0.95,scaleY:0.95,x:-72.3,y:389.9},0).wait(1).to({scaleX:0.95,scaleY:0.95,x:-74.2,y:387.3},0).wait(1).to({scaleX:0.95,scaleY:0.95,x:-75.9,y:384.8},0).wait(1).to({scaleX:0.96,scaleY:0.96,x:-77.5,y:382.5},0).wait(1).to({scaleX:0.96,scaleY:0.96,x:-79,y:380.4},0).wait(1).to({scaleX:0.96,scaleY:0.96,x:-80.3,y:378.5},0).wait(1).to({scaleX:0.97,scaleY:0.97,x:-81.6,y:376.7},0).wait(1).to({scaleX:0.97,scaleY:0.97,x:-82.8,y:375},0).wait(1).to({scaleX:0.97,scaleY:0.97,x:-83.9,y:373.5},0).wait(1).to({scaleX:0.97,scaleY:0.97,x:-84.9,y:372},0).wait(1).to({scaleX:0.98,scaleY:0.98,x:-85.8,y:370.7},0).wait(1).to({scaleX:0.98,scaleY:0.98,x:-86.7,y:369.5},0).wait(1).to({scaleX:0.98,scaleY:0.98,x:-87.5,y:368.3},0).wait(1).to({scaleX:0.98,scaleY:0.98,x:-88.2,y:367.3},0).wait(1).to({scaleX:0.98,scaleY:0.98,x:-88.9,y:366.3},0).wait(1).to({scaleX:0.98,scaleY:0.98,x:-89.6,y:365.4},0).wait(1).to({scaleX:0.99,scaleY:0.99,x:-90.2,y:364.5},0).wait(1).to({scaleX:0.99,scaleY:0.99,x:-90.7,y:363.7},0).wait(1).to({scaleX:0.99,scaleY:0.99,x:-91.2,y:362.9},0).wait(1).to({scaleX:0.99,scaleY:0.99,x:-91.7,y:362.3},0).wait(1).to({scaleX:0.99,scaleY:0.99,x:-92.2,y:361.6},0).wait(1).to({x:-92.6,y:361},0).wait(1).to({scaleX:0.99,scaleY:0.99,x:-93,y:360.5},0).wait(1).to({scaleX:0.99,scaleY:0.99,x:-93.3,y:360},0).wait(1).to({scaleX:0.99,scaleY:0.99,x:-93.6,y:359.5},0).wait(1).to({scaleX:0.99,scaleY:0.99,x:-94,y:359.1},0).wait(1).to({x:-94.2,y:358.7},0).wait(1).to({scaleX:1,scaleY:1,x:-94.5,y:358.3},0).wait(1).to({x:-94.7,y:358},0).wait(1).to({scaleX:1,scaleY:1,x:-94.9,y:357.6},0).wait(1).to({x:-95.2,y:357.4},0).wait(1).to({scaleX:1,scaleY:1,x:-95.3,y:357.1},0).wait(1).to({x:-95.5,y:356.9},0).wait(1).to({x:-95.7,y:356.6},0).wait(1).to({scaleX:1,scaleY:1,x:-95.8,y:356.4},0).wait(1).to({x:-95.9,y:356.3},0).wait(1).to({x:-96,y:356.1},0).wait(1).to({x:-96.1,y:356},0).wait(1).to({scaleX:1,scaleY:1,x:-96.2,y:355.8},0).wait(1).to({x:-96.3,y:355.7},0).wait(1).to({x:-96.4,y:355.6},0).wait(1).to({x:-96.5,y:355.5},0).wait(1).to({y:355.4},0).wait(1).to({x:-96.6,y:355.3},0).wait(1).to({scaleX:1,scaleY:1},0).wait(1).to({x:-96.7,y:355.2},0).wait(1).to({y:355.1},0).wait(2).to({x:-96.8},0).wait(2).to({y:355},0).wait(9).to({x:-97},0).to({_off:true},3).wait(9));

	// WRC_route.png
	this.instance_1 = new lib.WRC_route();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-674,186);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({_off:true},102).wait(9));

	// WRC_cloud.png
	this.instance_2 = new lib.WRC_cloud_1();
	this.instance_2.parent = this;
	this.instance_2.setTransform(171.1,503.3,0.6,0.6,20.5,0,0,0.1,176.2);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(23).to({_off:false},0).wait(1).to({regX:0,regY:176,scaleX:0.65,scaleY:0.65,rotation:17.7,x:142.1,y:487.5},0).wait(1).to({scaleX:0.7,scaleY:0.7,rotation:15.6,x:119.8,y:475.4},0).wait(1).to({scaleX:0.73,scaleY:0.73,rotation:13.9,x:101.7,y:465.6},0).wait(1).to({scaleX:0.76,scaleY:0.76,rotation:12.5,x:86.7,y:457.6},0).wait(1).to({scaleX:0.78,scaleY:0.78,rotation:11.3,x:74,y:450.6},0).wait(1).to({scaleX:0.8,scaleY:0.8,rotation:10.3,x:63,y:444.7},0).wait(1).to({scaleX:0.82,scaleY:0.82,rotation:9.4,x:53.4,y:439.5},0).wait(1).to({scaleX:0.83,scaleY:0.83,rotation:8.6,x:44.9,y:434.9},0).wait(1).to({scaleX:0.85,scaleY:0.85,rotation:7.9,x:37.4,y:430.9},0).wait(1).to({scaleX:0.86,scaleY:0.86,rotation:7.2,x:30.7,y:427.3},0).wait(1).to({scaleX:0.87,scaleY:0.87,rotation:6.7,x:24.6,y:423.9},0).wait(1).to({scaleX:0.88,scaleY:0.88,rotation:6.1,x:19.1,y:421},0).wait(1).to({scaleX:0.89,scaleY:0.89,rotation:5.7,x:14.1,y:418.3},0).wait(1).to({scaleX:0.9,scaleY:0.9,rotation:5.2,x:9.5,y:415.8},0).wait(1).to({scaleX:0.91,scaleY:0.91,rotation:4.9,x:5.4,y:413.6},0).wait(1).to({scaleX:0.91,scaleY:0.91,rotation:4.5,x:1.6,y:411.5},0).wait(1).to({scaleX:0.92,scaleY:0.92,rotation:4.2,x:-1.9,y:409.7},0).wait(1).to({scaleX:0.93,scaleY:0.93,rotation:3.9,x:-5.1,y:407.9},0).wait(1).to({scaleX:0.93,scaleY:0.93,rotation:3.6,x:-8.1,y:406.3},0).wait(1).to({scaleX:0.94,scaleY:0.94,rotation:3.3,x:-10.8,y:404.8},0).wait(1).to({scaleX:0.94,scaleY:0.94,rotation:3.1,x:-13.3,y:403.4},0).wait(1).to({scaleX:0.94,scaleY:0.94,rotation:2.9,x:-15.7,y:402.2},0).wait(1).to({scaleX:0.95,scaleY:0.95,rotation:2.7,x:-17.8,y:401},0).wait(1).to({scaleX:0.95,scaleY:0.95,rotation:2.5,x:-19.9,y:399.9},0).wait(1).to({scaleX:0.96,scaleY:0.96,rotation:2.3,x:-21.8,y:398.9},0).wait(1).to({scaleX:0.96,scaleY:0.96,rotation:2.1,x:-23.5,y:398},0).wait(1).to({scaleX:0.96,scaleY:0.96,rotation:2,x:-25.1,y:397.1},0).wait(1).to({scaleX:0.96,scaleY:0.96,rotation:1.8,x:-26.7,y:396.2},0).wait(1).to({scaleX:0.97,scaleY:0.97,rotation:1.7,x:-28.1,y:395.5},0).wait(1).to({scaleX:0.97,scaleY:0.97,rotation:1.6,x:-29.4,y:394.8},0).wait(1).to({scaleX:0.97,scaleY:0.97,rotation:1.4,x:-30.6,y:394.1},0).wait(1).to({scaleX:0.97,scaleY:0.97,rotation:1.3,x:-31.8,y:393.5},0).wait(1).to({scaleX:0.98,scaleY:0.98,rotation:1.2,x:-32.8,y:392.9},0).wait(1).to({scaleX:0.98,scaleY:0.98,rotation:1.1,x:-33.9,y:392.4},0).wait(1).to({scaleX:0.98,scaleY:0.98,x:-34.8,y:391.9},0).wait(1).to({scaleX:0.98,scaleY:0.98,rotation:1,x:-35.7,y:391.4},0).wait(1).to({scaleX:0.98,scaleY:0.98,rotation:0.9,x:-36.5,y:391},0).wait(1).to({scaleX:0.98,scaleY:0.98,rotation:0.8,x:-37.2,y:390.5},0).wait(1).to({scaleX:0.99,scaleY:0.99,x:-37.9,y:390.1},0).wait(1).to({scaleX:0.99,scaleY:0.99,rotation:0.7,x:-38.6,y:389.8},0).wait(1).to({scaleX:0.99,scaleY:0.99,rotation:0.6,x:-39.2,y:389.5},0).wait(1).to({scaleX:0.99,scaleY:0.99,x:-39.8,y:389.2},0).wait(1).to({scaleX:0.99,scaleY:0.99,rotation:0.5,x:-40.3,y:388.9},0).wait(1).to({x:-40.8,y:388.6},0).wait(1).to({scaleX:0.99,scaleY:0.99,rotation:0.4,x:-41.3,y:388.3},0).wait(1).to({scaleX:0.99,scaleY:0.99,x:-41.7,y:388.1},0).wait(1).to({scaleX:0.99,scaleY:0.99,x:-42.1,y:387.9},0).wait(1).to({scaleX:0.99,scaleY:0.99,rotation:0.3,x:-42.5,y:387.7},0).wait(1).to({x:-42.8,y:387.5},0).wait(1).to({scaleX:1,scaleY:1,x:-43.1,y:387.3},0).wait(1).to({rotation:0.2,x:-43.4,y:387.2},0).wait(1).to({scaleX:1,scaleY:1,x:-43.7,y:387},0).wait(1).to({x:-44,y:386.9},0).wait(1).to({scaleX:1,scaleY:1,x:-44.2,y:386.8},0).wait(1).to({x:-44.4,y:386.6},0).wait(1).to({rotation:0.1,x:-44.6},0).wait(1).to({scaleX:1,scaleY:1,x:-44.7,y:386.5},0).wait(1).to({x:-44.9,y:386.4},0).wait(1).to({x:-45,y:386.3},0).wait(1).to({scaleX:1,scaleY:1,x:-45.2},0).wait(1).to({x:-45.3,y:386.1},0).wait(1).to({x:-45.4},0).wait(1).to({rotation:0,x:-45.5},0).wait(1).to({x:-45.6,y:386},0).wait(1).to({x:-45.7},0).wait(1).to({scaleX:1,scaleY:1,x:-45.8,y:385.9},0).wait(3).to({x:-45.9},0).wait(1).to({y:385.8},0).wait(2).to({x:-46},0).wait(4).to({y:386},0).to({_off:true},3).wait(9));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-674,186,1024,500);


(lib.prject_illustration = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.project_illustration_placeHolder();
	this.instance.parent = this;
	this.instance.setTransform(52.2,-112.3,0.7,0.7,0,0,0,-15,-268.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(140));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-409.1,166.6,716.8,389);


(lib.masqueTexte = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// masqueTexte_1_ligne
	this.instance = new lib.masqueTexte_1_ligne();
	this.instance.parent = this;
	this.instance.setTransform(-218.8,191.5,0.008,1,0,0,0,0,25.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(6).to({_off:false},0).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.01,x:218.8},14).wait(1));

	// masqueTexte_1_ligne
	this.instance_1 = new lib.masqueTexte_1_ligne();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-218.8,136.1,0.008,1,0,0,0,0,25.5);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.01,x:218.8},14).to({_off:true},1).wait(2));

	// masqueTexte_1_ligne
	this.instance_2 = new lib.masqueTexte_1_ligne();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-218.8,80.8,0.008,1,0,0,0,0,25.5);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(2).to({_off:false},0).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.01,x:218.8},14).to({_off:true},1).wait(4));

	// masqueTexte_1_ligne
	this.instance_3 = new lib.masqueTexte_1_ligne();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-218.8,25.5,0.008,1,0,0,0,0,25.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.01,x:218.8},14).to({_off:true},1).wait(6));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-220.5,-1.9,3.5,53);


(lib.hashtag_graphic = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(64));

	// hastag graphic branche
	this.instance = new lib.hastaggraphicbranche();
	this.instance.parent = this;
	this.instance.setTransform(37.4,51.9,1.084,0.995,0,-78.3,102.1,0.1,4.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({x:6.5,y:50.1},17,cjs.Ease.quartOut).to({_off:true},5).wait(3).to({_off:false},0).to({_off:true},4).wait(2).to({_off:false,x:26.5},0).to({_off:true},5).wait(28));

	// hastag graphic branche
	this.instance_1 = new lib.hastaggraphicbranche();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-41.6,49.9,1.084,0.995,0,-78.3,102.1,0.1,4.6);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off:false},0).to({x:-7.4,y:49.8},18,cjs.Ease.quartOut).to({_off:true},4).wait(5).to({_off:false},0).to({_off:true},4).wait(31));

	// hastag graphic branche
	this.instance_2 = new lib.hastaggraphicbranche();
	this.instance_2.parent = this;
	this.instance_2.setTransform(2.7,21.7,1,1,0,0,0,0,4.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({x:-2.3,y:58.7},13,cjs.Ease.quartOut).to({_off:true},11).wait(3).to({_off:false,scaleX:2.33,scaleY:1.03,skewX:-12.8,x:25.5},0).to({_off:true},4).wait(33));

	// hastag graphic branche
	this.instance_3 = new lib.hastaggraphicbranche();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-7.3,77.7,1,1,0,0,0,0,4.6);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({x:0.7,y:41.7},14,cjs.Ease.quartOut).to({_off:true},4).wait(7).to({_off:false,regX:0.1,scaleX:1.98,scaleY:1.01,skewX:-9.8,x:19.9,y:49.7},0).to({_off:true},4).wait(4).to({_off:false,regX:0,scaleX:1,scaleY:1,skewX:0,x:0.7,y:41.7},0).to({_off:true},4).wait(21));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-18.1,17.1,64.6,57.7);


(lib.btn_playVideo = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{open:1,close:97});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_96 = function() {
		this.stop();
	}
	this.frame_135 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(96).call(this.frame_96).wait(39).call(this.frame_135).wait(1));

	// ZONE
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.004)").s().p("Am7G8IAAt3IN3AAIAAN3g");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(136));

	// Calque_16
	this.instance = new lib.line_explode();
	this.instance.parent = this;
	this.instance.setTransform(2.7,0,1,1,120);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(127).to({_off:false},0).wait(9));

	// Calque_15
	this.instance_1 = new lib.line_explode();
	this.instance_1.parent = this;
	this.instance_1.setTransform(2.8,0,0.778,1,-155.3);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(125).to({_off:false},0).wait(11));

	// Calque_14
	this.instance_2 = new lib.line_explode();
	this.instance_2.parent = this;
	this.instance_2.setTransform(2.8,0.1,1,1,-80.3);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(126).to({_off:false},0).wait(10));

	// Calque_13
	this.instance_3 = new lib.line_explode();
	this.instance_3.parent = this;
	this.instance_3.setTransform(2.9,0.1,0.807,1,69.7,0,0,0.1,-0.1);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(124).to({_off:false},0).wait(12));

	// Calque_12
	this.instance_4 = new lib.line_explode();
	this.instance_4.parent = this;
	this.instance_4.setTransform(2.8,0);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(123).to({_off:false},0).wait(13));

	// Calque_5
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgGADIANgF");
	this.shape_1.setTransform(-10.5,13.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgVALIArgU");
	this.shape_2.setTransform(-8.9,12.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgkASIBJgj");
	this.shape_3.setTransform(-7.4,12);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgzAZIBngx");
	this.shape_4.setTransform(-5.9,11.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhDAgICHg/");
	this.shape_5.setTransform(-4.4,10.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhSAnIClhN");
	this.shape_6.setTransform(-2.9,9.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhhAvIDDhc");
	this.shape_7.setTransform(-1.4,9.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhwA2IDhhr");
	this.shape_8.setTransform(0.2,8.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#FFFFFF").ss(2,1,1).p("Ah/A9ID/h5");
	this.shape_9.setTransform(1.7,7.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#FFFFFF").ss(2,1,1).p("AiOBEIEdiH");
	this.shape_10.setTransform(3.2,7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_1}]},54).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[]},34).wait(39));

	// Calque_6
	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAAgFIAAAL");
	this.shape_11.setTransform(-11.1,-13.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAAATIAAgl");
	this.shape_12.setTransform(-11.1,-12.5);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAAAhIAAhB");
	this.shape_13.setTransform(-11.1,-11.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAAAvIAAhc");
	this.shape_14.setTransform(-11.1,-9.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAAA8IAAh3");
	this.shape_15.setTransform(-11.1,-8.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAABKIAAiT");
	this.shape_16.setTransform(-11.1,-7.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAABXIAAit");
	this.shape_17.setTransform(-11.1,-5.7);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAABlIAAjJ");
	this.shape_18.setTransform(-11.1,-4.4);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAAByIAAjj");
	this.shape_19.setTransform(-11.1,-3);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAACAIAAj/");
	this.shape_20.setTransform(-11.1,-1.7);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAAiMIAAEZ");
	this.shape_21.setTransform(-11.1,-0.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_11}]},44).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[]},43).wait(39));

	// Calque_4
	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#FFFFFF").ss(2,1,1).p("AANAHIgZgM");
	this.shape_22.setTransform(16.2,-0.5);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgbgNIA3Ab");
	this.shape_23.setTransform(14.8,-1.2);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgpgUIBTAp");
	this.shape_24.setTransform(13.3,-2);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#FFFFFF").ss(2,1,1).p("Ag3gcIBwA5");
	this.shape_25.setTransform(11.9,-2.7);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhGgjICNBH");
	this.shape_26.setTransform(10.4,-3.4);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhUgrICpBX");
	this.shape_27.setTransform(9,-4.2);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhjgyIDHBl");
	this.shape_28.setTransform(7.5,-4.9);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#FFFFFF").ss(2,1,1).p("Ahxg5IDjBz");
	this.shape_29.setTransform(6.1,-5.7);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#FFFFFF").ss(2,1,1).p("AiAhBIEBCD");
	this.shape_30.setTransform(4.6,-6.4);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#FFFFFF").ss(2,1,1).p("ACPBJIkdiR");
	this.shape_31.setTransform(3.2,-7.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_22}]},35).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[]},53).wait(39));

	// Calque_3
	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAVAAIgpAA");
	this.shape_32.setTransform(-318.9,0);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgVAAIArAA");
	this.shape_33.setTransform(-318.6,0);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgZAAIAzAA");
	this.shape_34.setTransform(-317.5,0);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgfAAIA/AA");
	this.shape_35.setTransform(-315.8,0);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgnAAIBPAA");
	this.shape_36.setTransform(-313.3,0);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgyAAIBkAA");
	this.shape_37.setTransform(-310.1,0);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#FFFFFF").ss(2,1,1).p("Ag/AAIB+AA");
	this.shape_38.setTransform(-306.3,0);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhOAAICdAA");
	this.shape_39.setTransform(-301.7,0);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhfAAIC/AA");
	this.shape_40.setTransform(-296.5,0);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhzAAIDnAA");
	this.shape_41.setTransform(-290.5,0);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#FFFFFF").ss(2,1,1).p("AiJAAIETAA");
	this.shape_42.setTransform(-283.8,0);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#FFFFFF").ss(2,1,1).p("AiiAAIFFAA");
	this.shape_43.setTransform(-276.4,0);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#FFFFFF").ss(2,1,1).p("Ai9AAIF7AA");
	this.shape_44.setTransform(-268.4,0);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#FFFFFF").ss(2,1,1).p("AjaAAIG2AA");
	this.shape_45.setTransform(-259.6,0);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().s("#FFFFFF").ss(2,1,1).p("Aj6AAIH1AA");
	this.shape_46.setTransform(-250.1,0);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().s("#FFFFFF").ss(2,1,1).p("AkcAAII5AA");
	this.shape_47.setTransform(-239.9,0);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f().s("#FFFFFF").ss(2,1,1).p("AlAAAIKBAA");
	this.shape_48.setTransform(-229.1,0);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f().s("#FFFFFF").ss(2,1,1).p("AlnAAILPAA");
	this.shape_49.setTransform(-217.5,0);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f().s("#FFFFFF").ss(2,1,1).p("AmQAAIMhAA");
	this.shape_50.setTransform(-205.2,0);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f().s("#FFFFFF").ss(2,1,1).p("AmUAAIMpAA");
	this.shape_51.setTransform(-194.2,0);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f().s("#FFFFFF").ss(2,1,1).p("AmZAAIMzAA");
	this.shape_52.setTransform(-183.3,0);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f().s("#FFFFFF").ss(2,1,1).p("AmdAAIM7AA");
	this.shape_53.setTransform(-172.3,0);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f().s("#FFFFFF").ss(2,1,1).p("AmhAAINDAA");
	this.shape_54.setTransform(-161.3,0);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f().s("#FFFFFF").ss(2,1,1).p("AmmAAINNAA");
	this.shape_55.setTransform(-150.4,0);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f().s("#FFFFFF").ss(2,1,1).p("AmqAAINVAA");
	this.shape_56.setTransform(-139.4,0);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f().s("#FFFFFF").ss(2,1,1).p("AmvAAINeAA");
	this.shape_57.setTransform(-128.4,0);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f().s("#FFFFFF").ss(2,1,1).p("AmzAAINnAA");
	this.shape_58.setTransform(-117.5,0);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f().s("#FFFFFF").ss(2,1,1).p("Am4AAINxAA");
	this.shape_59.setTransform(-106.5,0);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f().s("#FFFFFF").ss(2,1,1).p("Am8AAIN5AA");
	this.shape_60.setTransform(-95.5,0);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f().s("#FFFFFF").ss(2,1,1).p("AnAAAIOBAA");
	this.shape_61.setTransform(-84.6,0);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f().s("#FFFFFF").ss(2,1,1).p("AnFAAIOLAA");
	this.shape_62.setTransform(-73.6,0);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f().s("#FFFFFF").ss(2,1,1).p("AnJAAIOTAA");
	this.shape_63.setTransform(-62.6,0);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f().s("#FFFFFF").ss(2,1,1).p("AnOAAIOdAA");
	this.shape_64.setTransform(-51.7,0);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f().s("#FFFFFF").ss(2,1,1).p("AnSAAIOlAA");
	this.shape_65.setTransform(-40.7,0);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f().s("#FFFFFF").ss(2,1,1).p("AHXAAIutAA");
	this.shape_66.setTransform(-29.8,0);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f().s("#FFFFFF").ss(2,1,1).p("AmfAAIM/AA");
	this.shape_67.setTransform(-24.2,0);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f().s("#FFFFFF").ss(2,1,1).p("AlrAAILXAA");
	this.shape_68.setTransform(-19,0);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f().s("#FFFFFF").ss(2,1,1).p("Ak7AAIJ3AA");
	this.shape_69.setTransform(-14.2,0);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f().s("#FFFFFF").ss(2,1,1).p("AkOAAIIdAA");
	this.shape_70.setTransform(-9.7,0);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f().s("#FFFFFF").ss(2,1,1).p("AjlAAIHLAA");
	this.shape_71.setTransform(-5.6,0);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f().s("#FFFFFF").ss(2,1,1).p("Ai/AAIF/AA");
	this.shape_72.setTransform(-1.8,0);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f().s("#FFFFFF").ss(2,1,1).p("AidAAIE7AA");
	this.shape_73.setTransform(1.6,0);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f().s("#FFFFFF").ss(2,1,1).p("Ah/AAID/AA");
	this.shape_74.setTransform(4.6,0);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhkAAIDJAA");
	this.shape_75.setTransform(7.3,0);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhNAAICbAA");
	this.shape_76.setTransform(9.6,0);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f().s("#FFFFFF").ss(2,1,1).p("Ag5AAIBzAA");
	this.shape_77.setTransform(11.6,0);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgpAAIBTAA");
	this.shape_78.setTransform(13.2,0);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgcAAIA5AA");
	this.shape_79.setTransform(14.5,0);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgTAAIAnAA");
	this.shape_80.setTransform(15.3,0);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgOAAIAdAA");
	this.shape_81.setTransform(15.9,0);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f().s("#FFFFFF").ss(2,1,1).p("AANAAIgZAA");
	this.shape_82.setTransform(16.1,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_51}]},1).to({state:[{t:this.shape_52}]},1).to({state:[{t:this.shape_53}]},1).to({state:[{t:this.shape_54}]},1).to({state:[{t:this.shape_55}]},1).to({state:[{t:this.shape_56}]},1).to({state:[{t:this.shape_57}]},1).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_59}]},1).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_63}]},1).to({state:[{t:this.shape_64}]},1).to({state:[{t:this.shape_65}]},1).to({state:[{t:this.shape_66}]},1).to({state:[{t:this.shape_67}]},1).to({state:[{t:this.shape_68}]},1).to({state:[{t:this.shape_69}]},1).to({state:[{t:this.shape_70}]},1).to({state:[{t:this.shape_71}]},1).to({state:[{t:this.shape_72}]},1).to({state:[{t:this.shape_73}]},1).to({state:[{t:this.shape_74}]},1).to({state:[{t:this.shape_75}]},1).to({state:[{t:this.shape_76}]},1).to({state:[{t:this.shape_77}]},1).to({state:[{t:this.shape_78}]},1).to({state:[{t:this.shape_79}]},1).to({state:[{t:this.shape_80}]},1).to({state:[{t:this.shape_81}]},1).to({state:[{t:this.shape_82}]},1).to({state:[]},1).wait(84));

	// Calque_8 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_62 = new cjs.Graphics().p("Ai/DUIAAmoIF/AAIAAGog");
	var mask_graphics_63 = new cjs.Graphics().p("Ai/DUIAAmoIF/AAIAAGog");
	var mask_graphics_64 = new cjs.Graphics().p("Ai/DUIAAmoIF/AAIAAGog");
	var mask_graphics_65 = new cjs.Graphics().p("Ai/DUIAAmoIF/AAIAAGog");
	var mask_graphics_66 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_67 = new cjs.Graphics().p("AjADUIAAmoIGAAAIAAGog");
	var mask_graphics_68 = new cjs.Graphics().p("Ai/DUIAAmoIGAAAIAAGog");
	var mask_graphics_69 = new cjs.Graphics().p("Ai/DUIAAmoIF/AAIAAGog");
	var mask_graphics_70 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_71 = new cjs.Graphics().p("Ai/DUIAAmoIGAAAIAAGog");
	var mask_graphics_72 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_73 = new cjs.Graphics().p("Ai/DUIAAmoIF/AAIAAGog");
	var mask_graphics_74 = new cjs.Graphics().p("AjADUIAAmoIGAAAIAAGog");
	var mask_graphics_75 = new cjs.Graphics().p("AjADUIAAmoIGAAAIAAGog");
	var mask_graphics_76 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_77 = new cjs.Graphics().p("Ai/DUIAAmoIF/AAIAAGog");
	var mask_graphics_78 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_79 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_80 = new cjs.Graphics().p("Ai/DUIAAmoIF/AAIAAGog");
	var mask_graphics_81 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_82 = new cjs.Graphics().p("Ai/DUIAAmoIF/AAIAAGog");
	var mask_graphics_83 = new cjs.Graphics().p("AjADUIAAmoIGAAAIAAGog");
	var mask_graphics_84 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_85 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_86 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_87 = new cjs.Graphics().p("Ai/DUIAAmoIGAAAIAAGog");
	var mask_graphics_88 = new cjs.Graphics().p("AjADUIAAmoIGAAAIAAGog");
	var mask_graphics_89 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_90 = new cjs.Graphics().p("Ai/DUIAAmoIGAAAIAAGog");
	var mask_graphics_91 = new cjs.Graphics().p("AjADUIAAmoIGAAAIAAGog");
	var mask_graphics_92 = new cjs.Graphics().p("Ai/DUIAAmoIF/AAIAAGog");
	var mask_graphics_93 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_94 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_95 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_96 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_97 = new cjs.Graphics().p("Ai/DUIAAmoIGAAAIAAGog");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(62).to({graphics:mask_graphics_62,x:38.2,y:0.7}).wait(1).to({graphics:mask_graphics_63,x:38.2,y:0.7}).wait(1).to({graphics:mask_graphics_64,x:38.2,y:0.7}).wait(1).to({graphics:mask_graphics_65,x:38.2,y:0.7}).wait(1).to({graphics:mask_graphics_66,x:38.2,y:0.7}).wait(1).to({graphics:mask_graphics_67,x:38.1,y:0.7}).wait(1).to({graphics:mask_graphics_68,x:38,y:0.7}).wait(1).to({graphics:mask_graphics_69,x:37.8,y:0.7}).wait(1).to({graphics:mask_graphics_70,x:37.5,y:0.7}).wait(1).to({graphics:mask_graphics_71,x:37.1,y:0.7}).wait(1).to({graphics:mask_graphics_72,x:36.5,y:0.7}).wait(1).to({graphics:mask_graphics_73,x:35.6,y:0.7}).wait(1).to({graphics:mask_graphics_74,x:34.5,y:0.7}).wait(1).to({graphics:mask_graphics_75,x:33.2,y:0.7}).wait(1).to({graphics:mask_graphics_76,x:31.4,y:0.7}).wait(1).to({graphics:mask_graphics_77,x:29.2,y:0.7}).wait(1).to({graphics:mask_graphics_78,x:26.6,y:0.7}).wait(1).to({graphics:mask_graphics_79,x:23.4,y:0.7}).wait(1).to({graphics:mask_graphics_80,x:20.2,y:0.7}).wait(1).to({graphics:mask_graphics_81,x:17.6,y:0.7}).wait(1).to({graphics:mask_graphics_82,x:15.4,y:0.7}).wait(1).to({graphics:mask_graphics_83,x:13.6,y:0.7}).wait(1).to({graphics:mask_graphics_84,x:12.3,y:0.7}).wait(1).to({graphics:mask_graphics_85,x:11.2,y:0.7}).wait(1).to({graphics:mask_graphics_86,x:10.4,y:0.7}).wait(1).to({graphics:mask_graphics_87,x:9.7,y:0.7}).wait(1).to({graphics:mask_graphics_88,x:9.3,y:0.7}).wait(1).to({graphics:mask_graphics_89,x:9,y:0.7}).wait(1).to({graphics:mask_graphics_90,x:8.8,y:0.7}).wait(1).to({graphics:mask_graphics_91,x:8.7,y:0.7}).wait(1).to({graphics:mask_graphics_92,x:8.6,y:0.7}).wait(1).to({graphics:mask_graphics_93,x:8.6,y:0.7}).wait(1).to({graphics:mask_graphics_94,x:8.6,y:0.7}).wait(1).to({graphics:mask_graphics_95,x:8.6,y:0.7}).wait(1).to({graphics:mask_graphics_96,x:8.6,y:0.7}).wait(1).to({graphics:mask_graphics_97,x:4.8,y:0.7}).wait(39));

	// Calque_7
	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("#FFFFFF").s().p("AiOiMIEdCRIkdCIg");
	this.shape_83.setTransform(3.2,-0.3);

	this.instance_5 = new lib.playArrow();
	this.instance_5.parent = this;
	this.instance_5.setTransform(3.2,-0.3);
	this.instance_5._off = true;

	var maskedShapeInstanceList = [this.shape_83,this.instance_5];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_83}]},62).to({state:[{t:this.instance_5}]},35).to({state:[{t:this.instance_5}]},3).to({state:[{t:this.instance_5}]},18).to({state:[]},1).wait(17));
	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(97).to({_off:false},0).wait(3).to({scaleX:0.07,scaleY:0.07},18,cjs.Ease.quartIn).to({_off:true},1).wait(17));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-44.3,-44.4,88.8,88.8);


(lib.Btn_next = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{rollOver:55,rollOut:70});

	// timeline functions:
	this.frame_54 = function() {
		this.stop();
	}
	this.frame_69 = function() {
		this.stop();
	}
	this.frame_84 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(54).call(this.frame_54).wait(15).call(this.frame_69).wait(15).call(this.frame_84).wait(1));

	// Calque_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.008)").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape.setTransform(0,29.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(85));

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgJAqIAAhDIgXAAIAAgQIBBAAIAAAQIgYAAIAABDg");
	this.shape_1.setTransform(46.1,29.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AASAqIgSgcIgRAcIgWAAIAcgqIgagpIAWAAIAPAZIAQgZIAWAAIgaApIAdAqg");
	this.shape_2.setTransform(30.2,29.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgeAqIAAhTIA8AAIAAARIgpAAIAAARIAkAAIAAAPIgkAAIAAASIAqAAIAAAQg");
	this.shape_3.setTransform(14.3,29.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAUAqIgngzIAAAzIgTAAIAAhTIASAAIAoA1IAAg1IATAAIAABTg");
	this.shape_4.setTransform(-2.7,29.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgKAQIAEgOQgEgCAAgFQAAgEADgDQADgDAEAAQAFAAADADQADADAAAEQAAAEgDAEIgIANg");
	this.shape_5.setTransform(-28.5,32.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAPAqIgYghIgIAKIAAAXIgTAAIAAhTIATAAIAAAjIAegjIAYAAIgiAlIAQAWIASAYg");
	this.shape_6.setTransform(-38,29.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgfAfQgMgNAAgSQAAgSAMgMQAOgNARAAQASAAANANQANAMAAASQAAASgNANQgNAMgSAAQgRAAgOgMgAgQgSQgIAIAAAKQAAALAIAIQAGAIAKAAQALAAAHgIQAHgIAAgLQAAgKgHgIQgHgIgLAAQgKAAgGAIg");
	this.shape_7.setTransform(-55.7,29.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#333333").ss(1,1,1).p("AxpAAMAjTAAA");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(85));

	// Layer_2
	this.instance = new lib.Btn_cliquable_area();
	this.instance.parent = this;
	this.instance.setTransform(0,29.5,1,1,0,0,0,0,30);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({alpha:0.012},46).wait(6).to({y:30},0).to({alpha:1},14).wait(1).to({alpha:0.012},13).wait(2));

	// BG
	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("rgba(0,0,0,0.098)").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape_9.setTransform(0,29.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_9).wait(85));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-114,-1,228,60.5);


(lib.Btn_detail = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"rollOver":55,"rollOut":70});

	// timeline functions:
	this.frame_54 = function() {
		this.stop();
	}
	this.frame_69 = function() {
		this.stop();
	}
	this.frame_84 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(54).call(this.frame_54).wait(15).call(this.frame_69).wait(15).call(this.frame_84).wait(1));

	// Calque_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.008)").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape.setTransform(0,29.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(85));

	// content
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgOAyIAAhkIAdAAIAABkg");
	this.shape_1.setTransform(-85.4,29.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgyAOIAAgcIBlAAIAAAcg");
	this.shape_2.setTransform(-85.4,29.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgaAqIAAhTIASAAIAABDIAjAAIAAAQg");
	this.shape_3.setTransform(48.4,29.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgIAqIAAhTIARAAIAABTg");
	this.shape_4.setTransform(34.7,29.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AAZAqIgHgSIgjAAIgHASIgUAAIAkhTIARAAIAkBTgAgKAIIAUAAIgKgYg");
	this.shape_5.setTransform(20.2,29.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgJAqIAAhDIgXAAIAAgQIBBAAIAAAQIgYAAIAABDg");
	this.shape_6.setTransform(4.1,29.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgeAqIAAhTIA8AAIAAARIgpAAIAAARIAkAAIAAAPIgkAAIAAASIAqAAIAAAQg");
	this.shape_7.setTransform(-11.3,29.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AglAqIAAhTIAeAAQAVAAAMALQAMALAAATQAAATgMALQgMAMgWAAgAgTAaIANAAQAMAAAGgHQAHgGAAgNQAAgLgHgHQgGgGgOgBIgLAAg");
	this.shape_8.setTransform(-27.8,29.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#333333").ss(1,1,1).p("AxpAAMAjTAAA");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(85));

	// Calque_7
	this.instance = new lib.Btn_cliquable_area();
	this.instance.parent = this;
	this.instance.setTransform(0,29.5,1,1,0,0,0,0,30);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({alpha:0.012},46).wait(6).to({y:30},0).to({alpha:1},14).wait(1).to({alpha:0.012},13).wait(2));

	// BG
	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("rgba(0,0,0,0.004)").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape_10.setTransform(0,29.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_10).wait(85));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-114,-1,228,60.5);


(lib.bloc_visuel = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":68});

	// timeline functions:
	this.frame_67 = function() {
		this.stop();
	}
	this.frame_104 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(67).call(this.frame_67).wait(37).call(this.frame_104).wait(1));

	// masque VISUEL (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("EgjsBDvMAAAiHdMBHZAAAMAAACHdg");
	mask.setTransform(-228.7,-19);

	// VISUEL
	this.instance = new lib.prject_illustration();
	this.instance.parent = this;
	this.instance.setTransform(242.6,-660.6,1,1,0,0,0,11.6,-308.4);
	this.instance._off = true;

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(26).to({_off:false},0).to({regX:11.7,regY:-308.3,x:-85.5,y:-660.5},33,cjs.Ease.quartOut).wait(15).to({rotation:-21.7,x:-1861.9,y:-560.5},30,cjs.Ease.quartInOut).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.bloc_video = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"open":1,"close":42});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_34 = function() {
		this.btn_playVideo.gotoAndPlay("open");
	}
	this.frame_41 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(34).call(this.frame_34).wait(7).call(this.frame_41).wait(16));

	// PLAY btn
	this.btn_playVideo = new lib.btn_playVideo();
	this.btn_playVideo.name = "btn_playVideo";
	this.btn_playVideo.parent = this;
	this.btn_playVideo.setTransform(358.3,-255.7);

	this.timeline.addTween(cjs.Tween.get(this.btn_playVideo).wait(57));

	// masque video (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_43 = new cjs.Graphics().p("Ega8A8AMAAAh3/MA7rAAAMAAAB3/g");
	var mask_graphics_44 = new cjs.Graphics().p("EgYrA8AMAAAh3/MA3HAAAMAAAB3/g");
	var mask_graphics_45 = new cjs.Graphics().p("EgWaA8AMAAAh3/MAyiAAAMAAAB3/g");
	var mask_graphics_46 = new cjs.Graphics().p("EgUJA8AMAAAh3/MAt+AAAMAAAB3/g");
	var mask_graphics_47 = new cjs.Graphics().p("EgR4A8AMAAAh3/MApaAAAMAAAB3/g");
	var mask_graphics_48 = new cjs.Graphics().p("EgPnA8AMAAAh3/MAk1AAAMAAAB3/g");
	var mask_graphics_49 = new cjs.Graphics().p("EgNWA8AMAAAh3/MAgRAAAMAAAB3/g");
	var mask_graphics_50 = new cjs.Graphics().p("EgLFA8AMAAAh3/IbtAAMAAAB3/g");
	var mask_graphics_51 = new cjs.Graphics().p("EgI0A8AMAAAh3/IXIAAMAAAB3/g");
	var mask_graphics_52 = new cjs.Graphics().p("EgGjA8AMAAAh3/ISkAAMAAAB3/g");
	var mask_graphics_53 = new cjs.Graphics().p("EgESA8AMAAAh3/IOAAAMAAAB3/g");
	var mask_graphics_54 = new cjs.Graphics().p("EgCBA8AMAAAh3/IJbAAMAAAB3/g");
	var mask_graphics_55 = new cjs.Graphics().p("EAAPA8AMAAAh3/IE4AAMAAAB3/g");
	var mask_graphics_56 = new cjs.Graphics().p("EACgA8AMAAAh3/IAUAAMAAAB3/g");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(43).to({graphics:mask_graphics_43,x:209.5,y:0}).wait(1).to({graphics:mask_graphics_44,x:194.8,y:0}).wait(1).to({graphics:mask_graphics_45,x:180,y:0}).wait(1).to({graphics:mask_graphics_46,x:165.3,y:0}).wait(1).to({graphics:mask_graphics_47,x:150.6,y:0}).wait(1).to({graphics:mask_graphics_48,x:135.8,y:0}).wait(1).to({graphics:mask_graphics_49,x:121.1,y:0}).wait(1).to({graphics:mask_graphics_50,x:106.4,y:0}).wait(1).to({graphics:mask_graphics_51,x:91.6,y:0}).wait(1).to({graphics:mask_graphics_52,x:76.9,y:0}).wait(1).to({graphics:mask_graphics_53,x:62.2,y:0}).wait(1).to({graphics:mask_graphics_54,x:47.4,y:0}).wait(1).to({graphics:mask_graphics_55,x:32.7,y:0}).wait(1).to({graphics:mask_graphics_56,x:18,y:0}).wait(1));

	// VIDEO
	this.videoContainer = new lib.video();
	this.videoContainer.name = "videoContainer";
	this.videoContainer.parent = this;
	this.videoContainer.setTransform(36.7,765,0.659,0.659,0,0,0,0.3,480.2);

	var maskedShapeInstanceList = [this.videoContainer];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.videoContainer).wait(1).to({y:1},40,cjs.Ease.quartInOut).wait(16));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(36.5,-300.1,382.1,1381.2);


(lib.bloc_NEXT_btn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":40});

	// timeline functions:
	this.frame_39 = function() {
		this.stop();
	}
	this.frame_56 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(39).call(this.frame_39).wait(17).call(this.frame_56).wait(1));

	// CTA DETAIL
	this.btnNext = new lib.Btn_next();
	this.btnNext.name = "btnNext";
	this.btnNext.parent = this;
	this.btnNext.setTransform(-340.5,411.5,1,1,0,0,0,0,26.5);

	this.timeline.addTween(cjs.Tween.get(this.btnNext).to({y:350.5},39,cjs.Ease.quartInOut).wait(2).to({y:411.5},14,cjs.Ease.quartInOut).to({_off:true},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-454,384.5,227,60);


(lib.bloc_lignes = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{close_OLD:166,"close":209});

	// timeline functions:
	this.frame_165 = function() {
		this.stop();
	}
	this.frame_208 = function() {
		this.stop();
	}
	this.frame_229 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(165).call(this.frame_165).wait(43).call(this.frame_208).wait(21).call(this.frame_229).wait(1));

	// ligne
	this.instance = new lib.ligne();
	this.instance.parent = this;
	this.instance.setTransform(-454,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(166).to({x:-883},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:-454},0).wait(21));

	// ligne
	this.instance_1 = new lib.ligne();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-227,406.5,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(119).to({y:320.5},40,cjs.Ease.quartInOut).wait(7).to({y:406.5},0).to({x:-883.2},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:-227,y:320.5},0).to({y:406.5},17,cjs.Ease.quartInOut).wait(4));

	// ligne
	this.instance_2 = new lib.ligne();
	this.instance_2.parent = this;
	this.instance_2.setTransform(0,406.5,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(166).to({x:-883.4},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:0},0).wait(21));

	// ligne
	this.instance_3 = new lib.ligne();
	this.instance_3.parent = this;
	this.instance_3.setTransform(227,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(124).to({y:320},40,cjs.Ease.quartInOut).to({_off:true},2).wait(43).to({_off:false},0).to({y:406},17,cjs.Ease.quartInOut).wait(4));

	// ligne
	this.instance_4 = new lib.ligne();
	this.instance_4.parent = this;
	this.instance_4.setTransform(454.1,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(166).to({x:-883.4},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:454.1},0).wait(21));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-455.4,-174.5,910.5,988);


(lib.bloc_hashtag = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_61 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(61).call(this.frame_61).wait(2));

	// Layer_1
	this.instance = new lib.hashtag_graphic("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(-456,243.2,2.317,2.317,0,0,0,-0.1,50.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true},61).wait(2));

	// Layer_5 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_28 = new cjs.Graphics().p("AtWBpIAAjRIatAAIAADRg");
	var mask_graphics_32 = new cjs.Graphics().p("AtWDSIAAmjIatAAIAAGjg");
	var mask_graphics_40 = new cjs.Graphics().p("AtWGYIAAsvIatAAIAAMvg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(28).to({graphics:mask_graphics_28,x:-400.4,y:265.5}).wait(4).to({graphics:mask_graphics_32,x:-400.4,y:255}).wait(8).to({graphics:mask_graphics_40,x:-400.4,y:255.9}).wait(23));

	// Layer_3
	this.instance_1 = new lib.txt_hashtag();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-421.5,244.1,1,1,0,0,0,0,31.6);
	this.instance_1._off = true;

	var maskedShapeInstanceList = [this.instance_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(28).to({_off:false},0).to({_off:true},34).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-497.8,166.1,149.7,129.4);


(lib.bloc_CTA = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":40});

	// timeline functions:
	this.frame_39 = function() {
		this.stop();
	}
	this.frame_56 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(39).call(this.frame_39).wait(17).call(this.frame_56).wait(1));

	// CTA DETAIL
	this.btnCta = new lib.Btn_detail();
	this.btnCta.name = "btnCta";
	this.btnCta.parent = this;
	this.btnCta.setTransform(-340.5,411.5,1,1,0,0,0,0,26.5);

	this.timeline.addTween(cjs.Tween.get(this.btnCta).to({y:350.5},39,cjs.Ease.quartInOut).wait(2).to({y:411.5},14,cjs.Ease.quartInOut).to({_off:true},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-454,384.5,227,60);


(lib.blocInfostextes = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":80});

	// timeline functions:
	this.frame_79 = function() {
		this.stop();
	}
	this.frame_120 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(79).call(this.frame_79).wait(41).call(this.frame_120).wait(1));

	// Bloc Hashtag
	this.instance = new lib.bloc_hashtag();
	this.instance.parent = this;
	this.instance.setTransform(-391.5,261.2,0.9,0.9,0,0,0,-412.9,244.7);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(62).to({_off:false},0).to({_off:true},18).wait(41));

	// masque Titre
	this.instance_1 = new lib.masque_generique();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-527.9,-13.9,0.007,0.471,0,0,0,0,17.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(34).to({_off:false},0).to({regX:0.5,scaleX:1.92,x:-527.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.01,x:-228.9},10,cjs.Ease.quartInOut).to({_off:true},1).wait(26).to({_off:false},0).to({regX:0.5,scaleX:1.92,x:-527.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.01,x:-528.7},10,cjs.Ease.quartInOut).to({_off:true},1).wait(21));

	// _NOM-du-CLIENT
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFF000").s().p("AAbA4Ig0hFIAABFIgaAAIAAhvIAYAAIA2BHIAAhHIAZAAIAABvg");
	this.shape.setTransform(-360.6,-14.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFF000").s().p("AgMA4IAAhvIAYAAIAABvg");
	this.shape_1.setTransform(-381.7,-14.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFF000").s().p("AgjA4IAAhvIAZAAIAABYIAuAAIAAAXg");
	this.shape_2.setTransform(-400.5,-14.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFF000").s().p("AgoA4IAAhvIBPAAIAAAXIg2AAIAAAWIAxAAIAAAUIgxAAIAAAYIA4AAIAAAWg");
	this.shape_3.setTransform(-422.4,-14.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFF000").s().p("AAWA4IAAgsIgrAAIAAAsIgZAAIAAhvIAZAAIAAAvIArAAIAAgvIAZAAIAABvg");
	this.shape_4.setTransform(-446,-14.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFF000").s().p("AgjApQgQgQAAgYQAAgZAQgQQASgRAXAAQAcAAASAVIgPASQgMgPgSAAQgNAAgJAJQgLAJAAAPQAAAPAKAKQAJAKAMAAQATAAALgPIAQARQgSAVgaAAQgZAAgRgRg");
	this.shape_5.setTransform(-469.9,-14.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFF000").s().p("AgLA4IAAhvIAXAAIAABvg");
	this.shape_6.setTransform(-490.4,-14.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFF000").s().p("AAlA4IAAhFIgeA7IgOAAIgdg7IAABFIgZAAIAAhvIAhAAIAcA9IAdg9IAiAAIAABvg");
	this.shape_7.setTransform(-512.7,-14.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},43).to({state:[]},46).to({state:[]},1).wait(31));

	// DECO 1
	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFF000").s().p("AAOAjIAAgbIgbAAIAAAbIgQAAIAAhFIAQAAIAAAdIAbAAIAAgdIAQAAIAABFg");
	this.shape_8.setTransform(-138.5,-31.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFF000").s().p("AgbAuIAphbIAOAAIgpBbg");
	this.shape_9.setTransform(-145.4,-31.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFF000").s().p("AAXAjIAAgrIgSAlIgJAAIgTglIAAArIgPAAIAAhFIAVAAIARAlIASglIAVAAIAABFg");
	this.shape_10.setTransform(-153.2,-31.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFF000").s().p("AAMAjIgTgbIgHAHIAAAUIgQAAIAAhFIAQAAIAAAdIAZgdIAUAAIgcAfIANASIAPAUg");
	this.shape_11.setTransform(-161.2,-31.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFF000").s().p("AgSAeQgIgHAAgJQAAgLAJgFQgGgGAAgIQAAgIAGgGQAHgFAKAAQAKAAAHAFQAHAGAAAIQAAAIgGAGQAJAFAAALQAAAJgIAHQgHAGgMAAQgLAAgHgGgAgHAHQgDACAAAFQAAAEADADQAEACADAAQAEAAADgCQAEgDAAgEQAAgFgDgCQgEgCgEAAQgDAAgEACgAgGgVQgDADAAADQAAAEADACQADADADAAQAEAAADgDQACgCAAgEQAAgDgCgDQgDgCgEAAQgDAAgDACg");
	this.shape_12.setTransform(-168.3,-31.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFF000").s().p("AgYAZIAIgMIADACIAFAEQAEACAEAAQADAAADgCQADgDAAgEQAAgFgDgDQgDgCgFAAQgFAAgHADIgGgGIAAghIAoAAIAAANIgaAAIAAAMIAGgBQAKAAAHAGQAIAFAAALQAAAKgIAHQgHAGgLAAQgNABgKgLg");
	this.shape_13.setTransform(-174.4,-31);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFF000").s().p("AACAjIAAgRIgbAAIAAgLIAZgpIARAAIgXAnIAIAAIAAgMIAPAAIAAAMIAJAAIAAANIgJAAIAAARg");
	this.shape_14.setTransform(-180.4,-31.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFF000").s().p("AgFAGQgDgCAAgEQAAgDADgDQACgCADAAQAEAAACACQADADAAADQAAAEgDACQgCADgEAAQgDAAgCgDg");
	this.shape_15.setTransform(-184.8,-28.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFF000").s().p("AgYAZIAIgMIACACIAGAEQAEACAEAAQADAAADgCQADgDAAgEQAAgFgDgDQgEgCgEAAQgFAAgHADIgHgGIAAghIApAAIAAANIgaAAIAAAMIAGgBQAKAAAIAGQAHAFAAALQAAAKgHAHQgJAGgJAAQgPABgJgLg");
	this.shape_16.setTransform(-189.1,-31);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFF000").s().p("AgSAeQgIgHAAgJQAAgLAJgFQgGgGAAgIQAAgIAGgGQAHgFAKAAQAKAAAHAFQAHAGAAAIQAAAIgGAGQAJAFAAALQAAAJgIAHQgHAGgMAAQgLAAgHgGgAgHAHQgDACAAAFQAAAEADADQAEACADAAQAEAAADgCQAEgDAAgEQAAgFgDgCQgEgCgEAAQgDAAgEACgAgGgVQgDADAAADQAAAEADACQADADADAAQAEAAADgDQACgCAAgEQAAgDgCgDQgDgCgEAAQgDAAgDACg");
	this.shape_17.setTransform(-195.3,-31.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFF000").s().p("AgFAGQgDgCAAgEQAAgDADgDQACgCADAAQAEAAACACQADADAAADQAAAEgDACQgCADgEAAQgDAAgCgDg");
	this.shape_18.setTransform(-202.6,-28.4);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFF000").s().p("AgfAjIAAhFIAZAAQASAAAKAJQAKAKAAAPQAAAQgKAKQgKAJgTAAgAgPAVIAKAAQAKAAAFgFQAGgFAAgLQAAgJgGgGQgFgFgLAAIgJAAg");
	this.shape_19.setTransform(-207.7,-31.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFF000").s().p("AgZAjIAAhFIAyAAIAAAOIgiAAIAAAOIAeAAIAAANIgeAAIAAAOIAjAAIAAAOg");
	this.shape_20.setTransform(-214.9,-31.1);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFF000").s().p("AgZAjIAAhFIAyAAIAAAOIgiAAIAAAOIAeAAIAAANIgeAAIAAAOIAjAAIAAAOg");
	this.shape_21.setTransform(-221.4,-31.1);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFF000").s().p("AgbAjIAAhFIAZAAQAPAAAHAHQAIAFAAAOQAAAMgIAFQgHAGgPABIgJAAIAAATgAgLACIAKAAQAHAAADgCQADgEAAgFQAAgGgEgDQgDgCgHAAIgJAAg");
	this.shape_22.setTransform(-228,-31.1);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFF000").s().p("AgbAYIAKgMQALAKAJAAQAEAAACgCQADgBAAgEQAAgDgDgCIgJgDQgMgDgGgEQgGgFAAgJQAAgKAIgGQAHgFAJAAQAHAAAHACQAHADAGAEIgIALQgJgHgKAAQgDAAgCACQgDACAAADQAAADADACIAMAEQAKADAFAEQAGAFAAAJQAAAJgHAGQgHAGgMAAQgPAAgOgMg");
	this.shape_23.setTransform(-234.9,-31.1);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFF000").s().p("AAAAjIAAg3IgNAAIAAgOIAbAAIAABFg");
	this.shape_24.setTransform(-202.5,-47.3);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFF000").s().p("AgUAaQgIgKAAgQQAAgQAIgJQAGgKAOAAQAOAAAIAKQAHAJAAAQQAAAQgHAKQgIAKgOAAQgOAAgGgKgAgKgQQgCAGAAAKQAAAKACAHQAEAGAGAAQAHAAADgGQAEgHAAgKQAAgKgEgGQgDgGgHAAQgGAAgEAGg");
	this.shape_25.setTransform(-207.6,-47.3);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFF000").s().p("AgFAGQgDgCAAgEQAAgDADgDQACgCADAAQAEAAACACQADADAAADQAAAEgDACQgCADgEAAQgDAAgCgDg");
	this.shape_26.setTransform(-215.2,-44.6);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFF000").s().p("AgbAYIAKgMQALAKAJAAQAEAAACgCQADgBAAgEQAAgDgDgCIgJgDQgMgDgGgEQgGgFAAgJQAAgKAIgGQAHgFAJAAQAHAAAHACQAHADAGAEIgIALQgJgHgKAAQgDAAgCACQgDACAAADQAAADADACIAMAEQAKADAFAEQAGAFAAAJQAAAJgHAGQgHAGgMAAQgPAAgOgMg");
	this.shape_27.setTransform(-219.9,-47.3);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFF000").s().p("AgZAaQgLgLAAgPQAAgPALgJQALgLAOAAQAQAAAKALQALAJAAAPQAAAPgLALQgKAKgQAAQgOAAgLgKgAgOgPQgGAHAAAIQAAAKAGAGQAGAGAIABQAJgBAGgGQAGgGAAgKQAAgIgGgHQgGgGgJAAQgIAAgGAGg");
	this.shape_28.setTransform(-227.1,-47.3);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFF000").s().p("AgbAjIAAhFIAZAAQAPAAAHAHQAIAFAAAOQAAAMgIAFQgHAGgPABIgJAAIAAATgAgLACIAKAAQAHAAADgCQADgEAAgFQAAgGgEgDQgDgCgHAAIgJAAg");
	this.shape_29.setTransform(-234.4,-47.3);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFF000").s().p("AgdAEIAAgHIA7AAIAAAHg");
	this.shape_30.setTransform(-270.6,-112.8);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFF000").s().p("AgdAEIAAgHIA7AAIAAAHg");
	this.shape_31.setTransform(-277.8,-112.8);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFF000").s().p("AgdAEIAAgHIA7AAIAAAHg");
	this.shape_32.setTransform(-285,-112.8);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFF000").s().p("AgdAEIAAgHIA7AAIAAAHg");
	this.shape_33.setTransform(-292.2,-112.8);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFF000").s().p("AgdAEIAAgHIA7AAIAAAHg");
	this.shape_34.setTransform(-299.4,-112.8);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFF000").s().p("AgdAEIAAgHIA7AAIAAAHg");
	this.shape_35.setTransform(-306.6,-112.8);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFF000").s().p("AgdAEIAAgHIA7AAIAAAHg");
	this.shape_36.setTransform(-313.8,-112.8);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFF000").s().p("AgdAEIAAgHIA7AAIAAAHg");
	this.shape_37.setTransform(-321,-112.8);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFF000").s().p("AgdAEIAAgHIA7AAIAAAHg");
	this.shape_38.setTransform(-328.2,-112.8);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFF000").s().p("AgdAEIAAgHIA7AAIAAAHg");
	this.shape_39.setTransform(-335.4,-112.8);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFF000").s().p("AgdAEIAAgHIA7AAIAAAHg");
	this.shape_40.setTransform(-342.6,-112.8);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFF000").s().p("AgYAZIAIgMIACACIAGAEQAEACAEAAQADAAADgCQADgDAAgEQAAgFgDgDQgDgCgFAAQgFAAgHADIgGgGIAAghIAoAAIAAANIgaAAIAAAMIAGgBQAKAAAHAGQAIAFAAALQAAAKgIAHQgIAGgJABQgPAAgJgLg");
	this.shape_41.setTransform(-204.7,147.7);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFF000").s().p("AACAjIAAgRIgbAAIAAgLIAZgpIARAAIgXAnIAIAAIAAgMIAQAAIAAAMIAIAAIAAANIgIAAIAAARg");
	this.shape_42.setTransform(-210.6,147.7);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFF000").s().p("AgFAGQgDgCAAgEQAAgCADgEQACgCADAAQAEAAACACQADAEAAACQAAAEgDACQgCADgEAAQgDAAgCgDg");
	this.shape_43.setTransform(-215,150.4);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFF000").s().p("AgYAZIAIgMIACACIAGAEQAEACAEAAQADAAADgCQADgDAAgEQAAgFgDgDQgEgCgEAAQgFAAgHADIgHgGIAAghIApAAIAAANIgaAAIAAAMIAGgBQAKAAAIAGQAHAFAAALQAAAKgHAHQgJAGgJABQgOAAgKgLg");
	this.shape_44.setTransform(-219.4,147.7);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFF000").s().p("AgFAGQgDgCAAgEQAAgCADgEQACgCADAAQAEAAACACQADAEAAACQAAAEgDACQgCADgEAAQgDAAgCgDg");
	this.shape_45.setTransform(-232.9,150.4);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FFF000").s().p("AgfAjIAAhFIAZAAQASAAAKAJQAKAKAAAPQAAAQgKAKQgKAJgTAAgAgPAVIAKAAQAKAAAFgFQAGgGAAgKQAAgJgGgGQgFgFgLAAIgJAAg");
	this.shape_46.setTransform(-238,147.7);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFF000").s().p("AgbAjIAAhFIAZAAQAPAAAHAHQAIAFAAANQAAANgIAFQgHAHgPAAIgJAAIAAATgAgLACIAKAAQAHAAADgCQADgEAAgFQAAgGgEgDQgDgCgHAAIgJAAg");
	this.shape_47.setTransform(-258.2,147.7);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FFF000").s().p("AgFAGQgDgCAAgEQAAgCADgEQACgCADAAQAEAAACACQADAEAAACQAAAEgDACQgCADgEAAQgDAAgCgDg");
	this.shape_48.setTransform(-115.8,134.2);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FFF000").s().p("AgZAaQgLgLAAgPQAAgPALgJQALgLAOAAQAQAAAKALQALAJAAAPQAAAPgLALQgKAKgQAAQgOAAgLgKgAgOgPQgGAHAAAIQAAAJAGAHQAGAGAIAAQAJAAAGgGQAGgHAAgJQAAgIgGgHQgGgGgJgBQgIABgGAGg");
	this.shape_49.setTransform(-127.8,131.4);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FFF000").s().p("AgbAjIAAhFIAZAAQAPAAAHAHQAIAFAAAOQAAAMgIAFQgHAHgPAAIgJAAIAAATgAgLACIAKAAQAHAAADgCQADgEAAgFQAAgGgEgDQgDgCgHAAIgJAAg");
	this.shape_50.setTransform(-135,131.5);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FFF000").s().p("AgcAEIAAgHIA5AAIAAAHg");
	this.shape_51.setTransform(-142.2,135.8);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FFF000").s().p("AgcAEIAAgHIA5AAIAAAHg");
	this.shape_52.setTransform(-149.4,135.8);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FFF000").s().p("AgcAEIAAgHIA5AAIAAAHg");
	this.shape_53.setTransform(-156.6,135.8);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FFF000").s().p("AgcAEIAAgHIA5AAIAAAHg");
	this.shape_54.setTransform(-163.8,135.8);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FFF000").s().p("AgcAEIAAgHIA5AAIAAAHg");
	this.shape_55.setTransform(-171,135.8);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FFF000").s().p("AgcAEIAAgHIA5AAIAAAHg");
	this.shape_56.setTransform(-178.2,135.8);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FFF000").s().p("AgcAEIAAgHIA5AAIAAAHg");
	this.shape_57.setTransform(-185.4,135.8);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#FFF000").s().p("AgcAEIAAgHIA5AAIAAAHg");
	this.shape_58.setTransform(-192.6,135.8);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#FFF000").s().p("AgcAEIAAgHIA5AAIAAAHg");
	this.shape_59.setTransform(-199.8,135.8);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#FFF000").s().p("AgcAEIAAgHIA5AAIAAAHg");
	this.shape_60.setTransform(-207,135.8);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#FFF000").s().p("AgcAEIAAgHIA5AAIAAAHg");
	this.shape_61.setTransform(-214.2,135.8);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#FFF000").s().p("AgcAEIAAgHIA5AAIAAAHg");
	this.shape_62.setTransform(-221.4,135.8);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#FFF000").s().p("AgcAEIAAgHIA5AAIAAAHg");
	this.shape_63.setTransform(-228.6,135.8);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#FFF000").s().p("AgcAEIAAgHIA5AAIAAAHg");
	this.shape_64.setTransform(-235.8,135.8);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#FFF000").s().p("AgcAEIAAgHIA5AAIAAAHg");
	this.shape_65.setTransform(-243,135.8);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#FFF000").s().p("AgcAEIAAgHIA5AAIAAAHg");
	this.shape_66.setTransform(-250.2,135.8);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#FFF000").s().p("AgcAEIAAgHIA5AAIAAAHg");
	this.shape_67.setTransform(-257.4,135.8);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#FFF000").s().p("AgcAEIAAgHIA5AAIAAAHg");
	this.shape_68.setTransform(-264.6,135.8);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#FFF000").s().p("AgYAZIAIgLIADACIAFADQAEACAEAAQADAAADgCQADgDAAgFQAAgEgDgCQgDgDgFAAQgFAAgHAEIgHgGIAAgjIAqAAIAAAOIgbAAIAAAMIAGgBQAKAAAIAGQAHAFAAAKQAAALgHAGQgJAIgKgBQgOAAgJgKg");
	this.shape_69.setTransform(33.4,-143);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#FFF000").s().p("AgLAjIgHgDIgFgEIAIgLIACACIAEACIAGACQAGAAADgFQAEgEABgIQgHAFgGAAQgKAAgGgHQgHgFAAgLQAAgKAHgHQAHgGALAAQAGAAAGADQAFACADAGQAGAJAAAPQAAAJgCAHQgDAHgEAFQgIAIgLAAIgJgBgAgIgTQgCADAAAEQAAAEACADQADADAEAAQAEAAADgCQADgDAAgEQAAgFgDgDQgDgDgDAAQgFAAgDADg");
	this.shape_70.setTransform(27.2,-143.1);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#FFF000").s().p("AgLAjIgHgDIgFgEIAIgLIACACIAEACIAGACQAGAAADgFQAEgEABgIQgHAFgGAAQgKAAgGgHQgHgFAAgLQAAgKAHgHQAHgGALAAQAGAAAGADQAFACADAGQAGAJAAAPQAAAJgCAHQgDAHgEAFQgIAIgLAAIgJgBgAgIgTQgCADAAAEQAAAEACADQADADAEAAQAEAAADgCQADgDAAgEQAAgFgDgDQgDgDgDAAQgFAAgDADg");
	this.shape_71.setTransform(20.9,-143.1);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#FFF000").s().p("AgLAhQgFgDgDgFQgGgKAAgPQAAgIADgHQACgIAEgEQAIgIALAAQAFAAAEABIAHADIAFAEIgHALIgCgCIgFgCIgGgCQgGAAgDAEQgDAFgCAHQAHgEAHAAQAJAAAGAHQAHAFAAALQAAAKgHAHQgHAGgLAAQgGAAgGgDgAgFAFQgDADAAAEQAAAFADADQADADADAAQAFAAADgDQADgDAAgEQAAgFgDgDQgCgDgFAAQgEAAgDADg");
	this.shape_72.setTransform(14.7,-143.1);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#FFF000").s().p("AADAjIAAgQIgbAAIAAgNIAYgoIAQAAIgWAnIAJAAIAAgLIAOAAIAAALIAIAAIAAAOIgIAAIAAAQg");
	this.shape_73.setTransform(8.6,-143.1);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#FFF000").s().p("AAAAjIAAg4IgNAAIAAgNIAbAAIAABFg");
	this.shape_74.setTransform(3.3,-143.1);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#FFF000").s().p("AgFAGQgDgDAAgDQAAgCADgDQACgDADAAQAEAAACADQADADAAACQAAADgDADQgCADgEAAQgDAAgCgDg");
	this.shape_75.setTransform(0.1,-140.4);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#FFF000").s().p("AgTAjIAag4IgQAAIAAAJIgPAAIAAgWIAxAAIAAAMIgbA5g");
	this.shape_76.setTransform(-4.1,-143.1);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#FFF000").s().p("AgSAeQgIgHAAgJQAAgLAJgFQgGgGAAgIQAAgIAGgGQAHgFAKAAQAKAAAHAFQAHAGAAAIQAAAIgGAGQAJAFAAALQAAAJgIAHQgHAGgMAAQgLAAgHgGgAgHAHQgDACAAAFQAAAEADADQAEACADAAQAEAAADgCQAEgDAAgEQAAgFgDgCQgEgCgEAAQgDAAgEACgAgGgVQgDADAAADQAAAEADACQADADADAAQAEAAADgDQACgCAAgEQAAgDgCgDQgDgCgEAAQgDAAgDACg");
	this.shape_77.setTransform(-10.3,-143.1);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("#FFF000").s().p("AACAjIAAgQIgbAAIAAgNIAZgoIARAAIgXAnIAIAAIAAgLIAQAAIAAALIAIAAIAAAOIgIAAIAAAQg");
	this.shape_78.setTransform(-21.6,-143.1);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("#FFF000").s().p("AgXAkIAAgMIAVgVIAHgJQADgEAAgDQAAgEgCgCQgDgCgDAAQgGAAgGAJIgMgHQAFgIAGgEQAFgEAJAAQAJAAAGAGQAHAGAAAKQAAAFgDAFQgCAEgIAIIgLANIAaAAIAAAOg");
	this.shape_79.setTransform(-27.6,-143.2);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f("#FFF000").s().p("AAAAjIAAg4IgNAAIAAgNIAbAAIAABFg");
	this.shape_80.setTransform(-32.9,-143.1);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("#FFF000").s().p("AgXAkIAAgMIAVgVIAHgJQADgEAAgDQAAgEgCgCQgDgCgDAAQgGAAgGAJIgMgHQAFgIAGgEQAFgEAJAAQAJAAAGAGQAHAGAAAKQAAAFgDAFQgCAEgIAIIgLANIAaAAIAAAOg");
	this.shape_81.setTransform(-37.7,-143.2);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f("#FFF000").s().p("AAAAjIAAg4IgNAAIAAgNIAbAAIAABFg");
	this.shape_82.setTransform(-43,-143.1);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("#FFF000").s().p("AgYAZIAIgLIADACIAFADQAEACAEAAQADAAADgCQADgDAAgFQAAgEgDgCQgEgDgEAAQgFAAgHAEIgHgGIAAgjIAqAAIAAAOIgbAAIAAAMIAGgBQAKAAAHAGQAIAFAAAKQAAALgIAGQgHAIgLgBQgOAAgJgKg");
	this.shape_83.setTransform(-47.7,-143);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f("#FFF000").s().p("AgYAZIAIgLIADACIAFADQAEACAEAAQADAAADgCQADgDAAgFQAAgEgDgCQgEgDgEAAQgFAAgHAEIgHgGIAAgjIAqAAIAAAOIgbAAIAAAMIAGgBQAKAAAHAGQAIAFAAAKQAAALgIAGQgHAIgLgBQgNAAgKgKg");
	this.shape_84.setTransform(-53.6,-143);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f("#FFF000").s().p("AgYAZIAIgLIADACIAFADQAEACAEAAQADAAADgCQADgDAAgFQAAgEgDgCQgDgDgFAAQgFAAgHAEIgGgGIAAgjIApAAIAAAOIgbAAIAAAMIAGgBQAKAAAHAGQAIAFAAAKQAAALgIAGQgHAIgLgBQgNAAgKgKg");
	this.shape_85.setTransform(-59.6,-143);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f("#FFF000").s().p("AgFAGQgDgDAAgDQAAgCADgDQACgDADAAQAEAAACADQADADAAACQAAADgDADQgCADgEAAQgDAAgCgDg");
	this.shape_86.setTransform(-64.1,-140.4);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f("#FFF000").s().p("AgSAeQgIgHAAgJQAAgLAJgFQgGgGAAgIQAAgIAGgGQAHgFAKAAQAKAAAHAFQAHAGAAAIQAAAIgGAGQAJAFAAALQAAAJgIAHQgHAGgMAAQgLAAgHgGgAgHAHQgDACAAAFQAAAEADADQAEACADAAQAEAAADgCQAEgDAAgEQAAgFgDgCQgEgCgEAAQgDAAgEACgAgGgVQgDADAAADQAAAEADACQADADADAAQAEAAADgDQACgCAAgEQAAgDgCgDQgDgCgEAAQgDAAgDACg");
	this.shape_87.setTransform(-68.7,-143.1);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f("#FFF000").s().p("AgYAZIAIgLIACACIAGADQAEACAEAAQADAAADgCQADgDAAgFQAAgEgDgCQgEgDgEAAQgFAAgHAEIgHgGIAAgjIApAAIAAAOIgaAAIAAAMIAGgBQAKAAAIAGQAHAFAAAKQAAALgHAGQgJAIgJgBQgPAAgJgKg");
	this.shape_88.setTransform(-74.8,-143);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f("#FFF000").s().p("AgFAGQgDgDAAgDQAAgCADgDQACgDADAAQAEAAACADQADADAAACQAAADgDADQgCADgEAAQgDAAgCgDg");
	this.shape_89.setTransform(-81.9,-140.4);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f("#FFF000").s().p("AgbAYIAKgMQALAKAJAAQAEAAACgCQADgBAAgEQAAgDgDgCIgJgDQgMgDgGgEQgGgFAAgJQAAgKAIgGQAHgFAJAAQAHAAAHACQAHADAGAEIgIALQgJgHgKAAQgDAAgCACQgDACAAADQAAADADACIAMAEQAKADAFAEQAGAFAAAJQAAAJgHAGQgHAGgMAAQgPAAgOgMg");
	this.shape_90.setTransform(-86.6,-143.1);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f("#FFF000").s().p("AgbAjIAAhFIAZAAQAPAAAHAGQAIAGAAANQAAAMgIAHQgHAFgPAAIgJAAIAAAUgAgLACIAKAAQAHAAADgCQADgDAAgHQAAgFgEgDQgDgCgHgBIgJAAg");
	this.shape_91.setTransform(-92.9,-143.1);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f("#FFF000").s().p("AgVAaQgLgKAAgQQAAgOALgLQALgKAOAAQAPAAALAKIgIALQgEgEgEgBQgFgCgEAAQgIAAgGAGQgGAGgBAJQAAAKAHAGQAFAGAHAAQAJAAAFgDIAAgTIAQAAIAAAYQgLAMgSAAQgOAAgLgKg");
	this.shape_92.setTransform(-100.3,-143.1);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.f("#FFF000").s().p("AgRAHIAAgNIAjAAIAAANg");
	this.shape_93.setTransform(-91.7,-158.8);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.f("#FFF000").s().p("AgRAHIAAgNIAjAAIAAANg");
	this.shape_94.setTransform(-96.6,-158.8);

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.f("#FFF000").s().p("AgRAHIAAgNIAjAAIAAANg");
	this.shape_95.setTransform(-101.5,-158.8);

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.f("#FFF000").s().p("AgcAEIAAgHIA5AAIAAAHg");
	this.shape_96.setTransform(-250.2,135.8);

	this.shape_97 = new cjs.Shape();
	this.shape_97.graphics.f("#FFF000").s().p("AgcAEIAAgHIA5AAIAAAHg");
	this.shape_97.setTransform(-257.4,135.8);

	this.shape_98 = new cjs.Shape();
	this.shape_98.graphics.f("#FFF000").s().p("AgcAEIAAgHIA5AAIAAAHg");
	this.shape_98.setTransform(-264.6,135.8);

	this.shape_99 = new cjs.Shape();
	this.shape_99.graphics.f("#FFF000").s().p("AgdAEIAAgHIA6AAIAAAHg");
	this.shape_99.setTransform(-18.2,136.4);

	this.shape_100 = new cjs.Shape();
	this.shape_100.graphics.f("#FFF000").s().p("AgdAEIAAgHIA6AAIAAAHg");
	this.shape_100.setTransform(-25.4,136.4);

	this.shape_101 = new cjs.Shape();
	this.shape_101.graphics.f("#FFF000").s().p("AgdAEIAAgHIA6AAIAAAHg");
	this.shape_101.setTransform(-32.6,136.4);

	this.shape_102 = new cjs.Shape();
	this.shape_102.graphics.f("#FFF000").s().p("AgdAEIAAgHIA6AAIAAAHg");
	this.shape_102.setTransform(39.4,90.2);

	this.shape_103 = new cjs.Shape();
	this.shape_103.graphics.f("#FFF000").s().p("AgdAEIAAgHIA6AAIAAAHg");
	this.shape_103.setTransform(32.2,90.2);

	this.shape_104 = new cjs.Shape();
	this.shape_104.graphics.f("#FFF000").s().p("AgdAEIAAgHIA6AAIAAAHg");
	this.shape_104.setTransform(25,90.2);

	this.shape_105 = new cjs.Shape();
	this.shape_105.graphics.f("#FFF000").s().p("AgdAEIAAgHIA6AAIAAAHg");
	this.shape_105.setTransform(17.8,90.2);

	this.shape_106 = new cjs.Shape();
	this.shape_106.graphics.f("#FFF000").s().p("AgdAEIAAgHIA6AAIAAAHg");
	this.shape_106.setTransform(10.6,90.2);

	this.shape_107 = new cjs.Shape();
	this.shape_107.graphics.f("#FFF000").s().p("AgdAEIAAgHIA6AAIAAAHg");
	this.shape_107.setTransform(3.4,90.2);

	this.shape_108 = new cjs.Shape();
	this.shape_108.graphics.f("#FFF000").s().p("AgdAEIAAgHIA6AAIAAAHg");
	this.shape_108.setTransform(-3.8,90.2);

	this.shape_109 = new cjs.Shape();
	this.shape_109.graphics.f("#FFF000").s().p("AgdAEIAAgHIA6AAIAAAHg");
	this.shape_109.setTransform(-11,90.2);

	this.shape_110 = new cjs.Shape();
	this.shape_110.graphics.f("#FFF000").s().p("AgdAEIAAgHIA6AAIAAAHg");
	this.shape_110.setTransform(-18.2,90.2);

	this.shape_111 = new cjs.Shape();
	this.shape_111.graphics.f("#FFF000").s().p("AgdAEIAAgHIA6AAIAAAHg");
	this.shape_111.setTransform(-25.4,90.2);

	this.shape_112 = new cjs.Shape();
	this.shape_112.graphics.f("#FFF000").s().p("AgdAEIAAgHIA6AAIAAAHg");
	this.shape_112.setTransform(-32.6,90.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_29},{t:this.shape_28},{t:this.shape_27,p:{x:-219.9,y:-47.3}},{t:this.shape_26},{t:this.shape_25,p:{x:-207.6,y:-47.3}},{t:this.shape_24,p:{x:-202.5,y:-47.3}},{t:this.shape_23,p:{x:-234.9,y:-31.1}},{t:this.shape_22},{t:this.shape_21,p:{x:-221.4,y:-31.1}},{t:this.shape_20,p:{x:-214.9,y:-31.1}},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17,p:{x:-195.3,y:-31.1}},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12,p:{x:-168.3,y:-31.1}},{t:this.shape_11,p:{x:-161.2,y:-31.1}},{t:this.shape_10,p:{x:-153.2,y:-31.1}},{t:this.shape_9,p:{x:-145.4,y:-31.1}},{t:this.shape_8,p:{x:-138.5,y:-31.1}}]},33).to({state:[{t:this.shape_95},{t:this.shape_94},{t:this.shape_93},{t:this.shape_92},{t:this.shape_91},{t:this.shape_90},{t:this.shape_89},{t:this.shape_88},{t:this.shape_87},{t:this.shape_86},{t:this.shape_85},{t:this.shape_84},{t:this.shape_83},{t:this.shape_82},{t:this.shape_81},{t:this.shape_80},{t:this.shape_79},{t:this.shape_78},{t:this.shape_77},{t:this.shape_76},{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68,p:{x:-264.6}},{t:this.shape_67,p:{x:-257.4}},{t:this.shape_66,p:{x:-250.2}},{t:this.shape_65,p:{x:-243}},{t:this.shape_64,p:{x:-235.8}},{t:this.shape_63,p:{x:-228.6}},{t:this.shape_62,p:{x:-221.4}},{t:this.shape_61,p:{x:-214.2,y:135.8}},{t:this.shape_60,p:{x:-207,y:135.8}},{t:this.shape_59,p:{x:-199.8,y:135.8}},{t:this.shape_58,p:{x:-192.6,y:135.8}},{t:this.shape_57,p:{x:-185.4,y:135.8}},{t:this.shape_56,p:{x:-178.2,y:135.8}},{t:this.shape_55,p:{x:-171,y:135.8}},{t:this.shape_54,p:{x:-163.8,y:135.8}},{t:this.shape_53,p:{x:-156.6,y:135.8}},{t:this.shape_52,p:{x:-149.4,y:135.8}},{t:this.shape_51,p:{x:-142.2,y:135.8}},{t:this.shape_50},{t:this.shape_49},{t:this.shape_27,p:{x:-120.5,y:131.4}},{t:this.shape_48},{t:this.shape_25,p:{x:-108.3,y:131.4}},{t:this.shape_24,p:{x:-103.1,y:131.5}},{t:this.shape_23,p:{x:-265.1,y:147.6}},{t:this.shape_47},{t:this.shape_21,p:{x:-251.7,y:147.7}},{t:this.shape_20,p:{x:-245.1,y:147.7}},{t:this.shape_46},{t:this.shape_45},{t:this.shape_17,p:{x:-225.6,y:147.6}},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_12,p:{x:-198.5,y:147.6}},{t:this.shape_11,p:{x:-191.4,y:147.7}},{t:this.shape_10,p:{x:-183.5,y:147.7}},{t:this.shape_9,p:{x:-175.6,y:147.7}},{t:this.shape_8,p:{x:-168.7,y:147.7}},{t:this.shape_40,p:{y:-112.8}},{t:this.shape_39,p:{y:-112.8}},{t:this.shape_38,p:{y:-112.8}},{t:this.shape_37,p:{y:-112.8}},{t:this.shape_36,p:{y:-112.8}},{t:this.shape_35,p:{y:-112.8}},{t:this.shape_34,p:{y:-112.8}},{t:this.shape_33,p:{y:-112.8}},{t:this.shape_32,p:{y:-112.8}},{t:this.shape_31,p:{y:-112.8}},{t:this.shape_30,p:{y:-112.8}}]},10).to({state:[{t:this.shape_95},{t:this.shape_94},{t:this.shape_93},{t:this.shape_92},{t:this.shape_91},{t:this.shape_90},{t:this.shape_89},{t:this.shape_88},{t:this.shape_87},{t:this.shape_86},{t:this.shape_85},{t:this.shape_84},{t:this.shape_83},{t:this.shape_82},{t:this.shape_81},{t:this.shape_80},{t:this.shape_79},{t:this.shape_78},{t:this.shape_77},{t:this.shape_76},{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_98},{t:this.shape_97},{t:this.shape_96},{t:this.shape_68,p:{x:-243}},{t:this.shape_67,p:{x:-235.8}},{t:this.shape_66,p:{x:-228.6}},{t:this.shape_65,p:{x:-221.4}},{t:this.shape_64,p:{x:-214.2}},{t:this.shape_63,p:{x:-207}},{t:this.shape_62,p:{x:-199.8}},{t:this.shape_61,p:{x:-192.6,y:135.8}},{t:this.shape_60,p:{x:-185.4,y:135.8}},{t:this.shape_59,p:{x:-178.2,y:135.8}},{t:this.shape_58,p:{x:-171,y:135.8}},{t:this.shape_57,p:{x:-163.8,y:135.8}},{t:this.shape_56,p:{x:-156.6,y:135.8}},{t:this.shape_55,p:{x:-149.4,y:135.8}},{t:this.shape_54,p:{x:-142.2,y:135.8}},{t:this.shape_50},{t:this.shape_49},{t:this.shape_27,p:{x:-120.5,y:131.4}},{t:this.shape_48},{t:this.shape_25,p:{x:-108.3,y:131.4}},{t:this.shape_24,p:{x:-103.1,y:131.5}},{t:this.shape_53,p:{x:-86.8,y:135.8}},{t:this.shape_52,p:{x:-79.6,y:135.8}},{t:this.shape_51,p:{x:-72.4,y:135.8}},{t:this.shape_23,p:{x:-265.1,y:147.6}},{t:this.shape_47},{t:this.shape_21,p:{x:-251.7,y:147.7}},{t:this.shape_20,p:{x:-245.1,y:147.7}},{t:this.shape_46},{t:this.shape_45},{t:this.shape_17,p:{x:-225.6,y:147.6}},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_12,p:{x:-198.5,y:147.6}},{t:this.shape_11,p:{x:-191.4,y:147.7}},{t:this.shape_10,p:{x:-183.5,y:147.7}},{t:this.shape_9,p:{x:-175.6,y:147.7}},{t:this.shape_8,p:{x:-168.7,y:147.7}},{t:this.shape_40,p:{y:87.2}},{t:this.shape_39,p:{y:87.2}},{t:this.shape_38,p:{y:87.2}},{t:this.shape_37,p:{y:87.2}},{t:this.shape_36,p:{y:87.2}},{t:this.shape_35,p:{y:87.2}},{t:this.shape_34,p:{y:87.2}},{t:this.shape_33,p:{y:87.2}},{t:this.shape_32,p:{y:87.2}},{t:this.shape_31,p:{y:87.2}},{t:this.shape_30,p:{y:87.2}}]},8).to({state:[{t:this.shape_61,p:{x:-301.6,y:-65.8}},{t:this.shape_60,p:{x:-294.4,y:-65.8}},{t:this.shape_59,p:{x:-287.2,y:-65.8}},{t:this.shape_58,p:{x:-280,y:-65.8}},{t:this.shape_57,p:{x:-272.8,y:-65.8}},{t:this.shape_56,p:{x:-265.6,y:-65.8}},{t:this.shape_55,p:{x:-258.4,y:-65.8}},{t:this.shape_54,p:{x:-251.2,y:-65.8}},{t:this.shape_53,p:{x:-244,y:-65.8}},{t:this.shape_52,p:{x:-236.8,y:-65.8}},{t:this.shape_51,p:{x:-229.6,y:-65.8}},{t:this.shape_112},{t:this.shape_111},{t:this.shape_110},{t:this.shape_109},{t:this.shape_108},{t:this.shape_107},{t:this.shape_106},{t:this.shape_105},{t:this.shape_104},{t:this.shape_103},{t:this.shape_102},{t:this.shape_101},{t:this.shape_100},{t:this.shape_99}]},17).to({state:[]},8).wait(45));

	// DECO 2
	this.shape_113 = new cjs.Shape();
	this.shape_113.graphics.f("#FFF000").s().p("AgdAEIAAgHIA7AAIAAAHg");
	this.shape_113.setTransform(50.8,-22.6);

	this.shape_114 = new cjs.Shape();
	this.shape_114.graphics.f("#FFF000").s().p("AgdAEIAAgHIA7AAIAAAHg");
	this.shape_114.setTransform(43.6,-22.6);

	this.shape_115 = new cjs.Shape();
	this.shape_115.graphics.f("#FFF000").s().p("AgdAEIAAgHIA7AAIAAAHg");
	this.shape_115.setTransform(36.4,-22.6);

	this.shape_116 = new cjs.Shape();
	this.shape_116.graphics.f("#FFF000").s().p("AgdAEIAAgHIA7AAIAAAHg");
	this.shape_116.setTransform(-434.6,173.2);

	this.shape_117 = new cjs.Shape();
	this.shape_117.graphics.f("#FFF000").s().p("AgdAEIAAgHIA7AAIAAAHg");
	this.shape_117.setTransform(-441.8,173.2);

	this.shape_118 = new cjs.Shape();
	this.shape_118.graphics.f("#FFF000").s().p("AgdAEIAAgHIA7AAIAAAHg");
	this.shape_118.setTransform(-449,173.2);

	this.shape_119 = new cjs.Shape();
	this.shape_119.graphics.f("#FFF000").s().p("AgdAEIAAgHIA7AAIAAAHg");
	this.shape_119.setTransform(-456.2,173.2);

	this.shape_120 = new cjs.Shape();
	this.shape_120.graphics.f("#FFF000").s().p("AgdAEIAAgHIA7AAIAAAHg");
	this.shape_120.setTransform(-463.4,173.2);

	this.shape_121 = new cjs.Shape();
	this.shape_121.graphics.f("#FFF000").s().p("AgdAEIAAgHIA7AAIAAAHg");
	this.shape_121.setTransform(-470.6,173.2);

	this.shape_122 = new cjs.Shape();
	this.shape_122.graphics.f("#FFF000").s().p("AgdAEIAAgHIA7AAIAAAHg");
	this.shape_122.setTransform(-477.8,173.2);

	this.shape_123 = new cjs.Shape();
	this.shape_123.graphics.f("#FFF000").s().p("AgdAEIAAgHIA7AAIAAAHg");
	this.shape_123.setTransform(-485,173.2);

	this.shape_124 = new cjs.Shape();
	this.shape_124.graphics.f("#FFF000").s().p("AgdAEIAAgHIA7AAIAAAHg");
	this.shape_124.setTransform(-492.2,173.2);

	this.shape_125 = new cjs.Shape();
	this.shape_125.graphics.f("#FFF000").s().p("AgdAEIAAgHIA7AAIAAAHg");
	this.shape_125.setTransform(-499.4,173.2);

	this.shape_126 = new cjs.Shape();
	this.shape_126.graphics.f("#FFF000").s().p("AgdAEIAAgHIA7AAIAAAHg");
	this.shape_126.setTransform(-506.6,173.2);

	this.shape_127 = new cjs.Shape();
	this.shape_127.graphics.f("#FFF000").s().p("AAHATIAAgPIgOAAIAAAPIgIAAIAAglIAIAAIAAAQIAOAAIAAgQIAJAAIAAAlg");
	this.shape_127.setTransform(-169.5,25.3);

	this.shape_128 = new cjs.Shape();
	this.shape_128.graphics.f("#FFF000").s().p("AgPAaIAXgzIAIAAIgXAzg");
	this.shape_128.setTransform(-173.4,25.3);

	this.shape_129 = new cjs.Shape();
	this.shape_129.graphics.f("#FFF000").s().p("AANATIAAgXIgKAUIgFAAIgKgUIAAAXIgJAAIAAglIAMAAIAJAUIAKgUIAMAAIAAAlg");
	this.shape_129.setTransform(-177.8,25.3);

	this.shape_130 = new cjs.Shape();
	this.shape_130.graphics.f("#FFF000").s().p("AAHATIgLgPIgEAFIAAAKIgIAAIAAglIAIAAIAAAQIAPgQIAKAAIgQARIAIAJIAIALg");
	this.shape_130.setTransform(-182.3,25.3);

	this.shape_131 = new cjs.Shape();
	this.shape_131.graphics.f("#FFF000").s().p("AgKARQgEgEAAgFQAAgGAFgDQgDgDAAgFQAAgEADgDQAEgDAFAAQAGAAADADQAEADABAEQAAAFgEADQAFADAAAGQAAAFgEAEQgFADgGAAQgFAAgFgDgAgDAEQgBAAAAABQAAAAgBABQAAAAAAABQAAAAAAABQAAAAAAABQAAAAAAABQABAAAAABQAAAAABABIADABIAEgBQAAgBABAAQAAgBAAAAQABgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQAAgBAAAAIgFgBIgDABgAgDgLQAAAAgBABQAAAAAAAAQAAABAAAAQgBABAAAAQAAABABAAQAAABAAAAQAAAAAAABQABAAAAAAIADACQABAAAAAAQAAAAABgBQAAAAABAAQAAAAAAgBQABAAAAAAQABgBAAAAQAAAAAAgBQABAAAAgBQAAAAgBgBQAAAAAAgBQAAAAgBAAQAAgBgBAAQAAgBAAAAQgBAAAAAAQgBgBAAAAQAAAAgBAAIgDACg");
	this.shape_131.setTransform(-186.2,25.2);

	this.shape_132 = new cjs.Shape();
	this.shape_132.graphics.f("#FFF000").s().p("AgNAOIAFgHIABACIAEACIADABQAAAAABAAQAAAAABgBQAAAAABAAQAAAAABgBQAAAAAAAAQABgBAAAAQAAgBAAAAQABgBAAgBQAAAAgBgBQAAAAAAgBQAAgBgBAAQAAAAAAgBQgDgBgCAAQgCAAgEACIgEgDIAAgTIAXAAIAAAIIgOAAIAAAGIACAAQAGAAAEADQAEADAAAFQAAAGgEAEQgFAEgFAAQgHAAgGgGg");
	this.shape_132.setTransform(-189.7,25.3);

	this.shape_133 = new cjs.Shape();
	this.shape_133.graphics.f("#FFF000").s().p("AABATIAAgJIgOAAIAAgHIANgVIAJAAIgMAUIAEAAIAAgFIAJAAIAAAFIAEAAIAAAIIgEAAIAAAJg");
	this.shape_133.setTransform(-193,25.3);

	this.shape_134 = new cjs.Shape();
	this.shape_134.graphics.f("#FFF000").s().p("AgCADQgBAAAAAAQAAgBgBAAQAAgBAAAAQAAgBAAAAQAAAAAAAAQAAAAAAgBQABAAAAgBQAAAAABgBQAAAAAAAAQABAAAAgBQABAAAAAAQAAAAAAAAQABAAAAAAQAAAAABAAQAAABABAAQAAAAABAAQAAABAAAAQAAABABAAQAAABAAAAQAAAAAAAAQAAAAAAABQAAAAAAABQgBAAAAABQAAAAAAAAQgBABAAAAQgBAAAAABQgBAAAAAAQAAAAgBAAQAAAAAAAAQAAAAgBAAQAAgBgBAAQAAAAAAgBg");
	this.shape_134.setTransform(-195.5,26.8);

	this.shape_135 = new cjs.Shape();
	this.shape_135.graphics.f("#FFF000").s().p("AgNAOIAFgHIABACIAEACIADABQAAAAABAAQAAAAABgBQAAAAABAAQAAAAAAgBQABAAAAAAQABgBAAAAQAAgBAAAAQABgBAAgBQAAAAgBgBQAAAAAAgBQAAgBgBAAQAAAAgBgBQgCgBgBAAQgDAAgEACIgEgDIAAgTIAXAAIAAAIIgOAAIAAAGIACAAQAGAAAEADQAEADAAAFQAAAGgEAEQgFAEgFAAQgIAAgFgGg");
	this.shape_135.setTransform(-197.9,25.3);

	this.shape_136 = new cjs.Shape();
	this.shape_136.graphics.f("#FFF000").s().p("AgKARQgEgEAAgFQAAgGAFgDQgDgDgBgFQAAgEAFgDQADgDAFAAQAGAAAEADQADADAAAEQABAFgEADQAFADAAAGQAAAFgEAEQgEADgHAAQgGAAgEgDgAgEAEQAAAAAAABQAAAAgBABQAAAAAAABQAAAAAAABQAAAAAAABQAAAAAAABQABAAAAABQAAAAAAABIAEABIAEgBQABgBAAAAQAAgBABAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQAAgBgBAAIgEgBIgEABgAgDgLQAAAAgBABQAAAAAAAAQAAABAAAAQgBABAAAAQAAABABAAQAAABAAAAQAAAAAAABQABAAAAAAIADACQAAAAABAAQAAAAABgBQAAAAABAAQAAAAAAgBQABAAAAAAQABgBAAAAQAAAAAAgBQAAAAAAgBQAAAAAAgBQAAAAAAgBQAAAAgBAAQAAgBgBAAQAAgBAAAAQgBAAAAAAQgBgBAAAAQgBAAAAAAIgDACg");
	this.shape_136.setTransform(-201.4,25.2);

	this.shape_137 = new cjs.Shape();
	this.shape_137.graphics.f("#FFF000").s().p("AgCADQgBAAAAAAQAAgBgBAAQAAgBAAAAQAAgBAAAAQAAAAAAAAQAAAAAAgBQABAAAAgBQAAAAABgBQAAAAAAAAQABAAAAgBQABAAAAAAQAAAAAAAAQABAAAAAAQAAAAABAAQAAABABAAQAAAAABAAQAAABAAAAQAAABABAAQAAABAAAAQAAAAAAAAQAAAAAAABQAAAAAAABQgBAAAAABQAAAAAAAAQgBABAAAAQgBAAAAABQgBAAAAAAQAAAAgBAAQAAAAAAAAQAAAAgBAAQAAgBgBAAQAAAAAAgBg");
	this.shape_137.setTransform(-205.5,26.8);

	this.shape_138 = new cjs.Shape();
	this.shape_138.graphics.f("#FFF000").s().p("AgRATIAAglIAOAAQAJAAAGAEQAGAFAAAJQAAAJgGAFQgFAGgLgBgAgIAMIAFAAQAFAAAEgDQADgEAAgFQAAgFgDgDQgEgDgFAAIgFAAg");
	this.shape_138.setTransform(-208.3,25.3);

	this.shape_139 = new cjs.Shape();
	this.shape_139.graphics.f("#FFF000").s().p("AgNATIAAglIAbAAIAAAHIgTAAIAAAIIARAAIAAAGIgRAAIAAAJIATAAIAAAHg");
	this.shape_139.setTransform(-212.3,25.3);

	this.shape_140 = new cjs.Shape();
	this.shape_140.graphics.f("#FFF000").s().p("AgNATIAAglIAbAAIAAAHIgTAAIAAAIIARAAIAAAGIgRAAIAAAJIATAAIAAAHg");
	this.shape_140.setTransform(-216,25.3);

	this.shape_141 = new cjs.Shape();
	this.shape_141.graphics.f("#FFF000").s().p("AgPATIAAglIAOAAQAIAAAEADQAFAEAAAGQAAAHgFADQgEADgHABIgGAAIAAAKgAgGABIAGAAQAEAAABgBQACgCAAgDQAAgBgBgBQAAAAAAgBQAAAAgBgBQAAAAgBAAQgBgCgEAAIgFAAg");
	this.shape_141.setTransform(-219.6,25.3);

	this.shape_142 = new cjs.Shape();
	this.shape_142.graphics.f("#FFF000").s().p("AgOANIAEgGQAHAFAFAAIADgBQAAAAABAAQAAgBAAAAQAAAAAAgBQAAAAAAAAQAAgBAAAAQAAgBAAAAQAAAAAAgBQgBAAAAAAIgFgCQgGgCgEgCQgDgCAAgGQAAgFAEgDQAEgDAFAAIAIABQAEABADADIgEAGQgFgEgGAAIgDABQAAAAAAABQAAAAgBAAQAAABAAAAQAAABAAAAQAAABAAAAQAAAAAAABQABAAAAAAQAAAAAAABIAHACQAGACACABQADADAAAFQAAAGgDADQgEADgGAAQgJAAgHgHg");
	this.shape_142.setTransform(-223.5,25.2);

	this.shape_143 = new cjs.Shape();
	this.shape_143.graphics.f("#FFF000").s().p("AgPACIAAgEIAfAAIAAAEg");
	this.shape_143.setTransform(-115.6,18.7);

	this.shape_144 = new cjs.Shape();
	this.shape_144.graphics.f("#FFF000").s().p("AgPACIAAgEIAfAAIAAAEg");
	this.shape_144.setTransform(-119.7,18.7);

	this.shape_145 = new cjs.Shape();
	this.shape_145.graphics.f("#FFF000").s().p("AgQACIAAgEIAhAAIAAAEg");
	this.shape_145.setTransform(-123.7,18.7);

	this.shape_146 = new cjs.Shape();
	this.shape_146.graphics.f("#FFF000").s().p("AAAAUIAAgfIgHAAIAAgIIAPAAIAAAng");
	this.shape_146.setTransform(-132.8,16.2);

	this.shape_147 = new cjs.Shape();
	this.shape_147.graphics.f("#FFF000").s().p("AgLAPQgEgGAAgJQAAgIAEgGQAEgFAHAAQAIAAAEAFQAEAGAAAIQAAAJgEAGQgEAFgIAAQgHAAgEgFgAgFgIQgCADAAAFQAAAGACADQACAEADAAQAEAAACgEQACgDAAgGQAAgFgCgDQgCgEgEAAQgDAAgCAEg");
	this.shape_147.setTransform(-135.7,16.2);

	this.shape_148 = new cjs.Shape();
	this.shape_148.graphics.f("#FFF000").s().p("AgDAEQAAgBAAAAQAAgBgBAAQAAgBAAAAQAAAAAAgBQAAAAAAAAQAAAAAAgBQABAAAAgBQAAAAAAAAQABgBAAAAQABAAAAgBQABAAAAAAQAAAAAAAAQAAAAABAAQAAAAABAAQAAABABAAQAAAAABABQAAAAAAAAQAAABABAAQAAABAAAAQAAAAAAAAQAAABAAAAQAAAAAAABQgBAAAAABQAAAAAAABQgBAAAAAAQgBAAAAABQgBAAAAAAQgBAAAAAAQAAAAAAAAQAAAAgBAAQAAgBgBAAQAAAAgBAAg");
	this.shape_148.setTransform(-139.9,17.7);

	this.shape_149 = new cjs.Shape();
	this.shape_149.graphics.f("#FFF000").s().p("AgPANIAGgGQAGAFAEAAIAEgBQAAAAABAAQAAgBAAAAQAAAAAAgBQABAAAAAAQAAgBgBAAQAAgBAAAAQAAAAAAgBQgBAAAAAAIgFgCQgHgCgCgCQgEgCAAgGQAAgFAEgDQAEgDAFAAIAIABQAEABADADIgFAGQgEgEgGAAIgCABQgBABAAAAQAAAAgBABQAAAAAAAAQAAABAAAAQAAABAAAAQAAAAAAABQABAAAAAAQAAABABAAIAGACQAFACADABQAEADAAAFQAAAGgFADQgDADgHAAQgIAAgIgHg");
	this.shape_149.setTransform(-142.6,16.2);

	this.shape_150 = new cjs.Shape();
	this.shape_150.graphics.f("#FFF000").s().p("AgOAOQgFgFAAgJQAAgIAFgFQAHgGAHAAQAJAAAGAGQAFAFAAAIQAAAJgFAFQgGAGgJAAQgHAAgHgGgAgHgIQgEAEABAEQgBAFAEAEQADAEAEAAQAFAAADgEQAEgEAAgFQAAgEgEgEQgDgEgFAAQgEAAgDAEg");
	this.shape_150.setTransform(-146.6,16.2);

	this.shape_151 = new cjs.Shape();
	this.shape_151.graphics.f("#FFF000").s().p("AgPAUIAAgnIAOAAQAIAAAEAEQAFADAAAIQgBAGgEADQgEADgHAAIgGAAIAAAMgAgGABIAGAAQAEAAABgBQABgBABgEQAAgBgBgBQAAAAAAgBQAAAAgBgBQAAAAAAgBQgDgBgDAAIgFAAg");
	this.shape_151.setTransform(-150.7,16.2);

	this.shape_152 = new cjs.Shape();
	this.shape_152.graphics.f("#FFF000").s().p("AgQACIAAgEIAhAAIAAAEg");
	this.shape_152.setTransform(-154.7,18.7);

	this.shape_153 = new cjs.Shape();
	this.shape_153.graphics.f("#FFF000").s().p("AgQACIAAgEIAhAAIAAAEg");
	this.shape_153.setTransform(-158.8,18.7);

	this.shape_154 = new cjs.Shape();
	this.shape_154.graphics.f("#FFF000").s().p("AgPACIAAgEIAfAAIAAAEg");
	this.shape_154.setTransform(-162.8,18.7);

	this.shape_155 = new cjs.Shape();
	this.shape_155.graphics.f("#FFF000").s().p("AgPACIAAgEIAfAAIAAAEg");
	this.shape_155.setTransform(-166.8,18.7);

	this.shape_156 = new cjs.Shape();
	this.shape_156.graphics.f("#FFF000").s().p("AgPACIAAgEIAfAAIAAAEg");
	this.shape_156.setTransform(-170.9,18.7);

	this.shape_157 = new cjs.Shape();
	this.shape_157.graphics.f("#FFF000").s().p("AgQACIAAgEIAgAAIAAAEg");
	this.shape_157.setTransform(-174.9,18.7);

	this.shape_158 = new cjs.Shape();
	this.shape_158.graphics.f("#FFF000").s().p("AgQACIAAgEIAgAAIAAAEg");
	this.shape_158.setTransform(-178.9,18.7);

	this.shape_159 = new cjs.Shape();
	this.shape_159.graphics.f("#FFF000").s().p("AgQACIAAgEIAgAAIAAAEg");
	this.shape_159.setTransform(-183,18.7);

	this.shape_160 = new cjs.Shape();
	this.shape_160.graphics.f("#FFF000").s().p("AgPACIAAgEIAgAAIAAAEg");
	this.shape_160.setTransform(-187,18.7);

	this.shape_161 = new cjs.Shape();
	this.shape_161.graphics.f("#FFF000").s().p("AgPACIAAgEIAgAAIAAAEg");
	this.shape_161.setTransform(-191,18.7);

	this.shape_162 = new cjs.Shape();
	this.shape_162.graphics.f("#FFF000").s().p("AgPACIAAgEIAgAAIAAAEg");
	this.shape_162.setTransform(-195.1,18.7);

	this.shape_163 = new cjs.Shape();
	this.shape_163.graphics.f("#FFF000").s().p("AgQACIAAgEIAgAAIAAAEg");
	this.shape_163.setTransform(-199.1,18.7);

	this.shape_164 = new cjs.Shape();
	this.shape_164.graphics.f("#FFF000").s().p("AgQACIAAgEIAgAAIAAAEg");
	this.shape_164.setTransform(-203.1,18.7);

	this.shape_165 = new cjs.Shape();
	this.shape_165.graphics.f("#FFF000").s().p("AgQACIAAgEIAhAAIAAAEg");
	this.shape_165.setTransform(-207.1,18.7);

	this.shape_166 = new cjs.Shape();
	this.shape_166.graphics.f("#FFF000").s().p("AgQACIAAgEIAhAAIAAAEg");
	this.shape_166.setTransform(-211.2,18.7);

	this.shape_167 = new cjs.Shape();
	this.shape_167.graphics.f("#FFF000").s().p("AgQACIAAgEIAhAAIAAAEg");
	this.shape_167.setTransform(-215.2,18.7);

	this.shape_168 = new cjs.Shape();
	this.shape_168.graphics.f("#FFF000").s().p("AgPACIAAgEIAfAAIAAAEg");
	this.shape_168.setTransform(-219.2,18.7);

	this.shape_169 = new cjs.Shape();
	this.shape_169.graphics.f("#FFF000").s().p("AgPACIAAgEIAfAAIAAAEg");
	this.shape_169.setTransform(-223.3,18.7);

	this.shape_170 = new cjs.Shape();
	this.shape_170.graphics.f("#FFF000").s().p("AgcAEIAAgHIA5AAIAAAHg");
	this.shape_170.setTransform(-69.2,-152.6);

	this.shape_171 = new cjs.Shape();
	this.shape_171.graphics.f("#FFF000").s().p("AgcAEIAAgHIA5AAIAAAHg");
	this.shape_171.setTransform(-76.4,-152.6);

	this.shape_172 = new cjs.Shape();
	this.shape_172.graphics.f("#FFF000").s().p("AgcAEIAAgHIA5AAIAAAHg");
	this.shape_172.setTransform(-83.6,-152.6);

	this.shape_173 = new cjs.Shape();
	this.shape_173.graphics.f().s("#FFFFFF").ss(1,1,1).p("AW3ISI4mONI1HzBILk58IcQC+g");
	this.shape_173.setTransform(-22.7,68.8);

	this.shape_174 = new cjs.Shape();
	this.shape_174.graphics.f().s("#FFFFFF").ss(1,1,1).p("AnFmSICWm8ISbU+I7XFfIAAgB");
	this.shape_174.setTransform(-240,-49.6);

	this.shape_175 = new cjs.Shape();
	this.shape_175.graphics.f("#FFFFFF").s().p("AqGJxIGnzhIAAAAINmPeI0MEDg");
	this.shape_175.setTransform(-262.9,-27.5);

	this.shape_176 = new cjs.Shape();
	this.shape_176.graphics.f().s("#FFFFFF").ss(1,1,1).p("ALnNZI3NtWIXJtbg");
	this.shape_176.setTransform(-577,114.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_126},{t:this.shape_125},{t:this.shape_124},{t:this.shape_123,p:{x:-485}},{t:this.shape_122,p:{x:-477.8}},{t:this.shape_121,p:{x:-470.6}},{t:this.shape_120,p:{x:-463.4}},{t:this.shape_119,p:{x:-456.2}},{t:this.shape_118,p:{x:-449}},{t:this.shape_117,p:{x:-441.8}},{t:this.shape_116,p:{x:-434.6}},{t:this.shape_115,p:{x:36.4,y:-22.6}},{t:this.shape_114,p:{x:43.6,y:-22.6}},{t:this.shape_113,p:{x:50.8,y:-22.6}}]},59).to({state:[]},6).to({state:[{t:this.shape_123,p:{x:-506.6}},{t:this.shape_122,p:{x:-499.4}},{t:this.shape_121,p:{x:-492.2}},{t:this.shape_120,p:{x:-485}},{t:this.shape_119,p:{x:-477.8}},{t:this.shape_118,p:{x:-470.6}},{t:this.shape_117,p:{x:-463.4}},{t:this.shape_116,p:{x:-456.2}},{t:this.shape_115,p:{x:-449,y:173.2}},{t:this.shape_114,p:{x:-441.8,y:173.2}},{t:this.shape_113,p:{x:-434.6,y:173.2}},{t:this.shape_172},{t:this.shape_171},{t:this.shape_170},{t:this.shape_169},{t:this.shape_168},{t:this.shape_167},{t:this.shape_166},{t:this.shape_165},{t:this.shape_164},{t:this.shape_163},{t:this.shape_162},{t:this.shape_161},{t:this.shape_160},{t:this.shape_159},{t:this.shape_158},{t:this.shape_157},{t:this.shape_156},{t:this.shape_155},{t:this.shape_154},{t:this.shape_153},{t:this.shape_152},{t:this.shape_151},{t:this.shape_150},{t:this.shape_149},{t:this.shape_148},{t:this.shape_147},{t:this.shape_146},{t:this.shape_145},{t:this.shape_144},{t:this.shape_143},{t:this.shape_142},{t:this.shape_141},{t:this.shape_140},{t:this.shape_139},{t:this.shape_138},{t:this.shape_137},{t:this.shape_136},{t:this.shape_135},{t:this.shape_134},{t:this.shape_133},{t:this.shape_132},{t:this.shape_131},{t:this.shape_130},{t:this.shape_129},{t:this.shape_128},{t:this.shape_127}]},11).to({state:[{t:this.shape_173}]},17).to({state:[{t:this.shape_175},{t:this.shape_174}]},4).to({state:[{t:this.shape_176}]},3).to({state:[]},6).to({state:[]},1).wait(14));

	// masque Titre (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_17 = new cjs.Graphics().p("EgpOArJMAAAhWRMAvJAAAMAAABWRg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(17).to({graphics:mask_graphics_17,x:-263.9,y:-6.1}).wait(24).to({graphics:null,x:0,y:0}).wait(80));

	// _LETTRES_ANARCHIQUES
	this.instance_2 = new lib.lettres_FAT();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-433.8,-73,1,1,0,0,0,0,186.8);
	this.instance_2._off = true;

	this.shape_177 = new cjs.Shape();
	this.shape_177.graphics.f("#FFF000").s().p("AjeEDQhohlgBicQABibBqhnQBqhpCaAAQCrAAB1CEIhiBuQhJhchuAAQhYgBg+A6Qg+A6gBBgQAABhA8A8QA6A6BTAAQB0ABBIhcIBkBoQh2CFiiAAQigABhphng");
	this.shape_177.setTransform(-336.1,7.5);

	this.shape_178 = new cjs.Shape();
	this.shape_178.graphics.f("#FFF000").s().p("AByFeIiajfIhsAAIAADfIidAAIAAq6IEKAAQCiAABFA2QBFA4AAB6QAACmiEAyICwD6gAiUgHIBxAAQBPAAAdgaQAegbAAg4QAAg4gfgWQgegUhJAAIh1AAg");
	this.shape_178.setTransform(-495.6,7.9);

	this.shape_179 = new cjs.Shape();
	this.shape_179.graphics.f("#FFF000").s().p("ACYFdIiYnlIiYHlIhzAAIj1q5ICpAAICLGQIB7mQICiAAIB7GQICMmQICoAAIj0K5g");
	this.shape_179.setTransform(-368.6,-116);

	this.shape_180 = new cjs.Shape();
	this.shape_180.graphics.f("#FFF000").s().p("AjeEDQhohlAAicQAAiaBqhoQBrhpCYAAQCtAABzCEIhgBtQhLhchuABQhWAAg/A5Qg/A6ABBgQAABiA6A7QA7A6BTAAQBzAABKhbIBjBoQh3CGihAAQigAAhphng");
	this.shape_180.setTransform(-280.7,-17.5);

	this.shape_181 = new cjs.Shape();
	this.shape_181.graphics.f("#FFF000").s().p("AByFeIiajgIhsAAIAADgIidAAIAAq7IEKAAQCiAABFA3QBFA3AAB7QAACmiFAxICxD7gAiUgIIBxAAQBPAAAdgaQAegZAAg5QgBg4gegWQgegUhJgBIh1AAg");
	this.shape_181.setTransform(-363.4,-17.1);

	this.shape_182 = new cjs.Shape();
	this.shape_182.graphics.f("#FFF000").s().p("ACYFeIiYnlIiYHlIh0AAIj0q7ICpAAICKGSIB8mSICiAAIB7GSICLmSICpAAIj0K7g");
	this.shape_182.setTransform(-466.8,-141);

	var maskedShapeInstanceList = [this.instance_2,this.shape_177,this.shape_178,this.shape_179,this.shape_180,this.shape_181,this.shape_182];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_2}]},17).to({state:[{t:this.instance_2}]},14).to({state:[{t:this.shape_179},{t:this.shape_178},{t:this.shape_177}]},1).to({state:[{t:this.shape_182},{t:this.shape_181},{t:this.shape_180}]},7).to({state:[]},9).to({state:[]},32).wait(41));
	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(17).to({_off:false},0).to({x:-276.8},14,cjs.Ease.quartOut).to({_off:true},1).wait(89));

	// masque Titre
	this.instance_3 = new lib.masque_generique();
	this.instance_3.parent = this;
	this.instance_3.setTransform(221.1,-163.9,0.013,0.471,0,0,0,0,17.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({regX:0.5,scaleX:1.92,x:-75.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.01,x:-68.7},10,cjs.Ease.quartInOut).to({_off:true},1).wait(101));

	// masque Titre
	this.instance_4 = new lib.masque_generique();
	this.instance_4.parent = this;
	this.instance_4.setTransform(-228.9,-13.9,0.013,0.471,0,0,0,0,17.2);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(4).to({_off:false},0).to({regX:0.5,scaleX:1.92,x:-527.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.01,x:-528.7},10,cjs.Ease.quartInOut).to({_off:true},1).wait(97));

	// _LETTRES_CLIENT
	this.shape_183 = new cjs.Shape();
	this.shape_183.graphics.f("#5FAFE5").s().p("AgJA0QgEgEAAgGQAAgFAEgFQAEgEAFAAQAGAAAEAEQAEAFAAAFQAAAGgEAEQgEAFgGAAQgFAAgEgFgAgJAVIgEg5IAAgUIAbAAIAAAUIgFA5g");
	this.shape_183.setTransform(-357.9,-14.1);

	this.shape_184 = new cjs.Shape();
	this.shape_184.graphics.f("#5FAFE5").s().p("AAbA4Ig1hFIAABFIgZAAIAAhvIAYAAIA2BHIAAhHIAZAAIAABvg");
	this.shape_184.setTransform(-378.9,-14.1);

	this.shape_185 = new cjs.Shape();
	this.shape_185.graphics.f("#5FAFE5").s().p("AgoA4IAAhvIBPAAIAAAXIg2AAIAAAWIAxAAIAAAUIgxAAIAAAYIA4AAIAAAWg");
	this.shape_185.setTransform(-402.4,-14.1);

	this.shape_186 = new cjs.Shape();
	this.shape_186.graphics.f("#5FAFE5").s().p("AgMA4IAAhvIAZAAIAABvg");
	this.shape_186.setTransform(-422.5,-14.1);

	this.shape_187 = new cjs.Shape();
	this.shape_187.graphics.f("#5FAFE5").s().p("AgCA5IADgYIgNAAIgEAYIgZAAIAFgYIgQAAIAFgWIAPAAIADgTIgRAAIAEgWIARAAIAEgaIAXAAIgDAaIANAAIAEgaIAZAAIgFAaIARAAIgEAWIgRAAIgDATIASAAIgEAWIgSAAIgEAYgAgJALIAOAAIAEgTIgOAAg");
	this.shape_187.setTransform(-459.4,-14.3);

	this.shape_188 = new cjs.Shape();
	this.shape_188.graphics.f("#5FAFE5").s().p("AgjA4IAAhvIAZAAIAABYIAuAAIAAAXg");
	this.shape_188.setTransform(-497.6,-14.1);

	this.shape_189 = new cjs.Shape();
	this.shape_189.graphics.f("#5FAFE5").s().p("AgjApQgRgQAAgYQAAgZARgQQASgRAXAAQAcAAASAVIgPASQgMgPgSAAQgNAAgKAJQgJAJgBAPQAAAPAKAKQAJAKANAAQASAAALgPIAQARQgTAVgZAAQgZAAgRgRg");
	this.shape_189.setTransform(-520.4,-14.2);

	this.shape_190 = new cjs.Shape();
	this.shape_190.graphics.f("#5FAFE5").s().p("AgMA4IAAhZIgfAAIAAgWIBXAAIAAAWIggAAIAABZg");
	this.shape_190.setTransform(-411.9,-14.1);

	this.shape_191 = new cjs.Shape();
	this.shape_191.graphics.f("#5FAFE5").s().p("AAbA4Ig0hFIAABFIgaAAIAAhvIAYAAIA2BHIAAhHIAZAAIAABvg");
	this.shape_191.setTransform(-435.3,-14.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_189},{t:this.shape_188},{t:this.shape_187},{t:this.shape_186,p:{x:-422.5}},{t:this.shape_185,p:{x:-402.4}},{t:this.shape_184},{t:this.shape_183,p:{x:-357.9}}]},13).to({state:[{t:this.shape_189},{t:this.shape_188},{t:this.shape_187},{t:this.shape_183,p:{x:-422.7}}]},5).to({state:[{t:this.shape_189},{t:this.shape_188},{t:this.shape_186,p:{x:-478.9}},{t:this.shape_185,p:{x:-458.8}},{t:this.shape_191},{t:this.shape_190}]},5).to({state:[]},20).to({state:[]},37).wait(41));

	// Layer_11
	this.instance_5 = new lib.masqueTexte("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(-328.9,105.9,0.9,0.85,0,0,0,0.1,108.9);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(37).to({_off:false},0).to({_off:true},40).wait(3).to({_off:false,regX:0.2,regY:108.8,rotation:180,x:-329,y:105.8},0).to({_off:true},40).wait(1));

	// _TEXTE_PRESENTATION
	this.shape_192 = new cjs.Shape();
	this.shape_192.graphics.f("#FFFFFF").s().p("AhYBoQgqgpAAg+QAAg9AqgqQArgqA9AAQBFAAAuA1IgmAsQgeglgtAAQghAAgZAXQgZAXAAAmQgBAnAYAYQAYAXAhAAQAtAAAdgkIApApQgwA2hBAAQhAAAgpgpg");
	this.shape_192.setTransform(-213.5,130.1);

	this.shape_193 = new cjs.Shape();
	this.shape_193.graphics.f("#FFFFFF").s().p("AAuCMIg+haIgrAAIAABaIg+AAIAAkXIBqAAQBAAAAcAWQAbAWAAAxQAABCg0AUIBGBkgAg7gCIAuAAQAfAAALgLQAMgKAAgXQAAgWgMgJQgMgIgdAAIgvAAg");
	this.shape_193.setTransform(-241.8,130.2);

	this.shape_194 = new cjs.Shape();
	this.shape_194.graphics.f("#FFFFFF").s().p("AA9CMIg9jCIg8DCIgvAAIhhkXIBDAAIA3CgIAzigIA/AAIAxCgIA4igIBEAAIhiEXg");
	this.shape_194.setTransform(-277.7,130.2);

	this.shape_195 = new cjs.Shape();
	this.shape_195.graphics.f("#FFFFFF").s().p("AgqBmQgagIgVgRIAbgmQAhAaAmAAQALAAAHgEQAGgFAAgHQAAgIgJgGQgKgHgQgFIgcgKQgJgEgLgIQgZgOAAgeQAAgdAYgTQAYgTAlAAQAmAAAmAaIgWAoQgcgUgdAAQgaAAAAAQQAAAIAJAFQAJAEATAHIAdAKQAIADAMAHQAXANAAAgQAAAggYATQgXATgkAAQgXAAgagJg");
	this.shape_195.setTransform(-319.4,133.4);

	this.shape_196 = new cjs.Shape();
	this.shape_196.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAggeQAhgeArAAQAtAAAfAbQAfAbAAAuIAAAfIicAAQADASAPALQAQALATAAQAfAAAVgVIAiAlQglAigxAAQgwAAgggegAgdgyQgPALgCATIBdAAQgCgUgNgKQgMgKgSAAQgQAAgPAKg");
	this.shape_196.setTransform(-341.7,133.4);

	this.shape_197 = new cjs.Shape();
	this.shape_197.graphics.f("#FFFFFF").s().p("AhvB8IAbgsQAQAPAPgBQAJABAGgJQAGgHAAgKQAAgJhUjRIBAAAIA0CGIA1iGIBAAAIhkD9QgJAWgSAMQgSAMgYAAQgfAAgcgag");
	this.shape_197.setTransform(-365.9,137.7);

	this.shape_198 = new cjs.Shape();
	this.shape_198.graphics.f("#FFFFFF").s().p("AgdCUIAAkoIA7AAIAAEog");
	this.shape_198.setTransform(-383.5,129.4);

	this.shape_199 = new cjs.Shape();
	this.shape_199.graphics.f("#FFFFFF").s().p("AgdCUIAAkoIA7AAIAAEog");
	this.shape_199.setTransform(-395.1,129.4);

	this.shape_200 = new cjs.Shape();
	this.shape_200.graphics.f("#FFFFFF").s().p("AhJBbQgXgUAAghQAAghAYgPQAYgQAqAAIArAAIAAgCQAAgigkAAQgPAAgRAGQgSAGgLAKIgbgnQAqgfA2AAQAnAAAZAUQAYATAAArIAACIIg4AAIAAgaQgYAdggAAQgjAAgXgUgAgkAmQAAALAJAHQAIAGAQAAQAQAAALgKQANgKAAgQIAAgLIglAAQgkAAAAAXg");
	this.shape_200.setTransform(-413.3,133.4);

	this.shape_201 = new cjs.Shape();
	this.shape_201.graphics.f("#FFFFFF").s().p("Ag9BtIAAjWIA8AAIAAAdQAKgOASgJQARgJASAAIAAA4IgLAAQgbAAgNATQgMATAAAeIAABdg");
	this.shape_201.setTransform(-432.1,133.3);

	this.shape_202 = new cjs.Shape();
	this.shape_202.graphics.f("#FFFFFF").s().p("AgqBmQgagIgVgRIAagmQAjAaAkAAQAMAAAGgEQAIgFAAgHQAAgIgKgGQgJgHgRgFIgbgKQgKgEgMgIQgYgOAAgeQAAgdAYgTQAYgTAlAAQAmAAAmAaIgXAoQgcgUgcAAQgaAAAAAQQAAAIAJAFQAIAEAUAHIAdAKQAJADAMAHQAWANAAAgQAAAggXATQgYATgkAAQgXAAgagJg");
	this.shape_202.setTransform(-462.4,133.4);

	this.shape_203 = new cjs.Shape();
	this.shape_203.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAggeQAhgeAsAAQAsAAAfAbQAfAbAAAuIAAAfIicAAQADASAPALQAQALATAAQAfAAAVgVIAhAlQgkAigxAAQgwAAgggegAgdgyQgPALgDATIBeAAQgCgUgNgKQgMgKgRAAQgSAAgOAKg");
	this.shape_203.setTransform(-484.7,133.4);

	this.shape_204 = new cjs.Shape();
	this.shape_204.graphics.f("#FFFFFF").s().p("AhQB2QgeggAAgwQAAgwAdgeQAegeAngBQAmAAAZAcIAAhqIA8AAIAAEoIg8AAIAAgcQgaAfglAAQgmAAgegggAgigDQgPARAAAaQAAAaAPARQAQAQATAAQAVAAAPgQQAOgRAAgaQAAgagOgRQgPgSgVAAQgUAAgPASg");
	this.shape_204.setTransform(-511.1,129.5);

	this.shape_205 = new cjs.Shape();
	this.shape_205.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAggeQAhgeArAAQAtAAAfAbQAfAbAAAuIAAAfIicAAQADASAPALQAQALATAAQAfAAAVgVIAiAlQglAigxAAQgwAAgggegAgdgyQgPALgCATIBdAAQgCgUgNgKQgMgKgSAAQgQAAgPAKg");
	this.shape_205.setTransform(-222,84.7);

	this.shape_206 = new cjs.Shape();
	this.shape_206.graphics.f("#FFFFFF").s().p("AgdBsIhWjXIA+AAIA1CHIA2iHIA+AAIhVDXg");
	this.shape_206.setTransform(-246.2,84.7);

	this.shape_207 = new cjs.Shape();
	this.shape_207.graphics.f("#FFFFFF").s().p("AgdCXIAAjWIA7AAIAADWgAgYhaQgKgKAAgPQAAgPAKgKQAKgKAOAAQAPAAAKAKQAKAKAAAPQAAAPgKAKQgKAKgPAAQgOAAgKgKg");
	this.shape_207.setTransform(-263.7,80.3);

	this.shape_208 = new cjs.Shape();
	this.shape_208.graphics.f("#FFFFFF").s().p("AgdCVIAAkoIA7AAIAAEog");
	this.shape_208.setTransform(-275.3,80.6);

	this.shape_209 = new cjs.Shape();
	this.shape_209.graphics.f("#FFFFFF").s().p("AggB5QgVgUAAgkIAAhiIgZAAIAAgrIAZAAIAAhAIA7AAIAABAIA0AAIAAArIg0AAIAABgQAAAMAHAHQAHAHAIAAQARAAALgPIAXAqQgcAZgfAAQgeAAgWgUg");
	this.shape_209.setTransform(-299.9,81.6);

	this.shape_210 = new cjs.Shape();
	this.shape_210.graphics.f("#FFFFFF").s().p("AApBtIAAh4QAAgyglAAQgRAAgNANQgNANAAAZIAAB3Ig9AAIAAjWIA9AAIAAAYQAagbAiAAQAhAAAYAYQAXAYgBAnIAACCg");
	this.shape_210.setTransform(-321.9,84.5);

	this.shape_211 = new cjs.Shape();
	this.shape_211.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAggeQAhgeArAAQAtAAAfAbQAfAbAAAuIAAAfIicAAQADASAPALQAQALATAAQAfAAAVgVIAiAlQglAigxAAQgwAAgggegAgdgyQgPALgCATIBdAAQgCgUgNgKQgMgKgSAAQgQAAgPAKg");
	this.shape_211.setTransform(-347.1,84.7);

	this.shape_212 = new cjs.Shape();
	this.shape_212.graphics.f("#FFFFFF").s().p("ABtBtIAAh0QAAgygkAAQgSAAgNANQgOANAAAZIAABzIg7AAIAAh0QAAgagIgMQgIgMgRAAQgSAAgNANQgNANAAAZIAABzIg8AAIAAjWIA8AAIAAAYQAZgbAfAAQAVAAAQALQAQANAIARQANgUAVgLQAVgKAVAAQAmAAAXAWQAXAXAAAqIAACCg");
	this.shape_212.setTransform(-379.1,84.5);

	this.shape_213 = new cjs.Shape();
	this.shape_213.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAggeQAhgeAsAAQAsAAAfAbQAfAbAAAuIAAAfIicAAQADASAPALQAQALATAAQAfAAAVgVIAiAlQglAigxAAQgwAAgggegAgdgyQgPALgDATIBeAAQgCgUgNgKQgMgKgSAAQgQAAgPAKg");
	this.shape_213.setTransform(-411.1,84.7);

	this.shape_214 = new cjs.Shape();
	this.shape_214.graphics.f("#FFFFFF").s().p("AgqBmQgagIgVgRIAbgmQAhAaAmAAQALAAAHgEQAGgFABgHQAAgIgKgGQgKgHgQgFIgbgKQgKgEgMgIQgYgOAAgeQAAgdAYgTQAYgTAlAAQAmAAAmAaIgXAoQgbgUgcAAQgbAAAAAQQAAAIAJAFQAJAEATAHIAdAKQAJADALAHQAXANAAAgQAAAggYATQgXATgkAAQgXAAgagJg");
	this.shape_214.setTransform(-434.3,84.7);

	this.shape_215 = new cjs.Shape();
	this.shape_215.graphics.f("#FFFFFF").s().p("AgqBmQgagIgVgRIAbgmQAhAaAmAAQALAAAHgEQAGgFABgHQAAgIgKgGQgKgHgQgFIgcgKQgJgEgMgIQgYgOAAgeQAAgdAYgTQAYgTAlAAQAmAAAmAaIgWAoQgcgUgcAAQgbAAAAAQQAAAIAJAFQAJAEATAHIAdAKQAIADAMAHQAXANAAAgQAAAggYATQgXATgkAAQgXAAgagJg");
	this.shape_215.setTransform(-455.1,84.7);

	this.shape_216 = new cjs.Shape();
	this.shape_216.graphics.f("#FFFFFF").s().p("AhJBbQgXgUAAghQAAghAYgPQAYgQApAAIAsAAIAAgCQAAgigkAAQgPAAgRAGQgRAGgNAKIgbgnQArgfA2AAQAmAAAZAUQAZATAAArIAACIIg4AAIAAgaQgXAdgiAAQghAAgYgUgAgkAmQAAALAJAHQAIAGARAAQAPAAAMgKQAMgKAAgQIAAgLIglAAQgkAAAAAXg");
	this.shape_216.setTransform(-477.5,84.7);

	this.shape_217 = new cjs.Shape();
	this.shape_217.graphics.f("#FFFFFF").s().p("AgdCVIAAkoIA7AAIAAEog");
	this.shape_217.setTransform(-494.7,80.6);

	this.shape_218 = new cjs.Shape();
	this.shape_218.graphics.f("#FFFFFF").s().p("AhEBQQgigeAAgyQAAgwAigfQAigfAtAAQAXAAAYAKQAYAJASATIgfApQgLgNgPgGQgOgHgPAAQgXAAgRAPQgRAQAAAaQAAAbARAQQARAPAWAAQAeAAAYgeIAkAoQgqArgwAAQgwAAghgfg");
	this.shape_218.setTransform(-512,84.7);

	this.shape_219 = new cjs.Shape();
	this.shape_219.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAhgeQAggeArAAQAtAAAfAbQAfAbAAAuIAAAfIicAAQADASAQALQAPALATAAQAfAAAVgVIAhAlQgkAigxAAQgwAAgggegAgdgyQgPALgDATIBeAAQgCgUgMgKQgNgKgRAAQgRAAgPAKg");
	this.shape_219.setTransform(-313.8,35.9);

	this.shape_220 = new cjs.Shape();
	this.shape_220.graphics.f("#FFFFFF").s().p("AhQB2QgeghAAgwQAAguAdggQAegeAnAAQAmABAZAbIAAhqIA8AAIAAEoIg8AAIAAgdQgaAgglAAQgmAAgegggAgigDQgPARAAAaQAAAaAPAQQAQARATAAQAVAAAPgRQAOgQAAgbQAAgZgOgRQgPgRgVAAQgUAAgPARg");
	this.shape_220.setTransform(-340.2,32);

	this.shape_221 = new cjs.Shape();
	this.shape_221.graphics.f("#FFFFFF").s().p("AhMBXQgYgWAAgsIAAiBIA9AAIAAB0QAAAyAkgBQARABANgNQANgNAAgZIAAhzIA9AAIAADWIg9AAIAAgdQgWAggiAAQgkAAgYgWg");
	this.shape_221.setTransform(-376.4,36.1);

	this.shape_222 = new cjs.Shape();
	this.shape_222.graphics.f("#FFFFFF").s().p("AhJBbQgXgUAAghQAAghAYgPQAYgQAqAAIArAAIAAgCQAAgigkAAQgPAAgRAGQgRAGgMAKIgbgnQAqgfA1AAQAnAAAaAUQAYATAAArIAACIIg4AAIAAgaQgYAdggAAQgiAAgYgUgAgkAmQAAALAIAHQAKAGAPAAQAPAAAMgKQANgKAAgQIAAgLIglAAQgkAAAAAXg");
	this.shape_222.setTransform(-401.4,35.9);

	this.shape_223 = new cjs.Shape();
	this.shape_223.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAggeQAhgeAsAAQAsAAAfAbQAfAbAAAuIAAAfIicAAQADASAPALQAQALATAAQAfAAAVgVIAiAlQglAigxAAQgwAAgggegAgdgyQgPALgDATIBeAAQgCgUgNgKQgMgKgSAAQgQAAgPAKg");
	this.shape_223.setTransform(-425,35.9);

	this.shape_224 = new cjs.Shape();
	this.shape_224.graphics.f("#FFFFFF").s().p("AgdCVIAAkoIA7AAIAAEog");
	this.shape_224.setTransform(-443.3,31.9);

	this.shape_225 = new cjs.Shape();
	this.shape_225.graphics.f("#FFFFFF").s().p("AgyB2IAAAdIg8AAIAAkoIA8AAIAABqQAZgbAmgBQAnAAAdAeQAeAgAAAuQAAAwgeAhQgeAggmAAQgmAAgZgggAgkgDQgPARAAAZQAAAbAPAQQAPARAUAAQAUAAAQgRQAPgQAAgaQAAgagPgRQgPgRgUAAQgVAAgPARg");
	this.shape_225.setTransform(-461.9,32);

	this.shape_226 = new cjs.Shape();
	this.shape_226.graphics.f("#FFFFFF").s().p("AhJBbQgXgUAAghQAAghAYgPQAYgQAqAAIArAAIAAgCQAAgigkAAQgPAAgRAGQgSAGgLAKIgbgnQAqgfA2AAQAmAAAaAUQAYATAAArIAACIIg4AAIAAgaQgYAdggAAQgjAAgXgUgAgkAmQAAALAIAHQAKAGAPAAQAPAAAMgKQANgKAAgQIAAgLIglAAQgkAAAAAXg");
	this.shape_226.setTransform(-488.1,35.9);

	this.shape_227 = new cjs.Shape();
	this.shape_227.graphics.f("#FFFFFF").s().p("AgeCMIAAjhIhPAAIAAg2IDbAAIAAA2IhPAAIAADhg");
	this.shape_227.setTransform(-511.7,32.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_227},{t:this.shape_226},{t:this.shape_225},{t:this.shape_224},{t:this.shape_223},{t:this.shape_222},{t:this.shape_221},{t:this.shape_220},{t:this.shape_219},{t:this.shape_218},{t:this.shape_217},{t:this.shape_216},{t:this.shape_215},{t:this.shape_214},{t:this.shape_213},{t:this.shape_212},{t:this.shape_211},{t:this.shape_210},{t:this.shape_209},{t:this.shape_208},{t:this.shape_207},{t:this.shape_206},{t:this.shape_205},{t:this.shape_204},{t:this.shape_203},{t:this.shape_202},{t:this.shape_201},{t:this.shape_200},{t:this.shape_199},{t:this.shape_198},{t:this.shape_197},{t:this.shape_196},{t:this.shape_195},{t:this.shape_194},{t:this.shape_193},{t:this.shape_192}]},57).to({state:[]},43).to({state:[]},1).wait(20));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(221.1,-172,2,16);


// stage content:
(lib.FS_projet_michelin_wrc_mobile = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{video:179,"close":515});

	// timeline functions:
	this.frame_0 = function() {
		//variable necessaire pour la fermeture des textes
		this.hiddentexts = false;
		
		//variable necessaire pour la fermeture de la vidéo
		this.hiddenvideo = true;
		
		//variable necessaire pour la fermeture du visuel de fond
		this.hiddenvisuel = false;
		
		//variable necessaire pour la fermeture du bouton play
		this.hiddenplay = true;
	}
	this.frame_139 = function() {
		//var frequency = 60;
		//stage.enableMouseOver(frequency);
		
		// EVENT DE CLIC POUR LA SUITE
		this.next_btn.addEventListener("click", fl_ClickToGoToAndPlayFromFrame.bind(this));
		this.next_btn.addEventListener("mouseover", fl_MouseOverHandler.bind(this));
		this.next_btn.addEventListener("mouseout", fl_MouseOutHandler.bind(this));
		
		
		function fl_ClickToGoToAndPlayFromFrame()
		{
			//this.gotoAndPlay("close");
			
			var event = new Event('next');
			this.dispatchEvent(event);		
			event = null;
			
			this.next_btn.btnNext.gotoAndPlay('rollOut');
			this.lignes_mc.gotoAndPlay("close");
			this.next_btn.gotoAndPlay("close");
			this.cta_mc.gotoAndPlay("close");
			
		}
		function fl_MouseOverHandler(){	this.next_btn.btnNext.gotoAndPlay('rollOver'); }
		function fl_MouseOutHandler(){	this.next_btn.btnNext.gotoAndPlay('rollOut'); }
		
		
		// EVENT DE CLIC POUR LE LIEN
		this.cta_mc.addEventListener("click", projectDetails.bind(this));
		this.cta_mc.addEventListener("mouseover", pDetails_MouseOverHandler.bind(this));
		this.cta_mc.addEventListener("mouseout", pDetails_MouseOutHandler.bind(this));
		
		function projectDetails()
		{	
			var event = new Event('details');
			this.dispatchEvent(event);		
			event = null;
			
			this.cta_mc.btnCta.gotoAndPlay('rollOut');
			
		}
		function pDetails_MouseOverHandler(){	this.cta_mc.btnCta.gotoAndPlay('rollOver'); }
		function pDetails_MouseOutHandler(){	this.cta_mc.btnCta.gotoAndPlay('rollOut'); }
		
		// EVENT DE CLIC POUR LA VIDEO
		this.video_mc.btn_playVideo.addEventListener("click", playVideo.bind(this));
		
		function playVideo()
		{	
			var event = new Event('video');
			this.dispatchEvent(event);		
			event = null;
			
			this.hiddenplay = true;	
			this.video_mc.btn_playVideo.gotoAndPlay('close');
		}
	}
	this.frame_179 = function() {
		this.visuel_mc.gotoAndPlay("close");
		this.video_mc.gotoAndPlay("open");
		
		this.hiddenvideo = false;
		this.hiddenvisuel = true;
		this.hiddenplay = false;
		
		//On envoie l'information de l'affichage de la video
		var event = new Event('videoStarter');
		this.dispatchEvent(event);		
		event = null;
	}
	this.frame_339 = function() {
		this.textes_mc.gotoAndPlay("close");
		this.hiddentexts = true;
	}
	this.frame_514 = function() {
		//PAUSE
		this.stop();
	}
	this.frame_516 = function() {
		this.cta_mc.gotoAndPlay("close");
		this.next_btn.gotoAndPlay("close");
		
		
		if(this.hiddenvisuel==false){
			this.visuel_mc.gotoAndPlay("close");
		}
		
		if(this.hiddenvideo==false){
			this.video_mc.gotoAndPlay("close");
		}
		
		if(this.hiddentexts==false){
			this.textes_mc.gotoAndPlay("close");
			this.hiddentexts = true;
		}
		
		if(this.hiddenplay==false){
			this.video_mc.btn_playVideo.gotoAndPlay('close');
		}
	}
	this.frame_535 = function() {
		this.lignes_mc.gotoAndPlay("close");
	}
	this.frame_562 = function() {
		// EVENT DE FIN D'ANIM
		var event = new Event('closed');
		this.dispatchEvent(event);		
		event = null;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(139).call(this.frame_139).wait(40).call(this.frame_179).wait(160).call(this.frame_339).wait(175).call(this.frame_514).wait(2).call(this.frame_516).wait(19).call(this.frame_535).wait(27).call(this.frame_562).wait(1));

	// CTA NEXT
	this.next_btn = new lib.bloc_NEXT_btn();
	this.next_btn.name = "next_btn";
	this.next_btn.parent = this;
	this.next_btn.setTransform(359.2,861.2,1.06,1.06,0,0,0,-340.4,403.9);
	this.next_btn._off = true;

	this.timeline.addTween(cjs.Tween.get(this.next_btn).wait(128).to({_off:false},0).wait(435));

	// CTA DETAIL
	this.cta_mc = new lib.bloc_CTA();
	this.cta_mc.name = "cta_mc";
	this.cta_mc.parent = this;
	this.cta_mc.setTransform(120.2,861.4,1.06,1.06,0,0,0,-340.4,404.1);
	this.cta_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.cta_mc).wait(119).to({_off:false},0).to({_off:true},420).wait(24));

	// BLOC INFOS TEXTE
	this.textes_mc = new lib.blocInfostextes();
	this.textes_mc.name = "textes_mc";
	this.textes_mc.parent = this;
	this.textes_mc.setTransform(788.2,242,1,1,0,0,0,222.1,-164);
	this.textes_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.textes_mc).wait(17).to({_off:false},0).wait(546));

	// VIDEO
	this.video_mc = new lib.bloc_video();
	this.video_mc.name = "video_mc";
	this.video_mc.parent = this;
	this.video_mc.setTransform(238.9,1185.2,1.05,1.05,0,0,0,227.2,765);

	this.timeline.addTween(cjs.Tween.get(this.video_mc).to({_off:true},539).wait(24));

	// VISUEL
	this.visuel_mc = new lib.bloc_visuel();
	this.visuel_mc.name = "visuel_mc";
	this.visuel_mc.parent = this;
	this.visuel_mc.setTransform(1030.6,381.5,1,1,0,0,0,552.5,26.5);
	this.visuel_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.visuel_mc).wait(17).to({_off:false},0).wait(546));

	// lignes
	this.lignes_mc = new lib.bloc_lignes();
	this.lignes_mc.name = "lignes_mc";
	this.lignes_mc.parent = this;
	this.lignes_mc.setTransform(240.8,453.9,0.525,1,0,0,0,0.7,405.9);

	this.timeline.addTween(cjs.Tween.get(this.lignes_mc).wait(563));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(241.3,293.5,478,1643.4);
// library properties:
lib.properties = {
	id: '686F4A33A3050E4BB6327A06DB8CD2B4',
	width: 480,
	height: 840,
	fps: 60,
	color: "#000000",
	opacity: 1.00,
	manifest: [
		{src:"images/WRC_cloud.png?1540567888167", id:"WRC_cloud"},
		{src:"images/WRC_route.png?1540567888167", id:"WRC_route"},
		{src:"images/WRC_voiture.png?1540567888167", id:"WRC_voiture"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['686F4A33A3050E4BB6327A06DB8CD2B4'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;