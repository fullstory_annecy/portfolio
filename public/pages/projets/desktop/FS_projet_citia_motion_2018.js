(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.affiche2017 = function() {
	this.initialize(img.affiche2017);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,263,400);


(lib.affiche2018 = function() {
	this.initialize(img.affiche2018);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,263,400);


(lib.affiche2019 = function() {
	this.initialize(img.affiche2019);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,263,400);


(lib.CITIA_2018_b = function() {
	this.initialize(img.CITIA_2018_b);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,477,251);


(lib.CITIA_2018_c = function() {
	this.initialize(img.CITIA_2018_c);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,525,316);


(lib.CITIA_2018_d = function() {
	this.initialize(img.CITIA_2018_d);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,368,279);


(lib.sucette = function() {
	this.initialize(img.sucette);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,303,514);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.visuel_montagne = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.CITIA_2018_d();
	this.instance.parent = this;
	this.instance.setTransform(-147.2,0,0.8,0.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-147.2,0,294.4,223.2);


(lib.visuel_chateau = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.CITIA_2018_c();
	this.instance.parent = this;
	this.instance.setTransform(-210,0,0.8,0.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-210,0,420,252.8);


(lib.visuel_bonhomme = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.CITIA_2018_b();
	this.instance.parent = this;
	this.instance.setTransform(-143.1,0,0.6,0.6);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-143.1,0,286.2,150.6);


(lib.video = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.008)").s().p("EgtTBLAMAAAiV/MBanAAAMAAACV/g");
	this.shape.setTransform(290,480);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.video, new cjs.Rectangle(0,0,580,960), null);


(lib.txt_hashtag = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgQAwIgJgDIgIgGIALgPIACACIAHADQAFADADAAQAJAAAFgGQAEgGABgLQgIAGgKAAQgOAAgIgJQgJgIAAgPQgBgOAKgKQAKgIAPgBQAJAAAIAFQAHADAFAHQAJAOAAAVQAAAMgEALQgDAKgGAGQgLAMgQAAQgGgBgHgCgAgMgbQgDAEAAAGQAAAGADAEQAEAEAGABQAGAAAFgEQADgEABgGQgBgHgDgDQgFgFgFgBQgGAAgFAFg");
	this.shape.setTransform(62.2,73.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAAAxIAAhOIgTAAIAAgTIAnAAIAABhg");
	this.shape_1.setTransform(54.6,73.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgdAlQgKgOAAgXQAAgWAKgOQAKgOATAAQAUAAAKAOQAKAOAAAWQAAAXgKAOQgKAOgUAAQgTAAgKgOgAgNgXQgFAJAAAOQAAAPAFAJQAEAIAJABQAKgBAFgIQAEgJAAgPQAAgOgEgJQgFgIgKgBQgJABgEAIg");
	this.shape_2.setTransform(47.3,73.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AghAyIAAgRIAdgcIALgOQAEgFAAgFQAAgFgDgDQgDgDgFAAQgJAAgIANIgRgLQAHgKAIgGQAIgFAMAAQAMAAAKAIQAJAIAAAOQAAAHgDAIQgEAFgLALIgQATIAlAAIAAATg");
	this.shape_3.setTransform(38.2,73.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgYAJIAAgSIAxAAIAAASg");
	this.shape_4.setTransform(30.6,74.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgaAqQgLgJABgNQAAgPAMgJQgJgIAAgLQAAgLAKgIQAKgIANAAQAOAAAKAIQAKAIAAALQAAAMgJAHQANAJAAAPQAAANgLAJQgLAJgQAAQgPAAgLgJgAgKAKQgEAEgBAFQAAAHAGADQAEAEAFAAQAGAAAFgEQAEgDAAgHQAAgFgEgEQgFgEgGABQgFgBgFAEgAgIgdQgFADAAAFQAAAFAFADQAEAEAEAAQAFAAAFgEQADgDAAgFQAAgFgDgDQgFgEgFAAQgEAAgEAEg");
	this.shape_5.setTransform(22.7,73.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAAAxIAAhOIgTAAIAAgTIAnAAIAABhg");
	this.shape_6.setTransform(15,73.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgdAlQgKgOAAgXQAAgWAKgOQAKgOATAAQAUAAAKAOQAKAOAAAWQAAAXgKAOQgKAOgUAAQgTAAgKgOgAgNgXQgFAJAAAOQAAAPAFAJQAEAIAJABQAKgBAFgIQAEgJAAgPQAAgOgEgJQgFgIgKgBQgJABgEAIg");
	this.shape_7.setTransform(7.7,73.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AghAyIAAgRIAdgcIALgOQAEgFAAgFQAAgFgDgDQgDgDgFAAQgJAAgIANIgRgLQAHgKAIgGQAIgFAMAAQAMAAAKAIQAJAIAAAOQAAAHgDAIQgEAFgLALIgQATIAlAAIAAATg");
	this.shape_8.setTransform(-1.4,73.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgYAJIAAgSIAxAAIAAASg");
	this.shape_9.setTransform(-9,74.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgbAxIAkhOIgXAAIAAANIgVAAIAAggIBGAAIAAARIgmBQg");
	this.shape_10.setTransform(-16.3,73.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AAAAxIAAhOIgTAAIAAgTIAnAAIAABhg");
	this.shape_11.setTransform(-23.7,73.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgdAlQgKgOAAgXQAAgWAKgOQAKgOATAAQAUAAAKAOQAKAOAAAWQAAAXgKAOQgKAOgUAAQgTAAgKgOgAgNgXQgFAJAAAOQAAAPAFAJQAEAIAJABQAKgBAFgIQAEgJAAgPQAAgOgEgJQgFgIgKgBQgJABgEAIg");
	this.shape_12.setTransform(-31,73.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AghAyIAAgRIAdgcIALgOQAEgFAAgFQAAgFgDgDQgDgDgFAAQgJAAgIANIgRgLQAHgKAIgGQAIgFAMAAQAMAAAKAIQAJAIAAAOQAAAHgDAIQgEAFgLALIgQATIAlAAIAAATg");
	this.shape_13.setTransform(-40.1,73.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgUIANAAIADgQIgPAAIADgUIAPAAIAEgWIAUAAIgDAWIAMAAIADgWIAWAAIgEAWIAOAAIgDAUIgPAAIgDAQIARAAIgEAUIgQAAIgDAVgAgHAJIALAAIAEgQIgMAAg");
	this.shape_14.setTransform(-49.5,73.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgjAkQgPgPAAgVQAAgUAPgPQAOgPAVAAQAWAAAPAPQAOAPAAAUQAAAVgOAPQgPAPgWAAQgVAAgOgPgAgTgVQgJAJAAAMQAAANAJAKQAIAIALABQAMgBAJgIQAIgKAAgNQAAgMgIgJQgJgJgMAAQgLAAgIAJg");
	this.shape_15.setTransform(83.3,52.7);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBFAAIAAATIgvAAIAAAVIArAAIAAASIgrAAIAAAUIAxAAIAAATg");
	this.shape_16.setTransform(73.2,52.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgsAxIAAhhIAjAAQAaAAAOANQAOAMgBAXQABAWgOAOQgOANgbAAgAgVAeIANAAQAOgBAJgHQAHgIABgOQgBgOgHgHQgJgIgQAAIgLAAg");
	this.shape_17.setTransform(63.3,52.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgKAxIAAhhIAVAAIAABhg");
	this.shape_18.setTransform(55.3,52.8);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgIAxIgohhIAYAAIAYA9IAZg9IAYAAIgnBhg");
	this.shape_19.setTransform(48.2,52.8);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgmAiIANgQQAQAOANAAQAGgBADgDQADgCAAgFQAAgEgDgCQgEgDgKgDQgRgDgIgGQgIgGAAgOQAAgPALgHQAKgIANAAQAKAAAKAEQAKADAHAHIgLAQQgNgKgNAAQgFAAgDACQgDADAAAEQAAAEAEADQAEACANAEQAOADAHAGQAIAHAAANQAAANgKAJQgKAHgQABQgWAAgTgRg");
	this.shape_20.setTransform(35.1,52.7);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBFAAIAAATIgvAAIAAAVIArAAIAAASIgrAAIAAAUIAxAAIAAATg");
	this.shape_21.setTransform(26.4,52.8);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgeAkQgOgOgBgWQABgVAPgOQAOgPAVAAQAUAAAQAOIgMARQgGgGgGgCQgFgCgGAAQgMgBgJAJQgIAJAAAMQAAAOAIAJQAJAIAKAAQALAAAIgFIAAgaIAVAAIAAAjQgOAQgZAAQgWAAgOgPg");
	this.shape_22.setTransform(16.3,52.7);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AAeAxIgKgVIgoAAIgJAVIgXAAIAqhhIAVAAIAqBhgAgLAJIAXAAIgMgcg");
	this.shape_23.setTransform(5.8,52.8);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgKAxIAAhOIgbAAIAAgTIBLAAIAAATIgbAAIAABOg");
	this.shape_24.setTransform(-3.6,52.8);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AAXAxIgtg9IAAA9IgWAAIAAhhIAUAAIAvA+IAAg+IAWAAIAABhg");
	this.shape_25.setTransform(-13.6,52.8);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgjAkQgPgPAAgVQAAgUAPgPQAOgPAVAAQAWAAAPAPQAOAPAAAUQAAAVgOAPQgPAPgWAAQgVAAgOgPgAgTgVQgJAJAAAMQAAANAJAKQAIAIALABQAMgBAJgIQAIgKAAgNQAAgMgIgJQgJgJgMAAQgLAAgIAJg");
	this.shape_26.setTransform(-25.1,52.7);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AAhAxIAAg9IgbA1IgMAAIgag1IAAA9IgWAAIAAhhIAeAAIAYA1IAZg1IAeAAIAABhg");
	this.shape_27.setTransform(-37.6,52.8);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgTIANAAIADgRIgPAAIADgUIAPAAIAEgWIAUAAIgDAWIAMAAIADgWIAWAAIgEAWIAOAAIgDAUIgPAAIgDARIARAAIgEATIgQAAIgDAVgAgHAKIALAAIAEgRIgMAAg");
	this.shape_28.setTransform(-49.5,52.7);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AAXAxIgtg9IAAA9IgWAAIAAhhIAUAAIAvA+IAAg+IAWAAIAABhg");
	this.shape_29.setTransform(70.1,31.7);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AgeAkQgPgOAAgWQABgUAPgPQAOgOAVAAQAUAAAQANIgLARQgHgGgGgDQgFgBgGAAQgMAAgIAIQgJAJAAAMQAAAOAIAJQAJAIAKAAQALAAAIgFIAAgaIAVAAIAAAiQgOAQgZABQgWgBgOgOg");
	this.shape_30.setTransform(59.1,31.7);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AgKAxIAAhhIAVAAIAABhg");
	this.shape_31.setTransform(51.6,31.7);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AgmAiIANgRQAQAPANAAQAGAAADgEQADgCAAgEQAAgFgDgCQgEgDgKgDQgRgEgIgFQgIgHAAgNQAAgPALgHQAKgIANABQAKAAAKADQAKADAHAGIgLARQgNgKgNAAQgFAAgDACQgDADAAAEQAAAFAEACQAEACANAEQAOADAHAGQAIAHAAANQAAANgKAJQgKAHgQABQgWgBgTgQg");
	this.shape_32.setTransform(44.7,31.7);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBFAAIAAATIgvAAIAAAVIArAAIAAASIgrAAIAAATIAxAAIAAAUg");
	this.shape_33.setTransform(36,31.7);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AgrAxIAAhhIAiAAQAaAAAOANQANAMAAAXQAAAWgNAOQgOANgbAAgAgVAdIANAAQAPAAAHgHQAJgIAAgOQAAgOgJgHQgHgIgRAAIgLAAg");
	this.shape_34.setTransform(26.1,31.7);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AAXAxIgtg9IAAA9IgWAAIAAhhIAUAAIAvA+IAAg+IAWAAIAABhg");
	this.shape_35.setTransform(11,31.7);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AgjAkQgPgPAAgVQAAgVAPgOQAOgOAVAAQAWAAAPAOQAOAOAAAVQAAAVgOAPQgPAPgWAAQgVAAgOgPgAgTgVQgJAJAAAMQAAANAJAKQAIAIALABQAMgBAJgIQAIgKAAgNQAAgMgIgJQgJgJgMAAQgLAAgIAJg");
	this.shape_36.setTransform(-0.5,31.7);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AgKAxIAAhhIAVAAIAABhg");
	this.shape_37.setTransform(-8.5,31.7);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("AgKAxIAAhOIgcAAIAAgTIBMAAIAAATIgbAAIAABOg");
	this.shape_38.setTransform(-15.1,31.7);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AgjAkQgPgPAAgVQAAgVAPgOQAOgOAVAAQAWAAAPAOQAOAOAAAVQAAAVgOAPQgPAPgWAAQgVAAgOgPgAgTgVQgJAJAAAMQAAANAJAKQAIAIALABQAMgBAJgIQAIgKAAgNQAAgMgIgJQgJgJgMAAQgLAAgIAJg");
	this.shape_39.setTransform(-25.1,31.7);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("AAhAxIAAg9IgbA1IgMAAIgag1IAAA9IgWAAIAAhhIAeAAIAYA1IAZg1IAeAAIAABhg");
	this.shape_40.setTransform(-37.6,31.7);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgTIANAAIADgRIgPAAIADgTIAPAAIAEgXIAUAAIgDAXIAMAAIADgXIAWAAIgEAXIAOAAIgDATIgPAAIgDARIARAAIgEATIgQAAIgDAVgAgHAKIALAAIAEgRIgMAAg");
	this.shape_41.setTransform(-49.5,31.6);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("AgKAxIAAhOIgbAAIAAgTIBLAAIAAATIgbAAIAABOg");
	this.shape_42.setTransform(100.9,10.7);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("AAXAxIgtg9IAAA9IgWAAIAAhhIAUAAIAvA+IAAg+IAWAAIAABhg");
	this.shape_43.setTransform(90.9,10.7);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBFAAIAAAUIgvAAIAAAUIArAAIAAASIgrAAIAAATIAxAAIAAAUg");
	this.shape_44.setTransform(80.8,10.7);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFFFFF").s().p("AAhAxIAAg9IgbA1IgMAAIgag1IAAA9IgWAAIAAhhIAeAAIAYA1IAZg1IAeAAIAABhg");
	this.shape_45.setTransform(69.3,10.7);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBFAAIAAAUIgvAAIAAAUIArAAIAAASIgrAAIAAATIAxAAIAAAUg");
	this.shape_46.setTransform(58.3,10.7);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFFFFF").s().p("AAXAxIgtg9IAAA9IgWAAIAAhhIAUAAIAvA+IAAg+IAWAAIAABhg");
	this.shape_47.setTransform(47.8,10.7);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FFFFFF").s().p("AgeAkQgOgOAAgWQgBgUAPgPQAPgOAUAAQAWAAAPANIgMARQgGgGgFgDQgGgBgGAAQgMAAgJAIQgIAIAAANQAAAOAIAJQAJAIAJAAQAMAAAHgFIAAgaIAXAAIAAAiQgPAQgaABQgUgBgPgOg");
	this.shape_48.setTransform(36.8,10.6);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FFFFFF").s().p("AAeAxIgKgVIgoAAIgJAVIgXAAIAqhhIAVAAIAqBhgAgLAJIAXAAIgMgcg");
	this.shape_49.setTransform(26.3,10.7);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FFFFFF").s().p("AgmAxIAAhhIAjAAQAVAAAKAJQALAJAAASQAAARgLAIQgKAJgVgBIgNAAIAAAcgAgQADIAPAAQAKAAADgDQAFgFAAgJQgBgIgFgDQgFgEgJAAIgNAAg");
	this.shape_50.setTransform(16.7,10.7);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FFFFFF").s().p("AAhAxIAAg9IgbA1IgMAAIgag1IAAA9IgWAAIAAhhIAeAAIAYA1IAZg1IAeAAIAABhg");
	this.shape_51.setTransform(4.9,10.7);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FFFFFF").s().p("AgjAkQgPgPAAgVQAAgVAPgOQAOgOAVAAQAWAAAPAOQAOAOAAAVQAAAVgOAPQgPAPgWAAQgVAAgOgPgAgTgVQgJAJAAAMQAAANAJAKQAIAIALABQAMgBAJgIQAIgKAAgNQAAgMgIgJQgJgKgMABQgLgBgIAKg");
	this.shape_52.setTransform(-7.5,10.6);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FFFFFF").s().p("AgeAkQgPgOAAgWQAAgUAPgPQAPgOAUAAQAZgBAQATIgOAPQgKgMgQAAQgLgBgJAJQgJAHAAANQABAOAIAIQAJAIAKAAQAQAAAKgNIAPAPQgSATgWAAQgWgBgOgOg");
	this.shape_53.setTransform(-18.3,10.6);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FFFFFF").s().p("AgeAkQgPgOAAgWQAAgUAPgPQAPgOAUAAQAYgBARATIgOAPQgKgMgQAAQgLgBgJAJQgJAHAAANQAAAOAJAIQAIAIALAAQAQAAAKgNIAPAPQgRATgXAAQgWgBgOgOg");
	this.shape_54.setTransform(-28.6,10.6);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FFFFFF").s().p("AAeAxIgKgVIgoAAIgJAVIgXAAIAqhhIAVAAIAqBhgAgLAJIAXAAIgMgcg");
	this.shape_55.setTransform(-39,10.7);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgTIANAAIADgRIgPAAIADgTIAPAAIAEgXIAUAAIgDAXIAMAAIADgXIAWAAIgEAXIAOAAIgDATIgPAAIgDARIARAAIgEATIgQAAIgDAVgAgHAKIALAAIAEgRIgMAAg");
	this.shape_56.setTransform(-49.5,10.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.txt_hashtag, new cjs.Rectangle(-56.7,0,164,84.2), null);


(lib.over = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFA39").s().p("ANPUrIAAAAIAAAAIAAAAgAgKUoQtcAAACgCUgAJAAIAAOgpYQbDAJAEABQACABgPUmQgOUeACAGQgFgCtUgBg");
	this.shape.setTransform(87.2,132.3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.over, new cjs.Rectangle(0,0,174.4,264.6), null);


(lib.masqueTexte_1_ligne = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("EgicAEJIAAoRMBE5AAAIAAIRg");
	this.shape.setTransform(0,24.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.masqueTexte_1_ligne, new cjs.Rectangle(-220.5,-1.9,441,53), null);


(lib.masque_generique = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#5AB6B2").s().p("AsQCqIAAlTIYhAAIAAFTg");
	this.shape.setTransform(78.5,17);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.masque_generique, new cjs.Rectangle(0,0,157,34), null);


(lib.ligne = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(49));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#333333").ss(1,1,1).p("EAAAg/bMAAAB+3");
	this.shape.setTransform(0,406);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#333333").ss(1,1,1).p("EAA/A/cQhDv0ggwoQgdwxAExFQAEwXAhwHQAfufA4to");
	this.shape_1.setTransform(-6,406);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#333333").ss(1,1,1).p("EABwA/cQh4vfg5w8Qg0wqAHxQQAHweA7wDQA5uxBjtQ");
	this.shape_2.setTransform(-10.7,406);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#333333").ss(1,1,1).p("EACVA/cQigvPhLxLQhHwmAKxYQAJwjBPv/QBMvACEs9");
	this.shape_3.setTransform(-14.2,406);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#333333").ss(1,1,1).p("EACwA/cQi+vDhZxWQhTwjAMxeQAKwnBev8QBavKCcsw");
	this.shape_4.setTransform(-16.9,406);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#333333").ss(1,1,1).p("EADEA/cQjTu7hjxdQhdwhANxhQAMwrBov6QBkvSCusm");
	this.shape_5.setTransform(-18.8,406);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#333333").ss(1,1,1).p("EADSA/cQjiu1hqxjQhjwfAOxlQAMwsBvv5QBsvXC6sf");
	this.shape_6.setTransform(-20.1,406);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#333333").ss(1,1,1).p("EADbA/cQjsuxhuxnQhnweAOxnQANwtB0v4QBwvaDCsb");
	this.shape_7.setTransform(-20.9,406);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#333333").ss(1,1,1).p("EADgA/cQjyuuhxxpQhpwdAPxpQANwuB3v3QByvdDHsY");
	this.shape_8.setTransform(-21.4,406);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#333333").ss(1,1,1).p("EADjA/cQj1uthyxqQhrwdAPxpQANwvB5v3QB0vdDJsX");
	this.shape_9.setTransform(-21.7,406);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#333333").ss(1,1,1).p("EADkA/cQj2uthzxqQhswdAPxpQANwvB6v3QB1veDKsW");
	this.shape_10.setTransform(-21.8,406);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#333333").ss(1,1,1).p("EADlA/cQj3ush0xrQhrwdAPxpQANwvB5v3QB2veDLsW");
	this.shape_11.setTransform(-21.9,406);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#333333").ss(1,1,1).p("EADlA/cQj3ushzxrQhswdAPxpQANwvB6v3QB1veDLsW");
	this.shape_12.setTransform(-21.9,406);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#333333").ss(1,1,1).p("EADlg/bQjLMWh1PeQh6P3gNQvQgPRpBsQdQBzRrD3Os");
	this.shape_13.setTransform(-21.9,406);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#333333").ss(1,1,1).p("EADlA/cQj3ushzxrQhswdAPxpQANwvB5v3QB2veDLsW");
	this.shape_14.setTransform(-21.9,406);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#333333").ss(1,1,1).p("EADlA/cQj3ushzxrQhswdAPxpQAOwvB5v3QB1veDLsW");
	this.shape_15.setTransform(-21.9,406);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#333333").ss(1,1,1).p("EADkA/cQj2uthzxqQhrwdAPxpQANwvB5v3QB1veDKsW");
	this.shape_16.setTransform(-21.8,406);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#333333").ss(1,1,1).p("EADjA/cQj0uthzxqQhqwdAOxpQAOwvB4v2QB0veDJsX");
	this.shape_17.setTransform(-21.7,406);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#333333").ss(1,1,1).p("EADhA/cQjyuuhyxpQhpwdAOxpQANwuB4v3QBzvdDHsY");
	this.shape_18.setTransform(-21.5,406);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#333333").ss(1,1,1).p("EADeA/cQjvuvhwxoQhoweAOxoQANwuB2v3QBxvcDFsZ");
	this.shape_19.setTransform(-21.2,406);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#333333").ss(1,1,1).p("EADaA/cQjruxhuxnQhnweAPxmQAMwuB0v4QBwvaDBsb");
	this.shape_20.setTransform(-20.9,406);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#333333").ss(1,1,1).p("EADWA/cQjnuzhrxlQhlweAOxmQANwsBxv5QBtvZC+sd");
	this.shape_21.setTransform(-20.4,406);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#333333").ss(1,1,1).p("EADQA/cQjgu2hpxiQhiwfAOxlQAMwsBuv4QBrvXC4sg");
	this.shape_22.setTransform(-19.9,406);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#333333").ss(1,1,1).p("EADJA/cQjZu4hlxgQhewgANxjQAMwrBqv6QBnvTCysk");
	this.shape_23.setTransform(-19.2,406);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#333333").ss(1,1,1).p("EADAA/cQjPu8hhxcQhawhAMxhQAMwqBlv7QBjvQCqso");
	this.shape_24.setTransform(-18.4,406);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#333333").ss(1,1,1).p("EAC2A/cQjEvAhcxZQhWwiAMxfQALwoBhv8QBdvMChst");
	this.shape_25.setTransform(-17.4,406);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#333333").ss(1,1,1).p("EACqA/cQi3vFhWxUQhRwjAMxdQAKwnBav8QBXvICXsz");
	this.shape_26.setTransform(-16.3,406);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#333333").ss(1,1,1).p("EACdA/cQipvLhPxPQhKwkAKxaQAKwlBTv+QBQvDCLs5");
	this.shape_27.setTransform(-15,406);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#333333").ss(1,1,1).p("EACOA/cQiYvShIxIQhCwnAJxWQAIwiBLwAQBJu9B9tB");
	this.shape_28.setTransform(-13.6,406);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#333333").ss(1,1,1).p("EAB8A/cQiFvZg/xCQg6woAIxSQAHwhBCwBQA/u2ButK");
	this.shape_29.setTransform(-11.9,406);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#333333").ss(1,1,1).p("EABpA/cQhxvig1w5QgxwrAHxPQAGwdA3wDQA2uvBdtT");
	this.shape_30.setTransform(-10,406);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#333333").ss(1,1,1).p("EABYA/cQhevpgswyQgqwuAGxKQAFwbAvwFQAsuoBOtc");
	this.shape_31.setTransform(-8.4,406);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#333333").ss(1,1,1).p("EABIA/cQhNvwgkwsQgiwvAFxHQAEwYAmwHQAkujBAtj");
	this.shape_32.setTransform(-6.9,406);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#333333").ss(1,1,1).p("EAA7A/cQg/v2gdwmQgcwxAExEQADwXAgwIQAdudA0tq");
	this.shape_33.setTransform(-5.6,406);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#333333").ss(1,1,1).p("EAAvA/cQgyv7gYwhQgWwzADxBQADwVAZwJQAXuZAqtw");
	this.shape_34.setTransform(-4.5,406);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#333333").ss(1,1,1).p("EAAlA/cQgnv/gTweQgRwzACxAQACwTAUwKQASuVAht1");
	this.shape_35.setTransform(-3.5,406);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#333333").ss(1,1,1).p("EAAcA/cQgdwDgPwaQgNw1ACw9QABwSAQwLQANuSAZt5");
	this.shape_36.setTransform(-2.7,406);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#333333").ss(1,1,1).p("EAAVA/cQgWwGgLwXQgKw2ACw7QABwSALwMQAKuPATt8");
	this.shape_37.setTransform(-2,406);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#333333").ss(1,1,1).p("EAAQA/cQgQwIgIwVQgHw3ABw6QABwRAIwMQAHuNAOt/");
	this.shape_38.setTransform(-1.5,406);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#333333").ss(1,1,1).p("EAALA/cQgLwLgFwSQgFw3AAw6QABwQAGwMQAFuMAJuB");
	this.shape_39.setTransform(-1.1,406);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#333333").ss(1,1,1).p("EAAIA/cQgIwMgDwSQgEw3ABw5QAAwPAEwNQADuKAHuD");
	this.shape_40.setTransform(-0.7,406);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#333333").ss(1,1,1).p("EAAFA/cQgFwNgBwRQgDw3ABw5QAAwOACwOQACuJAEuE");
	this.shape_41.setTransform(-0.5,406);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#333333").ss(1,1,1).p("EAADA/cQgDwOAAwQQgCw3ABw4QAAwOABwPQAAuIADuF");
	this.shape_42.setTransform(-0.3,406);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#333333").ss(1,1,1).p("EAABA/cQgBwOAAwQQgBw4ABw3QAAwOAAwOQAAuIABuG");
	this.shape_43.setTransform(-0.1,406);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#333333").ss(1,1,1).p("EAAAA/cQAAwPAAwPQAAw4AAw3QAAwOAAwOQAAuIAAuG");
	this.shape_44.setTransform(0,406);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#333333").ss(1,1,1).p("EAAAA/cQAAwPAAwPQAAw4AAw3QAAwOAAwOQAAuHAAuH");
	this.shape_45.setTransform(0,406);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1,-1,2,814);


(lib.lettres_FAT = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#5AB6B2").s().p("AixBDIAAiFIFjAAIAACFg");
	this.shape.setTransform(170.9,192.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#5AB6B2").s().p("ACnFeIlMm2IAAG2IicAAIAAq7ICSAAIFWHBIAAnBICbAAIAAK7g");
	this.shape_1.setTransform(93.5,187.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#5AB6B2").s().p("ACmFdIlMm0IAAG0IicAAIAAq5ICSAAIFWHBIAAnBICdAAIAAK5g");
	this.shape_2.setTransform(26.4,63.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#5AB6B2").s().p("ADTFdIhBiXIkkAAIhBCXIilAAIEtq5ICWAAIEvK5gAhXA9ICuAAIhWjJg");
	this.shape_3.setTransform(-63.7,63.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.lettres_FAT, new cjs.Rectangle(-102.9,0,312.5,249.8), null);


(lib.hastaggraphicbranche = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AjQAuIAShbIDlAAICqAAIgSBbg");
	this.shape.setTransform(0,4.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.hastaggraphicbranche, new cjs.Rectangle(-20.8,0,41.7,9.2), null);


(lib.fenetresdefilantes = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_15
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFF100").s().p("AiFl5IELhuIAAMkIkLCrg");
	this.shape.setTransform(-529.6,462.1);
	this.shape._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(30).to({_off:false},0).to({_off:true},10).wait(20));

	// Layer_12
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFF100").s().p("AiFl5IELhuIAAMkIkLCrg");
	this.shape_1.setTransform(-312.8,952.4);
	this.shape_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(27).to({_off:false},0).to({_off:true},10).wait(23));

	// Layer_14
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFF100").s().p("AiFl5IELhuIAAMkIkLCrg");
	this.shape_2.setTransform(-187.6,306.7);
	this.shape_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(24).to({_off:false},0).to({_off:true},10).wait(26));

	// Layer_13
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFF100").s().p("AiFl5IELhuIAAMkIkLCrg");
	this.shape_3.setTransform(-170.6,673.2);
	this.shape_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape_3).wait(21).to({_off:false},0).to({_off:true},10).wait(29));

	// Layer_18
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFF100").s().p("AjDmeIGIigIgPPAIl5C9g");
	this.shape_4.setTransform(-7.8,441.9);
	this.shape_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape_4).wait(18).to({_off:false},0).to({_off:true},10).wait(32));

	// Layer_20
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFF100").s().p("AjEmeIGJigIgPPAIl6C9g");
	this.shape_5.setTransform(106.2,615.9);
	this.shape_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape_5).wait(15).to({_off:false},0).to({_off:true},10).wait(35));

	// Layer_21
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFF100").s().p("AjEmeIGJigIgPPAIl6C9g");
	this.shape_6.setTransform(206.2,355.8);
	this.shape_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape_6).wait(12).to({_off:false},0).to({_off:true},10).wait(38));

	// Layer_22
	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFF100").s().p("AjEmeIGJigIgPPAIl6C9g");
	this.shape_7.setTransform(335.3,303.6);
	this.shape_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape_7).wait(9).to({_off:false},0).to({_off:true},10).wait(41));

	// Layer_17
	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFF100").s().p("AjLnfIGXiSIAASoImXA7g");
	this.shape_8.setTransform(334.6,553.2);
	this.shape_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape_8).wait(6).to({_off:false},0).to({_off:true},10).wait(44));

	// Layer_16
	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFF100").s().p("AifnCIE/iDIAAO/Ik/DMg");
	this.shape_9.setTransform(559.2,90.2);
	this.shape_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape_9).wait(3).to({_off:false},0).to({_off:true},10).wait(47));

	// Layer_19
	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFF100").s().p("AjEmeIGJigIgPPAIl6C9g");
	this.shape_10.setTransform(734.3,470.7);

	this.timeline.addTween(cjs.Tween.get(this.shape_10).to({_off:true},10).wait(50));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(714.7,413.3,39.3,115);


(lib.couleur = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFA39").s().p("EhQjAHcMBLVhYKMBVyBJTMhLVBYKg");
	this.shape.setTransform(515.6,516.7);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.couleur, new cjs.Rectangle(0,0,1031.2,1033.5), null);


(lib.Btn_cliquable_area = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#5AB6B2").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape.setTransform(0,30);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Btn_cliquable_area, new cjs.Rectangle(-113,0,226,60), null);


(lib.bonhomme_vecto = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#5AB6B2").s().p("ACpHSIgZAAIhtmBIg2DRIiCCpIgQgHIBni6IAXjmIgfivIg0BAIghCrIgUgDIAQi8IBNjQIApgrIAOgXIAGhLQABgPAUgGQALgDAHAPQADAHACAIIADBNIAMAQIASAkIAiFQIBWG4g");
	this.shape.setTransform(0,46.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-17.6,0,35.2,93.3);


(lib.visuel_triangle = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.couleur();
	this.instance.parent = this;
	this.instance.setTransform(276.6,351.4,1,1,0,0,0,515.6,516.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(281));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-239,-165.3,1031.2,1033.5);


(lib.Symbole1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_4
	this.instance = new lib.couleur();
	this.instance.parent = this;
	this.instance.setTransform(276.6,351.4,1,1,0,0,0,515.6,516.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(281));

	// Calque_1
	this.instance_1 = new lib.couleur();
	this.instance_1.parent = this;
	this.instance_1.setTransform(276.6,351.4,1,1,0,0,0,515.6,516.7);
	this.instance_1.filters = [new cjs.ColorFilter(0, 0, 0, 1, 142, 203, 234, 0)];
	this.instance_1.cache(-2,-2,1035,1038);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(281));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-239,-165.3,1031.2,1033.5);


(lib.sucette_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_3
	this.instance = new lib.over();
	this.instance.parent = this;
	this.instance.setTransform(100.5,146.2,1,1,0,0,0,87.2,132.3);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleY:0.01,y:15.6},9,cjs.Ease.get(1)).to({_off:true},1).wait(79).to({_off:false,regY:131.3,scaleY:0.03,y:274.9},0).to({regY:132.3,scaleY:1,y:146.2},11,cjs.Ease.get(-1)).to({scaleY:0.01,y:15.6},11,cjs.Ease.get(1)).to({_off:true},1).wait(67).to({_off:false,regY:131.3,scaleY:0.03,y:274.9},0).to({regY:132.3,scaleY:1,y:146.2},11,cjs.Ease.get(-1)).to({scaleY:0.01,y:15.6},11,cjs.Ease.get(1)).to({_off:true},1).wait(67).to({_off:false,regY:131.3,scaleY:0.03,y:274.9},0).to({regY:132.3,scaleY:1,y:146.2},11,cjs.Ease.get(-1)).wait(1));

	// Calque_5
	this.instance_1 = new lib.affiche2019();
	this.instance_1.parent = this;
	this.instance_1.setTransform(14,14,0.661,0.661);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({_off:true},100).wait(181));

	// Calque_4
	this.instance_2 = new lib.affiche2018();
	this.instance_2.parent = this;
	this.instance_2.setTransform(14,14,0.661,0.661);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(100).to({_off:false},0).to({_off:true},90).wait(91));

	// Calque_2
	this.instance_3 = new lib.affiche2017();
	this.instance_3.parent = this;
	this.instance_3.setTransform(14,14,0.661,0.661);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(190).to({_off:false},0).wait(91));

	// Calque_1
	this.instance_4 = new lib.sucette();
	this.instance_4.parent = this;
	this.instance_4.setTransform(0,0,0.661,0.661);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#042E23").s().p("AsWGQIoVmPQn8l8gSgTQAhALUGgMQAFgBFBBbQFABcAEgCQDTglAJgDQAEgBLvCwQLjCtAQABQgVAC0TCZI0oCbIAAAAg");
	this.shape.setTransform(217.3,377.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.instance_4}]}).wait(281));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,402.3,417.5);


(lib.project_illustration_placeHolder = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{close:60});

	// timeline functions:
	this.frame_59 = function() {
		this.stop();
	}
	this.frame_106 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(59).call(this.frame_59).wait(47).call(this.frame_106).wait(2));

	// Layer_2
	this.instance = new lib.visuel_triangle();
	this.instance.parent = this;
	this.instance.setTransform(378.6,239.9,1,1,-25.2,0,0,0,372.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(60).to({regX:-0.1,rotation:-37.7,x:-37.7,y:358.1},46,cjs.Ease.quartInOut).wait(2));

	// Calque_3
	this.instance_1 = new lib.sucette_1();
	this.instance_1.parent = this;
	this.instance_1.setTransform(423.2,246.7,1,1,0,0,0,201.2,208.7);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({regY:208.8,scaleX:0.63,scaleY:0.63,x:57.7,y:281.9},59,cjs.Ease.quartOut).to({x:123.7},47,cjs.Ease.quartInOut).to({_off:true},1).wait(1));

	// CITIA_2018_b.png
	this.instance_2 = new lib.visuel_bonhomme("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(320.1,355.3,1,1,0,0,0,0,75.3);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({x:211.1,y:349.3},59,cjs.Ease.quartOut).to({x:390.1,y:355.3},47,cjs.Ease.quartInOut).to({_off:true},1).wait(1));

	// CITIA_2018_c.png
	this.instance_3 = new lib.visuel_chateau("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(140.9,153.6,1,1,0,0,0,0,126.4);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({x:7,y:155.4},59,cjs.Ease.quartOut).to({x:140.9,y:153.6},47,cjs.Ease.quartInOut).to({_off:true},1).wait(1));

	// CITIA_2018_d.png
	this.instance_4 = new lib.visuel_montagne("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(170.2,95.6,1,1,0,0,0,0,111.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).to({x:115.2},59,cjs.Ease.quartOut).to({x:170.2},47,cjs.Ease.quartInOut).to({_off:true},1).wait(1));

	// Layer_7
	this.instance_5 = new lib.Symbole1();
	this.instance_5.parent = this;
	this.instance_5.setTransform(915,355.1,1,1,0,0,0,515.6,516.7);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).to({x:593,y:349.1},59,cjs.Ease.quartOut).to({regX:515.5,rotation:-45,x:476.8,y:149},47,cjs.Ease.quartInOut).to({_off:true},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-69.1,-583.9,1375.7,1374.4);


(lib.prject_illustration = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.illustration = new lib.project_illustration_placeHolder();
	this.illustration.name = "illustration";
	this.illustration.parent = this;
	this.illustration.setTransform(-5.8,-270.3,1,1,0,0,0,-15,-268.2);

	this.timeline.addTween(cjs.Tween.get(this.illustration).wait(140));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-60,-585.9,1375.7,1374.4);


(lib.masqueTexte = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// masqueTexte_1_ligne
	this.instance = new lib.masqueTexte_1_ligne();
	this.instance.parent = this;
	this.instance.setTransform(-218.8,191.5,0.008,1,0,0,0,0,25.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(6).to({_off:false},0).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.01,x:218.8},14).wait(1));

	// masqueTexte_1_ligne
	this.instance_1 = new lib.masqueTexte_1_ligne();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-218.8,136.1,0.008,1,0,0,0,0,25.5);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.01,x:218.8},14).to({_off:true},1).wait(2));

	// masqueTexte_1_ligne
	this.instance_2 = new lib.masqueTexte_1_ligne();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-218.8,80.8,0.008,1,0,0,0,0,25.5);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(2).to({_off:false},0).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.01,x:218.8},14).to({_off:true},1).wait(4));

	// masqueTexte_1_ligne
	this.instance_3 = new lib.masqueTexte_1_ligne();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-218.8,25.5,0.008,1,0,0,0,0,25.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.01,x:218.8},14).to({_off:true},1).wait(6));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-220.5,-1.9,3.5,53);


(lib.lignes_all = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":114,close_lines:157});

	// timeline functions:
	this.frame_113 = function() {
		this.stop();
	}
	this.frame_156 = function() {
		this.stop();
	}
	this.frame_174 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(113).call(this.frame_113).wait(43).call(this.frame_156).wait(18).call(this.frame_174).wait(1));

	// ligne
	this.instance = new lib.ligne();
	this.instance.parent = this;
	this.instance.setTransform(-454,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(114).to({x:-883},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:-454},0).wait(18));

	// ligne
	this.instance_1 = new lib.ligne();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-227,406.5,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(114).to({x:-883.2},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:-227},0).wait(18));

	// ligne
	this.instance_2 = new lib.ligne();
	this.instance_2.parent = this;
	this.instance_2.setTransform(0,406.5,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(114).to({x:-883.4},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:0},0).wait(18));

	// ligne
	this.instance_3 = new lib.ligne();
	this.instance_3.parent = this;
	this.instance_3.setTransform(227,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(93).to({y:322},20,cjs.Ease.quartInOut).wait(1).to({y:323},0).to({x:-883,y:406},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:227,y:322},0).to({y:406},16,cjs.Ease.quartInOut).wait(2));

	// ligne
	this.instance_4 = new lib.ligne();
	this.instance_4.parent = this;
	this.instance_4.setTransform(454.1,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(114).to({x:-883.4},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:454.1},0).wait(18));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-454.5,-0.5,909.2,813.5);


(lib.hashtag_graphic = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(64));

	// hastag graphic branche
	this.instance = new lib.hastaggraphicbranche();
	this.instance.parent = this;
	this.instance.setTransform(37.4,51.9,1.084,0.995,0,-78.3,102.1,0.1,4.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({x:6.5,y:50.1},17,cjs.Ease.quartOut).to({_off:true},5).wait(3).to({_off:false},0).to({_off:true},4).wait(2).to({_off:false,x:26.5},0).to({_off:true},5).wait(28));

	// hastag graphic branche
	this.instance_1 = new lib.hastaggraphicbranche();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-41.6,49.9,1.084,0.995,0,-78.3,102.1,0.1,4.6);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off:false},0).to({x:-7.4,y:49.8},18,cjs.Ease.quartOut).to({_off:true},4).wait(5).to({_off:false},0).to({_off:true},4).wait(31));

	// hastag graphic branche
	this.instance_2 = new lib.hastaggraphicbranche();
	this.instance_2.parent = this;
	this.instance_2.setTransform(2.7,21.7,1,1,0,0,0,0,4.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({x:-2.3,y:58.7},13,cjs.Ease.quartOut).to({_off:true},11).wait(3).to({_off:false,scaleX:2.33,scaleY:1.03,skewX:-12.8,x:25.5},0).to({_off:true},4).wait(33));

	// hastag graphic branche
	this.instance_3 = new lib.hastaggraphicbranche();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-7.3,77.7,1,1,0,0,0,0,4.6);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({x:0.7,y:41.7},14,cjs.Ease.quartOut).to({_off:true},4).wait(7).to({_off:false,regX:0.1,scaleX:1.98,scaleY:1.01,skewX:-9.8,x:19.9,y:49.7},0).to({_off:true},4).wait(4).to({_off:false,regX:0,scaleX:1,scaleY:1,skewX:0,x:0.7,y:41.7},0).to({_off:true},4).wait(21));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-18.1,17.1,64.6,57.7);


(lib.Btn_next = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{rollOver:55,rollOut:70});

	// timeline functions:
	this.frame_54 = function() {
		this.stop();
	}
	this.frame_69 = function() {
		this.stop();
	}
	this.frame_84 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(54).call(this.frame_54).wait(15).call(this.frame_69).wait(15).call(this.frame_84).wait(1));

	// ZONE
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.008)").s().p("EgjTAEsIAApXMBGnAAAIAAJXg");
	this.shape.setTransform(113,29.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(85));

	// txt
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgIAqIAAhDIgYAAIAAgQIBBAAIAAAQIgYAAIAABDg");
	this.shape_1.setTransform(161.1,29.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AASAqIgSgcIgQAcIgYAAIAdgqIgagpIAWAAIAPAZIAQgZIAWAAIgaApIAcAqg");
	this.shape_2.setTransform(145.2,29.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgeAqIAAhTIA8AAIAAARIgpAAIAAARIAkAAIAAAPIgkAAIAAASIAqAAIAAAQg");
	this.shape_3.setTransform(129.3,29.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAUAqIgngzIAAAzIgTAAIAAhTIASAAIAoA1IAAg1IATAAIAABTg");
	this.shape_4.setTransform(112.3,29.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgKAQIAEgOQgEgCAAgFQAAgEADgDQADgDAEAAQAFAAADADQADADAAAEQAAAEgDAEIgIANg");
	this.shape_5.setTransform(86.5,32.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAPAqIgYghIgIAKIAAAXIgTAAIAAhTIATAAIAAAjIAegjIAYAAIgiAlIAQAWIASAYg");
	this.shape_6.setTransform(77,29.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgeAfQgNgNAAgSQAAgSANgMQANgNARAAQASAAAOANQAMAMAAASQAAASgMANQgOAMgSAAQgRAAgNgMgAgRgSQgHAIAAAKQAAALAHAIQAIAIAJAAQAKAAAIgIQAHgIAAgLQAAgKgHgIQgIgIgKAAQgJAAgIAIg");
	this.shape_7.setTransform(59.3,29.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#333333").ss(1,1,1).p("EgjOAADMBGdgAF");
	this.shape_8.setTransform(113,-0.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(85));

	// Layer_2
	this.instance = new lib.Btn_cliquable_area();
	this.instance.parent = this;
	this.instance.setTransform(113,29.5,2,1,0,0,0,0,30);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("rgba(0,0,0,0.008)").s().p("EgjTAEsIAApXMBGnAAAIAAJXg");
	this.shape_9.setTransform(113,29.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance}]},3).to({state:[{t:this.instance}]},46).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.instance}]},5).to({state:[{t:this.instance}]},14).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},14).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({alpha:0.012},46).to({_off:true},1).wait(5).to({_off:false},0).to({alpha:1},14).wait(1).to({alpha:0.012},14).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-113.5,-1.5,453,61);


(lib.Btn_detail = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"rollOver":55,"rollOut":70});

	// timeline functions:
	this.frame_54 = function() {
		this.stop();
	}
	this.frame_69 = function() {
		this.stop();
	}
	this.frame_84 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(54).call(this.frame_54).wait(15).call(this.frame_69).wait(15).call(this.frame_84).wait(1));

	// ZONE
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.008)").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape.setTransform(0,29.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(85));

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgOAyIAAhkIAdAAIAABkg");
	this.shape_1.setTransform(-85.4,29.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgyAOIAAgcIBlAAIAAAcg");
	this.shape_2.setTransform(-85.4,29.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgaAqIAAhTIASAAIAABDIAjAAIAAAQg");
	this.shape_3.setTransform(38.4,29.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgIAqIAAhTIARAAIAABTg");
	this.shape_4.setTransform(24.7,29.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AAZAqIgHgSIgjAAIgHASIgUAAIAkhTIARAAIAkBTgAgKAIIAUAAIgKgYg");
	this.shape_5.setTransform(10.2,29.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgIAqIAAhDIgYAAIAAgQIBBAAIAAAQIgYAAIAABDg");
	this.shape_6.setTransform(-5.9,29.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgeAqIAAhTIA8AAIAAARIgpAAIAAARIAkAAIAAAPIgkAAIAAASIAqAAIAAAQg");
	this.shape_7.setTransform(-21.3,29.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AglAqIAAhTIAeAAQAVAAAMALQAMALAAATQAAATgMALQgMAMgXAAgAgTAaIANAAQAMAAAGgHQAHgGAAgNQAAgLgHgHQgGgGgOgBIgLAAg");
	this.shape_8.setTransform(-37.8,29.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#333333").ss(1,1,1).p("AxpAAMAjTAAA");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(85));

	// Layer_2
	this.instance = new lib.Btn_cliquable_area();
	this.instance.parent = this;
	this.instance.setTransform(0,29.5,1,1,0,0,0,0,30);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("rgba(0,0,0,0.008)").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape_10.setTransform(0,29.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance}]},3).to({state:[{t:this.instance}]},45).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.instance}]},6).to({state:[{t:this.instance}]},14).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},13).wait(2));
	this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({alpha:0.012},45).to({_off:true},1).wait(6).to({_off:false},0).to({alpha:1},14).wait(1).to({alpha:0.012},13).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-114,-1,228,60.5);


(lib.bloc_visuel = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":68});

	// timeline functions:
	this.frame_67 = function() {
		this.stop();
	}
	this.frame_68 = function() {
		this.illustration_container.illustration.gotoAndPlay("close");
	}
	this.frame_120 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(67).call(this.frame_67).wait(1).call(this.frame_68).wait(52).call(this.frame_120).wait(1));

	// masque VISUEL (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("EgVdBDvI8dAAMAAAiHdIcdAAMBHYAAAMAAACHdg");
	mask.setTransform(-319.8,-19);

	// VISUEL
	this.illustration_container = new lib.prject_illustration();
	this.illustration_container.name = "illustration_container";
	this.illustration_container.parent = this;
	this.illustration_container.setTransform(242.6,-660.6,1,1,0,0,0,11.6,-308.4);
	this.illustration_container._off = true;

	var maskedShapeInstanceList = [this.illustration_container];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.illustration_container).wait(26).to({_off:false},0).to({regX:11.7,regY:-308.3,x:-85.5,y:-660.5},33,cjs.Ease.quartOut).wait(15).to({x:-1208.6},46,cjs.Ease.quartInOut).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.bloc_video = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":41});

	// timeline functions:
	this.frame_40 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(40).call(this.frame_40).wait(17));

	// masque video (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_42 = new cjs.Graphics().p("Ega8A8AMAAAh3/MA7rAAAMAAAB3/g");
	var mask_graphics_43 = new cjs.Graphics().p("EgYrA8AMAAAh3/MA3HAAAMAAAB3/g");
	var mask_graphics_44 = new cjs.Graphics().p("EgWaA8AMAAAh3/MAyiAAAMAAAB3/g");
	var mask_graphics_45 = new cjs.Graphics().p("EgUJA8AMAAAh3/MAt+AAAMAAAB3/g");
	var mask_graphics_46 = new cjs.Graphics().p("EgR4A8AMAAAh3/MApaAAAMAAAB3/g");
	var mask_graphics_47 = new cjs.Graphics().p("EgPnA8AMAAAh3/MAk1AAAMAAAB3/g");
	var mask_graphics_48 = new cjs.Graphics().p("EgNWA8AMAAAh3/MAgRAAAMAAAB3/g");
	var mask_graphics_49 = new cjs.Graphics().p("EgLFA8AMAAAh3/IbtAAMAAAB3/g");
	var mask_graphics_50 = new cjs.Graphics().p("EgI0A8AMAAAh3/IXIAAMAAAB3/g");
	var mask_graphics_51 = new cjs.Graphics().p("EgGjA8AMAAAh3/ISkAAMAAAB3/g");
	var mask_graphics_52 = new cjs.Graphics().p("EgESA8AMAAAh3/IOAAAMAAAB3/g");
	var mask_graphics_53 = new cjs.Graphics().p("EgCBA8AMAAAh3/IJbAAMAAAB3/g");
	var mask_graphics_54 = new cjs.Graphics().p("EAAPA8AMAAAh3/IE4AAMAAAB3/g");
	var mask_graphics_55 = new cjs.Graphics().p("EACgA8AMAAAh3/IAUAAMAAAB3/g");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(42).to({graphics:mask_graphics_42,x:209.5,y:0}).wait(1).to({graphics:mask_graphics_43,x:194.8,y:0}).wait(1).to({graphics:mask_graphics_44,x:180,y:0}).wait(1).to({graphics:mask_graphics_45,x:165.3,y:0}).wait(1).to({graphics:mask_graphics_46,x:150.6,y:0}).wait(1).to({graphics:mask_graphics_47,x:135.8,y:0}).wait(1).to({graphics:mask_graphics_48,x:121.1,y:0}).wait(1).to({graphics:mask_graphics_49,x:106.4,y:0}).wait(1).to({graphics:mask_graphics_50,x:91.6,y:0}).wait(1).to({graphics:mask_graphics_51,x:76.9,y:0}).wait(1).to({graphics:mask_graphics_52,x:62.2,y:0}).wait(1).to({graphics:mask_graphics_53,x:47.4,y:0}).wait(1).to({graphics:mask_graphics_54,x:32.7,y:0}).wait(1).to({graphics:mask_graphics_55,x:18,y:0}).wait(1).to({graphics:null,x:0,y:0}).wait(1));

	// VIDEO
	this.videoContainer = new lib.video();
	this.videoContainer.name = "videoContainer";
	this.videoContainer.parent = this;
	this.videoContainer.setTransform(36.7,765,0.659,0.659,0,0,0,0.3,480.2);

	var maskedShapeInstanceList = [this.videoContainer];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.videoContainer).to({y:-33},40,cjs.Ease.quartInOut).to({_off:true},16).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(36.5,448.7,382.1,632.3);


(lib.bloc_NEXT_btn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":20});

	// timeline functions:
	this.frame_19 = function() {
		this.stop();
	}
	this.frame_36 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(19).call(this.frame_19).wait(17).call(this.frame_36).wait(1));

	// CTA DETAIL
	this.btnNext = new lib.Btn_next();
	this.btnNext.name = "btnNext";
	this.btnNext.parent = this;
	this.btnNext.setTransform(-340.5,411.5,1,1,0,0,0,0,26.5);

	this.timeline.addTween(cjs.Tween.get(this.btnNext).to({y:350.5},19,cjs.Ease.quartInOut).wait(2).to({y:411.5},14,cjs.Ease.quartInOut).to({_off:true},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-453.5,384,452,60.5);


(lib.bloc_hashtag = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_61 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(61).call(this.frame_61).wait(2));

	// Layer_1
	this.instance = new lib.hashtag_graphic("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(-456,243.2,2.317,2.317,0,0,0,-0.1,50.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true},61).wait(2));

	// Layer_5 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_28 = new cjs.Graphics().p("AtWBpIAAjRIatAAIAADRg");
	var mask_graphics_32 = new cjs.Graphics().p("AtWDSIAAmjIatAAIAAGjg");
	var mask_graphics_40 = new cjs.Graphics().p("AtWGYIAAsvIatAAIAAMvg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(28).to({graphics:mask_graphics_28,x:-400.4,y:265.5}).wait(4).to({graphics:mask_graphics_32,x:-400.4,y:255}).wait(8).to({graphics:mask_graphics_40,x:-400.4,y:255.9}).wait(23));

	// Layer_3
	this.instance_1 = new lib.txt_hashtag();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-421.5,244.1,1,1,0,0,0,0,31.6);
	this.instance_1._off = true;

	var maskedShapeInstanceList = [this.instance_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(28).to({_off:false},0).to({_off:true},34).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-497.8,166.1,149.7,129.4);


(lib.bloc_CTA = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":25});

	// timeline functions:
	this.frame_24 = function() {
		this.stop();
	}
	this.frame_41 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(24).call(this.frame_24).wait(17).call(this.frame_41).wait(1));

	// CTA DETAIL
	this.btnCta = new lib.Btn_detail();
	this.btnCta.name = "btnCta";
	this.btnCta.parent = this;
	this.btnCta.setTransform(-340.5,411.5,1,1,0,0,0,0,26.5);

	this.timeline.addTween(cjs.Tween.get(this.btnCta).to({y:350.5},24,cjs.Ease.quartInOut).wait(2).to({y:411.5},14,cjs.Ease.quartInOut).to({_off:true},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-454,384.5,227,60);


(lib.blocInfostextes = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":80});

	// timeline functions:
	this.frame_79 = function() {
		this.stop();
	}
	this.frame_120 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(79).call(this.frame_79).wait(41).call(this.frame_120).wait(1));

	// Bloc Hashtag
	this.instance = new lib.bloc_hashtag();
	this.instance.parent = this;
	this.instance.setTransform(-391.5,261.2,0.9,0.9,0,0,0,-412.9,244.7);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(62).to({_off:false},0).to({_off:true},18).wait(41));

	// masque Titre
	this.instance_1 = new lib.masque_generique();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-527.9,-13.9,0.007,0.471,0,0,0,0,17.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(34).to({_off:false},0).to({regX:0.5,scaleX:1.92,x:-527.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.01,x:-228.9},10,cjs.Ease.quartInOut).to({_off:true},1).wait(26).to({_off:false},0).to({regX:0.5,scaleX:1.92,x:-527.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.01,x:-528.7},10,cjs.Ease.quartInOut).to({_off:true},1).wait(21));

	// _NOM-du-CLIENT
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#5AB6B2").s().p("AgjA4IAAhvIAZAAIAABYIAuAAIAAAXg");
	this.shape.setTransform(-204.7,-14.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#5AB6B2").s().p("AAiA4IgLgYIgtAAIgKAYIgbAAIAwhvIAXAAIAwBvgAgNAKIAbAAIgOgfg");
	this.shape_1.setTransform(-227.7,-14.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#5AB6B2").s().p("AgJA4IgthvIAbAAIAbBGIAchGIAbAAIgtBvg");
	this.shape_2.setTransform(-251.1,-14.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#5AB6B2").s().p("AgMA4IAAhvIAYAAIAABvg");
	this.shape_3.setTransform(-271.2,-14.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#5AB6B2").s().p("AgMA4IAAhZIgfAAIAAgWIBXAAIAAAWIggAAIAABZg");
	this.shape_4.setTransform(-290.8,-14.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#5AB6B2").s().p("AgrAmIAOgSQATAQAOAAQAHAAAEgDQAEgDAAgGQAAgEgFgDQgEgDgLgDQgUgFgJgGQgJgIAAgQQAAgPAMgKQALgIAQAAQAMAAALAEQALAEAIAHIgMASQgPgLgPAAQgGAAgDADQgEADAAAFQAAAFAEADQAFACAPAFQAQAEAJAGQAIAIAAAPQAAAPgLAJQgLAKgTAAQgZAAgVgUg");
	this.shape_5.setTransform(-312.8,-14.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#5AB6B2").s().p("AgoA4IAAhvIBQAAIAAAXIg3AAIAAAWIAxAAIAAAUIgxAAIAAAYIA4AAIAAAWg");
	this.shape_6.setTransform(-334.8,-14.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#5AB6B2").s().p("AglA4IAAhvIBLAAIAAAWIgyAAIAAAYIAwAAIAAAVIgwAAIAAAsg");
	this.shape_7.setTransform(-356.9,-14.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#5AB6B2").s().p("AgLA4IAAgsIgnhDIAbAAIAXAqIAYgqIAbAAIgmBDIAAAsg");
	this.shape_8.setTransform(-395.3,-14.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#5AB6B2").s().p("AgiApQgSgQABgYQgBgZASgQQAQgRAYAAQAcAAASAVIgPASQgMgPgSAAQgMAAgKAJQgKAJAAAPQAAAPAJAKQAJAKAMAAQATAAAMgPIAPARQgSAVgaAAQgZAAgQgRg");
	this.shape_9.setTransform(-417.9,-14.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#5AB6B2").s().p("AgoA4IAAhvIBQAAIAAAXIg3AAIAAAWIAxAAIAAAUIgxAAIAAAYIA4AAIAAAWg");
	this.shape_10.setTransform(-440.8,-14.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#5AB6B2").s().p("AAaA4IgzhFIAABFIgZAAIAAhvIAWAAIA2BHIAAhHIAZAAIAABvg");
	this.shape_11.setTransform(-464.8,-14.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#5AB6B2").s().p("AAaA4Ig0hFIAABFIgZAAIAAhvIAXAAIA2BHIAAhHIAaAAIAABvg");
	this.shape_12.setTransform(-489.9,-14.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#5AB6B2").s().p("AAiA4IgKgYIgvAAIgKAYIgbAAIAxhvIAXAAIAxBvgAgNAKIAbAAIgOgfg");
	this.shape_13.setTransform(-514.4,-14.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},34).to({state:[]},55).to({state:[]},1).wait(31));

	// DECO 1
	this.instance_2 = new lib.bonhomme_vecto("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(204.1,170.9,0.7,0.7,0,0,0,-0.1,46.7);

	this.instance_3 = new lib.bonhomme_vecto("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(204.1,62.3,0.7,0.7,0,0,0,-0.1,46.6);

	this.instance_4 = new lib.bonhomme_vecto("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(204.1,-46.2,0.7,0.7,0,0,0,-0.1,46.6);

	this.instance_5 = new lib.bonhomme_vecto("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(143.9,105.6,0.7,0.7,0,0,0,-0.1,46.7);

	this.instance_6 = new lib.bonhomme_vecto("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(143.9,-3,0.7,0.7,0,0,0,-0.1,46.6);

	this.instance_7 = new lib.bonhomme_vecto("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(143.9,-111.5,0.7,0.7,0,0,0,-0.1,46.6);

	this.instance_8 = new lib.bonhomme_vecto("synched",0);
	this.instance_8.parent = this;
	this.instance_8.setTransform(5.6,170.9,0.7,0.7,0,0,0,0.1,46.7);

	this.instance_9 = new lib.bonhomme_vecto("synched",0);
	this.instance_9.parent = this;
	this.instance_9.setTransform(5.6,62.3,0.7,0.7,0,0,0,0.1,46.6);

	this.instance_10 = new lib.bonhomme_vecto("synched",0);
	this.instance_10.parent = this;
	this.instance_10.setTransform(5.6,-46.2,0.7,0.7,0,0,0,0.1,46.6);

	this.instance_11 = new lib.bonhomme_vecto("synched",0);
	this.instance_11.parent = this;
	this.instance_11.setTransform(-54.6,105.6,0.7,0.7,0,0,0,0,46.7);

	this.instance_12 = new lib.bonhomme_vecto("synched",0);
	this.instance_12.parent = this;
	this.instance_12.setTransform(-54.6,-3,0.7,0.7,0,0,0,0,46.6);

	this.instance_13 = new lib.bonhomme_vecto("synched",0);
	this.instance_13.parent = this;
	this.instance_13.setTransform(-54.6,-111.5,0.7,0.7,0,0,0,0,46.6);

	this.instance_14 = new lib.bonhomme_vecto("synched",0);
	this.instance_14.parent = this;
	this.instance_14.setTransform(-373.7,142.8,0.7,0.7,0,0,0,0.1,46.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_7,p:{x:143.9,regX:-0.1,regY:46.6,y:-111.5}},{t:this.instance_6,p:{x:143.9,regX:-0.1,regY:46.6,y:-3}},{t:this.instance_5,p:{x:143.9,regX:-0.1,y:105.6}},{t:this.instance_4,p:{x:204.1,regX:-0.1,regY:46.6,y:-46.2}},{t:this.instance_3,p:{x:204.1,regX:-0.1,regY:46.6,y:62.3}},{t:this.instance_2,p:{x:204.1,regX:-0.1,y:170.9}}]},15).to({state:[{t:this.instance_13,p:{regX:0,x:-54.6,y:-111.5}},{t:this.instance_12,p:{regX:0,regY:46.6,x:-54.6,y:-3}},{t:this.instance_11,p:{regX:0,x:-54.6,y:105.6}},{t:this.instance_10,p:{regX:0.1,x:5.6,y:-46.2}},{t:this.instance_9,p:{regX:0.1,regY:46.6,x:5.6,y:62.3}},{t:this.instance_8,p:{regX:0.1,x:5.6,y:170.9}},{t:this.instance_7,p:{x:-169.5,regX:-0.1,regY:46.6,y:-111.5}},{t:this.instance_6,p:{x:-169.5,regX:-0.1,regY:46.6,y:-3}},{t:this.instance_5,p:{x:-169.5,regX:-0.1,y:105.6}},{t:this.instance_4,p:{x:-109.3,regX:-0.1,regY:46.6,y:-46.2}},{t:this.instance_3,p:{x:-109.3,regX:-0.1,regY:46.6,y:62.3}},{t:this.instance_2,p:{x:-109.3,regX:-0.1,y:170.9}}]},7).to({state:[{t:this.instance_14},{t:this.instance_13,p:{regX:0.1,x:-313.5,y:99.5}},{t:this.instance_12,p:{regX:0.1,regY:46.7,x:-313.5,y:208.1}},{t:this.instance_11,p:{regX:-0.1,x:-488.6,y:142.8}},{t:this.instance_10,p:{regX:-0.1,x:-428.4,y:99.5}},{t:this.instance_9,p:{regX:-0.1,regY:46.7,x:-428.4,y:208.1}},{t:this.instance_8,p:{regX:0,x:-89.5,y:99.6}},{t:this.instance_7,p:{x:-89.5,regX:0,regY:46.7,y:208.1}},{t:this.instance_6,p:{x:-29.3,regX:0,regY:46.7,y:164.9}},{t:this.instance_5,p:{x:-204.3,regX:0,y:99.6}},{t:this.instance_4,p:{x:-204.3,regX:0,regY:46.7,y:208.1}},{t:this.instance_3,p:{x:-144.1,regX:0,regY:46.7,y:164.9}},{t:this.instance_2,p:{x:-257.5,regX:0,y:164.9}}]},8).to({state:[]},11).wait(80));

	// DECO 2
	this.instance_15 = new lib.fenetresdefilantes("synched",0);
	this.instance_15.parent = this;
	this.instance_15.setTransform(-275.5,54.5,0.384,0.384,0,0,0,-0.1,500.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#FFFFFF").ss(1,1,1).p("AW3ISI4mONI1HzBILk58IcQC+g");
	this.shape_14.setTransform(-22.7,68.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#FFFFFF").ss(1,1,1).p("AnFmSICWm8ISbU+I7XFfIAAgB");
	this.shape_15.setTransform(-240,-49.6);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AqGJxIGnzhIAAAAINmPeI0MEDg");
	this.shape_16.setTransform(-262.9,-27.5);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#FFFFFF").ss(1,1,1).p("ALnNZI3NtWIXJtbg");
	this.shape_17.setTransform(-577,114.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_15}]},9).to({state:[]},40).to({state:[{t:this.shape_14}]},44).to({state:[{t:this.shape_16},{t:this.shape_15}]},4).to({state:[{t:this.shape_17}]},3).to({state:[]},6).to({state:[]},1).wait(14));

	// masque Titre (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_17 = new cjs.Graphics().p("EgpOArJMAAAhWRMAvJAAAMAAABWRg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(17).to({graphics:mask_graphics_17,x:-263.9,y:-6.1}).wait(24).to({graphics:null,x:0,y:0}).wait(80));

	// _LETTRES_ANARCHIQUES
	this.instance_16 = new lib.lettres_FAT();
	this.instance_16.parent = this;
	this.instance_16.setTransform(-433.8,-73,1,1,0,0,0,0,186.8);
	this.instance_16._off = true;

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#5AB6B2").s().p("AhNFeIAAkUIjymmICpAAICWEEICXkEICpAAIjyGmIAAEUg");
	this.shape_18.setTransform(-196.7,7.9);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#5AB6B2").s().p("AjeEDQhphmAAibQAAibBrhnQBrhpCYAAQCtAABzCEIhhBuQhJhchvAAQhXAAg+A5Qg/A5AABhQABBhA7A8QA7A6BTAAQByABBKhcIBkBnQh4CGihAAQihABhohng");
	this.shape_19.setTransform(-359.2,-116.4);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#5AB6B2").s().p("AkBFdIAAq5IH3AAIAACKIlbAAIAACQIE4AAIAACEIk4AAIAACRIFmAAIAACKg");
	this.shape_20.setTransform(-439.4,-116);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#5AB6B2").s().p("AixBCIAAiDIFjAAIAACDg");
	this.shape_21.setTransform(-510.3,-111.1);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFF00").s().p("Ah2FbQgugPgXgOQgXgOgegYIBPhuIASAQQALAJAiARQAgAPAeAAQBAAAAjgrQAjgrAJhMQhAAshKgBQhhAAhChAQhCg/AAhrQABhqBGhAQBHhBBrAAQBIABA3AcQA2AcAfA0QA8BiAACWQAABegYBJQgYBIgoAqQhRBUh1AAQgxAAgsgOgAhVjJQgcAdgBAsQAAArAaAfQAaAeAvAAQAtAAAggbQAegbAAgrQAAgtgdgfQgeghgsABQguAAgcAcg");
	this.shape_22.setTransform(50.4,106.4);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFF00").s().p("AixBCIAAiDIFjAAIAACDg");
	this.shape_23.setTransform(-17.4,111.7);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFF00").s().p("AjAEqQhKg+AAhdQAAhsBXhCQg/g8AAhQQAAhQBGg3QBFg3BnAAQBoAABFA3QBFA4AABQQAABQg+A7QBXBCAABsQAABdhKA+QhKA/h3AAQh2AAhKg/gAhMBHQgiAXAAAuQAAAsAjAYQAjAYAoAAQApAAAjgYQAjgYAAgsQAAgugigXQgigZgrAAQgqAAgiAZgAhCjYQgcAYAAAlQAAAjAdAXQAcAYAlABQAmgBAcgYQAdgXAAgjQAAglgcgYQgcgYgnAAQgmAAgcAYg");
	this.shape_24.setTransform(-86,106.4);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFF00").s().p("AixBCIAAiDIFjAAIAACDg");
	this.shape_25.setTransform(-154.6,111.7);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFF00").s().p("AjIFdIENozIiuAAIAABaIiSAAIAAjgIH4AAIAAB2IkbJDg");
	this.shape_26.setTransform(-219,106.8);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFF00").s().p("AgIFeIAAo1IiFAAIAAiGIEbAAIAAK7g");
	this.shape_27.setTransform(-227.5,-17.1);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFF00").s().p("AjYEHQhIhjAAikQAAijBIhjQBIhjCQAAQCRAABIBjQBIBjAACjQAACkhIBjQhIBjiRAAQiQAAhIhjgAhminQghBAAABoQAABpAhA/QAhBABFAAQBGAAAhhAQAhg/AAhpQAAhoghhAQghhAhGAAQhFAAghBAg");
	this.shape_28.setTransform(-483.3,-17.5);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFF00").s().p("AjwFmIAAh0IDOjTQA8g9AbgmQAbglAAgkQAAgigYgYQgXgYgiAAQhDABg6BeIh9hLQAwhMA7gmQA7gnBZgBQBaABBFA5QBFA5AABjQAAA2gcAyQgbAxhLBNIh9CCIEUAAIAACNg");
	this.shape_29.setTransform(-487.9,-141.8);

	var maskedShapeInstanceList = [this.instance_16,this.shape_18,this.shape_19,this.shape_20,this.shape_21,this.shape_22,this.shape_23,this.shape_24,this.shape_25,this.shape_26,this.shape_27,this.shape_28,this.shape_29];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_16}]},17).to({state:[{t:this.instance_16}]},14).to({state:[{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18}]},1).to({state:[{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22}]},7).to({state:[]},9).to({state:[]},32).wait(41));
	this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(17).to({_off:false},0).to({x:-276.8},14,cjs.Ease.quartOut).to({_off:true},1).wait(89));

	// masque Titre
	this.instance_17 = new lib.masque_generique();
	this.instance_17.parent = this;
	this.instance_17.setTransform(221.1,-163.9,0.013,0.471,0,0,0,0,17.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_17).to({regX:0.5,scaleX:1.92,x:-75.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.01,x:-68.7},10,cjs.Ease.quartInOut).to({_off:true},1).wait(101));

	// masque Titre
	this.instance_18 = new lib.masque_generique();
	this.instance_18.parent = this;
	this.instance_18.setTransform(-228.9,-13.9,0.013,0.471,0,0,0,0,17.2);
	this.instance_18._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(4).to({_off:false},0).to({regX:0.5,scaleX:1.92,x:-527.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.01,x:-528.7},10,cjs.Ease.quartInOut).to({_off:true},1).wait(97));

	// _LETTRES_CLIENT
	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#00CD6A").s().p("AgJA0QgEgEAAgGQAAgFAEgFQAEgEAFAAQAGAAAEAEQAEAFAAAFQAAAGgEAEQgEAFgGAAQgFAAgEgFgAgJAVIgEg5IAAgUIAbAAIAAAUIgFA5g");
	this.shape_30.setTransform(-357.9,-14.1);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#00CD6A").s().p("AAbA4Ig1hFIAABFIgZAAIAAhvIAYAAIA2BHIAAhHIAZAAIAABvg");
	this.shape_31.setTransform(-378.9,-14.1);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#00CD6A").s().p("AgoA4IAAhvIBPAAIAAAXIg2AAIAAAWIAxAAIAAAUIgxAAIAAAYIA4AAIAAAWg");
	this.shape_32.setTransform(-402.4,-14.1);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#00CD6A").s().p("AgMA4IAAhvIAZAAIAABvg");
	this.shape_33.setTransform(-422.5,-14.1);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#00CD6A").s().p("AgCA5IADgYIgNAAIgEAYIgZAAIAFgYIgQAAIAFgWIAPAAIADgTIgRAAIAEgWIARAAIAEgaIAXAAIgDAaIANAAIAEgaIAZAAIgFAaIARAAIgEAWIgRAAIgDATIASAAIgEAWIgSAAIgEAYgAgJALIAOAAIAEgTIgOAAg");
	this.shape_34.setTransform(-459.4,-14.3);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#00CD6A").s().p("AgjA4IAAhvIAZAAIAABYIAuAAIAAAXg");
	this.shape_35.setTransform(-497.6,-14.1);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#00CD6A").s().p("AgjApQgRgQAAgYQAAgZARgQQASgRAXAAQAcAAASAVIgPASQgMgPgSAAQgNAAgKAJQgJAJgBAPQAAAPAKAKQAJAKANAAQASAAALgPIAQARQgTAVgZAAQgZAAgRgRg");
	this.shape_36.setTransform(-520.4,-14.2);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFF100").s().p("AgMA4IAAhZIgfAAIAAgWIBXAAIAAAWIggAAIAABZg");
	this.shape_37.setTransform(-411.9,-14.1);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFF100").s().p("AAbA4Ig0hFIAABFIgaAAIAAhvIAYAAIA2BHIAAhHIAZAAIAABvg");
	this.shape_38.setTransform(-435.3,-14.1);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFF100").s().p("AgoA4IAAhvIBPAAIAAAXIg2AAIAAAWIAxAAIAAAUIgxAAIAAAYIA4AAIAAAWg");
	this.shape_39.setTransform(-458.8,-14.1);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFF100").s().p("AgMA4IAAhvIAZAAIAABvg");
	this.shape_40.setTransform(-478.9,-14.1);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFF100").s().p("AgjA4IAAhvIAZAAIAABYIAuAAIAAAXg");
	this.shape_41.setTransform(-497.6,-14.1);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFF100").s().p("AgjApQgRgQAAgYQAAgZARgQQASgRAXAAQAcAAASAVIgPASQgMgPgSAAQgNAAgKAJQgJAJgBAPQAAAPAKAKQAJAKANAAQASAAALgPIAQARQgTAVgZAAQgZAAgRgRg");
	this.shape_42.setTransform(-520.4,-14.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30,p:{x:-357.9}}]},13).to({state:[{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_30,p:{x:-422.7}}]},5).to({state:[{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37}]},5).to({state:[]},11).to({state:[]},46).wait(41));

	// Layer_11
	this.instance_19 = new lib.masqueTexte("synched",0);
	this.instance_19.parent = this;
	this.instance_19.setTransform(-328.9,105.9,0.9,0.85,0,0,0,0.1,108.9);
	this.instance_19._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_19).wait(37).to({_off:false},0).to({_off:true},40).wait(3).to({_off:false,regX:0.2,regY:108.8,rotation:180,x:-329,y:105.8},0).to({_off:true},40).wait(1));

	// _TEXTE_PRESENTATION
	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("AhMBLQgfgeAAgtQAAgsAfgeQAfgeAtAAQAvAAAeAeQAfAeAAAsQAAAtgfAeQgeAegvAAQgtAAgfgegAgjgnQgPAPAAAYQAAAZAPAQQAOAQAVAAQAWAAAPgQQAOgQAAgZQAAgYgOgPQgPgQgWgBQgVABgOAQg");
	this.shape_43.setTransform(-224.6,127.3);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFFFFF").s().p("AhHB7QgfgdAAgwQAAgwAfgcQAfgdApAAQArAAAdAaQAeAZAAAsIAAAeIiUAAQACARAPALQAPAKASAAQAdAAAUgUIAgAjQgjAgguAAQguAAgegcgAgcgBQgOAJgCASIBZAAQgCgTgMgJQgMgJgRAAQgQAAgOAKgAgihTIAvhDIA7AYIg1Arg");
	this.shape_44.setTransform(-248.6,122.7);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFFFFF").s().p("AhMBwQgcgfAAguQAAgsAbgdQAcgdAmAAQAkAAAXAaIAAhlIA5AAIAAEaIg5AAIAAgbQgYAdgjAAQgkAAgdgegAgggCQgPAPAAAYQAAAZAPAQQAPAQATAAQATAAAOgQQAOgQAAgZQAAgYgOgQQgOgRgUAAQgTABgOARg");
	this.shape_45.setTransform(-273.7,123.6);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FFFFFF").s().p("AgbCQIAAjMIA3AAIAADMgAgXhWQgKgJAAgOQAAgPAKgJQAKgKANAAQAOAAAJAKQALAJAAAPQAAAOgLAJQgJAKgOAAQgNAAgKgKg");
	this.shape_46.setTransform(-291.3,123.1);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFFFFF").s().p("AgcBnIhRjNIA7AAIAyCAIAziAIA7AAIhRDNg");
	this.shape_47.setTransform(-307.9,127.3);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FFFFFF").s().p("AgoBhQgYgIgVgQIAagjQAgAYAjAAQALAAAGgEQAHgFAAgGQAAgIgJgGQgJgGgQgFIgagJQgJgFgLgGQgYgOAAgcQAAgcAXgSQAYgTAjABQAjAAAlAYIgWAmQgbgSgaAAQgZAAAAAOQAAAIAIAFQAIAEATAHIAbAJQAJADALAHQAWALgBAfQABAegXASQgWASgjAAQgVABgZgJg");
	this.shape_48.setTransform(-339.3,127.3);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FFFFFF").s().p("AhHBNQgfgdAAgwQAAgwAfgcQAfgcApAAQArAAAdAZQAeAZAAAtIAAAeIiUAAQACAQAPALQAPALASAAQAdgBAUgTIAgAiQgjAgguAAQguABgegdgAgcgvQgOAKgCASIBZAAQgCgTgMgJQgMgKgRAAQgQAAgOAKg");
	this.shape_49.setTransform(-360.5,127.3);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FFFFFF").s().p("AgeBzQgUgUAAghIAAhdIgYAAIAAgpIAYAAIAAg9IA4AAIAAA9IAyAAIAAApIgyAAIAABaQAAAMAGAHQAGAGAJAAQAQAAALgOIAVAoQgbAYgdAAQgcAAgVgTg");
	this.shape_50.setTransform(-380.2,124.4);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FFFFFF").s().p("Ag6BoIAAjLIA5AAIAAAbQAKgNAQgJQARgIARgBIAAA2IgLAAQgZAAgNASQgLASAAAcIAABZg");
	this.shape_51.setTransform(-396,127.1);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FFFFFF").s().p("AhFBWQgWgSAAggQAAgfAXgPQAWgPAoAAIApAAIAAgBQAAghgiABQgOgBgRAGQgQAGgMAIIgZgkQAogdAzAAQAlAAAYASQAXATAAAoIAACCIg1AAIAAgZQgWAcgggBQggAAgWgTgAgiAkQAAALAIAGQAIAFAQABQAOAAALgJQAMgKAAgQIAAgKIgjAAQgiAAAAAWg");
	this.shape_52.setTransform(-416.4,127.3);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FFFFFF").s().p("AAmCNIAAhvQAAgvgiAAQgQAAgNAMQgMAMAAAYIAABuIg6AAIAAkZIA6AAIAABkQAagaAeAAQAhAAAVAXQAXAXAAAkIAAB9g");
	this.shape_53.setTransform(-439.2,123.4);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FFFFFF").s().p("AhBBNQgfgdgBgwQAAgtAhgeQAggdArAAQAWAAAWAJQAXAJASASIgeAmQgKgLgOgGQgOgHgOAAQgWAAgRAPQgPAOAAAZQAAAaAPAPQARAOAVAAQAcAAAXgdIAhAnQgnApgugBQgtAAgggcg");
	this.shape_54.setTransform(-462.3,127.3);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FFFFFF").s().p("AgeBzQgUgUAAghIAAhdIgYAAIAAgpIAYAAIAAg9IA4AAIAAA9IAyAAIAAApIgyAAIAABaQAAAMAGAHQAGAGAJAAQAQAAALgOIAVAoQgbAYgdAAQgcAAgVgTg");
	this.shape_55.setTransform(-491.5,124.4);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FFFFFF").s().p("AhHBNQgfgdAAgwQAAgwAfgcQAfgcApAAQArAAAdAZQAeAZAAAtIAAAeIiUAAQACAQAPALQAPALASAAQAdgBAUgTIAgAiQgjAgguAAQguABgegdgAgcgvQgOAKgCASIBZAAQgCgTgMgJQgMgKgRAAQgQAAgOAKg");
	this.shape_56.setTransform(-512,127.3);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FFFFFF").s().p("AAmBoIAAhzQAAgugigBQgQABgNAMQgMAMAAAYIAABxIg6AAIAAjLIA6AAIAAAWQAZgaAfAAQAhAAAWAXQAVAXAAAlIAAB8g");
	this.shape_57.setTransform(-94.2,80.8);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#FFFFFF").s().p("AhMBLQgfgeAAgtQAAgsAfgeQAfgeAtAAQAvAAAeAeQAfAeAAAsQAAAtgfAeQgeAegvAAQgtAAgfgegAgjgnQgPAPAAAYQAAAZAPAQQAOAQAVAAQAWAAAPgQQAOgQAAgZQAAgYgOgPQgPgQgWgBQgVABgOAQg");
	this.shape_58.setTransform(-118.8,80.9);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#FFFFFF").s().p("AgcCQIAAjMIA4AAIAADMgAgXhWQgKgJAAgOQAAgPAKgJQAKgKANAAQAOAAAKAKQAKAJAAAPQAAAOgKAJQgKAKgOAAQgNAAgKgKg");
	this.shape_59.setTransform(-136.6,76.8);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#FFFFFF").s().p("AgeBzQgUgUAAghIAAhdIgYAAIAAgpIAYAAIAAg9IA4AAIAAA9IAyAAIAAApIgyAAIAABaQAAAMAGAHQAGAGAJAAQAQAAALgOIAVAoQgbAYgdAAQgcAAgVgTg");
	this.shape_60.setTransform(-150,78);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#FFFFFF").s().p("AhFBWQgWgSAAggQAAgfAXgPQAWgPAoAAIApAAIAAgBQAAghgiABQgOgBgRAGQgQAGgMAIIgZgkQAogdAzAAQAlAAAYASQAXATAAAoIAACCIg1AAIAAgZQgWAcgggBQggAAgWgTgAgiAkQAAALAIAGQAIAFAQABQAOAAALgJQAMgKAAgQIAAgKIgjAAQgiAAAAAWg");
	this.shape_61.setTransform(-170.6,80.9);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#FFFFFF").s().p("ABoBoIAAhuQAAgwgjAAQgRAAgMAMQgNANgBAYIAABtIg3AAIAAhuQgBgZgHgLQgIgMgQAAQgQAAgOAMQgMANAAAYIAABtIg5AAIAAjLIA5AAIAAAWQAYgaAeAAQAUAAAPALQAPAMAIARQALgUAVgKQATgKAUAAQAlAAAWAWQAWAVAAAoIAAB8g");
	this.shape_62.setTransform(-200,80.8);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#FFFFFF").s().p("AgbCQIAAjMIA4AAIAADMgAgXhWQgKgJAAgOQAAgPAKgJQAKgKANAAQAOAAAJAKQAKAJAAAPQAAAOgKAJQgJAKgOAAQgNAAgKgKg");
	this.shape_63.setTransform(-224.3,76.8);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#FFFFFF").s().p("AAmBoIAAhzQABgugjgBQgQABgNAMQgMAMAAAYIAABxIg5AAIAAjLIA5AAIAAAWQAagaAeAAQAhAAAWAXQAVAXABAlIAAB8g");
	this.shape_64.setTransform(-241.9,80.8);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#FFFFFF").s().p("AhFBWQgWgSAAggQAAgfAXgPQAWgPAoAAIApAAIAAgBQAAghgiABQgOgBgRAGQgQAGgMAIIgZgkQAogdAzAAQAlAAAYASQAXATAAAoIAACCIg1AAIAAgZQgWAcgggBQggAAgWgTgAgiAkQAAALAIAGQAIAFAQABQAOAAALgJQAMgKAAgQIAAgKIgjAAQgiAAAAAWg");
	this.shape_65.setTransform(-265.9,80.9);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#FFFFFF").s().p("AgWAzIAAhlIAsAAIAABlg");
	this.shape_66.setTransform(-281.7,69.7);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#FFFFFF").s().p("AhMBwQgcgfAAguQAAgsAbgdQAcgdAmAAQAkAAAXAaIAAhlIA5AAIAAEaIg5AAIAAgbQgYAdgjAAQgkAAgdgegAgggCQgPAPAAAYQAAAZAPAQQAPAQATAAQATAAAOgQQAOgQAAgZQAAgYgOgQQgOgRgUABQgTAAgOARg");
	this.shape_67.setTransform(-299.9,77.2);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#FFFFFF").s().p("AgbCNIAAkZIA3AAIAAEZg");
	this.shape_68.setTransform(-327.5,77.1);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#FFFFFF").s().p("AhFBWQgWgSAAggQAAgfAXgPQAWgPAoAAIApAAIAAgBQAAghgiABQgOgBgRAGQgQAGgMAIIgZgkQAogdAzAAQAlAAAYASQAXATAAAoIAACCIg1AAIAAgZQgWAcgggBQggAAgWgTgAgiAkQAAALAIAGQAIAFAQABQAOAAALgJQAMgKAAgQIAAgKIgjAAQgiAAAAAWg");
	this.shape_69.setTransform(-344.8,80.9);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#FFFFFF").s().p("AgcBnIhRjMIA7AAIAyB/IAzh/IA7AAIhRDMg");
	this.shape_70.setTransform(-366.7,80.9);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#FFFFFF").s().p("AgcCQIAAjMIA5AAIAADMgAgXhWQgJgJAAgOQAAgPAJgJQAKgKANAAQAOAAAJAKQAKAJAAAPQAAAOgKAJQgJAKgOAAQgNAAgKgKg");
	this.shape_71.setTransform(-383.4,76.8);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#FFFFFF").s().p("AgeBzQgUgUAAghIAAhdIgYAAIAAgpIAYAAIAAg9IA4AAIAAA9IAyAAIAAApIgyAAIAABaQAAAMAGAHQAGAGAJAAQAQAAALgOIAVAoQgbAYgdAAQgcAAgVgTg");
	this.shape_72.setTransform(-396.8,78);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#FFFFFF").s().p("AgoBhQgYgIgVgQIAagjQAgAYAjAAQALAAAHgEQAGgFAAgGQAAgIgJgGQgJgGgQgFIgagJQgJgFgLgGQgYgOABgcQAAgcAWgSQAXgTAjABQAkAAAkAYIgUAmQgbgSgaAAQgaAAAAAOQAAAIAJAFQAHAEATAHIAcAJQAIADALAHQAVALAAAfQAAAegWASQgWASgiAAQgWABgZgJg");
	this.shape_73.setTransform(-415.8,80.9);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#FFFFFF").s().p("AhHBNQgfgdAAgwQAAgwAfgcQAfgcApAAQArAAAdAZQAeAZAAAtIAAAeIiUAAQACAQAPALQAPALASAAQAdgBAUgTIAgAiQgjAgguAAQguABgegdgAgcgvQgOAKgCASIBZAAQgCgTgMgKQgMgJgRAAQgQAAgOAKg");
	this.shape_74.setTransform(-437.1,80.9);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#FFFFFF").s().p("AgyCQIAAihIgYAAIAAgpIAYAAIAAgMQAAgiAVgTQAUgUAdAAQAeAAAZAXIgVAmQgMgOgQAAQgIAAgGAGQgFAGAAANIAAANIAxAAIAAApIgxAAIAAChg");
	this.shape_75.setTransform(-455.3,76.8);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#FFFFFF").s().p("AhIBTQgXgVAAgqIAAh7IA6AAIAABvQAAAvAiAAQARAAAMgNQANgMAAgYIAAhtIA5AAIAADMIg5AAIAAgbQgWAeggAAQgiAAgXgVg");
	this.shape_76.setTransform(-486.5,81.1);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#FFFFFF").s().p("AhMBwQgcgfAAguQAAgsAbgdQAcgdAmAAQAkAAAXAaIAAhlIA5AAIAAEaIg5AAIAAgbQgYAdgjAAQgkAAgdgegAgggCQgPAPAAAYQAAAZAPAQQAPAQATAAQATAAAOgQQAOgQAAgZQAAgYgOgQQgOgRgUABQgTAAgOARg");
	this.shape_77.setTransform(-511.8,77.2);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("#FFFFFF").s().p("AgoBhQgYgIgVgQIAagjQAgAYAjAAQALAAAHgEQAGgFAAgGQAAgIgJgGQgJgGgQgFIgagJQgJgFgLgGQgXgOAAgcQAAgcAWgSQAXgTAkABQAjAAAlAYIgWAmQgbgSgZAAQgaAAAAAOQAAAIAJAFQAIAEASAHIAcAJQAIADALAHQAWALgBAfQABAegXASQgWASgjAAQgVABgZgJg");
	this.shape_78.setTransform(-202.3,34.6);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("#FFFFFF").s().p("AhHBNQgfgdAAgwQAAgwAfgcQAfgcApAAQArAAAdAZQAeAZAAAtIAAAeIiUAAQACAQAPALQAPALASAAQAdgBAUgTIAgAiQgjAgguAAQguABgegdgAgcgvQgOAKgCASIBZAAQgCgTgMgKQgMgJgRAAQgQAAgOAKg");
	this.shape_79.setTransform(-223.5,34.6);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f("#FFFFFF").s().p("AhHB7QgfgdAAgwQAAgwAfgcQAfgdApAAQArAAAdAaQAeAZAAAsIAAAeIiUAAQACARAPALQAPAKASAAQAdAAAUgUIAgAjQgjAgguAAQguAAgegcgAgcgBQgOAJgCASIBZAAQgCgTgMgJQgMgJgRAAQgQAAgOAKgAgihTIAvhDIA7AYIg1Arg");
	this.shape_80.setTransform(-247,30);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("#FFFFFF").s().p("ABoBoIAAhuQAAgwgjAAQgRAAgMAMQgOANAAAYIAABtIg3AAIAAhuQgBgZgHgLQgIgMgQAAQgQAAgOAMQgMANAAAYIAABtIg5AAIAAjLIA5AAIAAAWQAYgaAeAAQAUAAAPALQAPAMAIARQALgUAVgKQATgKAUAAQAlAAAWAWQAWAVAAAoIAAB8g");
	this.shape_81.setTransform(-277.4,34.4);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f("#FFFFFF").s().p("AgbCQIAAjMIA4AAIAADMgAgXhWQgKgJAAgOQAAgPAKgJQAKgKANAAQAOAAAJAKQAKAJAAAPQAAAOgKAJQgJAKgOAAQgNAAgKgKg");
	this.shape_82.setTransform(-301.7,30.4);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("#FFFFFF").s().p("AAmBoIAAhzQABgugjgBQgQABgNAMQgMAMAAAYIAABxIg5AAIAAjLIA5AAIAAAWQAagaAeAAQAhAAAWAXQAVAXABAlIAAB8g");
	this.shape_83.setTransform(-319.3,34.4);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f("#FFFFFF").s().p("AhFBXQgWgTAAggQAAgfAXgPQAWgPAoAAIApAAIAAgBQAAghgiABQgOgBgRAGQgQAGgMAJIgZglQAogdAzAAQAlAAAYASQAXATAAAoIAACCIg1AAIAAgZQgWAcgggBQggAAgWgSgAgiAkQAAALAIAGQAIAFAQABQAOAAALgJQAMgKAAgQIAAgJIgjAAQgigBAAAWg");
	this.shape_84.setTransform(-343.3,34.6);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f("#FFFFFF").s().p("AgoBhQgZgIgUgQIAagjQAgAYAjAAQALAAAGgEQAHgFAAgGQAAgIgJgGQgJgGgQgFIgagJQgJgFgLgGQgYgOAAgcQAAgcAXgSQAYgTAjABQAjAAAlAYIgWAmQgbgSgaAAQgZAAAAAOQAAAIAIAFQAIAEATAHIAbAJQAJADALAHQAWALgBAfQABAegXASQgWASgjAAQgVABgZgJg");
	this.shape_85.setTransform(-374.4,34.6);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f("#FFFFFF").s().p("AhHBNQgfgdAAgwQAAgwAfgcQAfgcApAAQArAAAdAZQAeAZAAAtIAAAeIiUAAQACAQAPALQAPALASAAQAdgBAUgTIAgAiQgjAgguAAQguABgegdgAgcgvQgOAKgCASIBZAAQgCgTgMgKQgMgJgRAAQgQAAgOAKg");
	this.shape_86.setTransform(-395.6,34.6);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f("#FFFFFF").s().p("AAnCNIAAhvQAAgvgjAAQgQAAgNAMQgNAMAAAYIAABuIg4AAIAAkZIA4AAIAABkQAagaAgAAQAfAAAXAXQAVAXAAAkIAAB9g");
	this.shape_87.setTransform(-419.5,30.7);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f("#FFFFFF").s().p("AhBBNQgfgdAAgwQAAgtAggeQAggdArAAQAWAAAXAJQAWAJARASIgdAnQgKgMgPgHQgOgGgOAAQgVAAgQAPQgQAOgBAZQABAaAQAPQAQAOAUAAQAdAAAXgdIAiAnQgoApgugBQgtAAgggcg");
	this.shape_88.setTransform(-442.6,34.6);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f("#FFFFFF").s().p("AgbCQIAAjMIA3AAIAADMgAgXhWQgKgJAAgOQAAgPAKgJQAKgKANAAQAOAAAKAKQAJAJABAPQgBAOgJAJQgKAKgOAAQgNAAgKgKg");
	this.shape_89.setTransform(-459.4,30.4);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f("#FFFFFF").s().p("AgyCQIAAihIgYAAIAAgpIAYAAIAAgMQAAgiAUgTQAVgUAdAAQAfAAAYAXIgUAmQgNgOgPAAQgJAAgGAGQgFAGgBANIAAANIAyAAIAAApIgyAAIAAChg");
	this.shape_90.setTransform(-471.4,30.4);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f("#FFFFFF").s().p("AgyCQIAAihIgYAAIAAgpIAYAAIAAgMQAAgiAUgTQAVgUAdAAQAfAAAYAXIgUAmQgNgOgPAAQgJAAgGAGQgFAGgBANIAAANIAyAAIAAApIgyAAIAAChg");
	this.shape_91.setTransform(-486.7,30.4);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f("#FFFFFF").s().p("ABQCFIgZg6IhuAAIgZA6Ig/AAIBzkJIA5AAIByEJgAggAXIBCAAIgihMg");
	this.shape_92.setTransform(-509.7,31.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_92},{t:this.shape_91},{t:this.shape_90},{t:this.shape_89},{t:this.shape_88},{t:this.shape_87},{t:this.shape_86},{t:this.shape_85},{t:this.shape_84},{t:this.shape_83},{t:this.shape_82},{t:this.shape_81},{t:this.shape_80},{t:this.shape_79},{t:this.shape_78},{t:this.shape_77},{t:this.shape_76},{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43}]},57).to({state:[]},43).to({state:[]},1).wait(20));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(221.1,-172,2,16);


// stage content:
(lib.FS_projet_citia_motion_2018 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":150});

	// timeline functions:
	this.frame_99 = function() {
		//var frequency = 60;
		//stage.enableMouseOver(frequency);
		
		// EVENT DE CLIC POUR LA SUITE
		this.next_btn.addEventListener("click", fl_ClickToGoToAndPlayFromFrame.bind(this));
		this.next_btn.addEventListener("mouseover", fl_MouseOverHandler.bind(this));
		this.next_btn.addEventListener("mouseout", fl_MouseOutHandler.bind(this));
		
		
		function fl_ClickToGoToAndPlayFromFrame()
		{
			//this.gotoAndPlay("close");
			
			var event = new Event('next');
			this.dispatchEvent(event);		
			event = null;
			
			this.next_btn.btnNext.gotoAndPlay('rollOut');
			
		}
		function fl_MouseOverHandler(){	this.next_btn.btnNext.gotoAndPlay('rollOver'); }
		function fl_MouseOutHandler(){	this.next_btn.btnNext.gotoAndPlay('rollOut'); }
		
		
		// EVENT DE CLIC POUR LE LIEN
		this.cta_mc.addEventListener("click", projectDetails.bind(this));
		this.cta_mc.addEventListener("mouseover", pDetails_MouseOverHandler.bind(this));
		this.cta_mc.addEventListener("mouseout", pDetails_MouseOutHandler.bind(this));
		
		function projectDetails()
		{	
			var event = new Event('details');
			this.dispatchEvent(event);		
			event = null;
			
			this.cta_mc.btnCta.gotoAndPlay('rollOut');
			
		}
		function pDetails_MouseOverHandler(){	this.cta_mc.btnCta.gotoAndPlay('rollOver'); }
		function pDetails_MouseOutHandler(){	this.cta_mc.btnCta.gotoAndPlay('rollOut'); }
	}
	this.frame_146 = function() {
		//PAUSE
		this.stop();
	}
	this.frame_150 = function() {
		this.textes_mc.gotoAndPlay("close");
		this.visuel_mc.gotoAndPlay("close");
		this.cta_mc.gotoAndPlay("close");
		this.video_mc.gotoAndPlay("close");
		this.next_btn.gotoAndPlay("close");
		this.lignes_mc.gotoAndPlay("close_lines");
	}
	this.frame_169 = function() {
		//this.lignes_mc.gotoAndPlay("close");
	}
	this.frame_211 = function() {
		// EVENT DE FIN D'ANIM
		var event = new Event('closed');
		this.dispatchEvent(event);		
		event = null;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(99).call(this.frame_99).wait(47).call(this.frame_146).wait(4).call(this.frame_150).wait(19).call(this.frame_169).wait(42).call(this.frame_211).wait(1));

	// BLOC INFOS TEXTE
	this.textes_mc = new lib.blocInfostextes();
	this.textes_mc.name = "textes_mc";
	this.textes_mc.parent = this;
	this.textes_mc.setTransform(858.2,182,1,1,0,0,0,222.1,-164);
	this.textes_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.textes_mc).wait(17).to({_off:false},0).wait(195));

	// masque VISUEL
	this.visuel_mc = new lib.bloc_visuel();
	this.visuel_mc.name = "visuel_mc";
	this.visuel_mc.parent = this;
	this.visuel_mc.setTransform(1192.6,391.5,1,1,0,0,0,552.5,26.5);
	this.visuel_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.visuel_mc).wait(17).to({_off:false},0).wait(195));

	// CTA NEXT
	this.next_btn = new lib.bloc_NEXT_btn();
	this.next_btn.name = "next_btn";
	this.next_btn.parent = this;
	this.next_btn.setTransform(753.2,786.9,1,1,0,0,0,-340.5,403.9);
	this.next_btn._off = true;

	this.timeline.addTween(cjs.Tween.get(this.next_btn).wait(89).to({_off:false},0).wait(123));

	// CTA DETAIL
	this.cta_mc = new lib.bloc_CTA();
	this.cta_mc.name = "cta_mc";
	this.cta_mc.parent = this;
	this.cta_mc.setTransform(299.5,787,1,1,0,0,0,-340.5,403.9);
	this.cta_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.cta_mc).wait(99).to({_off:false},0).to({_off:true},74).wait(39));

	// VIDEO
	this.video_mc = new lib.bloc_video();
	this.video_mc.name = "video_mc";
	this.video_mc.parent = this;
	this.video_mc.setTransform(866.7,1147.2,1,1,0,0,0,227.5,764.9);
	this.video_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.video_mc).wait(94).to({_off:false},0).to({_off:true},79).wait(39));

	// lignes
	this.lignes_mc = new lib.lignes_all();
	this.lignes_mc.name = "lignes_mc";
	this.lignes_mc.parent = this;
	this.lignes_mc.setTransform(640,384.3,1,1,0,0,0,0,406.3);

	this.timeline.addTween(cjs.Tween.get(this.lignes_mc).wait(212));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(825.5,361.5,909.1,813.5);
// library properties:
lib.properties = {
	id: '76E92D65B57F4146A0586857C86BB051',
	width: 1280,
	height: 768,
	fps: 60,
	color: "#000000",
	opacity: 1.00,
	manifest: [
		{src:"images/affiche2017.jpg?1561738317609", id:"affiche2017"},
		{src:"images/affiche2018.jpg?1561738317609", id:"affiche2018"},
		{src:"images/affiche2019.jpg?1561738317609", id:"affiche2019"},
		{src:"images/CITIA_2018_b.png?1561738317609", id:"CITIA_2018_b"},
		{src:"images/CITIA_2018_c.png?1561738317609", id:"CITIA_2018_c"},
		{src:"images/CITIA_2018_d.png?1561738317609", id:"CITIA_2018_d"},
		{src:"images/sucette.png?1561738317609", id:"sucette"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['76E92D65B57F4146A0586857C86BB051'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;