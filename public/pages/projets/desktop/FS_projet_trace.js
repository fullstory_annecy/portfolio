(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.hublet_TRACE_contour = function() {
	this.initialize(img.hublet_TRACE_contour);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,700,654);


(lib.hublet_TRACE_ile = function() {
	this.initialize(img.hublet_TRACE_ile);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,679,618);


(lib.nuage_TRACE = function() {
	this.initialize(img.nuage_TRACE);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,679,262);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.video = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.008)").s().p("EgtTBLAMAAAiV/MBanAAAMAAACV/g");
	this.shape.setTransform(290,480);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.video, new cjs.Rectangle(0,0,580,960), null);


(lib.txt_hashtag = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgKAxIAAhhIAVAAIAABhg");
	this.shape.setTransform(58.2,73.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgmAxIAAhhIAiAAQAWAAALAJQAKAIAAATQAAARgLAJQgKAIgVAAIgNAAIAAAbgAgQADIAOAAQALAAADgEQAEgEABgIQAAgJgGgDQgFgEgKAAIgMAAg");
	this.shape_1.setTransform(51.5,73.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgKAxIAAgmIgig7IAYAAIAUAkIAVgkIAYAAIgiA7IAAAmg");
	this.shape_2.setTransform(38.4,73.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAQAxIgVgfIgPAAIAAAfIgWAAIAAhhIAlAAQAWAAAKAIQAKAHAAASQAAAWgTAHIAZAjgAgUAAIAQAAQAKAAAEgEQAEgEAAgHQAAgIgEgDQgEgDgKAAIgQAAg");
	this.shape_3.setTransform(29.3,73.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAQAxIgVgfIgPAAIAAAfIgWAAIAAhhIAlAAQAWAAAKAIQAKAHAAASQAAAWgTAHIAZAjgAgUAAIAQAAQAKAAAEgEQAEgEAAgHQAAgIgEgDQgEgDgKAAIgQAAg");
	this.shape_4.setTransform(19.1,73.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBGAAIAAATIgwAAIAAAVIAqAAIAAASIgqAAIAAAUIAxAAIAAATg");
	this.shape_5.setTransform(9.3,73.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgmAxIAAhhIAmAAQAJAAAIACQAGADAEAEQAHAIAAAJQgBAMgGAFIgEACIgEACQAKABAFAGQAFAHAAAJQAAAKgHAIQgIAJgUAAgAgQAfIAPAAQAIgBAFgCQAEgCABgHQAAgGgGgDQgEgCgKAAIgNAAgAgQgIIAKAAQAIAAAFgCQAEgCAAgHQAAgGgEgCQgEgCgJgBIgKAAg");
	this.shape_6.setTransform(0,73.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgmAxIAAhhIAjAAQAVAAALAJQAKAIAAATQAAARgLAJQgLAIgUAAIgNAAIAAAbgAgQADIAOAAQAKAAAFgEQADgEAAgIQABgJgGgDQgFgEgJAAIgNAAg");
	this.shape_7.setTransform(-9.5,73.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgmAiIANgQQAQANANAAQAGAAADgDQADgCAAgFQAAgDgDgDQgEgDgKgDQgRgDgIgGQgIgGAAgOQAAgOALgIQAKgIANAAQAKAAAKAEQAKADAHAHIgLAQQgNgLgNAAQgFAAgDADQgDADAAAEQAAAEAEADQAEACANAEQAOADAHAGQAIAHAAANQAAANgKAJQgKAHgQABQgWAAgTgRg");
	this.shape_8.setTransform(-19.2,73.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AAeAxIgKgVIgoAAIgJAVIgXAAIAqhhIAVAAIAqBhgAgLAIIAXAAIgMgbg");
	this.shape_9.setTransform(-28.8,73.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AAQAxIgVgfIgPAAIAAAfIgWAAIAAhhIAlAAQAWAAAKAIQAKAHAAASQAAAWgTAHIAZAjgAgUAAIAQAAQAKAAAEgEQAEgEAAgHQAAgIgEgDQgEgDgKAAIgQAAg");
	this.shape_10.setTransform(-38.8,73.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgUIANAAIADgQIgPAAIADgUIAPAAIAEgWIAUAAIgDAWIAMAAIADgWIAWAAIgEAWIAOAAIgDAUIgPAAIgDAQIARAAIgEAUIgQAAIgDAVgAgHAJIALAAIAEgQIgMAAg");
	this.shape_11.setTransform(-49.5,73.7);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgmAiIANgQQAQAOANAAQAGgBADgDQADgCAAgFQAAgEgDgCQgEgDgKgDQgRgDgIgGQgIgGAAgOQAAgPALgHQAKgIANAAQAKAAAKAEQAKADAHAHIgLAQQgNgKgNAAQgFAAgDACQgDADAAAEQAAAEAEADQAEACANAEQAOADAHAGQAIAHAAANQAAANgKAJQgKAHgQABQgWAAgTgRg");
	this.shape_12.setTransform(14.9,52.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgeAkIALgQQAJAKAJgBQAEAAADgDQADgEAAgGIAAgtIgcAAIAAgTIAzAAIAAA/QAAARgJAJQgKAIgNABQgRAAgNgOg");
	this.shape_13.setTransform(6.2,52.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgIAJQgDgEAAgFQAAgEADgEQAEgDAEAAQAFAAAEADQADAEAAAEQAAAFgDAEQgEADgFAAQgEAAgEgDg");
	this.shape_14.setTransform(0.7,56.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBFAAIAAATIgvAAIAAAVIAqAAIAAASIgqAAIAAAUIAxAAIAAATg");
	this.shape_15.setTransform(-5.7,52.8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgsAxIAAhhIAjAAQAaAAAOANQAOAMgBAXQABAWgOAOQgOANgbAAgAgVAeIANAAQAOgBAJgHQAHgIABgOQgBgOgHgHQgJgIgQAAIgLAAg");
	this.shape_16.setTransform(-15.6,52.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgjAkQgPgPAAgVQAAgUAPgPQAOgPAVAAQAWAAAPAPQAOAPAAAUQAAAVgOAPQgPAPgWAAQgVAAgOgPgAgTgVQgJAJAAAMQAAANAJAKQAIAIALABQAMgBAJgIQAIgKAAgNQAAgMgIgJQgJgJgMAAQgLAAgIAJg");
	this.shape_17.setTransform(-27,52.7);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AAXAxIgtg9IAAA9IgWAAIAAhhIAUAAIAvA+IAAg+IAWAAIAABhg");
	this.shape_18.setTransform(-38.5,52.8);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgTIANAAIADgRIgPAAIADgUIAPAAIAEgWIAUAAIgDAWIAMAAIADgWIAWAAIgEAWIAOAAIgDAUIgPAAIgDARIARAAIgEATIgQAAIgDAVgAgHAKIALAAIAEgRIgMAAg");
	this.shape_19.setTransform(-49.5,52.7);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgeAkQgOgOAAgWQgBgUAPgPQAPgOAUAAQAWAAAPANIgMARQgGgGgFgDQgGgBgGAAQgMAAgJAIQgIAJAAAMQAAAOAIAJQAJAIAJAAQAMAAAHgFIAAgaIAXAAIAAAiQgPAQgaABQgUgBgPgOg");
	this.shape_20.setTransform(117.3,31.7);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AAXAxIgtg9IAAA9IgWAAIAAhhIAUAAIAvA+IAAg+IAWAAIAABhg");
	this.shape_21.setTransform(106.4,31.7);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgKAxIAAhhIAVAAIAABhg");
	this.shape_22.setTransform(98.4,31.7);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgmAxIAAhhIAiAAQAWAAAKAJQALAIAAATQAAARgLAIQgLAJgUgBIgNAAIAAAcgAgQADIAPAAQAJAAAEgDQAFgFAAgJQAAgIgGgDQgFgEgKAAIgMAAg");
	this.shape_23.setTransform(91.7,31.7);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgmAxIAAhhIAjAAQAVAAAKAJQALAIAAATQAAARgLAIQgKAJgVgBIgNAAIAAAcgAgQADIAPAAQAJAAAFgDQADgFAAgJQAAgIgFgDQgFgEgJAAIgNAAg");
	this.shape_24.setTransform(82.2,31.7);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AAeAxIgKgVIgoAAIgJAVIgXAAIAqhhIAVAAIAqBhgAgLAJIAXAAIgMgcg");
	this.shape_25.setTransform(71.8,31.7);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AAhAxIAAg9IgbA1IgMAAIgag1IAAA9IgWAAIAAhhIAeAAIAYA1IAZg1IAeAAIAABhg");
	this.shape_26.setTransform(59.9,31.7);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgsAxIAAhhIAjAAQAaAAAOANQANAMAAAXQAAAWgNAOQgOANgbAAgAgVAdIANAAQAOAAAIgHQAJgIAAgOQAAgOgJgHQgIgIgQAAIgLAAg");
	this.shape_27.setTransform(44.5,31.7);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBFAAIAAATIgvAAIAAAVIArAAIAAASIgrAAIAAATIAxAAIAAAUg");
	this.shape_28.setTransform(34.5,31.7);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AgKAxIAAhOIgcAAIAAgTIBNAAIAAATIgcAAIAABOg");
	this.shape_29.setTransform(25.4,31.7);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AAXAxIgtg9IAAA9IgWAAIAAhhIAUAAIAvA+IAAg+IAWAAIAABhg");
	this.shape_30.setTransform(15.4,31.7);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBGAAIAAATIgwAAIAAAVIAqAAIAAASIgqAAIAAATIAxAAIAAAUg");
	this.shape_31.setTransform(5.4,31.7);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AAhAxIAAg9IgbA1IgMAAIgag1IAAA9IgWAAIAAhhIAeAAIAYA1IAZg1IAeAAIAABhg");
	this.shape_32.setTransform(-6.1,31.7);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AgeAkQgPgOAAgWQAAgUAPgPQAPgOAUAAQAVAAAQANIgLARQgHgGgGgDQgFgBgHAAQgLAAgIAIQgJAJAAAMQAAAOAIAJQAJAIAKAAQALAAAHgFIAAgaIAWAAIAAAiQgOAQgaABQgUgBgPgOg");
	this.shape_33.setTransform(-18.1,31.7);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AgdAmQgMgMAAgUIAAg2IAWAAIAAA1QAAAMAGAGQAFAHAIAAQAJAAAGgHQAFgGAAgMIAAg1IAWAAIAAA2QAAAUgMAMQgMAMgSAAQgRAAgMgMg");
	this.shape_34.setTransform(-28.5,31.8);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AAeAxIgKgVIgoAAIgJAVIgXAAIAqhhIAVAAIAqBhgAgLAJIAXAAIgMgcg");
	this.shape_35.setTransform(-39,31.7);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgTIANAAIADgRIgPAAIADgTIAPAAIAEgXIAUAAIgDAXIAMAAIADgXIAWAAIgEAXIAOAAIgDATIgPAAIgDARIARAAIgEATIgQAAIgDAVgAgHAKIALAAIAEgRIgMAAg");
	this.shape_36.setTransform(-49.5,31.6);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AAXAxIgtg9IAAA9IgWAAIAAhhIAUAAIAvA+IAAg+IAWAAIAABhg");
	this.shape_37.setTransform(70.1,10.7);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("AgeAkQgPgOAAgWQABgUAPgPQAOgOAVAAQAUAAAQANIgLARQgHgGgGgDQgFgBgGAAQgMAAgIAIQgJAIAAANQAAAOAIAJQAJAIAKAAQALAAAIgFIAAgaIAVAAIAAAiQgOAQgZABQgWgBgOgOg");
	this.shape_38.setTransform(59.1,10.6);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AgKAxIAAhhIAVAAIAABhg");
	this.shape_39.setTransform(51.6,10.7);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("AgmAhIANgQQAQAOANABQAGAAADgEQADgCAAgEQAAgFgDgCQgEgDgKgDQgRgEgIgFQgIgHAAgNQAAgOALgIQAKgIANABQAKgBAKAEQAKADAHAGIgLARQgNgLgNABQgFgBgDADQgDADAAAEQAAAFAEACQAEADANADQAOADAHAGQAIAHAAANQAAANgKAIQgKAJgQAAQgWgBgTgRg");
	this.shape_40.setTransform(44.7,10.6);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBFAAIAAAUIgvAAIAAAUIArAAIAAASIgrAAIAAATIAxAAIAAAUg");
	this.shape_41.setTransform(36,10.7);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("AgrAxIAAhhIAiAAQAaAAAOANQANAMAAAXQAAAWgNAOQgOANgbAAgAgVAdIANAAQAPAAAHgHQAJgIAAgOQAAgOgJgHQgHgIgRAAIgLAAg");
	this.shape_42.setTransform(26.1,10.7);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("AAXAxIgtg9IAAA9IgWAAIAAhhIAUAAIAvA+IAAg+IAWAAIAABhg");
	this.shape_43.setTransform(11,10.7);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFFFFF").s().p("AgjAkQgPgPAAgVQAAgVAPgOQAOgOAVAAQAWAAAPAOQAOAOAAAVQAAAVgOAPQgPAPgWAAQgVAAgOgPgAgTgVQgJAJAAAMQAAANAJAKQAIAIALABQAMgBAJgIQAIgKAAgNQAAgMgIgJQgJgKgMABQgLgBgIAKg");
	this.shape_44.setTransform(-0.5,10.6);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFFFFF").s().p("AgKAxIAAhhIAVAAIAABhg");
	this.shape_45.setTransform(-8.5,10.7);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FFFFFF").s().p("AgKAxIAAhOIgcAAIAAgTIBMAAIAAATIgbAAIAABOg");
	this.shape_46.setTransform(-15.1,10.7);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFFFFF").s().p("AgjAkQgPgPAAgVQAAgVAPgOQAOgOAVAAQAWAAAPAOQAOAOAAAVQAAAVgOAPQgPAPgWAAQgVAAgOgPgAgTgVQgJAJAAAMQAAANAJAKQAIAIALABQAMgBAJgIQAIgKAAgNQAAgMgIgJQgJgKgMABQgLgBgIAKg");
	this.shape_47.setTransform(-25.1,10.6);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FFFFFF").s().p("AAhAxIAAg9IgbA1IgMAAIgag1IAAA9IgWAAIAAhhIAeAAIAYA1IAZg1IAeAAIAABhg");
	this.shape_48.setTransform(-37.6,10.7);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgTIANAAIADgRIgPAAIADgTIAPAAIAEgXIAUAAIgDAXIAMAAIADgXIAWAAIgEAXIAOAAIgDATIgPAAIgDARIARAAIgEATIgQAAIgDAVgAgHAKIALAAIAEgRIgMAAg");
	this.shape_49.setTransform(-49.5,10.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.txt_hashtag, new cjs.Rectangle(-56.7,0,185.2,84.2), null);


(lib.nuage = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.nuage_TRACE();
	this.instance.parent = this;
	this.instance.setTransform(-339.5,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-339.5,0,679,262);


(lib.masqueTexte_1_ligne = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("EgicAEJIAAoRMBE5AAAIAAIRg");
	this.shape.setTransform(0,24.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.masqueTexte_1_ligne, new cjs.Rectangle(-220.5,-1.9,441,53), null);


(lib.masque_generique = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#5FAFE5").s().p("AsQCqIAAlTIYhAAIAAFTg");
	this.shape.setTransform(78.5,17);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.masque_generique, new cjs.Rectangle(0,0,157,34), null);


(lib.ligne = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(49));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#333333").ss(1,1,1).p("EAAAg/bMAAAB+3");
	this.shape.setTransform(0,406);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#333333").ss(1,1,1).p("EAA/A/cQhDv0ggwoQgdwxAExFQAEwXAhwHQAfufA4to");
	this.shape_1.setTransform(-6,406);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#333333").ss(1,1,1).p("EABwA/cQh4vfg5w8Qg0wqAHxQQAHweA7wDQA5uxBjtQ");
	this.shape_2.setTransform(-10.7,406);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#333333").ss(1,1,1).p("EACVA/cQigvPhLxLQhHwmAKxYQAJwjBPv/QBMvACEs9");
	this.shape_3.setTransform(-14.2,406);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#333333").ss(1,1,1).p("EACwA/cQi+vDhZxWQhTwjAMxeQAKwnBev8QBavKCcsw");
	this.shape_4.setTransform(-16.9,406);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#333333").ss(1,1,1).p("EADEA/cQjTu7hjxdQhdwhANxhQAMwrBov6QBkvSCusm");
	this.shape_5.setTransform(-18.8,406);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#333333").ss(1,1,1).p("EADSA/cQjiu1hqxjQhjwfAOxlQAMwsBvv5QBsvXC6sf");
	this.shape_6.setTransform(-20.1,406);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#333333").ss(1,1,1).p("EADbA/cQjsuxhuxnQhnweAOxnQANwtB0v4QBwvaDCsb");
	this.shape_7.setTransform(-20.9,406);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#333333").ss(1,1,1).p("EADgA/cQjyuuhxxpQhpwdAPxpQANwuB3v3QByvdDHsY");
	this.shape_8.setTransform(-21.4,406);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#333333").ss(1,1,1).p("EADjA/cQj1uthyxqQhrwdAPxpQANwvB5v3QB0vdDJsX");
	this.shape_9.setTransform(-21.7,406);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#333333").ss(1,1,1).p("EADkA/cQj2uthzxqQhswdAPxpQANwvB6v3QB1veDKsW");
	this.shape_10.setTransform(-21.8,406);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#333333").ss(1,1,1).p("EADlA/cQj3ush0xrQhrwdAPxpQANwvB5v3QB2veDLsW");
	this.shape_11.setTransform(-21.9,406);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#333333").ss(1,1,1).p("EADlA/cQj3ushzxrQhswdAPxpQANwvB6v3QB1veDLsW");
	this.shape_12.setTransform(-21.9,406);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#333333").ss(1,1,1).p("EADlg/bQjLMWh1PeQh6P3gNQvQgPRpBsQdQBzRrD3Os");
	this.shape_13.setTransform(-21.9,406);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#333333").ss(1,1,1).p("EADlA/cQj3ushzxrQhswdAPxpQANwvB5v3QB2veDLsW");
	this.shape_14.setTransform(-21.9,406);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#333333").ss(1,1,1).p("EADlA/cQj3ushzxrQhswdAPxpQAOwvB5v3QB1veDLsW");
	this.shape_15.setTransform(-21.9,406);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#333333").ss(1,1,1).p("EADkA/cQj2uthzxqQhrwdAPxpQANwvB5v3QB1veDKsW");
	this.shape_16.setTransform(-21.8,406);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#333333").ss(1,1,1).p("EADjA/cQj0uthzxqQhqwdAOxpQAOwvB4v2QB0veDJsX");
	this.shape_17.setTransform(-21.7,406);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#333333").ss(1,1,1).p("EADhA/cQjyuuhyxpQhpwdAOxpQANwuB4v3QBzvdDHsY");
	this.shape_18.setTransform(-21.5,406);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#333333").ss(1,1,1).p("EADeA/cQjvuvhwxoQhoweAOxoQANwuB2v3QBxvcDFsZ");
	this.shape_19.setTransform(-21.2,406);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#333333").ss(1,1,1).p("EADaA/cQjruxhuxnQhnweAPxmQAMwuB0v4QBwvaDBsb");
	this.shape_20.setTransform(-20.9,406);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#333333").ss(1,1,1).p("EADWA/cQjnuzhrxlQhlweAOxmQANwsBxv5QBtvZC+sd");
	this.shape_21.setTransform(-20.4,406);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#333333").ss(1,1,1).p("EADQA/cQjgu2hpxiQhiwfAOxlQAMwsBuv4QBrvXC4sg");
	this.shape_22.setTransform(-19.9,406);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#333333").ss(1,1,1).p("EADJA/cQjZu4hlxgQhewgANxjQAMwrBqv6QBnvTCysk");
	this.shape_23.setTransform(-19.2,406);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#333333").ss(1,1,1).p("EADAA/cQjPu8hhxcQhawhAMxhQAMwqBlv7QBjvQCqso");
	this.shape_24.setTransform(-18.4,406);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#333333").ss(1,1,1).p("EAC2A/cQjEvAhcxZQhWwiAMxfQALwoBhv8QBdvMChst");
	this.shape_25.setTransform(-17.4,406);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#333333").ss(1,1,1).p("EACqA/cQi3vFhWxUQhRwjAMxdQAKwnBav8QBXvICXsz");
	this.shape_26.setTransform(-16.3,406);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#333333").ss(1,1,1).p("EACdA/cQipvLhPxPQhKwkAKxaQAKwlBTv+QBQvDCLs5");
	this.shape_27.setTransform(-15,406);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#333333").ss(1,1,1).p("EACOA/cQiYvShIxIQhCwnAJxWQAIwiBLwAQBJu9B9tB");
	this.shape_28.setTransform(-13.6,406);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#333333").ss(1,1,1).p("EAB8A/cQiFvZg/xCQg6woAIxSQAHwhBCwBQA/u2ButK");
	this.shape_29.setTransform(-11.9,406);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#333333").ss(1,1,1).p("EABpA/cQhxvig1w5QgxwrAHxPQAGwdA3wDQA2uvBdtT");
	this.shape_30.setTransform(-10,406);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#333333").ss(1,1,1).p("EABYA/cQhevpgswyQgqwuAGxKQAFwbAvwFQAsuoBOtc");
	this.shape_31.setTransform(-8.4,406);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#333333").ss(1,1,1).p("EABIA/cQhNvwgkwsQgiwvAFxHQAEwYAmwHQAkujBAtj");
	this.shape_32.setTransform(-6.9,406);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#333333").ss(1,1,1).p("EAA7A/cQg/v2gdwmQgcwxAExEQADwXAgwIQAdudA0tq");
	this.shape_33.setTransform(-5.6,406);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#333333").ss(1,1,1).p("EAAvA/cQgyv7gYwhQgWwzADxBQADwVAZwJQAXuZAqtw");
	this.shape_34.setTransform(-4.5,406);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#333333").ss(1,1,1).p("EAAlA/cQgnv/gTweQgRwzACxAQACwTAUwKQASuVAht1");
	this.shape_35.setTransform(-3.5,406);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#333333").ss(1,1,1).p("EAAcA/cQgdwDgPwaQgNw1ACw9QABwSAQwLQANuSAZt5");
	this.shape_36.setTransform(-2.7,406);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#333333").ss(1,1,1).p("EAAVA/cQgWwGgLwXQgKw2ACw7QABwSALwMQAKuPATt8");
	this.shape_37.setTransform(-2,406);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#333333").ss(1,1,1).p("EAAQA/cQgQwIgIwVQgHw3ABw6QABwRAIwMQAHuNAOt/");
	this.shape_38.setTransform(-1.5,406);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#333333").ss(1,1,1).p("EAALA/cQgLwLgFwSQgFw3AAw6QABwQAGwMQAFuMAJuB");
	this.shape_39.setTransform(-1.1,406);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#333333").ss(1,1,1).p("EAAIA/cQgIwMgDwSQgEw3ABw5QAAwPAEwNQADuKAHuD");
	this.shape_40.setTransform(-0.7,406);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#333333").ss(1,1,1).p("EAAFA/cQgFwNgBwRQgDw3ABw5QAAwOACwOQACuJAEuE");
	this.shape_41.setTransform(-0.5,406);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#333333").ss(1,1,1).p("EAADA/cQgDwOAAwQQgCw3ABw4QAAwOABwPQAAuIADuF");
	this.shape_42.setTransform(-0.3,406);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#333333").ss(1,1,1).p("EAABA/cQgBwOAAwQQgBw4ABw3QAAwOAAwOQAAuIABuG");
	this.shape_43.setTransform(-0.1,406);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#333333").ss(1,1,1).p("EAAAA/cQAAwPAAwPQAAw4AAw3QAAwOAAwOQAAuIAAuG");
	this.shape_44.setTransform(0,406);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#333333").ss(1,1,1).p("EAAAA/cQAAwPAAwPQAAw4AAw3QAAwOAAwOQAAuHAAuH");
	this.shape_45.setTransform(0,406);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1,-1,2,814);


(lib.lettres_FAT = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#5FAFE5").s().p("AkBFeIAAq6IH3AAIAACKIlaAAIAACQIE3AAIAACEIk3AAIAACRIFmAAIAACLg");
	this.shape.setTransform(194.8,311.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#5FAFE5").s().p("AixBDIAAiEIFjAAIAACEg");
	this.shape_1.setTransform(124,316.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#5FAFE5").s().p("AjeEDQhohlgBicQABibBqhnQBqhpCZAAQCsAAB0CEIhhBuQhJhchuAAQhYgBg+A6Qg/A6AABgQAABhA8A8QA6A6BUAAQByABBJhcIBlBoQh3CFiiAAQigABhphng");
	this.shape_2.setTransform(-63.8,311.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#5FAFE5").s().p("ADTFeIhBiYIkjAAIhCCYIilAAIEtq7ICWAAIEvK7gAhXA+ICvAAIhXjLg");
	this.shape_3.setTransform(21,187.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#5FAFE5").s().p("ABxFeIiajgIhsAAIAADgIibAAIAAq7IEIAAQCjAABFA3QBGA3AAB6QAACniGAxICyD7gAiVgIIBzAAQBPAAAcgaQAdgaAAg4QAAg5gdgVQgegUhLgBIh1AAg");
	this.shape_4.setTransform(-61.8,187.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#5FAFE5").s().p("AhNFdIAAoyIjGAAIAAiHIInAAIAACHIjGAAIAAIyg");
	this.shape_5.setTransform(-70.5,63.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.lettres_FAT, new cjs.Rectangle(-102.9,0,343,373.7), null);


(lib.ile = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.hublet_TRACE_ile();
	this.instance.parent = this;
	this.instance.setTransform(-339.5,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-339.5,0,679,618);


(lib.hastaggraphicbranche = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AjQAuIAShbIDlAAICqAAIgSBbg");
	this.shape.setTransform(0,4.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.hastaggraphicbranche, new cjs.Rectangle(-20.8,0,41.7,9.2), null);


(lib.grille = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").p("EAAAhL2MAAACXt");
	this.shape.setTransform(-453.2,679.7,1.4,1.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#FFFFFF").p("EAAAhL2MAAACXt");
	this.shape_1.setTransform(-226.6,679.7,1.4,1.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#FFFFFF").p("EAAAhL2MAAACXt");
	this.shape_2.setTransform(0,679.7,1.4,1.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#FFFFFF").p("EAAAhL2MAAACXt");
	this.shape_3.setTransform(226.5,679.7,1.4,1.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#FFFFFF").p("EAAAhL2MAAACXt");
	this.shape_4.setTransform(453.1,679.7,1.4,1.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#FFFFFF").p("EhL2AAAMCXtAAA");
	this.shape_5.setTransform(0,226.5,1.4,1.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#FFFFFF").p("EhL2AAAMCXtAAA");
	this.shape_6.setTransform(0,453,1.4,1.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#FFFFFF").p("EhL2AAAMCXtAAA");
	this.shape_7.setTransform(0,679.6,1.4,1.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#FFFFFF").p("EhL2AAAMCXtAAA");
	this.shape_8.setTransform(0,906.1,1.4,1.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#FFFFFF").p("EhL2AAAMCXtAAA");
	this.shape_9.setTransform(0,1132.7,1.4,1.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#FFFFFF").p("EhL2hL2MCXtAAAMAAACXtMiXtAAAg");
	this.shape_10.setTransform(0,679.7,1.4,1.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.grille, new cjs.Rectangle(-680.7,-1,1361.4,1361.3), null);


(lib.Btn_cliquable_area = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#5FAFE5").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape.setTransform(0,30);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Btn_cliquable_area, new cjs.Rectangle(-113,0,226,60), null);


(lib.nuagemouvant = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.nuage("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(645.7,367.7,1.268,1.268,0,0,0,0.1,130.9);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(197).to({_off:false},0).to({regX:0,x:-909,y:326.2},239).to({_off:true},1).wait(36));

	// Layer_1
	this.instance_1 = new lib.nuage("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(645.7,367.7,2.113,2.113,0,0,0,0.1,130.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({regX:0,x:-909,y:326.3},367).to({_off:true},1).wait(105));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-71.7,91.2,1434.5,553.5);


(lib.masqueTexte = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// masqueTexte_1_ligne
	this.instance = new lib.masqueTexte_1_ligne();
	this.instance.parent = this;
	this.instance.setTransform(-218.8,191.5,0.008,1,0,0,0,0,25.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(6).to({_off:false},0).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.01,x:218.8},14).wait(1));

	// masqueTexte_1_ligne
	this.instance_1 = new lib.masqueTexte_1_ligne();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-218.8,136.1,0.008,1,0,0,0,0,25.5);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.01,x:218.8},14).to({_off:true},1).wait(2));

	// masqueTexte_1_ligne
	this.instance_2 = new lib.masqueTexte_1_ligne();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-218.8,80.8,0.008,1,0,0,0,0,25.5);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(2).to({_off:false},0).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.01,x:218.8},14).to({_off:true},1).wait(4));

	// masqueTexte_1_ligne
	this.instance_3 = new lib.masqueTexte_1_ligne();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-218.8,25.5,0.008,1,0,0,0,0,25.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.01,x:218.8},14).to({_off:true},1).wait(6));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-220.5,-1.9,3.5,53);


(lib.lignes_all = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{close:114,close_lines:157});

	// timeline functions:
	this.frame_113 = function() {
		this.stop();
	}
	this.frame_156 = function() {
		this.stop();
	}
	this.frame_174 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(113).call(this.frame_113).wait(43).call(this.frame_156).wait(18).call(this.frame_174).wait(1));

	// ligne
	this.instance = new lib.ligne();
	this.instance.parent = this;
	this.instance.setTransform(-454,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(114).to({x:-883},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:-454},0).wait(18));

	// ligne
	this.instance_1 = new lib.ligne();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-227,406.5,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(114).to({x:-883.2},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:-227},0).wait(18));

	// ligne
	this.instance_2 = new lib.ligne();
	this.instance_2.parent = this;
	this.instance_2.setTransform(0,406.5,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(114).to({x:-883.4},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:0},0).wait(18));

	// ligne
	this.instance_3 = new lib.ligne();
	this.instance_3.parent = this;
	this.instance_3.setTransform(227,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(93).to({y:322},20,cjs.Ease.quartInOut).wait(1).to({y:323},0).to({x:-883,y:406},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:227,y:322},0).to({y:406},16,cjs.Ease.quartInOut).wait(2));

	// ligne
	this.instance_4 = new lib.ligne();
	this.instance_4.parent = this;
	this.instance_4.setTransform(454.1,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(114).to({x:-883.4},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:454.1},0).wait(18));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-454.5,-0.5,909.2,813.5);


(lib.hashtag_graphic = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(64));

	// hastag graphic branche
	this.instance = new lib.hastaggraphicbranche();
	this.instance.parent = this;
	this.instance.setTransform(37.4,51.9,1.084,0.995,0,-78.3,102.1,0.1,4.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({x:6.5,y:50.1},17,cjs.Ease.quartOut).to({_off:true},5).wait(3).to({_off:false},0).to({_off:true},4).wait(2).to({_off:false,x:26.5},0).to({_off:true},5).wait(28));

	// hastag graphic branche
	this.instance_1 = new lib.hastaggraphicbranche();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-41.6,49.9,1.084,0.995,0,-78.3,102.1,0.1,4.6);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off:false},0).to({x:-7.4,y:49.8},18,cjs.Ease.quartOut).to({_off:true},4).wait(5).to({_off:false},0).to({_off:true},4).wait(31));

	// hastag graphic branche
	this.instance_2 = new lib.hastaggraphicbranche();
	this.instance_2.parent = this;
	this.instance_2.setTransform(2.7,21.7,1,1,0,0,0,0,4.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({x:-2.3,y:58.7},13,cjs.Ease.quartOut).to({_off:true},11).wait(3).to({_off:false,scaleX:2.33,scaleY:1.03,skewX:-12.8,x:25.5},0).to({_off:true},4).wait(33));

	// hastag graphic branche
	this.instance_3 = new lib.hastaggraphicbranche();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-7.3,77.7,1,1,0,0,0,0,4.6);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({x:0.7,y:41.7},14,cjs.Ease.quartOut).to({_off:true},4).wait(7).to({_off:false,regX:0.1,scaleX:1.98,scaleY:1.01,skewX:-9.8,x:19.9,y:49.7},0).to({_off:true},4).wait(4).to({_off:false,regX:0,scaleX:1,scaleY:1,skewX:0,x:0.7,y:41.7},0).to({_off:true},4).wait(21));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-18.1,17.1,64.6,57.7);


(lib.Btn_next = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{rollOver:55,rollOut:70});

	// timeline functions:
	this.frame_54 = function() {
		this.stop();
	}
	this.frame_69 = function() {
		this.stop();
	}
	this.frame_84 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(54).call(this.frame_54).wait(15).call(this.frame_69).wait(15).call(this.frame_84).wait(1));

	// ZONE
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.008)").s().p("EgjTAEsIAApXMBGnAAAIAAJXg");
	this.shape.setTransform(113,29.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(85));

	// txt
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgIAqIAAhDIgYAAIAAgQIBBAAIAAAQIgYAAIAABDg");
	this.shape_1.setTransform(161.1,29.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AASAqIgSgcIgQAcIgYAAIAdgqIgagpIAWAAIAPAZIAQgZIAWAAIgaApIAcAqg");
	this.shape_2.setTransform(145.2,29.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgeAqIAAhTIA8AAIAAARIgpAAIAAARIAkAAIAAAPIgkAAIAAASIAqAAIAAAQg");
	this.shape_3.setTransform(129.3,29.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAUAqIgngzIAAAzIgTAAIAAhTIASAAIAoA1IAAg1IATAAIAABTg");
	this.shape_4.setTransform(112.3,29.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgKAQIAEgOQgEgCAAgFQAAgEADgDQADgDAEAAQAFAAADADQADADAAAEQAAAEgDAEIgIANg");
	this.shape_5.setTransform(86.5,32.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAPAqIgYghIgIAKIAAAXIgTAAIAAhTIATAAIAAAjIAegjIAYAAIgiAlIAQAWIASAYg");
	this.shape_6.setTransform(77,29.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgeAfQgNgNAAgSQAAgSANgMQANgNARAAQASAAAOANQAMAMAAASQAAASgMANQgOAMgSAAQgRAAgNgMgAgRgSQgHAIAAAKQAAALAHAIQAIAIAJAAQAKAAAIgIQAHgIAAgLQAAgKgHgIQgIgIgKAAQgJAAgIAIg");
	this.shape_7.setTransform(59.3,29.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#333333").ss(1,1,1).p("EgjOAADMBGdgAF");
	this.shape_8.setTransform(113,-0.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(85));

	// Layer_2
	this.instance = new lib.Btn_cliquable_area();
	this.instance.parent = this;
	this.instance.setTransform(113,29.5,2,1,0,0,0,0,30);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("rgba(0,0,0,0.008)").s().p("EgjTAEsIAApXMBGnAAAIAAJXg");
	this.shape_9.setTransform(113,29.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance}]},3).to({state:[{t:this.instance}]},46).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.instance}]},5).to({state:[{t:this.instance}]},14).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},14).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({alpha:0.012},46).to({_off:true},1).wait(5).to({_off:false},0).to({alpha:1},14).wait(1).to({alpha:0.012},14).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-113.5,-1.5,453,61);


(lib.Btn_detail = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"rollOver":55,"rollOut":70});

	// timeline functions:
	this.frame_54 = function() {
		this.stop();
	}
	this.frame_69 = function() {
		this.stop();
	}
	this.frame_84 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(54).call(this.frame_54).wait(15).call(this.frame_69).wait(15).call(this.frame_84).wait(1));

	// ZONE
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.008)").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape.setTransform(0,29.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(85));

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgOAyIAAhkIAdAAIAABkg");
	this.shape_1.setTransform(-85.4,29.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgyAOIAAgcIBlAAIAAAcg");
	this.shape_2.setTransform(-85.4,29.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgaAqIAAhTIASAAIAABDIAjAAIAAAQg");
	this.shape_3.setTransform(38.4,29.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgIAqIAAhTIARAAIAABTg");
	this.shape_4.setTransform(24.7,29.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AAZAqIgHgSIgjAAIgHASIgUAAIAkhTIARAAIAkBTgAgKAIIAUAAIgKgYg");
	this.shape_5.setTransform(10.2,29.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgIAqIAAhDIgYAAIAAgQIBBAAIAAAQIgYAAIAABDg");
	this.shape_6.setTransform(-5.9,29.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgeAqIAAhTIA8AAIAAARIgpAAIAAARIAkAAIAAAPIgkAAIAAASIAqAAIAAAQg");
	this.shape_7.setTransform(-21.3,29.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AglAqIAAhTIAeAAQAVAAAMALQAMALAAATQAAATgMALQgMAMgXAAgAgTAaIANAAQAMAAAGgHQAHgGAAgNQAAgLgHgHQgGgGgOgBIgLAAg");
	this.shape_8.setTransform(-37.8,29.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#333333").ss(1,1,1).p("AxpAAMAjTAAA");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(85));

	// Layer_2
	this.instance = new lib.Btn_cliquable_area();
	this.instance.parent = this;
	this.instance.setTransform(0,29.5,1,1,0,0,0,0,30);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("rgba(0,0,0,0.008)").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape_10.setTransform(0,29.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance}]},3).to({state:[{t:this.instance}]},45).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.instance}]},6).to({state:[{t:this.instance}]},14).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},13).wait(2));
	this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({alpha:0.012},45).to({_off:true},1).wait(6).to({_off:false},0).to({alpha:1},14).wait(1).to({alpha:0.012},13).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-114,-1,228,60.5);


(lib.bloc_video = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":41});

	// timeline functions:
	this.frame_40 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(40).call(this.frame_40).wait(17));

	// masque video (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_42 = new cjs.Graphics().p("Ega8A8AMAAAh3/MA7rAAAMAAAB3/g");
	var mask_graphics_43 = new cjs.Graphics().p("EgYrA8AMAAAh3/MA3HAAAMAAAB3/g");
	var mask_graphics_44 = new cjs.Graphics().p("EgWaA8AMAAAh3/MAyiAAAMAAAB3/g");
	var mask_graphics_45 = new cjs.Graphics().p("EgUJA8AMAAAh3/MAt+AAAMAAAB3/g");
	var mask_graphics_46 = new cjs.Graphics().p("EgR4A8AMAAAh3/MApaAAAMAAAB3/g");
	var mask_graphics_47 = new cjs.Graphics().p("EgPnA8AMAAAh3/MAk1AAAMAAAB3/g");
	var mask_graphics_48 = new cjs.Graphics().p("EgNWA8AMAAAh3/MAgRAAAMAAAB3/g");
	var mask_graphics_49 = new cjs.Graphics().p("EgLFA8AMAAAh3/IbtAAMAAAB3/g");
	var mask_graphics_50 = new cjs.Graphics().p("EgI0A8AMAAAh3/IXIAAMAAAB3/g");
	var mask_graphics_51 = new cjs.Graphics().p("EgGjA8AMAAAh3/ISkAAMAAAB3/g");
	var mask_graphics_52 = new cjs.Graphics().p("EgESA8AMAAAh3/IOAAAMAAAB3/g");
	var mask_graphics_53 = new cjs.Graphics().p("EgCBA8AMAAAh3/IJbAAMAAAB3/g");
	var mask_graphics_54 = new cjs.Graphics().p("EAAPA8AMAAAh3/IE4AAMAAAB3/g");
	var mask_graphics_55 = new cjs.Graphics().p("EACgA8AMAAAh3/IAUAAMAAAB3/g");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(42).to({graphics:mask_graphics_42,x:209.5,y:0}).wait(1).to({graphics:mask_graphics_43,x:194.8,y:0}).wait(1).to({graphics:mask_graphics_44,x:180,y:0}).wait(1).to({graphics:mask_graphics_45,x:165.3,y:0}).wait(1).to({graphics:mask_graphics_46,x:150.6,y:0}).wait(1).to({graphics:mask_graphics_47,x:135.8,y:0}).wait(1).to({graphics:mask_graphics_48,x:121.1,y:0}).wait(1).to({graphics:mask_graphics_49,x:106.4,y:0}).wait(1).to({graphics:mask_graphics_50,x:91.6,y:0}).wait(1).to({graphics:mask_graphics_51,x:76.9,y:0}).wait(1).to({graphics:mask_graphics_52,x:62.2,y:0}).wait(1).to({graphics:mask_graphics_53,x:47.4,y:0}).wait(1).to({graphics:mask_graphics_54,x:32.7,y:0}).wait(1).to({graphics:mask_graphics_55,x:18,y:0}).wait(1).to({graphics:null,x:0,y:0}).wait(1));

	// VIDEO
	this.videoContainer = new lib.video();
	this.videoContainer.name = "videoContainer";
	this.videoContainer.parent = this;
	this.videoContainer.setTransform(36.7,765,0.659,0.659,0,0,0,0.3,480.2);

	var maskedShapeInstanceList = [this.videoContainer];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.videoContainer).to({y:-33},40,cjs.Ease.quartInOut).to({_off:true},16).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(36.5,448.7,382.1,632.3);


(lib.bloc_NEXT_btn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":20});

	// timeline functions:
	this.frame_19 = function() {
		this.stop();
	}
	this.frame_36 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(19).call(this.frame_19).wait(17).call(this.frame_36).wait(1));

	// CTA DETAIL
	this.btnNext = new lib.Btn_next();
	this.btnNext.name = "btnNext";
	this.btnNext.parent = this;
	this.btnNext.setTransform(-340.5,411.5,1,1,0,0,0,0,26.5);

	this.timeline.addTween(cjs.Tween.get(this.btnNext).to({y:350.5},19,cjs.Ease.quartInOut).wait(2).to({y:411.5},14,cjs.Ease.quartInOut).to({_off:true},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-453.5,384,452,60.5);


(lib.bloc_hashtag = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_61 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(61).call(this.frame_61).wait(2));

	// Layer_1
	this.instance = new lib.hashtag_graphic("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(-456,243.2,2.317,2.317,0,0,0,-0.1,50.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true},61).wait(2));

	// Layer_5 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_28 = new cjs.Graphics().p("AtWBpIAAjRIatAAIAADRg");
	var mask_graphics_32 = new cjs.Graphics().p("AtWDSIAAmjIatAAIAAGjg");
	var mask_graphics_40 = new cjs.Graphics().p("AtWGYIAAsvIatAAIAAMvg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(28).to({graphics:mask_graphics_28,x:-400.4,y:265.5}).wait(4).to({graphics:mask_graphics_32,x:-400.4,y:255}).wait(8).to({graphics:mask_graphics_40,x:-400.4,y:255.9}).wait(23));

	// Layer_3
	this.instance_1 = new lib.txt_hashtag();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-421.5,244.1,1,1,0,0,0,0,31.6);
	this.instance_1._off = true;

	var maskedShapeInstanceList = [this.instance_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(28).to({_off:false},0).to({_off:true},34).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-497.8,166.1,149.7,129.4);


(lib.bloc_CTA = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":25});

	// timeline functions:
	this.frame_24 = function() {
		this.stop();
	}
	this.frame_41 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(24).call(this.frame_24).wait(17).call(this.frame_41).wait(1));

	// CTA DETAIL
	this.btnCta = new lib.Btn_detail();
	this.btnCta.name = "btnCta";
	this.btnCta.parent = this;
	this.btnCta.setTransform(-340.5,411.5,1,1,0,0,0,0,26.5);

	this.timeline.addTween(cjs.Tween.get(this.btnCta).to({y:350.5},24,cjs.Ease.quartInOut).wait(2).to({y:411.5},14,cjs.Ease.quartInOut).to({_off:true},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-454,384.5,227,60);


(lib.project_illustration_placeHolder = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_106 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(106).call(this.frame_106).wait(1));

	// hublet_TRACE_contour.png
	this.instance = new lib.hublet_TRACE_contour();
	this.instance.parent = this;
	this.instance.setTransform(-411,-73);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(107));

	// Layer_2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("A+UeVQskskAAxxQAAxwMkskQMkskRwAAQJmAAIEDqQG5DIFyFyQMkMkAARwQAARxskMkQsOMOxJAWIg+AAQxwAAskskg");
	mask.setTransform(-61.3,251.9);

	// Layer_8
	this.instance_1 = new lib.nuagemouvant();
	this.instance_1.parent = this;
	this.instance_1.setTransform(791.7,277.3,1,1,0,0,0,645.5,367.9);

	var maskedShapeInstanceList = [this.instance_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(107));

	// Layer_9
	this.instance_2 = new lib.grille();
	this.instance_2.parent = this;
	this.instance_2.setTransform(343.9,220.8,1,1,0,0,0,0,679.6);
	this.instance_2.alpha = 0.422;

	var maskedShapeInstanceList = [this.instance_2];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({x:-422,y:-131.1},36,cjs.Ease.quartOut).to({x:-194.3,y:-110.4},35,cjs.Ease.quartInOut).wait(36));

	// Layer_7
	this.instance_3 = new lib.nuage("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(617.1,53.3,1.408,1.408,0,0,0,0.1,130.9);
	this.instance_3._off = true;

	var maskedShapeInstanceList = [this.instance_3];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(11).to({_off:false},0).to({regX:0,scaleX:1.4,scaleY:1.4,x:-544.8,y:75.1},22).to({_off:true},1).wait(73));

	// Layer_6
	this.instance_4 = new lib.nuage("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(521.3,332.3,1.291,1.291,0,0,0,0.1,130.9);

	var maskedShapeInstanceList = [this.instance_4];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_4).to({x:-720.5},16).to({_off:true},1).wait(90));

	// hublet_TRACE_ile.jpg
	this.instance_5 = new lib.ile("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(569.6,245,1,1,0,0,0,0,309);

	var maskedShapeInstanceList = [this.instance_5];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_5).to({x:-176.5,y:261},36,cjs.Ease.quartOut).to({x:-73.5,y:248},35,cjs.Ease.quartInOut).wait(36));

	// CITIA_2018_a.png
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#086999").s().p("A+UeVQskskAAxxQAAxwMkskQMkskRwAAQJmAAIEDqQG5DIFyFyQMkMkAARwQAARxskMkQsOMOxJAWIg+AAQxwAAskskg");
	this.shape.setTransform(-61.3,251.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#00CD6A").ss(1,1,1).p("EgNkgq4QAfAAAeAAQRwAAMlMkQMkMkAARwQAARxskMkQslMkxwAAQplAAoFjq");
	this.shape_1.setTransform(19.4,251.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(107));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-411,-73,700,654);


(lib.prject_illustration = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.project_illustration_placeHolder();
	this.instance.parent = this;
	this.instance.setTransform(52.2,-112.3,0.7,0.7,0,0,0,-15,-268.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(140));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-225,-246,1343.9,952.2);


(lib.blocInfostextes = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":80});

	// timeline functions:
	this.frame_79 = function() {
		this.stop();
	}
	this.frame_120 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(79).call(this.frame_79).wait(41).call(this.frame_120).wait(1));

	// Bloc Hashtag
	this.instance = new lib.bloc_hashtag();
	this.instance.parent = this;
	this.instance.setTransform(-391.5,261.2,0.9,0.9,0,0,0,-412.9,244.7);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(62).to({_off:false},0).to({_off:true},18).wait(41));

	// masque Titre
	this.instance_1 = new lib.masque_generique();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-527.9,-13.9,0.007,0.471,0,0,0,0,17.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(34).to({_off:false},0).to({regX:0.5,scaleX:1.92,x:-527.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.01,x:-228.9},10,cjs.Ease.quartInOut).to({_off:true},1).wait(26).to({_off:false},0).to({regX:0.5,scaleX:1.92,x:-527.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.01,x:-528.7},10,cjs.Ease.quartInOut).to({_off:true},1).wait(21));

	// _NOM-du-CLIENT
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#5FAFE5").s().p("AAEAVIAAgpIATAAIAAApgAgWAVIAAgpIATAAIAAApg");
	this.shape.setTransform(-272.7,-17.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#5FAFE5").s().p("AgoA4IAAhvIBPAAIAAAXIg2AAIAAAWIAxAAIAAAUIgxAAIAAAYIA4AAIAAAWg");
	this.shape_1.setTransform(-293.1,-14.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#5FAFE5").s().p("AgiApQgSgQABgYQgBgZASgQQAQgRAYAAQAcAAASAVIgPASQgMgPgSAAQgNAAgJAJQgKAJAAAPQgBAPAKAKQAJAKAMAAQATAAALgPIAQARQgSAVgaAAQgZAAgQgRg");
	this.shape_2.setTransform(-316.3,-14.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#5FAFE5").s().p("AAiA4IgKgYIgvAAIgKAYIgbAAIAxhvIAXAAIAxBvgAgNAKIAbAAIgOgfg");
	this.shape_3.setTransform(-340.2,-14.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#5FAFE5").s().p("AASA4IgYgjIgRAAIAAAjIgZAAIAAhvIAqAAQAaAAALAJQALAJAAATQAAAagWAIIAdAogAgXgBIASAAQAMAAAFgDQAEgFAAgJQAAgIgFgEQgEgEgLAAIgTAAg");
	this.shape_4.setTransform(-363.6,-14.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#5FAFE5").s().p("AgMA4IAAhZIgfAAIAAgWIBXAAIAAAWIggAAIAABZg");
	this.shape_5.setTransform(-386.7,-14.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#5FAFE5").s().p("AAEAVIAAgpIATAAIAAApgAgWAVIAAgpIATAAIAAApg");
	this.shape_6.setTransform(-407.1,-17.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#5FAFE5").s().p("AgpApQgRgQAAgZQAAgXARgRQARgRAYAAQAZAAARARQARARAAAXQAAAZgRAQQgRARgZAAQgYAAgRgRgAgXgYQgJAKAAAOQAAAPAJALQAKAKANAAQAOAAAKgKQAJgLAAgPQAAgOgJgKQgKgLgOAAQgNAAgKALg");
	this.shape_7.setTransform(-445.3,-14.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#5FAFE5").s().p("AgrA4IAAhvIAnAAQAZAAAMAKQALAKAAAVQAAAUgMAJQgMAKgYAAIgOAAIAAAfgAgSADIAQAAQAMAAAEgEQAFgFAAgKQAAgJgGgEQgGgEgLgBIgOAAg");
	this.shape_8.setTransform(-469,-14.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#5FAFE5").s().p("AAYA4IgYglIgXAlIgeAAIAlg5Igjg2IAfAAIAUAiIAVgiIAeAAIgiA2IAlA5g");
	this.shape_9.setTransform(-492.3,-14.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#5FAFE5").s().p("AgoA4IAAhvIBPAAIAAAXIg2AAIAAAWIAxAAIAAAUIgxAAIAAAYIA4AAIAAAWg");
	this.shape_10.setTransform(-514.8,-14.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},43).to({state:[]},46).to({state:[]},1).wait(31));

	// DECO 1
	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#5FAFE5").s().p("AgUhpIApgqIAAEng");
	this.shape_11.setTransform(-81,-73.2,4.184,4.184,-91.7);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#5FAFE5").s().p("AgtArIBbhaIg9Bfg");
	this.shape_12.setTransform(-181.9,-117.6,4.184,4.184,-91.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#5FAFE5").s().p("AgUiSIApD8IgpAqg");
	this.shape_13.setTransform(-207,-87.6,4.184,4.184,-91.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#5FAFE5").s().p("AiTAVID9gpIAqApg");
	this.shape_14.setTransform(-151.2,-17.3,4.184,4.184,-91.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#5FAFE5").s().p("AAQgvIAeAEIhbBbg");
	this.shape_15.setTransform(-106.1,-43.1,4.184,4.184,-91.7);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#5FAFE5").s().p("AgvgPIAEgeIBcBbg");
	this.shape_16.setTransform(-106.8,-118.2,4.184,4.184,-91.7);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#5FAFE5").s().p("AgvgtIBgA8IgFAfg");
	this.shape_17.setTransform(-181.2,-42.4,4.184,4.184,-91.7);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#5FAFE5").s().p("AiTgUIEnAAIj9Apg");
	this.shape_18.setTransform(-136.8,-143.3,4.184,4.184,-91.7);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#5FAFE5").s().p("AAAGBIAAg1IgJgBIgBAZIgBABIgBgBIABgZIgKAAIgCAYIgBABIgBgBIACgYIgKgBIgCAYIgBABIgBgBIACgYIgJgCIgDAZIgBAAIgBAAIAAgBIADgYIgJgBIgIApIgBABIAAAAIgBgBIAIgqIgKgBIgFAXIgBABIAAAAIgBgBIAFgYIgJgCIgGAYIgBAAIAAAAQgBAAAAAAQAAAAAAAAQAAAAAAAAQAAAAAAgBIAGgXIgJgDIgHAXIgBABIAAAAIgBgBIAHgYIgJgCIgIAXIgBABIAAgBIgBgBIAIgXIgJgDIgPAoIgBAAIAAAAIgBgBIAPgoIgJgDIgJAWIgBABIgBAAIAAgBIAJgXIgJgEIgKAXIgBAAIAAAAIgBgBIAKgXIgIgEIgLAWIgBABIAAAAQgBAAAAAAQAAAAAAgBQAAAAAAAAQAAAAAAgBIALgVIgJgFIgLAWIgBAAIAAAAIgBgBIAMgWIgJgEIgbAuIgBABIAAAAIAAgCIAbguIgJgFIgNAUIAAABIgBAAIAAgCIANgUIgIgGIgOAVIgBAAIAAAAIgBgBIAOgVIgIgFIgOAUIgCAAIAAgCIAPgTIgIgGIgPATIgCAAIAAgBIAPgTIgHgGIgbAgIgCAAIAAgBIAbghIgHgGIgQASIgCAAIAAgBIARgSIgHgHIgRASIgBAAIgBAAIAAgCIARgRIgGgHIgTARIgBAAIAAgBIASgRIgGgHIgTAQIAAAAIgBAAIAAgCIASgQIgGgHIghAbIgBAAQAAAAAAAAQgBgBAAAAQAAAAABAAQAAAAAAAAIAhgcIgGgHIgUAPIAAAAIgBAAIAAgCIAUgPIgGgHIgUAOIgBAAIAAAAIAAgCIAUgOIgGgIIgUAOIgBAAIgBAAIABgCIAUgNIgFgIIgVANIgBAAIAAgBIAAgBIAVgNIgFgIIgvAbIAAAAIgBgBIAAgBIAvgbIgFgIIgVALIgBAAIgBAAIABgCIAVgLIgEgJIgWALIgBAAIAAAAQgBgBAAAAQAAAAAAAAQAAgBAAAAQAAAAABAAIAWgKIgEgJIgWAKIgBAAIgBgBIABgBIAWgKIgEgJIgWAJIgBAAIgBAAIABgCIAXgJIgEgJIgoAPIAAAAIgBgBIABgBIAngOIgDgJIgXAHIAAAAIgBgBIAAgBIAYgHIgDgKIgYAHIgBAAIABgCIAXgGIgCgKIgYAGIAAAAIgBgBQAAAAAAAAQAAAAAAAAQAAgBAAAAQAAAAABAAIAXgGIgCgJIgYAFIgBgBIABgBIAYgFIgCgJIgqAHIgBgBQAAAAAAAAQAAAAAAAAQAAgBAAAAQAAAAABAAIAqgHIgCgKIgYAEIgBgBIABgBIAYgEIgBgJIgZACIgBgBIABgBIAYgCIAAgKIgZACIgBgBIABgBIAZgCIgBgJIgYABIgBgBIABgBIAYgBIAAgKIg2AAIgBgBIABAAIA2AAIAAgJIgYgBIgBgBIABgBIAYABIABgKIgZgCIgBgBIABgBIAZACIAAgJIgYgDIgBgBQAAgBAAAAQAAAAAAAAQABAAAAAAQAAAAAAAAIAZADIABgKIgYgDIgBgBIABgBIAYADIABgJIgpgIIgBgBQAAAAAAAAQAAAAAAgBQAAAAABAAQAAAAAAAAIAqAIIACgKIgYgFQgBAAAAAAQAAAAAAAAQAAAAAAAAQAAgBAAAAIABgBIAYAFIACgJIgXgGIgBgBIABgBIAYAGIACgJIgXgHIgBgBIABgBIAYAHIACgJIgXgIIAAgBIABgBIAXAIIADgJIgogPIAAgBIABgBIAoAPIADgJIgWgJIgBgBQAAgBAAAAQAAAAABAAQAAAAAAAAQAAAAABAAIAWAJIAEgJIgWgKQgBAAAAAAQAAAAAAAAQAAAAAAAAQAAgBAAAAQAAAAAAAAQAAgBAAAAQABAAAAAAQAAABAAAAIAXAKIAEgJIgWgLIgBgBIACgBIAWALIAEgIIgWgMIAAgBIABgBIAWAMIAFgJIgvgaIAAgCQAAAAAAAAQAAgBAAAAQAAAAAAAAQABAAAAABIAvAbIAFgIIgVgNIgBgCIACAAIAVANIAFgIIgUgOIgBgBIACgBIAUAOIAFgIIgTgOIgBgBIACgBIAUAPIAFgIIgTgPIAAgBIABgBIAUAQIAGgIIghgbIAAgCIABAAIAhAcIAGgIIgSgQIAAgBIABgBIATARIAGgHIgSgRIAAgCIACAAIARARIAHgGIgRgSQAAAAAAAAQAAgBAAAAQAAAAAAAAQAAAAAAAAIACAAIARARIAHgGIgRgSQAAgBAAAAQAAAAAAAAQAAgBAAAAQAAAAAAAAIACAAIAQASIAHgGIgbggQAAgBAAAAQAAAAAAAAQAAgBAAAAQAAAAAAAAIACAAIAbAhIAHgGIgPgTQAAgBAAAAQAAAAAAAAQAAgBAAAAQAAAAAAAAIACAAIAPAUIAIgGIgPgUQAAAAAAAAQgBgBAAAAQAAAAABAAQAAAAAAAAIACAAIAOAUIAIgGIgOgUIABgCIABABIAOAUIAIgFIgNgVIAAgBIABAAIANAVIAJgFIgbgvQgBAAAAAAQAAgBAAAAQAAAAAAAAQAAAAABAAQAAgBAAAAQABAAAAAAQAAAAAAAAQAAABAAAAIAbAvIAJgFIgMgVQAAgBAAAAQAAAAAAAAQAAgBAAAAQAAAAABAAQAAAAAAAAQAAAAAAAAQABAAAAAAQAAAAAAABIALAVIAJgEIgLgWQAAgBAAAAQAAAAAAAAQAAAAAAAAQAAAAABAAQAAgBAAAAQAAAAABAAQAAAAAAABQAAAAAAAAIALAWIAIgEIgKgWIABgCIABABIAKAWIAJgEIgJgWIAAgCIACABIAJAXIAJgEIgPgoIABgBIABABIAPAnIAJgDIgIgXIABgBIABABIAIAXIAJgDIgHgXQAAgBAAAAQAAAAAAAAQAAgBAAAAQABAAAAAAIABABIAHAXIAJgCIgGgYQAAAAAAAAQAAAAAAgBQAAAAAAAAQAAAAABAAQAAAAAAAAQAAAAAAAAQABAAAAAAQAAAAAAABIAGAXIAJgCIgFgYQAAAAAAAAQAAAAAAAAQAAgBAAAAQAAAAABAAIABABIAFAYIAKgCIgIgqIABgBIABABIAHAqIAKgCIgDgYIAAgBIABABIAEAYIAJgBIgCgYQAAgBAAAAQAAAAAAAAQAAgBAAAAQABAAAAAAIABABIACAYIAKAAIgCgZIABgBIABABIACAZIAJgBIAAgYIABgBIABABIAAAYIAKAAIAAg2IAAgBQAAAAAAAAQABAAAAAAQAAAAAAAAQAAAAAAABIAAA2IAKAAIAAgYIABgBIABABIAAAYIAJABIACgZIABgBIABABIgCAZIAKAAIACgYIABgBQABAAAAAAQAAAAAAABQAAAAAAAAQAAAAAAABIgCAYIAJABIAEgYIABgBIAAABIgDAYIAKACIAHgqIABgBIABABIgHAqIAJACIAFgYIABgBQABAAAAAAQAAAAAAABQAAAAAAAAQAAAAAAAAIgFAYIAJACIAGgXQAAgBAAAAQAAAAABAAQAAAAAAAAQAAAAAAAAIABABIgGAYIAJACIAHgXIABgBQABAAAAAAQAAAAAAABQAAAAAAAAQAAAAAAABIgHAXIAJADIAIgXIABgBIABABIgIAXIAJADIAPgnIABgBIABABIgPAoIAJAEIAJgXIACgBIAAACIgJAWIAJAEIAKgWIABgBIABACIgKAWIAIAEIALgWQAAAAAAAAQAAgBABAAQAAAAAAAAQAAAAAAABQABAAAAAAQAAAAAAAAQAAAAAAAAQAAAAAAABIgLAWIAJAEIALgVQAAgBAAAAQAAAAABAAQAAAAAAAAQAAAAAAAAIABABIAAABIgMAVIAJAFIAbgvIABAAQABAAAAAAQAAAAAAABQAAAAAAAAQAAAAgBAAIgbAvIAJAFIANgVIABAAIAAABIgNAVIAIAFIAOgUIABgBIABACIgOAUIAIAGIAOgUIACAAQAAAAAAAAQAAAAAAAAQAAAAAAABQAAAAAAAAIgPAUIAIAGIAPgUIACAAQAAAAAAAAQAAAAAAABQAAAAAAAAQAAAAAAABIgPATIAHAGIAbghIACAAQAAAAAAAAQABAAAAABQAAAAgBAAQAAAAAAABIgbAgIAHAGIAQgSIACAAQAAAAAAAAQABAAAAABQAAAAgBAAQAAAAAAABIgRASIAHAGIARgRIACAAQAAAAAAAAQABAAAAAAQAAAAgBABQAAAAAAAAIgRASIAHAGIARgRIACAAIAAACIgSARIAGAHIATgRIABABIAAABIgSAQIAGAIIAhgcIABAAIAAACIghAbIAGAIIAUgQIABABIAAABIgTAPIAFAIIAUgPIACABIgBABIgUAOIAGAIIAUgOIACABIgBABIgUAOIAFAIIAVgNIABAAQAAAAABABQAAAAAAAAQAAAAAAAAQgBABAAAAIgVANIAFAIIAvgbQAAgBABAAQAAAAAAAAQAAAAAAABQAAAAAAAAQABAAAAABQAAAAAAAAQAAAAAAAAQAAAAgBAAIgvAbIAFAJIAWgMIABABQAAAAAAAAQAAAAAAABQAAAAAAAAQAAAAAAAAIgWAMIAEAIIAWgLIACABIgBABIgWALIAEAJIAXgKQAAAAAAgBQAAAAAAAAQABAAAAABQAAAAAAAAQAAAAAAABQAAAAAAAAQAAAAAAAAQAAAAgBAAIgWAKIAEAJIAWgJQABAAAAAAQAAAAAAAAQABAAAAAAQAAAAAAABIgBABIgWAJIADAJIAogPIABABIAAABIgoAPIADAJIAXgIQAAAAAAAAQABAAAAAAQAAABAAAAQAAAAAAAAIAAABIgYAIIADAJIAYgHIABABIgBABIgXAHIACAJIAYgGIABABIgBABIgXAGIACAJIAYgFIABABQAAAAAAABQAAAAAAAAQAAAAAAAAQAAAAgBAAIgYAFIACAKIAqgIIABABIgBABIgqAIQACAEAAAFIAYgDIABABIgBABIgYADIABAKIAZgDQAAAAAAAAQAAAAAAAAQABAAAAAAQAAAAAAABIgBABIgYADIAAAJIAZgCIABABIgBABIgYACIAAAKIAYgBIABABIAAABIgZABIAAAJIA2AAIABAAIgBABIg2AAIAAAKIAZABIABABIgBABIgZgBIAAAJIAYACIABABIgBABIgZgCIAAAKIAYACQAAAAAAAAQAAAAAAABQABAAAAAAQAAAAAAABIgBAAIgZgCIgBAJIAYAEIABABIgBABIgYgEIgBAKIApAHIABABIgBABIAAAAIgqgHIgCAJIAYAFQABAAAAAAQAAAAAAABQAAAAAAAAQAAAAAAAAIgBABIgYgFIgCAJIAXAGIABABIgBABIAAAAIgYgGIgCAKIAXAGQABAAAAAAQAAAAAAABQAAAAAAAAQAAAAAAABIgBAAIgYgHIgDAKIAYAHIAAABIgBABIAAAAIgXgHIgDAJIAoAOIAAABIgBABIAAAAIgogPIgDAJIAWAJIABACIgBAAIgBAAIgWgJIgEAJIAWAKIABABIgBABIgXgKIgEAJIAWAKIABACIgBAAIgBAAIgWgLIgEAJIAWALQAAABAAAAQAAAAAAAAQAAAAAAAAQAAABAAAAIgBAAIAAAAIgWgLIgFAIIAvAbQABAAAAAAQAAAAAAABQAAAAAAAAQAAAAgBAAIAAABIgBAAIgvgbIgFAIIAVANIABABIgBABIgBAAIgVgNIgFAIIAUANIABACIgBAAIgBAAIgUgOIgGAIIAUAOIABACIgBAAIgBAAIgUgOIgFAHIATAPIAAACIgBAAIAAAAIgUgPIgGAHIAhAcIAAABIgBAAIghgbIgGAHIASAQIAAACIgBAAIAAAAIgTgQIgGAGIASASIAAABIgBAAIgSgRIgHAHIARARQAAABAAAAQAAAAAAAAQAAAAAAAAQAAABAAAAIgBAAIgBAAIgRgSIgHAHIARASQAAAAAAAAQAAAAAAABQAAAAAAAAQAAAAAAAAIgCAAIgQgSIgHAGIAbAhIAAABIgCAAIgbggIgHAGIAPATQAAAAAAAAQAAAAAAABQAAAAAAAAQAAAAAAAAIgCAAIgPgTIgIAGIAPATQAAAAAAABQAAAAAAAAQAAAAAAAAQAAABAAAAIgBAAIgBgBIgOgTIgIAFIAOAUQAAABAAAAQAAAAAAAAQAAABAAAAQAAAAAAAAIgBAAIgBAAIgNgVIgJAFIANAVQAAABABAAQAAAAAAAAQAAAAAAAAQgBABAAAAIAAAAIgBgBIgNgUIgIAEIAbAvIgBACIAAAAIgBgBIgbgvIgIAFIALAWQAAAAAAAAQAAAAAAABQAAAAAAAAQAAAAAAAAIgBAAIgBAAIgLgWIgJAEIALAWIgBACIgBgBIgLgWIgIAEIAKAXIgBABIAAAAIgBgBIgKgWIgJAEIAJAXIAAABIgBAAIgBgBIgJgWIgJADIAPAoQAAAAAAABQAAAAAAAAQAAAAAAAAQAAAAgBAAIAAAAIgBAAIgPgoIgJADIAIAXQAAAAAAABQAAAAAAAAQAAAAAAAAQAAAAgBAAIgBAAIgHgXIgKACIAHAYIgBABIgBgBIgHgXIgJACIAGAYIgBABIgBAAIgGgYIgJACIAFAYIgBABIgBgBIgFgXIgJABIAHAqIgBABIgBgBIgHgpIgKABIAEAYIgBABIgBAAIgEgZIgJACIACAYIgBABIgBgBIgCgYIgKABIACAYIgBABIgBgBIgCgYIgJAAIABAZIgBABIgBgBIgBgZIgKABIAAA1IgBABgAhwk0QhEAZg0AzQgzA0gZBFQgUA2AAA5QAAA7AUA2QAYBEA0A0QA0A0BEAZQAmANApAFIAhACIAigCQApgFAmgNQBEgZA0g0QA0g0AZhEQATg3AAg6QAAg5gTg2QgZhFg0g0Qg0gzhEgZQg2gUg7AAQg5AAg3AUg");
	this.shape_19.setTransform(-307.2,79.9,2.172,2.172,-10);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_18,p:{scaleX:4.184,scaleY:4.184,rotation:-91.7,x:-136.8,y:-143.3}},{t:this.shape_17,p:{scaleX:4.184,scaleY:4.184,rotation:-91.7,x:-181.2,y:-42.4}},{t:this.shape_16,p:{scaleX:4.184,scaleY:4.184,rotation:-91.7,x:-106.8,y:-118.2}},{t:this.shape_15,p:{scaleX:4.184,scaleY:4.184,rotation:-91.7,x:-106.1,y:-43.1}},{t:this.shape_14,p:{scaleX:4.184,scaleY:4.184,rotation:-91.7,x:-151.2,y:-17.3}},{t:this.shape_13,p:{scaleX:4.184,scaleY:4.184,rotation:-91.7,x:-207,y:-87.6}},{t:this.shape_12,p:{scaleX:4.184,scaleY:4.184,rotation:-91.7,x:-181.9,y:-117.6}},{t:this.shape_11,p:{scaleX:4.184,scaleY:4.184,rotation:-91.7,x:-81,y:-73.2}}]},27).to({state:[{t:this.shape_18,p:{scaleX:2.172,scaleY:2.172,rotation:-10,x:-274.2,y:78.9}},{t:this.shape_17,p:{scaleX:2.172,scaleY:2.172,rotation:-10,x:-329.4,y:63.6}},{t:this.shape_16,p:{scaleX:2.172,scaleY:2.172,rotation:-10,x:-284.9,y:96.2}},{t:this.shape_15,p:{scaleX:2.172,scaleY:2.172,rotation:-10,x:-323.4,y:102.1}},{t:this.shape_14,p:{scaleX:2.172,scaleY:2.172,rotation:-10,x:-340.1,y:80.9}},{t:this.shape_13,p:{scaleX:2.172,scaleY:2.172,rotation:-10,x:-308.1,y:47}},{t:this.shape_12,p:{scaleX:2.172,scaleY:2.172,rotation:-10,x:-290.8,y:57.7}},{t:this.shape_11,p:{scaleX:2.172,scaleY:2.172,rotation:-10,x:-306.1,y:112.8}},{t:this.shape_19}]},16).to({state:[]},27).wait(51));

	// DECO 2
	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#FFFFFF").ss(1,1,1).p("AW3ISI4mONI1HzBILk58IcQC+g");
	this.shape_20.setTransform(-22.7,68.8);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#FFFFFF").ss(1,1,1).p("AnFmSICWm8ISbU+I7XFfIAAgB");
	this.shape_21.setTransform(-240,-49.6);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AqGJxIGnzhIAAAAINmPeI0MEDg");
	this.shape_22.setTransform(-262.9,-27.5);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#FFFFFF").ss(1,1,1).p("ALnNZI3NtWIXJtbg");
	this.shape_23.setTransform(-577,114.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_20}]},93).to({state:[{t:this.shape_22},{t:this.shape_21}]},4).to({state:[{t:this.shape_23}]},3).to({state:[]},6).to({state:[]},1).wait(14));

	// masque Titre (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_17 = new cjs.Graphics().p("EgpOArJMAAAhWRMAvJAAAMAAABWRg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(17).to({graphics:mask_graphics_17,x:-263.9,y:-6.1}).wait(24).to({graphics:null,x:0,y:0}).wait(80));

	// _LETTRES_ANARCHIQUES
	this.instance_2 = new lib.lettres_FAT();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-433.8,-73,1,1,0,0,0,0,186.8);
	this.instance_2._off = true;

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#5FAFE5").s().p("AkAFeIAAq6IH3AAIAACKIlbAAIAACQIE4AAIAACEIk4AAIAACRIFmAAIAACLg");
	this.shape_24.setTransform(-98.8,7.9);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#5FAFE5").s().p("ADUFeIhBiXIklAAIhACXIinAAIEvq6ICWAAIEtK6gAhWA+ICtAAIhXjLg");
	this.shape_25.setTransform(-412.8,7.9);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#5FAFE5").s().p("AByFeIiajfIhsAAIAADfIidAAIAAq6IEKAAQCiAABFA2QBFA4AAB6QAACmiEAyICwD6gAiUgHIBxAAQBPAAAdgaQAegbAAg4QAAg4gfgWQgegUhJAAIh1AAg");
	this.shape_26.setTransform(-495.6,7.9);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#5FAFE5").s().p("AixBCIAAiDIFjAAIAACDg");
	this.shape_27.setTransform(-198.7,-111.1);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#5FAFE5").s().p("AjeEDQhohmgBibQAAibBrhnQBqhpCZAAQCsAAB0CEIhhBuQhJhchuAAQhYAAg+A5Qg/A5AABhQAABhA8A8QA6A6BUAAQByABBJhcIBlBnQh3CGiiAAQigABhphng");
	this.shape_28.setTransform(-271.2,-116.4);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#5FAFE5").s().p("AhNFdIAAoyIjGAAIAAiHIInAAIAACHIjGAAIAAIyg");
	this.shape_29.setTransform(-504.4,-116);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#5FAFE5").s().p("AkEECQhqhoAAiaQAAiZBqhoQBqhnCaAAQCbAABqBnQBqBoAACZQAACahqBoQhqBnibAAQiaAAhqhngAiSieQg8BCAABcQAABeA8BCQA8BBBWAAQBYAAA8hBQA8hCAAheQAAhcg8hCQg8hChYAAQhWAAg8BCg");
	this.shape_30.setTransform(-320,-17.6);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#5FAFE5").s().p("AkUFeIAAq7ID2AAQCgAABKA/QBJA+AACCQAACAhLA8QhLA9ibgBIhcAAIAADEgAh4ATIBpAAQBMAAAcgeQAcggAAg8QABg9glgZQglgahLAAIhZAAg");
	this.shape_31.setTransform(-404.6,-17.1);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#5FAFE5").s().p("ACUFeIiVjnIiTDnIi8AAIDrllIjblWIC6AAICFDPICFjPIC8AAIjaFRIDsFqg");
	this.shape_32.setTransform(-483,-141);

	var maskedShapeInstanceList = [this.instance_2,this.shape_24,this.shape_25,this.shape_26,this.shape_27,this.shape_28,this.shape_29,this.shape_30,this.shape_31,this.shape_32];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_2}]},17).to({state:[{t:this.instance_2}]},14).to({state:[{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24}]},1).to({state:[{t:this.shape_32},{t:this.shape_31},{t:this.shape_30}]},7).to({state:[]},9).to({state:[]},32).wait(41));
	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(17).to({_off:false},0).to({x:-276.8},14,cjs.Ease.quartOut).to({_off:true},1).wait(89));

	// masque Titre
	this.instance_3 = new lib.masque_generique();
	this.instance_3.parent = this;
	this.instance_3.setTransform(221.1,-163.9,0.013,0.471,0,0,0,0,17.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({regX:0.5,scaleX:1.92,x:-75.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.01,x:-68.7},10,cjs.Ease.quartInOut).to({_off:true},1).wait(101));

	// masque Titre
	this.instance_4 = new lib.masque_generique();
	this.instance_4.parent = this;
	this.instance_4.setTransform(-228.9,-13.9,0.013,0.471,0,0,0,0,17.2);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(4).to({_off:false},0).to({regX:0.5,scaleX:1.92,x:-527.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.01,x:-528.7},10,cjs.Ease.quartInOut).to({_off:true},1).wait(97));

	// _LETTRES_CLIENT
	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#5FAFE5").s().p("AgJA0QgEgEAAgGQAAgFAEgFQAEgEAFAAQAGAAAEAEQAEAFAAAFQAAAGgEAEQgEAFgGAAQgFAAgEgFgAgJAVIgEg5IAAgUIAbAAIAAAUIgFA5g");
	this.shape_33.setTransform(-357.9,-14.1);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#5FAFE5").s().p("AAbA4Ig1hFIAABFIgZAAIAAhvIAYAAIA2BHIAAhHIAZAAIAABvg");
	this.shape_34.setTransform(-378.9,-14.1);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#5FAFE5").s().p("AgoA4IAAhvIBPAAIAAAXIg2AAIAAAWIAxAAIAAAUIgxAAIAAAYIA4AAIAAAWg");
	this.shape_35.setTransform(-402.4,-14.1);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#5FAFE5").s().p("AgMA4IAAhvIAZAAIAABvg");
	this.shape_36.setTransform(-422.5,-14.1);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#5FAFE5").s().p("AgCA5IADgYIgNAAIgEAYIgZAAIAFgYIgQAAIAFgWIAPAAIADgTIgRAAIAEgWIARAAIAEgaIAXAAIgDAaIANAAIAEgaIAZAAIgFAaIARAAIgEAWIgRAAIgDATIASAAIgEAWIgSAAIgEAYgAgJALIAOAAIAEgTIgOAAg");
	this.shape_37.setTransform(-459.4,-14.3);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#5FAFE5").s().p("AgjA4IAAhvIAZAAIAABYIAuAAIAAAXg");
	this.shape_38.setTransform(-497.6,-14.1);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#5FAFE5").s().p("AgjApQgRgQAAgYQAAgZARgQQASgRAXAAQAcAAASAVIgPASQgMgPgSAAQgNAAgKAJQgJAJgBAPQAAAPAKAKQAJAKANAAQASAAALgPIAQARQgTAVgZAAQgZAAgRgRg");
	this.shape_39.setTransform(-520.4,-14.2);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#5FAFE5").s().p("AgMA4IAAhZIgfAAIAAgWIBXAAIAAAWIggAAIAABZg");
	this.shape_40.setTransform(-411.9,-14.1);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#5FAFE5").s().p("AAbA4Ig0hFIAABFIgaAAIAAhvIAYAAIA2BHIAAhHIAZAAIAABvg");
	this.shape_41.setTransform(-435.3,-14.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36,p:{x:-422.5}},{t:this.shape_35,p:{x:-402.4}},{t:this.shape_34},{t:this.shape_33,p:{x:-357.9}}]},13).to({state:[{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_33,p:{x:-422.7}}]},5).to({state:[{t:this.shape_39},{t:this.shape_38},{t:this.shape_36,p:{x:-478.9}},{t:this.shape_35,p:{x:-458.8}},{t:this.shape_41},{t:this.shape_40}]},5).to({state:[]},20).to({state:[]},37).wait(41));

	// Layer_11
	this.instance_5 = new lib.masqueTexte("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(-328.9,105.9,0.9,0.85,0,0,0,0.1,108.9);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(37).to({_off:false},0).to({_off:true},40).wait(3).to({_off:false,regX:0.2,regY:108.8,rotation:180,x:-329,y:105.8},0).to({_off:true},40).wait(1));

	// _TEXTE_PRESENTATION
	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAggeQAhgeArAAQAtAAAfAbQAfAbAAAuIAAAfIicAAQADASAPALQAQALATAAQAfAAAVgVIAiAlQglAigxAAQgwAAgggegAgdgyQgPALgCATIBdAAQgCgUgNgKQgMgKgSAAQgQAAgPAKg");
	this.shape_42.setTransform(-180.6,133.4);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("AhLCBQgggeAAgzQAAgyAggeQAhgeAsAAQAsAAAfAbQAfAbAAAuIAAAfIicAAQADATAPAKQAQAMATAAQAfAAAVgWIAhAlQgkAigxAAQgwAAgggegAgdgCQgPALgDASIBeAAQgCgUgNgJQgMgKgRAAQgSAAgOAKgAglhXIAzhHIA+AaIg4Atg");
	this.shape_43.setTransform(-205.3,128.6);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFFFFF").s().p("AgfB5QgWgUAAgkIAAhiIgZAAIAAgrIAZAAIAAhAIA7AAIAABAIA1AAIAAArIg1AAIAABgQABAMAGAHQAGAHAJAAQARAAAMgPIAWAqQgcAZgfAAQgeAAgVgUg");
	this.shape_44.setTransform(-226,130.4);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFFFFF").s().p("AAoBtIAAh4QABgyglAAQgRAAgNANQgOANAAAZIAAB3Ig7AAIAAjWIA7AAIAAAYQAcgbAgAAQAjAAAXAYQAWAYAAAnIAACCg");
	this.shape_45.setTransform(-248,133.3);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAggeQAhgeAsAAQAsAAAfAbQAfAbAAAuIAAAfIicAAQADASAPALQAQALATAAQAfAAAVgVIAiAlQglAigxAAQgwAAgggegAgdgyQgPALgDATIBeAAQgCgUgNgKQgMgKgRAAQgSAAgOAKg");
	this.shape_46.setTransform(-273.1,133.4);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFFFFF").s().p("ABtBtIAAh0QAAgygkAAQgSAAgNANQgOANAAAZIAABzIg7AAIAAh0QAAgagIgMQgIgMgRAAQgSAAgNANQgNANAAAZIAABzIg8AAIAAjWIA8AAIAAAYQAZgbAfAAQAVAAAQALQAQANAIARQANgUAVgLQAVgKAVAAQAmAAAXAWQAXAXAAAqIAACCg");
	this.shape_47.setTransform(-305.1,133.3);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FFFFFF").s().p("Ag1CQQgZgIgSgOIAXguQAdAXAhAAQAgAAAUgRQATgQAAgkQgdAggpAAQgqAAgdgdQgdgcAAgvQAAgwAegfQAegeAmAAQAlAAAaAgIAAgdIA8AAIAAC7QAAAdgKAWQgKAWgRANQghAbgtAAQgYAAgZgIgAgihVQgQAPAAAZQAAAaAPAQQAPAOAVAAQAWAAAOgOQAPgQAAgZQAAgagPgPQgOgPgVAAQgVAAgPAPg");
	this.shape_48.setTransform(-338.8,137.5);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FFFFFF").s().p("AhMBXQgYgWAAgsIAAiBIA9AAIAAB0QAAAxAkAAQARAAANgMQANgNAAgZIAAhzIA9AAIAADWIg9AAIAAgcQgWAfgiAAQgkAAgYgWg");
	this.shape_49.setTransform(-364.5,133.6);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FFFFFF").s().p("AhJBbQgXgUAAghQAAghAYgPQAYgQApAAIAsAAIAAgCQAAgigkAAQgPAAgRAGQgRAGgNAKIgbgnQArgfA1AAQAnAAAZAUQAZATAAArIAACIIg4AAIAAgaQgYAdghAAQghAAgYgUgAgkAmQAAALAIAHQAKAGAQAAQAOAAANgKQAMgKAAgQIAAgLIglAAQgkAAAAAXg");
	this.shape_50.setTransform(-389.5,133.4);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FFFFFF").s().p("AhQBPQghggAAgvQAAguAhggQAgggAwAAQAxAAAgAgQAhAgAAAuQAAAvghAgQggAggxAAQgwAAgggggAglgqQgPARAAAZQAAAbAPAQQAPARAWAAQAXAAAPgRQAPgQAAgbQAAgZgPgRQgPgRgXAAQgWAAgPARg");
	this.shape_51.setTransform(-424.4,133.4);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FFFFFF").s().p("AhLCBQgggeAAgzQAAgyAggeQAhgeAsAAQAsAAAfAbQAfAbAAAuIAAAfIicAAQADATAPAKQAQAMATAAQAfAAAVgWIAhAlQgkAigxAAQgwAAgggegAgdgCQgPALgDASIBeAAQgCgUgNgJQgMgKgRAAQgSAAgOAKgAglhXIAzhHIA+AaIg4Atg");
	this.shape_52.setTransform(-449.6,128.6);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FFFFFF").s().p("AhQB2QgeggAAgwQAAgwAdgeQAegeAngBQAmAAAZAcIAAhqIA8AAIAAEoIg8AAIAAgcQgaAfglAAQgmAAgegggAgigDQgPARAAAaQAAAaAPARQAQAQATAAQAVAAAPgQQAOgRAAgaQAAgagOgRQgPgSgVAAQgUAAgPASg");
	this.shape_53.setTransform(-476,129.5);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FFFFFF").s().p("AgdCXIAAjWIA7AAIAADWgAgYhaQgKgKAAgPQAAgPAKgKQAKgKAOAAQAPAAAKAKQAKAKAAAPQAAAPgKAKQgKAKgPAAQgOAAgKgKg");
	this.shape_54.setTransform(-494.6,129.1);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FFFFFF").s().p("AgdBsIhWjXIA+AAIA1CHIA2iHIA+AAIhVDXg");
	this.shape_55.setTransform(-512.1,133.4);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FFFFFF").s().p("Ag1CQQgZgIgSgOIAXguQAdAXAhAAQAgAAAUgRQATgQAAgkQgdAggpAAQgqAAgdgdQgdgcAAgvQAAgwAegfQAegeAmAAQAlAAAaAgIAAgdIA8AAIAAC7QAAAdgKAWQgKAWgRANQghAbgtAAQgYAAgZgIgAgihVQgQAPAAAZQAAAaAPAQQAPAOAVAAQAWAAAOgOQAPgQAAgZQAAgagPgPQgOgPgVAAQgVAAgPAPg");
	this.shape_56.setTransform(-296.8,88.8);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FFFFFF").s().p("AAoBtIAAh4QAAgygkAAQgRAAgNANQgOANAAAZIAAB3Ig8AAIAAjWIA8AAIAAAYQAbgbAiAAQAhAAAXAYQAYAYAAAnIAACCg");
	this.shape_57.setTransform(-322.2,84.5);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#FFFFFF").s().p("AgdCXIAAjWIA7AAIAADWgAgYhaQgKgKAAgPQAAgPAKgKQAKgKAOAAQAPAAAKAKQAKAKAAAPQAAAPgKAKQgKAKgPAAQgOAAgKgKg");
	this.shape_58.setTransform(-340.9,80.3);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#FFFFFF").s().p("AhuCUIAAkjIA8AAIAAAXQAcgbAkAAQAmAAAdAfQAeAgAAAvQAAAvgeAgQgdAfgnABQgngBgYgfIAABqgAgkhPQgPARAAAaQAAAbAPAPQAPAQAUAAQAUAAAQgQQAPgPAAgaQAAgbgPgRQgPgSgUAAQgVAAgPASg");
	this.shape_59.setTransform(-359.5,88.4);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#FFFFFF").s().p("AhuCUIAAkjIA8AAIAAAXQAcgbAkAAQAmAAAdAfQAeAgAAAvQAAAvgeAgQgdAfgnABQgngBgYgfIAABqgAgkhPQgPARAAAaQAAAbAPAPQAPAQAUAAQAUAAAQgQQAPgPAAgaQAAgbgPgRQgPgSgUAAQgVAAgPASg");
	this.shape_60.setTransform(-386.1,88.4);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#FFFFFF").s().p("AhJBbQgXgUAAghQAAghAYgPQAYgQApAAIAsAAIAAgCQAAgigkAAQgPAAgRAGQgSAGgMAKIgbgnQArgfA1AAQAnAAAZAUQAZATAAArIAACIIg4AAIAAgaQgXAdgiAAQgiAAgXgUgAgkAmQAAALAIAHQAKAGAQAAQAOAAAMgKQANgKAAgQIAAgLIglAAQgkAAAAAXg");
	this.shape_61.setTransform(-412.4,84.7);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#FFFFFF").s().p("ABtBtIAAh0QAAgygkAAQgSAAgNANQgOANAAAZIAABzIg7AAIAAh0QAAgagIgMQgIgMgRAAQgSAAgNANQgNANAAAZIAABzIg8AAIAAjWIA8AAIAAAYQAZgbAfAAQAVAAAQALQAQANAIARQANgUAVgLQAVgKAVAAQAmAAAXAWQAXAXAAAqIAACCg");
	this.shape_62.setTransform(-443.3,84.5);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#FFFFFF").s().p("AAoBtIAAh4QAAgygkAAQgRAAgNANQgOANAAAZIAAB3Ig7AAIAAjWIA7AAIAAAYQAbgbAhAAQAjAAAWAYQAYAYAAAnIAACCg");
	this.shape_63.setTransform(-486.2,84.5);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAhgeQAggeArAAQAtAAAfAbQAfAbAAAuIAAAfIicAAQADASAQALQAPALATAAQAfAAAVgVIAhAlQgkAigxAAQgwAAgggegAgdgyQgPALgDATIBeAAQgCgUgMgKQgNgKgRAAQgRAAgPAKg");
	this.shape_64.setTransform(-511.4,84.7);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAhgeQAggeArAAQAtAAAfAbQAfAbAAAuIAAAfIicAAQADASAQALQAPALATAAQAfAAAVgVIAhAlQgkAigxAAQgwAAgggegAgdgyQgPALgCATIBdAAQgCgUgMgKQgNgKgSAAQgRAAgOAKg");
	this.shape_65.setTransform(-267.6,35.9);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#FFFFFF").s().p("AgdCXIAAjWIA7AAIAADWgAgYhaQgKgKAAgPQAAgPAKgKQAKgKAOAAQAPAAAKAKQAKAKAAAPQAAAPgKAKQgKAKgPAAQgOAAgKgKg");
	this.shape_66.setTransform(-285.9,31.6);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#FFFFFF").s().p("AAoCVIAAh2QAAgwgkgBQgRAAgNAOQgOAMAAAYIAAB1Ig7AAIAAkoIA7AAIAABqQAcgcAgAAQAiAAAXAYQAXAYAAAmIAACEg");
	this.shape_67.setTransform(-304.3,31.9);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#FFFFFF").s().p("AhuCUIAAkkIA8AAIAAAZQAcgcAkAAQAmAAAdAfQAeAgAAAvQAAAvgeAgQgdAggngBQgnABgYggIAABqgAgkhQQgPASAAAaQAAAaAPAQQAPAQAUABQAUgBAQgQQAPgPAAgbQAAgagPgSQgPgRgUAAQgVAAgPARg");
	this.shape_68.setTransform(-330,39.7);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#FFFFFF").s().p("AhJBbQgXgUAAghQAAghAYgPQAYgQApAAIAsAAIAAgCQAAgigkAAQgPAAgRAGQgRAGgNAKIgbgnQArgfA1AAQAnAAAZAUQAZATAAArIAACIIg4AAIAAgaQgYAdghAAQghAAgYgUgAgkAmQAAALAIAHQAKAGAQAAQAOAAANgKQAMgKAAgQIAAgLIglAAQgkAAAAAXg");
	this.shape_69.setTransform(-356.2,35.9);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#FFFFFF").s().p("Ag9BtIAAjWIA8AAIAAAdQAKgOASgJQARgJASAAIAAA4IgLAAQgbAAgNATQgMATAAAeIAABdg");
	this.shape_70.setTransform(-375,35.8);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#FFFFFF").s().p("Ag1CQQgZgIgSgOIAXguQAdAXAhAAQAgAAAUgRQATgQAAgkQgdAggpAAQgqAAgdgdQgdgcAAgvQAAgwAegfQAegeAmAAQAlAAAaAgIAAgdIA8AAIAAC7QAAAdgKAWQgKAWgRANQghAbgtAAQgYAAgZgIgAgihVQgQAPAAAZQAAAaAPAQQAPAOAVAAQAWAAAOgOQAPgQAAgZQAAgagPgPQgOgPgVAAQgVAAgPAPg");
	this.shape_71.setTransform(-398,40);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#FFFFFF").s().p("AhQBPQghggAAgvQAAguAhggQAgggAwAAQAxAAAgAgQAhAgAAAuQAAAvghAgQggAggxAAQgwAAgggggAglgqQgPARAAAZQAAAbAPAQQAPARAWAAQAXAAAPgRQAPgQAAgbQAAgZgPgRQgPgRgXAAQgWAAgPARg");
	this.shape_72.setTransform(-423.8,35.9);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#FFFFFF").s().p("AggB5QgVgUAAgkIAAhiIgZAAIAAgrIAZAAIAAhAIA8AAIAABAIA0AAIAAArIg0AAIAABgQAAAMAGAHQAGAHAJAAQARAAALgPIAXAqQgcAZgfAAQgeAAgWgUg");
	this.shape_73.setTransform(-445,32.9);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#FFFFFF").s().p("Ag9BtIAAjWIA8AAIAAAdQAKgOASgJQARgJASAAIAAA4IgLAAQgbAAgNATQgMATAAAeIAABdg");
	this.shape_74.setTransform(-461.7,35.8);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#FFFFFF").s().p("AhJBbQgXgUAAghQAAghAYgPQAYgQApAAIAsAAIAAgCQAAgigkAAQgPAAgRAGQgRAGgNAKIgbgnQArgfA1AAQAnAAAZAUQAZATAAArIAACIIg4AAIAAgaQgYAdghAAQghAAgYgUgAgkAmQAAALAIAHQAKAGAQAAQAOAAANgKQAMgKAAgQIAAgLIglAAQgkAAAAAXg");
	this.shape_75.setTransform(-483.1,35.9);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#FFFFFF").s().p("AhYBoQgqgpAAg+QAAg9AqgqQArgqA9AAQBFABAuA0IgnAsQgeglgsAAQgiAAgZAXQgYAXAAAmQAAAnAXAYQAYAXAhAAQAuAAAdgkIAnApQgvA2hAgBQhAAAgqgog");
	this.shape_76.setTransform(-509,32.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_76},{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42}]},57).to({state:[]},43).to({state:[]},1).wait(20));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(221.1,-172,2,16);


(lib.bloc_visuel = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":68});

	// timeline functions:
	this.frame_67 = function() {
		this.stop();
	}
	this.frame_104 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(67).call(this.frame_67).wait(37).call(this.frame_104).wait(1));

	// masque VISUEL (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("EgjsBDvMAAAiHdMBHZAAAMAAACHdg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:-228.7,y:-19}).wait(87).to({graphics:null,x:0,y:0}).wait(18));

	// VISUEL
	this.instance = new lib.prject_illustration();
	this.instance.parent = this;
	this.instance.setTransform(242.6,-660.6,1,1,0,0,0,11.6,-308.4);
	this.instance._off = true;

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(26).to({_off:false},0).to({regX:11.7,regY:-308.3,x:-85.5,y:-660.5},33,cjs.Ease.quartOut).wait(15).to({regX:24.7,regY:256.6,x:-72.5,y:-95.6},0).wait(1).to({regX:20,regY:253.2,x:-77.7,y:-99},0).wait(1).to({rotation:-0.1,x:-79.2},0).wait(1).to({rotation:-0.2,x:-82},0).wait(1).to({rotation:-0.3,x:-86.1},0).wait(1).to({rotation:-0.5,x:-91.9,y:-98.9},0).wait(1).to({rotation:-0.7,x:-99.6},0).wait(1).to({rotation:-1.1,x:-109.7},0).wait(1).to({rotation:-1.5,x:-122.7},0).wait(1).to({rotation:-2.1,x:-139.5,y:-98.8},0).wait(1).to({rotation:-2.8,x:-161.3},0).wait(1).to({rotation:-3.8,x:-190,y:-98.7},0).wait(1).to({rotation:-5.1,x:-229.3,y:-98.6},0).wait(1).to({rotation:-7,x:-285.4,y:-98.4},0).wait(1).to({rotation:-9.8,x:-370.9,y:-98.1},0).wait(1).to({rotation:-13.9,x:-491.2,y:-97.8},0).wait(1).to({rotation:-17.5,x:-598.7,y:-97.4},0).wait(1).to({rotation:-19.9,x:-670.4,y:-97.2},0).wait(1).to({rotation:-21.5,x:-719.4,y:-97.1},0).wait(1).to({rotation:-22.7,x:-755,y:-96.9},0).wait(1).to({rotation:-23.6,x:-781.9},0).wait(1).to({rotation:-24.3,x:-802.8,y:-96.8},0).wait(1).to({rotation:-24.9,x:-819.4,y:-96.7},0).wait(1).to({rotation:-25.3,x:-832.4},0).wait(1).to({rotation:-25.6,x:-842.9,y:-96.6},0).wait(1).to({rotation:-25.9,x:-851.1},0).wait(1).to({rotation:-26.1,x:-857.5},0).wait(1).to({rotation:-26.3,x:-862.4},0).wait(1).to({rotation:-26.4,x:-865.9},0).wait(1).to({regX:24.7,regY:256.6,rotation:-26.5,x:-862.5,y:-95.6},0).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


// stage content:
(lib.FS_projet_trace = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":150});

	// timeline functions:
	this.frame_100 = function() {
		//var frequency = 60;
		//stage.enableMouseOver(frequency);
		
		// EVENT DE CLIC POUR LA SUITE
		this.next_btn.addEventListener("click", fl_ClickToGoToAndPlayFromFrame.bind(this));
		this.next_btn.addEventListener("mouseover", fl_MouseOverHandler.bind(this));
		this.next_btn.addEventListener("mouseout", fl_MouseOutHandler.bind(this));
		
		
		function fl_ClickToGoToAndPlayFromFrame()
		{
			//this.gotoAndPlay("close");
			
			var event = new Event('next');
			this.dispatchEvent(event);		
			event = null;
			
			this.next_btn.btnNext.gotoAndPlay('rollOut');
			
		}
		function fl_MouseOverHandler(){	this.next_btn.btnNext.gotoAndPlay('rollOver'); }
		function fl_MouseOutHandler(){	this.next_btn.btnNext.gotoAndPlay('rollOut'); }
		
		
		// EVENT DE CLIC POUR LE LIEN
		this.cta_mc.addEventListener("click", projectDetails.bind(this));
		this.cta_mc.addEventListener("mouseover", pDetails_MouseOverHandler.bind(this));
		this.cta_mc.addEventListener("mouseout", pDetails_MouseOutHandler.bind(this));
		
		function projectDetails()
		{	
			var event = new Event('details');
			this.dispatchEvent(event);		
			event = null;
			
			this.cta_mc.btnCta.gotoAndPlay('rollOut');
			
		}
		function pDetails_MouseOverHandler(){	this.cta_mc.btnCta.gotoAndPlay('rollOver'); }
		function pDetails_MouseOutHandler(){	this.cta_mc.btnCta.gotoAndPlay('rollOut'); }
	}
	this.frame_146 = function() {
		//PAUSE
		this.stop();
	}
	this.frame_150 = function() {
		this.textes_mc.gotoAndPlay("close");
		this.visuel_mc.gotoAndPlay("close");
		this.cta_mc.gotoAndPlay("close");
		this.video_mc.gotoAndPlay("close");
		this.next_btn.gotoAndPlay("close");
		this.lignes_mc.gotoAndPlay("close_lines");
	}
	this.frame_169 = function() {
		//this.lignes_mc.gotoAndPlay("close");
	}
	this.frame_211 = function() {
		// EVENT DE FIN D'ANIM
		var event = new Event('closed');
		this.dispatchEvent(event);		
		event = null;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(100).call(this.frame_100).wait(46).call(this.frame_146).wait(4).call(this.frame_150).wait(19).call(this.frame_169).wait(42).call(this.frame_211).wait(1));

	// BLOC INFOS TEXTE
	this.textes_mc = new lib.blocInfostextes();
	this.textes_mc.name = "textes_mc";
	this.textes_mc.parent = this;
	this.textes_mc.setTransform(858.2,182,1,1,0,0,0,222.1,-164);
	this.textes_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.textes_mc).wait(17).to({_off:false},0).wait(195));

	// masque VISUEL
	this.visuel_mc = new lib.bloc_visuel();
	this.visuel_mc.name = "visuel_mc";
	this.visuel_mc.parent = this;
	this.visuel_mc.setTransform(1192.6,391.5,1,1,0,0,0,552.5,26.5);
	this.visuel_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.visuel_mc).wait(17).to({_off:false},0).wait(195));

	// CTA NEXT
	this.next_btn = new lib.bloc_NEXT_btn();
	this.next_btn.name = "next_btn";
	this.next_btn.parent = this;
	this.next_btn.setTransform(753.2,786.9,1,1,0,0,0,-340.5,403.9);
	this.next_btn._off = true;

	this.timeline.addTween(cjs.Tween.get(this.next_btn).wait(89).to({_off:false},0).wait(123));

	// CTA DETAIL
	this.cta_mc = new lib.bloc_CTA();
	this.cta_mc.name = "cta_mc";
	this.cta_mc.parent = this;
	this.cta_mc.setTransform(299.5,787,1,1,0,0,0,-340.5,403.9);
	this.cta_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.cta_mc).wait(99).to({_off:false},0).to({_off:true},74).wait(39));

	// VIDEO
	this.video_mc = new lib.bloc_video();
	this.video_mc.name = "video_mc";
	this.video_mc.parent = this;
	this.video_mc.setTransform(866.7,1147.2,1,1,0,0,0,227.5,764.9);
	this.video_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.video_mc).wait(94).to({_off:false},0).to({_off:true},79).wait(39));

	// lignes
	this.lignes_mc = new lib.lignes_all();
	this.lignes_mc.name = "lignes_mc";
	this.lignes_mc.parent = this;
	this.lignes_mc.setTransform(640,384.3,1,1,0,0,0,0,406.3);

	this.timeline.addTween(cjs.Tween.get(this.lignes_mc).wait(212));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(825.5,361.5,909.1,813.5);
// library properties:
lib.properties = {
	id: '7F2B4310C78733479958EA9833D7DCF7',
	width: 1280,
	height: 768,
	fps: 60,
	color: "#000000",
	opacity: 1.00,
	manifest: [
		{src:"images/hublet_TRACE_contour.png?1540809770220", id:"hublet_TRACE_contour"},
		{src:"images/hublet_TRACE_ile.jpg?1540809770220", id:"hublet_TRACE_ile"},
		{src:"images/nuage_TRACE.png?1540809770220", id:"nuage_TRACE"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['7F2B4310C78733479958EA9833D7DCF7'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;