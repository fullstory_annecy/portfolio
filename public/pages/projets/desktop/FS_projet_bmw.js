(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.bmw = function() {
	this.initialize(img.bmw);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,701,381);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.video = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.008)").s().p("EgtTBLAMAAAiV/MBanAAAMAAACV/g");
	this.shape.setTransform(290,479.975);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.video, new cjs.Rectangle(0,0,580,960), null);


(lib.txt_hashtag = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgeAkQgPgOAAgWQAAgVAPgOQAPgPAUAAQAWAAAPAOIgLARQgHgGgFgCQgGgCgHAAQgLgBgIAJQgJAJAAAMQAAAOAIAJQAIAIAKAAQAMAAAHgFIAAgaIAXAAIAAAjQgPAQgaAAQgVAAgOgPg");
	this.shape.setTransform(19.8,52.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAXAxIgtg9IAAA9IgWAAIAAhhIAUAAIAvA+IAAg+IAWAAIAABhg");
	this.shape_1.setTransform(8.875,52.75);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgKAxIAAhhIAVAAIAABhg");
	this.shape_2.setTransform(0.85,52.75);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgmAxIAAhhIAiAAQAWAAALAJQAKAIAAATQAAARgLAIQgLAJgUgBIgNAAIAAAcgAgQADIAOAAQALAAAEgEQADgEAAgJQABgIgGgDQgFgEgKAAIgMAAg");
	this.shape_3.setTransform(-5.85,52.75);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgmAxIAAhhIAiAAQAWAAAKAJQALAIAAATQAAARgLAIQgKAJgVgBIgNAAIAAAcgAgQADIAPAAQAJAAAEgEQAFgEAAgJQgBgIgFgDQgFgEgKAAIgMAAg");
	this.shape_4.setTransform(-15.35,52.75);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AAeAxIgKgVIgoAAIgJAVIgXAAIAqhhIAVAAIAqBhgAgLAJIAXAAIgMgcg");
	this.shape_5.setTransform(-25.675,52.75);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAhAxIAAg9IgbA1IgMAAIgag1IAAA9IgWAAIAAhhIAeAAIAYA1IAZg1IAeAAIAABhg");
	this.shape_6.setTransform(-37.575,52.75);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgTIANAAIADgRIgPAAIADgUIAPAAIAEgWIAUAAIgDAWIAMAAIADgWIAWAAIgEAWIAOAAIgDAUIgPAAIgDARIARAAIgEATIgQAAIgDAVgAgHAKIALAAIAEgRIgMAAg");
	this.shape_7.setTransform(-49.475,52.65);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AAXAxIgtg9IAAA9IgWAAIAAhhIAUAAIAvA+IAAg+IAWAAIAABhg");
	this.shape_8.setTransform(70.125,31.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgeAkQgPgOAAgWQABgUAPgPQAOgOAVAAQAUAAAQANIgLARQgHgGgGgDQgFgBgGAAQgMAAgIAIQgJAJAAAMQAAAOAIAJQAJAIAKAAQALAAAIgFIAAgaIAVAAIAAAiQgOAQgZABQgWgBgOgOg");
	this.shape_9.setTransform(59.1,31.65);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgKAxIAAhhIAVAAIAABhg");
	this.shape_10.setTransform(51.6,31.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgmAiIANgRQAQAPANAAQAGAAADgEQADgCAAgEQAAgFgDgCQgEgDgKgDQgRgEgIgFQgIgHAAgNQAAgPALgHQAKgIANABQAKAAAKADQAKADAHAGIgLARQgNgKgNAAQgFAAgDACQgDADAAAEQAAAFAEACQAEACANAEQAOADAHAGQAIAHAAANQAAANgKAJQgKAHgQABQgWgBgTgQg");
	this.shape_11.setTransform(44.725,31.65);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBFAAIAAATIgvAAIAAAVIArAAIAAASIgrAAIAAATIAxAAIAAAUg");
	this.shape_12.setTransform(36,31.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgrAxIAAhhIAiAAQAaAAAOANQANAMAAAXQAAAWgNAOQgOANgbAAgAgVAdIANAAQAPAAAHgHQAJgIAAgOQAAgOgJgHQgHgIgRAAIgLAAg");
	this.shape_13.setTransform(26.1,31.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AAXAxIgtg9IAAA9IgWAAIAAhhIAUAAIAvA+IAAg+IAWAAIAABhg");
	this.shape_14.setTransform(11.025,31.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgjAkQgPgPAAgVQAAgVAPgOQAOgOAVAAQAWAAAPAOQAOAOAAAVQAAAVgOAPQgPAPgWAAQgVAAgOgPgAgTgVQgJAJAAAMQAAANAJAKQAIAIALABQAMgBAJgIQAIgKAAgNQAAgMgIgJQgJgJgMAAQgLAAgIAJg");
	this.shape_15.setTransform(-0.475,31.65);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgKAxIAAhhIAVAAIAABhg");
	this.shape_16.setTransform(-8.55,31.7);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgKAxIAAhOIgcAAIAAgTIBMAAIAAATIgbAAIAABOg");
	this.shape_17.setTransform(-15.1,31.7);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgjAkQgPgPAAgVQAAgVAPgOQAOgOAVAAQAWAAAPAOQAOAOAAAVQAAAVgOAPQgPAPgWAAQgVAAgOgPgAgTgVQgJAJAAAMQAAANAJAKQAIAIALABQAMgBAJgIQAIgKAAgNQAAgMgIgJQgJgJgMAAQgLAAgIAJg");
	this.shape_18.setTransform(-25.125,31.65);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AAhAxIAAg9IgbA1IgMAAIgag1IAAA9IgWAAIAAhhIAeAAIAYA1IAZg1IAeAAIAABhg");
	this.shape_19.setTransform(-37.575,31.7);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgTIANAAIADgRIgPAAIADgTIAPAAIAEgXIAUAAIgDAXIAMAAIADgXIAWAAIgEAXIAOAAIgDATIgPAAIgDARIARAAIgEATIgQAAIgDAVgAgHAKIALAAIAEgRIgMAAg");
	this.shape_20.setTransform(-49.475,31.6);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AAXAxIgtg9IAAA9IgWAAIAAhhIAUAAIAvA+IAAg+IAWAAIAABhg");
	this.shape_21.setTransform(70.575,10.65);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgjAkQgPgPAAgVQAAgVAPgOQAOgOAVAAQAWAAAPAOQAOAOAAAVQAAAVgOAPQgPAPgWAAQgVAAgOgPgAgTgVQgJAJAAAMQAAANAJAKQAIAIALABQAMgBAJgIQAIgKAAgNQAAgMgIgJQgJgKgMABQgLgBgIAKg");
	this.shape_22.setTransform(59.075,10.6);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgKAxIAAhhIAVAAIAABhg");
	this.shape_23.setTransform(51,10.65);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgKAxIAAhOIgcAAIAAgTIBNAAIAAATIgcAAIAABOg");
	this.shape_24.setTransform(44.45,10.65);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AAeAxIgKgVIgoAAIgJAVIgXAAIAqhhIAVAAIAqBhgAgLAJIAXAAIgMgcg");
	this.shape_25.setTransform(34.975,10.65);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgmAhIANgQQAQAOANABQAGAAADgEQADgCAAgEQAAgFgDgCQgEgDgKgDQgRgEgIgFQgIgHAAgNQAAgOALgIQAKgIANABQAKgBAKAEQAKADAHAGIgLARQgNgLgNABQgFgBgDADQgDADAAAEQAAAFAEACQAEADANADQAOADAHAGQAIAHAAANQAAANgKAIQgKAJgQAAQgWgBgTgRg");
	this.shape_26.setTransform(25.175,10.6);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgKAxIAAhhIAVAAIAABhg");
	this.shape_27.setTransform(18.5,10.65);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AAQAxIgVgfIgPAAIAAAfIgWAAIAAhhIAlAAQAWAAAKAIQAKAHAAARQAAAXgTAHIAZAjgAgUAAIAQAAQAKAAAEgEQAEgDAAgJQAAgHgEgDQgEgDgKAAIgQAAg");
	this.shape_28.setTransform(11.475,10.65);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AAeAxIgKgVIgoAAIgJAVIgXAAIAqhhIAVAAIAqBhgAgLAJIAXAAIgMgcg");
	this.shape_29.setTransform(0.775,10.65);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AAXAxIgtg9IAAA9IgWAAIAAhhIAUAAIAvA+IAAg+IAWAAIAABhg");
	this.shape_30.setTransform(-10.175,10.65);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBFAAIAAAUIgvAAIAAAUIArAAIAAASIgrAAIAAATIAxAAIAAAUg");
	this.shape_31.setTransform(-20.25,10.65);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AgeAkQgPgOAAgWQAAgUAPgPQAPgOAVAAQAXgBARATIgNAPQgLgMgPAAQgMgBgJAJQgIAHgBANQAAAOAJAIQAJAIAKAAQAQAAALgNIANAPQgQATgXAAQgVgBgPgOg");
	this.shape_32.setTransform(-30.1,10.6);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AgmAhIANgQQAQAOANABQAGAAADgEQADgCAAgEQAAgFgDgCQgEgDgKgDQgRgEgIgFQgIgHAAgNQAAgOALgIQAKgIANABQAKgBAKAEQAKADAHAGIgLARQgNgLgNABQgFgBgDADQgDADAAAEQAAAFAEACQAEADANADQAOADAHAGQAIAHAAANQAAANgKAIQgKAJgQAAQgWgBgTgRg");
	this.shape_33.setTransform(-39.875,10.6);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgTIANAAIADgRIgPAAIADgTIAPAAIAEgXIAUAAIgDAXIAMAAIADgXIAWAAIgEAXIAOAAIgDATIgPAAIgDARIARAAIgEATIgQAAIgDAVgAgHAKIALAAIAEgRIgMAAg");
	this.shape_34.setTransform(-49.475,10.55);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.txt_hashtag, new cjs.Rectangle(-56.7,0,135.2,63.2), null);


(lib.masqueTexte_1_ligne = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("EgicAEJIAAoRMBE5AAAIAAIRg");
	this.shape.setTransform(0,24.55);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.masqueTexte_1_ligne, new cjs.Rectangle(-220.5,-1.9,441,53), null);


(lib.masque_generique = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#01FBFC").s().p("AsQCqIAAlTIYhAAIAAFTg");
	this.shape.setTransform(78.5,17);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.masque_generique, new cjs.Rectangle(0,0,157,34), null);


(lib.line = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#0FAFF1").ss(1,1,1).p("AAAgHIAAAP");
	this.shape.setTransform(0,62.75);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#0FAFF1").ss(1,1,1).p("AAAAHIAAgN");
	this.shape_1.setTransform(0,62.675);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#0FAFF1").ss(1,1,1).p("AAAAEIAAgI");
	this.shape_2.setTransform(0,62.45);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#0FB0F1").ss(1,1,1).p("AAAgEIAAAJ");
	this.shape_3.setTransform(0,61.575);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#0FB1F1").ss(1,1,1).p("AAAgKIAAAV");
	this.shape_4.setTransform(0,60.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#0EB2F1").ss(1,1,1).p("AAAgTIAAAn");
	this.shape_5.setTransform(0,60.05);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#0EB3F2").ss(1,1,1).p("AAAgdIAAA7");
	this.shape_6.setTransform(0,59.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#0EB5F2").ss(1,1,1).p("AAAgoIAABR");
	this.shape_7.setTransform(0,57.975);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#0EB6F2").ss(1,1,1).p("AAAg2IAABs");
	this.shape_8.setTransform(0,56.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#0DB8F2").ss(1,1,1).p("AAAhEIAACJ");
	this.shape_9.setTransform(0,55.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#0DBAF3").ss(1,1,1).p("AAAhUIAACp");
	this.shape_10.setTransform(0,53.725);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#0DBCF3").ss(1,1,1).p("AAAhmIAADN");
	this.shape_11.setTransform(0,52);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#0CBEF3").ss(1,1,1).p("AAAh4IAADx");
	this.shape_12.setTransform(0,50.15);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#0CC1F4").ss(1,1,1).p("AAAiNIAAEb");
	this.shape_13.setTransform(0,48.125);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#0BC3F4").ss(1,1,1).p("AAAikIAAFI");
	this.shape_14.setTransform(0,45.95);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#0BC6F4").ss(1,1,1).p("AAAi7IAAF3");
	this.shape_15.setTransform(0,43.65);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#0AC9F5").ss(1,1,1).p("AAAjUIAAGp");
	this.shape_16.setTransform(0,41.175);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#0ACCF5").ss(1,1,1).p("AAAjvIAAHf");
	this.shape_17.setTransform(0,38.575);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#09D0F6").ss(1,1,1).p("AAAkLIAAIX");
	this.shape_18.setTransform(0,35.825);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#08D3F6").ss(1,1,1).p("AAAkpIAAJT");
	this.shape_19.setTransform(0,32.9);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#08D7F7").ss(1,1,1).p("AAAlIIAAKS");
	this.shape_20.setTransform(0,29.85);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#07DBF7").ss(1,1,1).p("AAAlpIAALT");
	this.shape_21.setTransform(0,26.625);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#06DFF8").ss(1,1,1).p("AAAmMIAAMZ");
	this.shape_22.setTransform(0,23.275);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#05E3F9").ss(1,1,1).p("AAAmwIAANh");
	this.shape_23.setTransform(0,19.775);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#05E7F9").ss(1,1,1).p("AAAnVIAAOr");
	this.shape_24.setTransform(0,16.1);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#04ECFA").ss(1,1,1).p("AAAn8IAAP5");
	this.shape_25.setTransform(0,12.3);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#03F1FB").ss(1,1,1).p("AAAolIAARL");
	this.shape_26.setTransform(0,8.35);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#02F6FB").ss(1,1,1).p("AAApPIAASf");
	this.shape_27.setTransform(0,4.25);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#01FBFC").ss(1,1,1).p("AAAJ7IAAz1");

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#01FBFC").ss(1,1,1).p("AAApQIAASh");
	this.shape_29.setTransform(0,-4.1);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#01FBFC").ss(1,1,1).p("AAAonIAARP");
	this.shape_30.setTransform(0,-8.05);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#01FBFC").ss(1,1,1).p("AAAn/IAAP/");
	this.shape_31.setTransform(0,-11.875);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#01FBFC").ss(1,1,1).p("AAAnaIAAO1");
	this.shape_32.setTransform(0,-15.55);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#01FBFC").ss(1,1,1).p("AAAm1IAANr");
	this.shape_33.setTransform(0,-19.1);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#01FBFC").ss(1,1,1).p("AAAmSIAAMl");
	this.shape_34.setTransform(0,-22.5);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#01FBFC").ss(1,1,1).p("AAAlwIAALh");
	this.shape_35.setTransform(0,-25.775);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#01FBFC").ss(1,1,1).p("AAAlQIAAKh");
	this.shape_36.setTransform(0,-28.9);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#01FBFC").ss(1,1,1).p("AAAkxIAAJj");
	this.shape_37.setTransform(0,-31.875);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#01FBFC").ss(1,1,1).p("AAAkUIAAIp");
	this.shape_38.setTransform(0,-34.725);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#01FBFC").ss(1,1,1).p("AAAj4IAAHx");
	this.shape_39.setTransform(0,-37.425);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#01FBFC").ss(1,1,1).p("AAAjdIAAG7");
	this.shape_40.setTransform(0,-40);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#01FBFC").ss(1,1,1).p("AAAjEIAAGJ");
	this.shape_41.setTransform(0,-42.425);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#01FBFC").ss(1,1,1).p("AAAisIAAFa");
	this.shape_42.setTransform(0,-44.7);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#01FBFC").ss(1,1,1).p("AAAiWIAAEt");
	this.shape_43.setTransform(0,-46.875);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#01FBFC").ss(1,1,1).p("AAAiBIAAEE");
	this.shape_44.setTransform(0,-48.9);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#01FBFC").ss(1,1,1).p("AAAhuIAADd");
	this.shape_45.setTransform(0,-50.775);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().s("#01FBFC").ss(1,1,1).p("AAAhcIAAC5");
	this.shape_46.setTransform(0,-52.5);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().s("#01FBFC").ss(1,1,1).p("AAAhLIAACX");
	this.shape_47.setTransform(0,-54.1);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f().s("#01FBFC").ss(1,1,1).p("AAAg9IAAB6");
	this.shape_48.setTransform(0,-55.55);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f().s("#01FBFC").ss(1,1,1).p("AAAgvIAABf");
	this.shape_49.setTransform(0,-56.875);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f().s("#01FBFC").ss(1,1,1).p("AAAgjIAABH");
	this.shape_50.setTransform(0,-58.05);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f().s("#01FBFC").ss(1,1,1).p("AAAgYIAAAx");
	this.shape_51.setTransform(0,-59.1);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f().s("#01FBFC").ss(1,1,1).p("AAAgPIAAAf");
	this.shape_52.setTransform(0,-60);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f().s("#01FBFC").ss(1,1,1).p("AAAgHIAAAP");
	this.shape_53.setTransform(0,-60.75);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f().s("#01FBFC").ss(1,1,1).p("AAAAAIAAAB");
	this.shape_54.setTransform(0,-61.4);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f().s("#01FBFC").ss(1,1,1).p("AAAAEIAAgH");
	this.shape_55.setTransform(0,-61.875);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f().s("#01FBFC").ss(1,1,1).p("AAAAHIAAgN");
	this.shape_56.setTransform(0,-62.225);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f().s("#01FBFC").ss(1,1,1).p("AAAAJIAAgR");
	this.shape_57.setTransform(0,-62.425);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f().s("#01FBFC").ss(1,1,1).p("AAAgJIAAAT");
	this.shape_58.setTransform(0,-62.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_51}]},1).to({state:[{t:this.shape_52}]},1).to({state:[{t:this.shape_53}]},1).to({state:[{t:this.shape_54}]},1).to({state:[{t:this.shape_55}]},1).to({state:[{t:this.shape_56}]},1).to({state:[{t:this.shape_57}]},1).to({state:[{t:this.shape_58}]},1).to({state:[]},1).wait(61));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1,-64.5,2,129);


(lib.ligne = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(49));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#333333").ss(1,1,1).p("EAAAg/bMAAAB+3");
	this.shape.setTransform(0,406);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#333333").ss(1,1,1).p("EAA/A/cQhDv0ggwoQgdwxAExFQAEwXAhwHQAfufA4to");
	this.shape_1.setTransform(-6.0239,406);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#333333").ss(1,1,1).p("EABwA/cQh4vfg5w8Qg0wqAHxQQAHweA7wDQA5uxBjtQ");
	this.shape_2.setTransform(-10.6719,406);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#333333").ss(1,1,1).p("EACVA/cQigvPhLxLQhHwmAKxYQAJwjBPv/QBMvACEs9");
	this.shape_3.setTransform(-14.2371,406);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#333333").ss(1,1,1).p("EACwA/cQi+vDhZxWQhTwjAMxeQAKwnBev8QBavKCcsw");
	this.shape_4.setTransform(-16.895,406);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#333333").ss(1,1,1).p("EADEA/cQjTu7hjxdQhdwhANxhQAMwrBov6QBkvSCusm");
	this.shape_5.setTransform(-18.7801,406);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#333333").ss(1,1,1).p("EADSA/cQjiu1hqxjQhjwfAOxlQAMwsBvv5QBsvXC6sf");
	this.shape_6.setTransform(-20.0814,406);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#333333").ss(1,1,1).p("EADbA/cQjsuxhuxnQhnweAOxnQANwtB0v4QBwvaDCsb");
	this.shape_7.setTransform(-20.9145,406);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#333333").ss(1,1,1).p("EADgA/cQjyuuhxxpQhpwdAPxpQANwuB3v3QByvdDHsY");
	this.shape_8.setTransform(-21.4184,406);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#333333").ss(1,1,1).p("EADjA/cQj1uthyxqQhrwdAPxpQANwvB5v3QB0vdDJsX");
	this.shape_9.setTransform(-21.7172,406);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#333333").ss(1,1,1).p("EADkA/cQj2uthzxqQhswdAPxpQANwvB6v3QB1veDKsW");
	this.shape_10.setTransform(-21.8418,406);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#333333").ss(1,1,1).p("EADlA/cQj3ush0xrQhrwdAPxpQANwvB5v3QB2veDLsW");
	this.shape_11.setTransform(-21.8918,406);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#333333").ss(1,1,1).p("EADlA/cQj3ushzxrQhswdAPxpQANwvB6v3QB1veDLsW");
	this.shape_12.setTransform(-21.9165,406);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#333333").ss(1,1,1).p("EADlg/bQjLMWh1PeQh6P3gNQvQgPRpBsQdQBzRrD3Os");
	this.shape_13.setTransform(-21.9165,406);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#333333").ss(1,1,1).p("EADlA/cQj3ushzxrQhswdAPxpQANwvB5v3QB2veDLsW");
	this.shape_14.setTransform(-21.8915,406);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#333333").ss(1,1,1).p("EADlA/cQj3ushzxrQhswdAPxpQAOwvB5v3QB1veDLsW");
	this.shape_15.setTransform(-21.8665,406);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#333333").ss(1,1,1).p("EADkA/cQj2uthzxqQhrwdAPxpQANwvB5v3QB1veDKsW");
	this.shape_16.setTransform(-21.7865,406);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#333333").ss(1,1,1).p("EADjA/cQj0uthzxqQhqwdAOxpQAOwvB4v2QB0veDJsX");
	this.shape_17.setTransform(-21.6619,406);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#333333").ss(1,1,1).p("EADhA/cQjyuuhyxpQhpwdAOxpQANwuB4v3QBzvdDHsY");
	this.shape_18.setTransform(-21.4626,406);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#333333").ss(1,1,1).p("EADeA/cQjvuvhwxoQhoweAOxoQANwuB2v3QBxvcDFsZ");
	this.shape_19.setTransform(-21.2133,406);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#333333").ss(1,1,1).p("EADaA/cQjruxhuxnQhnweAPxmQAMwuB0v4QBwvaDBsb");
	this.shape_20.setTransform(-20.8645,406);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#333333").ss(1,1,1).p("EADWA/cQjnuzhrxlQhlweAOxmQANwsBxv5QBtvZC+sd");
	this.shape_21.setTransform(-20.4106,406);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#333333").ss(1,1,1).p("EADQA/cQjgu2hpxiQhiwfAOxlQAMwsBuv4QBrvXC4sg");
	this.shape_22.setTransform(-19.8817,406);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#333333").ss(1,1,1).p("EADJA/cQjZu4hlxgQhewgANxjQAMwrBqv6QBnvTCysk");
	this.shape_23.setTransform(-19.1786,406);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#333333").ss(1,1,1).p("EADAA/cQjPu8hhxcQhawhAMxhQAMwqBlv7QBjvQCqso");
	this.shape_24.setTransform(-18.3758,406);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#333333").ss(1,1,1).p("EAC2A/cQjEvAhcxZQhWwiAMxfQALwoBhv8QBdvMChst");
	this.shape_25.setTransform(-17.4235,406);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#333333").ss(1,1,1).p("EACqA/cQi3vFhWxUQhRwjAMxdQAKwnBav8QBXvICXsz");
	this.shape_26.setTransform(-16.2969,406);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#333333").ss(1,1,1).p("EACdA/cQipvLhPxPQhKwkAKxaQAKwlBTv+QBQvDCLs5");
	this.shape_27.setTransform(-15.0095,406);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#333333").ss(1,1,1).p("EACOA/cQiYvShIxIQhCwnAJxWQAIwiBLwAQBJu9B9tB");
	this.shape_28.setTransform(-13.5536,406);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#333333").ss(1,1,1).p("EAB8A/cQiFvZg/xCQg6woAIxSQAHwhBCwBQA/u2ButK");
	this.shape_29.setTransform(-11.8731,406);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#333333").ss(1,1,1).p("EABpA/cQhxvig1w5QgxwrAHxPQAGwdA3wDQA2uvBdtT");
	this.shape_30.setTransform(-10.0434,406);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#333333").ss(1,1,1).p("EABYA/cQhevpgswyQgqwuAGxKQAFwbAvwFQAsuoBOtc");
	this.shape_31.setTransform(-8.3629,406);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#333333").ss(1,1,1).p("EABIA/cQhNvwgkwsQgiwvAFxHQAEwYAmwHQAkujBAtj");
	this.shape_32.setTransform(-6.9071,406);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#333333").ss(1,1,1).p("EAA7A/cQg/v2gdwmQgcwxAExEQADwXAgwIQAdudA0tq");
	this.shape_33.setTransform(-5.6198,406);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#333333").ss(1,1,1).p("EAAvA/cQgyv7gYwhQgWwzADxBQADwVAZwJQAXuZAqtw");
	this.shape_34.setTransform(-4.493,406);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#333333").ss(1,1,1).p("EAAlA/cQgnv/gTweQgRwzACxAQACwTAUwKQASuVAht1");
	this.shape_35.setTransform(-3.5406,406);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#333333").ss(1,1,1).p("EAAcA/cQgdwDgPwaQgNw1ACw9QABwSAQwLQANuSAZt5");
	this.shape_36.setTransform(-2.7379,406);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#333333").ss(1,1,1).p("EAAVA/cQgWwGgLwXQgKw2ACw7QABwSALwMQAKuPATt8");
	this.shape_37.setTransform(-2.0348,406);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#333333").ss(1,1,1).p("EAAQA/cQgQwIgIwVQgHw3ABw6QABwRAIwMQAHuNAOt/");
	this.shape_38.setTransform(-1.5059,406);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#333333").ss(1,1,1).p("EAALA/cQgLwLgFwSQgFw3AAw6QABwQAGwMQAFuMAJuB");
	this.shape_39.setTransform(-1.0523,406);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#333333").ss(1,1,1).p("EAAIA/cQgIwMgDwSQgEw3ABw5QAAwPAEwNQADuKAHuD");
	this.shape_40.setTransform(-0.7031,406);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#333333").ss(1,1,1).p("EAAFA/cQgFwNgBwRQgDw3ABw5QAAwOACwOQACuJAEuE");
	this.shape_41.setTransform(-0.4542,406);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#333333").ss(1,1,1).p("EAADA/cQgDwOAAwQQgCw3ABw4QAAwOABwPQAAuIADuF");
	this.shape_42.setTransform(-0.2562,406);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#333333").ss(1,1,1).p("EAABA/cQgBwOAAwQQgBw4ABw3QAAwOAAwOQAAuIABuG");
	this.shape_43.setTransform(-0.1333,406);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#333333").ss(1,1,1).p("EAAAA/cQAAwPAAwPQAAw4AAw3QAAwOAAwOQAAuIAAuG");
	this.shape_44.setTransform(-0.05,406);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#333333").ss(1,1,1).p("EAAAA/cQAAwPAAwPQAAw4AAw3QAAwOAAwOQAAuHAAuH");
	this.shape_45.setTransform(-0.025,406);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45,p:{x:-0.025}}]},1).to({state:[{t:this.shape_45,p:{x:0}}]},1).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-45.8,-1,47.8,814);


(lib.lettres_FAT = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#01FBFC").s().p("AkWFeIAAq6IEQAAQBGAAAzARQA0ARAaAdQAvA1AABEQAABTg1AoQgSANgHAEIgZALQBBANAmAtQAnAtAABBQAABJgyA4Qg6BCiQAAgAh6DaIBrAAQBCAAAigRQAhgRAAgwQAAgwgjgQQgkgQhNAAIhcAAgAh6hCIBKAAQBAAAAggNQAfgOAAgvQAAgvgdgOQgdgPhEAAIhLAAg");
	this.shape.setTransform(50.75,311.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#01FBFC").s().p("ACZFeIiZnlIiYHlIh0AAIj0q7ICqAAICJGSIB9mSIChAAIB8GSICKmSICqAAIj0K7g");
	this.shape_1.setTransform(-50,187.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.lettres_FAT, new cjs.Rectangle(-102.9,0,200.3,373.7), null);


(lib.hastaggraphicbranche = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AjQAuIAShbIDlAAICqAAIgSBbg");
	this.shape.setTransform(0,4.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.hastaggraphicbranche, new cjs.Rectangle(-20.8,0,41.7,9.2), null);


(lib.circle3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#01FBFC").ss(2,1,1).p("EAzZAAAQAADYvDCYQvDCZ1SAAQ1SAAvEiZQvDiYAAjYQAAjXPDiYQPEiZVSAAQVSAAPDCZQPDCYAADXg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.circle3, new cjs.Rectangle(-329.9,-53,659.8,106.1), null);


(lib.circle2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#33CCFF").ss(2,1,1).p("EAqyAAAQAACgsiBxQsiBxxuAAQxtAAsjhxQshhxAAigQAAifMhhxQMjhxRtAAQRuAAMiBxQMiBxAACfg");
	this.shape.setTransform(-0.025,-0.025);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.circle2, new cjs.Rectangle(-274.8,-39.6,549.6,79.2), null);


(lib.circle1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#3399FF").ss(2,1,1).p("EAjyAAAQAAByqfBRQqeBQu1AAQu0AAqfhQQqehRAAhyQAAhxKehRQKfhQO0AAQO1AAKeBQQKfBRAABxg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.circle1, new cjs.Rectangle(-230,-28.5,460,57), null);


(lib.Btn_cliquable_area = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#00FFFF").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape.setTransform(0,30);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Btn_cliquable_area, new cjs.Rectangle(-113,0,226,60), null);


(lib.bmw_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.instance = new lib.bmw();
	this.instance.parent = this;
	this.instance.setTransform(-350.5,-190.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.bmw_1, new cjs.Rectangle(-350.5,-190.5,701,381), null);


(lib.masqueTexte = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// masqueTexte_1_ligne
	this.instance = new lib.masqueTexte_1_ligne();
	this.instance.parent = this;
	this.instance.setTransform(-218.8,191.5,0.0079,1,0,0,0,0,25.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(6).to({_off:false},0).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.0079,x:218.8},14).wait(1));

	// masqueTexte_1_ligne
	this.instance_1 = new lib.masqueTexte_1_ligne();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-218.8,136.1,0.0079,1,0,0,0,0,25.5);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.0079,x:218.8},14).to({_off:true},1).wait(2));

	// masqueTexte_1_ligne
	this.instance_2 = new lib.masqueTexte_1_ligne();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-218.8,80.8,0.0079,1,0,0,0,0,25.5);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(2).to({_off:false},0).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.0079,x:218.8},14).to({_off:true},1).wait(4));

	// masqueTexte_1_ligne
	this.instance_3 = new lib.masqueTexte_1_ligne();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-218.8,25.5,0.0079,1,0,0,0,0,25.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.0079,x:218.8},14).to({_off:true},1).wait(6));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-220.5,-1.9,441.1,219);


(lib.lignes_all = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{close:114,close_lines:157});

	// timeline functions:
	this.frame_113 = function() {
		this.stop();
	}
	this.frame_156 = function() {
		this.stop();
	}
	this.frame_174 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(113).call(this.frame_113).wait(43).call(this.frame_156).wait(18).call(this.frame_174).wait(1));

	// ligne
	this.instance = new lib.ligne();
	this.instance.parent = this;
	this.instance.setTransform(-454.05,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(114).to({x:-883},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:-454.05},0).wait(18));

	// ligne
	this.instance_1 = new lib.ligne();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-227.05,406.5,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(114).to({x:-883.2},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:-227.05},0).wait(18));

	// ligne
	this.instance_2 = new lib.ligne();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-0.05,406.5,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(114).to({x:-883.4},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:-0.05},0).wait(18));

	// ligne
	this.instance_3 = new lib.ligne();
	this.instance_3.parent = this;
	this.instance_3.setTransform(226.95,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(93).to({x:227,y:322},20,cjs.Ease.quartInOut).wait(1).to({y:323},0).to({x:-883.05,y:406},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:227,y:322},0).to({x:226.95,y:406},16,cjs.Ease.quartInOut).wait(2));

	// ligne
	this.instance_4 = new lib.ligne();
	this.instance_4.parent = this;
	this.instance_4.setTransform(454.1,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(114).to({x:-883.45},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:454.1},0).wait(18));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-884.4,-85,1339.5,898.5);


(lib.hashtag_graphic = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(64));

	// hastag graphic branche
	this.instance = new lib.hastaggraphicbranche();
	this.instance.parent = this;
	this.instance.setTransform(37.35,51.9,1.0844,0.9952,0,-78.3276,102.1252,0.1,4.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({x:6.45,y:50.05},17,cjs.Ease.quartOut).to({_off:true},5).wait(3).to({_off:false},0).to({_off:true},4).wait(2).to({_off:false,x:26.45},0).to({_off:true},5).wait(28));

	// hastag graphic branche
	this.instance_1 = new lib.hastaggraphicbranche();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-41.65,49.9,1.0844,0.9952,0,-78.3276,102.1252,0.1,4.6);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off:false},0).to({x:-7.4,y:49.8},18,cjs.Ease.quartOut).to({_off:true},4).wait(5).to({_off:false},0).to({_off:true},4).wait(31));

	// hastag graphic branche
	this.instance_2 = new lib.hastaggraphicbranche();
	this.instance_2.parent = this;
	this.instance_2.setTransform(2.7,21.7,1,1,0,0,0,0,4.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({x:-2.3,y:58.7},13,cjs.Ease.quartOut).to({_off:true},11).wait(3).to({_off:false,scaleX:2.3297,scaleY:1.0257,skewX:-12.8379,x:25.5},0).to({_off:true},4).wait(33));

	// hastag graphic branche
	this.instance_3 = new lib.hastaggraphicbranche();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-7.3,77.7,1,1,0,0,0,0,4.6);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({x:0.7,y:41.7},14,cjs.Ease.quartOut).to({_off:true},4).wait(7).to({_off:false,regX:0.1,scaleX:1.9808,scaleY:1.0147,skewX:-9.7728,x:19.9,y:49.7},0).to({_off:true},4).wait(4).to({_off:false,regX:0,scaleX:1,scaleY:1,skewX:0,x:0.7,y:41.7},0).to({_off:true},4).wait(21));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-50.9,0,126.1,82.3);


(lib.Btn_next = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{rollOver:55,rollOut:70});

	// timeline functions:
	this.frame_54 = function() {
		this.stop();
	}
	this.frame_69 = function() {
		this.stop();
	}
	this.frame_84 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(54).call(this.frame_54).wait(15).call(this.frame_69).wait(15).call(this.frame_84).wait(1));

	// ZONE
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.008)").s().p("EgjTAEsIAApXMBGnAAAIAAJXg");
	this.shape.setTransform(113,29.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(85));

	// txt
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgIAqIAAhDIgYAAIAAgQIBBAAIAAAQIgYAAIAABDg");
	this.shape_1.setTransform(161.05,29.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AASAqIgSgcIgQAcIgYAAIAdgqIgagpIAWAAIAPAZIAQgZIAWAAIgaApIAcAqg");
	this.shape_2.setTransform(145.2,29.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgeAqIAAhTIA8AAIAAARIgpAAIAAARIAkAAIAAAPIgkAAIAAASIAqAAIAAAQg");
	this.shape_3.setTransform(129.325,29.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAUAqIgngzIAAAzIgTAAIAAhTIASAAIAoA1IAAg1IATAAIAABTg");
	this.shape_4.setTransform(112.3,29.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgKAQIAEgOQgEgCAAgFQAAgEADgDQADgDAEAAQAFAAADADQADADAAAEQAAAEgDAEIgIANg");
	this.shape_5.setTransform(86.525,32.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAPAqIgYghIgIAKIAAAXIgTAAIAAhTIATAAIAAAjIAegjIAYAAIgiAlIAQAWIASAYg");
	this.shape_6.setTransform(76.975,29.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgeAfQgNgNAAgSQAAgSANgMQANgNARAAQASAAAOANQAMAMAAASQAAASgMANQgOAMgSAAQgRAAgNgMgAgRgSQgHAIAAAKQAAALAHAIQAIAIAJAAQAKAAAIgIQAHgIAAgLQAAgKgHgIQgIgIgKAAQgJAAgIAIg");
	this.shape_7.setTransform(59.3,29.05);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#333333").ss(1,1,1).p("EgjOAADMBGdgAF");
	this.shape_8.setTransform(113,-0.25);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(85));

	// Layer_2
	this.instance = new lib.Btn_cliquable_area();
	this.instance.parent = this;
	this.instance.setTransform(113,29.5,2,1,0,0,0,0,30);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("rgba(0,0,0,0.008)").s().p("EgjTAEsIAApXMBGnAAAIAAJXg");
	this.shape_9.setTransform(113,29.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance}]},3).to({state:[{t:this.instance}]},46).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.instance}]},5).to({state:[{t:this.instance}]},14).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},14).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({alpha:0.0117},46).to({_off:true},1).wait(5).to({_off:false},0).to({alpha:1},14).wait(1).to({alpha:0.0117},14).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-113.5,-1.5,453,61);


(lib.Btn_detail = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"rollOver":55,"rollOut":70});

	// timeline functions:
	this.frame_54 = function() {
		this.stop();
	}
	this.frame_69 = function() {
		this.stop();
	}
	this.frame_84 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(54).call(this.frame_54).wait(15).call(this.frame_69).wait(15).call(this.frame_84).wait(1));

	// ZONE
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.008)").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape.setTransform(0,29.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(85));

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgOAyIAAhkIAdAAIAABkg");
	this.shape_1.setTransform(-85.45,29.55);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgyAOIAAgcIBlAAIAAAcg");
	this.shape_2.setTransform(-85.45,29.55);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgaAqIAAhTIASAAIAABDIAjAAIAAAQg");
	this.shape_3.setTransform(38.425,29.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgIAqIAAhTIARAAIAABTg");
	this.shape_4.setTransform(24.675,29.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AAZAqIgHgSIgjAAIgHASIgUAAIAkhTIARAAIAkBTgAgKAIIAUAAIgKgYg");
	this.shape_5.setTransform(10.225,29.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgIAqIAAhDIgYAAIAAgQIBBAAIAAAQIgYAAIAABDg");
	this.shape_6.setTransform(-5.9,29.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgeAqIAAhTIA8AAIAAARIgpAAIAAARIAkAAIAAAPIgkAAIAAASIAqAAIAAAQg");
	this.shape_7.setTransform(-21.275,29.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AglAqIAAhTIAeAAQAVAAAMALQAMALAAATQAAATgMALQgMAMgXAAgAgTAaIANAAQAMAAAGgHQAHgGAAgNQAAgLgHgHQgGgGgOgBIgLAAg");
	this.shape_8.setTransform(-37.8,29.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#333333").ss(1,1,1).p("AxpAAMAjTAAA");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(85));

	// Layer_2
	this.instance = new lib.Btn_cliquable_area();
	this.instance.parent = this;
	this.instance.setTransform(0,29.5,1,1,0,0,0,0,30);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("rgba(0,0,0,0.008)").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape_10.setTransform(0,29.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance}]},3).to({state:[{t:this.instance}]},45).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.instance}]},6).to({state:[{t:this.instance}]},14).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},13).wait(2));
	this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({alpha:0.0117},45).to({_off:true},1).wait(6).to({_off:false},0).to({alpha:1},14).wait(1).to({alpha:0.0117},13).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-114,-1,228,60.5);


(lib.bloc_visuel = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":121});

	// timeline functions:
	this.frame_120 = function() {
		this.stop();
	}
	this.frame_169 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(120).call(this.frame_120).wait(49).call(this.frame_169).wait(1));

	// Calque_16
	this.instance = new lib.line();
	this.instance.parent = this;
	this.instance.setTransform(-54.5,89.95);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(82).to({_off:false},0).wait(88));

	// Calque_15
	this.instance_1 = new lib.line();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-189.5,24.95);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(32).to({_off:false},0).wait(138));

	// Calque_8
	this.instance_2 = new lib.line();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-374.5,-200.05);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(170));

	// masque VISUEL (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_1 = new cjs.Graphics().p("Egx6BDvMAAAiHdMBjwAAAMAAACHdg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(1).to({graphics:mask_graphics_1,x:-319.4601,y:-19}).wait(169));

	// Calque_2
	this.instance_3 = new lib.bmw_1();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-122.8,-131.8,0.586,0.586,0,1.9757,2.6698,-0.1,-0.3);
	this.instance_3.alpha = 0;
	this.instance_3._off = true;

	var maskedShapeInstanceList = [this.instance_3];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(1).to({_off:false},0).to({alpha:1},63).wait(69).to({alpha:0},35).wait(2));

	// Calque_5
	this.instance_4 = new lib.circle3();
	this.instance_4.parent = this;
	this.instance_4.setTransform(-109.15,-50.7,0.097,0.097);
	this.instance_4._off = true;

	var maskedShapeInstanceList = [this.instance_4];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(1).to({_off:false},0).to({scaleX:1,scaleY:0.7406,y:-30.7},26,cjs.Ease.quartOut).wait(94).to({scaleX:1.2981,scaleY:0.9614,alpha:0},23,cjs.Ease.cubicIn).wait(26));

	// Calque_4
	this.instance_5 = new lib.circle2();
	this.instance_5.parent = this;
	this.instance_5.setTransform(-109.1,-51.65,0.1451,0.1451);
	this.instance_5._off = true;

	var maskedShapeInstanceList = [this.instance_5];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(9).to({_off:false},0).to({regX:-0.1,regY:-0.1,scaleX:1,scaleY:0.6628,x:-109.15,y:-38.7},27,cjs.Ease.quartOut).wait(94).to({regX:0,regY:0,scaleX:1.3104,scaleY:0.8685,x:-109.05,y:-38.65,alpha:0},23,cjs.Ease.cubicIn).wait(17));

	// Calque_3
	this.instance_6 = new lib.circle1();
	this.instance_6.parent = this;
	this.instance_6.setTransform(-109,-54.55,0.1455,0.1455);
	this.instance_6._off = true;

	var maskedShapeInstanceList = [this.instance_6];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(18).to({_off:false},0).to({scaleX:1,scaleY:0.4909,y:-44.55},27,cjs.Ease.quartOut).wait(95).to({regX:-0.1,scaleX:1.6334,scaleY:0.8018,x:-109.15,alpha:0},23,cjs.Ease.cubicIn).wait(7));

	// Calque_14
	this.instance_7 = new lib.line();
	this.instance_7.parent = this;
	this.instance_7.setTransform(-219.5,-285.05);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(119).to({_off:false},0).wait(51));

	// Calque_13
	this.instance_8 = new lib.line();
	this.instance_8.parent = this;
	this.instance_8.setTransform(-39.5,-385.05);
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(101).to({_off:false},0).wait(69));

	// Calque_12
	this.instance_9 = new lib.line();
	this.instance_9.parent = this;
	this.instance_9.setTransform(-394.5,-345.05);
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(61).to({_off:false},0).wait(109));

	// Calque_11
	this.instance_10 = new lib.line();
	this.instance_10.parent = this;
	this.instance_10.setTransform(-89.5,-225.05);
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(69).to({_off:false},0).wait(101));

	// Calque_10
	this.instance_11 = new lib.line();
	this.instance_11.parent = this;
	this.instance_11.setTransform(-504.5,-240.05);
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(41).to({_off:false},0).wait(129));

	// Calque_9
	this.instance_12 = new lib.line();
	this.instance_12.parent = this;
	this.instance_12.setTransform(-209.5,-235.05);
	this.instance_12._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(17).to({_off:false},0).wait(153));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-537.4,-323.5,537,477.5);


(lib.bloc_video = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":41});

	// timeline functions:
	this.frame_40 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(40).call(this.frame_40).wait(17));

	// masque video (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_42 = new cjs.Graphics().p("Ega8A8AMAAAh3/MA7rAAAMAAAB3/g");
	var mask_graphics_43 = new cjs.Graphics().p("Ega8A8AMAAAh3/MA7qAAAMAAAB3/g");
	var mask_graphics_44 = new cjs.Graphics().p("Ega0A8AMAAAh3/MA7aAAAMAAAB3/g");
	var mask_graphics_45 = new cjs.Graphics().p("EgaSA8AMAAAh3/MA6VAAAMAAAB3/g");
	var mask_graphics_46 = new cjs.Graphics().p("EgY1A8AMAAAh3/MA3bAAAMAAAB3/g");
	var mask_graphics_47 = new cjs.Graphics().p("EgVyA8AMAAAh3/MAxSAAAMAAAB3/g");
	var mask_graphics_48 = new cjs.Graphics().p("EgQQA8AMAAAh3/MAmIAAAMAAAB3/g");
	var mask_graphics_49 = new cjs.Graphics().p("EgIMA8AMAAAh3/IV3AAMAAAB3/g");
	var mask_graphics_50 = new cjs.Graphics().p("EgCpA8AMAAAh3/IKsAAMAAAB3/g");
	var mask_graphics_51 = new cjs.Graphics().p("EAAZA8AMAAAh3/IEkAAMAAAB3/g");
	var mask_graphics_52 = new cjs.Graphics().p("EAB1A8AMAAAh3/IBqAAMAAAB3/g");
	var mask_graphics_53 = new cjs.Graphics().p("EACXA8AMAAAh3/IAlAAMAAAB3/g");
	var mask_graphics_54 = new cjs.Graphics().p("EACfA8AMAAAh3/IAVAAMAAAB3/g");
	var mask_graphics_55 = new cjs.Graphics().p("EACgA8AMAAAh3/IAUAAMAAAB3/g");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(42).to({graphics:mask_graphics_42,x:209.4828,y:0}).wait(1).to({graphics:mask_graphics_43,x:209.4292,y:0}).wait(1).to({graphics:mask_graphics_44,x:208.6245,y:0}).wait(1).to({graphics:mask_graphics_45,x:205.1378,y:0}).wait(1).to({graphics:mask_graphics_46,x:195.7504,y:0}).wait(1).to({graphics:mask_graphics_47,x:175.9563,y:0}).wait(1).to({graphics:mask_graphics_48,x:139.9623,y:0}).wait(1).to({graphics:mask_graphics_49,x:87.4933,y:0}).wait(1).to({graphics:mask_graphics_50,x:51.4993,y:0}).wait(1).to({graphics:mask_graphics_51,x:31.7053,y:0}).wait(1).to({graphics:mask_graphics_52,x:22.3178,y:0}).wait(1).to({graphics:mask_graphics_53,x:18.8311,y:0}).wait(1).to({graphics:mask_graphics_54,x:18.0265,y:0}).wait(1).to({graphics:mask_graphics_55,x:17.973,y:0}).wait(1).to({graphics:null,x:0,y:0}).wait(1));

	// VIDEO
	this.videoContainer = new lib.video();
	this.videoContainer.name = "videoContainer";
	this.videoContainer.parent = this;
	this.videoContainer.setTransform(36.7,765,0.6587,0.6587,0,0,0,0.3,480.2);

	var maskedShapeInstanceList = [this.videoContainer];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.videoContainer).to({y:-33},40,cjs.Ease.quartInOut).to({_off:true},16).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-349.3,418.6,1430.3999999999999);


(lib.bloc_NEXT_btn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":20});

	// timeline functions:
	this.frame_19 = function() {
		this.stop();
	}
	this.frame_36 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(19).call(this.frame_19).wait(17).call(this.frame_36).wait(1));

	// CTA DETAIL
	this.btnNext = new lib.Btn_next();
	this.btnNext.name = "btnNext";
	this.btnNext.parent = this;
	this.btnNext.setTransform(-340.5,411.5,1,1,0,0,0,0,26.5);

	this.timeline.addTween(cjs.Tween.get(this.btnNext).to({y:350.5},19,cjs.Ease.quartInOut).wait(2).to({y:411.5},14,cjs.Ease.quartInOut).to({_off:true},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-454,0,453,444.5);


(lib.bloc_hashtag = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_61 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(61).call(this.frame_61).wait(2));

	// Layer_1
	this.instance = new lib.hashtag_graphic("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(-456.05,243.2,2.3169,2.3169,0,0,0,-0.1,50.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true},61).wait(2));

	// Layer_5 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_28 = new cjs.Graphics().p("AtWBpIAAjRIatAAIAADRg");
	var mask_graphics_32 = new cjs.Graphics().p("AtWDSIAAmjIatAAIAAGjg");
	var mask_graphics_40 = new cjs.Graphics().p("AtWExIAAphIatAAIAAJhg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(28).to({graphics:mask_graphics_28,x:-400.45,y:265.45}).wait(4).to({graphics:mask_graphics_32,x:-400.45,y:255.025}).wait(8).to({graphics:mask_graphics_40,x:-400.45,y:245.55}).wait(23));

	// Layer_3
	this.instance_1 = new lib.txt_hashtag();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-421.5,244.05,1,1,0,0,0,0,31.6);
	this.instance_1._off = true;

	var maskedShapeInstanceList = [this.instance_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(28).to({_off:false},0).to({_off:true},34).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-573.7,0,287.1,317.1);


(lib.bloc_CTA = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":25});

	// timeline functions:
	this.frame_24 = function() {
		this.stop();
	}
	this.frame_41 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(24).call(this.frame_24).wait(17).call(this.frame_41).wait(1));

	// CTA DETAIL
	this.btnCta = new lib.Btn_detail();
	this.btnCta.name = "btnCta";
	this.btnCta.parent = this;
	this.btnCta.setTransform(-340.5,411.5,1,1,0,0,0,0,26.5);

	this.timeline.addTween(cjs.Tween.get(this.btnCta).to({y:350.5},24,cjs.Ease.quartInOut).wait(2).to({y:411.5},14,cjs.Ease.quartInOut).to({_off:true},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-454.5,0,228,444.5);


(lib.blocInfostextes = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":80});

	// timeline functions:
	this.frame_79 = function() {
		this.stop();
	}
	this.frame_120 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(79).call(this.frame_79).wait(41).call(this.frame_120).wait(1));

	// Bloc Hashtag
	this.instance = new lib.bloc_hashtag();
	this.instance.parent = this;
	this.instance.setTransform(-391.5,251.2,0.9,0.9,0,0,0,-412.9,244.7);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(62).to({_off:false},0).to({_off:true},18).wait(41));

	// masque Titre
	this.instance_1 = new lib.masque_generique();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-527.95,-13.9,0.0075,0.4711,0,0,0,0,17.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(34).to({_off:false},0).to({regX:0.5,scaleX:1.9223,x:-527.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.0128,x:-228.9},10,cjs.Ease.quartInOut).to({_off:true},1).wait(26).to({_off:false},0).to({regX:0.5,scaleX:1.9223,x:-527.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.0074,x:-528.7},10,cjs.Ease.quartInOut).to({_off:true},1).wait(21));

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#01FBFC").s().p("AAZA4IgZhMIgYBMIgSAAIgnhvIAbAAIAWA/IAUg/IAZAAIAUA/IAWg/IAbAAIgnBvg");
	this.shape.setTransform(-467.95,-14.15);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#01FBFC").s().p("AAmA4IAAhFIgfA7IgNAAIgeg7IAABFIgZAAIAAhvIAhAAIAcA9IAdg9IAiAAIAABvg");
	this.shape_1.setTransform(-495.7,-14.15);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#01FBFC").s().p("AgsA4IAAhvIAsAAQAKAAAIADQAJACAEAFQAIAJgBAKQABAOgJAHIgEACIgEACQAKABAGAHQAHAIgBAKQABALgJAJQgIALgYAAgAgTAjIASAAQAJAAAGgCQAFgDAAgIQAAgIgGgCQgFgDgMAAIgPAAgAgTgKIAMAAQAKABAEgDQAFgCABgHQAAgIgFgCQgFgCgKgBIgMAAg");
	this.shape_2.setTransform(-520.5,-14.15);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},34).to({state:[]},55).to({state:[]},1).wait(31));

	// masque Titre (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_17 = new cjs.Graphics().p("EgpOArJMAAAhWRMA3SAAAMAAABWRg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(17).to({graphics:mask_graphics_17,x:-263.9,y:-6.1095}).wait(24).to({graphics:null,x:0,y:0}).wait(80));

	// Layer_5
	this.instance_2 = new lib.lettres_FAT();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-433.85,-73.05,1,1,0,0,0,0,186.8);
	this.instance_2._off = true;

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#01FBFC").s().p("AkWFdIAAq5IEQAAQBGAAAzAQQA0ARAaAeQAvA2AABDQAABTg1AoQgSAOgHADIgZAMQBBANAmAsQAnAsAABDQAABIgyA4Qg6BCiQgBgAh6DaIBrAAQBCgBAigQQAhgQAAgxQAAgxgjgPQgkgPhNAAIhcAAgAh6hCIBKAAQBAAAAggOQAfgNAAgvQAAgugdgPQgdgQhEAAIhLAAg");
	this.shape_3.setTransform(-229.5,131.75);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#01FBFC").s().p("ADrFeIAAm3Ii9F8IhcAAIi8l8IAAG3IicAAIAAq6IDTAAICzF/IC1l/IDSAAIAAK6g");
	this.shape_4.setTransform(-448.7,7.85);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#01FBFC").s().p("ACZFdIiZnlIiXHlIh0AAIj0q5ICoAAICLGQIB8mQICiAAIB6GQICMmQICpAAIj1K5g");
	this.shape_5.setTransform(-326.15,-116);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#01FBFC").s().p("AkWFdIAAq5IEQAAQBGgBA0ASQAzARAaAdQAvA1AABEQAABSg1ApQgSAOgHADIgZALQBBAOAnAsQAmAsAABDQAABIgyA4Qg6BBiQAAgAh6DaIBrAAQBCAAAhgRQAigQAAgxQAAgwgkgQQgjgPhNAAIhcAAgAh6hCIBKAAQBAABAfgOQAggOAAgvQAAgugdgPQgdgPhEgBIhLAAg");
	this.shape_6.setTransform(-498.3,-116);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#01FBFC").s().p("ADrFdIAAm2Ii9F8IhcAAIi8l8IAAG2IicAAIAAq5IDTAAICzF+IC1l+IDSAAIAAK5g");
	this.shape_7.setTransform(-316.5,106.75);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#01FBFC").s().p("AkWFeIAAq7IEQAAQBGABA0AQQAzARAaAdQAvA3AABEQAABRg1AoQgSAOgHAEIgZALQBBANAnAtQAmAtAABBQAABJgyA5Qg6BAiQABgAh6DZIBrAAQBCAAAhgQQAigQAAgxQAAgwgkgQQgjgQhNAAIhcAAgAh6hBIBKAAQBAAAAfgPQAggOAAguQAAgvgdgOQgdgPhEAAIhLAAg");
	this.shape_8.setTransform(-213.8,-17.15);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#01FBFC").s().p("ACYFeIiYnlIiYHlIh0AAIj0q7ICpAAICKGSIB8mSICiAAIB7GSICLmSICpAAIj0K7g");
	this.shape_9.setTransform(-466.85,-17.15);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#01FBFC").s().p("ADrFeIAAm3Ii9F8IhcAAIi8l8IAAG3IicAAIAAq7IDTAAICzGAIC1mAIDSAAIAAK7g");
	this.shape_10.setTransform(-470.1,-141);

	var maskedShapeInstanceList = [this.instance_2,this.shape_3,this.shape_4,this.shape_5,this.shape_6,this.shape_7,this.shape_8,this.shape_9,this.shape_10];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_2}]},17).to({state:[{t:this.instance_2}]},14).to({state:[{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3}]},1).to({state:[{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7}]},5).to({state:[]},4).to({state:[]},39).wait(41));
	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(17).to({_off:false},0).to({x:-276.85},14,cjs.Ease.quartOut).to({_off:true},1).wait(89));

	// masque Titre
	this.instance_3 = new lib.masque_generique();
	this.instance_3.parent = this;
	this.instance_3.setTransform(221.1,-163.9,0.0128,0.4711,0,0,0,0,17.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({regX:0.5,scaleX:1.9223,x:-75.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.0074,x:-68.7},10,cjs.Ease.quartInOut).to({_off:true},1).wait(101));

	// Calque_2
	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#FFFFFF").ss(1,1,1).p("AxLpXIEsAAIEsAAIEsAAIErAAIcmAAAsftKIAADzIAAEsIAAErIAAEsIAAEsIAADzAxLtKIAADzIAAEsIAAErIEsAAIEsAAIAAEsIAAEsIEsAAIErAAIAADzA+JpXIM+AAAnztKIAADzIAAEsIAAErIEsAAIAAEsIAAEsIAADzAjHtKIAADzIAAEsIAAErIErAAIAAEsIAAEsABktKIAADzIAAEsIAAErIcmAAAnzkrIEsAAIErAAIcmAAAnzEsIEsAAIErAAIcmAAAnzJYIAADzAxLkrIEsAAIEsAAAxLEsIAAEsIEsAAIEsAAAxLEsIEsAAIEsAAAxLAAIAAEsAxLJYIAADzA+JJYIM+AAA+JEsIM+AAA+JAAIM+AA");
	this.shape_11.setTransform(238.525,38.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#FFFFFF").ss(1,1,1).p("EAkfgSEIksAAIksAAIAAEsIAAEsIAAEsIAAErIAAcmEAkfgfCIAAM+IAAEsIksAAIksAAIksAAIjzAAAbH/CIAAM+IksAAIjzAAAfz/CIAAM+IAAEsIAAEsIksAAIksAAIAAEsIjzAAEAs9gSEIjyAAIAAEsIAAEsIAAEsIAAErIksAAIksAAIksAAIksAAIjzAAEApLgfCIAAM+IksAAEAs9gNYIjyAAIksAAIAAEsIksAAIAAEsIksAAIksAAIAAErIAAcmEAs9gIsIjyAAIksAAIAAEsIksAAIAAErIAAcmEAs9gEAIjyAAIksAAIAAErEAs9AArIjyAAIAAcmAWbosIjzAAAWbtYIAAEsAWbyEIAAEsA4nOFIDiAAIAADiIAADiIAADhIAADiIAAC3A4nRnIDiAAA4nOFIAADiIAADiIAADhIDiAAA4nVJIDiAAIVfAAA4ncMIDiAAIVfAAA4nYqIAADiIAAC3A1FOFIVfAAA/qOFIDhAAIAADiIAADiIAADhIAADiIAAC3A/qRnIDhAAIDiAAA/qOFIAADiIAADiIAADhIAADiIDhAAIDiAAEgjMAOFIAADiIAADiIAADhIAADiIDiAAIAAC3A/qVJIDhAAIDiAAA/qYqIDhAAIDiAAEgjMAOFIDiAAEgjMAYqIDiAAEgjMAVJIDiAAEgjMARnIDiAAEgjMAcMIAAC3Egs8AYqIJwAAEgs8ARnIJwAAA8JOFIDiAAA4nLPIAAC2A1FLPIAAC2A8JLPIAAC2A/qLPIAAC2EgjMALPIAAC2");
	this.shape_12.setTransform(-157.975,-82.775);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#FFFFFF").ss(1,1,1).p("AEsxLIksAAIkrAAIAAEsIAAEsIAAEsIAAErIAAcmAEs+JIAAM+IAAEsIksAAIkrAAIksAAIjzAAAkr+JIAAM+IksAAIjzAAAAA+JIAAM+IAAEsIAAEsIkrAAIksAAIAAEsIjzAAANLxLIjzAAIAAEsIAAEsIAAEsIAAErIksAAIksAAIkrAAIksAAIjzAAAJY+JIAAM+IksAAANLsfIjzAAIksAAIAAEsIksAAIAAEsIkrAAIksAAIAAErIAAcmANLnzIjzAAIksAAIAAEsIksAAIAAErIAAcmANLjHIjzAAIksAAIAAErANLBkIjzAAIAAcmApXnzIjzAAApXsfIAAEsApXxLIAAEs");
	this.shape_13.setTransform(45.5,-88.475);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_11}]},21).to({state:[{t:this.shape_12}]},3).to({state:[{t:this.shape_13}]},6).to({state:[]},4).wait(87));

	// Layer_7
	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#FFFFFF").ss(2,1,1).p("EAhmAAAQAAN7p2J1Qp1J2t7AAQt5AAp2p2Qp2p1AAt7QAAt5J2p2QJ2p2N5AAQN7AAJ1J2QJ2J2AAN5g");
	this.shape_14.setTransform(161.9,-25.05);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#FFFFFF").ss(2,1,1).p("ARHAAQAAHFlBFBQlBFBnFAAQnFAAlAlBQlBlBAAnFQAAnFFBlBQFAlAHFAAQHFAAFBFAQFBFBAAHFg");
	this.shape_15.setTransform(4.525,38.5);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#FFFFFF").ss(2,1,1).p("EAhmAAAQAAN7p2J1Qp1J2t7AAQp1AAn0k7QjOiDi5i4QgigjghgjQoypgAAtKQAAt5J1p3QJ3p1N5AAQN7AAJ1J1QJ2J3AAN5g");
	this.shape_16.setTransform(44.9,-139.05);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#FFFFFF").ss(2,1,1).p("AIIAAQAADYiYCYQiYCYjYAAQjXAAiYiYQiYiYAAjYQAAjWCYiZQCYiYDXAAQDYAACYCYQAGAHAHAGQCLCVAADNg");
	this.shape_17.setTransform(-454.45,150.525);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#FFFFFF").ss(1,1,1).p("AiWnDIDhAAIAADiIAADhIAADhIAADiIAAC3ABLp5IAAC2IVhAAAiWp5IAAC2IAADiIDhAAAiWAAIDhAAIVhAAAiWjhIAADhIAADhIAADiIDhAAIVhAAAiWDhIDhAAAiWHDIAAC3ApZnDIDiAAIAADiIAADhIAADhIAADiIAAC3ApZp5IAAC2IAADiIDiAAIDhAAAl3p5IAAC2IDhAAAs7p5IAAC2IAADiIDiAAIAADhIDiAAIDhAAAs7jhIAADhIDiAAIAADhIDiAAIDhAAAs7nDIDiAAA2rDhIElAAIFLAAIAADiIDiAAIDiAAIDhAAApZDhIAADiIAAC3As7DhIDiAAAs7HDIAAC3As7AAIAADhA2rjhIJwAA");
	this.shape_18.setTransform(-300.525,92.525);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#FFFFFF").ss(1,1,1).p("ABkiyIAAErIAAEsIcmAAAjHiyIAAErIErAAIcmAAAjHiyIErAAAnziyIAAErIEsAAIAAEsIAAEsAjHGlIErAAAnzGlIEsAAAnziyIEsAAAnzB5IAAEsAsfiyIAAErIAAEsIAAEsAsfiyIksAAIAAErIEsAAIEsAAAxLGlIAAEsAxLGlIEsAAIEsAAAxLB5IAAEsAsfiyIEsAAA+JB5IM+AAA+JGlIM+AAAnzrQIAAIeABkrQIAAIe");
	this.shape_19.setTransform(-501.475,26.375);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_14}]},17).to({state:[{t:this.shape_15}]},4).to({state:[{t:this.shape_16}]},3).to({state:[{t:this.shape_16}]},6).to({state:[]},6).to({state:[{t:this.shape_15}]},57).to({state:[{t:this.shape_18},{t:this.shape_17}]},4).to({state:[{t:this.shape_19}]},3).to({state:[]},6).to({state:[]},1).wait(14));

	// Layer_8
	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#01FBFC").s().p("AAaJIIi6gDIhygDQABgBCGmtICHmtIAIgcQAMgiARgcQA2hbBYABQCNABBsgBIiQHoQiSHqABAAQgDASgPAQQgfAig7AAIgBgBgAkSJCIAAAAIAAAAIAAAAgAlzhpIg0gCIB7mKIAEgNQAGgPAIgNQAYgqApAAIBzAAIhDDhIhCDhQgBAIgIAIQgOAPgcAAIhVgCg");
	this.shape_20.setTransform(216.5001,146.1504);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#01FBFC").s().p("AimWRQijgBlXgIIk4gKQADAAFtySQFuyUACACQAIgeAQgvQAhhcAthNQCTj2DwABQGCADEmgDQABAAmLU2QmOU1ADABQgHAwgqAtQhTBaigAAIgGgBgAIdWPIhYgDIDNqNIAIgWQAIgZANgWQAqhFBCAAQBsABBSgBIhuF1QhvF0ABAAQgCANgNANQgXAZguAAIiMgCgAvYV+IAAAAIAAAAIAAAAg");
	this.shape_21.setTransform(104.6,50.7508);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#01FBFC").s().p("AOOKLQhLAAidgEIiPgEQACAACmoXICpoXIALgjQAPgqAVgjQBEhxBtABQCwABCGgBIi0JiQi1JgABABQgDAWgTAUQgmAqhJAAIgDgBgAhgKLQhLAAidgEIiOgEQABAACnoXICooXIALgjQAPgqAVgjQBEhxBtABQCwABCGgBIi0JiQi2JgABABQgCAWgUAUQglAqhJAAIgDgBgAwlKLQhKAAiegEIiOgEQABAACnoXICooXIAMgjQAPgqAUgjQBEhxBuABQCwABCGgBIi0JiQi2JgABABQgDAWgTAUQgmAqhJAAIgDgBgATRKKIgogBIBekrIADgJQAEgMAFgKQATggAgAAIBWAAIgyCrIgzCqQAAAGgHAGQgKALgVAAIhAgBgADiKKIgngBIBdkrIADgJQAEgMAGgKQATggAfAAIBXAAIgzCrIgyCqQgBAGgGAGQgKALgWAAIhAgBgAriKKIgogBIBekrIADgJQAEgMAGgKQATggAfAAIBWAAIgyCrIgyCqQgBAGgGAGQgLALgUAAIhBgBgAIXKDIAAAAIAAAAIAAAAgAnWKDIAAAAIAAAAIAAAAgA2bKDIAAAAIAAAAIAAAAg");
	this.shape_22.setTransform(-47.4,128.1507);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#01FBFC").s().p("Ag1KLQhKAAidgEIiPgEQABAACnoXICooXIAMgjQAPgqAUgjQBDhxBuABQCwABCGgBIi0JiQi1JgABABQgDAWgUAUQglAqhIAAIgEgBgAmrKDIAAAAIAAAAIAAAAgAJSCIIgngBIBdkqIAEgJQAEgMAFgKQATggAfAAIBXAAIgzCrIgyCpQgBAGgGAGQgKALgVAAIhBgBgAr1CIIgogBIBekqIADgJQAEgMAGgKQATggAfAAIBWAAIgyCrIgyCpQgBAGgGAGQgLALgVAAIhAgBg");
	this.shape_23.setTransform(161.075,-28.0993);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_20}]},8).to({state:[{t:this.shape_21}]},2).to({state:[{t:this.shape_22}]},3).to({state:[{t:this.shape_23}]},4).to({state:[]},2).to({state:[]},61).wait(41));

	// masque Titre
	this.instance_4 = new lib.masque_generique();
	this.instance_4.parent = this;
	this.instance_4.setTransform(-228.9,-13.9,0.0128,0.4711,0,0,0,0,17.2);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(4).to({_off:false},0).to({regX:0.5,scaleX:1.9223,x:-527.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.0074,x:-528.7},10,cjs.Ease.quartInOut).to({_off:true},1).wait(97));

	// Layer_10
	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#01FBFC").s().p("AgJA0QgEgEAAgGQAAgFAEgFQAEgEAFAAQAGAAAEAEQAEAFAAAFQAAAGgEAEQgEAFgGAAQgFAAgEgFgAgJAVIgEg5IAAgUIAbAAIAAAUIgFA5g");
	this.shape_24.setTransform(-357.925,-14.075);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#01FBFC").s().p("AAbA4Ig1hFIAABFIgZAAIAAhvIAYAAIA2BHIAAhHIAZAAIAABvg");
	this.shape_25.setTransform(-378.95,-14.15);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#01FBFC").s().p("AgoA4IAAhvIBPAAIAAAXIg2AAIAAAWIAxAAIAAAUIgxAAIAAAYIA4AAIAAAWg");
	this.shape_26.setTransform(-402.45,-14.15);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#01FBFC").s().p("AgMA4IAAhvIAZAAIAABvg");
	this.shape_27.setTransform(-422.55,-14.15);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#01FBFC").s().p("AgCA5IADgYIgNAAIgEAYIgZAAIAFgYIgQAAIAFgWIAPAAIADgTIgRAAIAEgWIARAAIAEgaIAXAAIgDAaIANAAIAEgaIAZAAIgFAaIARAAIgEAWIgRAAIgDATIASAAIgEAWIgSAAIgEAYgAgJALIAOAAIAEgTIgOAAg");
	this.shape_28.setTransform(-459.375,-14.275);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#01FBFC").s().p("AgjA4IAAhvIAZAAIAABYIAuAAIAAAXg");
	this.shape_29.setTransform(-497.625,-14.15);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#01FBFC").s().p("AgjApQgRgQAAgYQAAgZARgQQASgRAXAAQAcAAASAVIgPASQgMgPgSAAQgNAAgKAJQgJAJgBAPQAAAPAKAKQAJAKANAAQASAAALgPIAQARQgTAVgZAAQgZAAgRgRg");
	this.shape_30.setTransform(-520.4,-14.2);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#01FBFC").s().p("AgMA4IAAhZIgfAAIAAgWIBXAAIAAAWIggAAIAABZg");
	this.shape_31.setTransform(-411.925,-14.15);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#01FBFC").s().p("AAbA4Ig0hFIAABFIgaAAIAAhvIAYAAIA2BHIAAhHIAZAAIAABvg");
	this.shape_32.setTransform(-435.3,-14.15);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27,p:{x:-422.55}},{t:this.shape_26,p:{x:-402.45}},{t:this.shape_25},{t:this.shape_24,p:{x:-357.925}}]},13).to({state:[{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_24,p:{x:-422.675}}]},5).to({state:[{t:this.shape_30},{t:this.shape_29},{t:this.shape_27,p:{x:-478.9}},{t:this.shape_26,p:{x:-458.8}},{t:this.shape_32},{t:this.shape_31}]},5).to({state:[]},11).to({state:[]},46).wait(41));

	// Layer_11
	this.instance_5 = new lib.masqueTexte("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(-340,105.8,0.85,0.85,0,0,0,0.1,108.8);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(37).to({_off:false},0).to({_off:true},40).wait(3).to({_off:false,rotation:180},0).to({_off:true},40).wait(1));

	// Layer_12
	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAggeQAhgeArAAQAtAAAfAbQAfAbAAAuIAAAfIicAAQADASAPALQAQALATAAQAfAAAVgVIAiAlQglAigxAAQgwAAgggegAgdgyQgPALgCATIBdAAQgCgUgNgKQgMgKgSAAQgQAAgPAKg");
	this.shape_33.setTransform(-329.1,182.175);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AhMBXQgYgWAAgsIAAiBIA9AAIAAB0QAAAyAkgBQARABANgNQANgNAAgZIAAhzIA9AAIAADWIg9AAIAAgdQgWAggiAAQgkAAgYgWg");
	this.shape_34.setTransform(-354.475,182.35);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AAyCUIAAheQgZATggAAQgpABgfgfQgfgfAAgwQAAgwAeggQAegfAlAAQAkAAAbAcIAAgZIA9AAIAAEkgAgihQQgPASAAAaQAAAaAPAQQAQAQATABQAVgBAPgQQAOgQAAgaQAAgagOgSQgPgRgVAAQgUAAgPARg");
	this.shape_35.setTransform(-381.075,185.9);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AgdCXIAAjWIA7AAIAADWgAgYhaQgKgKAAgPQAAgPAKgKQAKgKAOAAQAPAAAKAKQAKAKAAAPQAAAPgKAKQgKAKgPAAQgOAAgKgKg");
	this.shape_36.setTransform(-399.625,177.825);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("Ag9BtIAAjWIA8AAIAAAdQAKgOASgJQARgJASAAIAAA4IgLAAQgbAAgNATQgMATAAAeIAABdg");
	this.shape_37.setTransform(-412.825,182.025);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("AggB5QgVgUAAgkIAAhiIgZAAIAAgrIAZAAIAAhAIA8AAIAABAIA0AAIAAArIg0AAIAABgQAAAMAGAHQAHAHAIAAQARAAALgPIAXAqQgcAZgfAAQgeAAgWgUg");
	this.shape_38.setTransform(-430.15,179.125);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AhEBQQgigeAAgyQAAgwAigfQAigfAtAAQAXAAAYAKQAYAJASATIgfApQgLgNgPgGQgOgHgPAAQgXAAgRAPQgRAQAAAaQAAAbARAQQARAPAWAAQAeAAAYgeIAkAoQgqArgwAAQgwAAghgfg");
	this.shape_39.setTransform(-450.925,182.175);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAhgeQAggeArAAQAtAAAfAbQAfAbAAAuIAAAfIicAAQADASAQALQAPALATAAQAfAAAVgVIAhAlQgkAigxAAQgwAAgggegAgdgyQgPALgCATIBdAAQgCgUgMgKQgNgKgSAAQgRAAgOAKg");
	this.shape_40.setTransform(-475.05,182.175);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("AgdCVIAAkoIA7AAIAAEog");
	this.shape_41.setTransform(-493.3,178.1);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("AhLCBQgggeAAgzQAAgyAhgeQAggdArgBQAtABAfAaQAfAbAAAtIAAAhIicAAQADARAQALQAPAMATgBQAfAAAVgUIAhAkQgkAigxAAQgwAAgggegAgdgBQgPAJgDATIBeAAQgCgUgMgJQgNgKgRAAQgRAAgPALgAglhYIAzhGIA+AZIg4Atg");
	this.shape_42.setTransform(-511.4,177.35);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAggeQAhgeAsAAQAsAAAfAbQAfAbAAAuIAAAfIicAAQADASAPALQAQALATAAQAfAAAVgVIAiAlQglAigxAAQgwAAgggegAgdgyQgPALgDATIBeAAQgCgUgNgKQgMgKgRAAQgSAAgOAKg");
	this.shape_43.setTransform(-267.3,133.425);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFFFFF").s().p("Ag9BtIAAjWIA8AAIAAAdQAKgOASgJQARgJASAAIAAA4IgLAAQgbAAgNATQgMATAAAeIAABdg");
	this.shape_44.setTransform(-287.125,133.275);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFFFFF").s().p("AhMBXQgYgWAAgsIAAiBIA9AAIAAB0QAAAxAkAAQARAAANgMQANgNAAgZIAAhzIA9AAIAADWIg9AAIAAgcQgWAfgiAAQgkAAgYgWg");
	this.shape_45.setTransform(-309.125,133.6);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FFFFFF").s().p("AgfB5QgWgUAAgkIAAhiIgZAAIAAgrIAZAAIAAhAIA7AAIAABAIA1AAIAAArIg1AAIAABgQABAMAGAHQAHAHAIAAQARAAAMgPIAWAqQgcAZgfAAQgeAAgVgUg");
	this.shape_46.setTransform(-330.05,130.375);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFFFFF").s().p("AgdCXIAAjWIA7AAIAADWgAgYhaQgKgKAAgPQAAgPAKgKQAKgKAOAAQAPAAAKAKQAKAKAAAPQAAAPgKAKQgKAKgPAAQgOAAgKgKg");
	this.shape_47.setTransform(-345.225,129.075);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FFFFFF").s().p("AhQBPQghggAAgvQAAguAhggQAgggAwAAQAxAAAgAgQAhAgAAAuQAAAvghAgQggAggxAAQgwAAgggggAglgqQgPARAAAZQAAAbAPAQQAPARAWAAQAXAAAPgRQAPgQAAgbQAAgZgPgRQgPgRgXAAQgWAAgPARg");
	this.shape_48.setTransform(-364.025,133.425);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FFFFFF").s().p("AgdBsIhWjXIA+AAIA1CHIA2iHIA+AAIhVDXg");
	this.shape_49.setTransform(-388.75,133.425);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAhgeQAggeArAAQAtAAAfAbQAfAbAAAuIAAAfIicAAQADASAQALQAPALATAAQAfAAAVgVIAhAlQgkAigxAAQgwAAgggegAgdgyQgPALgDATIBeAAQgCgUgMgKQgNgKgRAAQgRAAgPAKg");
	this.shape_50.setTransform(-423.2,133.425);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FFFFFF").s().p("AApBtIAAh4QAAgyglAAQgRAAgNANQgNANAAAZIAAB3Ig9AAIAAjWIA9AAIAAAYQAagbAiAAQAhAAAXAYQAYAYAAAnIAACCg");
	this.shape_51.setTransform(-448.3,133.275);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FFFFFF").s().p("AhMBXQgYgWAAgsIAAiBIA9AAIAAB0QAAAxAkAAQARAAANgMQANgNAAgZIAAhzIA9AAIAADWIg9AAIAAgcQgWAfgiAAQgkAAgYgWg");
	this.shape_52.setTransform(-474.125,133.6);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FFFFFF").s().p("AgXA2IAAhqIAvAAIAABqg");
	this.shape_53.setTransform(-491.975,121.6);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FFFFFF").s().p("AhQB2QgeggAAgwQAAgwAdgeQAegeAngBQAmAAAZAcIAAhqIA8AAIAAEoIg8AAIAAgcQgaAfglAAQgmAAgegggAgigDQgPARAAAaQAAAaAPARQAQAQATAAQAVAAAPgQQAOgRAAgaQAAgagOgRQgPgSgVAAQgUAAgPASg");
	this.shape_54.setTransform(-511.125,129.5);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FFFFFF").s().p("AgfB5QgWgUAAgkIAAhiIgZAAIAAgrIAZAAIAAhAIA7AAIAABAIA1AAIAAArIg1AAIAABgQABAMAGAHQAHAHAIAAQARAAAMgPIAWAqQgcAZgfAAQgeAAgVgUg");
	this.shape_55.setTransform(-164,81.625);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FFFFFF").s().p("AAoBtIAAh4QAAgygkAAQgRAAgNANQgOANAAAZIAAB3Ig7AAIAAjWIA7AAIAAAYQAcgbAgAAQAjAAAWAYQAXAYAAAnIAACCg");
	this.shape_56.setTransform(-186,84.525);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAggeQAhgeAsAAQAsAAAfAbQAfAbAAAuIAAAfIicAAQADASAPALQAQALATAAQAfAAAVgVIAhAlQgkAigxAAQgwAAgggegAgdgyQgPALgDATIBeAAQgCgUgNgKQgMgKgRAAQgSAAgOAKg");
	this.shape_57.setTransform(-211.15,84.675);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#FFFFFF").s().p("ABtBtIAAh0QAAgygkAAQgSAAgNANQgOANAAAZIAABzIg7AAIAAh0QAAgagIgMQgIgMgRAAQgSAAgNANQgNANAAAZIAABzIg8AAIAAjWIA8AAIAAAYQAZgbAfAAQAVAAAQALQAQANAIARQANgUAVgLQAVgKAVAAQAmAAAXAWQAXAXAAAqIAACCg");
	this.shape_58.setTransform(-243.125,84.525);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAhgeQAggeArAAQAtAAAfAbQAfAbAAAuIAAAfIicAAQADASAQALQAPALATAAQAfAAAVgVIAhAlQgkAigxAAQgwAAgggegAgdgyQgPALgDATIBeAAQgCgUgMgKQgNgKgRAAQgRAAgPAKg");
	this.shape_59.setTransform(-275.15,84.675);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#FFFFFF").s().p("AhEBQQgigeAAgyQAAgwAigfQAigfAtAAQAXAAAYAKQAYAJASATIgfApQgLgNgPgGQgOgHgPAAQgXAAgRAPQgRAQAAAaQAAAbARAQQARAPAWAAQAeAAAYgeIAkAoQgqArgwAAQgwAAghgfg");
	this.shape_60.setTransform(-299.025,84.675);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#FFFFFF").s().p("AAoBtIAAh4QAAgygkAAQgRAAgNANQgOANAAAZIAAB3Ig8AAIAAjWIA8AAIAAAYQAbgbAiAAQAhAAAXAYQAYAYAAAnIAACCg");
	this.shape_61.setTransform(-323.55,84.525);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#FFFFFF").s().p("AhJBbQgXgUAAghQAAghAYgPQAYgQApAAIAsAAIAAgCQAAgigkAAQgPAAgRAGQgRAGgNAKIgbgnQArgfA2AAQAmAAAZAUQAZATAAArIAACIIg4AAIAAgaQgXAdgiAAQghAAgYgUgAgkAmQAAALAJAHQAIAGARAAQAPAAAMgKQAMgKAAgQIAAgLIglAAQgkAAAAAXg");
	this.shape_62.setTransform(-348.8,84.675);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#FFFFFF").s().p("AgdCVIAAkoIA7AAIAAEog");
	this.shape_63.setTransform(-366,80.6);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAhgeQAggeArAAQAtAAAfAbQAfAbAAAuIAAAfIicAAQADASAQALQAPALATAAQAfAAAVgVIAhAlQgkAigxAAQgwAAgggegAgdgyQgPALgCATIBdAAQgCgUgMgKQgNgKgSAAQgRAAgOAKg");
	this.shape_64.setTransform(-394.6,84.675);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#FFFFFF").s().p("AgdCVIAAkoIA7AAIAAEog");
	this.shape_65.setTransform(-412.85,80.6);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#FFFFFF").s().p("Ag9BtIAAjWIA8AAIAAAdQAKgOASgJQARgJASAAIAAA4IgLAAQgbAAgNATQgMATAAAeIAABdg");
	this.shape_66.setTransform(-436.575,84.525);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#FFFFFF").s().p("AhMBXQgYgWAAgsIAAiBIA9AAIAAB0QAAAxAkABQARAAANgOQANgNAAgYIAAhzIA9AAIAADWIg9AAIAAgcQgWAfgiAAQgkAAgYgWg");
	this.shape_67.setTransform(-458.575,84.85);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#FFFFFF").s().p("AhQBPQghggAAgvQAAguAhggQAgggAwAAQAxAAAgAgQAhAgAAAuQAAAvghAgQggAggxAAQgwAAgggggAglgqQgPARAAAZQAAAbAPAQQAPARAWAAQAXAAAPgRQAPgQAAgbQAAgZgPgRQgPgRgXAAQgWAAgPARg");
	this.shape_68.setTransform(-484.225,84.675);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#FFFFFF").s().p("AhuCUIAAkjIA8AAIAAAXQAcgbAkAAQAmAAAdAfQAeAgAAAvQAAAvgeAgQgdAfgnABQgngBgYgfIAABqgAgkhPQgPARAAAaQAAAbAPAPQAPAQAUAAQAUAAAQgQQAPgPAAgaQAAgbgPgRQgPgSgUAAQgVAAgPASg");
	this.shape_69.setTransform(-509.925,88.4);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#FFFFFF").s().p("AhQBPQghggAAgvQAAguAhggQAgggAwAAQAxAAAgAgQAhAgAAAuQAAAvghAgQggAggxAAQgwAAgggggAglgqQgPARAAAZQAAAbAPAQQAPARAWAAQAXAAAPgRQAPgQAAgbQAAgZgPgRQgPgRgXAAQgWAAgPARg");
	this.shape_70.setTransform(-234.825,35.925);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#FFFFFF").s().p("AhLCBQgggeAAgzQAAgyAhgeQAggdArgBQAtABAfAaQAfAbAAAtIAAAhIicAAQADARAQAMQAPALATgBQAfAAAVgUIAhAkQgkAigxAAQgwAAgggegAgdgBQgPAJgCATIBdAAQgCgUgMgJQgNgKgSAAQgRAAgOALgAgkhYIAyhGIA9AZIg3Atg");
	this.shape_71.setTransform(-260.05,31.1);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#FFFFFF").s().p("AhQB2QgeghAAgwQAAguAdggQAegeAnAAQAmABAZAbIAAhqIA8AAIAAEoIg8AAIAAgdQgaAgglAAQgmAAgegggAgigDQgPARAAAaQAAAaAPAQQAQARATAAQAVAAAPgRQAOgQAAgbQAAgZgOgRQgPgRgVAAQgUAAgPARg");
	this.shape_72.setTransform(-286.425,32);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#FFFFFF").s().p("AgdCXIAAjWIA7AAIAADWgAgYhaQgKgKAAgPQAAgPAKgKQAKgKAOAAQAPAAAKAKQAKAKAAAPQAAAPgKAKQgKAKgPAAQgOAAgKgKg");
	this.shape_73.setTransform(-304.975,31.575);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#FFFFFF").s().p("AgeBsIhVjXIA/AAIA0CHIA2iHIA+AAIhVDXg");
	this.shape_74.setTransform(-322.55,35.925);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#FFFFFF").s().p("Ag1CQQgZgIgSgOIAXguQAdAXAhAAQAgAAAUgRQATgQAAgkQgdAggpAAQgqAAgdgdQgdgcAAgvQAAgwAegfQAegeAmAAQAlAAAaAgIAAgdIA8AAIAAC7QAAAdgKAWQgKAWgRANQghAbgtAAQgYAAgZgIgAgihVQgQAPAAAZQAAAaAPAQQAPAOAVAAQAWAAAOgOQAPgQAAgZQAAgagPgPQgOgPgVAAQgVAAgPAPg");
	this.shape_75.setTransform(-358.675,40.025);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#FFFFFF").s().p("AAoBtIAAh4QAAgygkAAQgRAAgNANQgOANAAAZIAAB3Ig7AAIAAjWIA7AAIAAAYQAbgbAhAAQAjAAAWAYQAYAYAAAnIAACCg");
	this.shape_76.setTransform(-384.1,35.775);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#FFFFFF").s().p("AgdCXIAAjWIA7AAIAADWgAgYhaQgKgKAAgPQAAgPAKgKQAKgKAOAAQAPAAAKAKQAKAKAAAPQAAAPgKAKQgKAKgPAAQgOAAgKgKg");
	this.shape_77.setTransform(-402.825,31.575);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("#FFFFFF").s().p("AhuCUIAAkkIA8AAIAAAZQAcgcAkAAQAmAAAdAfQAeAgAAAvQAAAvgeAgQgdAggngBQgnABgYggIAABqgAgkhQQgPASAAAaQAAAaAPAQQAPAQAUABQAUgBAQgQQAPgPAAgbQAAgagPgSQgPgRgUAAQgVAAgPARg");
	this.shape_78.setTransform(-421.375,39.65);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("#FFFFFF").s().p("AhuCUIAAkkIA8AAIAAAZQAcgcAkAAQAmAAAdAfQAeAgAAAvQAAAvgeAgQgdAggngBQgnABgYggIAABqgAgkhQQgPASAAAaQAAAaAPAQQAPAQAUABQAUgBAQgQQAPgPAAgbQAAgagPgSQgPgRgUAAQgVAAgPARg");
	this.shape_79.setTransform(-448.025,39.65);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f("#FFFFFF").s().p("AhJBbQgXgUAAghQAAghAYgPQAYgQAqAAIArAAIAAgCQAAgigkAAQgPAAgRAGQgRAGgMAKIgbgnQAqgfA1AAQAnAAAaAUQAYATAAArIAACIIg4AAIAAgaQgYAdggAAQgiAAgYgUgAgkAmQAAALAIAHQAKAGAPAAQAPAAAMgKQANgKAAgQIAAgLIglAAQgkAAAAAXg");
	this.shape_80.setTransform(-474.3,35.925);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("#FFFFFF").s().p("ABeCMIAAivIhMCYIgkAAIhLiYIAACvIg+AAIAAkXIBUAAIBHCZIBIiZIBVAAIAAEXg");
	this.shape_81.setTransform(-504.75,32.725);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_81},{t:this.shape_80},{t:this.shape_79},{t:this.shape_78},{t:this.shape_77},{t:this.shape_76},{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33}]},57).to({state:[]},43).to({state:[]},1).wait(20));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-695.4,-355,1127.9,655.7);


// stage content:
(lib.FS_projet_bmw = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":150});

	// timeline functions:
	this.frame_100 = function() {
		//var frequency = 60;
		//stage.enableMouseOver(frequency);
		
		// EVENT DE CLIC POUR LA SUITE
		this.next_btn.addEventListener("click", fl_ClickToGoToAndPlayFromFrame.bind(this));
		this.next_btn.addEventListener("mouseover", fl_MouseOverHandler.bind(this));
		this.next_btn.addEventListener("mouseout", fl_MouseOutHandler.bind(this));
		
		
		function fl_ClickToGoToAndPlayFromFrame()
		{
			//this.gotoAndPlay("close");
			
			var event = new Event('next');
			this.dispatchEvent(event);		
			event = null;
			
			this.next_btn.btnNext.gotoAndPlay('rollOut');
			
		}
		function fl_MouseOverHandler(){	this.next_btn.btnNext.gotoAndPlay('rollOver'); }
		function fl_MouseOutHandler(){	this.next_btn.btnNext.gotoAndPlay('rollOut'); }
		
		
		// EVENT DE CLIC POUR LE LIEN
		this.cta_mc.addEventListener("click", projectDetails.bind(this));
		this.cta_mc.addEventListener("mouseover", pDetails_MouseOverHandler.bind(this));
		this.cta_mc.addEventListener("mouseout", pDetails_MouseOutHandler.bind(this));
		
		function projectDetails()
		{	
			var event = new Event('details');
			this.dispatchEvent(event);		
			event = null;
			
			this.cta_mc.btnCta.gotoAndPlay('rollOut');
			
		}
		function pDetails_MouseOverHandler(){	this.cta_mc.btnCta.gotoAndPlay('rollOver'); }
		function pDetails_MouseOutHandler(){	this.cta_mc.btnCta.gotoAndPlay('rollOut'); }
	}
	this.frame_149 = function() {
		//PAUSE
		this.stop();
	}
	this.frame_150 = function() {
		this.textes_mc.gotoAndPlay("close");
		this.visuel_mc.gotoAndPlay("close");
		this.cta_mc.gotoAndPlay("close");
		this.video_mc.gotoAndPlay("close");
		this.next_btn.gotoAndPlay("close");
		this.lignes_mc.gotoAndPlay("close_lines");
	}
	this.frame_169 = function() {
		//this.lignes_mc.gotoAndPlay("close");
	}
	this.frame_211 = function() {
		// EVENT DE FIN D'ANIM
		var event = new Event('closed');
		this.dispatchEvent(event);		
		event = null;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(100).call(this.frame_100).wait(49).call(this.frame_149).wait(1).call(this.frame_150).wait(19).call(this.frame_169).wait(42).call(this.frame_211).wait(1));

	// BLOC INFOS TEXTE
	this.textes_mc = new lib.blocInfostextes();
	this.textes_mc.name = "textes_mc";
	this.textes_mc.parent = this;
	this.textes_mc.setTransform(858.2,182,1,1,0,0,0,222.1,-164);
	this.textes_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.textes_mc).wait(17).to({_off:false},0).wait(195));

	// masque VISUEL
	this.visuel_mc = new lib.bloc_visuel();
	this.visuel_mc.name = "visuel_mc";
	this.visuel_mc.parent = this;
	this.visuel_mc.setTransform(1192.55,391.5,1,1,0,0,0,552.5,26.5);
	this.visuel_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.visuel_mc).wait(17).to({_off:false},0).wait(195));

	// CTA NEXT
	this.next_btn = new lib.bloc_NEXT_btn();
	this.next_btn.name = "next_btn";
	this.next_btn.parent = this;
	this.next_btn.setTransform(753.2,786.9,1,1,0,0,0,-340.5,403.9);
	this.next_btn._off = true;

	this.timeline.addTween(cjs.Tween.get(this.next_btn).wait(89).to({_off:false},0).wait(123));

	// CTA DETAIL
	this.cta_mc = new lib.bloc_CTA();
	this.cta_mc.name = "cta_mc";
	this.cta_mc.parent = this;
	this.cta_mc.setTransform(299.5,787,1,1,0,0,0,-340.5,403.9);
	this.cta_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.cta_mc).wait(99).to({_off:false},0).to({_off:true},74).wait(39));

	// VIDEO
	this.video_mc = new lib.bloc_video();
	this.video_mc.name = "video_mc";
	this.video_mc.parent = this;
	this.video_mc.setTransform(866.7,1147.15,1,1,0,0,0,227.5,764.9);
	this.video_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.video_mc).wait(94).to({_off:false},0).to({_off:true},79).wait(39));

	// lignes
	this.lignes_mc = new lib.lignes_all();
	this.lignes_mc.name = "lignes_mc";
	this.lignes_mc.parent = this;
	this.lignes_mc.setTransform(640,384.3,1,1,0,0,0,0,406.3);

	this.timeline.addTween(cjs.Tween.get(this.lignes_mc).wait(212));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(825.5,361.5,269.0999999999999,1101.8);
// library properties:
lib.properties = {
	id: '6D3BA6291E1AA946841E9675383BA836',
	width: 1280,
	height: 768,
	fps: 60,
	color: "#000000",
	opacity: 1.00,
	manifest: [
		{src:"images/bmw.png", id:"bmw"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['6D3BA6291E1AA946841E9675383BA836'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;