(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.casquetteHeadict = function() {
	this.initialize(img.casquetteHeadict);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,844,450);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.video = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.008)").s().p("EgtTBLAMAAAiV/MBanAAAMAAACV/g");
	this.shape.setTransform(290,480);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.video, new cjs.Rectangle(0,0,580,960), null);


(lib.txt_hashtag = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgiAmIAJgRQAOAKALAAQAFgBAEgDQAFgDAAgGQAAgGgFgEQgFgDgHAAQgFAAgJACIAAgPIASgVIgdAAIAAgTIA7AAIAAAPIgVAXQAMABAGAIQAHAIAAAKQAAAPgKAJQgLAJgPAAQgRAAgQgMg");
	this.shape.setTransform(95.6,52.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AghAKIAAgTIBDggIAAAVIgsAUIAsAVIAAAVg");
	this.shape_1.setTransform(87.1,52.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AAVAxIgVggIgUAgIgaAAIAhgxIgfgwIAaAAIASAdIASgdIAbAAIgfAvIAhAyg");
	this.shape_2.setTransform(70.9,52.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgjAkQgPgPAAgVQAAgUAPgPQAOgPAVAAQAWAAAPAPQAOAPAAAUQAAAVgOAPQgPAPgWAAQgVAAgOgPgAgTgVQgJAJAAAMQAAANAJAKQAIAIALABQAMgBAJgIQAIgKAAgNQAAgMgIgJQgJgJgMAAQgLAAgIAJg");
	this.shape_3.setTransform(60.2,52.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgmAxIAAhhIAmAAQAJAAAIACQAGADAEAEQAHAHAAAKQgBALgGAGIgEACIgEACQAKABAFAGQAFAHAAAJQAAAKgHAIQgIAJgUAAgAgQAeIAPAAQAIAAAFgCQAEgCABgHQAAgHgGgCQgEgCgKAAIgNAAgAgQgIIAKAAQAIAAAFgCQAEgCAAgHQAAgGgEgCQgEgCgJgBIgKAAg");
	this.shape_4.setTransform(49.9,52.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBGAAIAAATIgwAAIAAAVIAqAAIAAASIgqAAIAAAUIAxAAIAAATg");
	this.shape_5.setTransform(40.5,52.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAUAxIAAgmIgmAAIAAAmIgWAAIAAhhIAWAAIAAApIAmAAIAAgpIAVAAIAABhg");
	this.shape_6.setTransform(30.3,52.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgKAxIAAhOIgbAAIAAgTIBLAAIAAATIgbAAIAABOg");
	this.shape_7.setTransform(20.8,52.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AAXAxIgtg9IAAA9IgWAAIAAhhIAUAAIAvA+IAAg+IAWAAIAABhg");
	this.shape_8.setTransform(10.8,52.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgKAxIAAhhIAVAAIAABhg");
	this.shape_9.setTransform(2.8,52.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgKAxIAAgmIgig7IAYAAIAUAkIAVgkIAYAAIgiA7IAAAmg");
	this.shape_10.setTransform(-7.6,52.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgmAxIAAhhIAmAAQAJAAAIACQAGADAEAEQAHAHgBAKQAAALgGAGIgEACIgDACQAJABAFAGQAFAHAAAJQAAAKgHAIQgIAJgUAAgAgQAeIAPAAQAIAAAFgCQAFgCAAgHQgBgHgFgCQgEgCgKAAIgNAAgAgQgIIAKAAQAJAAAEgCQAEgCAAgHQAAgGgEgCQgEgCgJgBIgKAAg");
	this.shape_11.setTransform(-16.4,52.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgsAxIAAhhIAjAAQAaAAAOANQAOAMgBAXQABAWgOAOQgOANgbAAgAgVAeIANAAQAOgBAJgHQAHgIABgOQgBgOgHgHQgJgIgQAAIgLAAg");
	this.shape_12.setTransform(-30.3,52.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgiAmIAJgRQAOAKALAAQAFgBAEgDQAFgDAAgGQAAgGgFgEQgFgDgHAAQgFAAgJACIAAgPIASgVIgeAAIAAgTIA8AAIAAAPIgVAXQAMABAGAIQAHAIAAAKQAAAPgLAJQgKAJgPAAQgRAAgQgMg");
	this.shape_13.setTransform(-40.2,52.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgTIANAAIADgRIgPAAIADgUIAPAAIAEgWIAUAAIgDAWIAMAAIADgWIAWAAIgEAWIAOAAIgDAUIgPAAIgDARIARAAIgEATIgQAAIgDAVgAgHAKIALAAIAEgRIgMAAg");
	this.shape_14.setTransform(-49.5,52.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBGAAIAAATIgwAAIAAAVIAqAAIAAASIgqAAIAAATIAxAAIAAAUg");
	this.shape_15.setTransform(54.7,31.7);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgeAkQgPgOAAgWQAAgUAPgPQAPgOAVAAQAYgBAQATIgNAPQgLgMgPAAQgMgBgIAJQgJAHAAANQgBANAJAJQAIAIALAAQAQAAALgNIANAPQgQATgXAAQgVgBgPgOg");
	this.shape_16.setTransform(44.8,31.7);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AAQAxIgVgfIgPAAIAAAfIgWAAIAAhhIAlAAQAWAAAKAIQAKAHAAASQAAAWgTAHIAZAjgAgUAAIAQAAQAKAAAEgEQAEgEAAgHQAAgIgEgDQgEgDgKAAIgQAAg");
	this.shape_17.setTransform(34.9,31.7);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBGAAIAAATIgwAAIAAAVIAqAAIAAASIgqAAIAAATIAxAAIAAAUg");
	this.shape_18.setTransform(25.1,31.7);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AAhAxIAAg9IgbA1IgMAAIgag1IAAA9IgWAAIAAhhIAeAAIAYA1IAZg1IAeAAIAABhg");
	this.shape_19.setTransform(13.6,31.7);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AAhAxIAAg9IgbA1IgMAAIgag1IAAA9IgWAAIAAhhIAeAAIAYA1IAZg1IAeAAIAABhg");
	this.shape_20.setTransform(0.2,31.7);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgjAkQgPgPAAgVQAAgVAPgOQAOgOAVAAQAWAAAPAOQAOAOAAAVQAAAVgOAPQgPAPgWAAQgVAAgOgPgAgTgVQgJAJAAAMQAAANAJAKQAIAIALABQAMgBAJgIQAIgKAAgNQAAgMgIgJQgJgJgMAAQgLAAgIAJg");
	this.shape_21.setTransform(-12.2,31.7);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgeAkQgPgOAAgWQAAgUAPgPQAPgOAUAAQAZgBAQATIgOAPQgKgMgQAAQgLgBgIAJQgKAHABANQAAANAIAJQAJAIAKAAQAQAAALgNIANAPQgRATgWAAQgVgBgPgOg");
	this.shape_22.setTransform(-23,31.7);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgYAKIAAgTIAxAAIAAATg");
	this.shape_23.setTransform(-31.7,32.4);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBFAAIAAATIgvAAIAAAVIAqAAIAAASIgqAAIAAATIAxAAIAAAUg");
	this.shape_24.setTransform(-39.4,31.7);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgTIANAAIADgRIgPAAIADgTIAPAAIAEgXIAUAAIgDAXIAMAAIADgXIAWAAIgEAXIAOAAIgDATIgPAAIgDARIARAAIgEATIgQAAIgDAVgAgHAKIALAAIAEgRIgMAAg");
	this.shape_25.setTransform(-49.5,31.6);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgfAxIAAhhIAWAAIAABNIApAAIAAAUg");
	this.shape_26.setTransform(3.7,10.7);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgeAkQgOgOAAgWQgBgUAPgPQAPgOAUAAQAWAAAPANIgMARQgGgGgFgDQgGgBgGAAQgMAAgJAIQgIAIAAANQAAAOAIAJQAJAIAJAAQAMAAAHgFIAAgaIAXAAIAAAiQgPAQgaABQgUgBgPgOg");
	this.shape_27.setTransform(-6,10.6);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AgmAxIAAhhIAmAAQAJAAAHADQAIACADAEQAGAHAAAKQABALgIAGIgDACIgDACQAIABAGAHQAFAFAAAKQAAAKgHAIQgIAJgUAAgAgQAeIAPAAQAIABAFgDQAFgCgBgHQAAgGgEgDQgGgCgJAAIgNAAgAgQgIIAKAAQAIAAAFgCQAEgCAAgGQAAgHgEgCQgEgDgIAAIgLAAg");
	this.shape_28.setTransform(-15.7,10.7);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBFAAIAAAUIgvAAIAAAUIArAAIAAASIgrAAIAAATIAxAAIAAAUg");
	this.shape_29.setTransform(-25.2,10.7);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AAVAxIgVhDIgUBDIgQAAIgjhhIAYAAIATA4IASg4IAVAAIASA4IATg4IAYAAIgiBhg");
	this.shape_30.setTransform(-37.1,10.7);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgTIANAAIADgRIgPAAIADgTIAPAAIAEgXIAUAAIgDAXIAMAAIADgXIAWAAIgEAXIAOAAIgDATIgPAAIgDARIARAAIgEATIgQAAIgDAVgAgHAKIALAAIAEgRIgMAAg");
	this.shape_31.setTransform(-49.5,10.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.txt_hashtag, new cjs.Rectangle(-56.7,0,162.4,63.2), null);


(lib.project_illustration_placeHolder = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.casquetteHeadict();
	this.instance.parent = this;
	this.instance.setTransform(-115.6,-65.1,0.64,0.64,50.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.project_illustration_placeHolder, new cjs.Rectangle(-336.6,-65.1,567,599), null);


(lib.masqueTexte_1_ligne = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("EgicAEJIAAoRMBE5AAAIAAIRg");
	this.shape.setTransform(0,24.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.masqueTexte_1_ligne, new cjs.Rectangle(-220.5,-1.9,441,53), null);


(lib.masque_generique = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF8D72").s().p("AsQCqIAAlTIYhAAIAAFTg");
	this.shape.setTransform(78.5,17);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.masque_generique, new cjs.Rectangle(0,0,157,34), null);


(lib.ligne = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(49));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#333333").ss(1,1,1).p("EAAAg/bMAAAB+3");
	this.shape.setTransform(0,406);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#333333").ss(1,1,1).p("EAA/A/cQhDv0ggwoQgdwxAExFQAEwXAhwHQAfufA4to");
	this.shape_1.setTransform(-6,406);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#333333").ss(1,1,1).p("EABwA/cQh4vfg5w8Qg0wqAHxQQAHweA7wDQA5uxBjtQ");
	this.shape_2.setTransform(-10.7,406);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#333333").ss(1,1,1).p("EACVA/cQigvPhLxLQhHwmAKxYQAJwjBPv/QBMvACEs9");
	this.shape_3.setTransform(-14.2,406);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#333333").ss(1,1,1).p("EACwA/cQi+vDhZxWQhTwjAMxeQAKwnBev8QBavKCcsw");
	this.shape_4.setTransform(-16.9,406);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#333333").ss(1,1,1).p("EADEA/cQjTu7hjxdQhdwhANxhQAMwrBov6QBkvSCusm");
	this.shape_5.setTransform(-18.8,406);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#333333").ss(1,1,1).p("EADSA/cQjiu1hqxjQhjwfAOxlQAMwsBvv5QBsvXC6sf");
	this.shape_6.setTransform(-20.1,406);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#333333").ss(1,1,1).p("EADbA/cQjsuxhuxnQhnweAOxnQANwtB0v4QBwvaDCsb");
	this.shape_7.setTransform(-20.9,406);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#333333").ss(1,1,1).p("EADgA/cQjyuuhxxpQhpwdAPxpQANwuB3v3QByvdDHsY");
	this.shape_8.setTransform(-21.4,406);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#333333").ss(1,1,1).p("EADjA/cQj1uthyxqQhrwdAPxpQANwvB5v3QB0vdDJsX");
	this.shape_9.setTransform(-21.7,406);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#333333").ss(1,1,1).p("EADkA/cQj2uthzxqQhswdAPxpQANwvB6v3QB1veDKsW");
	this.shape_10.setTransform(-21.8,406);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#333333").ss(1,1,1).p("EADlA/cQj3ush0xrQhrwdAPxpQANwvB5v3QB2veDLsW");
	this.shape_11.setTransform(-21.9,406);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#333333").ss(1,1,1).p("EADlA/cQj3ushzxrQhswdAPxpQANwvB6v3QB1veDLsW");
	this.shape_12.setTransform(-21.9,406);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#333333").ss(1,1,1).p("EADlg/bQjLMWh1PeQh6P3gNQvQgPRpBsQdQBzRrD3Os");
	this.shape_13.setTransform(-21.9,406);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#333333").ss(1,1,1).p("EADlA/cQj3ushzxrQhswdAPxpQANwvB5v3QB2veDLsW");
	this.shape_14.setTransform(-21.9,406);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#333333").ss(1,1,1).p("EADlA/cQj3ushzxrQhswdAPxpQAOwvB5v3QB1veDLsW");
	this.shape_15.setTransform(-21.9,406);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#333333").ss(1,1,1).p("EADkA/cQj2uthzxqQhrwdAPxpQANwvB5v3QB1veDKsW");
	this.shape_16.setTransform(-21.8,406);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#333333").ss(1,1,1).p("EADjA/cQj0uthzxqQhqwdAOxpQAOwvB4v2QB0veDJsX");
	this.shape_17.setTransform(-21.7,406);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#333333").ss(1,1,1).p("EADhA/cQjyuuhyxpQhpwdAOxpQANwuB4v3QBzvdDHsY");
	this.shape_18.setTransform(-21.5,406);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#333333").ss(1,1,1).p("EADeA/cQjvuvhwxoQhoweAOxoQANwuB2v3QBxvcDFsZ");
	this.shape_19.setTransform(-21.2,406);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#333333").ss(1,1,1).p("EADaA/cQjruxhuxnQhnweAPxmQAMwuB0v4QBwvaDBsb");
	this.shape_20.setTransform(-20.9,406);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#333333").ss(1,1,1).p("EADWA/cQjnuzhrxlQhlweAOxmQANwsBxv5QBtvZC+sd");
	this.shape_21.setTransform(-20.4,406);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#333333").ss(1,1,1).p("EADQA/cQjgu2hpxiQhiwfAOxlQAMwsBuv4QBrvXC4sg");
	this.shape_22.setTransform(-19.9,406);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#333333").ss(1,1,1).p("EADJA/cQjZu4hlxgQhewgANxjQAMwrBqv6QBnvTCysk");
	this.shape_23.setTransform(-19.2,406);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#333333").ss(1,1,1).p("EADAA/cQjPu8hhxcQhawhAMxhQAMwqBlv7QBjvQCqso");
	this.shape_24.setTransform(-18.4,406);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#333333").ss(1,1,1).p("EAC2A/cQjEvAhcxZQhWwiAMxfQALwoBhv8QBdvMChst");
	this.shape_25.setTransform(-17.4,406);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#333333").ss(1,1,1).p("EACqA/cQi3vFhWxUQhRwjAMxdQAKwnBav8QBXvICXsz");
	this.shape_26.setTransform(-16.3,406);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#333333").ss(1,1,1).p("EACdA/cQipvLhPxPQhKwkAKxaQAKwlBTv+QBQvDCLs5");
	this.shape_27.setTransform(-15,406);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#333333").ss(1,1,1).p("EACOA/cQiYvShIxIQhCwnAJxWQAIwiBLwAQBJu9B9tB");
	this.shape_28.setTransform(-13.6,406);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#333333").ss(1,1,1).p("EAB8A/cQiFvZg/xCQg6woAIxSQAHwhBCwBQA/u2ButK");
	this.shape_29.setTransform(-11.9,406);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#333333").ss(1,1,1).p("EABpA/cQhxvig1w5QgxwrAHxPQAGwdA3wDQA2uvBdtT");
	this.shape_30.setTransform(-10,406);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#333333").ss(1,1,1).p("EABYA/cQhevpgswyQgqwuAGxKQAFwbAvwFQAsuoBOtc");
	this.shape_31.setTransform(-8.4,406);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#333333").ss(1,1,1).p("EABIA/cQhNvwgkwsQgiwvAFxHQAEwYAmwHQAkujBAtj");
	this.shape_32.setTransform(-6.9,406);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#333333").ss(1,1,1).p("EAA7A/cQg/v2gdwmQgcwxAExEQADwXAgwIQAdudA0tq");
	this.shape_33.setTransform(-5.6,406);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#333333").ss(1,1,1).p("EAAvA/cQgyv7gYwhQgWwzADxBQADwVAZwJQAXuZAqtw");
	this.shape_34.setTransform(-4.5,406);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#333333").ss(1,1,1).p("EAAlA/cQgnv/gTweQgRwzACxAQACwTAUwKQASuVAht1");
	this.shape_35.setTransform(-3.5,406);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#333333").ss(1,1,1).p("EAAcA/cQgdwDgPwaQgNw1ACw9QABwSAQwLQANuSAZt5");
	this.shape_36.setTransform(-2.7,406);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#333333").ss(1,1,1).p("EAAVA/cQgWwGgLwXQgKw2ACw7QABwSALwMQAKuPATt8");
	this.shape_37.setTransform(-2,406);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#333333").ss(1,1,1).p("EAAQA/cQgQwIgIwVQgHw3ABw6QABwRAIwMQAHuNAOt/");
	this.shape_38.setTransform(-1.5,406);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#333333").ss(1,1,1).p("EAALA/cQgLwLgFwSQgFw3AAw6QABwQAGwMQAFuMAJuB");
	this.shape_39.setTransform(-1.1,406);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#333333").ss(1,1,1).p("EAAIA/cQgIwMgDwSQgEw3ABw5QAAwPAEwNQADuKAHuD");
	this.shape_40.setTransform(-0.7,406);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#333333").ss(1,1,1).p("EAAFA/cQgFwNgBwRQgDw3ABw5QAAwOACwOQACuJAEuE");
	this.shape_41.setTransform(-0.5,406);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#333333").ss(1,1,1).p("EAADA/cQgDwOAAwQQgCw3ABw4QAAwOABwPQAAuIADuF");
	this.shape_42.setTransform(-0.3,406);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#333333").ss(1,1,1).p("EAABA/cQgBwOAAwQQgBw4ABw3QAAwOAAwOQAAuIABuG");
	this.shape_43.setTransform(-0.1,406);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#333333").ss(1,1,1).p("EAAAA/cQAAwPAAwPQAAw4AAw3QAAwOAAwOQAAuIAAuG");
	this.shape_44.setTransform(0,406);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#333333").ss(1,1,1).p("EAAAA/cQAAwPAAwPQAAw4AAw3QAAwOAAwOQAAuHAAuH");
	this.shape_45.setTransform(0,406);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1,-1,2,814);


(lib.lettres_FAT = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF8D72").s().p("ADUFeIhCiXIkjAAIhBCXIinAAIEvq6ICVAAIEuK6gAhXA+ICvAAIhYjLg");
	this.shape.setTransform(13.1,311.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF8D72").s().p("AkBFeIAAq7IH3AAIAACMIlaAAIAACQIE3AAIAACDIk3AAIAACSIFlAAIAACKg");
	this.shape_1.setTransform(10.2,187.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF8D72").s().p("ACLFdIAAkSIkVAAIAAESIicAAIAAq5ICcAAIAAEjIEVAAIAAkjICcAAIAAK5g");
	this.shape_2.setTransform(-62.8,63.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.lettres_FAT, new cjs.Rectangle(-102.9,0,167.5,373.7), null);


(lib.hastaggraphicbranche = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AjQAuIAShbIDlAAICqAAIgSBbg");
	this.shape.setTransform(0,4.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.hastaggraphicbranche, new cjs.Rectangle(-20.8,0,41.7,9.2), null);


(lib.Btn_cliquable_area = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF8D72").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape.setTransform(0,30);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Btn_cliquable_area, new cjs.Rectangle(-113,0,226,60), null);


(lib.prject_illustration = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.project_illustration_placeHolder();
	this.instance.parent = this;
	this.instance.setTransform(0,316.5,1,1,0,0,0,0,316.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:-0.1,regY:316.6,rotation:-1.5,x:-9.6,y:335.9},59,cjs.Ease.quadInOut).to({regX:0,regY:316.5,rotation:0,x:0,y:316.5},60,cjs.Ease.quadInOut).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-336.6,-65.1,567,599);


(lib.masqueTexte = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// masqueTexte_1_ligne
	this.instance = new lib.masqueTexte_1_ligne();
	this.instance.parent = this;
	this.instance.setTransform(-218.8,191.5,0.008,1,0,0,0,0,25.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(6).to({_off:false},0).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.01,x:218.8},14).wait(1));

	// masqueTexte_1_ligne
	this.instance_1 = new lib.masqueTexte_1_ligne();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-218.8,136.1,0.008,1,0,0,0,0,25.5);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.01,x:218.8},14).to({_off:true},1).wait(2));

	// masqueTexte_1_ligne
	this.instance_2 = new lib.masqueTexte_1_ligne();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-218.8,80.8,0.008,1,0,0,0,0,25.5);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(2).to({_off:false},0).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.01,x:218.8},14).to({_off:true},1).wait(4));

	// masqueTexte_1_ligne
	this.instance_3 = new lib.masqueTexte_1_ligne();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-218.8,25.5,0.008,1,0,0,0,0,25.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.01,x:218.8},14).to({_off:true},1).wait(6));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-220.5,-1.9,3.5,53);


(lib.lignes_all = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{close:114,close_lines:157});

	// timeline functions:
	this.frame_113 = function() {
		this.stop();
	}
	this.frame_156 = function() {
		this.stop();
	}
	this.frame_174 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(113).call(this.frame_113).wait(43).call(this.frame_156).wait(18).call(this.frame_174).wait(1));

	// ligne
	this.instance = new lib.ligne();
	this.instance.parent = this;
	this.instance.setTransform(-454,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(114).to({x:-883},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:-454},0).wait(18));

	// ligne
	this.instance_1 = new lib.ligne();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-227,406.5,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(114).to({x:-883.2},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:-227},0).wait(18));

	// ligne
	this.instance_2 = new lib.ligne();
	this.instance_2.parent = this;
	this.instance_2.setTransform(0,406.5,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(114).to({x:-883.4},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:0},0).wait(18));

	// ligne
	this.instance_3 = new lib.ligne();
	this.instance_3.parent = this;
	this.instance_3.setTransform(227,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(93).to({y:322},20,cjs.Ease.quartInOut).wait(1).to({y:323},0).to({x:-883,y:406},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:227,y:322},0).to({y:406},16,cjs.Ease.quartInOut).wait(2));

	// ligne
	this.instance_4 = new lib.ligne();
	this.instance_4.parent = this;
	this.instance_4.setTransform(454.1,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(114).to({x:-883.4},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:454.1},0).wait(18));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-454.5,-0.5,909.2,813.5);


(lib.hashtag_graphic = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(64));

	// hastag graphic branche
	this.instance = new lib.hastaggraphicbranche();
	this.instance.parent = this;
	this.instance.setTransform(37.4,51.9,1.084,0.995,0,-78.3,102.1,0.1,4.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({x:6.5,y:50.1},17,cjs.Ease.quartOut).to({_off:true},5).wait(3).to({_off:false},0).to({_off:true},4).wait(2).to({_off:false,x:26.5},0).to({_off:true},5).wait(28));

	// hastag graphic branche
	this.instance_1 = new lib.hastaggraphicbranche();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-41.6,49.9,1.084,0.995,0,-78.3,102.1,0.1,4.6);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off:false},0).to({x:-7.4,y:49.8},18,cjs.Ease.quartOut).to({_off:true},4).wait(5).to({_off:false},0).to({_off:true},4).wait(31));

	// hastag graphic branche
	this.instance_2 = new lib.hastaggraphicbranche();
	this.instance_2.parent = this;
	this.instance_2.setTransform(2.7,21.7,1,1,0,0,0,0,4.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({x:-2.3,y:58.7},13,cjs.Ease.quartOut).to({_off:true},11).wait(3).to({_off:false,scaleX:2.33,scaleY:1.03,skewX:-12.8,x:25.5},0).to({_off:true},4).wait(33));

	// hastag graphic branche
	this.instance_3 = new lib.hastaggraphicbranche();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-7.3,77.7,1,1,0,0,0,0,4.6);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({x:0.7,y:41.7},14,cjs.Ease.quartOut).to({_off:true},4).wait(7).to({_off:false,regX:0.1,scaleX:1.98,scaleY:1.01,skewX:-9.8,x:19.9,y:49.7},0).to({_off:true},4).wait(4).to({_off:false,regX:0,scaleX:1,scaleY:1,skewX:0,x:0.7,y:41.7},0).to({_off:true},4).wait(21));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-18.1,17.1,64.6,57.7);


(lib.Btn_next = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{rollOver:55,rollOut:70});

	// timeline functions:
	this.frame_54 = function() {
		this.stop();
	}
	this.frame_69 = function() {
		this.stop();
	}
	this.frame_84 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(54).call(this.frame_54).wait(15).call(this.frame_69).wait(15).call(this.frame_84).wait(1));

	// ZONE
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.008)").s().p("EgjTAEsIAApXMBGnAAAIAAJXg");
	this.shape.setTransform(113,29.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(85));

	// txt
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgIAqIAAhDIgYAAIAAgQIBBAAIAAAQIgYAAIAABDg");
	this.shape_1.setTransform(161.1,29.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AASAqIgSgcIgQAcIgYAAIAdgqIgagpIAWAAIAPAZIAQgZIAWAAIgaApIAcAqg");
	this.shape_2.setTransform(145.2,29.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgeAqIAAhTIA8AAIAAARIgpAAIAAARIAkAAIAAAPIgkAAIAAASIAqAAIAAAQg");
	this.shape_3.setTransform(129.3,29.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAUAqIgngzIAAAzIgTAAIAAhTIASAAIAoA1IAAg1IATAAIAABTg");
	this.shape_4.setTransform(112.3,29.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgKAQIAEgOQgEgCAAgFQAAgEADgDQADgDAEAAQAFAAADADQADADAAAEQAAAEgDAEIgIANg");
	this.shape_5.setTransform(86.5,32.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAPAqIgYghIgIAKIAAAXIgTAAIAAhTIATAAIAAAjIAegjIAYAAIgiAlIAQAWIASAYg");
	this.shape_6.setTransform(77,29.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgeAfQgNgNAAgSQAAgSANgMQANgNARAAQASAAAOANQAMAMAAASQAAASgMANQgOAMgSAAQgRAAgNgMgAgRgSQgHAIAAAKQAAALAHAIQAIAIAJAAQAKAAAIgIQAHgIAAgLQAAgKgHgIQgIgIgKAAQgJAAgIAIg");
	this.shape_7.setTransform(59.3,29.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#333333").ss(1,1,1).p("EgjOAADMBGdgAF");
	this.shape_8.setTransform(113,-0.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(85));

	// Layer_2
	this.instance = new lib.Btn_cliquable_area();
	this.instance.parent = this;
	this.instance.setTransform(113,29.5,2,1,0,0,0,0,30);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("rgba(0,0,0,0.008)").s().p("EgjTAEsIAApXMBGnAAAIAAJXg");
	this.shape_9.setTransform(113,29.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance}]},3).to({state:[{t:this.instance}]},46).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.instance}]},5).to({state:[{t:this.instance}]},14).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},14).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({alpha:0.012},46).to({_off:true},1).wait(5).to({_off:false},0).to({alpha:1},14).wait(1).to({alpha:0.012},14).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-113.5,-1.5,453,61);


(lib.Btn_detail = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"rollOver":55,"rollOut":70});

	// timeline functions:
	this.frame_54 = function() {
		this.stop();
	}
	this.frame_69 = function() {
		this.stop();
	}
	this.frame_84 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(54).call(this.frame_54).wait(15).call(this.frame_69).wait(15).call(this.frame_84).wait(1));

	// ZONE
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.008)").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape.setTransform(0,29.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(85));

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgOAyIAAhkIAdAAIAABkg");
	this.shape_1.setTransform(-85.4,29.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgyAOIAAgcIBlAAIAAAcg");
	this.shape_2.setTransform(-85.4,29.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgaAqIAAhTIASAAIAABDIAjAAIAAAQg");
	this.shape_3.setTransform(38.4,29.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgIAqIAAhTIARAAIAABTg");
	this.shape_4.setTransform(24.7,29.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AAZAqIgHgSIgjAAIgHASIgUAAIAkhTIARAAIAkBTgAgKAIIAUAAIgKgYg");
	this.shape_5.setTransform(10.2,29.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgIAqIAAhDIgYAAIAAgQIBBAAIAAAQIgYAAIAABDg");
	this.shape_6.setTransform(-5.9,29.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgeAqIAAhTIA8AAIAAARIgpAAIAAARIAkAAIAAAPIgkAAIAAASIAqAAIAAAQg");
	this.shape_7.setTransform(-21.3,29.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AglAqIAAhTIAeAAQAVAAAMALQAMALAAATQAAATgMALQgMAMgXAAgAgTAaIANAAQAMAAAGgHQAHgGAAgNQAAgLgHgHQgGgGgOgBIgLAAg");
	this.shape_8.setTransform(-37.8,29.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#333333").ss(1,1,1).p("AxpAAMAjTAAA");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(85));

	// Layer_2
	this.instance = new lib.Btn_cliquable_area();
	this.instance.parent = this;
	this.instance.setTransform(0,29.5,1,1,0,0,0,0,30);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("rgba(0,0,0,0.008)").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape_10.setTransform(0,29.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance}]},3).to({state:[{t:this.instance}]},45).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.instance}]},6).to({state:[{t:this.instance}]},14).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},13).wait(2));
	this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({alpha:0.012},45).to({_off:true},1).wait(6).to({_off:false},0).to({alpha:1},14).wait(1).to({alpha:0.012},13).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-114,-1,228,60.5);


(lib.bloc_visuel = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":63});

	// timeline functions:
	this.frame_62 = function() {
		this.stop();
	}
	this.frame_99 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(62).call(this.frame_62).wait(37).call(this.frame_99).wait(1));

	// masque VISUEL (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_20 = new cjs.Graphics().p("EgjsBDvMAAAiHdMBHZAAAMAAACHdg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(20).to({graphics:mask_graphics_20,x:-228.7,y:-19}).wait(80));

	// VISUEL
	this.instance = new lib.prject_illustration();
	this.instance.parent = this;
	this.instance.setTransform(552.6,26.5,1,1,175,0,0,0,316.5);
	this.instance.alpha = 0.012;

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:0.1,regY:316.6,rotation:354.5,x:-159.3,y:6.6,alpha:1},20,cjs.Ease.cubicOut).to({regX:0,rotation:347.3,x:-39.5},28,cjs.Ease.cubicInOut).wait(20).to({x:-719.5},30,cjs.Ease.quartInOut).to({_off:true},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(304.3,-219.2,616.8,645.8);


(lib.bloc_video = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":41});

	// timeline functions:
	this.frame_40 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(40).call(this.frame_40).wait(17));

	// masque video (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_42 = new cjs.Graphics().p("Ega8A8AMAAAh3/MA7rAAAMAAAB3/g");
	var mask_graphics_43 = new cjs.Graphics().p("EgYrA8AMAAAh3/MA3HAAAMAAAB3/g");
	var mask_graphics_44 = new cjs.Graphics().p("EgWaA8AMAAAh3/MAyiAAAMAAAB3/g");
	var mask_graphics_45 = new cjs.Graphics().p("EgUJA8AMAAAh3/MAt+AAAMAAAB3/g");
	var mask_graphics_46 = new cjs.Graphics().p("EgR4A8AMAAAh3/MApaAAAMAAAB3/g");
	var mask_graphics_47 = new cjs.Graphics().p("EgPnA8AMAAAh3/MAk1AAAMAAAB3/g");
	var mask_graphics_48 = new cjs.Graphics().p("EgNWA8AMAAAh3/MAgRAAAMAAAB3/g");
	var mask_graphics_49 = new cjs.Graphics().p("EgLFA8AMAAAh3/IbtAAMAAAB3/g");
	var mask_graphics_50 = new cjs.Graphics().p("EgI0A8AMAAAh3/IXIAAMAAAB3/g");
	var mask_graphics_51 = new cjs.Graphics().p("EgGjA8AMAAAh3/ISkAAMAAAB3/g");
	var mask_graphics_52 = new cjs.Graphics().p("EgESA8AMAAAh3/IOAAAMAAAB3/g");
	var mask_graphics_53 = new cjs.Graphics().p("EgCBA8AMAAAh3/IJbAAMAAAB3/g");
	var mask_graphics_54 = new cjs.Graphics().p("EAAPA8AMAAAh3/IE4AAMAAAB3/g");
	var mask_graphics_55 = new cjs.Graphics().p("EACgA8AMAAAh3/IAUAAMAAAB3/g");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(42).to({graphics:mask_graphics_42,x:209.5,y:0}).wait(1).to({graphics:mask_graphics_43,x:194.8,y:0}).wait(1).to({graphics:mask_graphics_44,x:180,y:0}).wait(1).to({graphics:mask_graphics_45,x:165.3,y:0}).wait(1).to({graphics:mask_graphics_46,x:150.6,y:0}).wait(1).to({graphics:mask_graphics_47,x:135.8,y:0}).wait(1).to({graphics:mask_graphics_48,x:121.1,y:0}).wait(1).to({graphics:mask_graphics_49,x:106.4,y:0}).wait(1).to({graphics:mask_graphics_50,x:91.6,y:0}).wait(1).to({graphics:mask_graphics_51,x:76.9,y:0}).wait(1).to({graphics:mask_graphics_52,x:62.2,y:0}).wait(1).to({graphics:mask_graphics_53,x:47.4,y:0}).wait(1).to({graphics:mask_graphics_54,x:32.7,y:0}).wait(1).to({graphics:mask_graphics_55,x:18,y:0}).wait(1).to({graphics:null,x:0,y:0}).wait(1));

	// VIDEO
	this.videoContainer = new lib.video();
	this.videoContainer.name = "videoContainer";
	this.videoContainer.parent = this;
	this.videoContainer.setTransform(36.7,765,0.659,0.659,0,0,0,0.3,480.2);

	var maskedShapeInstanceList = [this.videoContainer];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.videoContainer).to({y:-33},40,cjs.Ease.quartInOut).to({_off:true},16).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(36.5,448.7,382.1,632.3);


(lib.bloc_NEXT_btn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":20});

	// timeline functions:
	this.frame_19 = function() {
		this.stop();
	}
	this.frame_36 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(19).call(this.frame_19).wait(17).call(this.frame_36).wait(1));

	// CTA DETAIL
	this.btnNext = new lib.Btn_next();
	this.btnNext.name = "btnNext";
	this.btnNext.parent = this;
	this.btnNext.setTransform(-340.5,411.5,1,1,0,0,0,0,26.5);

	this.timeline.addTween(cjs.Tween.get(this.btnNext).to({y:350.5},19,cjs.Ease.quartInOut).wait(2).to({y:411.5},14,cjs.Ease.quartInOut).to({_off:true},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-453.5,384,452,60.5);


(lib.bloc_hashtag = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_61 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(61).call(this.frame_61).wait(2));

	// Layer_1
	this.instance = new lib.hashtag_graphic("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(-456,243.2,2.317,2.317,0,0,0,-0.1,50.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true},61).wait(2));

	// Layer_5 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_28 = new cjs.Graphics().p("AtWBpIAAjRIatAAIAADRg");
	var mask_graphics_32 = new cjs.Graphics().p("AtWDSIAAmjIatAAIAAGjg");
	var mask_graphics_40 = new cjs.Graphics().p("AtWGYIAAsvIatAAIAAMvg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(28).to({graphics:mask_graphics_28,x:-400.4,y:265.5}).wait(4).to({graphics:mask_graphics_32,x:-400.4,y:255}).wait(8).to({graphics:mask_graphics_40,x:-400.4,y:255.9}).wait(23));

	// Layer_3
	this.instance_1 = new lib.txt_hashtag();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-421.5,244.1,1,1,0,0,0,0,31.6);
	this.instance_1._off = true;

	var maskedShapeInstanceList = [this.instance_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(28).to({_off:false},0).to({_off:true},34).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-497.8,166.1,149.7,129.4);


(lib.bloc_CTA = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":25});

	// timeline functions:
	this.frame_24 = function() {
		this.stop();
	}
	this.frame_41 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(24).call(this.frame_24).wait(17).call(this.frame_41).wait(1));

	// CTA DETAIL
	this.btnCta = new lib.Btn_detail();
	this.btnCta.name = "btnCta";
	this.btnCta.parent = this;
	this.btnCta.setTransform(-340.5,411.5,1,1,0,0,0,0,26.5);

	this.timeline.addTween(cjs.Tween.get(this.btnCta).to({y:350.5},24,cjs.Ease.quartInOut).wait(2).to({y:411.5},14,cjs.Ease.quartInOut).to({_off:true},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-454,384.5,227,60);


(lib.blocInfostextes = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":80});

	// timeline functions:
	this.frame_79 = function() {
		this.stop();
	}
	this.frame_120 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(79).call(this.frame_79).wait(41).call(this.frame_120).wait(1));

	// Bloc Hashtag
	this.instance = new lib.bloc_hashtag();
	this.instance.parent = this;
	this.instance.setTransform(-391.5,261.2,0.9,0.9,0,0,0,-412.9,244.7);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(62).to({_off:false},0).to({_off:true},18).wait(41));

	// masque Titre
	this.instance_1 = new lib.masque_generique();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-527.9,-13.9,0.007,0.471,0,0,0,0,17.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(34).to({_off:false},0).to({regX:0.5,scaleX:1.92,x:-527.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.01,x:-228.9},10,cjs.Ease.quartInOut).to({_off:true},1).wait(26).to({_off:false},0).to({regX:0.5,scaleX:1.92,x:-527.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.01,x:-528.7},10,cjs.Ease.quartInOut).to({_off:true},1).wait(21));

	// _NOM-du-CLIENT
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF8D72").s().p("AgMA4IAAhZIgfAAIAAgWIBXAAIAAAWIggAAIAABZg");
	this.shape.setTransform(-385.7,-14.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF8D72").s().p("AgiApQgRgQgBgYQABgZARgQQAQgRAZAAQAbAAATAVIgQASQgMgPgRAAQgNAAgLAJQgKAJABAPQAAAPAJAKQAJAKAMAAQATAAAMgPIAQARQgUAVgZAAQgZAAgQgRg");
	this.shape_1.setTransform(-408.3,-14.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF8D72").s().p("AgMA4IAAhvIAZAAIAABvg");
	this.shape_2.setTransform(-428.8,-14.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF8D72").s().p("AgyA4IAAhvIAoAAQAcAAARAPQAPAOAAAaQAAAZgPAQQgPAPggAAgAgYAiIAPAAQAQAAAKgJQAIgJABgQQgBgPgIgJQgKgJgSAAIgNAAg");
	this.shape_3.setTransform(-449.3,-14.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FF8D72").s().p("AAiA4IgLgYIguAAIgJAYIgbAAIAwhvIAXAAIAwBvgAgNAKIAbAAIgOgfg");
	this.shape_4.setTransform(-473.7,-14.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FF8D72").s().p("AgoA4IAAhvIBQAAIAAAXIg3AAIAAAWIAxAAIAAAUIgxAAIAAAYIA4AAIAAAWg");
	this.shape_5.setTransform(-496.6,-14.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FF8D72").s().p("AAWA4IAAgsIgrAAIAAAsIgZAAIAAhvIAZAAIAAAvIArAAIAAgvIAZAAIAABvg");
	this.shape_6.setTransform(-520.2,-14.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},34).to({state:[]},55).to({state:[]},1).wait(31));

	// masque Titre (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_17 = new cjs.Graphics().p("EgpOArJMAAAhWRMAvJAAAMAAABWRg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(17).to({graphics:mask_graphics_17,x:-263.9,y:-6.1}).wait(24).to({graphics:null,x:0,y:0}).wait(80));

	// _LETTRES_ANARCHIQUES
	this.instance_2 = new lib.lettres_FAT();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-433.8,-73,1,1,0,0,0,0,186.8);
	this.instance_2._off = true;

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FF8D72").s().p("AhNFeIAAozIjGAAIAAiHIInAAIAACHIjGAAIAAIzg");
	this.shape_7.setTransform(-259.2,7.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FF8D72").s().p("AjeEDQhphlAAicQAAibBrhnQBrhpCYAAQCtAABzCEIhhBuQhJhchvAAQhXgBg+A6Qg/A6AABgQABBhA7A8QA7A6BTAAQByABBKhcIBkBoQh4CFihAAQihABhohng");
	this.shape_8.setTransform(-337.6,7.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FF8D72").s().p("AhNFeIAAq6ICbAAIAAK6g");
	this.shape_9.setTransform(-403.2,7.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FF8D72").s().p("AixBCIAAiDIFjAAIAACDg");
	this.shape_10.setTransform(-360.4,-111.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FF8D72").s().p("AixBCIAAiDIFjAAIAACDg");
	this.shape_11.setTransform(-421.4,-111.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FF8D72").s().p("Ak8FdIAAq5ID3AAQC6AABkBbQBkBbAACjQAACihiBgQhhBejHAAgAigDTIBjAAQBqAAA5g2QA6g2AAhmQAAhmg6g4Qg5g4h2ABIhXAAg");
	this.shape_12.setTransform(-494.5,-116);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FF8D72").s().p("AhNFdIAAoyIjGAAIAAiHIInAAIAACHIjGAAIAAIyg");
	this.shape_13.setTransform(-219.6,106.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FF8D72").s().p("AjeEEQhohngBibQAAibBrhnQBrhoCYgBQCtAABzCEIhhBuQhJhchuAAQhYAAg+A5Qg/A5AABhQAABhA8A8QA6A6BUAAQByABBKhcIBkBnQh3CGiiAAQihABhohmg");
	this.shape_14.setTransform(-435.8,106.4);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FF8D72").s().p("AhNFdIAAq5ICbAAIAAK5g");
	this.shape_15.setTransform(-501.4,106.8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FF8D72").s().p("Ak8FeIAAq7ID3AAQC6AABkBcQBkBbAACjQAACjhiBeQhhBgjHAAgAigDTIBjAAQBqAAA5g2QA6g1AAhnQAAhng6g3Qg5g3h2gBIhXAAg");
	this.shape_16.setTransform(-300.4,-141);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FF8D72").s().p("Ak8FeIAAq7ID3AAQC6AABkBcQBkBbAACjQAACjhiBeQhhBgjHAAgAigDTIBjAAQBqAAA5g2QA6g1AAhnQAAhng6g3Qg5g3h2gBIhXAAg");
	this.shape_17.setTransform(-389.3,-141);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FF8D72").s().p("ACLFeIAAkUIkVAAIAAEUIicAAIAAq7ICcAAIAAEkIEVAAIAAkkICcAAIAAK7g");
	this.shape_18.setTransform(-479.7,-141);

	var maskedShapeInstanceList = [this.instance_2,this.shape_7,this.shape_8,this.shape_9,this.shape_10,this.shape_11,this.shape_12,this.shape_13,this.shape_14,this.shape_15,this.shape_16,this.shape_17,this.shape_18];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_2}]},17).to({state:[{t:this.instance_2}]},14).to({state:[{t:this.shape_12},{t:this.shape_11},{t:this.shape_10,p:{x:-360.4,y:-111.1}},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7}]},1).to({state:[{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_10,p:{x:-286.5,y:111.7}},{t:this.shape_13}]},5).to({state:[]},4).to({state:[]},39).wait(41));
	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(17).to({_off:false},0).to({x:-276.8},14,cjs.Ease.quartOut).to({_off:true},1).wait(89));

	// masque Titre
	this.instance_3 = new lib.masque_generique();
	this.instance_3.parent = this;
	this.instance_3.setTransform(221.1,-163.9,0.013,0.471,0,0,0,0,17.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({regX:0.5,scaleX:1.92,x:-75.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.01,x:-68.7},10,cjs.Ease.quartInOut).to({_off:true},1).wait(101));

	// DECO 2
	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FF8D72").s().p("AgDAKIgHgKQgCgDABgEQABgDADgCQAGgEAEAHIAIAKQACADgBAEQgBADgDACIgFABQgDAAgDgEg");
	this.shape_19.setTransform(-36.7,-68,2.741,2.741);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FF8D72").s().p("AgDAKIgGgLQgFgHAIgFQAHgEADAHIAGAMQAFAHgIAEIgFACQgDAAgCgFg");
	this.shape_20.setTransform(-25.9,-50.4,2.741,2.741);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FF8D72").s().p("AAAAPQgHgCACgIQAAgDgCgCQgGgGAGgGQAGgGAFAGQAJAJgDANQgBAGgGAAIgDgBg");
	this.shape_21.setTransform(-17.6,-29.9,2.741,2.741);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FF8D72").s().p("AgHAHIAAgOQAAgHAHAAQAJAAAAAHIAAAOQAAAJgJgBQgHABAAgJg");
	this.shape_22.setTransform(-13.6,-11.9,2.741,2.741);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FF8D72").s().p("AgHAKIAAgTQAAgHAHAAQAIAAAAAHIAAATQAAAHgIAAQgHAAAAgHg");
	this.shape_23.setTransform(-13,8.4,2.741,2.741);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FF8D72").s().p("AgFAUQgIgCACgIIAHgYQADgIAHADQAIACgCAIIgIAYQgBAGgFAAIgDgBg");
	this.shape_24.setTransform(-16.2,28.9,2.741,2.741);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FF8D72").s().p("AgEAIQgHAAgBgIQABgHAHAAIAIAAQAIAAABAHQgBAIgIAAg");
	this.shape_25.setTransform(-78.3,-81,2.741,2.741);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FF8D72").s().p("AgMADQgCgHAIgCIAJgCQAIgCACAIQADAHgJACIgJADIgCAAQgHAAgBgHg");
	this.shape_26.setTransform(-94.8,-79.5,2.741,2.741);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FF8D72").s().p("AgJAGQgCgDABgDQAAgDAEgCIAGgDQAGgDAEAGQACADgBADQgBADgDACIgGADIgDABQgFAAgCgEg");
	this.shape_27.setTransform(-108.7,-74.9,2.741,2.741);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FF8D72").s().p("AgJAPQgEgBgCgDQgDgHAGgEIAQgNQADgCAEACQADABACACQADAHgGAEIgQANIgEABIgCAAg");
	this.shape_28.setTransform(-122.9,-68.8,2.741,2.741);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FF8D72").s().p("AgHALQgCgBgBgEQgBgDACgDIAGgHQADgIAIAEQACACABAEQABADgCACIgGAIQgDAFgDAAIgFgCg");
	this.shape_29.setTransform(-136.4,-58.5,2.741,2.741);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FF8D72").s().p("AgJAJQAAgJADgJQADgJAGADQADABACADQACADgBADQgDAGABAIQABADgDADQgDACgCAAQgIAAgBgIg");
	this.shape_30.setTransform(-142.8,-40.6,2.741,2.741);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FF8D72").s().p("AgHAEIAAgIQgBgHAIAAQAJAAAAAHIAAAIQAAAJgJgBQgIABABgJg");
	this.shape_31.setTransform(-144.9,-22.6,2.741,2.741);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FF8D72").s().p("AgDAQQgHgDACgIIACgOQABgDADgCQACgBADABQAIABgCAJIgCAOQgBADgDACIgDABIgDAAg");
	this.shape_32.setTransform(-146.3,-4.4,2.741,2.741);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FF8D72").s().p("ACHBrQgGgCAAgGIgBgEQgKAFgJgBQgJgBgDgJQgYAGgTAAQgFABgCgFQgCgEACgEIADgGQgoAKgXgCQgigCALgaQg8AQgigGQgrgHADgoQAGg9A7grQBAguA1AkQAHAEgFAIQgEAHgHgFQgZgRgkAKQghAIgXAYQgUAUgHAQQgNAbABARQADAcAogDQAtgDAqgLQAVgdAcgLQAhgNAdAPQAEACAAAEQAAAGgEACQgmATg8ATQgPAWAUgBQAlgCAmgPQAGgCADAGQADAGgEAEQgEADgFAGQAPgCASgFQAEgBAEACQADADAAAEIAQgCQAEgDAEABQAFABABAEIAEAMQADADAKgBQAIgCADAIQACAJgIABIgPACQgEAAgLgEgAAJAaQAYgHAcgNIgHgBQgXAAgWAVg");
	this.shape_33.setTransform(-80.1,-17.4,2.741,2.741);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FF8D72").s().p("AgSGxQgkgFg4gMQhagUg/gkQhVgxgRhHQgJgjAKgmQAHgcAVgpQAvhdAsgxQAAgKACgPIAEgUIALhzQAHhBAigpQAVglAtghQAtghAogJQA4gLAiACQADgEAEAAQBJgFBAAbQBBAbAuA5QAeAlANApQAPAsgHApQADADAAADQALBhgGArQgKBLg3AlQACBNgdBIQgdBJg3A0QgeAcgYAPQgiAWgeADQgRADgUAAQgaAAgdgEgAk7BeQgbA1gHAbQgMAvAVAlQBABwC6AhIBYAQQA2AHAhgLQBAgWAxg6QBXhlABiCQgSAKgdAEQgcBIgWAnQghA5gsAjQhTBCh4gsQg/gXgtgfQg6gngWgwQgQghAWhEQASg7Aig1IgNgIQgDgCgCgEQgkAqgoBNgAkFBwQgVBMAnAnQBMBMBdAcQB+AlBOhVQAdggAXgtQARggAVg2IgTACIAAABQgoBfhHA4QhVBEhZghQgvgSgjggIgkghQgWgVgIgRQgTgkAOg7QAEgRAbhNIgBgBQgoA/gOAzgAjeBqQgKA9AeAgQApArAUAQQAmAfAkAHQBSAPBHg3QA7gwAlhVIgTABQgmBRg1ArQhFA3hIggQg7gbg3hLQgVgdAGguQADgTASg7IgPgJQgZBHgFAcgAixBYQgGAdAGAUQAFAUASAWQAXAbAPAOQAWAVAXAKQBFAfA9guQAwgjAlhMQhgAChAgbQhBgbhRgwQgLAogEAXgAieAFQBNAvAsAVQAtAVA4AHQArAGBAgBIAZgBIAegBQADgEAFgBQAEAAADADQAggFANgQIADgEQAFgIABgGIAQhOQAMhBgDgpQgEg0gdgwQgegygPgUQgeglgigOQgKAJgXgCQgUgBgNgHQgPgJgFgNIgFACQhBABghACQg6ADglAOQg2AUgaAsQgWAmgGA/IgIBZIgDAWQgCAOABAJQABALANAJIAWALIAWANIACAAQAHAAABAFgAFShrIAAAAQAAAmgLA6IgPBLQAVgUAKgaQAIgXABgiQAAg2gDgkQgFAMgGAKgAB+mVIABACQAQAAAPAJQAFADgBAEQAbAMgDASQBCAdA5BvQAUAnAGAnQAUgygSg4QgQg0grgoQhOhKhcgHQAKAGAIAHgAB1mBQgMAFADAJQADAJAKAEQAUAJAUgEQAJgCgDgHQgCgHgHgCIgKgDQgGgCABgHQgJgDgIAAQgFAAgEABgAARmfQg0AFghASQgfARgZAVQArgQA9gFIBrgFIAEABQABgIAGgGQAEgEAEgCQgXgRgrAAIgXABg");
	this.shape_34.setTransform(-90.5,14.6,2.741,2.741);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FF8D72").s().p("AgYAZQgDgCABgDIADgFIAkgmQAFgEAFABQABAAAAAAQAAAAABAAQABAAAAABQABAAABABQAAAFgEAEIgjAlQgFAFgEAAQgBAAAAAAQgBAAAAAAQgBgBAAAAQgBAAAAgBg");
	this.shape_35.setTransform(-84.3,114,3.176,3.176,18);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FF8D72").s().p("AgmANQgBgFAHgCQATgEAogSQAGgDAEAFQAEADgGAEQgeARggAIQgJAAgCgFg");
	this.shape_36.setTransform(-231.2,156.7,3.176,3.176,18);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FF8D72").s().p("AgiATQgDgCAEgEIAPgJQAJgDAMgIQAOgKAHgDQAGgEAEAEQADACgBADIgEAFQgaAUgbAKIgIADIgFgEg");
	this.shape_37.setTransform(-192.5,150.6,3.176,3.176,18);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FF8D72").s().p("AAXASQgmgJgPgJQgDAAgFgIQgEgEAEgDQAEgEAFAEQATANAZAFIASADQAIACAAAEQAAADgHADQgFgBgGABg");
	this.shape_38.setTransform(-315.8,145.2,3.176,3.176,18);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FF8D72").s().p("AggAbQgHgCADgGQAAgFAJgEQAbgOAWgRIAGgFQAEgBAEADQAFAFgJAGIg1AkQgFAEgEAAIgCAAg");
	this.shape_39.setTransform(-154.3,141.2,3.176,3.176,18);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FF8D72").s().p("AgjAcQgDgFAFgEIAVgPQAVgPAJgLIAIgHQAFgCADACQAFADgEAGIgFAFQgVATgdAVQgGAGgEAAIgFgDg");
	this.shape_40.setTransform(-119.1,128.3,3.176,3.176,18);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FF8D72").s().p("AADAoQgKgLgBgFQgHgfADgVQAAgEADgDQAEgGAFABQADABAAAFIgCAXIABAUQACAMAHAIIACAEQADAHgFABIgDABQgDAAgCgCg");
	this.shape_41.setTransform(-334.6,112.3,3.176,3.176,18);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FF8D72").s().p("AgsAPQgIgCACgFQAAgEAHAAQANAAASgEIAegHIAZgHQAFgBADAEQACAEgFADQgGAEgEAAQgpAMglADIgEAAg");
	this.shape_42.setTransform(-275.8,155.8,3.176,3.176,18);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FF8D72").s().p("AgVAmQgHgCACgHIAFgLQAKgYAZgbQAFgFAFABQAEAEAAAFQAAADgIAIQgUASgFAVQgFAQgIAAIgDAAg");
	this.shape_43.setTransform(-289.7,46.2,3.176,3.176,18);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FF8D72").s().p("AgQAlQgDgFAAgIIAJgeQAFgTAHgMQADgHAIADQAKACgEAJIgEAJQgOAVAAAbIAAAJQgBAFgHACQgFAAgEgGg");
	this.shape_44.setTransform(-320.7,77.3,3.176,3.176,18);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FF8D72").s().p("AlVGzQgdgKgkgWQgigVgQghQgOgbgDgoQgEg1AcgyIAeg6IAdg5QAHgPATgYQARgZADggQAEg/ASgtQAXg5AigyQAkg3BLgwQBLgxA/gUQBMgaBCADQBKADA/AnIAhATQATAMAMAKQAVARAUAcIAiAxQA1BSASBzQADAXgFAkQgDAJgCADQgcAngQASQhFBNhMBAIgmAiQgXAUgRAMQgbASgoAXIg6AhIg7AfQgYAMgYAJQg7AWhGASQgqAKhEAAIgRABQgZAAgbgJgAlKAIQgEABgEAFQgbAegOAfQgOAgglBDQgpBNAWBLQAMAmAfAXQA/AvBPgCQBagCBigjQAygSA1gbQAigRBBgnQBDglAhgfIBCg8IAugpQAcgYAPgUIAogvQgGgCgLACQhJANhKAFQghgBgRADQgaAEglACIhAACQghACgugDIhPgFQhcgEg0gNIgygLQgegIgTgKQgDgDgEAAIgCABgAgBjOQACACgBAEIgCAGIgEAHQgoA6ggBOQgIATgFAaIgKAuIAjACQByAIBFgFIBxgHQBEgFAsgFQAkgEAsgNQAggJACgZQACgQgFgvQgCgagDgHQgRgrgDgJQgMgpgmg+QgyhRhMgmQgwgYgdgBIgBACIACAFQAIANgCAFQgBAFgPAHQgIAFgJABQgQAAgJAEQgNAEgGAMQghAVgcAvIgWAfQgDAFgHgDQgFgCACgFQABgGADgFQAig9AlgbQALgJACgEQgBgCgHgCQgNgFgCgJQgBgJAKgIQAJgIAQgFQAHgCANAAIAVgBIgIgCQgPgEgXABQgZADgMAAQgrACgpASQhVAlgwAfQg2AlgWAbQgsA2gZA5QgNAcgIAjQgFAWgHArQgDATASALQAZANAUAFIBNASQAPADAsAFQAPABABgMQAGgkAQgyQAFgPAKgWIARgkQAKgYAIgNQAMgUAPgNIAIgGIAEgBQADAAACACgABlmOQgFAEABACQABAEAGAAQAcAFAbgKQgMgMgQgBIgEgBQgOAAgMAJg");
	this.shape_45.setTransform(-182.2,56,3.176,3.176,18);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().s("#FFFFFF").ss(1,1,1).p("AW3ISI4mONI1HzBILk58IcQC+g");
	this.shape_46.setTransform(-22.7,68.8);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().s("#FFFFFF").ss(1,1,1).p("AnFmSICWm8ISbU+I7XFfIAAgB");
	this.shape_47.setTransform(-240,-49.6);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FFFFFF").s().p("AqGJxIGnzhIAAAAINmPeI0MEDg");
	this.shape_48.setTransform(-262.9,-27.5);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f().s("#FFFFFF").ss(1,1,1).p("ALnNZI3NtWIXJtbg");
	this.shape_49.setTransform(-577,114.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_34,p:{x:-90.5,y:14.6}},{t:this.shape_33,p:{x:-80.1,y:-17.4}},{t:this.shape_32,p:{x:-146.3,y:-4.4}},{t:this.shape_31,p:{x:-144.9,y:-22.6}},{t:this.shape_30,p:{x:-142.8,y:-40.6}},{t:this.shape_29,p:{x:-136.4,y:-58.5}},{t:this.shape_28,p:{x:-122.9,y:-68.8}},{t:this.shape_27,p:{x:-108.7,y:-74.9}},{t:this.shape_26,p:{x:-94.8,y:-79.5}},{t:this.shape_25,p:{x:-78.3,y:-81}},{t:this.shape_24,p:{x:-16.2,y:28.9}},{t:this.shape_23,p:{x:-13,y:8.4}},{t:this.shape_22,p:{x:-13.6,y:-11.9}},{t:this.shape_21,p:{x:-17.6,y:-29.9}},{t:this.shape_20,p:{x:-25.9,y:-50.4}},{t:this.shape_19,p:{x:-36.7,y:-68}}]},17).to({state:[{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35}]},6).to({state:[{t:this.shape_34,p:{x:-196.1,y:-53.7}},{t:this.shape_33,p:{x:-185.7,y:-85.7}},{t:this.shape_32,p:{x:-251.9,y:-72.7}},{t:this.shape_31,p:{x:-250.5,y:-90.9}},{t:this.shape_30,p:{x:-248.4,y:-108.9}},{t:this.shape_29,p:{x:-242,y:-126.8}},{t:this.shape_28,p:{x:-228.5,y:-137.1}},{t:this.shape_27,p:{x:-214.3,y:-143.2}},{t:this.shape_26,p:{x:-200.4,y:-147.8}},{t:this.shape_25,p:{x:-183.9,y:-149.3}},{t:this.shape_24,p:{x:-121.8,y:-39.4}},{t:this.shape_23,p:{x:-118.6,y:-59.9}},{t:this.shape_22,p:{x:-119.2,y:-80.2}},{t:this.shape_21,p:{x:-123.2,y:-98.2}},{t:this.shape_20,p:{x:-131.5,y:-118.7}},{t:this.shape_19,p:{x:-142.3,y:-136.3}}]},10).to({state:[]},10).to({state:[{t:this.shape_46}]},50).to({state:[{t:this.shape_48},{t:this.shape_47}]},4).to({state:[{t:this.shape_49}]},3).to({state:[]},6).to({state:[]},1).wait(14));

	// masque Titre
	this.instance_4 = new lib.masque_generique();
	this.instance_4.parent = this;
	this.instance_4.setTransform(-228.9,-13.9,0.013,0.471,0,0,0,0,17.2);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(4).to({_off:false},0).to({regX:0.5,scaleX:1.92,x:-527.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.01,x:-528.7},10,cjs.Ease.quartInOut).to({_off:true},1).wait(97));

	// _LETTRES_CLIENT
	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FF8D72").s().p("AgJA0QgEgEAAgGQAAgFAEgFQAEgEAFAAQAGAAAEAEQAEAFAAAFQAAAGgEAEQgEAFgGAAQgFAAgEgFgAgJAVIgEg5IAAgUIAbAAIAAAUIgFA5g");
	this.shape_50.setTransform(-357.9,-14.1);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FF8D72").s().p("AAbA4Ig1hFIAABFIgZAAIAAhvIAYAAIA2BHIAAhHIAZAAIAABvg");
	this.shape_51.setTransform(-378.9,-14.1);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FF8D72").s().p("AgoA4IAAhvIBPAAIAAAXIg2AAIAAAWIAxAAIAAAUIgxAAIAAAYIA4AAIAAAWg");
	this.shape_52.setTransform(-402.4,-14.1);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FF8D72").s().p("AgMA4IAAhvIAZAAIAABvg");
	this.shape_53.setTransform(-422.5,-14.1);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FF8D72").s().p("AgCA5IADgYIgNAAIgEAYIgZAAIAFgYIgQAAIAFgWIAPAAIADgTIgRAAIAEgWIARAAIAEgaIAXAAIgDAaIANAAIAEgaIAZAAIgFAaIARAAIgEAWIgRAAIgDATIASAAIgEAWIgSAAIgEAYgAgJALIAOAAIAEgTIgOAAg");
	this.shape_54.setTransform(-459.4,-14.3);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FF8D72").s().p("AgjA4IAAhvIAZAAIAABYIAuAAIAAAXg");
	this.shape_55.setTransform(-497.6,-14.1);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FF8D72").s().p("AgjApQgRgQAAgYQAAgZARgQQASgRAXAAQAcAAASAVIgPASQgMgPgSAAQgNAAgKAJQgJAJgBAPQAAAPAKAKQAJAKANAAQASAAALgPIAQARQgTAVgZAAQgZAAgRgRg");
	this.shape_56.setTransform(-520.4,-14.2);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FF8D72").s().p("AgMA4IAAhZIgfAAIAAgWIBXAAIAAAWIggAAIAABZg");
	this.shape_57.setTransform(-411.9,-14.1);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#FF8D72").s().p("AAbA4Ig0hFIAABFIgaAAIAAhvIAYAAIA2BHIAAhHIAZAAIAABvg");
	this.shape_58.setTransform(-435.3,-14.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53,p:{x:-422.5}},{t:this.shape_52,p:{x:-402.4}},{t:this.shape_51},{t:this.shape_50,p:{x:-357.9}}]},13).to({state:[{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_50,p:{x:-422.7}}]},5).to({state:[{t:this.shape_56},{t:this.shape_55},{t:this.shape_53,p:{x:-478.9}},{t:this.shape_52,p:{x:-458.8}},{t:this.shape_58},{t:this.shape_57}]},5).to({state:[]},11).to({state:[]},46).wait(41));

	// Layer_11
	this.instance_5 = new lib.masqueTexte("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(-340,105.8,0.85,0.85,0,0,0,0.1,108.8);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(37).to({_off:false},0).to({_off:true},40).wait(3).to({_off:false,rotation:180},0).to({_off:true},40).wait(1));

	// _TEXTE_PRESENTATION
	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#FFFFFF").s().p("Ah+CMIAAkXIBjAAQBKAAAoAlQAoAkAABCQAABAgnAmQgnAmhPAAgAg/BUIAnAAQAqAAAXgVQAXgWAAgpQAAgogXgWQgXgWgvAAIgiAAg");
	this.shape_59.setTransform(-423.3,179);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#FFFFFF").s().p("AhjBtIAagyQAnAcAfAAQASAAAMgJQANgJAAgSQAAgRgPgKQgNgLgYAAQgOAAgXAIIAAgtIA0g/IhWAAIAAg1ICqAAIAAArIg6BBQAiAGASAWQATAWAAAdQAAAsgeAZQgeAZgtAAQguAAgwggg");
	this.shape_60.setTransform(-451.6,179.1);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#FFFFFF").s().p("AAoBtIAAh4QAAgygkAAQgRAAgNANQgOANAAAZIAAB3Ig7AAIAAjWIA7AAIAAAYQAbgbAhAAQAjAAAWAYQAYAYAAAnIAACCg");
	this.shape_61.setTransform(-486.2,182);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAhgeQAggeArAAQAtAAAfAbQAfAbAAAuIAAAfIicAAQADASAQALQAPALATAAQAfAAAVgVIAhAlQgkAigxAAQgwAAgggegAgdgyQgPALgDATIBeAAQgCgUgMgKQgNgKgRAAQgRAAgPAKg");
	this.shape_62.setTransform(-511.4,182.2);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#FFFFFF").s().p("AgqBmQgagIgVgRIAagmQAiAaAlAAQAMAAAGgEQAIgFgBgHQAAgIgJgGQgJgHgRgFIgbgKQgKgEgLgIQgZgOAAgeQAAgdAYgTQAYgTAlAAQAmAAAmAaIgXAoQgcgUgcAAQgaAAAAAQQAAAIAJAFQAIAEAUAHIAdAKQAJADAMAHQAWANAAAgQAAAggXATQgYATgkAAQgXAAgagJg");
	this.shape_63.setTransform(-247.2,133.4);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAggeQAhgeAsAAQAsAAAfAbQAfAbAAAuIAAAfIicAAQADASAPALQAQALATAAQAfAAAVgVIAiAlQglAigxAAQgwAAgggegAgdgyQgPALgDATIBeAAQgCgUgNgKQgMgKgRAAQgSAAgOAKg");
	this.shape_64.setTransform(-269.5,133.4);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#FFFFFF").s().p("AgfB5QgWgUAAgkIAAhiIgZAAIAAgrIAZAAIAAhAIA7AAIAABAIA0AAIAAArIg0AAIAABgQAAAMAHAHQAGAHAJAAQARAAAMgPIAWAqQgcAZgfAAQgeAAgVgUg");
	this.shape_65.setTransform(-290.2,130.4);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#FFFFFF").s().p("AggB5QgVgUAAgkIAAhiIgZAAIAAgrIAZAAIAAhAIA8AAIAABAIAzAAIAAArIgzAAIAABgQgBAMAHAHQAGAHAJAAQARAAALgPIAXAqQgcAZgfAAQgeAAgWgUg");
	this.shape_66.setTransform(-307.8,130.4);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAhgeQAggeArAAQAtAAAfAbQAfAbAAAuIAAAfIicAAQADASAQALQAPALATAAQAfAAAVgVIAhAlQgkAigxAAQgwAAgggegAgdgyQgPALgCATIBdAAQgCgUgMgKQgNgKgSAAQgRAAgOAKg");
	this.shape_67.setTransform(-329.4,133.4);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#FFFFFF").s().p("AhMBXQgYgWAAgsIAAiBIA9AAIAAB0QAAAxAkAAQARAAANgMQANgNAAgZIAAhzIA9AAIAADWIg9AAIAAgcQgWAfgiAAQgkAAgYgWg");
	this.shape_68.setTransform(-354.8,133.6);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#FFFFFF").s().p("AAyCUIAAhfQgZAVggAAQgpAAgfgfQgfggAAgvQAAgxAegfQAegfAlAAQAkAAAbAbIAAgYIA9AAIAAEkgAgihQQgPASAAAaQAAAaAPAQQAQARATAAQAVAAAPgRQAOgQAAgaQAAgagOgSQgPgRgVAAQgUAAgPARg");
	this.shape_69.setTransform(-381.4,137.2);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#FFFFFF").s().p("AgqBmQgagIgVgRIAbgmQAhAaAmAAQALAAAHgEQAGgFAAgHQAAgIgJgGQgKgHgQgFIgcgKQgJgEgLgIQgZgOAAgeQAAgdAYgTQAYgTAlAAQAmAAAmAaIgWAoQgcgUgdAAQgaAAAAAQQAAAIAJAFQAJAEATAHIAdAKQAIADAMAHQAXANAAAgQAAAggYATQgXATgkAAQgXAAgagJg");
	this.shape_70.setTransform(-404.9,133.4);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#FFFFFF").s().p("AhJBbQgXgUAAghQAAghAYgPQAYgQAqAAIArAAIAAgCQAAgigkAAQgPAAgRAGQgSAGgLAKIgbgnQAqgfA2AAQAnAAAZAUQAYATAAArIAACIIg4AAIAAgaQgYAdggAAQgjAAgXgUgAgkAmQAAALAJAHQAIAGAQAAQAQAAALgKQANgKAAgQIAAgLIglAAQgkAAAAAXg");
	this.shape_71.setTransform(-427.3,133.4);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#FFFFFF").s().p("AhEBQQgigeAAgyQAAgwAigfQAigfAtAAQAXAAAYAKQAYAJASATIgfApQgLgNgPgGQgOgHgPAAQgXAAgRAPQgRAQAAAaQAAAbARAQQARAPAWAAQAeAAAYgeIAkAoQgqArgwAAQgwAAghgfg");
	this.shape_72.setTransform(-450.1,133.4);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAggeQAhgeAsAAQAsAAAfAbQAfAbAAAuIAAAfIicAAQADASAPALQAQALATAAQAfAAAVgVIAhAlQgkAigxAAQgwAAgggegAgdgyQgPALgDATIBeAAQgCgUgNgKQgMgKgRAAQgSAAgOAKg");
	this.shape_73.setTransform(-484.7,133.4);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#FFFFFF").s().p("AhQB2QgeggAAgwQAAgwAdgeQAegeAngBQAmAAAZAcIAAhqIA8AAIAAEoIg8AAIAAgcQgaAfglAAQgmAAgegggAgigDQgPARAAAaQAAAaAPARQAQAQATAAQAVAAAPgQQAOgRAAgaQAAgagOgRQgPgSgVAAQgUAAgPASg");
	this.shape_74.setTransform(-511.1,129.5);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#FFFFFF").s().p("AAoBtIAAh4QABgyglAAQgRAAgNANQgOANAAAZIAAB3Ig7AAIAAjWIA7AAIAAAYQAcgbAgAAQAjAAAXAYQAWAYAAAnIAACCg");
	this.shape_75.setTransform(-198.5,84.5);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#FFFFFF").s().p("AhQBPQghggAAgvQAAguAhggQAgggAwAAQAxAAAgAgQAhAgAAAuQAAAvghAgQggAggxAAQgwAAgggggAglgqQgPARAAAZQAAAbAPAQQAPARAWAAQAXAAAPgRQAPgQAAgbQAAgZgPgRQgPgRgXAAQgWAAgPARg");
	this.shape_76.setTransform(-224.4,84.7);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#FFFFFF").s().p("AgdCXIAAjWIA7AAIAADWgAgYhaQgKgKAAgPQAAgPAKgKQAKgKAOAAQAPAAAKAKQAKAKAAAPQAAAPgKAKQgKAKgPAAQgOAAgKgKg");
	this.shape_77.setTransform(-243.2,80.3);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("#FFFFFF").s().p("AgfB5QgWgUAAgkIAAhiIgZAAIAAgrIAZAAIAAhAIA8AAIAABAIA0AAIAAArIg0AAIAABgQAAAMAGAHQAHAHAIAAQARAAALgPIAXAqQgcAZgfAAQgeAAgVgUg");
	this.shape_78.setTransform(-257.2,81.6);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("#FFFFFF").s().p("AhJBbQgXgUAAghQAAghAYgPQAYgQApAAIAsAAIAAgCQAAgigkAAQgPAAgRAGQgRAGgNAKIgbgnQArgfA1AAQAnAAAZAUQAZATAAArIAACIIg4AAIAAgaQgYAdghAAQghAAgYgUgAgkAmQAAALAJAHQAIAGARAAQAPAAAMgKQAMgKAAgQIAAgLIglAAQgkAAAAAXg");
	this.shape_79.setTransform(-278.9,84.7);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f("#FFFFFF").s().p("AgqBmQgagIgVgRIAbgmQAhAaAmAAQALAAAHgEQAGgFABgHQAAgIgKgGQgKgHgQgFIgbgKQgKgEgMgIQgYgOAAgeQAAgdAYgTQAYgTAlAAQAmAAAmAaIgXAoQgbgUgcAAQgbAAAAAQQAAAIAJAFQAJAEATAHIAdAKQAJADALAHQAXANAAAgQAAAggYATQgXATgkAAQgXAAgagJg");
	this.shape_80.setTransform(-301.1,84.7);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("#FFFFFF").s().p("AgdCXIAAjWIA7AAIAADWgAgYhaQgKgKAAgPQAAgPAKgKQAKgKAOAAQAPAAAKAKQAKAKAAAPQAAAPgKAKQgKAKgPAAQgOAAgKgKg");
	this.shape_81.setTransform(-317,80.3);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f("#FFFFFF").s().p("AgdCVIAAkoIA7AAIAAEog");
	this.shape_82.setTransform(-328.6,80.6);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("#FFFFFF").s().p("AhJBbQgXgUAAghQAAghAYgPQAYgQApAAIAsAAIAAgCQAAgigkAAQgPAAgRAGQgRAGgNAKIgbgnQArgfA1AAQAnAAAZAUQAZATAAArIAACIIg4AAIAAgaQgYAdghAAQghAAgYgUgAgkAmQAAALAIAHQAKAGAQAAQAOAAANgKQAMgKAAgQIAAgLIglAAQgkAAAAAXg");
	this.shape_83.setTransform(-346.8,84.7);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f("#FFFFFF").s().p("AAoBtIAAh4QABgyglAAQgRAAgNANQgOANAAAZIAAB3Ig7AAIAAjWIA7AAIAAAYQAcgbAgAAQAjAAAXAYQAWAYAAAnIAACCg");
	this.shape_84.setTransform(-370.8,84.5);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f("#FFFFFF").s().p("AApBtIAAh4QAAgyglAAQgRAAgNANQgNANAAAZIAAB3Ig8AAIAAjWIA8AAIAAAYQAbgbAgAAQAjAAAXAYQAWAYAAAnIAACCg");
	this.shape_85.setTransform(-396.4,84.5);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f("#FFFFFF").s().p("AhQBPQghggAAgvQAAguAhggQAgggAwAAQAxAAAgAgQAhAgAAAuQAAAvghAgQggAggxAAQgwAAgggggAglgqQgPARAAAZQAAAbAPAQQAPARAWAAQAXAAAPgRQAPgQAAgbQAAgZgPgRQgPgRgXAAQgWAAgPARg");
	this.shape_86.setTransform(-422.3,84.7);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f("#FFFFFF").s().p("AgqBmQgagIgVgRIAbgmQAhAaAmAAQALAAAHgEQAGgFABgHQAAgIgKgGQgKgHgQgFIgbgKQgKgEgMgIQgYgOAAgeQAAgdAYgTQAYgTAlAAQAmAAAmAaIgXAoQgbgUgcAAQgbAAAAAQQAAAIAJAFQAJAEATAHIAdAKQAJADALAHQAXANAAAgQAAAggYATQgXATgkAAQgXAAgagJg");
	this.shape_87.setTransform(-446,84.7);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f("#FFFFFF").s().p("Ag9BtIAAjWIA8AAIAAAdQAKgOASgJQARgJASAAIAAA4IgLAAQgbAAgNATQgMATAAAeIAABdg");
	this.shape_88.setTransform(-463.4,84.5);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAggeQAhgeAsAAQAsAAAfAbQAfAbAAAuIAAAfIicAAQADASAPALQAQALATAAQAfAAAVgVIAhAlQgkAigxAAQgwAAgggegAgdgyQgPALgDATIBeAAQgCgUgNgKQgMgKgRAAQgSAAgOAKg");
	this.shape_89.setTransform(-484.7,84.7);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f("#FFFFFF").s().p("AhuCUIAAkjIA8AAIAAAXQAcgbAkAAQAmAAAdAfQAeAgAAAvQAAAvgeAgQgdAfgnABQgngBgYgfIAABqgAgkhPQgPARAAAaQAAAbAPAPQAPAQAUAAQAUAAAQgQQAPgPAAgaQAAgbgPgRQgPgSgUAAQgVAAgPASg");
	this.shape_90.setTransform(-509.9,88.4);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAggeQAhgeAsAAQAsAAAfAbQAfAbAAAuIAAAfIicAAQADASAPALQAQALATAAQAfAAAVgVIAiAlQglAigxAAQgwAAgggegAgdgyQgPALgDATIBeAAQgCgUgNgKQgMgKgRAAQgRAAgPAKg");
	this.shape_91.setTransform(-269.9,35.9);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f("#FFFFFF").s().p("AhQB2QgeghAAgwQAAguAdggQAegeAnAAQAmABAZAbIAAhqIA8AAIAAEoIg8AAIAAgdQgaAgglAAQgmAAgegggAgigDQgPARAAAaQAAAaAPAQQAQARATAAQAVAAAPgRQAOgQAAgbQAAgZgOgRQgPgRgVAAQgUAAgPARg");
	this.shape_92.setTransform(-296.3,32);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.f("#FFFFFF").s().p("AgyB2IAAAdIg8AAIAAkoIA8AAIAABqQAZgbAmgBQAnAAAdAeQAeAgAAAuQAAAwgeAhQgeAggmAAQgmAAgZgggAgkgDQgPARAAAZQAAAbAPAQQAPARAUAAQAUAAAQgRQAPgQAAgaQAAgagPgRQgPgRgUAAQgVAAgPARg");
	this.shape_93.setTransform(-332.2,32);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAhgeQAggeArAAQAtAAAfAbQAfAbAAAuIAAAfIicAAQADASAQALQAPALATAAQAfAAAVgVIAhAlQgkAigxAAQgwAAgggegAgdgyQgPALgDATIBeAAQgCgUgMgKQgNgKgRAAQgRAAgPAKg");
	this.shape_94.setTransform(-358.4,35.9);

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.f("#FFFFFF").s().p("AAoBsIgoh0IgnB0Ig+AAIhJjXIA+AAIApCBIAqiBIA6AAIApCBIAriBIA+AAIhJDXg");
	this.shape_95.setTransform(-388.9,36);

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.f("#FFFFFF").s().p("AgdCXIAAjWIA7AAIAADWgAgYhaQgKgKAAgPQAAgPAKgKQAKgKAOAAQAPAAAKAKQAKAKAAAPQAAAPgKAKQgKAKgPAAQgOAAgKgKg");
	this.shape_96.setTransform(-423.3,31.6);

	this.shape_97 = new cjs.Shape();
	this.shape_97.graphics.f("#FFFFFF").s().p("AgdCVIAAkoIA7AAIAAEog");
	this.shape_97.setTransform(-434.9,31.9);

	this.shape_98 = new cjs.Shape();
	this.shape_98.graphics.f("#FFFFFF").s().p("AhuCUIAAkkIA8AAIAAAZQAcgcAkAAQAmAAAdAfQAeAgAAAvQAAAvgeAgQgdAggngBQgnABgYggIAABqgAgkhQQgPASAAAaQAAAaAPAQQAPAQAUABQAUgBAQgQQAPgPAAgbQAAgagPgSQgPgRgUAAQgVAAgPARg");
	this.shape_98.setTransform(-453.5,39.7);

	this.shape_99 = new cjs.Shape();
	this.shape_99.graphics.f("#FFFFFF").s().p("AhuCUIAAkkIA8AAIAAAZQAcgcAkAAQAmAAAdAfQAeAgAAAvQAAAvgeAgQgdAggngBQgnABgYggIAABqgAgkhQQgPASAAAaQAAAaAPAQQAPAQAUABQAUgBAQgQQAPgPAAgbQAAgagPgSQgPgRgUAAQgVAAgPARg");
	this.shape_99.setTransform(-480.1,39.7);

	this.shape_100 = new cjs.Shape();
	this.shape_100.graphics.f("#FFFFFF").s().p("ABVCMIgbg9Ih0AAIgaA9IhCAAIB5kXIA8AAIB4EXgAgiAZIBFAAIgjhRg");
	this.shape_100.setTransform(-508.9,32.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_100},{t:this.shape_99},{t:this.shape_98},{t:this.shape_97},{t:this.shape_96},{t:this.shape_95},{t:this.shape_94},{t:this.shape_93},{t:this.shape_92},{t:this.shape_91},{t:this.shape_90},{t:this.shape_89},{t:this.shape_88},{t:this.shape_87},{t:this.shape_86},{t:this.shape_85},{t:this.shape_84},{t:this.shape_83},{t:this.shape_82},{t:this.shape_81},{t:this.shape_80},{t:this.shape_79},{t:this.shape_78},{t:this.shape_77},{t:this.shape_76},{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59}]},57).to({state:[]},43).to({state:[]},1).wait(20));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(221.1,-172,2,16);


// stage content:
(lib.FS_projet_headict = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":150});

	// timeline functions:
	this.frame_99 = function() {
		//var frequency = 60;
		//stage.enableMouseOver(frequency);
		
		// EVENT DE CLIC POUR LA SUITE
		this.next_btn.addEventListener("click", fl_ClickToGoToAndPlayFromFrame.bind(this));
		this.next_btn.addEventListener("mouseover", fl_MouseOverHandler.bind(this));
		this.next_btn.addEventListener("mouseout", fl_MouseOutHandler.bind(this));
		
		
		function fl_ClickToGoToAndPlayFromFrame()
		{
			//this.gotoAndPlay("close");
			
			var event = new Event('next');
			this.dispatchEvent(event);		
			event = null;
			
			this.next_btn.btnNext.gotoAndPlay('rollOut');
			
		}
		function fl_MouseOverHandler(){	this.next_btn.btnNext.gotoAndPlay('rollOver'); }
		function fl_MouseOutHandler(){	this.next_btn.btnNext.gotoAndPlay('rollOut'); }
		
		
		// EVENT DE CLIC POUR LE LIEN
		this.cta_mc.addEventListener("click", projectDetails.bind(this));
		this.cta_mc.addEventListener("mouseover", pDetails_MouseOverHandler.bind(this));
		this.cta_mc.addEventListener("mouseout", pDetails_MouseOutHandler.bind(this));
		
		function projectDetails()
		{	
			var event = new Event('details');
			this.dispatchEvent(event);		
			event = null;
			
			this.cta_mc.btnCta.gotoAndPlay('rollOut');
			
		}
		function pDetails_MouseOverHandler(){	this.cta_mc.btnCta.gotoAndPlay('rollOver'); }
		function pDetails_MouseOutHandler(){	this.cta_mc.btnCta.gotoAndPlay('rollOut'); }
	}
	this.frame_145 = function() {
		//PAUSE
		this.stop();
	}
	this.frame_150 = function() {
		this.textes_mc.gotoAndPlay("close");
		this.visuel_mc.gotoAndPlay("close");
		this.cta_mc.gotoAndPlay("close");
		this.video_mc.gotoAndPlay("close");
		this.next_btn.gotoAndPlay("close");
		this.lignes_mc.gotoAndPlay("close_lines");
	}
	this.frame_169 = function() {
		//this.lignes_mc.gotoAndPlay("close");
	}
	this.frame_211 = function() {
		// EVENT DE FIN D'ANIM
		var event = new Event('closed');
		this.dispatchEvent(event);		
		event = null;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(99).call(this.frame_99).wait(46).call(this.frame_145).wait(5).call(this.frame_150).wait(19).call(this.frame_169).wait(42).call(this.frame_211).wait(1));

	// BLOC INFOS TEXTE
	this.textes_mc = new lib.blocInfostextes();
	this.textes_mc.name = "textes_mc";
	this.textes_mc.parent = this;
	this.textes_mc.setTransform(858.2,182,1,1,0,0,0,222.1,-164);
	this.textes_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.textes_mc).wait(17).to({_off:false},0).wait(195));

	// masque VISUEL
	this.visuel_mc = new lib.bloc_visuel();
	this.visuel_mc.name = "visuel_mc";
	this.visuel_mc.parent = this;
	this.visuel_mc.setTransform(1192.6,391.5,1,1,0,0,0,552.5,26.5);
	this.visuel_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.visuel_mc).wait(17).to({_off:false},0).wait(195));

	// CTA NEXT
	this.next_btn = new lib.bloc_NEXT_btn();
	this.next_btn.name = "next_btn";
	this.next_btn.parent = this;
	this.next_btn.setTransform(753.2,786.9,1,1,0,0,0,-340.5,403.9);
	this.next_btn._off = true;

	this.timeline.addTween(cjs.Tween.get(this.next_btn).wait(89).to({_off:false},0).wait(123));

	// CTA DETAIL
	this.cta_mc = new lib.bloc_CTA();
	this.cta_mc.name = "cta_mc";
	this.cta_mc.parent = this;
	this.cta_mc.setTransform(299.5,787,1,1,0,0,0,-340.5,403.9);
	this.cta_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.cta_mc).wait(98).to({_off:false},0).to({_off:true},75).wait(39));

	// VIDEO
	this.video_mc = new lib.bloc_video();
	this.video_mc.name = "video_mc";
	this.video_mc.parent = this;
	this.video_mc.setTransform(866.7,1147.2,1,1,0,0,0,227.5,764.9);
	this.video_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.video_mc).wait(94).to({_off:false},0).to({_off:true},79).wait(39));

	// lignes
	this.lignes_mc = new lib.lignes_all();
	this.lignes_mc.name = "lignes_mc";
	this.lignes_mc.parent = this;
	this.lignes_mc.setTransform(640,384.3,1,1,0,0,0,0,406.3);

	this.timeline.addTween(cjs.Tween.get(this.lignes_mc).wait(212));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(825.5,361.5,909.1,813.5);
// library properties:
lib.properties = {
	id: 'C37E56F38C5D314789F796975B5C4D94',
	width: 1280,
	height: 768,
	fps: 60,
	color: "#000000",
	opacity: 1.00,
	manifest: [
		{src:"images/casquetteHeadict.png?1540809379759", id:"casquetteHeadict"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['C37E56F38C5D314789F796975B5C4D94'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;