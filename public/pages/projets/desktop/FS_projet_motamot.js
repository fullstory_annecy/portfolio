(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib._27_perroquet_rouge = function() {
	this.initialize(img._27_perroquet_rouge);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,227,308);


(lib.bateau1 = function() {
	this.initialize(img.bateau1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,251,221);


(lib.bateau2 = function() {
	this.initialize(img.bateau2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,246,300);


(lib.bateau3 = function() {
	this.initialize(img.bateau3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,246,300);


(lib.bateau4 = function() {
	this.initialize(img.bateau4);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,272,300);


(lib.Calque1 = function() {
	this.initialize(img.Calque1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,334,364);


(lib.Calque2 = function() {
	this.initialize(img.Calque2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,360,509);


(lib.Calque3 = function() {
	this.initialize(img.Calque3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,131,284);


(lib.Calque4 = function() {
	this.initialize(img.Calque4);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,157,300);


(lib.Calque5 = function() {
	this.initialize(img.Calque5);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,203,282);


(lib.mongol = function() {
	this.initialize(img.mongol);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,117,140);


(lib.motamot = function() {
	this.initialize(img.motamot);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,933,700);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.video = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.008)").s().p("EgtTBLAMAAAiV/MBanAAAMAAACV/g");
	this.shape.setTransform(290,479.975);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.video, new cjs.Rectangle(0,0,580,960), null);


(lib.txt_hashtag = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgmAiIANgQQAQANANAAQAGAAADgCQADgDAAgFQAAgDgDgEQgEgCgKgCQgRgEgIgGQgIgGAAgPQAAgOALgHQAKgIANAAQAKABAKADQAKADAHAHIgLAPQgNgJgNgBQgFAAgDADQgDADAAAEQAAAEAEADQAEADANADQAOADAHAGQAIAHAAANQAAAOgKAIQgKAHgQAAQgWAAgTgQg");
	this.shape.setTransform(12.525,94.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBGAAIAAATIgwAAIAAAVIAqAAIAAARIgqAAIAAAVIAxAAIAAATg");
	this.shape_1.setTransform(3.8,94.85);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgKAxIAAhOIgbAAIAAgTIBLAAIAAATIgbAAIAABOg");
	this.shape_2.setTransform(-5.25,94.85);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAeAxIgKgVIgoAAIgJAVIgXAAIAqhhIAVAAIAqBhgAgLAIIAXAAIgMgbg");
	this.shape_3.setTransform(-14.725,94.85);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAQAxIgVgfIgPAAIAAAfIgWAAIAAhhIAlAAQAWAAAKAIQAKAHAAASQAAAWgTAHIAZAjgAgUAAIAQAAQAKAAAEgDQAEgEAAgIQAAgIgEgDQgEgDgKAAIgQAAg");
	this.shape_4.setTransform(-24.675,94.85);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgKAxIAAhhIAVAAIAABhg");
	this.shape_5.setTransform(-32.45,94.85);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgmAxIAAhhIAiAAQAWAAALAJQAKAJAAASQAAARgLAJQgLAHgUABIgNAAIAAAbgAgQADIAOAAQALAAAEgEQADgEAAgIQABgJgGgEQgFgDgKAAIgMAAg");
	this.shape_6.setTransform(-39.15,94.85);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgBAyIACgWIgMAAIgDAWIgVAAIADgWIgNAAIAEgTIANAAIADgQIgPAAIADgUIAPAAIAEgWIAUAAIgDAWIAMAAIADgWIAWAAIgEAWIAOAAIgDAUIgPAAIgDAQIARAAIgEATIgQAAIgDAWgAgHAJIALAAIAEgQIgMAAg");
	this.shape_7.setTransform(-49.475,94.75);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgmAiIANgQQAQANANAAQAGAAADgDQADgCAAgFQAAgDgDgDQgEgDgKgDQgRgDgIgGQgIgGAAgOQAAgOALgIQAKgIANAAQAKAAAKAEQAKADAHAHIgLAQQgNgLgNAAQgFAAgDADQgDADAAAEQAAAEAEADQAEACANAEQAOADAHAGQAIAHAAANQAAANgKAJQgKAHgQABQgWAAgTgRg");
	this.shape_8.setTransform(32.425,73.75);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgeAlQgPgPAAgWQAAgUAPgPQAPgPAVAAQAXAAARASIgNAQQgLgMgPAAQgMAAgJAIQgIAHgBANQAAANAJAJQAJAIAKAAQAQAAALgMIANAOQgQASgXABQgVAAgPgOg");
	this.shape_9.setTransform(23,73.75);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgKAxIAAhhIAVAAIAABhg");
	this.shape_10.setTransform(15.5,73.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgKAxIAAhOIgbAAIAAgTIBLAAIAAATIgbAAIAABOg");
	this.shape_11.setTransform(8.95,73.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgKAxIAAgmIgig7IAYAAIAUAkIAVgkIAYAAIgiA7IAAAmg");
	this.shape_12.setTransform(0.375,73.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgfAxIAAhhIAWAAIAABOIApAAIAAATg");
	this.shape_13.setTransform(-7.525,73.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AAeAxIgKgVIgoAAIgJAVIgXAAIAqhhIAVAAIAqBhgAgLAIIAXAAIgMgbg");
	this.shape_14.setTransform(-17.125,73.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AAXAxIgtg9IAAA9IgWAAIAAhhIAUAAIAvA+IAAg+IAWAAIAABhg");
	this.shape_15.setTransform(-28.075,73.8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AAeAxIgKgVIgoAAIgJAVIgXAAIAqhhIAVAAIAqBhgAgLAIIAXAAIgMgbg");
	this.shape_16.setTransform(-39.025,73.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgUIANAAIADgQIgPAAIADgUIAPAAIAEgWIAUAAIgDAWIAMAAIADgWIAWAAIgEAWIAOAAIgDAUIgPAAIgDAQIARAAIgEAUIgQAAIgDAVgAgHAJIALAAIAEgQIgMAAg");
	this.shape_17.setTransform(-49.475,73.7);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgmAiIANgQQAQAOANAAQAGgBADgDQADgCAAgFQAAgEgDgCQgEgDgKgDQgRgDgIgGQgIgGAAgOQAAgPALgHQAKgIANAAQAKAAAKAEQAKADAHAHIgLAQQgNgKgNAAQgFAAgDACQgDADAAAEQAAAEAEADQAEACANAEQAOADAHAGQAIAHAAANQAAANgKAJQgKAHgQABQgWAAgTgRg");
	this.shape_18.setTransform(12.375,52.7);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AAeAxIgKgVIgoAAIgJAVIgXAAIAqhhIAVAAIAqBhgAgLAJIAXAAIgMgcg");
	this.shape_19.setTransform(2.775,52.75);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgIAxIgohhIAYAAIAYA9IAZg9IAYAAIgoBhg");
	this.shape_20.setTransform(-7.3,52.75);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AAXAxIgtg9IAAA9IgWAAIAAhhIAUAAIAvA+IAAg+IAWAAIAABhg");
	this.shape_21.setTransform(-17.825,52.75);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AAeAxIgKgVIgoAAIgJAVIgXAAIAqhhIAVAAIAqBhgAgLAJIAXAAIgMgcg");
	this.shape_22.setTransform(-28.775,52.75);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgeAkQgPgOAAgWQAAgVAPgOQAPgPAUAAQAZAAAQASIgOAQQgKgMgQAAQgLgBgJAJQgJAHAAANQABANAIAJQAJAIAKAAQAQAAAKgMIAPAOQgSASgWABQgWAAgOgPg");
	this.shape_23.setTransform(-39.05,52.7);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgTIANAAIADgRIgPAAIADgUIAPAAIAEgWIAUAAIgDAWIAMAAIADgWIAWAAIgEAWIAOAAIgDAUIgPAAIgDARIARAAIgEATIgQAAIgDAVgAgHAKIALAAIAEgRIgMAAg");
	this.shape_24.setTransform(-49.475,52.65);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AAXAxIgtg9IAAA9IgWAAIAAhhIAUAAIAvA+IAAg+IAWAAIAABhg");
	this.shape_25.setTransform(58.825,31.7);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgjAkQgPgPAAgVQAAgVAPgOQAOgOAVAAQAWAAAPAOQAOAOAAAVQAAAVgOAPQgPAPgWAAQgVAAgOgPgAgTgVQgJAJAAAMQAAANAJAKQAIAIALABQAMgBAJgIQAIgKAAgNQAAgMgIgJQgJgJgMAAQgLAAgIAJg");
	this.shape_26.setTransform(47.325,31.65);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgKAxIAAhhIAVAAIAABhg");
	this.shape_27.setTransform(39.25,31.7);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AgKAxIAAhOIgbAAIAAgTIBLAAIAAATIgbAAIAABOg");
	this.shape_28.setTransform(32.7,31.7);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AAeAxIgKgVIgoAAIgJAVIgXAAIAqhhIAVAAIAqBhgAgLAJIAXAAIgMgcg");
	this.shape_29.setTransform(23.225,31.7);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AgeAkQgPgOAAgWQAAgUAPgPQAPgOAVAAQAYgBAQATIgOAPQgKgMgQAAQgLgBgIAJQgKAHABANQAAANAIAJQAJAIAKAAQAQAAALgNIANAPQgRATgWAAQgVgBgPgOg");
	this.shape_30.setTransform(12.95,31.65);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AgKAxIAAhhIAVAAIAABhg");
	this.shape_31.setTransform(5.45,31.7);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AggAxIAAhhIBBAAIAAATIgsAAIAAAWIAqAAIAAASIgqAAIAAAmg");
	this.shape_32.setTransform(-0.8,31.7);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AgKAxIAAhhIAVAAIAABhg");
	this.shape_33.setTransform(-7.65,31.7);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AAhAxIAAg9IgbA1IgMAAIgag1IAAA9IgWAAIAAhhIAeAAIAYA1IAZg1IAeAAIAABhg");
	this.shape_34.setTransform(-16.625,31.7);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AAeAxIgKgVIgoAAIgJAVIgXAAIAqhhIAVAAIAqBhgAgLAJIAXAAIgMgcg");
	this.shape_35.setTransform(-28.525,31.7);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AgeAkQgOgOAAgWQAAgUAOgPQAPgOAUAAQAWAAAPANIgMARQgGgGgFgDQgGgBgHAAQgLAAgJAIQgIAJAAAMQAAAOAIAJQAIAIAKAAQAMAAAHgFIAAgaIAXAAIAAAiQgPAQgaABQgUgBgPgOg");
	this.shape_36.setTransform(-39.05,31.65);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgTIANAAIADgRIgPAAIADgTIAPAAIAEgXIAUAAIgDAXIAMAAIADgXIAWAAIgEAXIAOAAIgDATIgPAAIgDARIARAAIgEATIgQAAIgDAVgAgHAKIALAAIAEgRIgMAAg");
	this.shape_37.setTransform(-49.475,31.6);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("AAXAxIgtg9IAAA9IgWAAIAAhhIAUAAIAvA+IAAg+IAWAAIAABhg");
	this.shape_38.setTransform(48.275,10.65);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AgjAkQgPgPAAgVQAAgVAPgOQAOgOAVAAQAWAAAPAOQAOAOAAAVQAAAVgOAPQgPAPgWAAQgVAAgOgPgAgTgVQgJAJAAAMQAAANAJAKQAIAIALABQAMgBAJgIQAIgKAAgNQAAgMgIgJQgJgKgMABQgLgBgIAKg");
	this.shape_39.setTransform(36.775,10.6);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("AgKAxIAAhhIAVAAIAABhg");
	this.shape_40.setTransform(28.7,10.65);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("AgKAxIAAhOIgcAAIAAgTIBNAAIAAATIgcAAIAABOg");
	this.shape_41.setTransform(22.15,10.65);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("AgmAxIAAhhIAjAAQAVAAALAJQAKAJAAASQAAARgLAIQgLAJgUgBIgNAAIAAAcgAgQADIAOAAQALAAAEgDQADgFAAgJQABgIgGgDQgFgEgJAAIgNAAg");
	this.shape_42.setTransform(13.5,10.65);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBFAAIAAAUIgvAAIAAAUIArAAIAAASIgrAAIAAATIAxAAIAAAUg");
	this.shape_43.setTransform(4.05,10.65);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFFFFF").s().p("AgeAkQgPgOAAgWQAAgUAPgPQAPgOAVAAQAXgBARATIgNAPQgLgMgPAAQgMgBgJAJQgIAHgBANQAAAOAJAIQAJAIAKAAQAQAAALgNIANAPQgQATgXAAQgVgBgPgOg");
	this.shape_44.setTransform(-5.8,10.6);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFFFFF").s().p("AAXAxIgtg9IAAA9IgWAAIAAhhIAUAAIAvA+IAAg+IAWAAIAABhg");
	this.shape_45.setTransform(-16.725,10.65);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FFFFFF").s().p("AgjAkQgPgPAAgVQAAgVAPgOQAOgOAVAAQAWAAAPAOQAOAOAAAVQAAAVgOAPQgPAPgWAAQgVAAgOgPgAgTgVQgJAJAAAMQAAANAJAKQAIAIALABQAMgBAJgIQAIgKAAgNQAAgMgIgJQgJgKgMABQgLgBgIAKg");
	this.shape_46.setTransform(-28.225,10.6);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFFFFF").s().p("AgeAkQgPgOAAgWQAAgUAPgPQAPgOAUAAQAZgBAQATIgOAPQgKgMgQAAQgLgBgJAJQgJAHAAANQABAOAIAIQAJAIAKAAQAQAAAKgNIAPAPQgSATgWAAQgWgBgOgOg");
	this.shape_47.setTransform(-39.05,10.6);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgTIANAAIADgRIgPAAIADgTIAPAAIAEgXIAUAAIgDAXIAMAAIADgXIAWAAIgEAXIAOAAIgDATIgPAAIgDARIARAAIgEATIgQAAIgDAVgAgHAKIALAAIAEgRIgMAAg");
	this.shape_48.setTransform(-49.475,10.55);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.txt_hashtag, new cjs.Rectangle(-56.7,0,123.5,105.3), null);


(lib.perso6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.instance = new lib.Calque4();
	this.instance.parent = this;
	this.instance.setTransform(-78.5,-150);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.perso6, new cjs.Rectangle(-78.5,-150,157,300), null);


(lib.perso5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.instance = new lib.Calque3();
	this.instance.parent = this;
	this.instance.setTransform(-65.5,-142);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.perso5, new cjs.Rectangle(-65.5,-142,131,284), null);


(lib.perso4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.instance = new lib._27_perroquet_rouge();
	this.instance.parent = this;
	this.instance.setTransform(-113.5,-154);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.perso4, new cjs.Rectangle(-113.5,-154,227,308), null);


(lib.perso3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.instance = new lib.Calque5();
	this.instance.parent = this;
	this.instance.setTransform(-101.5,-141);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.perso3, new cjs.Rectangle(-101.5,-141,203,282), null);


(lib.perso2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.instance = new lib.Calque2();
	this.instance.parent = this;
	this.instance.setTransform(-180,-254.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.perso2, new cjs.Rectangle(-180,-254.5,360,509), null);


(lib.perso1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.instance = new lib.Calque1();
	this.instance.parent = this;
	this.instance.setTransform(-167,-182);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.perso1, new cjs.Rectangle(-167,-182,334,364), null);


(lib.mongol_move = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.instance = new lib.mongol();
	this.instance.parent = this;
	this.instance.setTransform(-58.5,-70);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.mongol_move, new cjs.Rectangle(-58.5,-70,117,140), null);


(lib.masqueTexte_1_ligne = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("EgicAEJIAAoRMBE5AAAIAAIRg");
	this.shape.setTransform(0,24.55);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.masqueTexte_1_ligne, new cjs.Rectangle(-220.5,-1.9,441,53), null);


(lib.masque_generique = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#11C8D8").s().p("AsQCqIAAlTIYhAAIAAFTg");
	this.shape.setTransform(78.5,17);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.masque_generique, new cjs.Rectangle(0,0,157,34), null);


(lib.ligne = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(49));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#333333").ss(1,1,1).p("EAAAg/bMAAAB+3");
	this.shape.setTransform(0,406);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#333333").ss(1,1,1).p("EAA/A/cQhDv0ggwoQgdwxAExFQAEwXAhwHQAfufA4to");
	this.shape_1.setTransform(-6.0239,406);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#333333").ss(1,1,1).p("EABwA/cQh4vfg5w8Qg0wqAHxQQAHweA7wDQA5uxBjtQ");
	this.shape_2.setTransform(-10.6719,406);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#333333").ss(1,1,1).p("EACVA/cQigvPhLxLQhHwmAKxYQAJwjBPv/QBMvACEs9");
	this.shape_3.setTransform(-14.2371,406);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#333333").ss(1,1,1).p("EACwA/cQi+vDhZxWQhTwjAMxeQAKwnBev8QBavKCcsw");
	this.shape_4.setTransform(-16.895,406);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#333333").ss(1,1,1).p("EADEA/cQjTu7hjxdQhdwhANxhQAMwrBov6QBkvSCusm");
	this.shape_5.setTransform(-18.7801,406);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#333333").ss(1,1,1).p("EADSA/cQjiu1hqxjQhjwfAOxlQAMwsBvv5QBsvXC6sf");
	this.shape_6.setTransform(-20.0814,406);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#333333").ss(1,1,1).p("EADbA/cQjsuxhuxnQhnweAOxnQANwtB0v4QBwvaDCsb");
	this.shape_7.setTransform(-20.9145,406);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#333333").ss(1,1,1).p("EADgA/cQjyuuhxxpQhpwdAPxpQANwuB3v3QByvdDHsY");
	this.shape_8.setTransform(-21.4184,406);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#333333").ss(1,1,1).p("EADjA/cQj1uthyxqQhrwdAPxpQANwvB5v3QB0vdDJsX");
	this.shape_9.setTransform(-21.7172,406);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#333333").ss(1,1,1).p("EADkA/cQj2uthzxqQhswdAPxpQANwvB6v3QB1veDKsW");
	this.shape_10.setTransform(-21.8418,406);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#333333").ss(1,1,1).p("EADlA/cQj3ush0xrQhrwdAPxpQANwvB5v3QB2veDLsW");
	this.shape_11.setTransform(-21.8918,406);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#333333").ss(1,1,1).p("EADlA/cQj3ushzxrQhswdAPxpQANwvB6v3QB1veDLsW");
	this.shape_12.setTransform(-21.9165,406);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#333333").ss(1,1,1).p("EADlg/bQjLMWh1PeQh6P3gNQvQgPRpBsQdQBzRrD3Os");
	this.shape_13.setTransform(-21.9165,406);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#333333").ss(1,1,1).p("EADlA/cQj3ushzxrQhswdAPxpQANwvB5v3QB2veDLsW");
	this.shape_14.setTransform(-21.8915,406);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#333333").ss(1,1,1).p("EADlA/cQj3ushzxrQhswdAPxpQAOwvB5v3QB1veDLsW");
	this.shape_15.setTransform(-21.8665,406);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#333333").ss(1,1,1).p("EADkA/cQj2uthzxqQhrwdAPxpQANwvB5v3QB1veDKsW");
	this.shape_16.setTransform(-21.7865,406);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#333333").ss(1,1,1).p("EADjA/cQj0uthzxqQhqwdAOxpQAOwvB4v2QB0veDJsX");
	this.shape_17.setTransform(-21.6619,406);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#333333").ss(1,1,1).p("EADhA/cQjyuuhyxpQhpwdAOxpQANwuB4v3QBzvdDHsY");
	this.shape_18.setTransform(-21.4626,406);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#333333").ss(1,1,1).p("EADeA/cQjvuvhwxoQhoweAOxoQANwuB2v3QBxvcDFsZ");
	this.shape_19.setTransform(-21.2133,406);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#333333").ss(1,1,1).p("EADaA/cQjruxhuxnQhnweAPxmQAMwuB0v4QBwvaDBsb");
	this.shape_20.setTransform(-20.8645,406);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#333333").ss(1,1,1).p("EADWA/cQjnuzhrxlQhlweAOxmQANwsBxv5QBtvZC+sd");
	this.shape_21.setTransform(-20.4106,406);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#333333").ss(1,1,1).p("EADQA/cQjgu2hpxiQhiwfAOxlQAMwsBuv4QBrvXC4sg");
	this.shape_22.setTransform(-19.8817,406);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#333333").ss(1,1,1).p("EADJA/cQjZu4hlxgQhewgANxjQAMwrBqv6QBnvTCysk");
	this.shape_23.setTransform(-19.1786,406);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#333333").ss(1,1,1).p("EADAA/cQjPu8hhxcQhawhAMxhQAMwqBlv7QBjvQCqso");
	this.shape_24.setTransform(-18.3758,406);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#333333").ss(1,1,1).p("EAC2A/cQjEvAhcxZQhWwiAMxfQALwoBhv8QBdvMChst");
	this.shape_25.setTransform(-17.4235,406);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#333333").ss(1,1,1).p("EACqA/cQi3vFhWxUQhRwjAMxdQAKwnBav8QBXvICXsz");
	this.shape_26.setTransform(-16.2969,406);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#333333").ss(1,1,1).p("EACdA/cQipvLhPxPQhKwkAKxaQAKwlBTv+QBQvDCLs5");
	this.shape_27.setTransform(-15.0095,406);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#333333").ss(1,1,1).p("EACOA/cQiYvShIxIQhCwnAJxWQAIwiBLwAQBJu9B9tB");
	this.shape_28.setTransform(-13.5536,406);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#333333").ss(1,1,1).p("EAB8A/cQiFvZg/xCQg6woAIxSQAHwhBCwBQA/u2ButK");
	this.shape_29.setTransform(-11.8731,406);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#333333").ss(1,1,1).p("EABpA/cQhxvig1w5QgxwrAHxPQAGwdA3wDQA2uvBdtT");
	this.shape_30.setTransform(-10.0434,406);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#333333").ss(1,1,1).p("EABYA/cQhevpgswyQgqwuAGxKQAFwbAvwFQAsuoBOtc");
	this.shape_31.setTransform(-8.3629,406);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#333333").ss(1,1,1).p("EABIA/cQhNvwgkwsQgiwvAFxHQAEwYAmwHQAkujBAtj");
	this.shape_32.setTransform(-6.9071,406);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#333333").ss(1,1,1).p("EAA7A/cQg/v2gdwmQgcwxAExEQADwXAgwIQAdudA0tq");
	this.shape_33.setTransform(-5.6198,406);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#333333").ss(1,1,1).p("EAAvA/cQgyv7gYwhQgWwzADxBQADwVAZwJQAXuZAqtw");
	this.shape_34.setTransform(-4.493,406);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#333333").ss(1,1,1).p("EAAlA/cQgnv/gTweQgRwzACxAQACwTAUwKQASuVAht1");
	this.shape_35.setTransform(-3.5406,406);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#333333").ss(1,1,1).p("EAAcA/cQgdwDgPwaQgNw1ACw9QABwSAQwLQANuSAZt5");
	this.shape_36.setTransform(-2.7379,406);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#333333").ss(1,1,1).p("EAAVA/cQgWwGgLwXQgKw2ACw7QABwSALwMQAKuPATt8");
	this.shape_37.setTransform(-2.0348,406);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#333333").ss(1,1,1).p("EAAQA/cQgQwIgIwVQgHw3ABw6QABwRAIwMQAHuNAOt/");
	this.shape_38.setTransform(-1.5059,406);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#333333").ss(1,1,1).p("EAALA/cQgLwLgFwSQgFw3AAw6QABwQAGwMQAFuMAJuB");
	this.shape_39.setTransform(-1.0523,406);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#333333").ss(1,1,1).p("EAAIA/cQgIwMgDwSQgEw3ABw5QAAwPAEwNQADuKAHuD");
	this.shape_40.setTransform(-0.7031,406);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#333333").ss(1,1,1).p("EAAFA/cQgFwNgBwRQgDw3ABw5QAAwOACwOQACuJAEuE");
	this.shape_41.setTransform(-0.4542,406);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#333333").ss(1,1,1).p("EAADA/cQgDwOAAwQQgCw3ABw4QAAwOABwPQAAuIADuF");
	this.shape_42.setTransform(-0.2562,406);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#333333").ss(1,1,1).p("EAABA/cQgBwOAAwQQgBw4ABw3QAAwOAAwOQAAuIABuG");
	this.shape_43.setTransform(-0.1333,406);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#333333").ss(1,1,1).p("EAAAA/cQAAwPAAwPQAAw4AAw3QAAwOAAwOQAAuIAAuG");
	this.shape_44.setTransform(-0.05,406);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#333333").ss(1,1,1).p("EAAAA/cQAAwPAAwPQAAw4AAw3QAAwOAAwOQAAuHAAuH");
	this.shape_45.setTransform(-0.025,406);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45,p:{x:-0.025}}]},1).to({state:[{t:this.shape_45,p:{x:0}}]},1).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-45.8,-1,47.8,814);


(lib.lettres_FAT = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#11C8D8").s().p("ADTHaIhBiXIkkAAIhBCXIilAAIEtq6ICWAAIEvK6gAhXC6ICuAAIhWjLgAhDknIiKhvICahDIB+Cyg");
	this.shape.setTransform(-63.7,299.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#11C8D8").s().p("AkEEBQhqhnAAiaQAAiZBqhnQBqhoCaAAQCbAABqBoQBqBnAACZQAACahqBnQhqBoibAAQiaAAhqhogAiSieQg8BCAABcQAABeA8BBQA8BCBXAAQBXAAA8hCQA8hBAAheQAAhcg8hCQg8hChXAAQhXAAg8BCg");
	this.shape_1.setTransform(-59.65,63.35);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.lettres_FAT, new cjs.Rectangle(-102.9,0,119.4,373.7), null);


(lib.hastaggraphicbranche = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AjQAuIAShbIDlAAICqAAIgSBbg");
	this.shape.setTransform(0,4.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.hastaggraphicbranche, new cjs.Rectangle(-20.8,0,41.7,9.2), null);


(lib.decors = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.instance = new lib.motamot();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.decors, new cjs.Rectangle(0,0,933,700), null);


(lib.Btn_cliquable_area = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#11C8D8").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape.setTransform(0,30);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Btn_cliquable_area, new cjs.Rectangle(-113,0,226,60), null);


(lib.mongol_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.instance = new lib.mongol_move();
	this.instance.parent = this;
	this.instance.setTransform(0,0,1,1,4.0004);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({rotation:0,x:-0.25,y:20},59,cjs.Ease.quadInOut).to({rotation:-3.9995,x:0,y:0},60,cjs.Ease.quadInOut).to({rotation:0,x:-0.25,y:20},60,cjs.Ease.quadInOut).to({rotation:3.9997,x:0,y:0},60,cjs.Ease.quadInOut).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-63.2,-73.9,126.5,164);


(lib.masqueTexte = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// masqueTexte_1_ligne
	this.instance = new lib.masqueTexte_1_ligne();
	this.instance.parent = this;
	this.instance.setTransform(-218.8,191.5,0.0079,1,0,0,0,0,25.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(6).to({_off:false},0).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.0079,x:218.8},14).wait(1));

	// masqueTexte_1_ligne
	this.instance_1 = new lib.masqueTexte_1_ligne();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-218.8,136.1,0.0079,1,0,0,0,0,25.5);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.0079,x:218.8},14).to({_off:true},1).wait(2));

	// masqueTexte_1_ligne
	this.instance_2 = new lib.masqueTexte_1_ligne();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-218.8,80.8,0.0079,1,0,0,0,0,25.5);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(2).to({_off:false},0).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.0079,x:218.8},14).to({_off:true},1).wait(4));

	// masqueTexte_1_ligne
	this.instance_3 = new lib.masqueTexte_1_ligne();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-218.8,25.5,0.0079,1,0,0,0,0,25.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.0079,x:218.8},14).to({_off:true},1).wait(6));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-220.5,-1.9,441.1,219);


(lib.mask_fond = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{out:59});

	// timeline functions:
	this.frame_58 = function() {
		this.stop();
	}
	this.frame_106 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(58).call(this.frame_58).wait(48).call(this.frame_106).wait(5));

	// Calque_2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_2 = new cjs.Graphics().p("EAXwAw1UgABgABgvegR9UAvfhPqAAAgABMAAABhpIAAAAg");
	var mask_graphics_3 = new cjs.Graphics().p("EAXwAw1UgABgABgvegR9UAvfhPqAAAgABMAAABhpIAAAAg");
	var mask_graphics_4 = new cjs.Graphics().p("EAXwAw1UgABgABgvegR9UAvfhPqAAAgABMAAABhpIAAAAg");
	var mask_graphics_5 = new cjs.Graphics().p("EAXwAw1UgABgABgvegR9UAvfhPqAAAgABMAAABhpIAAAAg");
	var mask_graphics_6 = new cjs.Graphics().p("EAXwAw1UgABgABgvegR9UAvfhPqAAAgABMAAABhpIAAAAg");
	var mask_graphics_7 = new cjs.Graphics().p("EAXwAw1UgABgABgvegR9UAvfhPqAAAgABMAAABhpIAAAAg");
	var mask_graphics_8 = new cjs.Graphics().p("EAXwAw1UgABgABgvegR9UAvfhPqAAAgABMAAABhpIAAAAg");
	var mask_graphics_9 = new cjs.Graphics().p("EAXwAw1UgABgABgvegR9UAvfhPqAAAgABMAAABhpIAAAAg");
	var mask_graphics_10 = new cjs.Graphics().p("EAXwAw1UgABgABgvegR9UAvfhPqAAAgABMAAABhpIAAAAg");
	var mask_graphics_11 = new cjs.Graphics().p("EAXwAw1UgABgABgvegR9UAvfhPqAAAgABMAAABhpIAAAAg");
	var mask_graphics_12 = new cjs.Graphics().p("EAXwAw1UgABgABgvegR9UAvfhPqAAAgABMAAABhpIAAAAg");
	var mask_graphics_13 = new cjs.Graphics().p("EAXwAw1UgABgABgvegR9UAvfhPqAAAgABMAAABhpIAAAAg");
	var mask_graphics_14 = new cjs.Graphics().p("EAXwAw1UgABgABgvegR9UAvfhPqAAAgABMAAABhpIAAAAg");
	var mask_graphics_15 = new cjs.Graphics().p("EAXwAw1UgABgABgvegR9UAvfhPqAAAgABMAAABhpIAAAAg");
	var mask_graphics_16 = new cjs.Graphics().p("EAXwAw1UgABgABgvegR9UAvfhPqAAAgABMAAABhpIAAAAg");
	var mask_graphics_17 = new cjs.Graphics().p("EAXwAw1UgABgABgvegR9UAvfhPqAAAgABMAAABhpIAAAAg");
	var mask_graphics_18 = new cjs.Graphics().p("EAXwAw1UgABgABgvegR9UAvfhPqAAAgABMAAABhpIAAAAg");
	var mask_graphics_19 = new cjs.Graphics().p("EAXwAw1UgABgABgvegR9UAvfhPqAAAgABMAAABhpIAAAAg");
	var mask_graphics_20 = new cjs.Graphics().p("EAXwAw1UgABgABgvegR9UAvfhPqAAAgABMAAABhpIAAAAg");
	var mask_graphics_21 = new cjs.Graphics().p("EAXwAw1UgABgABgvegR9UAvfhPqAAAgABMAAABhpIAAAAg");
	var mask_graphics_22 = new cjs.Graphics().p("EAXwAw1UgABgABgvegR9UAvfhPqAAAgABMAAABhpIAAAAg");
	var mask_graphics_23 = new cjs.Graphics().p("EAXwAw1UgABgABgvegR9UAvfhPqAAAgABMAAABhpIAAAAg");
	var mask_graphics_24 = new cjs.Graphics().p("EAXwAw1UgABgABgvegR9UAvfhPqAAAgABMAAABhpIAAAAg");
	var mask_graphics_25 = new cjs.Graphics().p("EAXwAw1UgABgABgvegR9UAvfhPqAAAgABMAAABhpIAAAAg");
	var mask_graphics_26 = new cjs.Graphics().p("EAXqAw1UgABgABgvegR9UAvfhPqAAAgABMAAABhpIAAAAg");
	var mask_graphics_27 = new cjs.Graphics().p("EAXQAw1UgABgABgvegR9UAvfhPqAAAgABMAAABhpIAAAAg");
	var mask_graphics_28 = new cjs.Graphics().p("EAW3Aw1UgABgABgvegR9UAvfhPqAAAgABMAAABhpIAAAAg");
	var mask_graphics_29 = new cjs.Graphics().p("EAWgAw1UgABgABgvegR9UAvfhPqAAAgABMAAABhpIAAAAg");
	var mask_graphics_30 = new cjs.Graphics().p("EAWKAw1UgABgABgvegR9UAvfhPqAAAgABMAAABhpIAAAAg");
	var mask_graphics_31 = new cjs.Graphics().p("EAV2Aw1UgABgABgvegR9UAvfhPqAAAgABMAAABhpIAAAAg");
	var mask_graphics_32 = new cjs.Graphics().p("EAVjAw1UgABgABgvegR9UAvfhPqAAAgABMAAABhpIAAAAg");
	var mask_graphics_33 = new cjs.Graphics().p("EAVRAw1UgABgABgvegR9UAvfhPqAAAgABMAAABhpIAAAAg");
	var mask_graphics_34 = new cjs.Graphics().p("EAVBAw1UgABgABgvegR9UAvfhPqAAAgABMAAABhpIAAAAg");
	var mask_graphics_35 = new cjs.Graphics().p("EAUyAw1UAAAgABgvfgR9UAvghPqgABgABMAAABhpIAAAAg");
	var mask_graphics_36 = new cjs.Graphics().p("EAUlAw1UgABgABgvegR9UAvfhPqAAAgABMAAABhpIAAAAg");
	var mask_graphics_37 = new cjs.Graphics().p("EAUZAw1UgABgABgvegR9UAvfhPqAAAgABMAAABhpIAAAAg");
	var mask_graphics_38 = new cjs.Graphics().p("EAUOAw1UAAAgABgvfgR9UAvghPqgABgABMAAABhpIAAAAg");
	var mask_graphics_39 = new cjs.Graphics().p("EAUFAw1UAAAgABgvfgR9UAvghPqgABgABMAAABhpIAAAAg");
	var mask_graphics_40 = new cjs.Graphics().p("EAT9Aw1UAAAgABgvfgR9UAvghPqgABgABMAAABhpIAAAAg");
	var mask_graphics_41 = new cjs.Graphics().p("EAT3Aw1UgABgABgvegR9UAvghPqgABgABMAAABhpIAAAAg");
	var mask_graphics_42 = new cjs.Graphics().p("EATyAw1UAAAgABgvfgR9UAvghPqgABgABMAAABhpIAAAAg");
	var mask_graphics_43 = new cjs.Graphics().p("EATvAw1UgABgABgvegR9UAvfhPqAAAgABMAAABhpIAAAAg");
	var mask_graphics_44 = new cjs.Graphics().p("EATsAw1UAAAgABgvfgR9UAvghPqgABgABMAAABhpIAAAAg");
	var mask_graphics_45 = new cjs.Graphics().p("EATsAw1UgABgABgvegR9UAvfhPqAAAgABMAAABhpIAAAAg");
	var mask_graphics_68 = new cjs.Graphics().p("EATsAw1UgABgABgvegR9UAvfhPqAAAgABMAAABhpIAAAAg");
	var mask_graphics_69 = new cjs.Graphics().p("EAToAwyUAAAgABgvcgR8UAvdhPlgABgABMAAABhjIAAAAg");
	var mask_graphics_70 = new cjs.Graphics().p("EATfAwrUgABgABgvVgR5UAvWhPaAAAgABMAAABhVIAAAAg");
	var mask_graphics_71 = new cjs.Graphics().p("EATOAwgUAAAgABgvKgR2UAvLhPHgABgABMAAABg/IAAAAg");
	var mask_graphics_72 = new cjs.Graphics().p("EAS3AwPMgu7gRwUAu7hOsAAAgABMAAABgdIAAAAg");
	var mask_graphics_73 = new cjs.Graphics().p("EASZAv6MgumgRoUAunhOKgABgABMAAABfzIAAAAg");
	var mask_graphics_74 = new cjs.Graphics().p("EAR1AvhMguNgRfUAuOhNhgABgABMAAABfBIAAAAg");
	var mask_graphics_75 = new cjs.Graphics().p("EARKAvCMgtvgRUUAtwhMugABgABMAAABeDIAAAAg");
	var mask_graphics_76 = new cjs.Graphics().p("EAQZAufMgtOgRHUAtOhL1AAAgABMAAABc9IAAAAg");
	var mask_graphics_77 = new cjs.Graphics().p("EAPhAt3MgsngQ4UAsnhK0AAAgABMAAABbtIAAAAg");
	var mask_graphics_78 = new cjs.Graphics().p("EAOiAtLMgr8gQoUAr8hJsAAAgABMAAABaVIAAAAg");
	var mask_graphics_79 = new cjs.Graphics().p("EANdAsaMgrMgQWUArMhIcAAAgABMAAABYzIAAAAg");
	var mask_graphics_80 = new cjs.Graphics().p("EAMRArkMgqYgQCUAqYhHEAAAgABMAAABXHIAAAAg");
	var mask_graphics_81 = new cjs.Graphics().p("EAK+AqqMgpegPtUApfhFlgABgABMAAABVTIAAAAg");
	var mask_graphics_82 = new cjs.Graphics().p("EAJlApqMgohgPVUAoihD+gABAAAMAAABTTIAAAAg");
	var mask_graphics_83 = new cjs.Graphics().p("EAIGAonMgnfgO9UAnfhCPAAAgABMAAABRNIAAAAg");
	var mask_graphics_84 = new cjs.Graphics().p("EAGgAneMgmZgOiUAmZhAYAAAgABMAAABO7IAAAAg");
	var mask_graphics_85 = new cjs.Graphics().p("EAEzAmRMglOgOFMAlOg+cMAAABMhIAAAAg");
	var mask_graphics_86 = new cjs.Graphics().p("EAC/Ak/Mgj+gNnMAj+g8WMAAABJ9IAAAAg");
	var mask_graphics_87 = new cjs.Graphics().p("EABFAjpMgiqgNIMAiqg6JMAAABHRIAAAAg");
	var mask_graphics_88 = new cjs.Graphics().p("EgA6AiOMghSgMmMAhSg31MAAABEbIAAAAg");
	var mask_graphics_89 = new cjs.Graphics().p("EgDBAguI/1sDMAf1g1YMAAABBbIAAAAg");
	var mask_graphics_90 = new cjs.Graphics().p("AlPfJI+TrdMAeTgy0MAAAA+RIAAAAg");
	var mask_graphics_91 = new cjs.Graphics().p("AnkdgI8sq3MAcsgwIMAAAA6/IAAAAg");
	var mask_graphics_92 = new cjs.Graphics().p("Ap/byI7CqOMAbCgtVMAAAA3jIAAAAg");
	var mask_graphics_93 = new cjs.Graphics().p("AsgZ/I5TpkMAZTgqZMAAAAz9IAAAAg");
	var mask_graphics_94 = new cjs.Graphics().p("AvIYII3fo4MAXfgnXMAAAAwPIAAAAg");
	var mask_graphics_95 = new cjs.Graphics().p("Ax3WMI1moKMAVmgkNMAAAAsXIAAAAg");
	var mask_graphics_96 = new cjs.Graphics().p("A0tUMIzoncMATogg7MAAAAoXIAAAAg");
	var mask_graphics_97 = new cjs.Graphics().p("A3pSGIxnmqIRn9hMAAAAkLIAAAAg");
	var mask_graphics_98 = new cjs.Graphics().p("A6rP+Ivhl3IPh6BIAAf4IAAAAg");
	var mask_graphics_99 = new cjs.Graphics().p("A90PSItWlDINW2YIAAbbIAAAAg");
	var mask_graphics_100 = new cjs.Graphics().p("EghEAOkIrHkNILHynIAAW0IAAAAg");
	var mask_graphics_101 = new cjs.Graphics().p("EgtNAKgIIzuvIAASEIozjVg");
	var mask_graphics_102 = new cjs.Graphics().p("EguSAKpIGbqvIAANLImbicg");
	var mask_graphics_103 = new cjs.Graphics().p("EgvYAKzID9mqIAAIKIj9hgg");
	var mask_graphics_104 = new cjs.Graphics().p("EgwhAK8IBciaIAAC9Ihcgjg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(2).to({graphics:mask_graphics_2,x:55.9625,y:22.425}).wait(1).to({graphics:mask_graphics_3,x:44.0101,y:22.425}).wait(1).to({graphics:mask_graphics_4,x:32.3389,y:22.425}).wait(1).to({graphics:mask_graphics_5,x:20.949,y:22.425}).wait(1).to({graphics:mask_graphics_6,x:9.8403,y:22.425}).wait(1).to({graphics:mask_graphics_7,x:-0.9872,y:22.425}).wait(1).to({graphics:mask_graphics_8,x:-11.5334,y:22.425}).wait(1).to({graphics:mask_graphics_9,x:-21.7985,y:22.425}).wait(1).to({graphics:mask_graphics_10,x:-31.7822,y:22.425}).wait(1).to({graphics:mask_graphics_11,x:-41.4848,y:22.425}).wait(1).to({graphics:mask_graphics_12,x:-50.9061,y:22.425}).wait(1).to({graphics:mask_graphics_13,x:-60.0462,y:22.425}).wait(1).to({graphics:mask_graphics_14,x:-68.905,y:22.425}).wait(1).to({graphics:mask_graphics_15,x:-77.4826,y:22.425}).wait(1).to({graphics:mask_graphics_16,x:-85.779,y:22.425}).wait(1).to({graphics:mask_graphics_17,x:-93.7941,y:22.425}).wait(1).to({graphics:mask_graphics_18,x:-101.528,y:22.425}).wait(1).to({graphics:mask_graphics_19,x:-108.9807,y:22.425}).wait(1).to({graphics:mask_graphics_20,x:-116.1522,y:22.425}).wait(1).to({graphics:mask_graphics_21,x:-123.0424,y:22.425}).wait(1).to({graphics:mask_graphics_22,x:-129.6513,y:22.425}).wait(1).to({graphics:mask_graphics_23,x:-135.9791,y:22.425}).wait(1).to({graphics:mask_graphics_24,x:-142.0256,y:22.425}).wait(1).to({graphics:mask_graphics_25,x:-147.7909,y:22.425}).wait(1).to({graphics:mask_graphics_26,x:-152.6437,y:22.425}).wait(1).to({graphics:mask_graphics_27,x:-155.2451,y:22.425}).wait(1).to({graphics:mask_graphics_28,x:-157.7059,y:22.425}).wait(1).to({graphics:mask_graphics_29,x:-160.0261,y:22.425}).wait(1).to({graphics:mask_graphics_30,x:-162.2056,y:22.425}).wait(1).to({graphics:mask_graphics_31,x:-164.2446,y:22.425}).wait(1).to({graphics:mask_graphics_32,x:-166.1429,y:22.425}).wait(1).to({graphics:mask_graphics_33,x:-167.9006,y:22.425}).wait(1).to({graphics:mask_graphics_34,x:-169.5177,y:22.425}).wait(1).to({graphics:mask_graphics_35,x:-170.9942,y:22.425}).wait(1).to({graphics:mask_graphics_36,x:-172.33,y:22.425}).wait(1).to({graphics:mask_graphics_37,x:-173.5253,y:22.425}).wait(1).to({graphics:mask_graphics_38,x:-174.5799,y:22.425}).wait(1).to({graphics:mask_graphics_39,x:-175.4939,y:22.425}).wait(1).to({graphics:mask_graphics_40,x:-176.2673,y:22.425}).wait(1).to({graphics:mask_graphics_41,x:-176.9001,y:22.425}).wait(1).to({graphics:mask_graphics_42,x:-177.3922,y:22.425}).wait(1).to({graphics:mask_graphics_43,x:-177.7438,y:22.425}).wait(1).to({graphics:mask_graphics_44,x:-177.9547,y:22.425}).wait(1).to({graphics:mask_graphics_45,x:-178.025,y:22.425}).wait(23).to({graphics:mask_graphics_68,x:-178.025,y:22.425}).wait(1).to({graphics:mask_graphics_69,x:-178.1273,y:22.5138}).wait(1).to({graphics:mask_graphics_70,x:-178.4342,y:22.7803}).wait(1).to({graphics:mask_graphics_71,x:-178.9457,y:23.2245}).wait(1).to({graphics:mask_graphics_72,x:-179.6618,y:23.8463}).wait(1).to({graphics:mask_graphics_73,x:-180.5825,y:24.6458}).wait(1).to({graphics:mask_graphics_74,x:-181.7078,y:25.623}).wait(1).to({graphics:mask_graphics_75,x:-183.0377,y:26.7778}).wait(1).to({graphics:mask_graphics_76,x:-184.5722,y:28.1103}).wait(1).to({graphics:mask_graphics_77,x:-186.3114,y:29.6205}).wait(1).to({graphics:mask_graphics_78,x:-188.2551,y:31.3083}).wait(1).to({graphics:mask_graphics_79,x:-190.4034,y:33.1738}).wait(1).to({graphics:mask_graphics_80,x:-192.7563,y:35.2169}).wait(1).to({graphics:mask_graphics_81,x:-195.3138,y:37.4377}).wait(1).to({graphics:mask_graphics_82,x:-198.076,y:39.8362}).wait(1).to({graphics:mask_graphics_83,x:-201.0427,y:42.4124}).wait(1).to({graphics:mask_graphics_84,x:-204.214,y:45.1662}).wait(1).to({graphics:mask_graphics_85,x:-207.5899,y:48.0977}).wait(1).to({graphics:mask_graphics_86,x:-211.1705,y:51.2068}).wait(1).to({graphics:mask_graphics_87,x:-214.9556,y:54.4936}).wait(1).to({graphics:mask_graphics_88,x:-218.9453,y:57.9581}).wait(1).to({graphics:mask_graphics_89,x:-223.1397,y:61.6003}).wait(1).to({graphics:mask_graphics_90,x:-227.5386,y:65.4201}).wait(1).to({graphics:mask_graphics_91,x:-232.1421,y:69.4175}).wait(1).to({graphics:mask_graphics_92,x:-236.9503,y:73.5927}).wait(1).to({graphics:mask_graphics_93,x:-241.963,y:77.9455}).wait(1).to({graphics:mask_graphics_94,x:-247.1804,y:82.476}).wait(1).to({graphics:mask_graphics_95,x:-252.6023,y:87.1841}).wait(1).to({graphics:mask_graphics_96,x:-258.2289,y:92.0699}).wait(1).to({graphics:mask_graphics_97,x:-264.06,y:97.1334}).wait(1).to({graphics:mask_graphics_98,x:-270.0958,y:102.2144}).wait(1).to({graphics:mask_graphics_99,x:-276.3361,y:97.7929}).wait(1).to({graphics:mask_graphics_100,x:-282.7811,y:93.2264}).wait(1).to({graphics:mask_graphics_101,x:-289.4306,y:88.5149}).wait(1).to({graphics:mask_graphics_102,x:-296.2848,y:83.6585}).wait(1).to({graphics:mask_graphics_103,x:-303.3435,y:78.6571}).wait(1).to({graphics:mask_graphics_104,x:-310.6069,y:73.5108}).wait(1).to({graphics:null,x:0,y:0}).wait(6));

	// Calque_4
	this.instance = new lib.decors();
	this.instance.parent = this;
	this.instance.setTransform(-244.75,-17.15,0.9264,0.9264,0,0,0,399.7,299.6);
	this.instance._off = true;

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(2).to({_off:false},0).to({_off:true},103).wait(6));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-615,-290,907.1,624.9);


(lib.lignes_all = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{close:114,close_lines:157});

	// timeline functions:
	this.frame_113 = function() {
		this.stop();
	}
	this.frame_156 = function() {
		this.stop();
	}
	this.frame_174 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(113).call(this.frame_113).wait(43).call(this.frame_156).wait(18).call(this.frame_174).wait(1));

	// ligne
	this.instance = new lib.ligne();
	this.instance.parent = this;
	this.instance.setTransform(-454.05,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(114).to({x:-883},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:-454.05},0).wait(18));

	// ligne
	this.instance_1 = new lib.ligne();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-227.05,406.5,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(114).to({x:-883.2},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:-227.05},0).wait(18));

	// ligne
	this.instance_2 = new lib.ligne();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-0.05,406.5,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(114).to({x:-883.4},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:-0.05},0).wait(18));

	// ligne
	this.instance_3 = new lib.ligne();
	this.instance_3.parent = this;
	this.instance_3.setTransform(226.95,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(93).to({x:227,y:322},20,cjs.Ease.quartInOut).wait(1).to({y:323},0).to({x:-883.05,y:406},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:227,y:322},0).to({x:226.95,y:406},16,cjs.Ease.quartInOut).wait(2));

	// ligne
	this.instance_4 = new lib.ligne();
	this.instance_4.parent = this;
	this.instance_4.setTransform(454.1,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(114).to({x:-883.45},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:454.1},0).wait(18));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-884.4,-85,1339.5,898.5);


(lib.hashtag_graphic = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(64));

	// hastag graphic branche
	this.instance = new lib.hastaggraphicbranche();
	this.instance.parent = this;
	this.instance.setTransform(37.35,51.9,1.0844,0.9952,0,-78.3276,102.1252,0.1,4.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({x:6.45,y:50.05},17,cjs.Ease.quartOut).to({_off:true},5).wait(3).to({_off:false},0).to({_off:true},4).wait(2).to({_off:false,x:26.45},0).to({_off:true},5).wait(28));

	// hastag graphic branche
	this.instance_1 = new lib.hastaggraphicbranche();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-41.65,49.9,1.0844,0.9952,0,-78.3276,102.1252,0.1,4.6);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off:false},0).to({x:-7.4,y:49.8},18,cjs.Ease.quartOut).to({_off:true},4).wait(5).to({_off:false},0).to({_off:true},4).wait(31));

	// hastag graphic branche
	this.instance_2 = new lib.hastaggraphicbranche();
	this.instance_2.parent = this;
	this.instance_2.setTransform(2.7,21.7,1,1,0,0,0,0,4.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({x:-2.3,y:58.7},13,cjs.Ease.quartOut).to({_off:true},11).wait(3).to({_off:false,scaleX:2.3297,scaleY:1.0257,skewX:-12.8379,x:25.5},0).to({_off:true},4).wait(33));

	// hastag graphic branche
	this.instance_3 = new lib.hastaggraphicbranche();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-7.3,77.7,1,1,0,0,0,0,4.6);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({x:0.7,y:41.7},14,cjs.Ease.quartOut).to({_off:true},4).wait(7).to({_off:false,regX:0.1,scaleX:1.9808,scaleY:1.0147,skewX:-9.7728,x:19.9,y:49.7},0).to({_off:true},4).wait(4).to({_off:false,regX:0,scaleX:1,scaleY:1,skewX:0,x:0.7,y:41.7},0).to({_off:true},4).wait(21));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-50.9,0,126.1,82.3);


(lib.Btn_next = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{rollOver:55,rollOut:70});

	// timeline functions:
	this.frame_54 = function() {
		this.stop();
	}
	this.frame_69 = function() {
		this.stop();
	}
	this.frame_84 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(54).call(this.frame_54).wait(15).call(this.frame_69).wait(15).call(this.frame_84).wait(1));

	// ZONE
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.008)").s().p("EgjTAEsIAApXMBGnAAAIAAJXg");
	this.shape.setTransform(113,29.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(85));

	// txt
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgIAqIAAhDIgYAAIAAgQIBBAAIAAAQIgYAAIAABDg");
	this.shape_1.setTransform(161.05,29.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AASAqIgSgcIgQAcIgYAAIAdgqIgagpIAWAAIAPAZIAQgZIAWAAIgaApIAcAqg");
	this.shape_2.setTransform(145.2,29.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgeAqIAAhTIA8AAIAAARIgpAAIAAARIAkAAIAAAPIgkAAIAAASIAqAAIAAAQg");
	this.shape_3.setTransform(129.325,29.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAUAqIgngzIAAAzIgTAAIAAhTIASAAIAoA1IAAg1IATAAIAABTg");
	this.shape_4.setTransform(112.3,29.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgKAQIAEgOQgEgCAAgFQAAgEADgDQADgDAEAAQAFAAADADQADADAAAEQAAAEgDAEIgIANg");
	this.shape_5.setTransform(86.525,32.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAPAqIgYghIgIAKIAAAXIgTAAIAAhTIATAAIAAAjIAegjIAYAAIgiAlIAQAWIASAYg");
	this.shape_6.setTransform(76.975,29.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgeAfQgNgNAAgSQAAgSANgMQANgNARAAQASAAAOANQAMAMAAASQAAASgMANQgOAMgSAAQgRAAgNgMgAgRgSQgHAIAAAKQAAALAHAIQAIAIAJAAQAKAAAIgIQAHgIAAgLQAAgKgHgIQgIgIgKAAQgJAAgIAIg");
	this.shape_7.setTransform(59.3,29.05);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#333333").ss(1,1,1).p("EgjOAADMBGdgAF");
	this.shape_8.setTransform(113,-0.25);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(85));

	// Layer_2
	this.instance = new lib.Btn_cliquable_area();
	this.instance.parent = this;
	this.instance.setTransform(113,29.5,2,1,0,0,0,0,30);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("rgba(0,0,0,0.008)").s().p("EgjTAEsIAApXMBGnAAAIAAJXg");
	this.shape_9.setTransform(113,29.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance}]},3).to({state:[{t:this.instance}]},46).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.instance}]},5).to({state:[{t:this.instance}]},14).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},14).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({alpha:0.0117},46).to({_off:true},1).wait(5).to({_off:false},0).to({alpha:1},14).wait(1).to({alpha:0.0117},14).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-113.5,-1.5,453,61);


(lib.Btn_detail = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"rollOver":55,"rollOut":70});

	// timeline functions:
	this.frame_54 = function() {
		this.stop();
	}
	this.frame_69 = function() {
		this.stop();
	}
	this.frame_84 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(54).call(this.frame_54).wait(15).call(this.frame_69).wait(15).call(this.frame_84).wait(1));

	// ZONE
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.008)").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape.setTransform(0,29.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(85));

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgOAyIAAhkIAdAAIAABkg");
	this.shape_1.setTransform(-85.45,29.55);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgyAOIAAgcIBlAAIAAAcg");
	this.shape_2.setTransform(-85.45,29.55);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgaAqIAAhTIASAAIAABDIAjAAIAAAQg");
	this.shape_3.setTransform(38.425,29.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgIAqIAAhTIARAAIAABTg");
	this.shape_4.setTransform(24.675,29.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AAZAqIgHgSIgjAAIgHASIgUAAIAkhTIARAAIAkBTgAgKAIIAUAAIgKgYg");
	this.shape_5.setTransform(10.225,29.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgIAqIAAhDIgYAAIAAgQIBBAAIAAAQIgYAAIAABDg");
	this.shape_6.setTransform(-5.9,29.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgeAqIAAhTIA8AAIAAARIgpAAIAAARIAkAAIAAAPIgkAAIAAASIAqAAIAAAQg");
	this.shape_7.setTransform(-21.275,29.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AglAqIAAhTIAeAAQAVAAAMALQAMALAAATQAAATgMALQgMAMgXAAgAgTAaIANAAQAMAAAGgHQAHgGAAgNQAAgLgHgHQgGgGgOgBIgLAAg");
	this.shape_8.setTransform(-37.8,29.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#333333").ss(1,1,1).p("AxpAAMAjTAAA");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(85));

	// Layer_2
	this.instance = new lib.Btn_cliquable_area();
	this.instance.parent = this;
	this.instance.setTransform(0,29.5,1,1,0,0,0,0,30);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("rgba(0,0,0,0.008)").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape_10.setTransform(0,29.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance}]},3).to({state:[{t:this.instance}]},45).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.instance}]},6).to({state:[{t:this.instance}]},14).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},13).wait(2));
	this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({alpha:0.0117},45).to({_off:true},1).wait(6).to({_off:false},0).to({alpha:1},14).wait(1).to({alpha:0.0117},13).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-114,-1,228,60.5);


(lib.bloc_visuel = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":68});

	// timeline functions:
	this.frame_67 = function() {
		this.stop();
	}
	this.frame_68 = function() {
		this.background.gotoAndPlay("out");
	}
	this.frame_110 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(67).call(this.frame_67).wait(1).call(this.frame_68).wait(42).call(this.frame_110).wait(10));

	// Calque_15
	this.instance = new lib.mongol_1();
	this.instance.parent = this;
	this.instance.setTransform(158.6,-1.95,1,1,-44.9994,0,0,-0.1,-29.9);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(4).to({_off:false},0).to({regX:0,regY:-30,rotation:21.1921,x:-298.55,y:-251.05},34,cjs.Ease.sineOut).to({rotation:-8.8072,x:-288.55,y:-251.1},17,cjs.Ease.quadInOut).to({regX:-0.1,regY:-30.1,rotation:-0.619,x:-288.65,y:-251.2},13,cjs.Ease.quadInOut).wait(1).to({rotation:-0.619},0).to({regX:0,rotation:-19.5683,x:-268.6,y:-241.2},15,cjs.Ease.quartInOut).to({regX:-0.1,regY:-30.2,scaleX:0.9999,scaleY:0.9999,rotation:-4.5688,x:-741.65,y:-291.3},17,cjs.Ease.quartIn).wait(19));

	// masque VISUEL (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("EgdDA3JMAAAhuRMA6HAAAMAAABuRg");
	mask.setTransform(-187.225,42.025);

	// Calque 1.png
	this.instance_1 = new lib.perso1();
	this.instance_1.parent = this;
	this.instance_1.setTransform(107.9,264.3,0.814,0.814,29.9989,0,0,0.1,0);
	this.instance_1._off = true;

	var maskedShapeInstanceList = [this.instance_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({regX:0,rotation:0,x:18.65,y:216.55},25,cjs.Ease.quartInOut).wait(57).to({x:8.65,y:555.9},21,cjs.Ease.quartIn).to({_off:true},3).wait(10));

	// Calque 2.png
	this.instance_2 = new lib.perso2();
	this.instance_2.parent = this;
	this.instance_2.setTransform(150.2,-60.15,0.814,0.814,37.2383);
	this.instance_2._off = true;

	var maskedShapeInstanceList = [this.instance_2];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(6).to({_off:false},0).to({rotation:0,x:24.8,y:17.3},25,cjs.Ease.quartInOut).wait(46).to({x:-5.2,y:600.85},21,cjs.Ease.quartIn).to({_off:true},12).wait(10));

	// Calque 5.png
	this.instance_3 = new lib.perso3();
	this.instance_3.parent = this;
	this.instance_3.setTransform(76.6,291.1,0.814,0.814,14.9982);
	this.instance_3._off = true;

	var maskedShapeInstanceList = [this.instance_3];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(20).to({_off:false},0).to({scaleX:0.6833,scaleY:0.6833,rotation:0,x:-48.3,y:306.5},25,cjs.Ease.quartInOut).wait(43).to({x:-68.3,y:514.85},21,cjs.Ease.quartIn).to({_off:true},1).wait(10));

	// 27_perroquet_rouge.png
	this.instance_4 = new lib.perso4();
	this.instance_4.parent = this;
	this.instance_4.setTransform(150.8,271.65,0.814,0.814,14.9982,0,0,0.1,0.1);
	this.instance_4._off = true;

	var maskedShapeInstanceList = [this.instance_4];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(9).to({_off:false},0).to({regX:0,regY:0,rotation:0,x:-92.85,y:166.45},25,cjs.Ease.quartInOut).wait(49).to({x:-122.85,y:542.05},21,cjs.Ease.quartIn).to({_off:true},6).wait(10));

	// Calque 3.png
	this.instance_5 = new lib.perso5();
	this.instance_5.parent = this;
	this.instance_5.setTransform(61.95,94.1,0.814,0.814);
	this.instance_5._off = true;

	var maskedShapeInstanceList = [this.instance_5];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(12).to({_off:false},0).to({x:-60.85,y:37.85},25,cjs.Ease.quartInOut).wait(36).to({x:-100.85,y:527.4},21,cjs.Ease.quartIn).to({_off:true},16).wait(10));

	// Calque 4.png
	this.instance_6 = new lib.perso6();
	this.instance_6.parent = this;
	this.instance_6.setTransform(79.9,-121.6,0.814,0.814,24.4407,0,0,0,-0.1);
	this.instance_6._off = true;

	var maskedShapeInstanceList = [this.instance_6];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(16).to({_off:false},0).to({regY:0,rotation:0,x:-40.4,y:-181.55},25,cjs.Ease.quartInOut).wait(27).to({x:-50.4,y:533.35},21,cjs.Ease.quartIn).to({_off:true},21).wait(10));

	// Calque_12
	this.background = new lib.mask_fond();
	this.background.name = "background";
	this.background.parent = this;
	this.background.setTransform(50,20);
	this.background._off = true;

	var maskedShapeInstanceList = [this.background];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.background).wait(23).to({_off:false},0).to({_off:true},87).wait(10));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-798.3,-331.8,1068.1999999999998,726.7);


(lib.bloc_video = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":41});

	// timeline functions:
	this.frame_40 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(40).call(this.frame_40).wait(17));

	// masque video (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_42 = new cjs.Graphics().p("Ega8A8AMAAAh3/MA7rAAAMAAAB3/g");
	var mask_graphics_43 = new cjs.Graphics().p("Ega8A8AMAAAh3/MA7qAAAMAAAB3/g");
	var mask_graphics_44 = new cjs.Graphics().p("Ega0A8AMAAAh3/MA7aAAAMAAAB3/g");
	var mask_graphics_45 = new cjs.Graphics().p("EgaSA8AMAAAh3/MA6VAAAMAAAB3/g");
	var mask_graphics_46 = new cjs.Graphics().p("EgY1A8AMAAAh3/MA3bAAAMAAAB3/g");
	var mask_graphics_47 = new cjs.Graphics().p("EgVyA8AMAAAh3/MAxSAAAMAAAB3/g");
	var mask_graphics_48 = new cjs.Graphics().p("EgQQA8AMAAAh3/MAmIAAAMAAAB3/g");
	var mask_graphics_49 = new cjs.Graphics().p("EgIMA8AMAAAh3/IV3AAMAAAB3/g");
	var mask_graphics_50 = new cjs.Graphics().p("EgCpA8AMAAAh3/IKsAAMAAAB3/g");
	var mask_graphics_51 = new cjs.Graphics().p("EAAZA8AMAAAh3/IEkAAMAAAB3/g");
	var mask_graphics_52 = new cjs.Graphics().p("EAB1A8AMAAAh3/IBqAAMAAAB3/g");
	var mask_graphics_53 = new cjs.Graphics().p("EACXA8AMAAAh3/IAlAAMAAAB3/g");
	var mask_graphics_54 = new cjs.Graphics().p("EACfA8AMAAAh3/IAVAAMAAAB3/g");
	var mask_graphics_55 = new cjs.Graphics().p("EACgA8AMAAAh3/IAUAAMAAAB3/g");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(42).to({graphics:mask_graphics_42,x:209.4828,y:0}).wait(1).to({graphics:mask_graphics_43,x:209.4292,y:0}).wait(1).to({graphics:mask_graphics_44,x:208.6245,y:0}).wait(1).to({graphics:mask_graphics_45,x:205.1378,y:0}).wait(1).to({graphics:mask_graphics_46,x:195.7504,y:0}).wait(1).to({graphics:mask_graphics_47,x:175.9563,y:0}).wait(1).to({graphics:mask_graphics_48,x:139.9623,y:0}).wait(1).to({graphics:mask_graphics_49,x:87.4933,y:0}).wait(1).to({graphics:mask_graphics_50,x:51.4993,y:0}).wait(1).to({graphics:mask_graphics_51,x:31.7053,y:0}).wait(1).to({graphics:mask_graphics_52,x:22.3178,y:0}).wait(1).to({graphics:mask_graphics_53,x:18.8311,y:0}).wait(1).to({graphics:mask_graphics_54,x:18.0265,y:0}).wait(1).to({graphics:mask_graphics_55,x:17.973,y:0}).wait(1).to({graphics:null,x:0,y:0}).wait(1));

	// VIDEO
	this.videoContainer = new lib.video();
	this.videoContainer.name = "videoContainer";
	this.videoContainer.parent = this;
	this.videoContainer.setTransform(36.7,765,0.6587,0.6587,0,0,0,0.3,480.2);

	var maskedShapeInstanceList = [this.videoContainer];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.videoContainer).to({y:-33},40,cjs.Ease.quartInOut).to({_off:true},16).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-349.3,418.6,1430.3999999999999);


(lib.bloc_NEXT_btn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":20});

	// timeline functions:
	this.frame_19 = function() {
		this.stop();
	}
	this.frame_36 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(19).call(this.frame_19).wait(17).call(this.frame_36).wait(1));

	// CTA DETAIL
	this.btnNext = new lib.Btn_next();
	this.btnNext.name = "btnNext";
	this.btnNext.parent = this;
	this.btnNext.setTransform(-340.5,411.5,1,1,0,0,0,0,26.5);

	this.timeline.addTween(cjs.Tween.get(this.btnNext).to({y:350.5},19,cjs.Ease.quartInOut).wait(2).to({y:411.5},14,cjs.Ease.quartInOut).to({_off:true},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-454,0,453,444.5);


(lib.bloc_hashtag = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_61 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(61).call(this.frame_61).wait(1));

	// Layer_1
	this.instance = new lib.hashtag_graphic("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(-456.05,243.2,2.3169,2.3169,0,0,0,-0.1,50.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true},61).wait(1));

	// Layer_5 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_28 = new cjs.Graphics().p("AtWBpIAAjRIatAAIAADRg");
	var mask_graphics_32 = new cjs.Graphics().p("AtWDSIAAmjIatAAIAAGjg");
	var mask_graphics_40 = new cjs.Graphics().p("AtWIoIAAxQIatAAIAARQg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(28).to({graphics:mask_graphics_28,x:-400.45,y:265.45}).wait(4).to({graphics:mask_graphics_32,x:-400.45,y:255.025}).wait(8).to({graphics:mask_graphics_40,x:-400.45,y:270.3}).wait(22));

	// Layer_3
	this.instance_1 = new lib.txt_hashtag();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-421.5,244.05,1,1,0,0,0,0,31.6);
	this.instance_1._off = true;

	var maskedShapeInstanceList = [this.instance_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(28).to({_off:false},0).wait(34));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-573.7,0,287.1,317.7);


(lib.bloc_CTA = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":25});

	// timeline functions:
	this.frame_24 = function() {
		this.stop();
	}
	this.frame_41 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(24).call(this.frame_24).wait(17).call(this.frame_41).wait(1));

	// CTA DETAIL
	this.btnCta = new lib.Btn_detail();
	this.btnCta.name = "btnCta";
	this.btnCta.parent = this;
	this.btnCta.setTransform(-340.5,411.5,1,1,0,0,0,0,26.5);

	this.timeline.addTween(cjs.Tween.get(this.btnCta).to({y:350.5},24,cjs.Ease.quartInOut).wait(2).to({y:411.5},14,cjs.Ease.quartInOut).to({_off:true},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-454.5,0,228,444.5);


(lib.blocInfostextes = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":80});

	// timeline functions:
	this.frame_79 = function() {
		this.stop();
	}
	this.frame_120 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(79).call(this.frame_79).wait(41).call(this.frame_120).wait(1));

	// Bloc Hashtag
	this.instance = new lib.bloc_hashtag();
	this.instance.parent = this;
	this.instance.setTransform(-391.5,261.2,0.9,0.9,0,0,0,-412.9,244.7);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(62).to({_off:false},0).to({_off:true},18).wait(41));

	// masque Titre
	this.instance_1 = new lib.masque_generique();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-527.95,-13.9,0.0075,0.4711,0,0,0,0,17.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(34).to({_off:false},0).to({regX:0.5,scaleX:1.9223,x:-527.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.0128,x:-228.9},10,cjs.Ease.quartInOut).to({_off:true},1).wait(26).to({_off:false},0).to({regX:0.5,scaleX:1.9223,x:-527.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.0074,x:-528.7},10,cjs.Ease.quartInOut).to({_off:true},1).wait(21));

	// _NOM-du-CLIENT
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#11C8D8").s().p("AgMA4IAAhZIgfAAIAAgWIBXAAIAAAWIggAAIAABZg");
	this.shape.setTransform(-332.525,-14.15);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#11C8D8").s().p("AgpApQgRgQAAgZQAAgXARgRQARgRAYAAQAZAAARARQARARAAAXQAAAZgRAQQgRARgZAAQgYAAgRgRgAgXgYQgJAKAAAOQAAAPAJALQAKAKANAAQAOAAAKgKQAJgLAAgPQAAgOgJgKQgKgLgOAAQgNAAgKALg");
	this.shape_1.setTransform(-355.975,-14.225);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#11C8D8").s().p("AAlA4IAAhFIgeA7IgOAAIgdg7IAABFIgZAAIAAhvIAhAAIAcA9IAdg9IAiAAIAABvg");
	this.shape_2.setTransform(-382.2,-14.15);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#11C8D8").s().p("AAiBMIgLgYIgtAAIgKAYIgbAAIAwhvIAXAAIAwBvgAgNAeIAbAAIgOgggAgKguIgWgSIAYgLIAUAdg");
	this.shape_3.setTransform(-424,-16.125);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#11C8D8").s().p("AgMA4IAAhZIgfAAIAAgWIBXAAIAAAWIggAAIAABZg");
	this.shape_4.setTransform(-463.025,-14.15);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#11C8D8").s().p("AgpApQgRgQAAgZQAAgXARgRQARgRAYAAQAZAAARARQARARAAAXQAAAZgRAQQgRARgZAAQgYAAgRgRgAgXgYQgJAKAAAOQAAAPAJALQAKAKANAAQAOAAAKgKQAJgLAAgPQAAgOgJgKQgKgLgOAAQgNAAgKALg");
	this.shape_5.setTransform(-486.475,-14.225);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#11C8D8").s().p("AAlA4IAAhFIgeA7IgOAAIgdg7IAABFIgZAAIAAhvIAhAAIAcA9IAdg9IAiAAIAABvg");
	this.shape_6.setTransform(-512.7,-14.15);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},34).to({state:[]},55).to({state:[]},1).wait(31));

	// DECO 1
	this.instance_2 = new lib.bateau1();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-161,37,0.4389,0.4389);

	this.instance_3 = new lib.bateau3();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-36.1,-3,0.5199,0.5447,0,0,180);

	this.instance_4 = new lib.bateau2();
	this.instance_4.parent = this;
	this.instance_4.setTransform(-337,-117,0.439,0.439);

	this.instance_5 = new lib.bateau4();
	this.instance_5.parent = this;
	this.instance_5.setTransform(-500,11,0.5001,0.5001);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_2,p:{scaleX:0.4389,scaleY:0.4389,skewY:0,x:-161,y:37}}]},15).to({state:[{t:this.instance_4,p:{x:-337,y:-117}},{t:this.instance_3,p:{scaleX:0.5199,scaleY:0.5447,x:-36.1,y:-3}},{t:this.instance_2,p:{scaleX:0.2575,scaleY:0.2488,skewY:180,x:-419.35,y:70}}]},7).to({state:[{t:this.instance_4,p:{x:-587,y:-76}},{t:this.instance_3,p:{scaleX:0.3093,scaleY:0.324,x:-343.9,y:107}}]},8).to({state:[{t:this.instance_5}]},8).to({state:[]},8).wait(75));

	// masque Titre (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_17 = new cjs.Graphics().p("EgpOArJMAAAhWRMAvJAAAMAAABWRg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(17).to({graphics:mask_graphics_17,x:-263.875,y:-6.1095}).wait(24).to({graphics:null,x:0,y:0}).wait(80));

	// _LETTRES_ANARCHIQUES
	this.instance_6 = new lib.lettres_FAT();
	this.instance_6.parent = this;
	this.instance_6.setTransform(-433.85,-73.05,1,1,0,0,0,0,186.8);
	this.instance_6._off = true;

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#11C8D8").s().p("AhNFeIAAozIjGAAIAAiHIInAAIAACHIjGAAIAAIzg");
	this.shape_7.setTransform(-197.2,7.85);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#11C8D8").s().p("AhNFdIAAoyIjGAAIAAiHIInAAIAACHIjGAAIAAIyg");
	this.shape_8.setTransform(-358.6,-116);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#11C8D8").s().p("ADrFdIAAm2Ii9F8IhcAAIi8l8IAAG2IicAAIAAq5IDTAAICzF+IC1l+IDSAAIAAK5g");
	this.shape_9.setTransform(-487.1,-116);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#11C8D8").s().p("AkEEBQhqhnAAiaQAAiZBqhoQBqhnCaAAQCbAABqBnQBqBoAACZQAACahqBnQhqBoibAAQiaAAhqhogAiSieQg8BCAABdQAABdA8BBQA8BCBXAAQBXAAA8hCQA8hBAAhdQAAhdg8hCQg8hChXAAQhXAAg8BCg");
	this.shape_10.setTransform(-322.9,106.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#11C8D8").s().p("ADUHaIhBiXIklAAIhACXIinAAIEuq7ICXAAIEtK7gAhWC6ICtAAIhXjLgAhCknIiMhvICbhCIB/Cxg");
	this.shape_11.setTransform(-288.55,-29.55);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#11C8D8").s().p("ADrFeIAAm3Ii9F8IhcAAIi8l8IAAG3IicAAIAAq7IDTAAICzGAIC1mAIDSAAIAAK7g");
	this.shape_12.setTransform(-470.1,-141);

	var maskedShapeInstanceList = [this.instance_6,this.shape_7,this.shape_8,this.shape_9,this.shape_10,this.shape_11,this.shape_12];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_6}]},17).to({state:[{t:this.instance_6}]},14).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7}]},1).to({state:[{t:this.shape_12},{t:this.shape_11},{t:this.shape_10}]},7).to({state:[]},9).to({state:[]},32).wait(41));
	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(17).to({_off:false},0).to({x:-276.85},14,cjs.Ease.quartOut).to({_off:true},1).wait(89));

	// masque Titre
	this.instance_7 = new lib.masque_generique();
	this.instance_7.parent = this;
	this.instance_7.setTransform(221.1,-163.9,0.0128,0.4711,0,0,0,0,17.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_7).to({regX:0.5,scaleX:1.9223,x:-75.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.0074,x:-68.7},10,cjs.Ease.quartInOut).to({_off:true},1).wait(101));

	// masque Titre
	this.instance_8 = new lib.masque_generique();
	this.instance_8.parent = this;
	this.instance_8.setTransform(-228.9,-13.9,0.0128,0.4711,0,0,0,0,17.2);
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(4).to({_off:false},0).to({regX:0.5,scaleX:1.9223,x:-527.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.0074,x:-528.7},10,cjs.Ease.quartInOut).to({_off:true},1).wait(97));

	// _LETTRES_CLIENT
	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#11C8D8").s().p("AgJA0QgEgEAAgGQAAgFAEgFQAEgEAFAAQAGAAAEAEQAEAFAAAFQAAAGgEAEQgEAFgGAAQgFAAgEgFgAgJAVIgEg5IAAgUIAbAAIAAAUIgFA5g");
	this.shape_13.setTransform(-357.925,-14.075);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#11C8D8").s().p("AAbA4Ig1hFIAABFIgZAAIAAhvIAYAAIA2BHIAAhHIAZAAIAABvg");
	this.shape_14.setTransform(-378.95,-14.15);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#11C8D8").s().p("AgoA4IAAhvIBPAAIAAAXIg2AAIAAAWIAxAAIAAAUIgxAAIAAAYIA4AAIAAAWg");
	this.shape_15.setTransform(-402.45,-14.15);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#11C8D8").s().p("AgMA4IAAhvIAZAAIAABvg");
	this.shape_16.setTransform(-422.55,-14.15);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#11C8D8").s().p("AgCA5IADgYIgNAAIgEAYIgZAAIAFgYIgQAAIAFgWIAPAAIADgTIgRAAIAEgWIARAAIAEgaIAXAAIgDAaIANAAIAEgaIAZAAIgFAaIARAAIgEAWIgRAAIgDATIASAAIgEAWIgSAAIgEAYgAgJALIAOAAIAEgTIgOAAg");
	this.shape_17.setTransform(-459.375,-14.275);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#11C8D8").s().p("AgjA4IAAhvIAZAAIAABYIAuAAIAAAXg");
	this.shape_18.setTransform(-497.625,-14.15);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#11C8D8").s().p("AgjApQgRgQAAgYQAAgZARgQQASgRAXAAQAcAAASAVIgPASQgMgPgSAAQgNAAgKAJQgJAJgBAPQAAAPAKAKQAJAKANAAQASAAALgPIAQARQgTAVgZAAQgZAAgRgRg");
	this.shape_19.setTransform(-520.4,-14.2);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#11C8D8").s().p("AgMA4IAAhZIgfAAIAAgWIBXAAIAAAWIggAAIAABZg");
	this.shape_20.setTransform(-411.925,-14.15);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#11C8D8").s().p("AAbA4Ig0hFIAABFIgaAAIAAhvIAYAAIA2BHIAAhHIAZAAIAABvg");
	this.shape_21.setTransform(-435.3,-14.15);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16,p:{x:-422.55}},{t:this.shape_15,p:{x:-402.45}},{t:this.shape_14},{t:this.shape_13,p:{x:-357.925}}]},13).to({state:[{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_13,p:{x:-422.675}}]},5).to({state:[{t:this.shape_19},{t:this.shape_18},{t:this.shape_16,p:{x:-478.9}},{t:this.shape_15,p:{x:-458.8}},{t:this.shape_21},{t:this.shape_20}]},5).to({state:[]},11).to({state:[]},46).wait(41));

	// Layer_11
	this.instance_9 = new lib.masqueTexte("synched",0);
	this.instance_9.parent = this;
	this.instance_9.setTransform(-328.95,105.85,0.8999,0.85,0,0,0,0.1,108.9);
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(37).to({_off:false},0).to({_off:true},40).wait(3).to({_off:false,regX:0.2,regY:108.8,rotation:180,x:-329.05,y:105.8},0).to({_off:true},40).wait(1));

	// _TEXTE_PRESENTATION
	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgqBmQgagIgVgRIAbgmQAhAaAmAAQALAAAHgEQAGgFABgHQAAgIgKgGQgKgHgQgFIgcgKQgJgEgMgIQgYgOAAgeQAAgdAYgTQAYgTAlAAQAmAAAmAaIgWAoQgcgUgcAAQgbAAAAAQQAAAIAJAFQAJAEATAHIAdAKQAIADAMAHQAXANAAAgQAAAggYATQgXATgkAAQgXAAgagJg");
	this.shape_22.setTransform(-186,182.175);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAhgeQAggeArAAQAtAAAfAbQAfAbAAAuIAAAfIicAAQADASAQALQAPALATAAQAfAAAVgVIAhAlQgkAigxAAQgwAAgggegAgdgyQgPALgCATIBdAAQgCgUgMgKQgNgKgSAAQgRAAgOAKg");
	this.shape_23.setTransform(-208.3,182.175);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AApBtIAAh4QAAgyglAAQgRAAgNANQgNANAAAZIAAB3Ig9AAIAAjWIA9AAIAAAYQAbgbAgAAQAjAAAXAYQAWAYAAAnIAACCg");
	this.shape_24.setTransform(-233.4,182.025);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AhQBPQghggAAgvQAAguAhggQAgggAwAAQAxAAAgAgQAhAgAAAuQAAAvghAgQggAggxAAQgwAAgggggAglgqQgPARAAAZQAAAbAPAQQAPARAWAAQAXAAAPgRQAPgQAAgbQAAgZgPgRQgPgRgXAAQgWAAgPARg");
	this.shape_25.setTransform(-259.275,182.175);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AAoCVIAAh2QABgwglgBQgRAAgNAOQgOAMAAAYIAAB1Ig7AAIAAkoIA7AAIAABqQAcgcAgAAQAiAAAYAYQAWAYAAAmIAACEg");
	this.shape_26.setTransform(-284.9,178.1);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AhuCUIAAkkIA8AAIAAAZQAcgcAkAAQAmAAAdAfQAeAgAAAvQAAAvgeAgQgdAggngBQgnABgYggIAABqgAgkhQQgPASAAAaQAAAaAPAQQAPAQAUABQAUgBAQgQQAPgPAAgaQAAgbgPgSQgPgRgUAAQgVAAgPARg");
	this.shape_27.setTransform(-310.525,185.9);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AhQBPQghggAAgvQAAguAhggQAgggAwAAQAxAAAgAgQAhAgAAAuQAAAvghAgQggAggxAAQgwAAgggggAglgqQgPARAAAZQAAAbAPAQQAPARAWAAQAXAAAPgRQAPgQAAgbQAAgZgPgRQgPgRgXAAQgWAAgPARg");
	this.shape_28.setTransform(-337.425,182.175);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("ABtBtIAAh0QAAgygkAAQgSAAgNANQgOANAAAZIAABzIg7AAIAAh0QAAgagIgMQgIgMgRAAQgSAAgNANQgNANAAAZIAABzIg8AAIAAjWIA8AAIAAAYQAZgbAfAAQAVAAAQALQAQANAIARQANgUAVgLQAVgKAVAAQAmAAAXAWQAXAXAAAqIAACCg");
	this.shape_29.setTransform(-369.925,182.025);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AhQBPQghggAAgvQAAguAhggQAgggAwAAQAxAAAgAgQAhAgAAAuQAAAvghAgQggAggxAAQgwAAgggggAglgqQgPARAAAZQAAAbAPAQQAPARAWAAQAXAAAPgRQAPgQAAgbQAAgZgPgRQgPgRgXAAQgWAAgPARg");
	this.shape_30.setTransform(-402.675,182.175);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AAoCVIAAh2QAAgwgkgBQgRAAgNAOQgOAMAAAYIAAB1Ig8AAIAAkoIA8AAIAABqQAbgcAiAAQAhAAAXAYQAYAYAAAmIAACEg");
	this.shape_31.setTransform(-428.3,178.1);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AgqBmQgagIgVgRIAagmQAjAaAkAAQAMAAAGgEQAIgFAAgHQAAgIgKgGQgJgHgRgFIgbgKQgKgEgMgIQgYgOAAgeQAAgdAYgTQAYgTAlAAQAmAAAmAaIgXAoQgcgUgcAAQgaAAAAAQQAAAIAJAFQAIAEAUAHIAdAKQAJADAMAHQAWANAAAgQAAAggXATQgYATgkAAQgXAAgagJg");
	this.shape_32.setTransform(-462.45,182.175);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAggeQAhgeAsAAQAsAAAfAbQAfAbAAAuIAAAfIicAAQADASAPALQAQALATAAQAfAAAVgVIAhAlQgkAigxAAQgwAAgggegAgdgyQgPALgDATIBeAAQgCgUgNgKQgMgKgRAAQgSAAgOAKg");
	this.shape_33.setTransform(-484.75,182.175);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AhQB2QgeghAAgwQAAguAdggQAegeAnAAQAmABAZAbIAAhqIA8AAIAAEoIg8AAIAAgdQgaAgglAAQgmAAgegggAgigDQgPARAAAaQAAAaAPAQQAQARATAAQAVAAAPgRQAOgQAAgbQAAgZgOgRQgPgRgVAAQgUAAgPARg");
	this.shape_34.setTransform(-511.125,178.25);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAhgeQAggeArAAQAtAAAfAbQAfAbAAAuIAAAfIicAAQADASAQALQAPALATAAQAfAAAVgVIAhAlQgkAigxAAQgwAAgggegAgdgyQgPALgDATIBeAAQgCgUgMgKQgNgKgRAAQgRAAgPAKg");
	this.shape_35.setTransform(-224.3,133.425);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("Ag1CQQgZgIgSgOIAXguQAdAXAhAAQAgAAAUgRQATgQAAgkQgdAggpAAQgqAAgdgdQgdgcAAgvQAAgwAegfQAegeAmAAQAlAAAaAgIAAgdIA8AAIAAC7QAAAdgKAWQgKAWgRANQghAbgtAAQgYAAgZgIgAgihVQgQAPAAAZQAAAaAPAQQAPAOAVAAQAWAAAOgOQAPgQAAgZQAAgagPgPQgOgPgVAAQgVAAgPAPg");
	this.shape_36.setTransform(-250.675,137.525);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AhJBbQgXgUAAghQAAghAYgPQAYgQApAAIAsAAIAAgCQAAgigkAAQgPAAgRAGQgRAGgNAKIgbgnQArgfA1AAQAnAAAZAUQAZATAAArIAACIIg4AAIAAgaQgYAdghAAQghAAgYgUgAgkAmQAAALAJAHQAIAGARAAQAPAAAMgKQAMgKAAgQIAAgLIglAAQgkAAAAAXg");
	this.shape_37.setTransform(-275.8,133.425);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("AgqBmQgagIgVgRIAbgmQAhAaAmAAQALAAAHgEQAGgFABgHQAAgIgKgGQgKgHgQgFIgbgKQgKgEgMgIQgYgOAAgeQAAgdAYgTQAYgTAlAAQAmAAAmAaIgXAoQgbgUgcAAQgbAAAAAQQAAAIAJAFQAJAEATAHIAdAKQAJADALAHQAXANAAAgQAAAggYATQgXATgkAAQgXAAgagJg");
	this.shape_38.setTransform(-297.95,133.425);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AgqBmQgagIgVgRIAbgmQAhAaAmAAQALAAAHgEQAGgFABgHQAAgIgKgGQgKgHgQgFIgcgKQgJgEgMgIQgYgOAAgeQAAgdAYgTQAYgTAlAAQAmAAAmAaIgWAoQgcgUgcAAQgbAAAAAQQAAAIAJAFQAJAEATAHIAdAKQAIADAMAHQAXANAAAgQAAAggYATQgXATgkAAQgXAAgagJg");
	this.shape_39.setTransform(-318.75,133.425);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("AgdCXIAAjWIA7AAIAADWgAgYhaQgKgKAAgPQAAgPAKgKQAKgKAOAAQAPAAAKAKQAKAKAAAPQAAAPgKAKQgKAKgPAAQgOAAgKgKg");
	this.shape_40.setTransform(-334.625,129.075);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("AggB5QgVgUAAgkIAAhiIgZAAIAAgrIAZAAIAAhAIA8AAIAABAIA0AAIAAArIg0AAIAABgQAAAMAGAHQAGAHAJAAQARAAALgPIAXAqQgcAZgfAAQgeAAgWgUg");
	this.shape_41.setTransform(-348.7,130.375);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("AAoBtIAAh4QAAgygkAAQgRAAgNANQgOANAAAZIAAB3Ig8AAIAAjWIA8AAIAAAYQAbgbAiAAQAiAAAWAYQAYAYAAAnIAACCg");
	this.shape_42.setTransform(-370.7,133.275);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAhgeQAggeArAAQAtAAAfAbQAfAbAAAuIAAAfIicAAQADASAQALQAPALATAAQAfAAAVgVIAhAlQgkAigxAAQgwAAgggegAgdgyQgPALgCATIBdAAQgCgUgMgKQgNgKgSAAQgRAAgOAKg");
	this.shape_43.setTransform(-395.85,133.425);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFFFFF").s().p("Ag9BtIAAjWIA8AAIAAAdQAKgOASgJQARgJASAAIAAA4IgLAAQgbAAgNATQgMATAAAeIAABdg");
	this.shape_44.setTransform(-415.675,133.275);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFFFFF").s().p("AhuCUIAAkkIA8AAIAAAYQAcgbAkAAQAmAAAdAfQAeAfAAAwQAAAvgeAgQgdAggnAAQgnAAgYggIAABqgAgkhQQgPASAAAaQAAAaAPAQQAPARAUAAQAUAAAQgRQAPgQAAgaQAAgZgPgTQgPgRgUAAQgVAAgPARg");
	this.shape_45.setTransform(-437.475,137.15);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FFFFFF").s().p("AhuCUIAAkkIA8AAIAAAYQAcgbAkAAQAmAAAdAfQAeAfAAAwQAAAvgeAgQgdAggnAAQgnAAgYggIAABqgAgkhQQgPASAAAaQAAAaAPAQQAPARAUAAQAUAAAQgRQAPgQAAgaQAAgZgPgTQgPgRgUAAQgVAAgPARg");
	this.shape_46.setTransform(-464.125,137.15);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFFFFF").s().p("AhJBbQgXgUAAghQAAghAYgPQAYgQAqAAIArAAIAAgCQAAgigkAAQgPAAgRAGQgSAGgLAKIgbgnQAqgfA2AAQAmAAAaAUQAYATAAArIAACIIg4AAIAAgaQgYAdggAAQgjAAgXgUgAgkAmQAAALAIAHQAKAGAPAAQAPAAAMgKQANgKAAgQIAAgLIglAAQgkAAAAAXg");
	this.shape_47.setTransform(-490.4,133.425);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FFFFFF").s().p("AgXA2IAAhqIAvAAIAABqg");
	this.shape_48.setTransform(-506.975,121.6);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FFFFFF").s().p("AgdCUIAAkoIA7AAIAAEog");
	this.shape_49.setTransform(-518,129.35);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAhgeQAggeArAAQAtAAAfAbQAfAbAAAuIAAAfIicAAQADASAQALQAPALATAAQAfAAAVgVIAhAlQgkAigxAAQgwAAgggegAgdgyQgPALgCATIBdAAQgCgUgMgKQgNgKgSAAQgRAAgOAKg");
	this.shape_50.setTransform(-319.9,84.675);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FFFFFF").s().p("AhQB2QgeghAAgvQAAgwAdgfQAegdAnAAQAmgBAZAcIAAhqIA8AAIAAEoIg8AAIAAgcQgaAfglAAQgmAAgegggAgigDQgPARAAAaQAAAaAPARQAQAQATAAQAVAAAPgQQAOgRAAgbQAAgagOgQQgPgSgVAAQgUAAgPASg");
	this.shape_51.setTransform(-346.275,80.75);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAggeQAhgeArAAQAtAAAfAbQAfAbAAAuIAAAfIicAAQADASAPALQAQALATAAQAfAAAVgVIAiAlQglAigxAAQgwAAgggegAgdgyQgPALgCATIBdAAQgCgUgNgKQgMgKgSAAQgQAAgPAKg");
	this.shape_52.setTransform(-381.75,84.675);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FFFFFF").s().p("AgdCVIAAkoIA7AAIAAEog");
	this.shape_53.setTransform(-400,80.6);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FFFFFF").s().p("AhJBbQgXgUAAghQAAghAYgPQAYgQApAAIAsAAIAAgCQAAgigkAAQgPAAgRAGQgRAGgNAKIgbgnQArgfA2AAQAnAAAZAUQAYATAAArIAACIIg4AAIAAgaQgXAdgiAAQgiAAgXgUgAgkAmQAAALAJAHQAIAGAQAAQAQAAAMgKQAMgKAAgQIAAgLIglAAQgkAAAAAXg");
	this.shape_54.setTransform(-418.2,84.675);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FFFFFF").s().p("AgfB5QgWgUAAgkIAAhiIgZAAIAAgrIAZAAIAAhAIA7AAIAABAIA0AAIAAArIg0AAIAABgQAAAMAHAHQAGAHAJAAQARAAAMgPIAWAqQgcAZgfAAQgeAAgVgUg");
	this.shape_55.setTransform(-437.85,81.625);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FFFFFF").s().p("AgdCXIAAjWIA7AAIAADWgAgYhaQgKgKAAgPQAAgPAKgKQAKgKAOAAQAPAAAKAKQAKAKAAAPQAAAPgKAKQgKAKgPAAQgOAAgKgKg");
	this.shape_56.setTransform(-453.025,80.325);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FFFFFF").s().p("Ag1CQQgZgIgSgOIAXguQAdAXAhAAQAgAAAUgRQATgQAAgkQgdAggpAAQgqAAgdgdQgdgcAAgvQAAgwAegfQAegeAmAAQAlAAAaAgIAAgdIA8AAIAAC7QAAAdgKAWQgKAWgRANQghAbgtAAQgYAAgZgIgAgihVQgQAPAAAZQAAAaAPAQQAPAOAVAAQAWAAAOgOQAPgQAAgZQAAgagPgPQgOgPgVAAQgVAAgPAPg");
	this.shape_57.setTransform(-472.775,88.775);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#FFFFFF").s().p("AgdCXIAAjWIA7AAIAADWgAgYhaQgKgKAAgPQAAgPAKgKQAKgKAOAAQAPAAAKAKQAKAKAAAPQAAAPgKAKQgKAKgPAAQgOAAgKgKg");
	this.shape_58.setTransform(-491.375,80.325);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#FFFFFF").s().p("AhQB2QgeghAAgvQAAgwAdgfQAegdAnAAQAmgBAZAcIAAhqIA8AAIAAEoIg8AAIAAgcQgaAfglAAQgmAAgegggAgigDQgPARAAAaQAAAaAPARQAQAQATAAQAVAAAPgQQAOgRAAgbQAAgagOgQQgPgSgVAAQgUAAgPASg");
	this.shape_59.setTransform(-511.125,80.75);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#FFFFFF").s().p("AApBtIAAh4QAAgyglAAQgRAAgNANQgNANAAAZIAAB3Ig8AAIAAjWIA8AAIAAAYQAbgbAgAAQAjAAAXAYQAWAYAAAnIAACCg");
	this.shape_60.setTransform(-276.25,35.775);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#FFFFFF").s().p("AhQBPQghggAAgvQAAguAhggQAgggAwAAQAxAAAgAgQAhAgAAAuQAAAvghAgQggAggxAAQgwAAgggggAglgqQgPARAAAZQAAAbAPAQQAPARAWAAQAXAAAPgRQAPgQAAgbQAAgZgPgRQgPgRgXAAQgWAAgPARg");
	this.shape_61.setTransform(-302.125,35.925);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#FFFFFF").s().p("AgdCXIAAjWIA7AAIAADWgAgYhaQgKgKAAgPQAAgPAKgKQAKgKAOAAQAPAAAKAKQAKAKAAAPQAAAPgKAKQgKAKgPAAQgOAAgKgKg");
	this.shape_62.setTransform(-320.925,31.575);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#FFFFFF").s().p("AgfB5QgWgUAAgkIAAhiIgZAAIAAgrIAZAAIAAhAIA7AAIAABAIA1AAIAAArIg1AAIAABgQABAMAGAHQAHAHAIAAQARAAAMgPIAWAqQgcAZgfAAQgeAAgVgUg");
	this.shape_63.setTransform(-335,32.875);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#FFFFFF").s().p("AhJBbQgXgUAAghQAAghAYgPQAYgQApAAIAsAAIAAgCQAAgigkAAQgPAAgRAGQgRAGgNAKIgbgnQArgfA1AAQAnAAAZAUQAZATAAArIAACIIg4AAIAAgaQgXAdgiAAQghAAgYgUgAgkAmQAAALAIAHQAKAGAQAAQAOAAANgKQAMgKAAgQIAAgLIglAAQgkAAAAAXg");
	this.shape_64.setTransform(-356.7,35.925);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#FFFFFF").s().p("AhEBQQgigeAAgyQAAgwAigfQAigfAtAAQAXAAAYAKQAYAJASATIgfApQgLgNgPgGQgOgHgPAAQgXAAgRAPQgRAQAAAaQAAAbARAQQARAPAWAAQAeAAAYgeIAkAoQgqArgwAAQgwAAghgfg");
	this.shape_65.setTransform(-379.525,35.925);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#FFFFFF").s().p("AgdCXIAAjWIA7AAIAADWgAgYhaQgKgKAAgPQAAgPAKgKQAKgKAOAAQAPAAAKAKQAKAKAAAPQAAAPgKAKQgKAKgPAAQgOAAgKgKg");
	this.shape_66.setTransform(-397.225,31.575);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#FFFFFF").s().p("Ag1CXIAAipIgZAAIAAgrIAZAAIAAgNQAAgjAWgVQAVgUAfAAQAgAAAaAYIgWAoQgNgPgQAAQgJAAgGAGQgGAHAAANIAAAOIA0AAIAAArIg0AAIAACpg");
	this.shape_67.setTransform(-409.825,31.575);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#FFFFFF").s().p("AgdCXIAAjWIA7AAIAADWgAgYhaQgKgKAAgPQAAgPAKgKQAKgKAOAAQAPAAAKAKQAKAKAAAPQAAAPgKAKQgKAKgPAAQgOAAgKgKg");
	this.shape_68.setTransform(-424.975,31.575);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#FFFFFF").s().p("ABtBtIAAh0QAAgygkAAQgSAAgNANQgOANAAAZIAABzIg7AAIAAh0QAAgagIgMQgIgMgRAAQgSAAgNANQgNANAAAZIAABzIg8AAIAAjWIA8AAIAAAYQAZgbAfAAQAVAAAQALQAQANAIARQANgUAVgLQAVgKAVAAQAmAAAXAWQAXAXAAAqIAACCg");
	this.shape_69.setTransform(-450.325,35.775);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#FFFFFF").s().p("AhJBbQgXgUAAghQAAghAYgPQAYgQApAAIAsAAIAAgCQAAgigkAAQgPAAgRAGQgRAGgNAKIgbgnQArgfA2AAQAnAAAYAUQAZATAAArIAACIIg4AAIAAgaQgXAdgiAAQghAAgYgUgAgkAmQAAALAJAHQAIAGARAAQAPAAAMgKQAMgKAAgQIAAgLIglAAQgkAAAAAXg");
	this.shape_70.setTransform(-482.45,35.925);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#FFFFFF").s().p("AhYBoQgpgpAAg+QAAg9AqgqQArgqA8AAQA7AAAtAoIggAvQgTgRgQgGQgQgGgTAAQgiAAgZAYQgXAZAAAmQAAAoAXAXQAXAYAfAAQAhAAAVgMIAAhMIA/AAIAABiQgpAvhKgBQg8AAgrgog");
	this.shape_71.setTransform(-509,32.55);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22}]},57).to({state:[]},43).to({state:[]},1).wait(20));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-587,-259.8,813.1,570.5);


// stage content:
(lib.FS_projet_motamot = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":150});

	// timeline functions:
	this.frame_99 = function() {
		//var frequency = 60;
		//stage.enableMouseOver(frequency);
		
		// EVENT DE CLIC POUR LA SUITE
		this.next_btn.addEventListener("click", fl_ClickToGoToAndPlayFromFrame.bind(this));
		this.next_btn.addEventListener("mouseover", fl_MouseOverHandler.bind(this));
		this.next_btn.addEventListener("mouseout", fl_MouseOutHandler.bind(this));
		
		
		function fl_ClickToGoToAndPlayFromFrame()
		{
			//this.gotoAndPlay("close");
			
			var event = new Event('next');
			this.dispatchEvent(event);		
			event = null;
			
			this.next_btn.btnNext.gotoAndPlay('rollOut');
			
		}
		function fl_MouseOverHandler(){	this.next_btn.btnNext.gotoAndPlay('rollOver'); }
		function fl_MouseOutHandler(){	this.next_btn.btnNext.gotoAndPlay('rollOut'); }
		
		
		// EVENT DE CLIC POUR LE LIEN
		this.cta_mc.addEventListener("click", projectDetails.bind(this));
		this.cta_mc.addEventListener("mouseover", pDetails_MouseOverHandler.bind(this));
		this.cta_mc.addEventListener("mouseout", pDetails_MouseOutHandler.bind(this));
		
		function projectDetails()
		{	
			var event = new Event('details');
			this.dispatchEvent(event);		
			event = null;
			
			this.cta_mc.btnCta.gotoAndPlay('rollOut');
			
		}
		function pDetails_MouseOverHandler(){	this.cta_mc.btnCta.gotoAndPlay('rollOver'); }
		function pDetails_MouseOutHandler(){	this.cta_mc.btnCta.gotoAndPlay('rollOut'); }
	}
	this.frame_146 = function() {
		//PAUSE
		this.stop();
	}
	this.frame_150 = function() {
		this.textes_mc.gotoAndPlay("close");
		this.visuel_mc.gotoAndPlay("close");
		this.cta_mc.gotoAndPlay("close");
		this.video_mc.gotoAndPlay("close");
		this.next_btn.gotoAndPlay("close");
		this.lignes_mc.gotoAndPlay("close_lines");
	}
	this.frame_169 = function() {
		//this.lignes_mc.gotoAndPlay("close");
	}
	this.frame_211 = function() {
		// EVENT DE FIN D'ANIM
		var event = new Event('closed');
		this.dispatchEvent(event);		
		event = null;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(99).call(this.frame_99).wait(47).call(this.frame_146).wait(4).call(this.frame_150).wait(19).call(this.frame_169).wait(42).call(this.frame_211).wait(1));

	// BLOC INFOS TEXTE
	this.textes_mc = new lib.blocInfostextes();
	this.textes_mc.name = "textes_mc";
	this.textes_mc.parent = this;
	this.textes_mc.setTransform(858.2,182,1,1,0,0,0,222.1,-164);
	this.textes_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.textes_mc).wait(17).to({_off:false},0).wait(195));

	// masque VISUEL
	this.visuel_mc = new lib.bloc_visuel();
	this.visuel_mc.name = "visuel_mc";
	this.visuel_mc.parent = this;
	this.visuel_mc.setTransform(1192.55,391.5,1,1,0,0,0,552.5,26.5);
	this.visuel_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.visuel_mc).wait(17).to({_off:false},0).wait(195));

	// CTA NEXT
	this.next_btn = new lib.bloc_NEXT_btn();
	this.next_btn.name = "next_btn";
	this.next_btn.parent = this;
	this.next_btn.setTransform(753.2,786.9,1,1,0,0,0,-340.5,403.9);
	this.next_btn._off = true;

	this.timeline.addTween(cjs.Tween.get(this.next_btn).wait(89).to({_off:false},0).wait(123));

	// CTA DETAIL
	this.cta_mc = new lib.bloc_CTA();
	this.cta_mc.name = "cta_mc";
	this.cta_mc.parent = this;
	this.cta_mc.setTransform(299.5,787,1,1,0,0,0,-340.5,403.9);
	this.cta_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.cta_mc).wait(98).to({_off:false},0).to({_off:true},75).wait(39));

	// VIDEO
	this.video_mc = new lib.bloc_video();
	this.video_mc.name = "video_mc";
	this.video_mc.parent = this;
	this.video_mc.setTransform(866.7,1147.15,1,1,0,0,0,227.5,764.9);
	this.video_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.video_mc).wait(94).to({_off:false},0).to({_off:true},79).wait(39));

	// lignes
	this.lignes_mc = new lib.lignes_all();
	this.lignes_mc.name = "lignes_mc";
	this.lignes_mc.parent = this;
	this.lignes_mc.setTransform(640,384.3,1,1,0,0,0,0,406.3);

	this.timeline.addTween(cjs.Tween.get(this.lignes_mc).wait(212));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(825.5,361.5,269.0999999999999,1101.8);
// library properties:
lib.properties = {
	id: '46C21C4CF632AD49869DA7617A13B65F',
	width: 1280,
	height: 768,
	fps: 60,
	color: "#000000",
	opacity: 1.00,
	manifest: [
		{src:"images/_27_perroquet_rouge.png", id:"_27_perroquet_rouge"},
		{src:"images/bateau1.png", id:"bateau1"},
		{src:"images/bateau2.png", id:"bateau2"},
		{src:"images/bateau3.png", id:"bateau3"},
		{src:"images/bateau4.png", id:"bateau4"},
		{src:"images/Calque1.png", id:"Calque1"},
		{src:"images/Calque2.png", id:"Calque2"},
		{src:"images/Calque3.png", id:"Calque3"},
		{src:"images/Calque4.png", id:"Calque4"},
		{src:"images/Calque5.png", id:"Calque5"},
		{src:"images/mongol.png", id:"mongol"},
		{src:"images/motamot.jpg", id:"motamot"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['46C21C4CF632AD49869DA7617A13B65F'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;