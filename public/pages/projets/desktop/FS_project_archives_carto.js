(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.video = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.008)").s().p("EgtTBLAMAAAiV/MBanAAAMAAACV/g");
	this.shape.setTransform(290,479.975);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.video, new cjs.Rectangle(0,0,580,960), null);


(lib.txt_hashtag = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBGAAIAAATIgwAAIAAAVIAqAAIAAASIgqAAIAAAUIAxAAIAAATg");
	this.shape.setTransform(107.3,52.75);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgjBCIAAhhIBGAAIAAAUIgwAAIAAATIAqAAIAAATIgqAAIAAAUIAxAAIAAATgAgLgpIAQgYIAWAJIgTAPg");
	this.shape_1.setTransform(98.15,51.025);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgKAxIAAhOIgcAAIAAgTIBNAAIAAATIgcAAIAABOg");
	this.shape_2.setTransform(89.1,52.75);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAXAxIgtg9IAAA9IgWAAIAAhhIAUAAIAvA+IAAg+IAWAAIAABhg");
	this.shape_3.setTransform(79.125,52.75);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBGAAIAAATIgwAAIAAAVIAqAAIAAASIgqAAIAAAUIAxAAIAAATg");
	this.shape_4.setTransform(69.05,52.75);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AAhAxIAAg9IgbA1IgMAAIgag1IAAA9IgWAAIAAhhIAeAAIAYA1IAZg1IAeAAIAABhg");
	this.shape_5.setTransform(57.575,52.75);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgeAkQgPgOAAgWQABgVAPgOQAOgPAVAAQAUAAAQAOIgLARQgHgGgGgCQgFgCgGAAQgMgBgIAJQgJAJAAAMQAAAOAIAJQAJAIAKAAQALAAAIgFIAAgaIAVAAIAAAjQgOAQgZAAQgWAAgOgPg");
	this.shape_6.setTransform(45.6,52.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgdAmQgMgMAAgUIAAg2IAWAAIAAA1QAAAMAFAGQAGAHAIAAQAJAAAGgHQAFgGAAgMIAAg1IAWAAIAAA2QAAAUgMAMQgMAMgSAAQgRAAgMgMg");
	this.shape_7.setTransform(35.15,52.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AAeAxIgKgVIgoAAIgJAVIgXAAIAqhhIAVAAIAqBhgAgLAJIAXAAIgMgcg");
	this.shape_8.setTransform(24.675,52.75);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgjBCIAAhhIBFAAIAAAUIgvAAIAAATIAqAAIAAATIgqAAIAAAUIAxAAIAAATgAgLgpIAQgYIAWAJIgUAPg");
	this.shape_9.setTransform(11.4,51.025);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgKAxIAAhOIgcAAIAAgTIBNAAIAAATIgcAAIAABOg");
	this.shape_10.setTransform(2.35,52.75);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgKAxIAAhhIAVAAIAABhg");
	this.shape_11.setTransform(-4.2,52.75);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgfAxIAAhhIAWAAIAABOIApAAIAAATg");
	this.shape_12.setTransform(-10.075,52.75);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AAeAxIgKgVIgoAAIgJAVIgXAAIAqhhIAVAAIAqBhgAgLAJIAXAAIgMgcg");
	this.shape_13.setTransform(-19.675,52.75);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgjBCIAAhhIBFAAIAAAUIgvAAIAAATIArAAIAAATIgrAAIAAAUIAxAAIAAATgAgMgpIARgYIAWAJIgUAPg");
	this.shape_14.setTransform(-29.25,51.025);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AAQAxIgVgfIgPAAIAAAfIgWAAIAAhhIAlAAQAWAAAKAIQAKAIAAARQAAAWgTAHIAZAjgAgUAAIAQAAQAKAAAEgEQAEgEAAgHQAAgIgEgDQgEgDgKAAIgQAAg");
	this.shape_15.setTransform(-38.775,52.75);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgTIANAAIADgRIgPAAIADgUIAPAAIAEgWIAUAAIgDAWIAMAAIADgWIAWAAIgEAWIAOAAIgDAUIgPAAIgDARIARAAIgEATIgQAAIgDAVgAgHAKIALAAIAEgRIgMAAg");
	this.shape_16.setTransform(-49.475,52.65);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgKAxIAAgmIgig7IAYAAIAUAkIAVgkIAYAAIgiA7IAAAmg");
	this.shape_17.setTransform(45.525,31.7);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgfAxIAAhhIAWAAIAABNIApAAIAAAUg");
	this.shape_18.setTransform(37.625,31.7);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgjAkQgPgPAAgVQAAgVAPgOQAOgOAVAAQAWAAAPAOQAOAOAAAVQAAAVgOAPQgPAPgWAAQgVAAgOgPgAgTgVQgJAJAAAMQAAANAJAKQAIAIALABQAMgBAJgIQAIgKAAgNQAAgMgIgJQgJgJgMAAQgLAAgIAJg");
	this.shape_19.setTransform(27.475,31.65);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgmAxIAAhhIAiAAQAWAAALAJQAKAIAAATQAAARgLAIQgKAJgVgBIgNAAIAAAcgAgQADIAOAAQALAAADgDQAEgFABgJQAAgIgGgDQgFgEgKAAIgMAAg");
	this.shape_20.setTransform(17.3,31.7);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AAVAxIgVhDIgUBDIgQAAIgjhhIAYAAIATA4IASg4IAVAAIASA4IATg4IAYAAIgiBhg");
	this.shape_21.setTransform(5.075,31.7);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgjAkQgPgPAAgVQAAgVAPgOQAOgOAVAAQAWAAAPAOQAOAOAAAVQAAAVgOAPQgPAPgWAAQgVAAgOgPgAgTgVQgJAJAAAMQAAANAJAKQAIAIALABQAMgBAJgIQAIgKAAgNQAAgMgIgJQgJgJgMAAQgLAAgIAJg");
	this.shape_22.setTransform(-7.825,31.65);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgfAxIAAhhIAWAAIAABNIApAAIAAAUg");
	this.shape_23.setTransform(-17.175,31.7);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgsAxIAAhhIAjAAQAaAAAOANQAOAMgBAXQABAWgOAOQgOANgbAAgAgVAdIANAAQAOAAAJgHQAHgIABgOQgBgOgHgHQgJgIgQAAIgLAAg");
	this.shape_24.setTransform(-30.35,31.7);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgiAmIAJgRQAOAKALAAQAFAAAEgEQAFgDAAgGQAAgGgFgEQgFgDgHAAQgFAAgJACIAAgPIASgVIgeAAIAAgTIA8AAIAAAPIgVAXQAMACAGAHQAHAHAAAKQAAAQgLAJQgKAJgPAAQgRAAgQgMg");
	this.shape_25.setTransform(-40.25,31.75);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgTIANAAIADgRIgPAAIADgTIAPAAIAEgXIAUAAIgDAXIAMAAIADgXIAWAAIgEAXIAOAAIgDATIgPAAIgDARIARAAIgEATIgQAAIgDAVgAgHAKIALAAIAEgRIgMAAg");
	this.shape_26.setTransform(-49.475,31.6);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AAXAxIgtg9IAAA9IgWAAIAAhhIAUAAIAvA+IAAg+IAWAAIAABhg");
	this.shape_27.setTransform(58.825,10.65);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AgjAkQgPgPAAgVQAAgVAPgOQAOgOAVAAQAWAAAPAOQAOAOAAAVQAAAVgOAPQgPAPgWAAQgVAAgOgPgAgTgVQgJAJAAAMQAAANAJAKQAIAIALABQAMgBAJgIQAIgKAAgNQAAgMgIgJQgJgKgMABQgLgBgIAKg");
	this.shape_28.setTransform(47.325,10.6);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AgKAxIAAhhIAVAAIAABhg");
	this.shape_29.setTransform(39.25,10.65);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AgKAxIAAhOIgbAAIAAgTIBLAAIAAATIgbAAIAABOg");
	this.shape_30.setTransform(32.7,10.65);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AAeAxIgKgVIgoAAIgJAVIgXAAIAqhhIAVAAIAqBhgAgLAJIAXAAIgMgcg");
	this.shape_31.setTransform(23.225,10.65);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AgeAkQgPgOAAgWQAAgUAPgPQAPgOAVAAQAYgBAQATIgOAPQgKgMgQAAQgLgBgIAJQgKAHABANQAAAOAIAIQAJAIAKAAQAQAAALgNIANAPQgRATgWAAQgVgBgPgOg");
	this.shape_32.setTransform(12.95,10.6);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AgKAxIAAhhIAVAAIAABhg");
	this.shape_33.setTransform(5.45,10.65);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AggAxIAAhhIBBAAIAAAUIgsAAIAAAVIAqAAIAAARIgqAAIAAAng");
	this.shape_34.setTransform(-0.8,10.65);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AgKAxIAAhhIAVAAIAABhg");
	this.shape_35.setTransform(-7.65,10.65);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AAhAxIAAg9IgbA1IgMAAIgag1IAAA9IgWAAIAAhhIAeAAIAYA1IAZg1IAeAAIAABhg");
	this.shape_36.setTransform(-16.625,10.65);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AAeAxIgKgVIgoAAIgJAVIgXAAIAqhhIAVAAIAqBhgAgLAJIAXAAIgMgcg");
	this.shape_37.setTransform(-28.525,10.65);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("AgeAkQgOgOAAgWQAAgUAOgPQAPgOAUAAQAWAAAPANIgMARQgGgGgFgDQgGgBgHAAQgLAAgJAIQgIAIAAANQAAAOAIAJQAIAIAKAAQAMAAAHgFIAAgaIAXAAIAAAiQgPAQgaABQgUgBgPgOg");
	this.shape_38.setTransform(-39.05,10.6);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgTIANAAIADgRIgPAAIADgTIAPAAIAEgXIAUAAIgDAXIAMAAIADgXIAWAAIgEAXIAOAAIgDATIgPAAIgDARIARAAIgEATIgQAAIgDAVgAgHAKIALAAIAEgRIgMAAg");
	this.shape_39.setTransform(-49.475,10.55);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.txt_hashtag, new cjs.Rectangle(-56.7,0,170.60000000000002,63.2), null);


(lib.trait_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_51 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(51).call(this.frame_51).wait(1));

	// Layer_1 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("EgpzAC3IITv9IESCOIoTP9g");
	var mask_graphics_1 = new cjs.Graphics().p("EgpzAC3IITv9IESCOIoTP9g");
	var mask_graphics_2 = new cjs.Graphics().p("EgpzAC3IITv9IESCOIoTP9g");
	var mask_graphics_3 = new cjs.Graphics().p("EgpzAC3IITv9IESCOIoTP+g");
	var mask_graphics_4 = new cjs.Graphics().p("EgpzAC3IITv9IETCPIoTP9g");
	var mask_graphics_5 = new cjs.Graphics().p("EgpzAC3IITv9IEVCQIoTP9g");
	var mask_graphics_6 = new cjs.Graphics().p("EgpzAC3IITv9IEYCRIoTP+g");
	var mask_graphics_7 = new cjs.Graphics().p("EgpzAC4IITv+IEeCUIoUP+g");
	var mask_graphics_8 = new cjs.Graphics().p("EgpzAC4IITv+IEmCYIoUP+g");
	var mask_graphics_9 = new cjs.Graphics().p("EgpzAC4IITv+IEyCeIoUP/g");
	var mask_graphics_10 = new cjs.Graphics().p("EgpzAC4IITv+IFCCnIoUP/g");
	var mask_graphics_11 = new cjs.Graphics().p("EgpzAC5IIUv/IFXCyIoUQAg");
	var mask_graphics_12 = new cjs.Graphics().p("Egp0AC6IIVwAIF1DCIoVQAg");
	var mask_graphics_13 = new cjs.Graphics().p("Egp0AC7IIVwBIGaDVIoVQBg");
	var mask_graphics_14 = new cjs.Graphics().p("Egp1AC8IIWwCIHKDtIoWQDg");
	var mask_graphics_15 = new cjs.Graphics().p("Egp1AC9IIWwEIIEENIoWQEg");
	var mask_graphics_16 = new cjs.Graphics().p("Egp2AC/IIXwGIJMEyIoXQFg");
	var mask_graphics_17 = new cjs.Graphics().p("Egp3ADBIIYwIIKiFeIoYQIg");
	var mask_graphics_18 = new cjs.Graphics().p("Egp4ADEIIawLIMJGTIoaQLg");
	var mask_graphics_19 = new cjs.Graphics().p("Egp6ADHIIcwOIOCHSIobQOg");
	var mask_graphics_20 = new cjs.Graphics().p("Egp7ADKIIewSIQQIdIoeQSg");
	var mask_graphics_21 = new cjs.Graphics().p("Egp9ADPIIgwXIS2JyIogQXg");
	var mask_graphics_22 = new cjs.Graphics().p("Egp/ACkIIjwcIV0LVIojQcg");
	var mask_graphics_23 = new cjs.Graphics().p("EgqCABuIImwiIZPNHIomQig");
	var mask_graphics_24 = new cjs.Graphics().p("EgqEAAxIIpwpIdIPIIoqQpg");
	var mask_graphics_25 = new cjs.Graphics().p("EgqHgAUIItwxMAhhARaIotQxg");
	var mask_graphics_26 = new cjs.Graphics().p("EgqLgBiIIyw6MAmfAT/IoxQ6g");
	var mask_graphics_27 = new cjs.Graphics().p("EgqHgC6II3xDMAsDAW4Io3RDg");
	var mask_graphics_31 = new cjs.Graphics().p("EgqKgC0II9xPMAsEAW4Io+RPg");
	var mask_graphics_32 = new cjs.Graphics().p("Egl2gAuII1xAMAjjASdIo0RAg");
	var mask_graphics_33 = new cjs.Graphics().p("EgiNABCIIuwyIcYOvIotQyg");
	var mask_graphics_34 = new cjs.Graphics().p("A/JCgIIowmIWXLnIooQmg");
	var mask_graphics_35 = new cjs.Graphics().p("A8mDvIIjwdIRWJAIojQdg");
	var mask_graphics_36 = new cjs.Graphics().p("A6iEvIIgwWINQG5IofQWg");
	var mask_graphics_37 = new cjs.Graphics().p("A42FjIIcwQIJ9FLIocQQg");
	var mask_graphics_38 = new cjs.Graphics().p("A3hGMIIawKIHVDzIoaQKg");
	var mask_graphics_39 = new cjs.Graphics().p("A2fGsIIYwHIFTCwIoYQHg");
	var mask_graphics_40 = new cjs.Graphics().p("A1sHEIIWwDIDvB8IoWQDg");
	var mask_graphics_41 = new cjs.Graphics().p("A1HHWIIWwBICkBWIoVQBg");
	var mask_graphics_42 = new cjs.Graphics().p("A0sHjIIVwAIBwA7IoVQAg");
	var mask_graphics_43 = new cjs.Graphics().p("A0ZHsIITv/IBMAoIoUP/g");
	var mask_graphics_44 = new cjs.Graphics().p("A0NHyIITv+IA0AbIoTP+g");
	var mask_graphics_45 = new cjs.Graphics().p("A0GH2IITv+IAmATIoTP+g");
	var mask_graphics_46 = new cjs.Graphics().p("A0CH4IITv+IAeAPIoTP+g");
	var mask_graphics_47 = new cjs.Graphics().p("A0AH4IITv9IAaAOIoTP9g");
	var mask_graphics_48 = new cjs.Graphics().p("Az/H5IITv+IAYANIoTP+g");
	var mask_graphics_49 = new cjs.Graphics().p("Az/H5IITv9IAYAMIoTP9g");
	var mask_graphics_50 = new cjs.Graphics().p("Az/H5IITv9IAYAMIoTP9g");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:-267.57,y:-83.9058}).wait(1).to({graphics:mask_graphics_1,x:-267.5701,y:-83.906}).wait(1).to({graphics:mask_graphics_2,x:-267.5702,y:-83.906}).wait(1).to({graphics:mask_graphics_3,x:-267.5705,y:-83.9061}).wait(1).to({graphics:mask_graphics_4,x:-267.5715,y:-83.9063}).wait(1).to({graphics:mask_graphics_5,x:-267.5734,y:-83.9067}).wait(1).to({graphics:mask_graphics_6,x:-267.577,y:-83.9073}).wait(1).to({graphics:mask_graphics_7,x:-267.5828,y:-83.9085}).wait(1).to({graphics:mask_graphics_8,x:-267.5918,y:-83.9102}).wait(1).to({graphics:mask_graphics_9,x:-267.6049,y:-83.9127}).wait(1).to({graphics:mask_graphics_10,x:-267.6232,y:-83.9162}).wait(1).to({graphics:mask_graphics_11,x:-267.6478,y:-83.921}).wait(1).to({graphics:mask_graphics_12,x:-267.6802,y:-83.9272}).wait(1).to({graphics:mask_graphics_13,x:-267.7217,y:-83.9352}).wait(1).to({graphics:mask_graphics_14,x:-267.774,y:-83.9453}).wait(1).to({graphics:mask_graphics_15,x:-267.8388,y:-83.9578}).wait(1).to({graphics:mask_graphics_16,x:-267.918,y:-83.973}).wait(1).to({graphics:mask_graphics_17,x:-268.0135,y:-83.9914}).wait(1).to({graphics:mask_graphics_18,x:-268.1274,y:-84.0134}).wait(1).to({graphics:mask_graphics_19,x:-268.2619,y:-84.0393}).wait(1).to({graphics:mask_graphics_20,x:-268.4195,y:-84.0696}).wait(1).to({graphics:mask_graphics_21,x:-268.6025,y:-84.1049}).wait(1).to({graphics:mask_graphics_22,x:-268.8137,y:-79.3863}).wait(1).to({graphics:mask_graphics_23,x:-269.0557,y:-73.5094}).wait(1).to({graphics:mask_graphics_24,x:-269.3314,y:-66.8139}).wait(1).to({graphics:mask_graphics_25,x:-269.6438,y:-59.227}).wait(1).to({graphics:mask_graphics_26,x:-269.9961,y:-50.673}).wait(1).to({graphics:mask_graphics_27,x:-269.6205,y:-40.2122}).wait(4).to({graphics:mask_graphics_31,x:-269.9431,y:-39.5899}).wait(1).to({graphics:mask_graphics_32,x:-242.3405,y:-26.2641}).wait(1).to({graphics:mask_graphics_33,x:-218.9744,y:-14.9835}).wait(1).to({graphics:mask_graphics_34,x:-199.3874,y:-5.5272}).wait(1).to({graphics:mask_graphics_35,x:-183.1482,y:2.3129}).wait(1).to({graphics:mask_graphics_36,x:-169.8517,y:8.7324}).wait(1).to({graphics:mask_graphics_37,x:-159.1189,y:13.9142}).wait(1).to({graphics:mask_graphics_38,x:-150.5969,y:18.0286}).wait(1).to({graphics:mask_graphics_39,x:-143.959,y:21.2334}).wait(1).to({graphics:mask_graphics_40,x:-138.9046,y:23.6737}).wait(1).to({graphics:mask_graphics_41,x:-135.1592,y:25.482}).wait(1).to({graphics:mask_graphics_42,x:-132.4747,y:26.7781}).wait(1).to({graphics:mask_graphics_43,x:-130.6287,y:27.6694}).wait(1).to({graphics:mask_graphics_44,x:-129.4253,y:28.2504}).wait(1).to({graphics:mask_graphics_45,x:-128.6945,y:28.6032}).wait(1).to({graphics:mask_graphics_46,x:-128.2926,y:28.7972}).wait(1).to({graphics:mask_graphics_47,x:-128.102,y:28.8892}).wait(1).to({graphics:mask_graphics_48,x:-128.0312,y:28.9234}).wait(1).to({graphics:mask_graphics_49,x:-128.0149,y:28.9313}).wait(1).to({graphics:mask_graphics_50,x:-128.029,y:28.916}).wait(2));

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#D6A432").s().p("AjNCrIkhgTQgGgBgFgFQgEgEABgHIAYjMIAjhyIhUgOQgDAAgDgCIi2hyQgFgDgCgGQgBgHADgFQAEgFAGgBQAGgCAFAEIC0BwIBiAQQAHABAEAGQADAFgCAHIgoCBIgVC5IESASIACABIOVCqQAGABAEAFQADAFgBAGQgCANgNAAg");
	this.shape.setTransform(-354.9038,-37.2988,1.7032,1.7032);

	var maskedShapeInstanceList = [this.shape];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape).to({_off:true},51).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-479.1,-95.5,248.50000000000003,116.4);


(lib.trait_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_52 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(52).call(this.frame_52).wait(1));

	// Layer_1 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("A+zqsIDrjHIU1YgIjrDHg");
	var mask_graphics_1 = new cjs.Graphics().p("A+zqsIDrjHIU1YgIjrDHg");
	var mask_graphics_2 = new cjs.Graphics().p("A+zqsIDrjHIU1YgIjrDHg");
	var mask_graphics_3 = new cjs.Graphics().p("A+zqsIDrjHIU1YgIjrDHg");
	var mask_graphics_4 = new cjs.Graphics().p("A+zqrIDsjJIU1YgIjsDJg");
	var mask_graphics_5 = new cjs.Graphics().p("A+zqrIDtjJIU1YgIjtDJg");
	var mask_graphics_6 = new cjs.Graphics().p("A+zqqIDwjLIU0YgIjvDLg");
	var mask_graphics_7 = new cjs.Graphics().p("A+zqoIDzjPIU1YgIjzDPg");
	var mask_graphics_8 = new cjs.Graphics().p("A+zqmID5jTIU1YgIj5DTg");
	var mask_graphics_9 = new cjs.Graphics().p("A+zqiIECjbIU1YgIkCDbg");
	var mask_graphics_10 = new cjs.Graphics().p("A+zqdIEOjlIU1YgIkODlg");
	var mask_graphics_11 = new cjs.Graphics().p("A+zqWIEejzIU1YgIkeDzg");
	var mask_graphics_12 = new cjs.Graphics().p("A+zqNIE0kFIU0YgIk0EFg");
	var mask_graphics_13 = new cjs.Graphics().p("A+zqBIFPkdIU1YgIlQEdg");
	var mask_graphics_14 = new cjs.Graphics().p("A+zpyIFyk7IU0YgIlyE7g");
	var mask_graphics_15 = new cjs.Graphics().p("A+0pgIGdlfIU1YgImdFfg");
	var mask_graphics_16 = new cjs.Graphics().p("A+0pKIHSmLIU0YgInRGLg");
	var mask_graphics_17 = new cjs.Graphics().p("A+0ovIIQnBIU1YgIoQHBg");
	var mask_graphics_18 = new cjs.Graphics().p("A+0oPIJcoBIU0YgIpbIBg");
	var mask_graphics_19 = new cjs.Graphics().p("A+0npIK0pNIU0YgIq0JNg");
	var mask_graphics_20 = new cjs.Graphics().p("A+1m9IMdqlIU0YgIscKlg");
	var mask_graphics_21 = new cjs.Graphics().p("A+1mKIOWsLIU0YgIuVMLg");
	var mask_graphics_22 = new cjs.Graphics().p("A+1lOIQhuDIU0YgIwhODg");
	var mask_graphics_23 = new cjs.Graphics().p("A+2kKITCwLIU0YgIzBQLg");
	var mask_graphics_24 = new cjs.Graphics().p("A+2i9IV4ylIU0YgI13Slg");
	var mask_graphics_25 = new cjs.Graphics().p("A+3hlIZH1VIU0YgI5GVVg");
	var mask_graphics_26 = new cjs.Graphics().p("A+3gCIcv4bIU0YgI8vYbg");
	var mask_graphics_27 = new cjs.Graphics().p("A+3BsMAg0gb4IU1YhMgg1Ab4g");
	var mask_graphics_31 = new cjs.Graphics().p("A+3BsMAg0gb4IU1YhMgg1Ab4g");
	var mask_graphics_32 = new cjs.Graphics().p("A7xg8Iao2nIU0YgI6nWng");
	var mask_graphics_33 = new cjs.Graphics().p("A5JjLIVYyJIU0YgI1XSJg");
	var mask_graphics_34 = new cjs.Graphics().p("A28lDIQ+uaIU0YhIw9Oag");
	var mask_graphics_35 = new cjs.Graphics().p("A1HmmINUrTIU0YgItTLTg");
	var mask_graphics_36 = new cjs.Graphics().p("AznozIKUoxIU0YgIqVIxg");
	var mask_graphics_37 = new cjs.Graphics().p("Ayaq2IH6muIU0YgIn6Gug");
	var mask_graphics_38 = new cjs.Graphics().p("AxdseIGAlGIUzYgIl/FGg");
	var mask_graphics_39 = new cjs.Graphics().p("AwttvIEgj1IUzYgIkfD1g");
	var mask_graphics_40 = new cjs.Graphics().p("AwJutIDXi3IU0YgIjXC3g");
	var mask_graphics_41 = new cjs.Graphics().p("AvuvbIChiJIU0YgIihCJg");
	var mask_graphics_42 = new cjs.Graphics().p("Avbv8IB7hoIU0YgIh7Bog");
	var mask_graphics_43 = new cjs.Graphics().p("AvNwSIBghSIUzYgIhgBSg");
	var mask_graphics_44 = new cjs.Graphics().p("AvFwhIBPhDIU0YgIhPBDg");
	var mask_graphics_45 = new cjs.Graphics().p("Au/wqIBEg6IU0YgIhFA6g");
	var mask_graphics_46 = new cjs.Graphics().p("Au8wvIA+g1IU0YgIg/A1g");
	var mask_graphics_47 = new cjs.Graphics().p("Au7wxIA8gzIUzYgIg7Azg");
	var mask_graphics_48 = new cjs.Graphics().p("Au7wyIA7gyIU0YgIg7Ayg");
	var mask_graphics_49 = new cjs.Graphics().p("Au6wyIA6gyIU0YgIg7Ayg");
	var mask_graphics_50 = new cjs.Graphics().p("Au6wyIA6gxIU0YgIg6Axg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:-197.2046,y:21.2597}).wait(1).to({graphics:mask_graphics_1,x:-197.2045,y:21.2596}).wait(1).to({graphics:mask_graphics_2,x:-197.2045,y:21.2574}).wait(1).to({graphics:mask_graphics_3,x:-197.2046,y:21.2478}).wait(1).to({graphics:mask_graphics_4,x:-197.2047,y:21.2219}).wait(1).to({graphics:mask_graphics_5,x:-197.2051,y:21.1674}).wait(1).to({graphics:mask_graphics_6,x:-197.2057,y:21.0683}).wait(1).to({graphics:mask_graphics_7,x:-197.2068,y:20.9051}).wait(1).to({graphics:mask_graphics_8,x:-197.2084,y:20.6548}).wait(1).to({graphics:mask_graphics_9,x:-197.2108,y:20.2907}).wait(1).to({graphics:mask_graphics_10,x:-197.214,y:19.7828}).wait(1).to({graphics:mask_graphics_11,x:-197.2185,y:19.0974}).wait(1).to({graphics:mask_graphics_12,x:-197.2243,y:18.1972}).wait(1).to({graphics:mask_graphics_13,x:-197.2318,y:17.0415}).wait(1).to({graphics:mask_graphics_14,x:-197.2412,y:15.5861}).wait(1).to({graphics:mask_graphics_15,x:-197.2529,y:13.7829}).wait(1).to({graphics:mask_graphics_16,x:-197.2671,y:11.5807}).wait(1).to({graphics:mask_graphics_17,x:-197.2843,y:8.9245}).wait(1).to({graphics:mask_graphics_18,x:-197.3049,y:5.7558}).wait(1).to({graphics:mask_graphics_19,x:-197.3291,y:2.0126}).wait(1).to({graphics:mask_graphics_20,x:-197.3575,y:-2.3707}).wait(1).to({graphics:mask_graphics_21,x:-197.3904,y:-7.4632}).wait(1).to({graphics:mask_graphics_22,x:-197.4285,y:-13.3375}).wait(1).to({graphics:mask_graphics_23,x:-197.472,y:-20.07}).wait(1).to({graphics:mask_graphics_24,x:-197.5217,y:-27.7402}).wait(1).to({graphics:mask_graphics_25,x:-197.578,y:-36.4316}).wait(1).to({graphics:mask_graphics_26,x:-197.6414,y:-46.2309}).wait(1).to({graphics:mask_graphics_27,x:-197.6308,y:-57.2503}).wait(4).to({graphics:mask_graphics_31,x:-197.6308,y:-57.2503}).wait(1).to({graphics:mask_graphics_32,x:-177.7764,y:-74.1221}).wait(1).to({graphics:mask_graphics_33,x:-160.9694,y:-88.4043}).wait(1).to({graphics:mask_graphics_34,x:-146.8807,y:-100.3767}).wait(1).to({graphics:mask_graphics_35,x:-135.2,y:-110.303}).wait(1).to({graphics:mask_graphics_36,x:-125.636,y:-112.4664}).wait(1).to({graphics:mask_graphics_37,x:-117.916,y:-112.4668}).wait(1).to({graphics:mask_graphics_38,x:-111.7862,y:-112.4671}).wait(1).to({graphics:mask_graphics_39,x:-107.0116,y:-112.4674}).wait(1).to({graphics:mask_graphics_40,x:-103.3761,y:-112.4676}).wait(1).to({graphics:mask_graphics_41,x:-100.6821,y:-112.4678}).wait(1).to({graphics:mask_graphics_42,x:-98.7511,y:-112.4679}).wait(1).to({graphics:mask_graphics_43,x:-97.4233,y:-112.468}).wait(1).to({graphics:mask_graphics_44,x:-96.5577,y:-112.468}).wait(1).to({graphics:mask_graphics_45,x:-96.032,y:-112.4681}).wait(1).to({graphics:mask_graphics_46,x:-95.743,y:-112.4681}).wait(1).to({graphics:mask_graphics_47,x:-95.6059,y:-112.4681}).wait(1).to({graphics:mask_graphics_48,x:-95.555,y:-112.4681}).wait(1).to({graphics:mask_graphics_49,x:-95.5432,y:-112.4681}).wait(1).to({graphics:mask_graphics_50,x:-95.5092,y:-112.4379}).wait(3));

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#D6A432").s().p("ADvJ8IlCjPQgFgDgBgGQgBgGADgFICzk4IiGhiQgJgGAEgLIAriCIgQgxIhdAaQgHABgFgDQgFgEgCgHQgHgkgMgqQgahWgdglQgggqgPhjQgHgygBguQAAgHAFgEQAEgEAGgBQAGABAFAEQAEAEAAAGQABAqAGAuQANBfAdAkQAbAiAXBHQAQAtAKAxIBbgaQAGgBAGADQAEADACAFIAWBFQABAEgBAFIgoB9ICIBiQAEAEACAFQABAGgDAFIizE4IE1DHQAFADABAGQACAGgDAFQgFAIgIAAQgEgBgEgCg");
	this.shape.setTransform(-190.7681,-34.5819,1.7032,1.7032,90.7905);

	var maskedShapeInstanceList = [this.shape];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape).to({_off:true},52).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-300,-77.7,218.6,86.3);


(lib.trait_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_51 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(51).call(this.frame_51).wait(1));

	// Layer_2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("AyAvwIEzgeIDKf/IkzAeg");
	var mask_graphics_1 = new cjs.Graphics().p("AyAvwIEzgeIDKf/IkzAeg");
	var mask_graphics_2 = new cjs.Graphics().p("AyAvwIEzgeIDKf/IkzAeg");
	var mask_graphics_3 = new cjs.Graphics().p("AyAvwIE0gfMADJAgAIkzAfg");
	var mask_graphics_4 = new cjs.Graphics().p("AyAvwIE0gfMADJAgAIkzAfg");
	var mask_graphics_5 = new cjs.Graphics().p("AyAvwIE1gfMADJAgAIk0Afg");
	var mask_graphics_6 = new cjs.Graphics().p("AyAvwIE3gfMADJAgAIk2Afg");
	var mask_graphics_7 = new cjs.Graphics().p("AyAvwIE6gfMADJAgAIk5Afg");
	var mask_graphics_8 = new cjs.Graphics().p("AyAvwIE+gfMADKAgAIk+Afg");
	var mask_graphics_9 = new cjs.Graphics().p("AyAvvIFFggIDJf/IlEAgg");
	var mask_graphics_10 = new cjs.Graphics().p("AyAvvIFOghMADJAgAIlNAhg");
	var mask_graphics_11 = new cjs.Graphics().p("AyAvuIFagiIDJf/IlZAig");
	var mask_graphics_12 = new cjs.Graphics().p("AyAvuIFqgjMADJAgAIlpAjg");
	var mask_graphics_13 = new cjs.Graphics().p("AyAvtIF/glMADJAgAIl+Alg");
	var mask_graphics_14 = new cjs.Graphics().p("AyAvrIGZgoIDJf/ImYAog");
	var mask_graphics_15 = new cjs.Graphics().p("AyAvqIG5grMADJAgAIm4Arg");
	var mask_graphics_16 = new cjs.Graphics().p("AyAvoIHggvMADKAgAIngAvg");
	var mask_graphics_17 = new cjs.Graphics().p("AyAvlIIQg0MADJAgAIoQAzg");
	var mask_graphics_18 = new cjs.Graphics().p("AyAvjIJIg5MADKAgAIpJA5g");
	var mask_graphics_19 = new cjs.Graphics().p("AyAvfIKLhAIDJf/IqLBAg");
	var mask_graphics_20 = new cjs.Graphics().p("AyAvcILZhHMADKAgAIraBHg");
	var mask_graphics_21 = new cjs.Graphics().p("AyBvXIM1hRMADKAgAIs1BRg");
	var mask_graphics_22 = new cjs.Graphics().p("AyBvSIOehbMADJAgAIudBbg");
	var mask_graphics_23 = new cjs.Graphics().p("AyBvMIQWhnMADJAgAIwWBng");
	var mask_graphics_24 = new cjs.Graphics().p("AyBvFISeh1MADKAgAIyfB1g");
	var mask_graphics_25 = new cjs.Graphics().p("AyCu+IU6iDMADKAgAI06CDg");
	var mask_graphics_26 = new cjs.Graphics().p("AyCu1IXpiVMADKAgAI3qCVg");
	var mask_graphics_27 = new cjs.Graphics().p("AyCurIauioIDKf/I6vCog");
	var mask_graphics_31 = new cjs.Graphics().p("AyCurIauioIDKf/I6vCog");
	var mask_graphics_32 = new cjs.Graphics().p("Aveu8IVmiHMADJAgAI1lCHg");
	var mask_graphics_33 = new cjs.Graphics().p("AtTvJIRQhtMADJAgAIxPBtg");
	var mask_graphics_34 = new cjs.Graphics().p("ArevVINmhVMADKAgAItnBVg");
	var mask_graphics_35 = new cjs.Graphics().p("Ap+veIKmhDMADJAgAIqlBDg");
	var mask_graphics_36 = new cjs.Graphics().p("AovvmIIJgzMADIAgAIoHAzg");
	var mask_graphics_37 = new cjs.Graphics().p("AnvvsIGJgnMADIAgAImHAng");
	var mask_graphics_38 = new cjs.Graphics().p("Am8vxIEjgdMADJAgAIkjAdg");
	var mask_graphics_39 = new cjs.Graphics().p("AmVv1IDVgVMADIAgAIjTAVg");
	var mask_graphics_40 = new cjs.Graphics().p("Al2v4ICYgPMADJAgAIiYAPg");
	var mask_graphics_41 = new cjs.Graphics().p("Algv6IBrgLMADKAgAIhsALg");
	var mask_graphics_42 = new cjs.Graphics().p("AlQv8IBLgHMADKAgAIhMAHg");
	var mask_graphics_43 = new cjs.Graphics().p("AlFv9IA1gFMADKAgAIg2AFg");
	var mask_graphics_44 = new cjs.Graphics().p("Ak+v9IAngEIDKf/IgoAEg");
	var mask_graphics_45 = new cjs.Graphics().p("Ak6v+IAfgDMADJAgAIgeADg");
	var mask_graphics_46 = new cjs.Graphics().p("Ak3v+IAZgDMADKAgAIgaADg");
	var mask_graphics_47 = new cjs.Graphics().p("Ak2v+IAXgDMADKAgAIgYADg");
	var mask_graphics_48 = new cjs.Graphics().p("Ak2v+IAXgDMADJAgAIgWADg");
	var mask_graphics_49 = new cjs.Graphics().p("Ak2v+IAXgDMADJAgAIgWADg");
	var mask_graphics_50 = new cjs.Graphics().p("Ak2v+IAXgDMADJAgAIgWADg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:-115.2518,y:-85.8592}).wait(1).to({graphics:mask_graphics_1,x:-115.2519,y:-85.8593}).wait(1).to({graphics:mask_graphics_2,x:-115.2519,y:-85.8595}).wait(1).to({graphics:mask_graphics_3,x:-115.2519,y:-85.8603}).wait(1).to({graphics:mask_graphics_4,x:-115.252,y:-85.8626}).wait(1).to({graphics:mask_graphics_5,x:-115.2522,y:-85.8674}).wait(1).to({graphics:mask_graphics_6,x:-115.2526,y:-85.8761}).wait(1).to({graphics:mask_graphics_7,x:-115.2532,y:-85.8906}).wait(1).to({graphics:mask_graphics_8,x:-115.2541,y:-85.9126}).wait(1).to({graphics:mask_graphics_9,x:-115.2555,y:-85.9448}).wait(1).to({graphics:mask_graphics_10,x:-115.2574,y:-85.9896}).wait(1).to({graphics:mask_graphics_11,x:-115.26,y:-86.0501}).wait(1).to({graphics:mask_graphics_12,x:-115.2634,y:-86.1296}).wait(1).to({graphics:mask_graphics_13,x:-115.2678,y:-86.2316}).wait(1).to({graphics:mask_graphics_14,x:-115.2732,y:-86.36}).wait(1).to({graphics:mask_graphics_15,x:-115.28,y:-86.5192}).wait(1).to({graphics:mask_graphics_16,x:-115.2883,y:-86.7135}).wait(1).to({graphics:mask_graphics_17,x:-115.2983,y:-86.948}).wait(1).to({graphics:mask_graphics_18,x:-115.3102,y:-87.2276}).wait(1).to({graphics:mask_graphics_19,x:-115.3243,y:-87.558}).wait(1).to({graphics:mask_graphics_20,x:-115.3409,y:-87.9449}).wait(1).to({graphics:mask_graphics_21,x:-115.36,y:-88.3943}).wait(1).to({graphics:mask_graphics_22,x:-115.3821,y:-88.9128}).wait(1).to({graphics:mask_graphics_23,x:-115.4075,y:-89.507}).wait(1).to({graphics:mask_graphics_24,x:-115.4364,y:-90.1839}).wait(1).to({graphics:mask_graphics_25,x:-115.4691,y:-90.951}).wait(1).to({graphics:mask_graphics_26,x:-115.506,y:-91.8158}).wait(1).to({graphics:mask_graphics_27,x:-115.4998,y:-92.7057}).wait(4).to({graphics:mask_graphics_31,x:-115.4998,y:-92.7057}).wait(1).to({graphics:mask_graphics_32,x:-99.079,y:-94.3278}).wait(1).to({graphics:mask_graphics_33,x:-85.1784,y:-95.7011}).wait(1).to({graphics:mask_graphics_34,x:-73.526,y:-96.8524}).wait(1).to({graphics:mask_graphics_35,x:-63.8652,y:-97.807}).wait(1).to({graphics:mask_graphics_36,x:-55.955,y:-98.5888}).wait(1).to({graphics:mask_graphics_37,x:-49.57,y:-99.2198}).wait(1).to({graphics:mask_graphics_38,x:-44.5002,y:-99.721}).wait(1).to({graphics:mask_graphics_39,x:-40.5513,y:-100.1113}).wait(1).to({graphics:mask_graphics_40,x:-37.5444,y:-100.4086}).wait(1).to({graphics:mask_graphics_41,x:-35.3163,y:-100.6288}).wait(1).to({graphics:mask_graphics_42,x:-33.7192,y:-100.7867}).wait(1).to({graphics:mask_graphics_43,x:-32.6211,y:-100.8953}).wait(1).to({graphics:mask_graphics_44,x:-31.9051,y:-100.9661}).wait(1).to({graphics:mask_graphics_45,x:-31.4704,y:-101.009}).wait(1).to({graphics:mask_graphics_46,x:-31.2313,y:-101.0327}).wait(1).to({graphics:mask_graphics_47,x:-31.1179,y:-101.0439}).wait(1).to({graphics:mask_graphics_48,x:-31.0758,y:-101.048}).wait(1).to({graphics:mask_graphics_49,x:-31.0661,y:-101.049}).wait(1).to({graphics:mask_graphics_50,x:-31.0621,y:-100.9599}).wait(2));

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#D6A432").s().p("AFiDcIl9mYIiMBuQgEAEgHgBQgGgBgEgEIhDhYIhiBaQgFAEgGAAQgHgBgEgFQgEgEAAgGQABgHAEgEIBvhkQAEgEAHAAQAHABAEAFIBEBYICKhtQAFgEAGABQAGAAAEAFIGGGiQAEAEAAAGQAAAHgFAEQgFAEgFAAQgHAAgEgFg");
	this.shape.setTransform(-115.217,-76.6063,1.7032,1.7032);

	var maskedShapeInstanceList = [this.shape];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape).to({_off:true},51).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-179.9,-114.8,129.4,76.5);


(lib.trait_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_51 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(51).call(this.frame_51).wait(1));

	// Layer_2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("AoKvwIEzgeIDKf/IkzAeg");
	var mask_graphics_1 = new cjs.Graphics().p("AoKvwIEzgeIDKf/IkzAeg");
	var mask_graphics_2 = new cjs.Graphics().p("AoKvwIEzgeIDKf/IkzAeg");
	var mask_graphics_3 = new cjs.Graphics().p("AoKvwIE0gfMADJAgAIkzAfg");
	var mask_graphics_4 = new cjs.Graphics().p("AoKvwIE0gfMADKAgAIk0Afg");
	var mask_graphics_5 = new cjs.Graphics().p("AoKvwIE2gfMADJAgAIk1Afg");
	var mask_graphics_6 = new cjs.Graphics().p("AoKvwIE4gfMADKAgAIk4Afg");
	var mask_graphics_7 = new cjs.Graphics().p("AoKvwIE8gfMADKAgAIk8Afg");
	var mask_graphics_8 = new cjs.Graphics().p("AoKvwIFCgfMADJAgAIlBAfg");
	var mask_graphics_9 = new cjs.Graphics().p("AoKvvIFLghMADJAgAIlKAhg");
	var mask_graphics_10 = new cjs.Graphics().p("AoKvvIFYghMADIAgAIlWAhg");
	var mask_graphics_11 = new cjs.Graphics().p("AoKvuIFpgjMADIAgAIlnAjg");
	var mask_graphics_12 = new cjs.Graphics().p("AoKvtIF/glMADIAgAIl9Alg");
	var mask_graphics_13 = new cjs.Graphics().p("AoKvrIGbgpMADJAgAImaApg");
	var mask_graphics_14 = new cjs.Graphics().p("AoKvpIG/gsIDJf/Im+Asg");
	var mask_graphics_15 = new cjs.Graphics().p("AoKvnIHsgxMADIAgAInrAxg");
	var mask_graphics_16 = new cjs.Graphics().p("AoKvlIIhg1MADJAgAIohA1g");
	var mask_graphics_17 = new cjs.Graphics().p("AoKvhIJig8IDKf/IpjA8g");
	var mask_graphics_18 = new cjs.Graphics().p("AoKveIKxhDMADJAgAIqxBDg");
	var mask_graphics_19 = new cjs.Graphics().p("AoLvZIMOhNMADJAgAIsNBNg");
	var mask_graphics_20 = new cjs.Graphics().p("AohvUIN6hXMADJAgAIt6BXg");
	var mask_graphics_21 = new cjs.Graphics().p("ApgvNIP3hkIDKf/Iv3Bkg");
	var mask_graphics_22 = new cjs.Graphics().p("AqovGISIhyIDJf/IyIByg");
	var mask_graphics_23 = new cjs.Graphics().p("Ar8u+IUviDMADKAgAI0vCDg");
	var mask_graphics_24 = new cjs.Graphics().p("Atau1IXsiVMADJAgAI3sCVg");
	var mask_graphics_25 = new cjs.Graphics().p("AvGuqIbDiqIDKf/I7DCqg");
	var mask_graphics_26 = new cjs.Graphics().p("Aw/ueIe1jCIDKf/I+1DCg");
	var mask_graphics_27 = new cjs.Graphics().p("AzGuRMAjEgDdMADJAgAMgjEADdg");
	var mask_graphics_31 = new cjs.Graphics().p("AzGuRMAjEgDdMADJAgAMgjEADdg");
	var mask_graphics_32 = new cjs.Graphics().p("AvuunIcTixMADKAgAI8TCxg");
	var mask_graphics_33 = new cjs.Graphics().p("As3u5IWliNMADKAgAI2lCNg");
	var mask_graphics_34 = new cjs.Graphics().p("AqCvIIRxhvMADKAgAIxxBvg");
	var mask_graphics_35 = new cjs.Graphics().p("AmDvUINyhXMADKAgAItzBXg");
	var mask_graphics_36 = new cjs.Graphics().p("AiyveIKhhCIDKf/IqjBCg");
	var mask_graphics_37 = new cjs.Graphics().p("AgKvnIH5gxMADKAgAIn6Axg");
	var mask_graphics_38 = new cjs.Graphics().p("AB7vtIF0glMADKAgAIl1Alg");
	var mask_graphics_39 = new cjs.Graphics().p("ADjvyIEMgbMADKAgAIkNAbg");
	var mask_graphics_40 = new cjs.Graphics().p("AEyv2IC9gTMADKAgAIi9ATg");
	var mask_graphics_41 = new cjs.Graphics().p("AFtv5ICCgNMADKAgAIiCANg");
	var mask_graphics_42 = new cjs.Graphics().p("AGXv7IBYgJMADKAgAIhYAJg");
	var mask_graphics_43 = new cjs.Graphics().p("AG0v9IA7gFMADKAgAIg7AFg");
	var mask_graphics_44 = new cjs.Graphics().p("AHHv9IAogEIDKf/IgoAEg");
	var mask_graphics_45 = new cjs.Graphics().p("AHTv+IAcgDMADKAgAIgdADg");
	var mask_graphics_46 = new cjs.Graphics().p("AHZv+IAWgDMADKAgAIgXADg");
	var mask_graphics_47 = new cjs.Graphics().p("AHcv+IATgCIDKf/IgUACg");
	var mask_graphics_48 = new cjs.Graphics().p("AHdv/IASgBMADKAgAIgTABg");
	var mask_graphics_49 = new cjs.Graphics().p("AHdv/IASgBMADKAgAIgSABg");
	var mask_graphics_50 = new cjs.Graphics().p("AHdv/IASgBMADKAgAIgSABg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:-52.2518,y:-49.7592}).wait(1).to({graphics:mask_graphics_1,x:-52.2519,y:-49.7593}).wait(1).to({graphics:mask_graphics_2,x:-52.2519,y:-49.7595}).wait(1).to({graphics:mask_graphics_3,x:-52.2519,y:-49.7607}).wait(1).to({graphics:mask_graphics_4,x:-52.2521,y:-49.7638}).wait(1).to({graphics:mask_graphics_5,x:-52.2524,y:-49.7705}).wait(1).to({graphics:mask_graphics_6,x:-52.2529,y:-49.7825}).wait(1).to({graphics:mask_graphics_7,x:-52.2538,y:-49.8023}).wait(1).to({graphics:mask_graphics_8,x:-52.2551,y:-49.8327}).wait(1).to({graphics:mask_graphics_9,x:-52.257,y:-49.877}).wait(1).to({graphics:mask_graphics_10,x:-52.2598,y:-49.9387}).wait(1).to({graphics:mask_graphics_11,x:-52.2634,y:-50.0219}).wait(1).to({graphics:mask_graphics_12,x:-52.2682,y:-50.1313}).wait(1).to({graphics:mask_graphics_13,x:-52.2744,y:-50.2717}).wait(1).to({graphics:mask_graphics_14,x:-52.2822,y:-50.4485}).wait(1).to({graphics:mask_graphics_15,x:-52.2918,y:-50.6675}).wait(1).to({graphics:mask_graphics_16,x:-52.3035,y:-50.935}).wait(1).to({graphics:mask_graphics_17,x:-52.3177,y:-51.2577}).wait(1).to({graphics:mask_graphics_18,x:-52.3346,y:-51.6426}).wait(1).to({graphics:mask_graphics_19,x:-52.3546,y:-52.0973}).wait(1).to({graphics:mask_graphics_20,x:-50.1559,y:-52.6298}).wait(1).to({graphics:mask_graphics_21,x:-43.9211,y:-53.2484}).wait(1).to({graphics:mask_graphics_22,x:-36.729,y:-53.962}).wait(1).to({graphics:mask_graphics_23,x:-28.4864,y:-54.7798}).wait(1).to({graphics:mask_graphics_24,x:-19.0956,y:-55.7115}).wait(1).to({graphics:mask_graphics_25,x:-8.4545,y:-56.7673}).wait(1).to({graphics:mask_graphics_26,x:3.543,y:-57.9577}).wait(1).to({graphics:mask_graphics_27,x:17.0138,y:-59.2044}).wait(4).to({graphics:mask_graphics_31,x:17.0138,y:-59.2044}).wait(1).to({graphics:mask_graphics_32,x:38.6781,y:-61.2972}).wait(1).to({graphics:mask_graphics_33,x:57.0147,y:-63.0728}).wait(1).to({graphics:mask_graphics_34,x:69.6855,y:-64.5643}).wait(1).to({graphics:mask_graphics_35,x:69.6856,y:-65.8029}).wait(1).to({graphics:mask_graphics_36,x:69.6856,y:-66.8185}).wait(1).to({graphics:mask_graphics_37,x:69.6855,y:-67.6392}).wait(1).to({graphics:mask_graphics_38,x:69.6853,y:-68.2914}).wait(1).to({graphics:mask_graphics_39,x:69.6851,y:-68.7998}).wait(1).to({graphics:mask_graphics_40,x:69.685,y:-69.1872}).wait(1).to({graphics:mask_graphics_41,x:69.6849,y:-69.4743}).wait(1).to({graphics:mask_graphics_42,x:69.6848,y:-69.6802}).wait(1).to({graphics:mask_graphics_43,x:69.6847,y:-69.8217}).wait(1).to({graphics:mask_graphics_44,x:69.6847,y:-69.9141}).wait(1).to({graphics:mask_graphics_45,x:69.6847,y:-69.9701}).wait(1).to({graphics:mask_graphics_46,x:69.6847,y:-70.001}).wait(1).to({graphics:mask_graphics_47,x:69.6846,y:-70.0156}).wait(1).to({graphics:mask_graphics_48,x:69.6846,y:-70.021}).wait(1).to({graphics:mask_graphics_49,x:69.6846,y:-70.0223}).wait(1).to({graphics:mask_graphics_50,x:69.6733,y:-70.1099}).wait(2));

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#D6A432").s().p("AFMGXIlfosIgnAyQgFAFgHAAQgHAAgFgFIiMivIiDhxQgFgEAAgGQgBgGAEgFQAEgFAHAAQAGAAAEAEICFBxICDCjIAogzQAFgGAIABQAIAAAEAGIFqI+QAEAGgCAGQgBAGgFADQgEACgEAAQgJAAgEgHg");
	this.shape.setTransform(32.7863,-62.679,1.7032,1.7032,-63.2623);

	var maskedShapeInstanceList = [this.shape];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape).to({_off:true},51).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-56.9,-87,179.4,48.7);


(lib.quartier_light_3_forme = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AmLBzIFakmIhHh/IGik9IBiBuIjTCpICtH1IgUAIQhJAchDAqQg6Akg4AmIAAAEIhGAmIBXCVIjDB8g");
	this.shape.setTransform(0,62.35);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.quartier_light_3_forme, new cjs.Rectangle(-39.6,0,79.2,124.7), null);


(lib.quartier_light_2_Forme = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("ApxBXIHXiRIiMltINmCdIAyCYIxoIag");
	this.shape.setTransform(-0.025,42.4);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.quartier_light_2_Forme, new cjs.Rectangle(-62.6,0,125.2,84.8), null);


(lib.quartier_light_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AGBHgIhghbIhEhAQgCgEjLi3IjNixIkakFICoi0IB1BxIBxCYIAdAXIAqglIAqAwIAOAlIAoAvIAAAIIA7BaIAgAjIAAAKIAyBGIBRB8ICcDaQg/AXgSAAQgFAAgBgBg");
	this.shape.setTransform(0.0037,68.9417,1.4323,1.4323);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.quartier_light_1, new cjs.Rectangle(-67.6,0,135.2,137.9), null);


(lib.parcelle_contours = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#D6A432").s().p("AAsZqQgBAAgBAAQAAAAgBAAQAAAAAAgBQgBAAAAgBIgLgdIgNgjIgHgMQgLgUgGgGIgHgKQgEgHgCgCQgHgGgJgMIgJgMIgHgHIgOgOIhPhJIgRgRIgrgnIgggcIASgNIABAAQAKgDAEgDQAGgFAQgJIAYgPIAugbQAKgFAGgHQAFgGAGgBQABAAAAgBQABAAAAAAQAAAAAAgBQABAAAAAAQAAAAAAgBQgBAAAAAAQAAgBAAAAQgBAAAAgBQgHgFgEgKQgGgKgPgXIgOgVQgEgHABgGQADgFAJgEIACgBQAIgMAZgRIAggZQAYgUAJgFIALgKIADgDQADgEgDgHIgBgCQgBgGgGgHQgNgPgFgVQgCgHgJgQIgOgZQgKgQgBgJQAAgFgFgHQgHgJABgHQgEgEgHgPQgFgNgGgFQgEgEgCgNIgBgJIgBgCQgIgJgHgTIgJgTQgHgNgCgGIgphTIgIgSQgHgSgHgJIgFgLIgCgFQgBgDgCgBQgCgBgDACQgOAHggAPIhBAeQgOAEgEADQgPALgiAPIgUAJQgMAGgJADIgOAFIgMAGQgKAIgRAGQgNAFgLAHIhcAsIg3AYIgBgDIgLgYQgQgkgDgSQgBgEgEgHQgFgIgBgEQgDgMgKgWIgKgZQgBgEgEgGIgEgKQgBgNgEgIIgDgFQgFgKgBgHIgCgDQgEgCgFACQgJAFgRAAIgOABQgIACgOgEIgogLQgLgBgOgKIgHgFIgUgLQgPgIgNgIIgkgVIgFgEIgHgDQgOgHgFgDQgHgFACgFIACgNIABgNQgBgGAEg0IAGg8IAAgBIABAAIADgiIADgoQACgPAAgVIAFg5QABgOgHgDQgUgJgdgQIgUgKIgagNIgogWIgtgYIgDACIAAgDQgMgFADgIIADgDIACgCIAAgBQAAgDADgCIA4hkIAOgbQAIgRAIgNIAPgWIABgBIAKgPQAOgWAMgHIAEgCIADgDIABgBQgDgBgHgHQgGgGgCgBQgFgCgKgHQgKgIgGgCQgDgBgCgEIgDgDQgWgKgJgJIgCgBQgJgBgXgRQgNgKgGgDQgJgDgRgKIgPgIQgEgCgBgLIgCgCIABgBIAAgCIACAAQAjghADgIQACgCAIgGIAKgJQAQgVANgKIACgCIgBgBIgFgHIgRgNIgbgXQgSgRgNgIIgXgVIgkggQgKgDAAgJQgBgEABgDIAAgDIABABQABgEAFgDIAwgxIAHgJQAKgNAHgHQAKgJAMgNIB/iOQAEgEAAgCQAAgCgGgCIgBAAIgEgDIgUgSIgOgNIhZhSIgDgCIC7jWQARgVAUgUIARgWIAhglQAEgFAHAAQAGAAAFAEQAEAEAKAHIAGAFIABABIAFAEQACAEACAAQABAAAAAAQABAAABgBQAAAAAAgBQABAAAAgBIAKgLIAGgFIAOgSQAcgfAPgKQAggZAbADIAHAAQALAAAGAEQAAAAABAAQAAAAAAAAQABAAAAAAQAAAAAAgBQADgCgBgIQgBgEABgEQAAgCgGgCIgBAAQgGgCgSgCIgEgBIgTgDIgSgCIgMAAIgLAAIgSgDQgTgDgKABIgCAAIgBgBQgDgDgCgOIgBgQIACgSIADgRQAAgJgDgHIAAgEQACgGAAgKQAAgLADgGQAAgBAAAAQAAgBAAAAQAAgBAAAAQgBgBAAAAIgBgBIABgUIAqgQQAJgDATgDQAXgFAKgEQALgFAZgFIAqgLQAQgDAHgDQAVgLAigEIASgFIAKgEQATgGAVgFIA5gPIAMgEIAQgFQAJgCAJADIABgBIADAAQAHgDAGAFQAFAEAAAIQAAAKgCAHIgNAtIgTBLQgHAdgMAoIgFAQQgRA6gQAjIgBAIQAAAKgCADIABACQADAGgFAEQgBABAAAAQAAAAAAABQAAAAAAABQAAAAAAABIACAPQADAVADAKQADAKAKAaIANAeQAIAOABAIIABACIACgBQAFgEAQgJQAPgHAIgHQALgKAJAIIAGAGQATARAGAJQADAEAKAFIABgCIADgFQATgQAiglIAUgUQAJgJAFADQAMAGALANQAFAGAFAAQAEAAAFgGQAFgIALgKIANgOIARgSIAOgOQAMgMACgIQABgFAGgEIAQgHQALgEAGgEQAJgHAJACQAFABANgCQALgCAWgBIAbgDIACAAQAOgCAIABQAKABAKgBIH0gaQAIAAAEAKQACAHgEADQgEADABAHIABAFIABAIQABAJAAAIQgBARADARQAEAVABAZQACAzAhAlIAOAOQAOAOAGAIQADAEAAAGIACAGQAGAIAEARQAEARAFAHQAFAIgBAQQAAAIAEAQQAEARgBAJIAFANQAAACAFAAIAMgBQAEgCAFAAQAKAAASgEIApgHQAAAAABAAQABAAAAAAQABAAAAAAQAAAAAAgBIAAgCIgBgCQgFgLAAgGQAAgJgFgKIgVgmQgDgDADgEQACgFAGADQAMADAEALQAEAKAGADQAIAEAMgFIAIgDQASgHAKgCIAFgDIABAAQgBgEAFgFIABgBQAEgDACgEIAEgGQADgDAFgLIAKgQQAGgIAJgSIACgFQAIgTAQABIABAAIACAAIADgBIADATIgEAAQgGgBgDADQgDACgDAHIgDAFIgNAXQgQAbgMARQgFAGABADQAAACAIAEQAOAHATANIAWANQALAGAGAGQAEADgDAGIgBAEQgCAEgDABIABACQABADgJAJQgIAIgBACQgGAPgJAKIgDAFIAEAFIABABQADAEANAHQALAFAHAHIALAHQASAKABAHIAAAEIgDAAQgHAAgBAOQAAAEgCADQgGAGgGAQQgGANgDAFIgJASIgFALQgEAIgHABQgGABgGgFQgGgGgVgNQgOgIgMgJIgMgHIgGAHQgNARgDALQgCAEgEAEIgDADIgBACIACABQARAJAUAMIAMAHQAJAHAXANIAUALIAGAFQAEAEADABIAlAYQAQAIAOALIAFADQADADAAAFQAAAJgHADQgFACgEgDIgMgHQgQgIgIgJIgDgBQgCABgDAEIgiA9QgIAQgGAKIgVAjIgNAYQgJAQgGAIQgBAAAAABQgBAAAAABQAAAAAAABQAAAAAAABQAAABAEADIBTAzQAZARAOAGQAMAFABAGQACAFgGAIIgCACQgJASgQAYIgzBTIgBACIACACIB5BHIADACIAjAVQAJAFABAAQAFgCAEACQAEACABAGQABAFgHAJIgFAGQgHAPgPAZIhVCVQgLAUgHAJQgGAHAAADQgBAFgGAGIgBABIgHgBIgBAAIgHgEQgQgHgWgQIgZgPIgJgEQgJgDgEgEIgJgHQgBgBgBAAQAAgBgBAAQAAAAgBAAQAAgBAAAAIgBABIgCADIACACQADABAAAHIgBABQABAGgCAKQgCANAAAFIABAIIACAOQAAAAgBABQAAAAAAABQAAAAgBABQAAAAAAABQgEADgPgCIgKAAQggABgQAIIgNAEQgNADgEAGQgBAAAAABQgBAAAAAAQAAABgBAAQAAAAgBAAQgCAAgCgDQgDgEgJgEIgzgYIgCAAQgJgGgCgEQgEgFAGgEIABgBQAAgCADgFIACgBQAEgFgBgCQAAgBgEgEIgDgCQgHgGgMgHQgOgIgIgGQgHgGADgGQADgDACAAQAEgBAEAEIApAbQAGACACAAQABgBADgGQAPgeAUgWIACgFQgBgDgEgBQgRgGgYAJIgXAIIgTAGIg8AUIgbAIIgzARIgBAAIAAADIAQAvIACAJQACAIACACQACAFgDAFQgCAEgFgBQgQAAgRAQIgBABIgLAJQgCACgEAGQgCADgDADIgVAbQgPAQgPAEQgSADgQgNQgYgTgigiIgTgSIgLgLQgFgHgLAEQgVAHguAVIgaAMQgFACAAAEQAAADAEACQAUANAUAQIARANICbB2IA+AxQARAOAXAbIAdAeIADACQABAAAAAAQABAAAAgBQABAAAAAAQABgBAAAAQAHgIAQgLIARgNQANgLAngeIASgNQAegYAfgSIALgFIAEgBQA1gOARgDQAZgFAQAAIAPAAQARgBAPABQAfADAfAMQARAFAbAUIAPALQAaARAWAVIADACQALAKAIAKQAFAHgCAGQAAAGgIADQgEACgHAIIgHAGQgaAWggAeIhBA5QgPAPgNAJQgBABgBAAQAAABgBAAQAAABAAAAQAAABAAAAQgBADAEAEIAgAiIAIAKQAIAJACAAQACAAAHgIIAPgNIAQgQQADgFAGAAQADAAADAEQAFAGgIAGIgmAhQgEADAAADQAAACAEADQAWAVAgAnIAJALQADADACAAQACAAACgCIApgpQAagYAggfIAggfQAIgHAEAEQADACAAADQAAAEgEAFIh/B6IgGAFIgDACQAAAAAAABQAAAAAAABQABAAAAABQAAAAABAAQAMALAQAUIAUAXQASASAOARQADAEACAAQADABAFgFIAbgYIBShIIAYgVQAGgFAGgBIADAAIAFAbIgEAAQgFAAgHAHQgJAKgOALIhpBcIAEAFIAbAdQAJAJgIAJIgbAbQgDAFgEAAQgEAAgCgEQgDgCAAgEQABgCAEgDIABAAIAUgWQAEgEgFgGIgXgZQgDgDgFAEIhfBUIgNAKIgCABQgBABAAAAQgBABAAAAQgBABAAAAQAAABAAAAQAAAAAAABQAAAAAAAAQAAABABAAQAAAAABABQANALAVAZICWCqIAoAsIA+BHQAZAfAMAJQAHAFgDAHQgBAIgKACQgHABgEgGQgFgJgQgQQgNgMgGgJQgHgJgRgHIgMgGIgagHQhggZgLgGQgCgCgIAAIgJgBIgIgCQgZgHgGgCQgEgCgGAAQgNgDgEgCIgCgCIgBAEIABAdQgBAWABALQABAJgCAKQgBAPACAJQABADgDASIgBAJQAAADgDADQgEADgOgCIgDAAIgJgBQgfgEghACIggAAQgegBgHgCIgXABIgDAAIAAgDIgCgTQAAgSgBgGQgCgKAAgVIgBgjQABgMgCgIIgBgCQgDgMABgIQABgEgFgGIgBgBIgCgBQgDAKgJgEQgGgBgPADIgGACIgRACQgeAGgJAEQgHADgEgCQgGgCgEAHQgCADgMADQgKACgBACQgBACgaAQIgdARQgKAIgRAIIgOAIQgKAIgIADQgGADhIAuIg9AmIgpAYQgvAdgLAGIgFADIhdA7Ig4AlQgvAggOAAgABIXHQgKAKgYAPIgSALIgPAIIgCAEIABABQAJAKANAZIAIANIAOAgIABABQACAGACAAQACAAAEgCQAPgNAWgMIAigWQAAAAABAAQAAgBAAAAQABAAAAgBQAAAAABgBIgCgFQgGgGgLgSIgJgQIgUgfIgFgIIgCgCIgBAAQgCAAgDACgADLV2IhGAsQgYARgPAIQAAAAgBAAQAAABgBAAQAAAAAAABQgBAAAAAAIACAEIAJAOQAUAeAMAWIAEAFIAFAJQAAABABAAQAAABABAAQAAAAABAAQAAAAABAAIABgBQAEAAAHgFIAEgDQApgbAigUIAEgDQAFgEAGgCQADgBABgDQABgFgEgEQgGgGgGgNIgig8IgCgCIAAAAIgDACgAgSVzQgJAEgEAFQgDADgKAFIgKAFIgJAFIgIAFQgDACAAACQAAADACACIAWAWQAWAUAHALIAGAKQAEAGACACQADACADgCIABAAIACgBQAGAAABgEQACgFAGAAIABABQAEABAGgGIACgCQAJgKAKgDQAHgBABgFQABgCADgCIAEgEQAAAAABgBQABAAAAAAQAAgBABAAQAAgBAAAAQgDgMgFgEQgIgFAAgKQAAgDgHgGIgCgDQgLgMgEgMQgCgFgHgJIgEgHIgCgCQgBABAAAAQgBAAAAABQAAAAgBABQAAAAAAAAQgDAIgGAAIgCAAQgDAFgMAIgAJsRsIgCACIAAAAQgCADgCABQgIACgPAKQgGAGgIADIgBABQgOAHgKAIIgGAFQgPALgGACQgEACgIAHIgKAHIgMAFQgIABgOALIjwCaIgDADQAAACACADQAJANAOAYIAGALQAJANADAJIABABIABADQADAHAEABQAFACAFgEQAagTAtgaIA9gmQATgLAfgUIApgaIASgKIAygiQAMgGADgEQAHgGAPAAIABAAQAEAAABgBIgBgFIgBgDIgBgLQAAgLgEgGQgDgEAEgDQADgEADgHIAFgKQADgEACgUQADgXgDgQQgCgKgEgDIgBAAQgCAAgDAFgACOUKQgDAAgGAEIhoBDQgBABAAAAQgBABAAABQAAAAAAABQAAAAABABIArBEIAIAQQACAEACABQACAAAFgDIBUg2QAEgCAEgEIAPgJQAAAAAAAAQABAAAAAAQAAgBAAAAQAAAAAAgBQABgDgCgCQgNgSgTghIgLgVQgFgHgCgFIgBgDIgBAAIgDABgAhMTUQgDACgEABIgIAEQgTAOgNAGIgOAJIgPAKIgMAHQgOAKgJACQgDABgHAHIgHAHIgBAAIAEADQAKAGAOAPIAPAOIAWAVIAXAUIAGAFQAOAKAFAIIABABQABAAAAAAQABAAAAAAQAAgBABAAQAAAAABgBQAJgJATgLIAMgHQAUgOAVgKQAIgFgDgKQgDgIgLgPQgFgHgIgPIgthFIgCgCgAOMWAQASACAIgDIANgBQgDgeACgRIgBgIQgBgJABgEQADgJgDgJQgDgHACgOQACgFAAgFQgBgKgIgCQgbgFgXgDQgLgCgLgEIgGgBQgFgCgRAAIgBAAQgeACgUgDIgEgBIgCgBQgDAFACAEQAEAJAAASIABANIABATIABATQACAOgBASIAAAUQAAAFADACQADACAGgBIADAAQAKgBAHAAIAHACIAIACIAGAAIADgBIAOABIAegBIADAAIASACgAEYSuQgHAIgQAJIgJAGIg5AjIgYAPQgDADAAABQgBACADAEIAKAQQAMARAEAJIAYAqQAAACAEAAIACABQAEgEAJgFIAJgFQALgJAWgNIAkgXQAPgLALgFIABgCIgBgDIgjg7QgGgNgLgQIgFgDgAAtSGQgGAGgMAGQgJAFgXAQIgHAFQgMAJgJABIgDABQgEAGgLAGIgBABQgDACAAADQAAAFAEAEQAIAHADALQABAFAHAJIAFAGIAZAoIALARIACAGQACAFACACQAFADADgBIABAAQAFgBAFgEQALgJAHgDQAFgCAIgGIAVgOQANgLAKgCQADgHAKgDQAFAAADgGQABgFgDgEQgEgGgBgHQAAgDgDgDQgFgFgEgKIgSgdIgBgCIgDgHQgDgLgIgGQgFgEgDgIIgFgIQgFgFgFAAIgBAAQgFAAgEAFgAN3QXQgBADACAEIADAIIACAGQAFAKAQARIAMAOQAhAqAeAiIAMAPQAMAPAJAIQAEAEAAADQABADgEAEIgXAWQgDADAAADIAEACIBiAbIABAAIA/AUIAGADQAGADADAAIACgBIgBgCQgEgGgIgHIgJgJQglgsgrgvIgzg4IgdghIgNgQQgQgSgJgIQgFgFgIgLIgKgMQgKgKgKAEQgHAEgNAAIgCAAQgGABgCADgAEvShQgDACgBACIABAEQAMARARAfIARAfQAGAIACABQACAAAIgFIBOgyQAUgLAJgIQAEgEAAgCIgDgHQgQgagRgeIgPgbIgDgCgAMnTSIAjABIANABQAcADANADQAeAIAlAGIAIADQAEACACAAQAEgBADgFIAFgGIAQgOQADgDgCgDQgIgLgTgWIgZgdQgdglgdggQgSgTgCgPIgDgHQgDgEgBgFIgDAAIgBAAIiNAXQgGABgCACQgBABADAIIAGAPQAIAUADALQADAOALAZQAEAHAFARQAGATAFAJIABAGIAAABQABAGAGABQAHACAJgBIAKgBIAEAAgAK5Q3QgMALgeARQgGADgBAEQgBAEABAJQAGARgBAZQgBAYgKATIgBABQgFAKgBAFQgCAHACAIQACAEgBADIgBAHIABAAQAHACAFgCQAOgIARgDQAWgEAYgDIAOgDIAFAAIAEAAQAFACACgCQACgCgBgGIgrh0IgLgeQAAgBAAAAQgBgBAAgBQAAAAgBAAQAAgBAAAAIgDABgAC6QwIgJAGQgIAHgRAJIgTAMQgJAHgIADIgMAHQgLAHgGABIgBABQgBAAAAAAQAAAAAAAAQAAABAAAAQAAAAABABQADAIAGAKQARAaASAhIAKAQIAOAYQADAIAHgCIAAAAQABgIAKgCQAOgDAMgMIASgNIAKgFIAGgFQARgLAIgEQAKgEACgIQABgIgIgKQgIgHAAgHIgDgFIgBgBQgIgJgBgIIgDgFIgEgEQgJgOgFgMQgEgJgFgFQgGgHgDgGQgCgEgDACIgCAAgAAAQXIgDAEIgRAMQgGADgKAKIgPANQgKAJgGADQgGADgOANQgMAMgKAFQgBAAABAFIAAABIADAHIAKAPQAGAHAIAOIAGALQAHAKADABQAHACAGgIQAGgGAEgCQAIgDAZgRQAYgRALgFIAKgGQAKgIAIgCIAAgCIgLgRIgIgMIgEgGQgJgNgDgIIgFgIQgHgKAAgIQAAAAAAgBQAAAAgBgBQAAAAAAgBQgBAAAAAAIgBAAIgDABgAJOPsQgKAEgDAEQgHAJgMAEQgHACgDAEQgDAEgKAFIgIAEIgBABQgZATgTAKQgHADgNAJIgOAKIgGADQgBAAgBAAQgBABAAAAQAAAAgBABQAAAAAAAAQAAABAAAAQAAABAAAAQAAABABAAQAAAAABABQAJAJACAMQACAGAIALIAIAOIAQAeIAFAEQAFABADgDIAGgFQARgMAGgDIAHgEIAPgIIAMgKQATgOAJgEQAJgDALgJQALgIAJgFIACgBIAFgCQAIgCgCgKQgBgFgDgGQgFgHAAgFQgFgDAAgIQAAgPgMgPQgFgHAAgJQAAgDgFgGIgCgCIgCAAQgDACgJAEgAFTPOQg9Aqg5AfIgDADIABAEQAGAGALAUIAuBQIADAHIACAFQAAABABAAQAAABABAAQAAAAABAAQAAAAABAAQAFgBAIgGIAGgFIBlg/QAFgEgFgIQgSgdgUglIgbgvIgCgCIgBAAQgBAAAAAAQgBAAAAABQgBAAAAAAQgBABAAAAgACEOuQgDAEgLAHIgkAeQgMAMgJADIgEACQgKALgPAJQgHAEAFAJQAGAOACAGQADAKAJANIAJAOQABADAFAFQAKANgBAKIABAAQAEgFARgMIAIgEQAjgYAYgNIAMgIQAKgHAIgDQAAAAABAAQAAgBAAAAQAAgBAAAAQAAgBAAgBIAAgBQgFgEgFgKQgCgHgEgDQgEgEgBgEQAAgIgKgMIgEgFQgEgFgCgGIgBgCQgDgJgFgEQgIgEgBgLIgBgCgAKMPGIgVAKIgHAEQgHAEADAFQANAcAMAhIADAIQAGAPABAIQAAAEAEABQADABADgDQAEgDAMgIIAIgFIABAAQAFgEgBgEIgghZQgCgGgCgBIgBAAgAHYOAQgGAGgKAFQgKADgMAJIgOAKQgOAIgFABQgGABgGAGIgYAPQgFAEgBACIADAGQAHAJAJASIAOAYQARAYAHATIADAFQAFAGABAEIACABQAGABACgCIAHgFQANgKAGgCQAIgEAUgNIAPgKQAogaATgLIANgJIAOgLQABAAAAAAQABAAAAgBQAAAAAAAAQABgBAAAAQAAgBgBAAQAAAAAAgBQAAAAgBgBQAAAAAAgBIgPgNQgNgMgKgGQgNgJgFgKIgCgCQgHgCgFgJQgCgEgDAAIAAAAQgHABgEgJQgCgEgHgEIgHgDQAAAAAAAAQgBgBAAAAQAAAAgBABQAAAAAAAAIgBAAIgBAAQgGABgHAHgAM3OnQgLAEgSAAIgUACQgVAFgdAEIgvAKQgBAAAAAAQgBAAAAAAQgBABAAAAQAAAAAAAAQAAABAAAAQAAABAAAAQAAABAAAAQABABAAABQAIAPAIAaIAIAXIAIAUQABAFAFACQAFADAJgEIAHgCIB7gTQAEgBACgCQABgCgCgGIgIgWQgJgXgCgMQgBgFgGgMIgGgOIgBgBIgDgBgAEFNIQgCABgBAEQgBACgDACQgHACgJAKIgNAKIgKAIQgIAHgIADQgEACgKAJIgDAEQgJAJgMAHQgNAHAGALIAMATIALARIAOAaQACAGAFAFQAJAKAAAKQAAABAAABQAAAAAAABQAAAAAAABQABAAAAAAQAAAAABAAQAAAAABAAQAAAAABAAQAAAAABAAIADgBQAGgCABgEQACgGAJgBQAHgBAEgFQAEgEAKgFIAJgFQAOgLANgDIACgDQABgDADgCIAZgPQAGgDABgGQABgHgFgFIgBgBQgFgEAAgFIgDgBQgCgBgBgEQgBgMgOgPIgBgBQgDgDAAgCQgFgKgLgSIgKgSIgBgEQAAgBAAAAQAAgBAAAAQgBAAAAgBQAAAAgBAAIgFgBIgCAAgANTO2IAAACIAEAJIAOAmIADAJIAGARQABAEABABIAFAAIAdgFQAAAAABAAQABAAAAgBQABAAAAAAQAAAAAAgBQAAAAAAAAQAAgBAAAAQAAgBgBAAQAAgBAAAAQgfgdgagkIgFgFIgBgBgAPGMxQgGAHgYAZIhIBFQgDADABAFQAAABADADQASASAoAtIADAEQAHAIADAFIACACQACAAAEgDIBAg4IAbgYIAUgQQADgEgEgEQgOgMgegiIgngqIgCgCIgDACgABaNkQgGANgOAQIgLAOIgDAEQgJAMgCAHQgBAEgHAHIgFAIIgWAkQgBADADAEIACAGIACAFIADgBIACAAIABAAQAGgIAQgLIAzgmQAPgJAEgJQACgDAFgCIACgBQAAAAABAAQAAgBABAAQAAgBAAAAQABgBAAAAIgBgCQgGgDgHgNIgBgEQgEgIgFgGQgIgMgDgHIAAAAgAJSMxIgJAGQgEAEgLAFIgLAHQgGAFgTALIgYAPQgFAEAAABQAAACAGADQAHAEAIAJQAHAIAVATIAaAWIAWAWQAEAFADABQADABAFgEQASgNAPgCIADgBQABgBgBgGIgNgjQgKgYgEgPIgIgVQgIgSgCgNQgBgFgFgCQgDAEgFACgADvFVQgDAAgCADIgDAFQgQAWgFAFIgCACQAAADAEACIALANIAVAdIAfAoIAJALQAJANAGAGQADADAAAFQAAAFgFAFIhnBaIhYBOIg6A0Ig6A0IgXAVIgfAbQgDADABAFQAAAFAEAGQAMASAIAWQAOAIAAAUQAJAJAJAWIAGANIABACQAAABAAAAQAAABABABQAAAAABAAQAAAAABAAQAAAAAAAAQAAAAAAAAQABgBAAgBQAAAAABgBIAAgCQAFgKALgOQAHgIAKgRIAFgHQAQgZAKgLIAZgiQAKgOAIgHQADgDAGgKIAIgLIAKgMQAXgaAJgHIAKgJQANgMANgHQAdgQAjgeIAcgWICPhxIAOgKIABgCQABgBgFgEQgMgLgWgYIgfggIgRgOQgZgSgjgcQgPgMgTgNIgUgPQgGgEgFgIQgBAAAAAAQAAgBgBAAQAAAAgBgBQAAAAgBAAIAAAAgAF/LnIhjBOQgFAEAAACQABAFAFAGIAQAaQANAVAFALIANAYQAKAPAEALQACAEACABQADABAEgEIAwgfIAZgPIAugcIACgDIgCgFQgMgUgNgQIgQgYIgVgeQgFgGgGgKIgLgQQgDgDgBAAQgBAAgBABQAAAAgBAAQAAAAgBABQAAAAgBAAgAKpMEQgRALgXAJIgKAFIgKAFQgCABABAHQAFAKAEAQIAOAkIAQAuIAEAMIAFANIABACQAAAAABAAQAAAAABAAQAAAAAAAAQABAAAAAAQAEgCAIgBIADgBQAUgDAUgEIAwgJIAogEIAAAAQAHAAABgCQAAgDgFgFQgHgFgIgLIgFgFQgOgQgdgfIhFhIIgBAAIgEABgADLLlQgKAHgVAWIgNAOIgEADQgOAOgFAKQgDAGgMANIgDAEQgJAJgDAJIAEAJQABAGAEABIABABIABACQACAHAHAKQAKAMACAJQAAABAAABQAAAAAAABQAAAAABAAQAAABABAAIADgCQAHgHAUgQIAPgMIAVgQIAZgVIAJgGQALgGADgHIACgEIgEgCQgFgDABgEQABgCgEgEQgDgFAAgDQAAgEgFgHIgEgGIgEgHQgGgJgBgFQAAgDgEgBQgFgBgCgIIgDgDIAAAAgAMyKPQgOAOgcAYIhAA5QgGAFAAACQAAACAGAFIAdAdIA9BBIATAVIAMAOIAMAPQABABAAABQABAAAAAAQAAABABAAQAAAAABgBIABAAQAEgBADgDQAFgIAOgMIAKgKQATgTAVgTIAKgLQAPgPAJgFQAAAAAAAAQABgBAAAAQAAAAAAgBQAAAAAAgBQAAAAAAgBQAAAAAAAAQAAgBgBAAQAAgBAAAAQgIgHgSgUIgPgRIglgpQgVgZgUgUIgOgQQgCgDgDAAQgCAAgCADgAITJyQgOAOgVAOIgqAiQggAYgLAKIgFAEQgFACAAACIAEAHIAEAEIAwBHQATAbANAVQAAABAAABQABAAAAAAQABABAAAAQABAAAAAAQADAAADgCQAJgIAWgNIBEgqQAEgDgDgGQgKgVgMgkIgJgYQgKgZgDgNQgBgGgEgIIgFgOIgBgEQgCgFAAgDQAAgFgBgBIgDgBIgBAAQgDAAgCADgAAkJjQgRANgtAdIg1AhIgWAOIgVANIgFAEQAAACACAEQAGAIAJAVIAhBDQAAABABAAQAAABABAAQAAAAAAAAQABAAAAAAIABgBIADgCQAIgIARgOIAPgNIARgPIAsgoQARgOAWgVQAEgDAAgCQABgDgCgFIgKgXQgLgWgEgOIgEgIIgBgBQgBgBAAAAQAAgBgBAAQAAAAAAAAQgBgBAAAAIAAAAIgEACgAFCKKQgIAIgSANIgQAMIgsAiIgBAAIgFAEQgFACAAADQgBAEADAEQAEAEAEAJIAOAaIATAkQAAABABAAQAAABAAAAQABABAAAAQABAAAAAAIADgCQAFgGAOgKIBIg4IAFgEIAHgEQAEgGgDgDQgJgJgQgZIgZgkIgDgDQgBAAAAABQgBAAAAAAQAAAAgBABQAAAAAAAAgAItJ9IgBABIAxCBQAFALABAIIABADIACAAIAMgGQAVgJAUgMIAIgEIgHgGQgGgFgOgPIgNgPIgagbQgSgUgLgKIgBgBQgLgJgCgIQgBgDgGgCIgBAAgArCIiIgFADQgFACgGAAQgJABgCACIgEACIACAEQAFAKAEANIAOAkIAKAaIAWA6IABAHQAAAFABACQAHAHAGAUIADANQADAGABABQAAAAAAAAQAAABABgBQAAAAABAAQAAAAABgBIADgBIAjgSIAQgHIAVgJIAVgKIAVgJIAOgJQACgDgBgJIgFgMIgGgMIgHgZIgDgHIgDgHIgDgIIgCgEQgEgKAAgGQgBgEgGgNIgDgGQgGgNgEgQIgFgRQgFgMgGgDIgEgBQgEAAgGACQgfAOggAKQgWAHgMgBIgDACgAKqH8QgHAHgOAKIgOAKQgfAcgUANIgNAKQgNAMgIAEIgCABQAAACADADQAMAKAWAXIBbBiQAEAEAEABIAFgDIBthiIADgDIgCgDIgCgCQglgmgagfIghgkIgUgWQgDgFgCAAQgCAAgEAFgAHFIjIhtBWIgFAEQAAACAEAFIAbAqIAJANIAMARIABABIADgCIAsgiQANgKAdgXIAvgmIACgCIgBgDIg4g6IgDgEQgDgEgCAAQgEABgIAHgAgPHoQgRAJgXALIgsAUIgpAUIgUAJIgeAPQgEACgBACQAAABACAEIAPAfQAVAqAJAWIAEAIQAGAJABAHIABABQAFABADgCIAfgVIAYgOIAngZIAmgaQAPgKAMgGQAFgDgCgGIAAgDQgHgMgJgXIgMgdIgMgfQgCgDgCgBIgCAAgAomHyQgFAAgDACIgBABIgBABIgGAGIAAAMIAHAQIAIAVQAHAOAIAZQAFATAHATQAEAHAEAPQADANAEAIQAFALAFACQAGABAIgIQACgCA9gcIARgJQAogVAKgDIABgBIgBgDIgDgEQgIgKAAgJIgzAYIgLAHQgHADgFgDQgHgEAAgKQgBgLgIgQQgDgFABgCQACgDgFgDQgHgDgDACIgNAEIgKACQgDACgFgCQgIgDgDgIIgFgUQgEgTgFgHQgBgDAAgHQgBgGgHgHIgBgCQgDADgKACgACfIXQgGABgIAHIgGAEIhQAzQgBAAgBABQAAAAgBABQAAAAAAABQAAAAAAAAQgBACACAEQAMAZAIAVIAEAIQAFAJABAHIADgCIABgBQAKgHAIgIQAWgVAagWIAagXQAAAAAAgBQAAAAAAgBQAAAAAAgBQgBgBAAgBQgGgNgMgiQgBgGgDAAIgBAAgAMZITQADACAAAEQAAADgEAEIgFADQgMAKgGAEQgBAAAAABQgBAAAAABQgBAAAAABQAAAAAAAAQAAACADADIA3A9QAAAAABAAQAAAAABgBQAAAAABAAQAAgBABgBICWiFIASgPQACgDABgCQAAgDgDgEQgXgWgXgRIgBgBQgmgcgWgMQgfgQgkgBIgQgBIgUgBIgBAAIgMACQgIACgDgBIgUADIgnAHQgvALgWAQIgFAGQAAABAEAEQAYAXAVAbQAHAHAFAEQADADADgEQAOgOAXgQIAbgWQAJgGAFgFQAFgGAEAAQADAAADAEQADAEgBACQAAADgFAEIgBABQgOAJgSAPIgZAUIgXASQgDACAAACQAAACADADQAdAdAPATQAEAFACAAQABAAAFgEIAIgGQAIgFADgFQACgDAEAAIABAAQADAAADACgAmPHDQgBACADAGQAIARADAPQADAMAIAPQAJAQAHAYIADAIQADAHABAGQAAAEADADQAHAFADAPQABAFACAAIABAAIADgCIARgHIBEghQAHgCAGgGIAlgPQADgEALgDQAGgCAJgFQAMgHAGgBIALgFQAMgIAIABQAAAAAAAAQABAAAAgBQAAAAABAAQAAgBAAgBIAHgFIAIgEQAOgFAFgBQAGgBAJgGIAOgIQAFgBAKgHQAJgHAGgCQACAAgBgGIAAgEQABgCgEgEQgFgGgBgDQgCgQgLgNQgCgCAAgEQgBgPgIgGQgFgFAAgKQAAgFgGgHIgDgEIgDgBIgEABQgJAIgIgEIgEABQgJAEgMACQgUAGgFADQgHAEgNACQgIABgGADIgGADQgIAGgFAAQgKAAgTAIIgQAFIgLADIgQAEIhcAfIgFABQgJADgDACQgDADgGgBIgEgBQgIACAAACgAnAIjQAGAEADASIAIAaIADAEQACABAFgCIAPgIQARgIAIgCQAEAAABgEQACgEgDgFIgFgOQgBgIgEgFQgGgKgJgZIgJgaIgEgMQgDgGgEAAIgIABIgRAFQgSAGgFABQgHABgOAFQgIAEgKACQgDABgBACQgBAEACADQAGAHgBAGQgBAGAGANQAHAOgBAIIABABIACAAIAKAAQAIAAAEgDIADgBIAMgBQAFAAADABgAJkGoQgOAMgVAPIgfAYQgMAKgZASIgUAPQgEADABADIAAABIACAFQAfAcAbAgQADADADgCIA1goIAcgXIASgPIAVgQQAHgEAAgCQAAgCgGgGIg4g7IgCgBgAB0GqQgGAFgNAEIgZAMQgRAJgPAGQgOAFgSAKQgEACACAEIAMAcIAJAWIASApIABADIAFAIQABAAAIgGIACgBIAfgUIAyggIAEgCIABAAQAGgDABgCQAAgCgCgHQgFgKgDgMIgWg6QgCgFgDAAgAD/HYQghAXgZAPIgUANIgBABQgEADADAEQAIAOAHAZIADAHIACAEIAAAAIACAAQAGgBAFgHIBFg+QAGgFAAgEQAAgEgFgFQgJgJgFgJQgCgDgDgBIAAAAQgBAAAAAAQgBAAAAAAQgBABAAAAQgBAAAAAAgAvgHLQgCAFAHAFIAcARQAOAHALAIQASALAYAMIASALIACACQAKAGAHAAQAJACAIADIANADQAHABAWAAIASAAIAIgCQAKgEAAgCQABgDgIgIQgCgBgKgcIgHgPIgGgLQgEgFgFgBIgIAAIgFAAQgNAAgLgEQgKgEgLACQgKADgQgEIgKgCIgWgCIgTgBIgDgBIgTgDQgGgDgDACQgFAEgJgEIgCAAIgDgBQgFAAgBAFgAvICYQgBAAgBAFIgBAOQgCAVgCAnIgDAuIgEA3QgBAhgEAXQgBAEAFABQAMADARABICcATQAKABACAJQAEAWAQAlIAJAYIAKAbQACAFACABQAAAAABAAQAAAAABAAQABAAAAAAQABAAABAAIALgEIAIgDIAhgKQAQgGAXgGIAGgCQAfgIANgIQAEgCAAgCQABgCgDgDQgEgFgBgIIgCgIQgJgRgKgfIgJgbQgVg1gOgpIgMggIgHgTQgdhCgRgOQgHgBhYgFIhggEQgNABgXgEIgDgBIgDAAQgBAAgBAAQgBAAAAAAQgBAAAAAAQgBABAAAAgAC/GGIgvAWQgHACgCACQgBADADAHQAUAwAIAdQABAGAHgCIACgBIBDgrQAGgEAAgDQABgDgFgFQgHgGgQgXIgMgQIgLgNIgDgBIgEABgAq6CuQABADAHAQIALAYIAIAQIAMAjIAMAhIALAdIALAfQAHAZALAaIATAyIAIAXQACAFACAAIAMgDQAPgEAGgEQAGgEAMgDQAKgDAFgDQAHgFARgEIAdgIQATgFAIgEIADgCIgBgEQgLgTgIghQgCgFgEgGQgFgGgFgOIgEgLQgDgIgCgKQgDgNgDgFQgLgUgGgRIgNghIgMgeQgCgFgJgBIghgFIghgFIgLAAQgYgBgLgEQgXgKgWAAQgFAAgCgCgAhNEYIgBAEQADADADAKIA6CKQAGAOADAEQADAFAEgDIAsgVIAegPQAVgKAOgFQAAAAABAAQAAAAAAgBQABAAAAAAQAAAAAAgBIAAgDQgHgOgKgeQgJgbgFgKQgFgLgLgCIgVgDQgYgDgLgDIgkgHQgWgDgWgGIgEgBgAvaGfQAAAEgCAIIAAADQgBAAAAAAQAAABAAAAQAAAAAAABQAAAAABAAQABACAEAAIACAAQAlAGAoABIAXAEQAWAEAXAAQAPAAAMADIAEAAIACgGIgBgKIAAgBQgFAAgHgDQgKgEgFABIgBAAQgDACgDgBQgHgCgQAAIgUgBIgGgCQgJgCgGAAQgKgBgRgDQgSgDgNAAQgQABgJgDIgBAAgAn1DMQgBABAAAAQAAABgBABQAAAAABABQAAAAAAABQAGAKAKAaIACAGIAaBBIASAuIAQAnQAJASABAKQABAEACACQAFACAEgDQAGgEANgCIAEgBQAIgBAMgFQALgFAKgCIAZgGQAFgBABgCIAAgFQAAgGgGgFIgFgIQgNgggTglIgeg/IgMgWQgFgHgOgBQgOgBgXgFIgbgFIgEgBQgPgCgEgCIgBAAgAlrDpIAOAbQAIATAQAeIAXAwIAGAMIAHARQADAHAEACQAEACAJgEQAIgDALgDIAUgHQAHgDAWgGIAhgJIABAAQAGgCAAgCQACgBgDgGQgDgJgEgHQgEgGAEgGQACgEAEAAIACAAQAGACACAIIAGAUQACAEACABIAGgCIADgBQAKgFAHgDIAagIIAHgDQAGgBABgCQABgCgBgFQgDgEgCgIQgFgMgCgEQgDgEgCgFQgCgIgHgBIghgGIgigHIi1ggIgGgCIgCgCIgFAAQgCABADAFgABhE3QgBABAAAAQAAAAAAAAQAAABAAAAQAAABAAAAIANAiQADAMAIATIAEAMIADADIADAAIAQgIIAlgSQAOgHAQgbQAAgBAAAAQAAAAAAgBQAAAAAAAAQAAgBAAAAQgBgBAAAAQAAAAgBgBQAAAAgBAAQAAAAgBAAIAAAAQgLABgUgEIgkgGQgRgEgKgBIgMgDIgDgBIgCAAIgBAAgAC8ExQgCgFgGgDQgMgGgWgSIgOgKQgLgHgFgHIgBAAQgDAAgCADIgCAGQgHARgFAHIgBADQAAABAFACIAOADIANACIAUAEQAUADAGADIAPACIAAAAgAg3CfQgCAAgBAHIgDAOIgDALQgEAXgMAnIAAABQABABAAAAQABABAAAAQABAAAAAAQABAAAAAAIBbAQQAqAHASAFIADABIADgDIABgFQALgXAHgKQADgEgFgDQgggYgcgLQgbgMgigRIgagNIgFgCIgBABgAFnCHQgHADgJADIglALIgNAEIgCABIgmANIgDACQAAAAAAABQAAAAABABQAAAAABABQAAAAABABIAUAUIAsApIAOAMIAJAGQAHAGAFgCQAQgCAOgUQATgZAJgJQAEgEAAgCQABgBgEgFQgDgEgDgKIgCgFQgJgbgHgNQgCgDgEAAIgDAAQgHABgMAFgAhQCWQgJATgKAjIgHAYQgCAJgEAFQgCADADAFQACAEAEgBQADgBABgGIAFgXQAGgZAEgMIAGgZIADgFQADgDgBgCIgBgDIAAAAQgBAAgBAAQAAAAgBAAQAAABgBAAQAAAAAAABgAlpDNIC1AhQAUAEALAAIALAEQAEACACgBQADgBABgEIAihgIAAgFIgDgDQgLgEgegPIgfgOIgygXIgjgRIgDAAQgBAAAAAAQgBAAAAABQgBAAAAABQgBAAAAABQgDAHgNAPIgkAsQgcAlgYAaQgDADABAFIABAAgAAXA5QgBAAAAAAQgBABAAAAQAAAAgBABQAAAAAAAAQgBABAAAAQAAABAAAAQAAABAAAAQAAABABABIAmB2QABAFACAAQANADAVAQIAJAGQAFAEABgBQACAAAEgFQAFgGAFgBQANgFAZgMIA3gWQAGgCAAgHQgBgKABgWIAAhCQAAgBAAAAQgBgBAAgBQAAAAAAgBQgBAAAAgBQAAAAgBAAQAAAAgBAAQAAAAAAAAQgBAAAAAAIgkACIggABQgHADgJgCIgJgBQgeACg7gCIgFAAgAl1AQQgHAOgSAUIhXBuQgHALgGAEQAAAAAAABQAAAAAAABQAAAAAAABQAAABAAAAQAAABAAABQABAAAAAAQAAABABAAQABAAABAAIAlAGIAZAEQARADAYAGQAFABAEgFIAogzIAsg1IAaggQABAAAAAAQAAgBAAAAQAAAAAAgBQAAAAAAAAQAAAAAAgBQAAAAgBgBQAAAAAAAAQgBgBAAAAQgvgTgsgYIgEgBQgDAAgCAEgAGyBvIgJADQgMADgKAFIgDACIABAEQAHALAKAdIAGARQAAABABAAQAAABAAAAQABABAAAAQAAAAABAAQAAAAAAAAQABAAAAAAQABgBABAAQAAAAABgBQAOgKALgFQACgBgBgEIgBgDQgIgdgIgUQgBAAAAgBQgBgBAAAAQAAgBgBAAQAAAAgBAAgALCAkQAAAAgBAAQAAAAAAAAQgBABAAAAQAAAAAAABQgFALgNAQIgHAJQgIALgNAXIgQAcIgEAFIgCAFQAAABAFACIAWALQAXALAIAFQAEADAHgDIATgFQAPgGAGgBIAGgBQAOgEAKAAIAHABQAHABACgCQACgBAAgHIABg6QAAgHgHgCIgcgRIgDgCIgTgLIgbgRIgDgBIgBAAgAgCBHIgJAKQgLAKgEAHIgTAfQgCADABADIABABIAgAPIAZAMQAMAEAKAIQAEADACgCQABgCgBgDIgBgCQgHgOgEgRQgFgUgJgXIgDgLIgDgLQgCgFgDgBQgCAAgDAEgApNhaQgCABgCAHIgBAFIgsBqIgRAoIgJAWQgCAIgEAFQgCADgFAQQgFASABAEQAFAEAPAGQAPAGAGABIAaACIAuAEQAOACAMADIAKACIALABIABAAQAFAAAEgGQAJgPAXgcIAVgaQAdgnAPgSIAbghQADgEAAgCQgBgCgEgCQgXgIhDgfIhjguIgFgEQgDgCgCAAIgBAAgADwA2QgCACABADQABAJgBAXIAAAeQABAYgCAKQAAABAAAAQAAABAAAAQAAAAABABQAAAAAAAAQAAAAABABQAAAAABAAQAAAAABAAQAAgBABAAQAQgHAjgKIAGgCIAcgJQAagKAQgDQAFgBACgDQABgCgBgGIgSgtQgCgGgDgBQgDgCgGABQghAEgigCIgOAAIgUgBQgBAAAAAAQgBAAgBAAQAAABgBAAQAAAAAAAAgAq0iEQgDAGgGAFQgLAKgOARIgVAYQgOANgCALIgBAhIAAAmIgBAaIAAAbIAAALQAEAsACAEIAQABQAUABAVAGQAFgCAOgrIBLi2IADgKQACgEgEgDIhRgmIgBAAIgDAFgAudCEIAKABQASACBOADIAoACQACgFgBgdIgCgiQgCgJACgMIABgMIABhEIAAgFQAAgOgBgEQgBgGAFgEQANgMAWgaQATgXALgLIAFgGQACgEgBgBQgBAAAAgBQAAAAAAAAQgBAAAAAAQAAgBgBAAQgLgCgWgLIgQgIIg5gaIgzgXIgIgEQgbgMgKgGQgDgCgEAAIgBgBIgCABIAAACQADAKgFAQIgBADIgBAJIgEAPQgBAAAAABQAAAAABABQAAAAABABQAAAAABABIBQArQAOAJALAFQADABABADQABADgCAEQgCADgCAAQgDABgFgDQgKgHgNgGIgIgEIgZgOQgfgSgMgFIgCgCIgJAxQgCAMgIAeIgIAnIgBALIAAALQgEAggBAoIgCATQgDAZACALQAAAFgBAFQgDADACACQABACAFABIAWADIAEAAIALABgANcAAQgDABgDAEIgLASQgLATgHAJIgKAQQgCADABACIADADQAMADAQALIA5AhIAIAGIAGAFIABABQABAAAAABQABAAAAAAQABAAAAAAQABAAAAAAIACAAQADAAABgEIAjhBIADgEQAEgDgBgCQAAgCgEgBQgQgGgYgPIg7ggIgEgBIgBAAgAg7BjIgIAWIAAACIANgWQALgTABgFIgGgDIgBgBQgEAFgGAVgAw4h/IhYCYIAZANQA4AeAQAIIBKAlIACACIAFABQAAAAAAAAQAAgBAAAAQAAgBAAAAQAAgBAAgBQAAgKACgTIABgSQACgxADgXIABgTQABgaAFgLQADgGgJgEQg4gggigYQgBAAAAAAQgBgBgBAAQAAAAgBAAQAAgBgBAAIAAAAIgEAEgAivgiIgDAFIgOASQgHAKgGAGIgNAQQgLAPgIAIQAAAAgBABQAAAAAAABQAAAAAAABQgBAAABAAQABADAIAAQAGAAAeAPIAOAGIANAHQAKADAeANQAeAOAEADQAEAFADAAQABgBACgGQACgLAGgNQAHgTABgGQAAAAAAgBQAAAAAAgBQgBAAAAgBQAAAAAAgBIgkgiIghgcIgOgOQgRgSgDAAIgBAAQgBAAgDAEgAGTAnQgMAHgPABIgEABIABAFIAJAXQAHARADALQABAEAEgBIARgFQAJgEAKgCIABgCQAAAAAAAAQABAAAAgBQAAAAgBgBQAAAAAAgBQgFgIgFgUIgGgSQgCgGgEgBIgDgBIgGACgAJCghQgMAKggAMIgEABQgUAHgPAIQgNAGgiAOIgNAEIgEADQgCACABAEQAFAJADALIAHAVIABAFQAAAGADABQAEAAAEgCIAFgDQAPgDAdgKQAYgJAPgEQAOgEASgHIACAAIBIgYQAFgCAFAAQAKAAAJAGIAGAEIABAAQAFAAABgCQABgDgFgDIgVgMIgfgRQgdgQgTgOQgBAAAAAAQgBgBAAAAQgBAAgBAAQAAAAgBAAQgDAAgDACgALig7QgEAKgKAQQgLARgEAJQAAADgFADQgDACAAADQAAAAAAABQAAAAAAABQABAAAAABQAAAAABAAQAVAKAgAUIAmAWIAGADIAFgGIABgBQANgXAYgiIACgFIgDgDIhig0IgDgBQgBABAAAAQgBAAAAABQAAAAgBABQAAAAAAABgANri8QgCAIgPAYIgMATIgTAfIgSAeQgGAMgHAGIgCADQAAABAGADIAWANQANAGAIAFIAeARIBVAsQAHAEAGAEIADABQAEgBACgFQAKgUAZgpIAGgLQAPgYAOgcQABgEAAgCIgDgCQgLgEgRgMIhcg2IgqgaIgCAAIgEgBIgFAEgAhog8QgGAHgPACIgPACQgBABABAAQAAABAAAAQAAABABAAQABAAABABIA9A2IADAEQAGAGAEACQAEAAABAFIAAABQABADAIAEQAIAEAEgBIABAAIAAgDQgBgFgGgGQgFgGgPgVIgRgbQgIgOgIgHIgDgFQAAAAAAgBQAAgBAAAAQAAgBAAAAQgBAAAAgBIgBAAIgDABgAgMhKIgmAGIgUADQgGABAAABIADAHIACACQAHAIAAAHIABABIACABQAJAJACAIQAAADAGAGIALANQAMARAEAKQAEAIAGgBQAIgCgBgKQgCgLACgQQACgKgCgWQgCgNACgMQAAgEgBgIIgBgCIgCgBgAlMgeIgHAHQgHAIgBADIgBACIAAABIAEAHQAIABANAGIANAGQARAGAHAHIABAAQAHAAALAGQAIAEAEABIABAAQAGgLAOgQIAGgHQAMgMAOgTIAQgWQAAgBgGgCIgEgEQgHgJgSgPIgSgQQgIgJgOgKgABDAfIAhADIAGgBIAlAAQARAAAVgCQAXgDASABQAHAAACgGIBriwQAEgKAGgGQAEgDAAgDQgBgDgGgDQgPgHghgVIiFhUIgFgEQAAgBgBAAQAAAAAAgBQgBAAAAAAQgBABAAAAQAAAAgBAAQAAAAgBAAQAAABAAAAQgBAAAAABQgHAPgUASIgLANIgKAMIgQATIgWAXIgCADIgJAIQgJANgRgCIgGAAIgJAAIAAACQACAIAAASIABAOIgBABQgBAHABAPQACATgDAKQgBAEABAJIADASQACAMgBAFQgEAMADAZQACARgBAIQAHABANgBIAbABgAEmgzIgaArQgIAQgJAJQgCADACAEQABADAFAAIALgCQAJgDAUgCQATgCAGgCIAIgDQAZgHALgGIASgJQAEgBgBgEQAAAAAAAAQAAAAgBgBQAAAAgBAAQgBgBgBAAIAAAAQgSgGgggPIgLgGIgVgKIgCgBIgBAAQgBABgBAAQAAAAgBABQAAAAAAABQgBAAAAAAgAGNAHIgDADQgLAHgFABQgDAAgFAFQAGACAHgBIAMgEIAIgDQADgBACgEQAAgDgBgCIgEgCQgCAAgEACgAHihhIgYAoQgUAfgMANQgFAEgDACQgBAAAAAAQAAABAAAAQgBABAAAAQAAABABABIADAIIAEAEQABABAEgCIAdgLIAmgSIBGgeIAEgCQAAAAAAgBQAAAAAAgBQgBAAgBgBQAAAAgBgBIgLgGIghgSQgUgKgLgIQgDgCgCAAIgBAAIgEAEgALnkUQgBAAAAABQgBAAAAAAQAAABgBAAQAAABAAAAIgSAhIhAB1QgQAegPAUQgIAMgLAHIgDADIACACQAUAKApAaIANAIQABABAAAAQABABAAAAQABAAABAAQAAAAABAAQAAAAABAAQAAAAAAgBQABAAAAAAQAAgBABgBIAnhCQAEgHAFgBIADAAQAFAAADACIAIAFQAKAFAGAFQADADAEgBQAEgBACgEIBKh6IABgCIAGgKQAAgBgKgGIhbg3QgOgIgGgFIgCgBIAAAAgArVnZQAGABACAFIAEAEQAPAKAVAXIAdAbQAMAKANAPIAIAGIAFAEQAJAJgBAHQgBAIgKAEQgEADgCAEQgHAMgWAWIgDADQgKAKgJAOQgKAQgOALQAAAAABABQAAAAAAABQAAAAAAABQgBABAAAAIABABIAZAYIAeAYIgMASIgQARQACADAdAOQAbAOAJAEQAHADARAKIAFADIAWAJIA1AXIBKAkIAUAJQAJAEAMAHQAJAFADgBQAEgCADgIIAAgBQA7hSAGgEIAAAAQAEgCAOgWIgBgCQgLgGgOgPIiDh3IgKgKQgCgCgRgMIgGgEQgOgLgMgPIgdgZQgGgEgQgPIgbgZQgNgOgKgFQgFgCgHgJIgiggIgBgCQgLgGgPgPIgQgOQgQgMgJgKQgRgUgXgRQAAAAgBAAQAAAAgBAAQAAAAgBABQgBAAAAABIgWAXIgJAJIgJAKIgBABQgLAOgIAIIgQARQgRAUgCABQgIAEgKAOIgLANQgNALgNAQQgFAGgEAIQgHAMgGAFQgEAEABACQADALALASIACAEQACADALACIAKADQAEAAACACQAEADADAAQADgBAFgFQAOgMANgRIBsh6QADgEAFAAIACAAgAFpijQgHANgOAXIgcAvIgDAEQgEAFAAADQABADAHACIBWAnQADACACgDIAJgLIASgaQAIgNASgYIAFgGQAEgFAAgDQgBgCgGgDIgogVIgygcIgEgCIgBAAQgBABgCAFgAIhjBIgEAEQgEAFgGAMIgSAcQgJAQgHAIIgCAEIAEAEIBGAoIADACQAJAFAFAAIADgBQAfgiAMgcIAEgFQADgDgCgDQgBgCgFgCIgigTQgfgRgQgMIgEgCIgBAAgAjMhsQgBAAAAABQAAAAABABQAAAAAAABQABABABAAIAJAIQASARAFAHQAEAGAIgCQAJgCAJAAIACAAQAEgCAIgBQAIgBAEgCIADgDIgCgDIgQgWQgEgEgIgPIgCgDQgCgEgJgLIgJgLQAAAAAAAAQAAgBgBAAQAAgBAAAAQgBAAAAgBIgFADIgFAIQgCAGgIAJQgHAHgCAFQgCADgEACIgBAAQgDACAAACgAiNiqIgHABIACAFIAuBEQAIAMACAEQAAABAFgBQAXgIAUgBQALgBAWgEIADAAQAFgBgBgGIABgNQABgIgBgJIAAgBQABgEgCgNQgDgQACgHQADgSgQgZIgWgdIgcgmIgDgFQgHgKgCgGQAAAAgBgBQAAAAAAgBQgBAAAAgBQgBAAgBgBQgFgDgBgCQgCgFgGgIIgDgEIgBgBQgBAIgJALIhABQQgHAHgBACQAAACAFAIIAJAQQADAHAEABQADABAIgCQAfgGATAGQAHADAEgCQAGgCAEAHIAAABQADAEgGAEIgBABQgLAIgUgFIgFgBIgFAAIgRABgAv+jCQgRAMgGAGIgKAMQgIANgFAEIAAAAQAAABAAAAQAAAAAAAAQAAABABAAQAAAAABAAQAMAGAUAOIAPAKQAQALAQAHIAHAFQAEADADgCQAAAAAAAAQABAAAAAAQAAAAAAgBQAAAAAAgBQAAgHAFgSQADgKABgIIAFgPQACgHgCgEQgBgDADgKIAAgBIg3gQIgCgBIgHgBIgCAAgAjHjIQgCAEgLAOIgeAkIgBADIAAACIACADQANAIADAKIACAAQABAAAAAAQABAAABgBQAAAAAAAAQABgBAAAAIAEgEQAMgIAOgYQAFgIAGgFQABgBAAAAQABgBAAAAQAAgBAAgBQAAAAAAgBQAAAAAAAAQAAgBAAAAQgBAAAAgBQAAAAgBgBQgLgIgDgNQAAgBAAAAQAAgBgBgBQAAAAAAAAQAAgBAAAAQgEABgCADgAGhkAIgeAxIgBACIgLARQgBABAAAAQAAABgBAAQAAABAAAAQAAABAAAAQAAABABAAQAAABAAAAQABABAAAAQABABABAAIAfASQAhATASAIIABABIAGAEQAEAEACgBQAFgDAEgHIADgFIANgWQAPgZAJgKIAAgEIAAgBIAAAAQAAgCgDgCIhXg0IgIgDIgBAAIgFAHgAJ2lSQgGAOgMASIgMAUIgcAwIgMATIgFAGQgEADABACQAAACADACIAfASQAXAOARAJIAGAEIACACQAHAEAEAAQADgBADgHQAFgMAagvIATghIARgeQADgEgFgDQgLgJgSgJIg1ggIgBAAIgBAAQgBAAAAABQAAAAgBAAQAAAAAAAAQAAABAAAAgAtnvqQgiAkgPATIgYAZQgIALgYAZIgoAuIgDACIACADQADAFAmAgQAtAmAEAGQADAFATASQATASAFADQAMAGA3A1ICrCYQA5A2AHAEIAyAtQAHADAzAyIABAAQALALA9A2IAHAGIAQAPQAOAPAIAGQAHAEAHAHIARAOIAJAJIADAEQAOARAcASIADAEQADAEACgBIABAAQADAAADgEQAHgNAdgiIAFgGQAFgGgEgGIg+hhIgLgPIgGgIQgYgigPgYIgigxIgbgoIgBgCIABAAQgEgKgUgWQgJgLgCgGIgPgZIgRgYQgMgRgDgCQgDgDgNgUQgNgVgDgHQgEgJgMgSIgPgXQgRgcgNgNQgFgEgBAAIgEAEIgGAIIgFAGQgEAGgKALIgJAJIgCAAQgEAAgTgWIgFgGQgHgGgDgIQgCgDgLgLIgKgMQgBgCgHgHIgJgKQgEgBgSghIgIgOIgZggIgNgRIgMgPIglghQgMgKgJgNQgHgJgOgLIgJgHQgIgEgEgFgAqimIIgBADQgHAMgHAEQgEADgGAJQgHAJgEACQgGAGgTAWQgMAPgKAJIgKAMQgLANgGAEIgEAFIgDAFQgIAJgMALIgIAIIgCADIADADQAGAEA4AaIArAUQAFADAKAEQALAFADgBIAGgIQAIgKADgGIAAgBQgIgDgHgKIgHgIQgGgEgGgHIgJgIQgLgKAIgKQAKgPAKgJQAFgFAKgOIAyg7QACgDgBgCQgGgKgJgFQgIgEgIgLQgDgEgIgDIgCgBIgDACgAgk0CIgWADIgOADQgKAEgGAAQgDABgFAGIgIAHIgNAMQgFADgGAKQgHAKgTAUIgdAcQgDAGABAFQACAEAEACQAIAEAGAJQAEAGAJAGIAKAIIBcBaQAHAGAAAFQAAAGgGAFIgMAMQgRASgNAKQgKAJgMAOQgKAMgGAFIgGAFQgIAEgBAEIAAACIgBACQACADAEAIIAEAIQAIAMABAYIAAACQAAALgBAMIgBAWQAAAJgCAcIgCAfQAAABAAAAQAAABAAABQAAAAABABQAAAAABAAQAJAGAXAVIAEAEQANANAdAXQAiAZAhAWQASALAOAEQAHADAKAGQAKAGAEABIACABIAIACQAFAAAcAUQAGAEARAHQARAHAHAGIAMAHIANAJQAFADAGABQAJADAEADIAGACQAYAJACAGQACAFgLAoQgJAjgGASQgFAQgHAfQgCAJgFAPIgDAFQgEAJABAGQABADgEAGQgEAFACAFQABAEAFADIAEAEQAEAEAHAEICLBZIAEABIACgCIAZgqQALgWAKgMIACgEIgCgBIgYgOQgWgOgJgEIgJgFIgDgDQgHgFADgGQAAAAABgBQAAAAAAgBQABAAAAAAQABgBABAAIACAAQADAAACABIAWANIARAKQARAJAIAGQAAABAAAAQABAAAAABQABAAAAAAQABAAABgBQACAAADgEIAng/QALgUAXgkQAEgGgCgDQAAgDgHgDQgIgEgPgJIgQgIQgUgMgFgFQgGgFADgGQAEgIAJAFIAxAdIAYAOIAQALIALAGQANAJAjAUIADABIAaARQAGAEABgBQADAAACgGIAQgeIAOgZQATgrALgqIALgjIARg+IAIgdQAHgZACgZQACgSAOgUQACgEAEgOIAehbQADgKAEgHQACgCgEgEQgBAAAAgBQgBAAAAAAQgBAAAAAAQgBAAAAAAQgEAEgLABIgGAAIgOAEQgeAGgOgBQgLgBgOgNIgDgDQgHgHgCgSIgEgWQgHg3gIgiIgDgQQgEgYgGgNIgCgFQgDgJgEgEQgEgDgCgFQgBgFgDgBQgIgCgJgMIgGgGQgGgGgHgOIgKgSQgEgLgEgEQAAAAgBgBQAAAAAAgBQgBAAAAgBQAAAAAAgBQABgFgDgKQgDgLABgGQABgEgDgDQgDgCgDABQgMACgVgEQgGgBgQADIgPACQgQADgGgCIgDABIAAAEIAFAdQACAHgEAEQgDACgIgCIgDAAQgFAEgNABIgGABIgCAAIABACQAGALgIAJQgCACABADQADAIgCAQIgBAJQAAAFgHADIgIAEQgMAIgJgBQgCAAgFAFIgGAEIgQAJQgVALgJADQgIAEgNAIQgNAIgJAEIgEABQgMAGgJAAQgIAAgMAEQgJACgGgFQgFgEACgHQABgLgDgRIgDgUIABgJIABgKIgHhNIAAgDQgEgrADgWIgBgMIgBgGQgCgIAHgFQAEgEAGACQAFABAGgCQAJgDASAAIARgBIABAAIAEgBQAIgDAJAAQAGABADgEQADgDgDgGQgEgJACgHQABgDgCgCQgCgDgGABIhCAFIgHAAQgHgCgJAAIgRADIgIABIgCABIgEAAQgHAAgBACQgBADACAGIABAFQAEAXgBAXIgBABQgBALACAGQADAGgBAKIAAAGIACAZQADAcgCAOQgCANAEAZQAFAggDAQQgBAGACALIACALQAAAOgKAFQgLAGgHABQgKACgKgFQgQgJgEgPQgDgLgBgWIgBgUQgBgJABgLIAAgRIgDgbQgFgrACgXQABgVgFgeQgBgGABgEIAAgKIgDgcIgCABIgCAAQgEAAgFACQgKAEgEgBIgEAAQgHgBgDABQgGADACAEQADAIgBAOIAAASQABAPgBAHQgBAMACAHQACAIgDAFQgBABAAABQAAAAAAABQAAAAAAABQAAABABAAQADAJgBASIAAACQgBAJgIABQgHAAgDgIQgGgNAEgRQABgHgBgOIAAgTIgBgLQgCgNAEgIQABgCgCgHQgFgKACgJQABgFgFgDQgBAAAAAAQgBAAAAAAQgBABAAAAQgBAAAAAAIgDACQgIADgPAAIgCAAQgJAAgRADgAhflhQACAAAHALQAAABABAAQAAABAAAAQAAABABAAQAAAAAAABIACACQAJAGAJAQIAEAIIAUAbIAMAPQAJANAGAGQADAEAGAKQAFAMAFAEQADADADAHQACAGACACQADACAGABQAGABADgCQALgFAOgPIAtgzQAGgGAPgTIAJgMQAAAAAAAAQAAgBAAAAQAAAAAAgBQABAAAAAAIADgDIAEgDIAJgOIAVANQAAAAAAABQABAAAAAAQABAAAAAAQABAAAAgBQAEgBABgGIAfhnIABgGQAKgdABgSIAEgSQAAgDgPgIIgagOQgQgHgGgFQgLgHgXgKQgWgLgHgEIgugaQgPgKgMgFQgPgFgTgNIgIgEIgmgbQgPgKgIgIQgDgDgHgFQgJgGgDgEQgFgGgNgKIgRgPIgDgCQgDgDgCgDQgLgIgBgFIgBgbQAGgegBgSIABgSIABgCQADgTgFgOIAAgBIAAgBQABgKgLgRIgEgFIgCACQgHALgHABQgHABgDgFIhdhgIgIgHQgEgFgGgEQgOgLgFgIIgCgCIgBACQgEAGgHAEQgFADgDAIQgBAFgRAOQgCAFgLAJQgIAEgDAFIghAnQgHAKgJAHIgHAJIgPAOIgJAJIg7BCIgLAJQgHAFgJAOIgFAHIA/BfIAVAeIAFAIIAOAXQAKAJALAVIAJALQAHAKACAEIAOAWQAFAEAKAQIACAFIAWAgIAQAZIAYAjIAKAQIAVAeIAAABQAMAQADAGQAEAJAPAVIANASIAhAzIAKAOIACACIAFgDIAlgxIALgOIAQgVIAMgRQAIgJAGgEIAEgBgAwQs0IgCACIhvB9QgRAUgLAKIgIAKQAAABAGAGIAEADQAKAHAfAbIBQBIQANAMAHAIQAEAGgBADQAAAEgFADIgGAHQgDAGgFADIgDADQgGADgDAEQgEAHgLALIgMALQgMAPgUATIgWAVIgPAPIgMAMQgLALgFAJIgHAIIARALIAoAZIA6AiIAOAKIAOAKIA6AiQAKAEAQAEIAKACQAHACACgBQABgBACgGIABgEQgBgDABgDQADgJABgTQABgSADgIQADgMgDgOIgHgZQgFgOgCgMIADgBIgDAAQgGgKAGgLIAGgLQALgUAIgIQAGgGALgOIAQgSQAXgXAXgaIAYgaIAbgdQAGgIANgLQAEgEgBgGQAAgBAAAAQgBgBAAAAQAAAAgBAAQAAgBAAAAQgGAAgbgcIgFgFQgRgRgTgOQgDgBgJgKQglgkgKgFQgHgEgEgGQgGgIgKgJIhNhGQgPgNgHgIIgDgCIAAAAgAM5mmQgCAAgEAHIgtBSQgLARgFANIgDADQgGAHAAACQABACAJAFIAoAYIA2AhQAMAGAGAGIACABIABAAIACgCIAEgEIAVgkIAMgUIAqhDIAAgCIgCgCIgwgdIgrgZQgRgJgLgJQgGgEgCAAgAH/mbQgCAAgCADIgrBLQgQAXgHARIgGAHIgDACQgBABAAABQAAABgBAAQAAABAAAAQAAABAAAAQAAAAAAABQAAAAABABQAAAAABAAQAAABABAAIAmAWIAiAVIASAKIACACQABAAABABQAAAAABAAQAAABABAAQAAAAABgBIAAAAIABAAQABAAAAAAQABAAABgBQAAAAAAgBQABAAAAgBQAGgMAQgbIAwhQQAFgIgBgCQAAgCgIgFIgBAAIhEgqIgSgKIgCAAIgBAAgAuOkaIgBAKIAAABIAFAEQALAIAIACIABABQAMADAFACQAKAFAFAHQACACAFAAQAHABAEgDIBThcQAWgaAKgJIAQgSQANgQAJgHIAAgBIgEgGIgHgIIgdgYQgBgBgEAAQgEABgBADQgHAMgWAXIgQASIgZAcQgLALgMAPQgWAagOALQgIAHgFAAQgGABgKgGIgQgHIgBAAQgEAEACASgALHoHQgDACgBAFIgOAvQgDAQgMAWIgIARQgMAcgRAWQgBABAAAAQgBABAAAAQAAABAAAAQAAABAAAAQAAAAAAABQAAAAABABQAAAAABAAQAAABABAAQAeAUApAWIAKAIIABAAIACABQAFgCACgFQACgGAHgLIAHgLQAKgWAbgvIAxhWQADgEAEgDQAAAAABgBQAAAAAAgBQAAAAAAgBQAAAAAAgBQgBAAAAgBQAAAAgBAAQAAgBgBAAQgBAAAAAAQgPABgNgBIgNAAQgPAAgJgCIgQgDQgYgEgPgEIgGgBIgCAAgAy1p9IgdAgIgeAgIgCADIgBABQAaATBNBDIAPAMIAWATQAFAFADAAQAEAAAFgFIAzg1QAFgEAAgDQAAgCgEgDQgMgJgVgUIhehSQgGgFgEgFQgDgDgBAAIgBAAQgCAAgDAEgANJoIQAIAEAFgJIAzhaIAEgFQADgDABgDQAAgFgDgBQgQgHgfgUIgTgLQgfgTgggRQgIgEgHAAIgBAAQgKACgBAJQgHArgJAlQgLAugNAhQgBADABACQABABAEABIAXAFQAhAHANAAIANABQASACAIgCIAHgBQAEAAADABgANUttIgoAKQgEABAAAFQABAHgFAIIgDAEQABALgJATQgEAIgCAIQgFASgNAaQgCAFAAAKIAAAFIACAAIAHABQAAAAABAAQABAAAAAAQABAAAAAAQABAAAAAAIADgFIAIgNIAGgIIAohAQATgdAHgOIAAgBQACgEACgBIABAAIADgCQgCgFgEAAIgFAAIgEAAIgEAAgAobuWIgkAmIgUAVQgRATgNAMIgRARIgMAOIgDAFIACACQACAEADABIAGAHIAJALQAKALADAIIABADQAAAAABAAQAAAAABAAQAAAAABgBQAAAAABgBIAOgOIAOgPIAcgeIAagdQAUgVAJgMQADgDgCgEIgFgIQgJgOgDgHIgGgHQgGgGgBgDQgBAAAAAAQgBAAAAAAQgBABAAAAQgBAAAAABgAOdtAQgCAIgIAOIgIAMQgFAHgBAEQAAADgFAFIgDAEIgBABQgFAHABACIACACQAGAJAEACIABAAQAJgGADgIIALgVIAGgPQAGgQAGgJIABgCQgHgHgGgBIAAAAQgFAAAAAFgANyteIAAAAQgDAMgJAMQgHAKgGANQgBADgGAGQgFAGgBADQgBAFADACIAYAQIACABIABgBQAAgJAIgJIABgCQAPgVAHgVQACgDAEgDQAAAAABAAQAAgBAAAAQABgBAAAAQAAgBAAgBQAAAAAAgBQAAAAAAgBQAAAAAAAAQgBAAAAgBIgIgFQgNgJgGgBIgBAAgAnlwTQgaAegrAtIgsAsQgQAPgYAbIgFAFQgLAJgTAVIgKAKIAAAEQAAAFAFAIQAFAHACABQACABAEgEICRiVIAzg4IASgRQAFgFAKgOIABgDQABAAAAgBQAAAAAAgBQABAAgBgBQAAAAAAgBIgBAAQgFgGgDAAIgFABIgBAAQgXAIgNARgAx/vMIgDAEQgFAHAAABQAAACAGAFIABABQAIAGAPAOIBJBBIAQAQQADADAEAAQAFgCAGgMQABgCgGgEIgBgBQgIgEgMgMIgLgMIgrglIgBgBIgLgKQgSgPgLgMQgBgBAAAAQgBgBAAAAQgBgBAAAAQgBAAAAAAQgCAAgCADgAqTvvQgFAJgQAQIgHAKQgGAIgFAEQgHAHgWAaQgLANgBAAQgDADAAACQAAABAAAAQAAABAAAAQABABAAAAQAAABABAAQAHAEAIANIAJANQAKALAEAJQACADACAAQADABAEgEQAeghARgQQAVgUAMgPQAFgGAIgGIAEgDQAIgHANgPIAIgKIAWgVIARgSQAKgKAOgTQAMgSANgDQACgBADgDQAKgJALAAQADgBABgCQAAAAAAAAQAAgBAAAAQAAgBAAAAQAAgBgBgBIAAgBQAAAAgBAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQABgGgGgKQgGgLAAgGIgBABQgEAFgLAGQgJAFgDADQgFAFgLgHQgWgQgogZQgNgIgZgTIgPgLIgHgEIgDgCIgBAAQgSgJgIgIQgHgHgHADQgBAAgBABQAAAAgBABQAAAAAAABQAAAAAAAAIACADQALAEANANIAGAEIAGAHQAGAHAEACQAKAEAPAOIAOALIAJAJQAEAGAEADQADACABAFQAAAGgCAGQgDAJgEACIgPAOIgGAIIgcAgQgCADgDABQgGAEACAFQAFAIAIAEQAJADgCAKQAAAHgIAAIgCABQgEABgDgEIgGgEIgDgEQgKgJgCAAQgCABgHAMgAKruJIAAAFIABAFIgBAEQgBAHADADQANATARACQAHABADgEQACgDADAAQAGABANgFQASgGAIABQAEABAIgFQABAAAAAAQABgBAAAAQABgBAAAAQAAgBAAAAQAAAAAAgBQAAAAAAgBQAAAAgBgBQAAAAAAgBIgCgBIABgCQACgFgEgKIgEgKQAAAAAAgBQAAAAAAAAQAAgBgBAAQAAAAAAgBQgBAAAAAAQAAAAgBAAQAAAAAAAAQgBAAAAAAQgXADgYABIgCABQgMAAgQAFIgIACIgGgBIgDAAIgBABgAxkvnQgFAHgDAGIADAEQARARAKAIQAdAWAEAGQADAFAJAHIAZAWQAQAOABADIACAAQAEgBAGgGQAHgGAAgEIgBgCIgIgIIgYgWIgOgOIgTgRIgpgjQgIgIgFgGQgCABgGAHgAmvwLIAAABQgBAHgIAFIhNBSIgEAFIgBACQAGABAFAKIAEAHQAJAJAGAOQADAFADAAQACAAADgDQAEgHAHgHIAHgIQAOgRANgLIALgNQAIgKAGgFQAGgFgCgHIgMgfIgCgJQgBgJgEgEIgEgEgAN2unQgZACgrAHIgLACQgEAAgCACQgBACABAFQAAAEADAEQAEAMgCAGIACABIANAAIAZgIQAGgCAHAAQAJAAAHADIAEACIAFgEQAEgEAGgMIAEgIQADgEACgCIADgBIgDgCQgGgFgIAAIgDAAgAxOv8QAIALAIAEQAEADAEAEQAFAHAKAHIALAJIA4A0IAHAHQAEAFAFgBQAFAAACgEQABgEAHgIIAFgFQAEgEgDgEIgRgQQgDgFgMgKIgFgEIgigfQgSgTgQgNIgNgKgAklwgQgDAAgDAGIgCACQgEAJAAACQABADAHAGQAeAaAaAbIALALQASASAFAHQAEAGAJAHIANAMIAFgCIADgDQAFgHAGgEQAAAAAAAAQABgBAAAAQAAAAAAAAQAAgBAAAAQAAAAAAgBQAAAAAAAAQgBgBAAAAQAAAAgBAAQgGgEgJgKIgWgWQgLgMgJgHIgKgLIgHgIIgKgJQgKgFgEgHQgFgGgMgKIgHgHQgFgEgCAAIgBAAgAsCxdIgKANQgKANgGAFIgMAOIgvA1IgBABIACACQAMAIAUAVQARARAKAHQAJAHAQARIAMAMIAFADQACAAABgEIAHgKQAJgLAJgHQADgBAEgGIAJgMIATgYIAWgbIAGgEQABAAAAgBQABAAAAAAQABAAAAgBQAAAAAAgBQgBgDgHgEIgKgIQgOgKgFgJQgCgEgJgFQgGgEgEgEIgNgLQgMgLgDgEQgJgKgJgEgAM+veQgJAEgcAHIgDABIAAAEQAAAEADAIQAEAMgBAHIACAAIALgBQAcgDARgEQAMgEAZgBIACAAQAFAAAFADQAIAGAJgFQgFgDgDABQgBABgFgDIglgWIgEgFIgDgDQgKgGgKAAQgGAAgGACgAkKw8QgKAJACAIQAAABAAAAQAAAAABABQAAAAAAAAQABAAABAAIAEADQAHAHAUASIAcAbIASASIAPAOQATASAFAJQABADAKACIAAAAQADgCAEgGIAGgIIgBgBQgGAAgDgGQgGgIgNgMQgJgIgGgHIgQgQIgTgUQgGgJgRgOQgMgLgIgJQgEgFgDAAQgDAAgDAEgAuHwJIgHAJIgUAXQgLAPgPAOQgBABAAAAQAAAAgBABQAAAAAAABQAAAAAAABIAIAJIAHAIIABAAQAFgIALgLQAHgGAHgIQAGgKAQgQIAEgDIACgFQAAgCgDgCQgHgEgEgJIgBgBIgEADgAjTx3QgBADgNAPIgSASIgCACIACADQAHAJANALQAOANAFAGQAJAMAMAKQAPAOAHAJQADAEAEADIAKAKQABADAHAHQAHAGADAEQADAFAEAAQADABADgFQAEgFAHgDIAIgIIAOgQQAFgGADgCQAFgCAAgDIgDgDQgHgDgLgMIg+g9QgRgQgIgJIgLgKIgLgKIgCgCQgKAAgCAIgAu5wcIgWAaIgCACQgUATAEAGQAIAKAJAGQAAAAABABQAAAAABAAQAAAAABAAQAAAAABAAIACAAIABgCIACgCIAIgJQAHgKAIgHQAEgCADgHIAHgJIADgDQAEgFAEgCQADgCABgDQAAgDgEgEIgTgRIgBgBgAmkxpQgHABgBADIgFAEIgCACIADALQADAMAEAGQAJAMAAAOQAAAJAEADQADACACAEQABAEAAADQgBAEAFAJIACADIAFAPIADALQAAABABABQAAAAAAABQAAAAABAAQAAAAAAAAIADAAQABAAAAAAQABAAAAAAQABAAAAgBQAAAAABgBQAFgKAOgMIANgQQAFgGALgJQAMgKAEgHIADgEIAGgKQgBgCgGgCQgEgCgFgGQgFgGgCAAQgFAAgDAFQgFAHgIABQgIABgJgHIgGgFIgQgOIgIgGQgLgGgCgIgABz0NQgFACABAGQAEAQAAAiIAAAMQAAAIACAGIABAKQABAMgBALQAAAWABAHQACAKABAUIABAQQADAaAAAUQAAAVAFATQADAIAIgCIABAAIAMgDQABgBgCgMIABgKIABgEQAAgHgCgNIgBgSQAAgJgCgQQgCgWABgLQABgJgCgNIgBgPIgDg2IgBgFQgBgVACgNQABgFgDgKQgDgIAAgFIAAgBQgCgCgDAAgAvMxMQgCALgzA0QgBABAAABQgBAAAAABQAAAAAAAAQAAABAAAAIAAABIACACIABAAQAIAHAEAHIABACIACgCQAFgLAPgMIACgBQAEgDAFgIIAEgFIAUgXQABgBAAgBQABAAAAgBQAAAAAAgBQAAAAAAgBQAAAAAAAAQAAgBgBAAQAAAAAAgBQgBAAAAgBQgLgIgDgFIgDgCgAEozgQgPADgeABIgUABQgMABgCAHIAAACQACAHAAATQAAAPABAEQADAVgCAMQgBADACAGQACAGAAAFQgBAUAEAdIgBAQIAAAFQgBAGADAKQAEAPgDAJIACAAQAFACAIgCIAHgBQAZgFAWgRIAGgCIABgBQAHgFABgHQABgHgDgWIgCgUIgDg8IgDgnIgCgVIAAgIIAAgDIgBgFIgCgBgAstxtQgZAegQAOQgDADgFAIQgGAJgFAEIgKAIIgBABIgCADIABABQAAACAGAHIAIAIQAAAAABAAQAAAAABAAQAAAAAAgBQABAAAAgBIAQgRIAOgOIA0g7IAIgJIgIgJIgKgIgAvuxsQgIAEgEAGQgJARgdAcIgIAKIAUATIACABIAcgeIARgSQACgBAEgHQAEgIADgCQAAAAAAgBQAAAAAAgBQAAAAAAgBQgBAAAAgBQgDgEgPgLIgBgBgApnxjQgkAogHADIgFAHIgFAHQABAFADAAQAFACAHAGIABACQADADACAAQACAAACgGIAFgHQAIgFAIgNQAJgMAFgEQALgKAFgIQADgFAAgCQAAgCgEgDIgDgEQgFgEgDAAIgEAAgAtAydIgIAJIgQASIgPAQQgOARgKAGIgaAgQAAADAJAHIAHAIIACABIABAAQAEgCADgFIAEgFQAZgaAMgOIAeghIADgFQAIgKAFgCIABgCIgDgCIgQgMIgDgBQAAAAgBABQAAAAAAAAQgBAAAAABQgBAAAAAAgAFIxxIgBAEIAAALQAAALACAEIAAAAQACAJAAAJIABANIACAIIAHgBQAMgBABgHQADgUgIgZIAAgBQgDgIABgFQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAAAAAAAIgCgBIgCAAQgDACgEgBIgCAAQgEAAgCACgAqIyQIgGAIQgQAVgMAKIgOANQgKALgCADIAEAFQAHADAHAIIADACQAAABABABQAAAAABABQAAAAABAAQAAAAABAAQACAAADgEIAYgaQAdggAHgFQgLgIgNgNIgDgBIgDACgAFtxzQgBAAAAABQAAAAAAABQABAAAAABQAAABABAAQAEAFgBAJIAAAGQAGAQgCAJQAAABAAAAQAAAAABABQAAAAABAAQABAAABAAQAEAAAGgFQAGgGAAgDIAAgDQAAgXADgLQABgDgFgEIgDgCIgBAAIgDACQgFAFgIgCIgBAAQgCAAgEAEgAj5ynQgDAIgMALQgIAHgDAEQgGAJgKAIQgMAMgEAFQgEAGgHADIgBABQgBADADADIAOANQAFAGAEgBQADgBACgDQAEgJAPgNQAMgKAFgHQACgDAHgHQAHgHADgGQACgEAFgDIABgBQABAAAAAAQABgBAAAAQAAgBAAAAQABgBAAAAQAAgBAAAAQgBgBAAAAQAAgBAAAAQgBgBAAAAIgQgNIgGgEIgCgBgAtiy6QgRAWgNAMIgSATIgoArIAAAAIALAKIAJAIQAAAAABAAQAAAAAAgBQABAAAAAAQAAgBABAAQAFgLATgUQAGgFAHgJIATgUQAVgTAJgPIACgDIgDgBQgGgDgFgHIgDgEQgCAAgEAFgAmIx7QgFABgBAFQgBADAFADIAEACQAOAKAKAKQAGAGAFgCIADAAQAAAAABAAQABgBAAAAQABAAAAAAQAAgBAAAAQABgFgFgDQgIgDgHgJIgNgOQgEgCgEAAIgDAAgAq2yxQgEAJgPAOIgHAGIgMAPIgJAKIgIAIQACADAOAMIALAKIAJgIIAEgFQALgIAKgOIAOgRQAHgJAFgDIACgCQAAgBgBAAQAAAAAAgBQAAAAgBgBQAAAAgBgBIgJgHQgHgIgHgCIgEgBIgBAAIgBAAIgCABgAuJzeIgVAYQgPAPgNARQgVAZgNANQgDACAAACIAFAEQAIAGAFAIQADAEACAAIABgCIACgCIAMgPIAXgYIAIgHQAXgWAGgIIABgCIAMgSIACgDQgCgIgGgBQgFgCgEgFQgEgEgCAAQgCAAgCADgAqazbQAFAAAKAHIAcASQATAMAPALIAVANIAdAUQAWARAMAFIAEADIABABQAGAGAHgFIADgDIAFgFQAGgCACgFQABgDgCgJIAAgBIgMgcQgHgPgDgJQgDgHgCgPQgBgLgCgFQgFgRAFgfIABgJQAAgDgBgCQgDgEgIgBQgIAAgPgEIgOgEIgPgFQgNgFgHAAIgBAAQgEABgGgEIgFgCQgJAAgXgGQgYgFgJAAIgFgCIgCgBIgGgBQgBABACAGQADAHgEANIgCAKQAAAGgFACQgFADgGgEQgUgOgfAIIgCAAIgFACQgEAEgHADQgJAEgDAEIgCAEIADADQAWAPANALQAIAHARAMIAXASQAGADAGgFQAIgIAHAAIABAAgArXzOQgJADgEAIQgFANgTAQIgRAVQgBAEARANIADgDIAOgOQAGgEAGgJQAKgLAFgDIAIgJIAIgJQgIgMgJgEIgCAAIgDAAgAFIypIgBABIgBAHQgDANAEAIQABAEAEAAIAVgCIAJABQAOgEAbgCIAXgCQAFgBABgCQACgDgCgEQgCgHADgJQAAgBAAAAQAAgBAAgBQgBAAAAAAQAAgBAAAAQgBAAgBAAQgBAAAAAAQgBAAAAAAQgBAAAAAAQgEAGgLgBIgRAAQgMACgHgBQgHgCgNACQgQADgKgDIgCAAIAAAAgAr7zoQgGAMgOALQgLAIgLAQIgDAEQgGAFAAACIAAACIAHAFIACACQAGAEACAAQACAAAFgFQAbgdALgPIAFgFQAFgEAAgDQAAgBgFgCIgIgFIgBgBIgDgCIgCgDgAsoz6IgCADIgBABQgSAXgOALQgDADAAACQAAADADADIACACQAHAGAAAFIADACQAAAAABAAQAAAAABgBQAAAAAAAAQABgBAAAAQAFgKAOgNIAPgOQAJgJAIgKIABgDQgBgCgIgGQgJgFgDAAQgEABgHAJgAE+0aIAEAzIACAdIAAAFQgCAHABACQABABAIgBIA3gDIARgBIANgBIABAAIAFABIABAAQAEABACgCIAAgBQACgEgDgDQgFgGACgSIAAgEQACgKgDgMIgEgZQgCgJgIAAIgGgBQgFgBgFACIgOABIgdAAIgPACQgLACgHgDIgBgBgAHB0fIgJABQgCABABAFIAGAoIABAKIAAALIACAGQAEAFgEAGIAAABQgCADABADQABACAFAAQAPACALgDIABAAQAVgDAIAAIANABIABgBIAEgBIAEgBQAJgBAEAEQAAABABAAQAAAAAAAAQABAAAAAAQABAAAAgBQAEgBABgEQABgIgDgDQgDgCACgEQACgLgDgWIgDgUQgCgNACgIIgBgDIgEAAQgIAEgOAAIgMABQgDAAgFgCIgGgBQgMAAgfAGgAp02WQABAXgGATQgCAIABAIQABADAEACQAEACAEAAQAPABAbAIIAWAGQALABAIAEIASAFQAPADAHADQABAAABAAQAAABABgBQAAAAABAAQAAgBABgBIAFgJQAIgMAAgHQABgEAEgHQAEgGAAgDQABgPAFgKIAAgDIgCgBIAAAAQgHAAgNgDIgPgCQgNAAgegFIhEgKQgFAAgFgDIgCgBQgDADAAAEgAsw2yIgJABIAAAEIAAACIgCAQIgEAhIAAAEIAFABIABAAIAbADQAnADASAEIAfAEIAhAGIAIACQAJADAEAAQABAAAAAAQABAAAAgBQABAAABAAQAAgBABAAIAAgCQgEgIAEgOIACgKQAAgPAEgSIABgHIABgGQABgKACgFQADgGgFgFIgBgBIgDACIgDACQgJAEgGgBQgDgCgDACQgKAIgYADQgPACgFACQgFACgNgBQgKgCgFACQgIADgMgBIgNABQgFABgIgEIgGAAIgHAAgAm14BQgIAEgPADIgQADIgRAEQgIADgGAFIgEAGQgBAPgKAbIgEAJIgBAEQgCAHgDAEIAHABIAGACQARACATAFIAJABIAKABIAGABIAAgHQAAgGACgCQAFgGgBgGQgBgEAEgEIACgDIABgIQACgRAEgKQACgFABgKIADgNIABgEIAAAAIgBgEgAoc3kQgTAEgGACQgLAEgSAEIgIACQgKADgDACQgJADACAJQACAGgBAHIgBAEQAAAAABABQAAAAABAAQABAAAAABQABAAABAAIAQADQAQADALAEQAGABAPgBIAFAAIABAAIAAAAIAAgEQgDgEACgEQAHgKAHgdQABgEAGgIIAAgBIAAAAgAr03EIACAAQAkABAZgKIANgEIAfgLIAGgCQAHAAABgBQABgBgCgGQgBgGADgMIACgKIAAgEQAAgOAFgFQAAAAgBAAQAAgBAAAAQAAAAgBAAQAAgBAAAAIgHgDQgBAAAAABQAAAAgBAAQAAAAAAAAQAAAAAAAAQgFAIgMgBQgDAAgJADIgLAEQghAFgOAJIgEABQgIABgSAGQgRAGgJABQgJAAgQAGIgQAEQAAAAAAABQgBAAAAAAQgBABAAAAQAAABgBABQgBADABADQADAEAAAIIgBAHQgBABAAABQAAABAAAAQABABAAAAQAAAAABAAIABABIAJACIAXgBIAhABgAme5aIh5AjQgXAGgKAEQgJAEgQACQgGAAgJAEIgEADIACADQACABAAAFIgCAPIgFAcIAAAGQABABAFAAQAKgFAPgDIASgFQAGgDAMgCIADAAIASgFQAPgFAMgCIAMgDQAIgFAHAAQAHAAANgFQAMgEAHgBQAIAAACgJIAEgLQAEgJABgEIAAgDQABgUAHgKIgBgBQAAgBgBAAQAAgBgBAAQAAAAgBAAQAAgBgBAAIgCABg");
	this.shape.setTransform(-0.0165,235.2815,1.4323,1.4323);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.parcelle_contours, new cjs.Rectangle(-184,0,368,470.6), null);


(lib.masqueTexte_1_ligne = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("EgicAEJIAAoRMBE5AAAIAAIRg");
	this.shape.setTransform(0,24.55);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.masqueTexte_1_ligne, new cjs.Rectangle(-220.5,-1.9,441,53), null);


(lib.masque_generique = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#DEA60E").s().p("AsOCqIAAlTIYdAAIAAFTg");
	this.shape.setTransform(78.325,17);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.masque_generique, new cjs.Rectangle(0,0,156.7,34), null);


(lib.ligne = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(49));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#333333").ss(1,1,1).p("EAAAg/bMAAAB+3");
	this.shape.setTransform(0,406);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#333333").ss(1,1,1).p("EAA/A/cQhDv0ggwoQgdwxAExFQAEwXAhwHQAfufA4to");
	this.shape_1.setTransform(-6.0239,406);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#333333").ss(1,1,1).p("EABwA/cQh4vfg5w8Qg0wqAHxQQAHweA7wDQA5uxBjtQ");
	this.shape_2.setTransform(-10.6719,406);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#333333").ss(1,1,1).p("EACVA/cQigvPhLxLQhHwmAKxYQAJwjBPv/QBMvACEs9");
	this.shape_3.setTransform(-14.2371,406);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#333333").ss(1,1,1).p("EACwA/cQi+vDhZxWQhTwjAMxeQAKwnBev8QBavKCcsw");
	this.shape_4.setTransform(-16.895,406);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#333333").ss(1,1,1).p("EADEA/cQjTu7hjxdQhdwhANxhQAMwrBov6QBkvSCusm");
	this.shape_5.setTransform(-18.7801,406);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#333333").ss(1,1,1).p("EADSA/cQjiu1hqxjQhjwfAOxlQAMwsBvv5QBsvXC6sf");
	this.shape_6.setTransform(-20.0814,406);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#333333").ss(1,1,1).p("EADbA/cQjsuxhuxnQhnweAOxnQANwtB0v4QBwvaDCsb");
	this.shape_7.setTransform(-20.9145,406);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#333333").ss(1,1,1).p("EADgA/cQjyuuhxxpQhpwdAPxpQANwuB3v3QByvdDHsY");
	this.shape_8.setTransform(-21.4184,406);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#333333").ss(1,1,1).p("EADjA/cQj1uthyxqQhrwdAPxpQANwvB5v3QB0vdDJsX");
	this.shape_9.setTransform(-21.7172,406);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#333333").ss(1,1,1).p("EADkA/cQj2uthzxqQhswdAPxpQANwvB6v3QB1veDKsW");
	this.shape_10.setTransform(-21.8418,406);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#333333").ss(1,1,1).p("EADlA/cQj3ush0xrQhrwdAPxpQANwvB5v3QB2veDLsW");
	this.shape_11.setTransform(-21.8918,406);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#333333").ss(1,1,1).p("EADlA/cQj3ushzxrQhswdAPxpQANwvB6v3QB1veDLsW");
	this.shape_12.setTransform(-21.9165,406);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#333333").ss(1,1,1).p("EADlg/bQjLMWh1PeQh6P3gNQvQgPRpBsQdQBzRrD3Os");
	this.shape_13.setTransform(-21.9165,406);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#333333").ss(1,1,1).p("EADlA/cQj3ushzxrQhswdAPxpQANwvB5v3QB2veDLsW");
	this.shape_14.setTransform(-21.8915,406);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#333333").ss(1,1,1).p("EADlA/cQj3ushzxrQhswdAPxpQAOwvB5v3QB1veDLsW");
	this.shape_15.setTransform(-21.8665,406);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#333333").ss(1,1,1).p("EADkA/cQj2uthzxqQhrwdAPxpQANwvB5v3QB1veDKsW");
	this.shape_16.setTransform(-21.7865,406);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#333333").ss(1,1,1).p("EADjA/cQj0uthzxqQhqwdAOxpQAOwvB4v2QB0veDJsX");
	this.shape_17.setTransform(-21.6619,406);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#333333").ss(1,1,1).p("EADhA/cQjyuuhyxpQhpwdAOxpQANwuB4v3QBzvdDHsY");
	this.shape_18.setTransform(-21.4626,406);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#333333").ss(1,1,1).p("EADeA/cQjvuvhwxoQhoweAOxoQANwuB2v3QBxvcDFsZ");
	this.shape_19.setTransform(-21.2133,406);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#333333").ss(1,1,1).p("EADaA/cQjruxhuxnQhnweAPxmQAMwuB0v4QBwvaDBsb");
	this.shape_20.setTransform(-20.8645,406);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#333333").ss(1,1,1).p("EADWA/cQjnuzhrxlQhlweAOxmQANwsBxv5QBtvZC+sd");
	this.shape_21.setTransform(-20.4106,406);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#333333").ss(1,1,1).p("EADQA/cQjgu2hpxiQhiwfAOxlQAMwsBuv4QBrvXC4sg");
	this.shape_22.setTransform(-19.8817,406);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#333333").ss(1,1,1).p("EADJA/cQjZu4hlxgQhewgANxjQAMwrBqv6QBnvTCysk");
	this.shape_23.setTransform(-19.1786,406);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#333333").ss(1,1,1).p("EADAA/cQjPu8hhxcQhawhAMxhQAMwqBlv7QBjvQCqso");
	this.shape_24.setTransform(-18.3758,406);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#333333").ss(1,1,1).p("EAC2A/cQjEvAhcxZQhWwiAMxfQALwoBhv8QBdvMChst");
	this.shape_25.setTransform(-17.4235,406);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#333333").ss(1,1,1).p("EACqA/cQi3vFhWxUQhRwjAMxdQAKwnBav8QBXvICXsz");
	this.shape_26.setTransform(-16.2969,406);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#333333").ss(1,1,1).p("EACdA/cQipvLhPxPQhKwkAKxaQAKwlBTv+QBQvDCLs5");
	this.shape_27.setTransform(-15.0095,406);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#333333").ss(1,1,1).p("EACOA/cQiYvShIxIQhCwnAJxWQAIwiBLwAQBJu9B9tB");
	this.shape_28.setTransform(-13.5536,406);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#333333").ss(1,1,1).p("EAB8A/cQiFvZg/xCQg6woAIxSQAHwhBCwBQA/u2ButK");
	this.shape_29.setTransform(-11.8731,406);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#333333").ss(1,1,1).p("EABpA/cQhxvig1w5QgxwrAHxPQAGwdA3wDQA2uvBdtT");
	this.shape_30.setTransform(-10.0434,406);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#333333").ss(1,1,1).p("EABYA/cQhevpgswyQgqwuAGxKQAFwbAvwFQAsuoBOtc");
	this.shape_31.setTransform(-8.3629,406);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#333333").ss(1,1,1).p("EABIA/cQhNvwgkwsQgiwvAFxHQAEwYAmwHQAkujBAtj");
	this.shape_32.setTransform(-6.9071,406);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#333333").ss(1,1,1).p("EAA7A/cQg/v2gdwmQgcwxAExEQADwXAgwIQAdudA0tq");
	this.shape_33.setTransform(-5.6198,406);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#333333").ss(1,1,1).p("EAAvA/cQgyv7gYwhQgWwzADxBQADwVAZwJQAXuZAqtw");
	this.shape_34.setTransform(-4.493,406);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#333333").ss(1,1,1).p("EAAlA/cQgnv/gTweQgRwzACxAQACwTAUwKQASuVAht1");
	this.shape_35.setTransform(-3.5406,406);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#333333").ss(1,1,1).p("EAAcA/cQgdwDgPwaQgNw1ACw9QABwSAQwLQANuSAZt5");
	this.shape_36.setTransform(-2.7379,406);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#333333").ss(1,1,1).p("EAAVA/cQgWwGgLwXQgKw2ACw7QABwSALwMQAKuPATt8");
	this.shape_37.setTransform(-2.0348,406);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#333333").ss(1,1,1).p("EAAQA/cQgQwIgIwVQgHw3ABw6QABwRAIwMQAHuNAOt/");
	this.shape_38.setTransform(-1.5059,406);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#333333").ss(1,1,1).p("EAALA/cQgLwLgFwSQgFw3AAw6QABwQAGwMQAFuMAJuB");
	this.shape_39.setTransform(-1.0523,406);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#333333").ss(1,1,1).p("EAAIA/cQgIwMgDwSQgEw3ABw5QAAwPAEwNQADuKAHuD");
	this.shape_40.setTransform(-0.7031,406);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#333333").ss(1,1,1).p("EAAFA/cQgFwNgBwRQgDw3ABw5QAAwOACwOQACuJAEuE");
	this.shape_41.setTransform(-0.4542,406);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#333333").ss(1,1,1).p("EAADA/cQgDwOAAwQQgCw3ABw4QAAwOABwPQAAuIADuF");
	this.shape_42.setTransform(-0.2562,406);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#333333").ss(1,1,1).p("EAABA/cQgBwOAAwQQgBw4ABw3QAAwOAAwOQAAuIABuG");
	this.shape_43.setTransform(-0.1333,406);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#333333").ss(1,1,1).p("EAAAA/cQAAwPAAwPQAAw4AAw3QAAwOAAwOQAAuIAAuG");
	this.shape_44.setTransform(-0.05,406);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#333333").ss(1,1,1).p("EAAAA/cQAAwPAAwPQAAw4AAw3QAAwOAAwOQAAuHAAuH");
	this.shape_45.setTransform(-0.025,406);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45,p:{x:-0.025}}]},1).to({state:[{t:this.shape_45,p:{x:0}}]},1).to({state:[{t:this.shape}]},1).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-45.8,-1,47.8,814);


(lib.lettres_FAT = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#DEA60E").s().p("AjeEDQhphlAAicQAAibBrhnQBqhpCaAAQCsAAB0CEIhiBuQhJhchuAAQhYgBg+A6Qg+A6AABgQAABhA6A8QA8A6BSAAQB0ABBIhcIBkBoQh2CFiiAAQigABhphng");
	this.shape.setTransform(59.3,311.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#DEA60E").s().p("ABxFeIiZjfIhsAAIAADfIicAAIAAq6IEIAAQCiAABGA2QBFA4ABB6QAACmiFAyICwD6gAiUgHIByAAQBOAAAegaQAdgbAAg4QAAg4gegWQgfgUhJAAIh1AAg");
	this.shape_1.setTransform(-23.4,311.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#DEA60E").s().p("ADTFdIhBiXIkkAAIhBCXIilAAIEtq5ICWAAIEvK5gAhXA9ICuAAIhWjJg");
	this.shape_2.setTransform(-63.7,63.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.lettres_FAT, new cjs.Rectangle(-102.9,0,212.5,373.7), null);


(lib.hastaggraphicbranche = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AjQAuIAShbIDlAAICqAAIgSBbg");
	this.shape.setTransform(0,4.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.hastaggraphicbranche, new cjs.Rectangle(-20.8,0,41.7,9.2), null);


(lib.carto_T = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgiDAIAAlIIhJAAIAAg3IDXAAIAAA3IhJAAIAAFIg");
	this.shape.setTransform(-0.024,27.5523,1.4323,1.4323);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.carto_T, new cjs.Rectangle(-15.5,0,31,55.1), null);


(lib.carto_R = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAmDBQgEgKgBgGQgCgKAAgWIAAg8QAAgbgLgLQgKgMgYAAIgYAAIAACeIhGAAIAAmBIBqAAQA1AAAZAXQAYAVABAtIAAAeQgBA9gtARQAZAIALAWQAKATAAAgIAAA8QAAAeAIARgAgmgSIAbAAQAUAAALgJQALgKgBgWIAAgmQAAgVgIgJQgJgKgRAAIgiAAg");
	this.shape.setTransform(0.0119,27.5658,1.4323,1.4323);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.carto_R, new cjs.Rectangle(-15.6,0,31.2,55.2), null);


(lib.carto_O = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AidBnQgdgYgIgzQgHgxAVgfQAVgfAugHIDEgdQAugGAdAXQAdAYAIAzQAHAygVAeQgVAfguAHIjFAdIgTABQghAAgWgSgABhgyIjMAeQgmAGAGAiQAFAkAlgFIDNgeQAlgGgFgiQgEgfgdAAIgKAAg");
	this.shape.setTransform(-0.0345,17.3664,1.4323,1.4323);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.carto_O, new cjs.Rectangle(-28.2,0,56.4,34.7), null);


(lib.carto_C = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhNCsQgbgZAAgvIAAjHQAAguAbgZQAbgaAyAAQAzAAAbAaQAbAZgBAuIAAAlIhCAAIAAgpQAAgmgkAAQgjAAAAAmIAADPQAAAlAjAAQAkAAAAglIAAg3IBCAAIAAAzQABAvgbAZQgbAZgzAAQgyAAgbgZg");
	this.shape.setTransform(-0.0327,28.18,1.4323,1.4323);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.carto_C, new cjs.Rectangle(-15,0,30,56.4), null);


(lib.carto_A = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AA0DAIgMhGIhVAAIgMBGIhAAAIBHl/IBmAAIBHF/gAAfBGIghjBIgiDBIBDAAg");
	this.shape.setTransform(-0.0125,27.5356,1.4323,1.4323);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.carto_A, new cjs.Rectangle(-17.5,0,35.1,55.1), null);


(lib.Btn_cliquable_area = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#D6A432").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape.setTransform(0,30);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.Btn_cliquable_area, new cjs.Rectangle(-113,0,226,60), null);


(lib.quartier_light_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.quartier_light_3_forme();
	this.instance.setTransform(0,62.4,1,1,0,0,0,0,62.4);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(167).to({_off:false},0).to({alpha:1},19).to({alpha:0},28).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-39.6,0,79.2,124.7);


(lib.quartier_light_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.quartier_light_2_Forme();
	this.instance.setTransform(-0.1,42.4,1,1,0,0,0,-0.1,42.4);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(52).to({_off:false},0).to({alpha:1},23).to({alpha:0},54).wait(88));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-62.6,0,125.2,84.8);


(lib.masqueTexte = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// masqueTexte_1_ligne
	this.instance = new lib.masqueTexte_1_ligne();
	this.instance.setTransform(-218.8,191.5,0.0079,1,0,0,0,0,25.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(6).to({_off:false},0).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.0079,x:218.8},14).wait(1));

	// masqueTexte_1_ligne
	this.instance_1 = new lib.masqueTexte_1_ligne();
	this.instance_1.setTransform(-218.8,136.1,0.0079,1,0,0,0,0,25.5);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.0079,x:218.8},14).to({_off:true},1).wait(2));

	// masqueTexte_1_ligne
	this.instance_2 = new lib.masqueTexte_1_ligne();
	this.instance_2.setTransform(-218.8,80.8,0.0079,1,0,0,0,0,25.5);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(2).to({_off:false},0).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.0079,x:218.8},14).to({_off:true},1).wait(4));

	// masqueTexte_1_ligne
	this.instance_3 = new lib.masqueTexte_1_ligne();
	this.instance_3.setTransform(-218.8,25.5,0.0079,1,0,0,0,0,25.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.0079,x:218.8},14).to({_off:true},1).wait(6));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-220.5,-1.9,441.1,219);


(lib.lignes_all = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{close:114,close_lines:157});

	// timeline functions:
	this.frame_113 = function() {
		this.stop();
	}
	this.frame_156 = function() {
		this.stop();
	}
	this.frame_174 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(113).call(this.frame_113).wait(43).call(this.frame_156).wait(18).call(this.frame_174).wait(1));

	// ligne
	this.instance = new lib.ligne();
	this.instance.setTransform(-454.05,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(114).to({x:-883},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:-454.05},0).wait(18));

	// ligne
	this.instance_1 = new lib.ligne();
	this.instance_1.setTransform(-227.05,406.5,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(114).to({x:-883.2},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:-227.05},0).wait(18));

	// ligne
	this.instance_2 = new lib.ligne();
	this.instance_2.setTransform(-0.05,406.5,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(114).to({x:-883.4},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:-0.05},0).wait(18));

	// ligne
	this.instance_3 = new lib.ligne();
	this.instance_3.setTransform(226.95,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(93).to({x:227,y:322},20,cjs.Ease.quartInOut).wait(1).to({y:323},0).to({x:-883.05,y:406},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:227,y:322},0).to({x:226.95,y:406},16,cjs.Ease.quartInOut).wait(2));

	// ligne
	this.instance_4 = new lib.ligne();
	this.instance_4.setTransform(454.1,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(114).to({x:-883.45},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:454.1},0).wait(18));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-884.4,-85,1339.5,898.5);


(lib.hashtag_graphic = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(64));

	// hastag_graphic_branche
	this.instance = new lib.hastaggraphicbranche();
	this.instance.setTransform(37.35,51.9,1.0844,0.9952,0,-78.3276,102.1252,0.1,4.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({x:6.45,y:50.05},17,cjs.Ease.quartOut).to({_off:true},5).wait(3).to({_off:false},0).to({_off:true},4).wait(2).to({_off:false,x:26.45},0).to({_off:true},5).wait(28));

	// hastag_graphic_branche
	this.instance_1 = new lib.hastaggraphicbranche();
	this.instance_1.setTransform(-41.65,49.9,1.0844,0.9952,0,-78.3276,102.1252,0.1,4.6);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off:false},0).to({x:-7.4,y:49.8},18,cjs.Ease.quartOut).to({_off:true},4).wait(5).to({_off:false},0).to({_off:true},4).wait(31));

	// hastag_graphic_branche
	this.instance_2 = new lib.hastaggraphicbranche();
	this.instance_2.setTransform(2.7,21.7,1,1,0,0,0,0,4.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({x:-2.3,y:58.7},13,cjs.Ease.quartOut).to({_off:true},11).wait(3).to({_off:false,scaleX:2.3297,scaleY:1.0257,skewX:-12.8379,x:25.5},0).to({_off:true},4).wait(33));

	// hastag_graphic_branche
	this.instance_3 = new lib.hastaggraphicbranche();
	this.instance_3.setTransform(-7.3,77.7,1,1,0,0,0,0,4.6);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({x:0.7,y:41.7},14,cjs.Ease.quartOut).to({_off:true},4).wait(7).to({_off:false,regX:0.1,scaleX:1.9808,scaleY:1.0147,skewX:-9.7728,x:19.9,y:49.7},0).to({_off:true},4).wait(4).to({_off:false,regX:0,scaleX:1,scaleY:1,skewX:0,x:0.7,y:41.7},0).to({_off:true},4).wait(21));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-50.9,0,126.1,82.3);


(lib.carto_parcelle = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.quartier_light_1();
	this.instance.setTransform(0,69,1,1,0,0,0,0,69);
	this.instance.alpha = 0.1602;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({alpha:1},23).to({alpha:0.1602},60).to({_off:true},1).wait(108));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-67.6,0,135.2,137.9);


(lib.Btn_next = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{rollOver:55,rollOut:70});

	// timeline functions:
	this.frame_54 = function() {
		this.stop();
	}
	this.frame_69 = function() {
		this.stop();
	}
	this.frame_84 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(54).call(this.frame_54).wait(15).call(this.frame_69).wait(15).call(this.frame_84).wait(1));

	// ZONE
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.008)").s().p("EgjTAEsIAApXMBGnAAAIAAJXg");
	this.shape.setTransform(113,29.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(85));

	// txt
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgIAqIAAhDIgYAAIAAgQIBBAAIAAAQIgYAAIAABDg");
	this.shape_1.setTransform(161.05,29.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AASAqIgSgcIgQAcIgYAAIAdgqIgagpIAWAAIAPAZIAQgZIAWAAIgaApIAcAqg");
	this.shape_2.setTransform(145.2,29.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgeAqIAAhTIA8AAIAAARIgpAAIAAARIAkAAIAAAPIgkAAIAAASIAqAAIAAAQg");
	this.shape_3.setTransform(129.325,29.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAUAqIgngzIAAAzIgTAAIAAhTIASAAIAoA1IAAg1IATAAIAABTg");
	this.shape_4.setTransform(112.3,29.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgKAQIAEgOQgEgCAAgFQAAgEADgDQADgDAEAAQAFAAADADQADADAAAEQAAAEgDAEIgIANg");
	this.shape_5.setTransform(86.525,32.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAPAqIgYghIgIAKIAAAXIgTAAIAAhTIATAAIAAAjIAegjIAYAAIgiAlIAQAWIASAYg");
	this.shape_6.setTransform(76.975,29.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgeAfQgNgNAAgSQAAgSANgMQANgNARAAQASAAAOANQAMAMAAASQAAASgMANQgOAMgSAAQgRAAgNgMgAgRgSQgHAIAAAKQAAALAHAIQAIAIAJAAQAKAAAIgIQAHgIAAgLQAAgKgHgIQgIgIgKAAQgJAAgIAIg");
	this.shape_7.setTransform(59.3,29.05);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#333333").ss(1,1,1).p("EgjOAADMBGdgAF");
	this.shape_8.setTransform(113,-0.25);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(85));

	// Layer_2
	this.instance = new lib.Btn_cliquable_area();
	this.instance.setTransform(113,29.5,2,1,0,0,0,0,30);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("rgba(0,0,0,0.008)").s().p("EgjTAEsIAApXMBGnAAAIAAJXg");
	this.shape_9.setTransform(113,29.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance}]},3).to({state:[{t:this.instance}]},46).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.instance}]},5).to({state:[{t:this.instance}]},14).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},14).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({alpha:0.0117},46).to({_off:true},1).wait(5).to({_off:false},0).to({alpha:1},14).wait(1).to({alpha:0.0117},14).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-113.5,-1.5,453,61);


(lib.Btn_detail = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"rollOver":55,"rollOut":70});

	// timeline functions:
	this.frame_54 = function() {
		this.stop();
	}
	this.frame_69 = function() {
		this.stop();
	}
	this.frame_84 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(54).call(this.frame_54).wait(15).call(this.frame_69).wait(15).call(this.frame_84).wait(1));

	// ZONE
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.008)").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape.setTransform(0,29.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(85));

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgOAyIAAhkIAdAAIAABkg");
	this.shape_1.setTransform(-85.45,29.55);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgyAOIAAgcIBlAAIAAAcg");
	this.shape_2.setTransform(-85.45,29.55);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgaAqIAAhTIASAAIAABDIAjAAIAAAQg");
	this.shape_3.setTransform(38.425,29.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgIAqIAAhTIARAAIAABTg");
	this.shape_4.setTransform(24.675,29.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AAZAqIgHgSIgjAAIgHASIgUAAIAkhTIARAAIAkBTgAgKAIIAUAAIgKgYg");
	this.shape_5.setTransform(10.225,29.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgIAqIAAhDIgYAAIAAgQIBBAAIAAAQIgYAAIAABDg");
	this.shape_6.setTransform(-5.9,29.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgeAqIAAhTIA8AAIAAARIgpAAIAAARIAkAAIAAAPIgkAAIAAASIAqAAIAAAQg");
	this.shape_7.setTransform(-21.275,29.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AglAqIAAhTIAeAAQAVAAAMALQAMALAAATQAAATgMALQgMAMgXAAgAgTAaIANAAQAMAAAGgHQAHgGAAgNQAAgLgHgHQgGgGgOgBIgLAAg");
	this.shape_8.setTransform(-37.8,29.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#333333").ss(1,1,1).p("AxpAAMAjTAAA");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(85));

	// Layer_2
	this.instance = new lib.Btn_cliquable_area();
	this.instance.setTransform(0,29.5,1,1,0,0,0,0,30);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("rgba(0,0,0,0.008)").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape_10.setTransform(0,29.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance}]},3).to({state:[{t:this.instance}]},45).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.instance}]},6).to({state:[{t:this.instance}]},14).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},13).wait(2));
	this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({alpha:0.0117},45).to({_off:true},1).wait(6).to({_off:false},0).to({alpha:1},14).wait(1).to({alpha:0.0117},13).wait(2));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-114,-1,228,60.5);


(lib.bloc_visuel = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":121});

	// timeline functions:
	this.frame_120 = function() {
		this.stop();
	}
	this.frame_169 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(120).call(this.frame_120).wait(49).call(this.frame_169).wait(1));

	// Calque_8 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("EgjYA+xMAAAh9hMBGxAAAMAAAB9hg");
	mask.setTransform(-227.225,14.125);

	// carto_R
	this.instance = new lib.carto_R();
	this.instance.setTransform(163.9,-118.5,1,1,0,0,0,0,27.6);
	this.instance._off = true;

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(14).to({_off:false},0).to({x:-136.1},59,cjs.Ease.quartOut).wait(47).to({x:-501.6},43,cjs.Ease.quartIn).wait(7));

	// carto_O
	this.instance_1 = new lib.carto_O();
	this.instance_1.setTransform(278.95,-181.7,1,1,0,0,0,-0.1,17.4);
	this.instance_1._off = true;

	var maskedShapeInstanceList = [this.instance_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(31).to({_off:false},0).to({x:-21.05},42,cjs.Ease.quartOut).wait(47).to({x:-558.55},42,cjs.Ease.quartIn).wait(8));

	// carto_C
	this.instance_2 = new lib.carto_C();
	this.instance_2.setTransform(74.05,-179.35,1,1,0,0,0,-0.1,28.2);
	this.instance_2._off = true;

	var maskedShapeInstanceList = [this.instance_2];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(7).to({_off:false},0).to({x:-225.95},66,cjs.Ease.quartOut).wait(47).to({x:-763.45},40,cjs.Ease.quartIn).wait(10));

	// carto_T
	this.instance_3 = new lib.carto_T();
	this.instance_3.setTransform(229.05,-57.65,1,1,0,0,0,-0.1,27.6);
	this.instance_3._off = true;

	var maskedShapeInstanceList = [this.instance_3];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(26).to({_off:false},0).to({x:-70.95},47,cjs.Ease.quartOut).wait(47).to({x:-608.45},38,cjs.Ease.quartIn).wait(12));

	// carto_A
	this.instance_4 = new lib.carto_A();
	this.instance_4.setTransform(203.25,-232.4,1,1,0,0,0,0,27.5);
	this.instance_4._off = true;

	var maskedShapeInstanceList = [this.instance_4];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(19).to({_off:false},0).to({x:-96.75},54,cjs.Ease.quartOut).wait(47).to({x:-634.25},42,cjs.Ease.quartIn).wait(8));

	// Isolation_Mode
	this.instance_5 = new lib.parcelle_contours();
	this.instance_5.setTransform(188.85,-146.45,1,1,0,0,0,-0.1,235.3);
	this.instance_5.alpha = 0.1602;
	this.instance_5._off = true;

	var maskedShapeInstanceList = [this.instance_5];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(1).to({_off:false},0).to({x:-111.15,alpha:1},72,cjs.Ease.quartOut).wait(47).to({x:-648.65},48,cjs.Ease.quartIn).to({_off:true},1).wait(1));

	// Isolation_Mode
	this.instance_6 = new lib.carto_parcelle();
	this.instance_6.setTransform(107.45,-222.3,1,1,0,0,0,0,69);
	this.instance_6.alpha = 0.1602;
	this.instance_6._off = true;

	var maskedShapeInstanceList = [this.instance_6];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(1).to({_off:false},0).to({x:-192.55,alpha:1},72,cjs.Ease.quartOut).wait(47).to({x:-730.05},48,cjs.Ease.quartIn).to({_off:true},1).wait(1));

	// Layer_12
	this.instance_7 = new lib.quartier_light_3();
	this.instance_7.setTransform(-51.65,-24.35,1,1,0,0,0,0,62.4);
	this.instance_7._off = true;

	var maskedShapeInstanceList = [this.instance_7];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(101).to({_off:false},0).wait(69));

	// Calque_16
	this.instance_8 = new lib.quartier_light_2();
	this.instance_8.setTransform(-155.9,-76.65,1,1,0,0,0,-0.1,42.4);
	this.instance_8._off = true;

	var maskedShapeInstanceList = [this.instance_8];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(82).to({_off:false},0).wait(88));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-453.7,-381.7,453.7,470.6);


(lib.bloc_video = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":41});

	// timeline functions:
	this.frame_40 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(40).call(this.frame_40).wait(17));

	// masque_video (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_42 = new cjs.Graphics().p("Ega8A8AMAAAh3/MA7rAAAMAAAB3/g");
	var mask_graphics_43 = new cjs.Graphics().p("Ega8A8AMAAAh3/MA7qAAAMAAAB3/g");
	var mask_graphics_44 = new cjs.Graphics().p("Ega0A8AMAAAh3/MA7aAAAMAAAB3/g");
	var mask_graphics_45 = new cjs.Graphics().p("EgaSA8AMAAAh3/MA6VAAAMAAAB3/g");
	var mask_graphics_46 = new cjs.Graphics().p("EgY1A8AMAAAh3/MA3bAAAMAAAB3/g");
	var mask_graphics_47 = new cjs.Graphics().p("EgVyA8AMAAAh3/MAxSAAAMAAAB3/g");
	var mask_graphics_48 = new cjs.Graphics().p("EgQQA8AMAAAh3/MAmIAAAMAAAB3/g");
	var mask_graphics_49 = new cjs.Graphics().p("EgIMA8AMAAAh3/IV3AAMAAAB3/g");
	var mask_graphics_50 = new cjs.Graphics().p("EgCpA8AMAAAh3/IKsAAMAAAB3/g");
	var mask_graphics_51 = new cjs.Graphics().p("EAAZA8AMAAAh3/IEkAAMAAAB3/g");
	var mask_graphics_52 = new cjs.Graphics().p("EAB1A8AMAAAh3/IBqAAMAAAB3/g");
	var mask_graphics_53 = new cjs.Graphics().p("EACXA8AMAAAh3/IAlAAMAAAB3/g");
	var mask_graphics_54 = new cjs.Graphics().p("EACfA8AMAAAh3/IAVAAMAAAB3/g");
	var mask_graphics_55 = new cjs.Graphics().p("EACgA8AMAAAh3/IAUAAMAAAB3/g");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(42).to({graphics:mask_graphics_42,x:209.4828,y:0}).wait(1).to({graphics:mask_graphics_43,x:209.4292,y:0}).wait(1).to({graphics:mask_graphics_44,x:208.6245,y:0}).wait(1).to({graphics:mask_graphics_45,x:205.1378,y:0}).wait(1).to({graphics:mask_graphics_46,x:195.7504,y:0}).wait(1).to({graphics:mask_graphics_47,x:175.9563,y:0}).wait(1).to({graphics:mask_graphics_48,x:139.9623,y:0}).wait(1).to({graphics:mask_graphics_49,x:87.4933,y:0}).wait(1).to({graphics:mask_graphics_50,x:51.4993,y:0}).wait(1).to({graphics:mask_graphics_51,x:31.7053,y:0}).wait(1).to({graphics:mask_graphics_52,x:22.3178,y:0}).wait(1).to({graphics:mask_graphics_53,x:18.8311,y:0}).wait(1).to({graphics:mask_graphics_54,x:18.0265,y:0}).wait(1).to({graphics:mask_graphics_55,x:17.973,y:0}).wait(1).to({graphics:null,x:0,y:0}).wait(1));

	// VIDEO
	this.videoContainer = new lib.video();
	this.videoContainer.name = "videoContainer";
	this.videoContainer.setTransform(36.7,765,0.6587,0.6587,0,0,0,0.3,480.2);

	var maskedShapeInstanceList = [this.videoContainer];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.videoContainer).to({y:-33},40,cjs.Ease.quartInOut).to({_off:true},16).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-349.3,418.6,1430.3999999999999);


(lib.bloc_NEXT_btn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":20});

	// timeline functions:
	this.frame_19 = function() {
		this.stop();
	}
	this.frame_36 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(19).call(this.frame_19).wait(17).call(this.frame_36).wait(1));

	// CTA_DETAIL
	this.btnNext = new lib.Btn_next();
	this.btnNext.name = "btnNext";
	this.btnNext.setTransform(-340.5,411.5,1,1,0,0,0,0,26.5);

	this.timeline.addTween(cjs.Tween.get(this.btnNext).to({y:350.5},19,cjs.Ease.quartInOut).wait(2).to({y:411.5},14,cjs.Ease.quartInOut).to({_off:true},1).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-454,0,453,444.5);


(lib.bloc_hashtag = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_61 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(61).call(this.frame_61).wait(2));

	// Layer_1
	this.instance = new lib.hashtag_graphic("synched",0);
	this.instance.setTransform(-456.05,243.2,2.3169,2.3169,0,0,0,-0.1,50.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true},61).wait(2));

	// Layer_5 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_28 = new cjs.Graphics().p("At7BpIAAjRIb2AAIAADRg");
	var mask_graphics_32 = new cjs.Graphics().p("At7DSIAAmjIb2AAIAAGjg");
	var mask_graphics_40 = new cjs.Graphics().p("AuAExIAAphIcBAAIAAJhg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(28).to({graphics:mask_graphics_28,x:-396.85,y:265.45}).wait(4).to({graphics:mask_graphics_32,x:-396.85,y:255.025}).wait(8).to({graphics:mask_graphics_40,x:-396.2,y:245.55}).wait(23));

	// Layer_3
	this.instance_1 = new lib.txt_hashtag();
	this.instance_1.setTransform(-421.5,244.05,1,1,0,0,0,0,31.6);
	this.instance_1._off = true;

	var maskedShapeInstanceList = [this.instance_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(28).to({_off:false},0).to({_off:true},34).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-573.7,0,287.1,317.1);


(lib.bloc_CTA = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":25});

	// timeline functions:
	this.frame_24 = function() {
		this.stop();
	}
	this.frame_41 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(24).call(this.frame_24).wait(17).call(this.frame_41).wait(1));

	// CTA_DETAIL
	this.btnCta = new lib.Btn_detail();
	this.btnCta.name = "btnCta";
	this.btnCta.setTransform(-340.5,411.5,1,1,0,0,0,0,26.5);

	this.timeline.addTween(cjs.Tween.get(this.btnCta).to({y:350.5},24,cjs.Ease.quartInOut).wait(2).to({y:411.5},14,cjs.Ease.quartInOut).to({_off:true},1).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-454.5,0,228,444.5);


(lib.blocInfostextes = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":80});

	// timeline functions:
	this.frame_79 = function() {
		this.stop();
	}
	this.frame_120 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(79).call(this.frame_79).wait(41).call(this.frame_120).wait(1));

	// Bloc_Hashtag
	this.instance = new lib.bloc_hashtag();
	this.instance.setTransform(-391.5,251.2,0.9,0.9,0,0,0,-412.9,244.7);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(62).to({_off:false},0).to({_off:true},18).wait(41));

	// masque_Titre
	this.instance_1 = new lib.masque_generique();
	this.instance_1.setTransform(-527.95,-13.9,0.0075,0.4711,0,0,0,0,17.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(34).to({_off:false},0).to({regX:0.5,scaleX:1.9223,x:-527.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.0128,x:-228.9},10,cjs.Ease.quartInOut).to({_off:true},1).wait(26).to({_off:false},0).to({regX:0.5,scaleX:1.9223,x:-527.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.0074,x:-528.7},10,cjs.Ease.quartInOut).to({_off:true},1).wait(21));

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#DEA60E").s().p("AAEA4IAAgaIgsAAIAAgUIAohBIAaAAIglA+IAPAAIAAgSIAYAAIAAASIANAAIAAAXIgNAAIAAAag");
	this.shape.setTransform(-303.875,-14.15);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#DEA60E").s().p("AgfA4IAqhaIgbAAIAAAPIgXAAIAAgkIBQAAIAAATIgtBcg");
	this.shape_1.setTransform(-325,-14.15);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#DEA60E").s().p("AgrAmIAOgSQATAQAOAAQAHAAAEgDQAEgDAAgGQAAgEgFgDQgEgDgLgDQgUgFgJgGQgJgIAAgQQAAgPAMgKQALgIAQAAQAMAAALAEQALAEAIAHIgMASQgPgLgPAAQgGAAgDADQgEADAAAFQAAAFAEADQAFACAPAFQAQAEAJAGQAIAIAAAPQAAAPgLAJQgLAKgTAAQgZAAgVgUg");
	this.shape_2.setTransform(-363.175,-14.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#DEA60E").s().p("AgoA4IAAhvIBPAAIAAAXIg2AAIAAAWIAxAAIAAAUIgxAAIAAAYIA4AAIAAAWg");
	this.shape_3.setTransform(-385.15,-14.15);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#DEA60E").s().p("AgKA4IgthvIAbAAIAcBGIAdhGIAbAAIgtBvg");
	this.shape_4.setTransform(-408.15,-14.15);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#DEA60E").s().p("AgLA4IAAhvIAYAAIAABvg");
	this.shape_5.setTransform(-428.25,-14.15);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#DEA60E").s().p("AAWA4IAAgsIgrAAIAAAsIgZAAIAAhvIAZAAIAAAvIArAAIAAgvIAZAAIAABvg");
	this.shape_6.setTransform(-449,-14.15);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#DEA60E").s().p("AgiApQgSgQABgYQgBgZASgQQAQgRAYAAQAcAAASAVIgPASQgMgPgSAAQgMAAgKAJQgKAJAAAPQAAAPAJAKQAJAKAMAAQATAAAMgPIAPARQgSAVgaAAQgZAAgQgRg");
	this.shape_7.setTransform(-472.85,-14.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#DEA60E").s().p("AASA4IgYgjIgRAAIAAAjIgZAAIAAhvIAqAAQAaAAALAJQALAJAAATQAAAagWAIIAdAogAgXgBIASAAQAMAAAFgDQAEgFAAgJQAAgIgFgEQgEgEgLAAIgTAAg");
	this.shape_8.setTransform(-496.175,-14.15);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#DEA60E").s().p("AAiA4IgKgYIguAAIgKAYIgbAAIAwhvIAXAAIAwBvgAgNAKIAbAAIgOgfg");
	this.shape_9.setTransform(-520.4,-14.15);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},34).to({state:[]},55).to({state:[]},1).wait(31));

	// masque_Titre (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_17 = new cjs.Graphics().p("EgpOArJMAAAhWRMA3LAAAMAAABWRg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(17).to({graphics:mask_graphics_17,x:-263.9,y:-6.1095}).wait(24).to({graphics:null,x:0,y:0}).wait(80));

	// Layer_5
	this.instance_2 = new lib.lettres_FAT();
	this.instance_2.setTransform(-433.85,-73.05,1,1,0,0,0,0,186.8);
	this.instance_2._off = true;

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#DEA60E").s().p("AAaFdIAAimIkYAAIAAh4ID5mbICpAAIjsGMIBiAAIAAh6ICWAAIAAB6IBPAAIAACHIhPAAIAACmg");
	this.shape_10.setTransform(-336.4,131.75);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#DEA60E").s().p("AixBCIAAiEIFjAAIAACEg");
	this.shape_11.setTransform(-440.4,136.65);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#DEA60E").s().p("AjHFdIENo0IiwAAIAABaIiSAAIAAjfIH4AAIAAB2IkZJDg");
	this.shape_12.setTransform(-504.85,131.75);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#DEA60E").s().p("ACLFeIAAkTIkVAAIAAETIicAAIAAq6ICcAAIAAEjIEVAAIAAkjICcAAIAAK6g");
	this.shape_13.setTransform(-411.5,7.85);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#DEA60E").s().p("AjeEDQhohlAAicQAAibBqhnQBrhpCYAAQCtAABzCEIhgBuQhLhchuAAQhWgBg/A6Qg/A6ABBgQAABhA6A8QA7A6BTAAQB0ABBJhcIBjBoQh3CFihAAQigABhphng");
	this.shape_14.setTransform(-497.65,7.45);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#DEA60E").s().p("ABxFdIiajeIhsAAIAADeIibAAIAAq5IEIAAQCiAABGA3QBGA3AAB6QAACmiFAyICxD5gAiVgHIBzAAQBPAAAdgaQAcgbAAg4QAAg4gdgWQgfgVhKABIh1AAg");
	this.shape_15.setTransform(-303.65,-116);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#DEA60E").s().p("AhNFdIAAq5ICbAAIAAK5g");
	this.shape_16.setTransform(-463,106.75);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#DEA60E").s().p("ACLFeIAAkUIkVAAIAAEUIicAAIAAq7ICcAAIAAElIEVAAIAAklICcAAIAAK7g");
	this.shape_17.setTransform(-271.4,-17.15);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#DEA60E").s().p("AjeEDQhohlgBicQAAiaBrhoQBrhpCYAAQCtAABzCEIhhBtQhJhchuABQhYAAg+A5Qg/A6AABgQAABiA8A7QA6A6BUAAQByAABKhbIBkBoQh3CGiiAAQihAAhohng");
	this.shape_18.setTransform(-357.55,-17.55);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#DEA60E").s().p("AByFeIiajgIhsAAIAADgIidAAIAAq7IEKAAQCiAABFA3QBFA3AAB7QAACmiFAxICxD7gAiUgIIBxAAQBPAAAdgaQAdgZABg5QgBg4gegWQgegUhJgBIh1AAg");
	this.shape_19.setTransform(-478.65,-17.15);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#DEA60E").s().p("ADUFeIhBiXIklAAIhACXIinAAIEuq7ICXAAIEtK7gAhWA+ICtAAIhXjLg");
	this.shape_20.setTransform(-403.75,-141);

	var maskedShapeInstanceList = [this.instance_2,this.shape_10,this.shape_11,this.shape_12,this.shape_13,this.shape_14,this.shape_15,this.shape_16,this.shape_17,this.shape_18,this.shape_19,this.shape_20];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_2}]},17).to({state:[{t:this.instance_2}]},14).to({state:[{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10}]},1).to({state:[{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16}]},5).to({state:[]},4).to({state:[]},39).wait(41));
	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(17).to({_off:false},0).to({x:-276.85},14,cjs.Ease.quartOut).to({_off:true},1).wait(89));

	// masque_Titre
	this.instance_3 = new lib.masque_generique();
	this.instance_3.setTransform(221.1,-163.9,0.0128,0.4711,0,0,0,0,17.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({regX:0.5,scaleX:1.9223,x:-75.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.0074,x:-68.7},10,cjs.Ease.quartInOut).to({_off:true},1).wait(101));

	// Calque_2
	this.instance_4 = new lib.trait_4();
	this.instance_4.setTransform(-229.85,128.95,0.7559,0.7559,-41.935,0,0,-382.7,-73.5);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(28).to({_off:false},0).to({_off:true},73).wait(20));

	// Layer_18
	this.instance_5 = new lib.trait_3();
	this.instance_5.setTransform(-182.55,-50.35,0.756,0.756,0,0,0,-0.1,-0.1);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(23).to({_off:false},0).to({_off:true},78).wait(20));

	// Layer_17
	this.instance_6 = new lib.trait_2();
	this.instance_6.setTransform(-343.1,-293.7,0.7559,0.7559,-3.9818,0,0,-140.7,-86.1);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(19).to({_off:false},0).to({_off:true},82).wait(20));

	// Layer_8
	this.instance_7 = new lib.trait_1();
	this.instance_7.setTransform(-276.45,45.1,0.84,0.84,-21.7042,0,0,8.8,-49.8);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(13).to({_off:false},0).to({_off:true},47).wait(61));

	// masque_Titre
	this.instance_8 = new lib.masque_generique();
	this.instance_8.setTransform(-228.9,-13.9,0.0128,0.4711,0,0,0,0,17.2);
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(4).to({_off:false},0).to({regX:0.5,scaleX:1.9223,x:-527.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.0074,x:-528.7},10,cjs.Ease.quartInOut).to({_off:true},1).wait(97));

	// Layer_10
	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#01FBFC").s().p("AgJA0QgEgEAAgGQAAgFAEgFQAEgEAFAAQAGAAAEAEQAEAFAAAFQAAAGgEAEQgEAFgGAAQgFAAgEgFgAgJAVIgEg5IAAgUIAbAAIAAAUIgFA5g");
	this.shape_21.setTransform(-357.925,-14.075);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#01FBFC").s().p("AAbA4Ig1hFIAABFIgZAAIAAhvIAYAAIA2BHIAAhHIAZAAIAABvg");
	this.shape_22.setTransform(-378.95,-14.15);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#01FBFC").s().p("AgoA4IAAhvIBPAAIAAAXIg2AAIAAAWIAxAAIAAAUIgxAAIAAAYIA4AAIAAAWg");
	this.shape_23.setTransform(-402.45,-14.15);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#01FBFC").s().p("AgMA4IAAhvIAZAAIAABvg");
	this.shape_24.setTransform(-422.55,-14.15);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#01FBFC").s().p("AgCA5IADgYIgNAAIgEAYIgZAAIAFgYIgQAAIAFgWIAPAAIADgTIgRAAIAEgWIARAAIAEgaIAXAAIgDAaIANAAIAEgaIAZAAIgFAaIARAAIgEAWIgRAAIgDATIASAAIgEAWIgSAAIgEAYgAgJALIAOAAIAEgTIgOAAg");
	this.shape_25.setTransform(-459.375,-14.275);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#01FBFC").s().p("AgjA4IAAhvIAZAAIAABYIAuAAIAAAXg");
	this.shape_26.setTransform(-497.625,-14.15);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#01FBFC").s().p("AgjApQgRgQAAgYQAAgZARgQQASgRAXAAQAcAAASAVIgPASQgMgPgSAAQgNAAgKAJQgJAJgBAPQAAAPAKAKQAJAKANAAQASAAALgPIAQARQgTAVgZAAQgZAAgRgRg");
	this.shape_27.setTransform(-520.4,-14.2);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#DEA60E").s().p("AgJA0QgEgEAAgGQAAgFAEgFQAEgEAFAAQAGAAAEAEQAEAFAAAFQAAAGgEAEQgEAFgGAAQgFAAgEgFgAgJAVIgEg5IAAgUIAbAAIAAAUIgFA5g");
	this.shape_28.setTransform(-422.675,-14.075);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#DEA60E").s().p("AgCA5IADgYIgNAAIgEAYIgZAAIAFgYIgQAAIAFgWIAPAAIADgTIgRAAIAEgWIARAAIAEgaIAXAAIgDAaIANAAIAEgaIAZAAIgFAaIARAAIgEAWIgRAAIgDATIASAAIgEAWIgSAAIgEAYgAgJALIAOAAIAEgTIgOAAg");
	this.shape_29.setTransform(-459.375,-14.275);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#DEA60E").s().p("AgjA4IAAhvIAZAAIAABYIAuAAIAAAXg");
	this.shape_30.setTransform(-497.625,-14.15);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#DEA60E").s().p("AgjApQgRgQAAgYQAAgZARgQQASgRAXAAQAcAAASAVIgPASQgMgPgSAAQgNAAgKAJQgJAJgBAPQAAAPAKAKQAJAKANAAQASAAALgPIAQARQgTAVgZAAQgZAAgRgRg");
	this.shape_31.setTransform(-520.4,-14.2);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#DEA60E").s().p("AgMA4IAAhZIgfAAIAAgWIBXAAIAAAWIggAAIAABZg");
	this.shape_32.setTransform(-411.925,-14.15);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#DEA60E").s().p("AAbA4Ig0hFIAABFIgaAAIAAhvIAYAAIA2BHIAAhHIAZAAIAABvg");
	this.shape_33.setTransform(-435.3,-14.15);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#DEA60E").s().p("AgoA4IAAhvIBPAAIAAAXIg2AAIAAAWIAxAAIAAAUIgxAAIAAAYIA4AAIAAAWg");
	this.shape_34.setTransform(-458.8,-14.15);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#DEA60E").s().p("AgMA4IAAhvIAZAAIAABvg");
	this.shape_35.setTransform(-478.9,-14.15);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21}]},13).to({state:[{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28}]},5).to({state:[{t:this.shape_31},{t:this.shape_30},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32}]},5).to({state:[]},11).to({state:[]},46).wait(41));

	// Layer_11
	this.instance_9 = new lib.masqueTexte("synched",0);
	this.instance_9.setTransform(-340,105.8,0.85,0.85,0,0,0,0.1,108.8);
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(37).to({_off:false},0).to({_off:true},40).wait(3).to({_off:false,rotation:180},0).to({_off:true},40).wait(1));

	// Layer_12
	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAggeQAhgeAsAAQAsAAAfAbQAfAbAAAuIAAAfIicAAQADASAPALQAQALATAAQAfAAAVgVIAiAlQglAigxAAQgwAAgggegAgdgyQgPALgDATIBeAAQgCgUgNgKQgMgKgRAAQgSAAgOAKg");
	this.shape_36.setTransform(-227.7,133.425);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AgdCXIAAjWIA7AAIAADWgAgYhaQgKgKAAgPQAAgPAKgKQAKgKAOAAQAPAAAKAKQAKAKAAAPQAAAPgKAKQgKAKgPAAQgOAAgKgKg");
	this.shape_37.setTransform(-245.975,129.075);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("AApCUIAAh1QAAgwglAAQgRAAgNAMQgNAMAAAZIAAB0Ig9AAIAAkoIA9AAIAABqQAbgbAgAAQAiAAAYAYQAWAYAAAnIAACCg");
	this.shape_38.setTransform(-264.45,129.35);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AhuCUIAAkkIA8AAIAAAYQAcgbAkAAQAmAAAdAfQAeAfAAAwQAAAvgeAgQgdAggnAAQgnAAgYggIAABqgAgkhQQgPASAAAaQAAAaAPAQQAPARAUAAQAUAAAQgRQAPgQAAgaQAAgZgPgTQgPgRgUAAQgVAAgPARg");
	this.shape_39.setTransform(-290.075,137.15);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("AhJBbQgXgUAAghQAAghAYgPQAYgQAqAAIArAAIAAgCQAAgigkAAQgPAAgRAGQgRAGgMAKIgbgnQAqgfA1AAQAnAAAaAUQAYATAAArIAACIIg4AAIAAgaQgYAdggAAQgiAAgYgUgAgkAmQAAALAIAHQAKAGAPAAQAPAAAMgKQANgKAAgQIAAgLIglAAQgkAAAAAXg");
	this.shape_40.setTransform(-316.35,133.425);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("Ag9BtIAAjWIA8AAIAAAdQAKgOASgJQARgJASAAIAAA4IgLAAQgbAAgNATQgMATAAAeIAABdg");
	this.shape_41.setTransform(-335.125,133.275);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("Ag1CQQgZgIgSgOIAXguQAdAXAhAAQAgAAAUgRQATgQAAgkQgdAggpAAQgqAAgdgdQgdgcAAgvQAAgwAegfQAegeAmAAQAlAAAaAgIAAgdIA8AAIAAC7QAAAdgKAWQgKAWgRANQghAbgtAAQgYAAgZgIgAgihVQgQAPAAAZQAAAaAPAQQAPAOAVAAQAWAAAOgOQAPgQAAgZQAAgagPgPQgOgPgVAAQgVAAgPAPg");
	this.shape_42.setTransform(-358.125,137.525);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("AhQBPQghggAAgvQAAguAhggQAgggAwAAQAxAAAgAgQAhAgAAAuQAAAvghAgQggAggxAAQgwAAgggggAglgqQgPARAAAZQAAAbAPAQQAPARAWAAQAXAAAPgRQAPgQAAgbQAAgZgPgRQgPgRgXAAQgWAAgPARg");
	this.shape_43.setTransform(-383.875,133.425);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFFFFF").s().p("AgfB5QgWgUAAgkIAAhiIgZAAIAAgrIAZAAIAAhAIA7AAIAABAIA1AAIAAArIg1AAIAABgQABAMAGAHQAGAHAJAAQARAAAMgPIAWAqQgcAZgfAAQgeAAgVgUg");
	this.shape_44.setTransform(-405.1,130.375);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFFFFF").s().p("Ag9BtIAAjWIA8AAIAAAdQAKgOASgJQARgJASAAIAAA4IgLAAQgbAAgNATQgMATAAAeIAABdg");
	this.shape_45.setTransform(-421.825,133.275);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FFFFFF").s().p("AhJBbQgXgUAAghQAAghAYgPQAYgQAqAAIArAAIAAgCQAAgigkAAQgPAAgRAGQgRAGgMAKIgbgnQAqgfA1AAQAnAAAaAUQAYATAAArIAACIIg4AAIAAgaQgYAdggAAQgiAAgYgUgAgkAmQAAALAIAHQAKAGAPAAQAPAAAMgKQANgKAAgQIAAgLIglAAQgkAAAAAXg");
	this.shape_46.setTransform(-443.25,133.425);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFFFFF").s().p("AhEBQQgigeAAgyQAAgwAigfQAigfAtAAQAXAAAYAKQAYAJASATIgfApQgLgNgPgGQgOgHgPAAQgXAAgRAPQgRAQAAAaQAAAbARAQQARAPAWAAQAeAAAYgeIAkAoQgqArgwAAQgwAAghgfg");
	this.shape_47.setTransform(-466.075,133.425);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FFFFFF").s().p("AhJBbQgXgUAAghQAAghAYgPQAYgQApAAIAsAAIAAgCQAAgigkAAQgPAAgRAGQgRAGgNAKIgbgnQArgfA1AAQAnAAAZAUQAZATAAArIAACIIg4AAIAAgaQgYAdghAAQghAAgYgUgAgkAmQAAALAJAHQAIAGARAAQAPAAAMgKQAMgKAAgQIAAgLIglAAQgkAAAAAXg");
	this.shape_48.setTransform(-500.8,133.425);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FFFFFF").s().p("AgdCUIAAkoIA7AAIAAEog");
	this.shape_49.setTransform(-518,129.35);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FFFFFF").s().p("Ag9BtIAAjWIA8AAIAAAdQAKgOASgJQARgJASAAIAAA4IgLAAQgbAAgNATQgMATAAAeIAABdg");
	this.shape_50.setTransform(-225.275,84.525);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FFFFFF").s().p("AhMBXQgYgWAAgsIAAiBIA9AAIAAB0QAAAxAkABQARAAANgOQANgNAAgYIAAhzIA9AAIAADWIg9AAIAAgcQgWAfgiAAQgkAAgYgWg");
	this.shape_51.setTransform(-247.275,84.85);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FFFFFF").s().p("AgqBmQgagIgVgRIAbgmQAhAaAmAAQALAAAHgEQAGgFAAgHQABgIgKgGQgKgHgQgFIgcgKQgJgEgMgIQgYgOAAgeQAAgdAYgTQAYgTAlAAQAmAAAmAaIgWAoQgcgUgcAAQgbAAAAAQQAAAIAJAFQAJAEATAHIAdAKQAIADAMAHQAXANAAAgQAAAggYATQgXATgkAAQgXAAgagJg");
	this.shape_52.setTransform(-270.7,84.675);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAhgeQAggeArAAQAtAAAfAbQAfAbAAAuIAAAfIicAAQADASAQALQAPALATAAQAfAAAVgVIAhAlQgkAigxAAQgwAAgggegAgdgyQgPALgDATIBeAAQgCgUgMgKQgNgKgRAAQgRAAgPAKg");
	this.shape_53.setTransform(-303.5,84.675);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FFFFFF").s().p("AhLCBQgggeAAgzQAAgyAggeQAhgeArABQAtgBAfAbQAfAbAAAuIAAAfIicAAQADATAPALQAQAKATABQAfgBAVgUIAiAkQglAigxAAQgwAAgggegAgdgCQgPALgCASIBdAAQgCgTgNgKQgMgKgSAAQgQAAgPAKgAgkhXIAxhHIA+AaIg3Atg");
	this.shape_54.setTransform(-328.2,79.85);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FFFFFF").s().p("AggB5QgVgUAAgkIAAhiIgZAAIAAgrIAZAAIAAhAIA8AAIAABAIAzAAIAAArIgzAAIAABgQgBAMAHAHQAGAHAJAAQARAAALgPIAXAqQgcAZgfAAQgeAAgWgUg");
	this.shape_55.setTransform(-348.9,81.625);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FFFFFF").s().p("AApBtIAAh4QAAgyglAAQgRAAgNANQgNANAAAZIAAB3Ig9AAIAAjWIA9AAIAAAYQAagbAiAAQAhAAAXAYQAYAYAAAnIAACCg");
	this.shape_56.setTransform(-370.9,84.525);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAhgeQAggeArAAQAtAAAfAbQAfAbAAAuIAAAfIicAAQADASAQALQAPALATAAQAfAAAVgVIAhAlQgkAigxAAQgwAAgggegAgdgyQgPALgCATIBdAAQgCgUgMgKQgNgKgSAAQgRAAgOAKg");
	this.shape_57.setTransform(-396.05,84.675);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#FFFFFF").s().p("ABtBtIAAh0QAAgygkAAQgSAAgNANQgOANAAAZIAABzIg7AAIAAh0QAAgagIgMQgIgMgRAAQgSAAgNANQgNANAAAZIAABzIg8AAIAAjWIA8AAIAAAYQAZgbAfAAQAVAAAQALQAQANAIARQANgUAVgLQAVgKAVAAQAmAAAXAWQAXAXAAAqIAACCg");
	this.shape_58.setTransform(-428.025,84.525);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#FFFFFF").s().p("Ag1CQQgZgIgSgOIAXguQAdAXAhAAQAgAAAUgRQATgQAAgkQgdAggpAAQgqAAgdgdQgdgcAAgvQAAgwAegfQAegeAmAAQAlAAAaAgIAAgdIA8AAIAAC7QAAAdgKAWQgKAWgRANQghAbgtAAQgYAAgZgIgAgihVQgQAPAAAZQAAAaAPAQQAPAOAVAAQAWAAAOgOQAPgQAAgZQAAgagPgPQgOgPgVAAQgVAAgPAPg");
	this.shape_59.setTransform(-461.725,88.775);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#FFFFFF").s().p("AhMBXQgYgWAAgsIAAiBIA9AAIAAB0QAAAxAkABQARAAANgOQANgNAAgYIAAhzIA9AAIAADWIg9AAIAAgcQgWAfgiAAQgkAAgYgWg");
	this.shape_60.setTransform(-487.425,84.85);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#FFFFFF").s().p("AhJBbQgXgUAAghQAAghAYgPQAYgQAqAAIArAAIAAgCQAAgigkAAQgPAAgRAGQgSAGgLAKIgbgnQAqgfA2AAQAmAAAaAUQAYATAAArIAACIIg4AAIAAgaQgYAdggAAQgjAAgXgUgAgkAmQAAALAIAHQAJAGAQAAQAPAAAMgKQANgKAAgQIAAgLIglAAQgkAAAAAXg");
	this.shape_61.setTransform(-512.45,84.675);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#FFFFFF").s().p("AAoBtIAAh4QAAgygkAAQgRAAgNANQgOANAAAZIAAB3Ig8AAIAAjWIA8AAIAAAYQAbgbAiAAQAiAAAWAYQAYAYAAAnIAACCg");
	this.shape_62.setTransform(-321.2,35.775);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#FFFFFF").s().p("AhQBPQghggAAgvQAAguAhggQAgggAwAAQAxAAAgAgQAhAgAAAuQAAAvghAgQggAggxAAQgwAAgggggAglgqQgPARAAAZQAAAbAPAQQAPARAWAAQAXAAAPgRQAPgQAAgbQAAgZgPgRQgPgRgXAAQgWAAgPARg");
	this.shape_63.setTransform(-347.075,35.925);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#FFFFFF").s().p("AgdCXIAAjWIA7AAIAADWgAgYhaQgKgKAAgPQAAgPAKgKQAKgKAOAAQAPAAAKAKQAKAKAAAPQAAAPgKAKQgKAKgPAAQgOAAgKgKg");
	this.shape_64.setTransform(-365.875,31.575);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#FFFFFF").s().p("AggB5QgVgUAAgkIAAhiIgZAAIAAgrIAZAAIAAhAIA8AAIAABAIAzAAIAAArIgzAAIAABgQgBAMAHAHQAGAHAJAAQARAAALgPIAXAqQgcAZgfAAQgeAAgWgUg");
	this.shape_65.setTransform(-379.95,32.875);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#FFFFFF").s().p("AgdCXIAAjWIA7AAIAADWgAgYhaQgKgKAAgPQAAgPAKgKQAKgKAOAAQAPAAAKAKQAKAKAAAPQAAAPgKAKQgKAKgPAAQgOAAgKgKg");
	this.shape_66.setTransform(-395.125,31.575);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#FFFFFF").s().p("AgqBmQgagIgVgRIAagmQAjAaAkAAQAMAAAGgEQAIgFAAgHQAAgIgKgGQgJgHgRgFIgbgKQgKgEgMgIQgYgOAAgeQAAgdAYgTQAYgTAlAAQAmAAAmAaIgXAoQgcgUgbAAQgbAAAAAQQAAAIAJAFQAIAEAUAHIAdAKQAIADANAHQAWANAAAgQAAAggYATQgXATgkAAQgXAAgagJg");
	this.shape_67.setTransform(-411.7,35.925);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#FFFFFF").s().p("AhQBPQghggAAgvQAAguAhggQAgggAwAAQAxAAAgAgQAhAgAAAuQAAAvghAgQggAggxAAQgwAAgggggAglgqQgPARAAAZQAAAbAPAQQAPARAWAAQAXAAAPgRQAPgQAAgbQAAgZgPgRQgPgRgXAAQgWAAgPARg");
	this.shape_68.setTransform(-434.725,35.925);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#FFFFFF").s().p("AhuCUIAAkkIA8AAIAAAZQAcgcAkAAQAmAAAdAfQAeAgAAAvQAAAvgeAgQgdAggngBQgnABgYggIAABqgAgkhQQgPASAAAaQAAAaAPAQQAPAQAUABQAUgBAQgQQAPgPAAgbQAAgagPgSQgPgRgUAAQgVAAgPARg");
	this.shape_69.setTransform(-460.425,39.65);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#FFFFFF").s().p("AAoBsIgpg8IgoA8IhGAAIBLhtIhHhqIBIAAIAkA6IAmg6IBGAAIhKBqIBNBtg");
	this.shape_70.setTransform(-486,35.95);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#FFFFFF").s().p("AhmCMIAAkXIDJAAIAAA4IiLAAIAAA5IB8AAIAAA0Ih8AAIAAA6ICPAAIAAA4g");
	this.shape_71.setTransform(-510.1,32.725);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36}]},57).to({state:[]},43).to({state:[]},1).wait(20));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-528.8,-368.8,754.1999999999999,669.5);


// stage content:
(lib.FS_project_archives_carto = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":150});

	// timeline functions:
	this.frame_100 = function() {
		//var frequency = 60;
		//stage.enableMouseOver(frequency);
		
		// EVENT DE CLIC POUR LA SUITE
		this.next_btn.addEventListener("click", fl_ClickToGoToAndPlayFromFrame.bind(this));
		this.next_btn.addEventListener("mouseover", fl_MouseOverHandler.bind(this));
		this.next_btn.addEventListener("mouseout", fl_MouseOutHandler.bind(this));
		
		
		function fl_ClickToGoToAndPlayFromFrame()
		{
			//this.gotoAndPlay("close");
			
			var event = new Event('next');
			this.dispatchEvent(event);		
			event = null;
			
			this.next_btn.btnNext.gotoAndPlay('rollOut');
			
		}
		function fl_MouseOverHandler(){	this.next_btn.btnNext.gotoAndPlay('rollOver'); }
		function fl_MouseOutHandler(){	this.next_btn.btnNext.gotoAndPlay('rollOut'); }
		
		
		// EVENT DE CLIC POUR LE LIEN
		this.cta_mc.addEventListener("click", projectDetails.bind(this));
		this.cta_mc.addEventListener("mouseover", pDetails_MouseOverHandler.bind(this));
		this.cta_mc.addEventListener("mouseout", pDetails_MouseOutHandler.bind(this));
		
		function projectDetails()
		{	
			var event = new Event('details');
			this.dispatchEvent(event);		
			event = null;
			
			this.cta_mc.btnCta.gotoAndPlay('rollOut');
			
		}
		function pDetails_MouseOverHandler(){	this.cta_mc.btnCta.gotoAndPlay('rollOver'); }
		function pDetails_MouseOutHandler(){	this.cta_mc.btnCta.gotoAndPlay('rollOut'); }
	}
	this.frame_149 = function() {
		//PAUSE
		this.stop();
	}
	this.frame_150 = function() {
		this.textes_mc.gotoAndPlay("close");
		this.visuel_mc.gotoAndPlay("close");
		this.cta_mc.gotoAndPlay("close");
		this.video_mc.gotoAndPlay("close");
		this.next_btn.gotoAndPlay("close");
		this.lignes_mc.gotoAndPlay("close_lines");
	}
	this.frame_169 = function() {
		//this.lignes_mc.gotoAndPlay("close");
	}
	this.frame_211 = function() {
		// EVENT DE FIN D'ANIM
		var event = new Event('closed');
		this.dispatchEvent(event);		
		event = null;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(100).call(this.frame_100).wait(49).call(this.frame_149).wait(1).call(this.frame_150).wait(19).call(this.frame_169).wait(42).call(this.frame_211).wait(1));

	// BLOC_INFOS_TEXTE
	this.textes_mc = new lib.blocInfostextes();
	this.textes_mc.name = "textes_mc";
	this.textes_mc.setTransform(636.1,346);
	this.textes_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.textes_mc).wait(17).to({_off:false},0).wait(195));

	// masque_VISUEL
	this.visuel_mc = new lib.bloc_visuel();
	this.visuel_mc.name = "visuel_mc";
	this.visuel_mc.setTransform(640.05,365);
	this.visuel_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.visuel_mc).wait(17).to({_off:false},0).wait(195));

	// CTA_NEXT
	this.next_btn = new lib.bloc_NEXT_btn();
	this.next_btn.name = "next_btn";
	this.next_btn.setTransform(753.2,786.9,1,1,0,0,0,-340.5,403.9);
	this.next_btn._off = true;

	this.timeline.addTween(cjs.Tween.get(this.next_btn).wait(89).to({_off:false},0).wait(123));

	// CTA_DETAIL
	this.cta_mc = new lib.bloc_CTA();
	this.cta_mc.name = "cta_mc";
	this.cta_mc.setTransform(299.5,787,1,1,0,0,0,-340.5,403.9);
	this.cta_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.cta_mc).wait(99).to({_off:false},0).to({_off:true},74).wait(39));

	// VIDEO
	this.video_mc = new lib.bloc_video();
	this.video_mc.name = "video_mc";
	this.video_mc.setTransform(866.7,1147.15,1,1,0,0,0,227.5,764.9);
	this.video_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.video_mc).wait(94).to({_off:false},0).to({_off:true},79).wait(39));

	// lignes
	this.lignes_mc = new lib.lignes_all();
	this.lignes_mc.name = "lignes_mc";
	this.lignes_mc.setTransform(640,384.3,1,1,0,0,0,0,406.3);

	this.timeline.addTween(cjs.Tween.get(this.lignes_mc).wait(212));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(825.5,361.5,269.0999999999999,1101.8);
// library properties:
lib.properties = {
	id: 'C37355BB9438744C85620EF448CFA81B',
	width: 1280,
	height: 768,
	fps: 60,
	color: "#000000",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.StageGL();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['C37355BB9438744C85620EF448CFA81B'] = {
	getStage: function() { return exportRoot.stage; },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


an.makeResponsive = function(isResp, respDim, isScale, scaleType, domContainers) {		
	var lastW, lastH, lastS=1;		
	window.addEventListener('resize', resizeCanvas);		
	resizeCanvas();		
	function resizeCanvas() {			
		var w = lib.properties.width, h = lib.properties.height;			
		var iw = window.innerWidth, ih=window.innerHeight;			
		var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;			
		if(isResp) {                
			if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {                    
				sRatio = lastS;                
			}				
			else if(!isScale) {					
				if(iw<w || ih<h)						
					sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==1) {					
				sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==2) {					
				sRatio = Math.max(xRatio, yRatio);				
			}			
		}			
		domContainers[0].width = w * pRatio * sRatio;			
		domContainers[0].height = h * pRatio * sRatio;			
		domContainers.forEach(function(container) {				
			container.style.width = w * sRatio + 'px';				
			container.style.height = h * sRatio + 'px';			
		});			
		stage.scaleX = pRatio*sRatio;			
		stage.scaleY = pRatio*sRatio;			
		lastW = iw; lastH = ih; lastS = sRatio;            
		stage.tickOnUpdate = false;            
		stage.update();            
		stage.tickOnUpdate = true;		
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;