(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.WRC_cloud = function() {
	this.initialize(img.WRC_cloud);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,612,352);


(lib.WRC_route = function() {
	this.initialize(img.WRC_route);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1024,500);


(lib.WRC_voiture = function() {
	this.initialize(img.WRC_voiture);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,292,254);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.WRC_voiture_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.WRC_voiture();
	this.instance.parent = this;
	this.instance.setTransform(-146,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.WRC_voiture_1, new cjs.Rectangle(-146,0,292,254), null);


(lib.WRC_cloud_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.WRC_cloud();
	this.instance.parent = this;
	this.instance.setTransform(-306,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.WRC_cloud_1, new cjs.Rectangle(-306,0,612,352), null);


(lib.video = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.008)").s().p("EgtTBLAMAAAiV/MBanAAAMAAACV/g");
	this.shape.setTransform(290,479.975);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.video, new cjs.Rectangle(0,0,580,960), null);


(lib.txt_hashtag = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgiAmIAJgRQAOAKALgBQAFAAAFgDQAEgDAAgGQAAgGgFgEQgFgDgIAAQgEAAgIADIAAgQIARgVIgeAAIAAgTIA7AAIAAAPIgTAXQALABAHAJQAGAGAAALQAAAPgLAJQgKAJgPAAQgQAAgRgMg");
	this.shape.setTransform(94.7,73.85);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AghAKIAAgTIBDggIAAAVIgsAUIAsAVIAAAVg");
	this.shape_1.setTransform(86.275,73.175);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgmAxIAAhhIAmAAQAJAAAHACQAIADADAEQAGAIABAJQAAAMgIAFIgDACIgEACQAJABAGAGQAFAHAAAJQAAAKgHAIQgIAJgUAAgAgQAfIAPAAQAIgBAFgCQAEgCAAgHQAAgGgEgDQgGgCgJAAIgNAAgAgQgIIAKAAQAIAAAFgCQAEgCAAgHQAAgGgEgCQgEgCgIgBIgLAAg");
	this.shape_2.setTransform(74.05,73.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBFAAIAAATIgvAAIAAAVIArAAIAAASIgrAAIAAAUIAxAAIAAATg");
	this.shape_3.setTransform(64.6,73.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgjAkQgPgPAAgVQAAgUAPgPQAOgPAVAAQAWAAAPAPQAOAPAAAUQAAAVgOAPQgPAPgWAAQgVAAgOgPgAgTgWQgJAKAAAMQAAANAJAJQAIAJALAAQAMAAAJgJQAIgJAAgNQAAgMgIgKQgJgIgMgBQgLABgIAIg");
	this.shape_4.setTransform(54.025,73.75);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgfAxIAAhhIAWAAIAABOIApAAIAAATg");
	this.shape_5.setTransform(44.675,73.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAXAxIgtg9IAAA9IgWAAIAAhhIAUAAIAvA+IAAg+IAWAAIAABhg");
	this.shape_6.setTransform(30.875,73.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBFAAIAAATIgvAAIAAAVIArAAIAAASIgrAAIAAAUIAxAAIAAATg");
	this.shape_7.setTransform(20.8,73.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgKAxIAAhhIAVAAIAABhg");
	this.shape_8.setTransform(13.7,73.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgKAxIAAhOIgbAAIAAgTIBLAAIAAATIgbAAIAABOg");
	this.shape_9.setTransform(7.15,73.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgmAiIANgQQAQANANAAQAGAAADgDQADgCAAgFQAAgDgDgDQgEgDgKgDQgRgDgIgGQgIgGAAgOQAAgOALgIQAKgIANAAQAKAAAKAEQAKADAHAHIgLAQQgNgLgNAAQgFAAgDADQgDADAAAEQAAAEAEADQAEACANAEQAOADAHAGQAIAHAAANQAAANgKAJQgKAHgQABQgWAAgTgRg");
	this.shape_10.setTransform(-1.675,73.75);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AAeAxIgKgVIgoAAIgJAVIgXAAIAqhhIAVAAIAqBhgAgLAIIAXAAIgMgbg");
	this.shape_11.setTransform(-11.275,73.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgmAxIAAhhIAmAAQAJAAAHACQAIADADAEQAHAIgBAJQAAAMgGAFIgEACIgDACQAIABAGAGQAFAHAAAJQAAAKgHAIQgIAJgUAAgAgQAfIAPAAQAIgBAFgCQAFgCgBgHQAAgGgEgDQgGgCgJAAIgNAAgAgQgIIAKAAQAJAAAEgCQAEgCAAgHQAAgGgEgCQgEgCgIgBIgLAAg");
	this.shape_12.setTransform(-21.05,73.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBFAAIAAATIgvAAIAAAVIArAAIAAASIgrAAIAAAUIAxAAIAAATg");
	this.shape_13.setTransform(-30.5,73.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgmAiIANgQQAQANANAAQAGAAADgDQADgCAAgFQAAgDgDgDQgEgDgKgDQgRgDgIgGQgIgGAAgOQAAgOALgIQAKgIANAAQAKAAAKAEQAKADAHAHIgLAQQgNgLgNAAQgFAAgDADQgDADAAAEQAAAEAEADQAEACANAEQAOADAHAGQAIAHAAANQAAANgKAJQgKAHgQABQgWAAgTgRg");
	this.shape_14.setTransform(-39.875,73.75);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgUIANAAIADgQIgPAAIADgUIAPAAIAEgWIAUAAIgDAWIAMAAIADgWIAWAAIgEAWIAOAAIgDAUIgPAAIgDAQIARAAIgEAUIgQAAIgDAVgAgHAJIALAAIAEgQIgMAAg");
	this.shape_15.setTransform(-49.475,73.7);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBFAAIAAATIgvAAIAAAVIArAAIAAASIgrAAIAAAUIAxAAIAAATg");
	this.shape_16.setTransform(44.55,52.75);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgIAxIgnhhIAXAAIAYA9IAZg9IAYAAIgoBhg");
	this.shape_17.setTransform(34.9,52.75);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgKAxIAAhhIAVAAIAABhg");
	this.shape_18.setTransform(27.8,52.75);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgmAiIANgQQAQAOANAAQAGgBADgDQADgCAAgFQAAgEgDgCQgEgDgKgDQgRgDgIgGQgIgGAAgOQAAgPALgHQAKgIANAAQAKAAAKAEQAKADAHAHIgLAQQgNgKgNAAQgFAAgDACQgDADAAAEQAAAEAEADQAEACANAEQAOADAHAGQAIAHAAANQAAANgKAJQgKAHgQABQgWAAgTgRg");
	this.shape_19.setTransform(20.925,52.7);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AAXAxIgtg9IAAA9IgWAAIAAhhIAUAAIAvA+IAAg+IAWAAIAABhg");
	this.shape_20.setTransform(10.825,52.75);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgjAkQgPgPAAgVQAAgUAPgPQAOgPAVAAQAWAAAPAPQAOAPAAAUQAAAVgOAPQgPAPgWAAQgVAAgOgPgAgTgVQgJAJAAAMQAAANAJAKQAIAIALABQAMgBAJgIQAIgKAAgNQAAgMgIgJQgJgJgMAAQgLAAgIAJg");
	this.shape_21.setTransform(-0.675,52.7);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgmAxIAAhhIAiAAQAWAAAKAJQALAIAAATQAAARgLAIQgKAJgVgBIgNAAIAAAcgAgQADIAPAAQAJAAAEgEQAFgEAAgJQgBgIgFgDQgFgEgKAAIgMAAg");
	this.shape_22.setTransform(-10.85,52.75);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgmAiIANgQQAQAOANAAQAGgBADgDQADgCAAgFQAAgEgDgCQgEgDgKgDQgRgDgIgGQgIgGAAgOQAAgPALgHQAKgIANAAQAKAAAKAEQAKADAHAHIgLAQQgNgKgNAAQgFAAgDACQgDADAAAEQAAAEAEADQAEACANAEQAOADAHAGQAIAHAAANQAAANgKAJQgKAHgQABQgWAAgTgRg");
	this.shape_23.setTransform(-20.525,52.7);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBFAAIAAATIgvAAIAAAVIArAAIAAASIgrAAIAAAUIAxAAIAAATg");
	this.shape_24.setTransform(-29.25,52.75);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AAQAxIgVgfIgPAAIAAAfIgWAAIAAhhIAlAAQAWAAAKAIQAKAIAAARQAAAWgTAHIAZAjgAgUAAIAQAAQAKAAAEgEQAEgEAAgHQAAgIgEgDQgEgDgKAAIgQAAg");
	this.shape_25.setTransform(-38.775,52.75);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgTIANAAIADgRIgPAAIADgUIAPAAIAEgWIAUAAIgDAWIAMAAIADgWIAWAAIgEAWIAOAAIgDAUIgPAAIgDARIARAAIgEATIgQAAIgDAVgAgHAKIALAAIAEgRIgMAAg");
	this.shape_26.setTransform(-49.475,52.65);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBFAAIAAATIgvAAIAAAVIArAAIAAASIgrAAIAAATIAxAAIAAAUg");
	this.shape_27.setTransform(7.65,31.7);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AAhAxIAAg9IgbA1IgMAAIgag1IAAA9IgWAAIAAhhIAeAAIAYA1IAZg1IAeAAIAABhg");
	this.shape_28.setTransform(-3.825,31.7);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AAeAxIgKgVIgoAAIgJAVIgXAAIAqhhIAVAAIAqBhgAgLAJIAXAAIgMgcg");
	this.shape_29.setTransform(-15.725,31.7);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AAQAxIgVgfIgPAAIAAAfIgWAAIAAhhIAlAAQAWAAAKAIQAKAHAAASQAAAWgTAHIAZAjgAgUAAIAQAAQAKAAAEgEQAEgEAAgHQAAgIgEgDQgEgDgKAAIgQAAg");
	this.shape_30.setTransform(-25.675,31.7);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AggAxIAAhhIBBAAIAAATIgsAAIAAAWIAqAAIAAASIgqAAIAAAmg");
	this.shape_31.setTransform(-35.1,31.7);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AgKAxIAAhhIAVAAIAABhg");
	this.shape_32.setTransform(-41.95,31.7);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgTIANAAIADgRIgPAAIADgTIAPAAIAEgXIAUAAIgDAXIAMAAIADgXIAWAAIgEAXIAOAAIgDATIgPAAIgDARIARAAIgEATIgQAAIgDAVgAgHAKIALAAIAEgRIgMAAg");
	this.shape_33.setTransform(-49.475,31.6);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AgKAxIAAhhIAVAAIAABhg");
	this.shape_34.setTransform(-22,10.65);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AgmAxIAAhhIAiAAQAWAAALAJQAKAJAAASQAAARgLAIQgLAJgUgBIgNAAIAAAcgAgQADIAOAAQALAAADgDQAEgFAAgJQABgIgGgDQgFgEgKAAIgMAAg");
	this.shape_35.setTransform(-28.7,10.65);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AAeAxIgKgVIgoAAIgJAVIgXAAIAqhhIAVAAIAqBhgAgLAJIAXAAIgMgcg");
	this.shape_36.setTransform(-39.025,10.65);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgTIANAAIADgRIgPAAIADgTIAPAAIAEgXIAUAAIgDAXIAMAAIADgXIAWAAIgEAXIAOAAIgDATIgPAAIgDARIARAAIgEATIgQAAIgDAVgAgHAKIALAAIAEgRIgMAAg");
	this.shape_37.setTransform(-49.475,10.55);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.txt_hashtag, new cjs.Rectangle(-56.7,0,157.9,84.2), null);


(lib.masqueTexte_1_ligne = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("EgicAEJIAAoRMBE5AAAIAAIRg");
	this.shape.setTransform(0,24.55);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.masqueTexte_1_ligne, new cjs.Rectangle(-220.5,-1.9,441,53), null);


(lib.masque_generique = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#004FA0").s().p("AsQCqIAAlTIYhAAIAAFTg");
	this.shape.setTransform(78.5,17);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.masque_generique, new cjs.Rectangle(0,0,157,34), null);


(lib.ligne = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(49));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#333333").ss(1,1,1).p("EAAAg/bMAAAB+3");
	this.shape.setTransform(0,406);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#333333").ss(1,1,1).p("EAA/A/cQhDv0ggwoQgdwxAExFQAEwXAhwHQAfufA4to");
	this.shape_1.setTransform(-6.0239,406);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#333333").ss(1,1,1).p("EABwA/cQh4vfg5w8Qg0wqAHxQQAHweA7wDQA5uxBjtQ");
	this.shape_2.setTransform(-10.6719,406);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#333333").ss(1,1,1).p("EACVA/cQigvPhLxLQhHwmAKxYQAJwjBPv/QBMvACEs9");
	this.shape_3.setTransform(-14.2371,406);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#333333").ss(1,1,1).p("EACwA/cQi+vDhZxWQhTwjAMxeQAKwnBev8QBavKCcsw");
	this.shape_4.setTransform(-16.895,406);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#333333").ss(1,1,1).p("EADEA/cQjTu7hjxdQhdwhANxhQAMwrBov6QBkvSCusm");
	this.shape_5.setTransform(-18.7801,406);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#333333").ss(1,1,1).p("EADSA/cQjiu1hqxjQhjwfAOxlQAMwsBvv5QBsvXC6sf");
	this.shape_6.setTransform(-20.0814,406);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#333333").ss(1,1,1).p("EADbA/cQjsuxhuxnQhnweAOxnQANwtB0v4QBwvaDCsb");
	this.shape_7.setTransform(-20.9145,406);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#333333").ss(1,1,1).p("EADgA/cQjyuuhxxpQhpwdAPxpQANwuB3v3QByvdDHsY");
	this.shape_8.setTransform(-21.4184,406);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#333333").ss(1,1,1).p("EADjA/cQj1uthyxqQhrwdAPxpQANwvB5v3QB0vdDJsX");
	this.shape_9.setTransform(-21.7172,406);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#333333").ss(1,1,1).p("EADkA/cQj2uthzxqQhswdAPxpQANwvB6v3QB1veDKsW");
	this.shape_10.setTransform(-21.8418,406);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#333333").ss(1,1,1).p("EADlA/cQj3ush0xrQhrwdAPxpQANwvB5v3QB2veDLsW");
	this.shape_11.setTransform(-21.8918,406);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#333333").ss(1,1,1).p("EADlA/cQj3ushzxrQhswdAPxpQANwvB6v3QB1veDLsW");
	this.shape_12.setTransform(-21.9165,406);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#333333").ss(1,1,1).p("EADlg/bQjLMWh1PeQh6P3gNQvQgPRpBsQdQBzRrD3Os");
	this.shape_13.setTransform(-21.9165,406);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#333333").ss(1,1,1).p("EADlA/cQj3ushzxrQhswdAPxpQANwvB5v3QB2veDLsW");
	this.shape_14.setTransform(-21.8915,406);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#333333").ss(1,1,1).p("EADlA/cQj3ushzxrQhswdAPxpQAOwvB5v3QB1veDLsW");
	this.shape_15.setTransform(-21.8665,406);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#333333").ss(1,1,1).p("EADkA/cQj2uthzxqQhrwdAPxpQANwvB5v3QB1veDKsW");
	this.shape_16.setTransform(-21.7865,406);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#333333").ss(1,1,1).p("EADjA/cQj0uthzxqQhqwdAOxpQAOwvB4v2QB0veDJsX");
	this.shape_17.setTransform(-21.6619,406);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#333333").ss(1,1,1).p("EADhA/cQjyuuhyxpQhpwdAOxpQANwuB4v3QBzvdDHsY");
	this.shape_18.setTransform(-21.4626,406);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#333333").ss(1,1,1).p("EADeA/cQjvuvhwxoQhoweAOxoQANwuB2v3QBxvcDFsZ");
	this.shape_19.setTransform(-21.2133,406);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#333333").ss(1,1,1).p("EADaA/cQjruxhuxnQhnweAPxmQAMwuB0v4QBwvaDBsb");
	this.shape_20.setTransform(-20.8645,406);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#333333").ss(1,1,1).p("EADWA/cQjnuzhrxlQhlweAOxmQANwsBxv5QBtvZC+sd");
	this.shape_21.setTransform(-20.4106,406);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#333333").ss(1,1,1).p("EADQA/cQjgu2hpxiQhiwfAOxlQAMwsBuv4QBrvXC4sg");
	this.shape_22.setTransform(-19.8817,406);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#333333").ss(1,1,1).p("EADJA/cQjZu4hlxgQhewgANxjQAMwrBqv6QBnvTCysk");
	this.shape_23.setTransform(-19.1786,406);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#333333").ss(1,1,1).p("EADAA/cQjPu8hhxcQhawhAMxhQAMwqBlv7QBjvQCqso");
	this.shape_24.setTransform(-18.3758,406);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#333333").ss(1,1,1).p("EAC2A/cQjEvAhcxZQhWwiAMxfQALwoBhv8QBdvMChst");
	this.shape_25.setTransform(-17.4235,406);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#333333").ss(1,1,1).p("EACqA/cQi3vFhWxUQhRwjAMxdQAKwnBav8QBXvICXsz");
	this.shape_26.setTransform(-16.2969,406);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#333333").ss(1,1,1).p("EACdA/cQipvLhPxPQhKwkAKxaQAKwlBTv+QBQvDCLs5");
	this.shape_27.setTransform(-15.0095,406);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#333333").ss(1,1,1).p("EACOA/cQiYvShIxIQhCwnAJxWQAIwiBLwAQBJu9B9tB");
	this.shape_28.setTransform(-13.5536,406);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#333333").ss(1,1,1).p("EAB8A/cQiFvZg/xCQg6woAIxSQAHwhBCwBQA/u2ButK");
	this.shape_29.setTransform(-11.8731,406);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#333333").ss(1,1,1).p("EABpA/cQhxvig1w5QgxwrAHxPQAGwdA3wDQA2uvBdtT");
	this.shape_30.setTransform(-10.0434,406);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#333333").ss(1,1,1).p("EABYA/cQhevpgswyQgqwuAGxKQAFwbAvwFQAsuoBOtc");
	this.shape_31.setTransform(-8.3629,406);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#333333").ss(1,1,1).p("EABIA/cQhNvwgkwsQgiwvAFxHQAEwYAmwHQAkujBAtj");
	this.shape_32.setTransform(-6.9071,406);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#333333").ss(1,1,1).p("EAA7A/cQg/v2gdwmQgcwxAExEQADwXAgwIQAdudA0tq");
	this.shape_33.setTransform(-5.6198,406);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#333333").ss(1,1,1).p("EAAvA/cQgyv7gYwhQgWwzADxBQADwVAZwJQAXuZAqtw");
	this.shape_34.setTransform(-4.493,406);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#333333").ss(1,1,1).p("EAAlA/cQgnv/gTweQgRwzACxAQACwTAUwKQASuVAht1");
	this.shape_35.setTransform(-3.5406,406);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#333333").ss(1,1,1).p("EAAcA/cQgdwDgPwaQgNw1ACw9QABwSAQwLQANuSAZt5");
	this.shape_36.setTransform(-2.7379,406);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#333333").ss(1,1,1).p("EAAVA/cQgWwGgLwXQgKw2ACw7QABwSALwMQAKuPATt8");
	this.shape_37.setTransform(-2.0348,406);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#333333").ss(1,1,1).p("EAAQA/cQgQwIgIwVQgHw3ABw6QABwRAIwMQAHuNAOt/");
	this.shape_38.setTransform(-1.5059,406);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#333333").ss(1,1,1).p("EAALA/cQgLwLgFwSQgFw3AAw6QABwQAGwMQAFuMAJuB");
	this.shape_39.setTransform(-1.0523,406);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#333333").ss(1,1,1).p("EAAIA/cQgIwMgDwSQgEw3ABw5QAAwPAEwNQADuKAHuD");
	this.shape_40.setTransform(-0.7031,406);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#333333").ss(1,1,1).p("EAAFA/cQgFwNgBwRQgDw3ABw5QAAwOACwOQACuJAEuE");
	this.shape_41.setTransform(-0.4542,406);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#333333").ss(1,1,1).p("EAADA/cQgDwOAAwQQgCw3ABw4QAAwOABwPQAAuIADuF");
	this.shape_42.setTransform(-0.2562,406);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#333333").ss(1,1,1).p("EAABA/cQgBwOAAwQQgBw4ABw3QAAwOAAwOQAAuIABuG");
	this.shape_43.setTransform(-0.1333,406);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#333333").ss(1,1,1).p("EAAAA/cQAAwPAAwPQAAw4AAw3QAAwOAAwOQAAuIAAuG");
	this.shape_44.setTransform(-0.05,406);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#333333").ss(1,1,1).p("EAAAA/cQAAwPAAwPQAAw4AAw3QAAwOAAwOQAAuHAAuH");
	this.shape_45.setTransform(-0.025,406);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45,p:{x:-0.025}}]},1).to({state:[{t:this.shape_45,p:{x:0}}]},1).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-45.8,-1,47.8,814);


(lib.lettres_FAT = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFF000").s().p("ACnFeIlMm2IAAG2IidAAIAAq7ICSAAIFXHBIAAnBICcAAIAAK7g");
	this.shape.setTransform(268.7,187.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFF000").s().p("AhNFeIAAq7ICbAAIAAK7g");
	this.shape_1.setTransform(199.45,187.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFF000").s().p("AjiFeIAAq7ICcAAIAAIvIEpAAIAACMg");
	this.shape_2.setTransform(107.15,187.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFF000").s().p("AkBFeIAAq7IH3AAIAACMIlaAAIAACQIE3AAIAACDIk3AAIAACSIFmAAIAACKg");
	this.shape_3.setTransform(32.8,187.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFF000").s().p("AixBDIAAiFIFjAAIAACFg");
	this.shape_4.setTransform(-38.05,192.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFF000").s().p("ACLFdIAAkSIkVAAIAAESIicAAIAAq5ICcAAIAAEjIEVAAIAAkjICcAAIAAK5g");
	this.shape_5.setTransform(174.55,63.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFF000").s().p("AjeEEQhohngBibQAAibBrhoQBrhnCYAAQCtAABzCCIhhBvQhJhdhuAAQhYAAg+A6Qg/A5AABhQAABiA8A6QA6A8BUAAQByAABKhbIBkBmQh3CGiiAAQihAAhohlg");
	this.shape_6.setTransform(88.4,63.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFF000").s().p("AhNFdIAAq5ICbAAIAAK5g");
	this.shape_7.setTransform(22.85,63.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFF000").s().p("ADrFdIAAm3Ii9F+IhcAAIi8l+IAAG3IicAAIAAq5IDTAAICzF+IC1l+IDSAAIAAK5g");
	this.shape_8.setTransform(-53.25,63.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.lettres_FAT, new cjs.Rectangle(-102.9,0,426.70000000000005,249.8), null);


(lib.hastaggraphicbranche = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AjQAuIAShbIDlAAICqAAIgSBbg");
	this.shape.setTransform(0,4.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.hastaggraphicbranche, new cjs.Rectangle(-20.8,0,41.7,9.2), null);


(lib.Btn_cliquable_area = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFF000").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape.setTransform(0,30);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Btn_cliquable_area, new cjs.Rectangle(-113,0,226,60), null);


(lib.project_illustration_placeHolder = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_100 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(100).call(this.frame_100).wait(11));

	// Layer_7 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("EAvbAo0QAJgGtShiQtthljDgDQi9gDj1ggQh9gRhqgTIpWgzQpOgygugGQgtgHg/gDQg3gCgJgCQgUgFlaAUQlyAUjdAWQlDAfpIg1QADgCjPo9QjOo8ADgDQADgCFtvQQFtvPACgDQADgCRNjfQRNjeADgDQADgCbREgQbSEfAEgCQAFgDhdaOQhcaLgGAAIAAAAg");
	mask.setTransform(-152.0361,261.1754);

	// WRC_voiture.png
	this.instance = new lib.WRC_voiture_1();
	this.instance.parent = this;
	this.instance.setTransform(64.4,585,0.64,0.64,0,0,0,-0.1,127);
	this.instance._off = true;

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(23).to({_off:false},0).wait(1).to({regX:0,scaleX:0.7349,scaleY:0.7349,x:21.9,y:524.35},0).wait(1).to({scaleX:0.7759,scaleY:0.7759,x:3.5,y:498.15},0).wait(1).to({scaleX:0.8044,scaleY:0.8044,x:-9.2,y:479.9},0).wait(1).to({scaleX:0.8265,scaleY:0.8265,x:-19.1,y:465.8},0).wait(1).to({scaleX:0.8444,scaleY:0.8444,x:-27.15,y:454.35},0).wait(1).to({scaleX:0.8596,scaleY:0.8596,x:-33.95,y:444.65},0).wait(1).to({scaleX:0.8725,scaleY:0.8725,x:-39.75,y:436.35},0).wait(1).to({scaleX:0.8839,scaleY:0.8839,x:-44.85,y:429.15},0).wait(1).to({scaleX:0.8939,scaleY:0.8939,x:-49.3,y:422.7},0).wait(1).to({scaleX:0.9028,scaleY:0.9028,x:-53.3,y:417.05},0).wait(1).to({scaleX:0.9107,scaleY:0.9107,x:-56.85,y:411.95},0).wait(1).to({scaleX:0.9179,scaleY:0.9179,x:-60.1,y:407.4},0).wait(1).to({scaleX:0.9244,scaleY:0.9244,x:-63,y:403.2},0).wait(1).to({scaleX:0.9304,scaleY:0.9304,x:-65.65,y:399.4},0).wait(1).to({scaleX:0.9358,scaleY:0.9358,x:-68.1,y:396},0).wait(1).to({scaleX:0.9407,scaleY:0.9407,x:-70.3,y:392.8},0).wait(1).to({scaleX:0.9453,scaleY:0.9453,x:-72.35,y:389.9},0).wait(1).to({scaleX:0.9494,scaleY:0.9494,x:-74.2,y:387.25},0).wait(1).to({scaleX:0.9533,scaleY:0.9533,x:-75.95,y:384.75},0).wait(1).to({scaleX:0.9568,scaleY:0.9568,x:-77.55,y:382.5},0).wait(1).to({scaleX:0.9601,scaleY:0.9601,x:-79,y:380.4},0).wait(1).to({scaleX:0.9632,scaleY:0.9632,x:-80.35,y:378.45},0).wait(1).to({scaleX:0.966,scaleY:0.966,x:-81.65,y:376.7},0).wait(1).to({scaleX:0.9686,scaleY:0.9686,x:-82.8,y:375},0).wait(1).to({scaleX:0.971,scaleY:0.971,x:-83.9,y:373.45},0).wait(1).to({scaleX:0.9733,scaleY:0.9733,x:-84.9,y:372},0).wait(1).to({scaleX:0.9753,scaleY:0.9753,x:-85.8,y:370.65},0).wait(1).to({scaleX:0.9773,scaleY:0.9773,x:-86.7,y:369.45},0).wait(1).to({scaleX:0.9791,scaleY:0.9791,x:-87.5,y:368.3},0).wait(1).to({scaleX:0.9807,scaleY:0.9807,x:-88.25,y:367.25},0).wait(1).to({scaleX:0.9823,scaleY:0.9823,x:-88.95,y:366.25},0).wait(1).to({scaleX:0.9837,scaleY:0.9837,x:-89.6,y:365.35},0).wait(1).to({scaleX:0.9851,scaleY:0.9851,x:-90.2,y:364.45},0).wait(1).to({scaleX:0.9863,scaleY:0.9863,x:-90.75,y:363.65},0).wait(1).to({scaleX:0.9875,scaleY:0.9875,x:-91.25,y:362.9},0).wait(1).to({scaleX:0.9886,scaleY:0.9886,x:-91.75,y:362.25},0).wait(1).to({scaleX:0.9896,scaleY:0.9896,x:-92.2,y:361.6},0).wait(1).to({scaleX:0.9905,scaleY:0.9905,x:-92.6,y:361},0).wait(1).to({scaleX:0.9913,scaleY:0.9913,x:-93,y:360.45},0).wait(1).to({scaleX:0.9921,scaleY:0.9921,x:-93.35,y:359.95},0).wait(1).to({scaleX:0.9929,scaleY:0.9929,x:-93.65,y:359.5},0).wait(1).to({scaleX:0.9935,scaleY:0.9935,x:-94,y:359.1},0).wait(1).to({scaleX:0.9942,scaleY:0.9942,x:-94.25,y:358.65},0).wait(1).to({scaleX:0.9947,scaleY:0.9947,x:-94.5,y:358.3},0).wait(1).to({scaleX:0.9953,scaleY:0.9953,x:-94.75,y:357.95},0).wait(1).to({scaleX:0.9958,scaleY:0.9958,x:-94.95,y:357.6},0).wait(1).to({scaleX:0.9962,scaleY:0.9962,x:-95.2,y:357.35},0).wait(1).to({scaleX:0.9966,scaleY:0.9966,x:-95.35,y:357.05},0).wait(1).to({scaleX:0.997,scaleY:0.997,x:-95.55,y:356.85},0).wait(1).to({scaleX:0.9973,scaleY:0.9973,x:-95.7,y:356.6},0).wait(1).to({scaleX:0.9977,scaleY:0.9977,x:-95.8,y:356.4},0).wait(1).to({scaleX:0.9979,scaleY:0.9979,x:-95.95,y:356.25},0).wait(1).to({scaleX:0.9982,scaleY:0.9982,x:-96.05,y:356.05},0).wait(1).to({scaleX:0.9984,scaleY:0.9984,x:-96.15,y:355.95},0).wait(1).to({scaleX:0.9986,scaleY:0.9986,x:-96.25,y:355.8},0).wait(1).to({scaleX:0.9988,scaleY:0.9988,x:-96.35,y:355.65},0).wait(1).to({scaleX:0.999,scaleY:0.999,x:-96.45,y:355.55},0).wait(1).to({scaleX:0.9992,scaleY:0.9992,x:-96.5,y:355.45},0).wait(1).to({scaleX:0.9993,scaleY:0.9993,x:-96.55,y:355.35},0).wait(1).to({scaleX:0.9994,scaleY:0.9994,x:-96.6,y:355.3},0).wait(1).to({scaleX:0.9995,scaleY:0.9995,x:-96.65,y:355.25},0).wait(1).to({scaleX:0.9996,scaleY:0.9996,x:-96.7,y:355.2},0).wait(1).to({scaleX:0.9997,scaleY:0.9997,x:-96.75,y:355.1},0).wait(1).to({y:355.05},0).wait(1).to({scaleX:0.9998,scaleY:0.9998,x:-96.8},0).wait(2).to({scaleX:0.9999,scaleY:0.9999,y:355},0).wait(1).to({x:-96.85},0).wait(1).to({y:354.95},0).wait(1).to({scaleX:1,scaleY:1},0).wait(6).to({x:-97,y:355},0).to({_off:true},3).wait(9));

	// WRC_route.png
	this.instance_1 = new lib.WRC_route();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-674,186);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({_off:true},102).wait(9));

	// WRC_cloud.png
	this.instance_2 = new lib.WRC_cloud_1();
	this.instance_2.parent = this;
	this.instance_2.setTransform(171.1,503.3,0.6,0.6,20.4735,0,0,0.1,176.2);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(23).to({_off:false},0).wait(1).to({regX:0,regY:176,scaleX:0.6534,scaleY:0.6534,rotation:17.7398,x:142.1,y:487.5},0).wait(1).to({scaleX:0.6946,scaleY:0.6946,rotation:15.6323,x:119.75,y:475.35},0).wait(1).to({scaleX:0.7278,scaleY:0.7278,rotation:13.9315,x:101.7,y:465.6},0).wait(1).to({scaleX:0.7554,scaleY:0.7554,rotation:12.5171,x:86.7,y:457.55},0).wait(1).to({scaleX:0.7789,scaleY:0.7789,rotation:11.3158,x:73.95,y:450.6},0).wait(1).to({scaleX:0.7992,scaleY:0.7992,rotation:10.2792,x:62.95,y:444.7},0).wait(1).to({scaleX:0.8169,scaleY:0.8169,rotation:9.3737,x:53.4,y:439.5},0).wait(1).to({scaleX:0.8325,scaleY:0.8325,rotation:8.5751,x:44.9,y:434.9},0).wait(1).to({scaleX:0.8463,scaleY:0.8463,rotation:7.8649,x:37.35,y:430.85},0).wait(1).to({scaleX:0.8587,scaleY:0.8587,rotation:7.2293,x:30.65,y:427.25},0).wait(1).to({scaleX:0.8699,scaleY:0.8699,rotation:6.6572,x:24.55,y:423.9},0).wait(1).to({scaleX:0.88,scaleY:0.88,rotation:6.1399,x:19.05,y:420.95},0).wait(1).to({scaleX:0.8892,scaleY:0.8892,rotation:5.6701,x:14.1,y:418.3},0).wait(1).to({scaleX:0.8976,scaleY:0.8976,rotation:5.242,x:9.5,y:415.8},0).wait(1).to({scaleX:0.9052,scaleY:0.9052,rotation:4.8506,x:5.4,y:413.6},0).wait(1).to({scaleX:0.9122,scaleY:0.9122,rotation:4.4919,x:1.6,y:411.5},0).wait(1).to({scaleX:0.9187,scaleY:0.9187,rotation:4.1623,x:-1.95,y:409.65},0).wait(1).to({scaleX:0.9246,scaleY:0.9246,rotation:3.8588,x:-5.15,y:407.9},0).wait(1).to({scaleX:0.9301,scaleY:0.9301,rotation:3.5789,x:-8.1,y:406.25},0).wait(1).to({scaleX:0.9351,scaleY:0.9351,rotation:3.3202,x:-10.8,y:404.8},0).wait(1).to({scaleX:0.9398,scaleY:0.9398,rotation:3.0808,x:-13.35,y:403.4},0).wait(1).to({scaleX:0.9441,scaleY:0.9441,rotation:2.859,x:-15.7,y:402.15},0).wait(1).to({scaleX:0.9482,scaleY:0.9482,rotation:2.6532,x:-17.85,y:401},0).wait(1).to({scaleX:0.9519,scaleY:0.9519,rotation:2.4621,x:-19.9,y:399.9},0).wait(1).to({scaleX:0.9554,scaleY:0.9554,rotation:2.2844,x:-21.8,y:398.85},0).wait(1).to({scaleX:0.9586,scaleY:0.9586,rotation:2.1191,x:-23.55,y:397.95},0).wait(1).to({scaleX:0.9616,scaleY:0.9616,rotation:1.9653,x:-25.15,y:397.05},0).wait(1).to({scaleX:0.9644,scaleY:0.9644,rotation:1.822,x:-26.7,y:396.2},0).wait(1).to({scaleX:0.967,scaleY:0.967,rotation:1.6885,x:-28.1,y:395.45},0).wait(1).to({scaleX:0.9695,scaleY:0.9695,rotation:1.564,x:-29.45,y:394.75},0).wait(1).to({scaleX:0.9717,scaleY:0.9717,rotation:1.4479,x:-30.65,y:394.05},0).wait(1).to({scaleX:0.9738,scaleY:0.9738,rotation:1.3395,x:-31.8,y:393.45},0).wait(1).to({scaleX:0.9758,scaleY:0.9758,rotation:1.2385,x:-32.85,y:392.85},0).wait(1).to({scaleX:0.9777,scaleY:0.9777,rotation:1.1442,x:-33.9,y:392.35},0).wait(1).to({scaleX:0.9794,scaleY:0.9794,rotation:1.0562,x:-34.85,y:391.85},0).wait(1).to({scaleX:0.981,scaleY:0.981,rotation:0.974,x:-35.7,y:391.35},0).wait(1).to({scaleX:0.9825,scaleY:0.9825,rotation:0.8974,x:-36.5,y:390.95},0).wait(1).to({scaleX:0.9839,scaleY:0.9839,rotation:0.8259,x:-37.25,y:390.5},0).wait(1).to({scaleX:0.9852,scaleY:0.9852,rotation:0.7592,x:-37.95,y:390.1},0).wait(1).to({scaleX:0.9864,scaleY:0.9864,rotation:0.6971,x:-38.6,y:389.8},0).wait(1).to({scaleX:0.9875,scaleY:0.9875,rotation:0.6391,x:-39.25,y:389.45},0).wait(1).to({scaleX:0.9886,scaleY:0.9886,rotation:0.5852,x:-39.85,y:389.15},0).wait(1).to({scaleX:0.9896,scaleY:0.9896,rotation:0.5349,x:-40.3,y:388.85},0).wait(1).to({scaleX:0.9905,scaleY:0.9905,rotation:0.4881,x:-40.85,y:388.55},0).wait(1).to({scaleX:0.9913,scaleY:0.9913,rotation:0.4446,x:-41.3,y:388.3},0).wait(1).to({scaleX:0.9921,scaleY:0.9921,rotation:0.4042,x:-41.75,y:388.1},0).wait(1).to({scaleX:0.9929,scaleY:0.9929,rotation:0.3667,x:-42.1,y:387.9},0).wait(1).to({scaleX:0.9935,scaleY:0.9935,rotation:0.3319,x:-42.5,y:387.65},0).wait(1).to({scaleX:0.9942,scaleY:0.9942,rotation:0.2997,x:-42.85,y:387.5},0).wait(1).to({scaleX:0.9947,scaleY:0.9947,rotation:0.2699,x:-43.15,y:387.3},0).wait(1).to({scaleX:0.9953,scaleY:0.9953,rotation:0.2423,x:-43.45,y:387.15},0).wait(1).to({scaleX:0.9958,scaleY:0.9958,rotation:0.2169,x:-43.7,y:387},0).wait(1).to({scaleX:0.9962,scaleY:0.9962,rotation:0.1934,x:-44,y:386.9},0).wait(1).to({scaleX:0.9967,scaleY:0.9967,rotation:0.1719,x:-44.2,y:386.75},0).wait(1).to({scaleX:0.997,scaleY:0.997,rotation:0.1521,x:-44.4,y:386.6},0).wait(1).to({scaleX:0.9974,scaleY:0.9974,rotation:0.134,x:-44.6,y:386.55},0).wait(1).to({scaleX:0.9977,scaleY:0.9977,rotation:0.1175,x:-44.75,y:386.45},0).wait(1).to({scaleX:0.998,scaleY:0.998,rotation:0.1024,x:-44.9,y:386.35},0).wait(1).to({scaleX:0.9983,scaleY:0.9983,rotation:0.0887,x:-45.05,y:386.3},0).wait(1).to({scaleX:0.9985,scaleY:0.9985,rotation:0.0763,x:-45.25,y:386.25},0).wait(1).to({scaleX:0.9987,scaleY:0.9987,rotation:0.0651,x:-45.35,y:386.1},0).wait(1).to({scaleX:0.9989,scaleY:0.9989,rotation:0.0551,x:-45.4},0).wait(1).to({scaleX:0.9991,scaleY:0.9991,rotation:0.0461,x:-45.55,y:386.05},0).wait(1).to({scaleX:0.9993,scaleY:0.9993,rotation:0.0382,x:-45.6,y:385.95},0).wait(1).to({scaleX:0.9994,scaleY:0.9994,rotation:0.0311,x:-45.7},0).wait(1).to({scaleX:0.9995,scaleY:0.9995,rotation:0.025,x:-45.8,y:385.9},0).wait(1).to({scaleX:0.9996,scaleY:0.9996,rotation:0.0196},0).wait(1).to({scaleX:0.9997,scaleY:0.9997,rotation:0.0151,x:-45.85,y:385.85},0).wait(1).to({scaleX:0.9998,scaleY:0.9998,rotation:0.0112,x:-45.9},0).wait(1).to({rotation:0.008,y:385.8},0).wait(1).to({scaleX:0.9999,scaleY:0.9999,rotation:0.0054,x:-45.95},0).wait(1).to({rotation:0.0033,x:-46},0).wait(1).to({scaleX:1,scaleY:1,rotation:0.0018},0).wait(1).to({rotation:0.0008},0).wait(1).to({rotation:0.0002},0).wait(1).to({rotation:0,y:386},0).to({_off:true},3).wait(9));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-674,186,1054.1,500);


(lib.prject_illustration = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.project_illustration_placeHolder();
	this.instance.parent = this;
	this.instance.setTransform(52.2,-112.3,0.7,0.7,0,0,0,-15,-268.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(140));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-409.1,166.6,716.8,389.1);


(lib.masqueTexte = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// masqueTexte_1_ligne
	this.instance = new lib.masqueTexte_1_ligne();
	this.instance.parent = this;
	this.instance.setTransform(-218.8,191.5,0.0079,1,0,0,0,0,25.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(6).to({_off:false},0).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.0079,x:218.8},14).wait(1));

	// masqueTexte_1_ligne
	this.instance_1 = new lib.masqueTexte_1_ligne();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-218.8,136.1,0.0079,1,0,0,0,0,25.5);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.0079,x:218.8},14).to({_off:true},1).wait(2));

	// masqueTexte_1_ligne
	this.instance_2 = new lib.masqueTexte_1_ligne();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-218.8,80.8,0.0079,1,0,0,0,0,25.5);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(2).to({_off:false},0).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.0079,x:218.8},14).to({_off:true},1).wait(4));

	// masqueTexte_1_ligne
	this.instance_3 = new lib.masqueTexte_1_ligne();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-218.8,25.5,0.0079,1,0,0,0,0,25.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.0079,x:218.8},14).to({_off:true},1).wait(6));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-220.5,-1.9,441.1,219);


(lib.lignes_all = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{close:114,close_lines:157});

	// timeline functions:
	this.frame_113 = function() {
		this.stop();
	}
	this.frame_156 = function() {
		this.stop();
	}
	this.frame_174 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(113).call(this.frame_113).wait(43).call(this.frame_156).wait(18).call(this.frame_174).wait(1));

	// ligne
	this.instance = new lib.ligne();
	this.instance.parent = this;
	this.instance.setTransform(-454.05,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(114).to({x:-883},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:-454.05},0).wait(18));

	// ligne
	this.instance_1 = new lib.ligne();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-227.05,406.5,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(114).to({x:-883.2},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:-227.05},0).wait(18));

	// ligne
	this.instance_2 = new lib.ligne();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-0.05,406.5,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(114).to({x:-883.4},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:-0.05},0).wait(18));

	// ligne
	this.instance_3 = new lib.ligne();
	this.instance_3.parent = this;
	this.instance_3.setTransform(226.95,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(93).to({x:227,y:322},20,cjs.Ease.quartInOut).wait(1).to({y:323},0).to({x:-883.05,y:406},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:227,y:322},0).to({x:226.95,y:406},16,cjs.Ease.quartInOut).wait(2));

	// ligne
	this.instance_4 = new lib.ligne();
	this.instance_4.parent = this;
	this.instance_4.setTransform(454.1,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(114).to({x:-883.45},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:454.1},0).wait(18));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-884.4,-85,1339.5,898.5);


(lib.hashtag_graphic = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(64));

	// hastag graphic branche
	this.instance = new lib.hastaggraphicbranche();
	this.instance.parent = this;
	this.instance.setTransform(37.35,51.9,1.0844,0.9952,0,-78.3276,102.1252,0.1,4.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({x:6.45,y:50.05},17,cjs.Ease.quartOut).to({_off:true},5).wait(3).to({_off:false},0).to({_off:true},4).wait(2).to({_off:false,x:26.45},0).to({_off:true},5).wait(28));

	// hastag graphic branche
	this.instance_1 = new lib.hastaggraphicbranche();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-41.65,49.9,1.0844,0.9952,0,-78.3276,102.1252,0.1,4.6);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off:false},0).to({x:-7.4,y:49.8},18,cjs.Ease.quartOut).to({_off:true},4).wait(5).to({_off:false},0).to({_off:true},4).wait(31));

	// hastag graphic branche
	this.instance_2 = new lib.hastaggraphicbranche();
	this.instance_2.parent = this;
	this.instance_2.setTransform(2.7,21.7,1,1,0,0,0,0,4.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({x:-2.3,y:58.7},13,cjs.Ease.quartOut).to({_off:true},11).wait(3).to({_off:false,scaleX:2.3297,scaleY:1.0257,skewX:-12.8379,x:25.5},0).to({_off:true},4).wait(33));

	// hastag graphic branche
	this.instance_3 = new lib.hastaggraphicbranche();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-7.3,77.7,1,1,0,0,0,0,4.6);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({x:0.7,y:41.7},14,cjs.Ease.quartOut).to({_off:true},4).wait(7).to({_off:false,regX:0.1,scaleX:1.9808,scaleY:1.0147,skewX:-9.7728,x:19.9,y:49.7},0).to({_off:true},4).wait(4).to({_off:false,regX:0,scaleX:1,scaleY:1,skewX:0,x:0.7,y:41.7},0).to({_off:true},4).wait(21));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-50.9,0,126.1,82.3);


(lib.Btn_next = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{rollOver:55,rollOut:70});

	// timeline functions:
	this.frame_54 = function() {
		this.stop();
	}
	this.frame_69 = function() {
		this.stop();
	}
	this.frame_84 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(54).call(this.frame_54).wait(15).call(this.frame_69).wait(15).call(this.frame_84).wait(1));

	// ZONE
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.008)").s().p("EgjTAEsIAApXMBGnAAAIAAJXg");
	this.shape.setTransform(113,29.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(85));

	// txt
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgIAqIAAhDIgYAAIAAgQIBBAAIAAAQIgYAAIAABDg");
	this.shape_1.setTransform(161.05,29.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AASAqIgSgcIgQAcIgYAAIAdgqIgagpIAWAAIAPAZIAQgZIAWAAIgaApIAcAqg");
	this.shape_2.setTransform(145.2,29.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgeAqIAAhTIA8AAIAAARIgpAAIAAARIAkAAIAAAPIgkAAIAAASIAqAAIAAAQg");
	this.shape_3.setTransform(129.325,29.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAUAqIgngzIAAAzIgTAAIAAhTIASAAIAoA1IAAg1IATAAIAABTg");
	this.shape_4.setTransform(112.3,29.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgKAQIAEgOQgEgCAAgFQAAgEADgDQADgDAEAAQAFAAADADQADADAAAEQAAAEgDAEIgIANg");
	this.shape_5.setTransform(86.525,32.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAPAqIgYghIgIAKIAAAXIgTAAIAAhTIATAAIAAAjIAegjIAYAAIgiAlIAQAWIASAYg");
	this.shape_6.setTransform(76.975,29.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgeAfQgNgNAAgSQAAgSANgMQANgNARAAQASAAAOANQAMAMAAASQAAASgMANQgOAMgSAAQgRAAgNgMgAgRgSQgHAIAAAKQAAALAHAIQAIAIAJAAQAKAAAIgIQAHgIAAgLQAAgKgHgIQgIgIgKAAQgJAAgIAIg");
	this.shape_7.setTransform(59.3,29.05);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#333333").ss(1,1,1).p("EgjOAADMBGdgAF");
	this.shape_8.setTransform(113,-0.25);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(85));

	// Layer_2
	this.instance = new lib.Btn_cliquable_area();
	this.instance.parent = this;
	this.instance.setTransform(113,29.5,2,1,0,0,0,0,30);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("rgba(0,0,0,0.008)").s().p("EgjTAEsIAApXMBGnAAAIAAJXg");
	this.shape_9.setTransform(113,29.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance}]},3).to({state:[{t:this.instance}]},46).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.instance}]},5).to({state:[{t:this.instance}]},14).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},14).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({alpha:0.0117},46).to({_off:true},1).wait(5).to({_off:false},0).to({alpha:1},14).wait(1).to({alpha:0.0117},14).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-113.5,-1.5,453,61);


(lib.Btn_detail = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"rollOver":55,"rollOut":70});

	// timeline functions:
	this.frame_54 = function() {
		this.stop();
	}
	this.frame_69 = function() {
		this.stop();
	}
	this.frame_84 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(54).call(this.frame_54).wait(15).call(this.frame_69).wait(15).call(this.frame_84).wait(1));

	// ZONE
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.008)").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape.setTransform(0,29.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(85));

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgOAyIAAhkIAdAAIAABkg");
	this.shape_1.setTransform(-85.45,29.55);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgyAOIAAgcIBlAAIAAAcg");
	this.shape_2.setTransform(-85.45,29.55);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgaAqIAAhTIASAAIAABDIAjAAIAAAQg");
	this.shape_3.setTransform(38.425,29.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgIAqIAAhTIARAAIAABTg");
	this.shape_4.setTransform(24.675,29.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AAZAqIgHgSIgjAAIgHASIgUAAIAkhTIARAAIAkBTgAgKAIIAUAAIgKgYg");
	this.shape_5.setTransform(10.225,29.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgIAqIAAhDIgYAAIAAgQIBBAAIAAAQIgYAAIAABDg");
	this.shape_6.setTransform(-5.9,29.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgeAqIAAhTIA8AAIAAARIgpAAIAAARIAkAAIAAAPIgkAAIAAASIAqAAIAAAQg");
	this.shape_7.setTransform(-21.275,29.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AglAqIAAhTIAeAAQAVAAAMALQAMALAAATQAAATgMALQgMAMgXAAgAgTAaIANAAQAMAAAGgHQAHgGAAgNQAAgLgHgHQgGgGgOgBIgLAAg");
	this.shape_8.setTransform(-37.8,29.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#333333").ss(1,1,1).p("AxpAAMAjTAAA");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(85));

	// Layer_2
	this.instance = new lib.Btn_cliquable_area();
	this.instance.parent = this;
	this.instance.setTransform(0,29.5,1,1,0,0,0,0,30);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("rgba(0,0,0,0.008)").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape_10.setTransform(0,29.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance}]},3).to({state:[{t:this.instance}]},45).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.instance}]},6).to({state:[{t:this.instance}]},14).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},13).wait(2));
	this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({alpha:0.0117},45).to({_off:true},1).wait(6).to({_off:false},0).to({alpha:1},14).wait(1).to({alpha:0.0117},13).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-114,-1,228,60.5);


(lib.bloc_visuel = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":68});

	// timeline functions:
	this.frame_67 = function() {
		this.stop();
	}
	this.frame_104 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(67).call(this.frame_67).wait(37).call(this.frame_104).wait(1));

	// masque VISUEL (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("EgjsBDvMAAAiHdMBHZAAAMAAACHdg");
	mask.setTransform(-228.75,-19);

	// VISUEL
	this.instance = new lib.prject_illustration();
	this.instance.parent = this;
	this.instance.setTransform(242.55,-660.65,0.9999,0.9999,0,0,0,11.6,-308.4);
	this.instance._off = true;

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(26).to({_off:false},0).to({regX:11.7,regY:-308.3,scaleX:1,scaleY:1,x:-85.55,y:-660.55},33,cjs.Ease.quartOut).wait(15).to({rotation:-21.7456,x:-1861.9,y:-560.55},30,cjs.Ease.quartInOut).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2063.7,-192.7,2063.7,590.5999999999999);


(lib.bloc_video = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":41});

	// timeline functions:
	this.frame_40 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(40).call(this.frame_40).wait(17));

	// masque video (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_42 = new cjs.Graphics().p("Ega8A8AMAAAh3/MA7rAAAMAAAB3/g");
	var mask_graphics_43 = new cjs.Graphics().p("Ega8A8AMAAAh3/MA7qAAAMAAAB3/g");
	var mask_graphics_44 = new cjs.Graphics().p("Ega0A8AMAAAh3/MA7aAAAMAAAB3/g");
	var mask_graphics_45 = new cjs.Graphics().p("EgaSA8AMAAAh3/MA6VAAAMAAAB3/g");
	var mask_graphics_46 = new cjs.Graphics().p("EgY1A8AMAAAh3/MA3bAAAMAAAB3/g");
	var mask_graphics_47 = new cjs.Graphics().p("EgVyA8AMAAAh3/MAxSAAAMAAAB3/g");
	var mask_graphics_48 = new cjs.Graphics().p("EgQQA8AMAAAh3/MAmIAAAMAAAB3/g");
	var mask_graphics_49 = new cjs.Graphics().p("EgIMA8AMAAAh3/IV3AAMAAAB3/g");
	var mask_graphics_50 = new cjs.Graphics().p("EgCpA8AMAAAh3/IKsAAMAAAB3/g");
	var mask_graphics_51 = new cjs.Graphics().p("EAAZA8AMAAAh3/IEkAAMAAAB3/g");
	var mask_graphics_52 = new cjs.Graphics().p("EAB1A8AMAAAh3/IBqAAMAAAB3/g");
	var mask_graphics_53 = new cjs.Graphics().p("EACXA8AMAAAh3/IAlAAMAAAB3/g");
	var mask_graphics_54 = new cjs.Graphics().p("EACfA8AMAAAh3/IAVAAMAAAB3/g");
	var mask_graphics_55 = new cjs.Graphics().p("EACgA8AMAAAh3/IAUAAMAAAB3/g");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(42).to({graphics:mask_graphics_42,x:209.4828,y:0}).wait(1).to({graphics:mask_graphics_43,x:209.4292,y:0}).wait(1).to({graphics:mask_graphics_44,x:208.6245,y:0}).wait(1).to({graphics:mask_graphics_45,x:205.1378,y:0}).wait(1).to({graphics:mask_graphics_46,x:195.7504,y:0}).wait(1).to({graphics:mask_graphics_47,x:175.9563,y:0}).wait(1).to({graphics:mask_graphics_48,x:139.9623,y:0}).wait(1).to({graphics:mask_graphics_49,x:87.4933,y:0}).wait(1).to({graphics:mask_graphics_50,x:51.4993,y:0}).wait(1).to({graphics:mask_graphics_51,x:31.7053,y:0}).wait(1).to({graphics:mask_graphics_52,x:22.3178,y:0}).wait(1).to({graphics:mask_graphics_53,x:18.8311,y:0}).wait(1).to({graphics:mask_graphics_54,x:18.0265,y:0}).wait(1).to({graphics:mask_graphics_55,x:17.973,y:0}).wait(1).to({graphics:null,x:0,y:0}).wait(1));

	// VIDEO
	this.videoContainer = new lib.video();
	this.videoContainer.name = "videoContainer";
	this.videoContainer.parent = this;
	this.videoContainer.setTransform(36.7,765,0.6587,0.6587,0,0,0,0.3,480.2);

	var maskedShapeInstanceList = [this.videoContainer];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.videoContainer).to({y:-33},40,cjs.Ease.quartInOut).to({_off:true},16).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-349.3,418.6,1430.3999999999999);


(lib.bloc_NEXT_btn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":20});

	// timeline functions:
	this.frame_19 = function() {
		this.stop();
	}
	this.frame_36 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(19).call(this.frame_19).wait(17).call(this.frame_36).wait(1));

	// CTA DETAIL
	this.btnNext = new lib.Btn_next();
	this.btnNext.name = "btnNext";
	this.btnNext.parent = this;
	this.btnNext.setTransform(-340.5,411.5,1,1,0,0,0,0,26.5);

	this.timeline.addTween(cjs.Tween.get(this.btnNext).to({y:350.5},19,cjs.Ease.quartInOut).wait(2).to({y:411.5},14,cjs.Ease.quartInOut).to({_off:true},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-454,0,453,444.5);


(lib.bloc_hashtag = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_61 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(61).call(this.frame_61).wait(2));

	// Layer_1
	this.instance = new lib.hashtag_graphic("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(-456.05,243.2,2.3169,2.3169,0,0,0,-0.1,50.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true},61).wait(2));

	// Layer_5 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_28 = new cjs.Graphics().p("AtWBpIAAjRIatAAIAADRg");
	var mask_graphics_32 = new cjs.Graphics().p("AtWDSIAAmjIatAAIAAGjg");
	var mask_graphics_40 = new cjs.Graphics().p("AtWGYIAAsvIatAAIAAMvg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(28).to({graphics:mask_graphics_28,x:-400.45,y:265.45}).wait(4).to({graphics:mask_graphics_32,x:-400.45,y:255.025}).wait(8).to({graphics:mask_graphics_40,x:-400.45,y:255.85}).wait(23));

	// Layer_3
	this.instance_1 = new lib.txt_hashtag();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-421.5,244.05,1,1,0,0,0,0,31.6);
	this.instance_1._off = true;

	var maskedShapeInstanceList = [this.instance_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(28).to({_off:false},0).to({_off:true},34).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-573.7,0,287.1,317.1);


(lib.bloc_CTA = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":25});

	// timeline functions:
	this.frame_24 = function() {
		this.stop();
	}
	this.frame_41 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(24).call(this.frame_24).wait(17).call(this.frame_41).wait(1));

	// CTA DETAIL
	this.btnCta = new lib.Btn_detail();
	this.btnCta.name = "btnCta";
	this.btnCta.parent = this;
	this.btnCta.setTransform(-340.5,411.5,1,1,0,0,0,0,26.5);

	this.timeline.addTween(cjs.Tween.get(this.btnCta).to({y:350.5},24,cjs.Ease.quartInOut).wait(2).to({y:411.5},14,cjs.Ease.quartInOut).to({_off:true},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-454.5,0,228,444.5);


(lib.blocInfostextes = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":80});

	// timeline functions:
	this.frame_79 = function() {
		this.stop();
	}
	this.frame_120 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(79).call(this.frame_79).wait(41).call(this.frame_120).wait(1));

	// Bloc Hashtag
	this.instance = new lib.bloc_hashtag();
	this.instance.parent = this;
	this.instance.setTransform(-391.5,261.2,0.9,0.9,0,0,0,-412.9,244.7);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(62).to({_off:false},0).to({_off:true},18).wait(41));

	// masque Titre
	this.instance_1 = new lib.masque_generique();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-527.95,-13.9,0.0075,0.4711,0,0,0,0,17.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(34).to({_off:false},0).to({regX:0.5,scaleX:1.9223,x:-527.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.0128,x:-228.9},10,cjs.Ease.quartInOut).to({_off:true},1).wait(26).to({_off:false},0).to({regX:0.5,scaleX:1.9223,x:-527.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.0074,x:-528.7},10,cjs.Ease.quartInOut).to({_off:true},1).wait(21));

	// _NOM-du-CLIENT
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFF000").s().p("AAbA4Ig0hFIAABFIgaAAIAAhvIAYAAIA2BHIAAhHIAZAAIAABvg");
	this.shape.setTransform(-360.6,-14.15);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFF000").s().p("AgMA4IAAhvIAYAAIAABvg");
	this.shape_1.setTransform(-381.75,-14.15);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFF000").s().p("AgjA4IAAhvIAZAAIAABYIAuAAIAAAXg");
	this.shape_2.setTransform(-400.475,-14.15);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFF000").s().p("AgoA4IAAhvIBPAAIAAAXIg2AAIAAAWIAxAAIAAAUIgxAAIAAAYIA4AAIAAAWg");
	this.shape_3.setTransform(-422.45,-14.15);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFF000").s().p("AAWA4IAAgsIgrAAIAAAsIgZAAIAAhvIAZAAIAAAvIArAAIAAgvIAZAAIAABvg");
	this.shape_4.setTransform(-446.05,-14.15);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFF000").s().p("AgjApQgQgQAAgYQAAgZAQgQQASgRAXAAQAcAAASAVIgPASQgMgPgSAAQgNAAgJAJQgLAJAAAPQAAAPAKAKQAJAKAMAAQATAAALgPIAQARQgSAVgaAAQgZAAgRgRg");
	this.shape_5.setTransform(-469.9,-14.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFF000").s().p("AgLA4IAAhvIAXAAIAABvg");
	this.shape_6.setTransform(-490.45,-14.15);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFF000").s().p("AAlA4IAAhFIgeA7IgOAAIgdg7IAABFIgZAAIAAhvIAhAAIAcA9IAdg9IAiAAIAABvg");
	this.shape_7.setTransform(-512.7,-14.15);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},43).to({state:[]},46).to({state:[]},1).wait(31));

	// DECO 1
	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFF000").s().p("AAOAjIAAgbIgbAAIAAAbIgQAAIAAhFIAQAAIAAAdIAbAAIAAgdIAQAAIAABFg");
	this.shape_8.setTransform(-138.5,-31.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFF000").s().p("AgbAuIAphbIAOAAIgpBbg");
	this.shape_9.setTransform(-145.375,-31.075);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFF000").s().p("AAXAjIAAgrIgSAlIgJAAIgTglIAAArIgPAAIAAhFIAVAAIARAlIASglIAVAAIAABFg");
	this.shape_10.setTransform(-153.225,-31.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFF000").s().p("AAMAjIgTgbIgHAHIAAAUIgQAAIAAhFIAQAAIAAAdIAZgdIAUAAIgcAfIANASIAPAUg");
	this.shape_11.setTransform(-161.175,-31.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFF000").s().p("AgSAeQgIgHAAgJQAAgLAJgFQgGgGAAgIQAAgIAGgGQAHgFAKAAQAKAAAHAFQAHAGAAAIQAAAIgGAGQAJAFAAALQAAAJgIAHQgHAGgMAAQgLAAgHgGgAgHAHQgDACAAAFQAAAEADADQAEACADAAQAEAAADgCQAEgDAAgEQAAgFgDgCQgEgCgEAAQgDAAgEACgAgGgVQgDADAAADQAAAEADACQADADADAAQAEAAADgDQACgCAAgEQAAgDgCgDQgDgCgEAAQgDAAgDACg");
	this.shape_12.setTransform(-168.275,-31.125);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFF000").s().p("AgYAZIAIgMIADACIAFAEQAEACAEAAQADAAADgCQADgDAAgEQAAgFgDgDQgDgCgFAAQgFAAgHADIgGgGIAAghIAoAAIAAANIgaAAIAAAMIAGgBQAKAAAHAGQAIAFAAALQAAAKgIAHQgHAGgLAAQgNABgKgLg");
	this.shape_13.setTransform(-174.45,-31.05);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFF000").s().p("AACAjIAAgRIgbAAIAAgLIAZgpIARAAIgXAnIAIAAIAAgMIAPAAIAAAMIAJAAIAAANIgJAAIAAARg");
	this.shape_14.setTransform(-180.4,-31.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFF000").s().p("AgFAGQgDgCAAgEQAAgDADgDQACgCADAAQAEAAACACQADADAAADQAAAEgDACQgCADgEAAQgDAAgCgDg");
	this.shape_15.setTransform(-184.775,-28.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFF000").s().p("AgYAZIAIgMIACACIAGAEQAEACAEAAQADAAADgCQADgDAAgEQAAgFgDgDQgEgCgEAAQgFAAgHADIgHgGIAAghIApAAIAAANIgaAAIAAAMIAGgBQAKAAAIAGQAHAFAAALQAAAKgHAHQgJAGgJAAQgPABgJgLg");
	this.shape_16.setTransform(-189.15,-31.05);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFF000").s().p("AgSAeQgIgHAAgJQAAgLAJgFQgGgGAAgIQAAgIAGgGQAHgFAKAAQAKAAAHAFQAHAGAAAIQAAAIgGAGQAJAFAAALQAAAJgIAHQgHAGgMAAQgLAAgHgGgAgHAHQgDACAAAFQAAAEADADQAEACADAAQAEAAADgCQAEgDAAgEQAAgFgDgCQgEgCgEAAQgDAAgEACgAgGgVQgDADAAADQAAAEADACQADADADAAQAEAAADgDQACgCAAgEQAAgDgCgDQgDgCgEAAQgDAAgDACg");
	this.shape_17.setTransform(-195.325,-31.125);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFF000").s().p("AgFAGQgDgCAAgEQAAgDADgDQACgCADAAQAEAAACACQADADAAADQAAAEgDACQgCADgEAAQgDAAgCgDg");
	this.shape_18.setTransform(-202.625,-28.4);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFF000").s().p("AgfAjIAAhFIAZAAQASAAAKAJQAKAKAAAPQAAAQgKAKQgKAJgTAAgAgPAVIAKAAQAKAAAFgFQAGgFAAgLQAAgJgGgGQgFgFgLAAIgJAAg");
	this.shape_19.setTransform(-207.725,-31.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFF000").s().p("AgZAjIAAhFIAyAAIAAAOIgiAAIAAAOIAeAAIAAANIgeAAIAAAOIAjAAIAAAOg");
	this.shape_20.setTransform(-214.875,-31.1);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFF000").s().p("AgZAjIAAhFIAyAAIAAAOIgiAAIAAAOIAeAAIAAANIgeAAIAAAOIAjAAIAAAOg");
	this.shape_21.setTransform(-221.425,-31.1);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFF000").s().p("AgbAjIAAhFIAZAAQAPAAAHAHQAIAFAAAOQAAAMgIAFQgHAGgPABIgJAAIAAATgAgLACIAKAAQAHAAADgCQADgEAAgFQAAgGgEgDQgDgCgHAAIgJAAg");
	this.shape_22.setTransform(-227.975,-31.1);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFF000").s().p("AgbAYIAKgMQALAKAJAAQAEAAACgCQADgBAAgEQAAgDgDgCIgJgDQgMgDgGgEQgGgFAAgJQAAgKAIgGQAHgFAJAAQAHAAAHACQAHADAGAEIgIALQgJgHgKAAQgDAAgCACQgDACAAADQAAADADACIAMAEQAKADAFAEQAGAFAAAJQAAAJgHAGQgHAGgMAAQgPAAgOgMg");
	this.shape_23.setTransform(-234.875,-31.125);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFF000").s().p("AAAAjIAAg3IgNAAIAAgOIAbAAIAABFg");
	this.shape_24.setTransform(-202.475,-47.3);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFF000").s().p("AgUAaQgIgKAAgQQAAgQAIgJQAGgKAOAAQAOAAAIAKQAHAJAAAQQAAAQgHAKQgIAKgOAAQgOAAgGgKgAgKgQQgCAGAAAKQAAAKACAHQAEAGAGAAQAHAAADgGQAEgHAAgKQAAgKgEgGQgDgGgHAAQgGAAgEAGg");
	this.shape_25.setTransform(-207.65,-47.325);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFF000").s().p("AgFAGQgDgCAAgEQAAgDADgDQACgCADAAQAEAAACACQADADAAADQAAAEgDACQgCADgEAAQgDAAgCgDg");
	this.shape_26.setTransform(-215.175,-44.6);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFF000").s().p("AgbAYIAKgMQALAKAJAAQAEAAACgCQADgBAAgEQAAgDgDgCIgJgDQgMgDgGgEQgGgFAAgJQAAgKAIgGQAHgFAJAAQAHAAAHACQAHADAGAEIgIALQgJgHgKAAQgDAAgCACQgDACAAADQAAADADACIAMAEQAKADAFAEQAGAFAAAJQAAAJgHAGQgHAGgMAAQgPAAgOgMg");
	this.shape_27.setTransform(-219.875,-47.325);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFF000").s().p("AgZAaQgLgLAAgPQAAgPALgJQALgLAOAAQAQAAAKALQALAJAAAPQAAAPgLALQgKAKgQAAQgOAAgLgKgAgOgPQgGAHAAAIQAAAKAGAGQAGAGAIABQAJgBAGgGQAGgGAAgKQAAgIgGgHQgGgGgJAAQgIAAgGAGg");
	this.shape_28.setTransform(-227.125,-47.35);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFF000").s().p("AgbAjIAAhFIAZAAQAPAAAHAHQAIAFAAAOQAAAMgIAFQgHAGgPABIgJAAIAAATgAgLACIAKAAQAHAAADgCQADgEAAgFQAAgGgEgDQgDgCgHAAIgJAAg");
	this.shape_29.setTransform(-234.375,-47.3);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFF000").s().p("AgdAEIAAgHIA7AAIAAAHg");
	this.shape_30.setTransform(-270.65,-112.775);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFF000").s().p("AgdAEIAAgHIA7AAIAAAHg");
	this.shape_31.setTransform(-277.85,-112.775);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFF000").s().p("AgdAEIAAgHIA7AAIAAAHg");
	this.shape_32.setTransform(-285.05,-112.775);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFF000").s().p("AgdAEIAAgHIA7AAIAAAHg");
	this.shape_33.setTransform(-292.25,-112.775);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFF000").s().p("AgdAEIAAgHIA7AAIAAAHg");
	this.shape_34.setTransform(-299.45,-112.775);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFF000").s().p("AgdAEIAAgHIA7AAIAAAHg");
	this.shape_35.setTransform(-306.65,-112.775);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFF000").s().p("AgdAEIAAgHIA7AAIAAAHg");
	this.shape_36.setTransform(-313.85,-112.775);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFF000").s().p("AgdAEIAAgHIA7AAIAAAHg");
	this.shape_37.setTransform(-321.05,-112.775);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFF000").s().p("AgdAEIAAgHIA7AAIAAAHg");
	this.shape_38.setTransform(-328.25,-112.775);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFF000").s().p("AgdAEIAAgHIA7AAIAAAHg");
	this.shape_39.setTransform(-335.45,-112.775);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFF000").s().p("AgdAEIAAgHIA7AAIAAAHg");
	this.shape_40.setTransform(-342.65,-112.775);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFF000").s().p("AgYAZIAIgMIACACIAGAEQAEACAEAAQADAAADgCQADgDAAgEQAAgFgDgDQgDgCgFAAQgFAAgHADIgGgGIAAghIAoAAIAAANIgaAAIAAAMIAGgBQAKAAAHAGQAIAFAAALQAAAKgIAHQgIAGgJABQgPAAgJgLg");
	this.shape_41.setTransform(-204.7,147.7);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFF000").s().p("AACAjIAAgRIgbAAIAAgLIAZgpIARAAIgXAnIAIAAIAAgMIAQAAIAAAMIAIAAIAAANIgIAAIAAARg");
	this.shape_42.setTransform(-210.65,147.65);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFF000").s().p("AgFAGQgDgCAAgEQAAgCADgEQACgCADAAQAEAAACACQADAEAAACQAAAEgDACQgCADgEAAQgDAAgCgDg");
	this.shape_43.setTransform(-215.025,150.35);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFF000").s().p("AgYAZIAIgMIACACIAGAEQAEACAEAAQADAAADgCQADgDAAgEQAAgFgDgDQgEgCgEAAQgFAAgHADIgHgGIAAghIApAAIAAANIgaAAIAAAMIAGgBQAKAAAIAGQAHAFAAALQAAAKgHAHQgJAGgJABQgOAAgKgLg");
	this.shape_44.setTransform(-219.4,147.7);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFF000").s().p("AgFAGQgDgCAAgEQAAgCADgEQACgCADAAQAEAAACACQADAEAAACQAAAEgDACQgCADgEAAQgDAAgCgDg");
	this.shape_45.setTransform(-232.875,150.35);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FFF000").s().p("AgfAjIAAhFIAZAAQASAAAKAJQAKAKAAAPQAAAQgKAKQgKAJgTAAgAgPAVIAKAAQAKAAAFgFQAGgGAAgKQAAgJgGgGQgFgFgLAAIgJAAg");
	this.shape_46.setTransform(-237.975,147.65);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFF000").s().p("AgbAjIAAhFIAZAAQAPAAAHAHQAIAFAAANQAAANgIAFQgHAHgPAAIgJAAIAAATgAgLACIAKAAQAHAAADgCQADgEAAgFQAAgGgEgDQgDgCgHAAIgJAAg");
	this.shape_47.setTransform(-258.225,147.65);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FFF000").s().p("AgFAGQgDgCAAgEQAAgCADgEQACgCADAAQAEAAACACQADAEAAACQAAAEgDACQgCADgEAAQgDAAgCgDg");
	this.shape_48.setTransform(-115.825,134.15);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FFF000").s().p("AgZAaQgLgLAAgPQAAgPALgJQALgLAOAAQAQAAAKALQALAJAAAPQAAAPgLALQgKAKgQAAQgOAAgLgKgAgOgPQgGAHAAAIQAAAJAGAHQAGAGAIAAQAJAAAGgGQAGgHAAgJQAAgIgGgHQgGgGgJgBQgIABgGAGg");
	this.shape_49.setTransform(-127.775,131.4);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FFF000").s().p("AgbAjIAAhFIAZAAQAPAAAHAHQAIAFAAAOQAAAMgIAFQgHAHgPAAIgJAAIAAATgAgLACIAKAAQAHAAADgCQADgEAAgFQAAgGgEgDQgDgCgHAAIgJAAg");
	this.shape_50.setTransform(-135.025,131.45);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FFF000").s().p("AgcAEIAAgHIA5AAIAAAHg");
	this.shape_51.setTransform(-142.25,135.825);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FFF000").s().p("AgcAEIAAgHIA5AAIAAAHg");
	this.shape_52.setTransform(-149.45,135.825);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FFF000").s().p("AgcAEIAAgHIA5AAIAAAHg");
	this.shape_53.setTransform(-156.65,135.825);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FFF000").s().p("AgcAEIAAgHIA5AAIAAAHg");
	this.shape_54.setTransform(-163.85,135.825);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FFF000").s().p("AgcAEIAAgHIA5AAIAAAHg");
	this.shape_55.setTransform(-171.05,135.825);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FFF000").s().p("AgcAEIAAgHIA5AAIAAAHg");
	this.shape_56.setTransform(-178.25,135.825);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FFF000").s().p("AgcAEIAAgHIA5AAIAAAHg");
	this.shape_57.setTransform(-185.45,135.825);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#FFF000").s().p("AgcAEIAAgHIA5AAIAAAHg");
	this.shape_58.setTransform(-192.65,135.825);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#FFF000").s().p("AgcAEIAAgHIA5AAIAAAHg");
	this.shape_59.setTransform(-199.85,135.825);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#FFF000").s().p("AgcAEIAAgHIA5AAIAAAHg");
	this.shape_60.setTransform(-207.05,135.825);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#FFF000").s().p("AgcAEIAAgHIA5AAIAAAHg");
	this.shape_61.setTransform(-214.25,135.825);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#FFF000").s().p("AgcAEIAAgHIA5AAIAAAHg");
	this.shape_62.setTransform(-221.45,135.825);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#FFF000").s().p("AgcAEIAAgHIA5AAIAAAHg");
	this.shape_63.setTransform(-228.65,135.825);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#FFF000").s().p("AgcAEIAAgHIA5AAIAAAHg");
	this.shape_64.setTransform(-235.85,135.825);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#FFF000").s().p("AgcAEIAAgHIA5AAIAAAHg");
	this.shape_65.setTransform(-243.05,135.825);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#FFF000").s().p("AgcAEIAAgHIA5AAIAAAHg");
	this.shape_66.setTransform(-250.25,135.825);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#FFF000").s().p("AgcAEIAAgHIA5AAIAAAHg");
	this.shape_67.setTransform(-257.45,135.825);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#FFF000").s().p("AgcAEIAAgHIA5AAIAAAHg");
	this.shape_68.setTransform(-264.65,135.825);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#FFF000").s().p("AgYAZIAIgLIADACIAFADQAEACAEAAQADAAADgCQADgDAAgFQAAgEgDgCQgDgDgFAAQgFAAgHAEIgHgGIAAgjIAqAAIAAAOIgbAAIAAAMIAGgBQAKAAAIAGQAHAFAAAKQAAALgHAGQgJAIgKgBQgOAAgJgKg");
	this.shape_69.setTransform(33.4,-143.05);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#FFF000").s().p("AgLAjIgHgDIgFgEIAIgLIACACIAEACIAGACQAGAAADgFQAEgEABgIQgHAFgGAAQgKAAgGgHQgHgFAAgLQAAgKAHgHQAHgGALAAQAGAAAGADQAFACADAGQAGAJAAAPQAAAJgCAHQgDAHgEAFQgIAIgLAAIgJgBgAgIgTQgCADAAAEQAAAEACADQADADAEAAQAEAAADgCQADgDAAgEQAAgFgDgDQgDgDgDAAQgFAAgDADg");
	this.shape_70.setTransform(27.225,-143.125);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#FFF000").s().p("AgLAjIgHgDIgFgEIAIgLIACACIAEACIAGACQAGAAADgFQAEgEABgIQgHAFgGAAQgKAAgGgHQgHgFAAgLQAAgKAHgHQAHgGALAAQAGAAAGADQAFACADAGQAGAJAAAPQAAAJgCAHQgDAHgEAFQgIAIgLAAIgJgBgAgIgTQgCADAAAEQAAAEACADQADADAEAAQAEAAADgCQADgDAAgEQAAgFgDgDQgDgDgDAAQgFAAgDADg");
	this.shape_71.setTransform(20.925,-143.125);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#FFF000").s().p("AgLAhQgFgDgDgFQgGgKAAgPQAAgIADgHQACgIAEgEQAIgIALAAQAFAAAEABIAHADIAFAEIgHALIgCgCIgFgCIgGgCQgGAAgDAEQgDAFgCAHQAHgEAHAAQAJAAAGAHQAHAFAAALQAAAKgHAHQgHAGgLAAQgGAAgGgDgAgFAFQgDADAAAEQAAAFADADQADADADAAQAFAAADgDQADgDAAgEQAAgFgDgDQgCgDgFAAQgEAAgDADg");
	this.shape_72.setTransform(14.65,-143.125);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#FFF000").s().p("AADAjIAAgQIgbAAIAAgNIAYgoIAQAAIgWAnIAJAAIAAgLIAOAAIAAALIAIAAIAAAOIgIAAIAAAQg");
	this.shape_73.setTransform(8.55,-143.1);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#FFF000").s().p("AAAAjIAAg4IgNAAIAAgNIAbAAIAABFg");
	this.shape_74.setTransform(3.325,-143.1);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#FFF000").s().p("AgFAGQgDgDAAgDQAAgCADgDQACgDADAAQAEAAACADQADADAAACQAAADgDADQgCADgEAAQgDAAgCgDg");
	this.shape_75.setTransform(0.125,-140.4);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#FFF000").s().p("AgTAjIAag4IgQAAIAAAJIgPAAIAAgWIAxAAIAAAMIgbA5g");
	this.shape_76.setTransform(-4.125,-143.1);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#FFF000").s().p("AgSAeQgIgHAAgJQAAgLAJgFQgGgGAAgIQAAgIAGgGQAHgFAKAAQAKAAAHAFQAHAGAAAIQAAAIgGAGQAJAFAAALQAAAJgIAHQgHAGgMAAQgLAAgHgGgAgHAHQgDACAAAFQAAAEADADQAEACADAAQAEAAADgCQAEgDAAgEQAAgFgDgCQgEgCgEAAQgDAAgEACgAgGgVQgDADAAADQAAAEADACQADADADAAQAEAAADgDQACgCAAgEQAAgDgCgDQgDgCgEAAQgDAAgDACg");
	this.shape_77.setTransform(-10.275,-143.125);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("#FFF000").s().p("AACAjIAAgQIgbAAIAAgNIAZgoIARAAIgXAnIAIAAIAAgLIAQAAIAAALIAIAAIAAAOIgIAAIAAAQg");
	this.shape_78.setTransform(-21.65,-143.1);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("#FFF000").s().p("AgXAkIAAgMIAVgVIAHgJQADgEAAgDQAAgEgCgCQgDgCgDAAQgGAAgGAJIgMgHQAFgIAGgEQAFgEAJAAQAJAAAGAGQAHAGAAAKQAAAFgDAFQgCAEgIAIIgLANIAaAAIAAAOg");
	this.shape_79.setTransform(-27.625,-143.175);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f("#FFF000").s().p("AAAAjIAAg4IgNAAIAAgNIAbAAIAABFg");
	this.shape_80.setTransform(-32.925,-143.1);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("#FFF000").s().p("AgXAkIAAgMIAVgVIAHgJQADgEAAgDQAAgEgCgCQgDgCgDAAQgGAAgGAJIgMgHQAFgIAGgEQAFgEAJAAQAJAAAGAGQAHAGAAAKQAAAFgDAFQgCAEgIAIIgLANIAaAAIAAAOg");
	this.shape_81.setTransform(-37.725,-143.175);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f("#FFF000").s().p("AAAAjIAAg4IgNAAIAAgNIAbAAIAABFg");
	this.shape_82.setTransform(-43.025,-143.1);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("#FFF000").s().p("AgYAZIAIgLIADACIAFADQAEACAEAAQADAAADgCQADgDAAgFQAAgEgDgCQgEgDgEAAQgFAAgHAEIgHgGIAAgjIAqAAIAAAOIgbAAIAAAMIAGgBQAKAAAHAGQAIAFAAAKQAAALgIAGQgHAIgLgBQgOAAgJgKg");
	this.shape_83.setTransform(-47.7,-143.05);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f("#FFF000").s().p("AgYAZIAIgLIADACIAFADQAEACAEAAQADAAADgCQADgDAAgFQAAgEgDgCQgEgDgEAAQgFAAgHAEIgHgGIAAgjIAqAAIAAAOIgbAAIAAAMIAGgBQAKAAAHAGQAIAFAAAKQAAALgIAGQgHAIgLgBQgNAAgKgKg");
	this.shape_84.setTransform(-53.65,-143.05);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f("#FFF000").s().p("AgYAZIAIgLIADACIAFADQAEACAEAAQADAAADgCQADgDAAgFQAAgEgDgCQgDgDgFAAQgFAAgHAEIgGgGIAAgjIApAAIAAAOIgbAAIAAAMIAGgBQAKAAAHAGQAIAFAAAKQAAALgIAGQgHAIgLgBQgNAAgKgKg");
	this.shape_85.setTransform(-59.6,-143.05);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f("#FFF000").s().p("AgFAGQgDgDAAgDQAAgCADgDQACgDADAAQAEAAACADQADADAAACQAAADgDADQgCADgEAAQgDAAgCgDg");
	this.shape_86.setTransform(-64.075,-140.4);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f("#FFF000").s().p("AgSAeQgIgHAAgJQAAgLAJgFQgGgGAAgIQAAgIAGgGQAHgFAKAAQAKAAAHAFQAHAGAAAIQAAAIgGAGQAJAFAAALQAAAJgIAHQgHAGgMAAQgLAAgHgGgAgHAHQgDACAAAFQAAAEADADQAEACADAAQAEAAADgCQAEgDAAgEQAAgFgDgCQgEgCgEAAQgDAAgEACgAgGgVQgDADAAADQAAAEADACQADADADAAQAEAAADgDQACgCAAgEQAAgDgCgDQgDgCgEAAQgDAAgDACg");
	this.shape_87.setTransform(-68.675,-143.125);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f("#FFF000").s().p("AgYAZIAIgLIACACIAGADQAEACAEAAQADAAADgCQADgDAAgFQAAgEgDgCQgEgDgEAAQgFAAgHAEIgHgGIAAgjIApAAIAAAOIgaAAIAAAMIAGgBQAKAAAIAGQAHAFAAAKQAAALgHAGQgJAIgJgBQgPAAgJgKg");
	this.shape_88.setTransform(-74.85,-143.05);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f("#FFF000").s().p("AgFAGQgDgDAAgDQAAgCADgDQACgDADAAQAEAAACADQADADAAACQAAADgDADQgCADgEAAQgDAAgCgDg");
	this.shape_89.setTransform(-81.925,-140.4);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f("#FFF000").s().p("AgbAYIAKgMQALAKAJAAQAEAAACgCQADgBAAgEQAAgDgDgCIgJgDQgMgDgGgEQgGgFAAgJQAAgKAIgGQAHgFAJAAQAHAAAHACQAHADAGAEIgIALQgJgHgKAAQgDAAgCACQgDACAAADQAAADADACIAMAEQAKADAFAEQAGAFAAAJQAAAJgHAGQgHAGgMAAQgPAAgOgMg");
	this.shape_90.setTransform(-86.625,-143.125);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f("#FFF000").s().p("AgbAjIAAhFIAZAAQAPAAAHAGQAIAGAAANQAAAMgIAHQgHAFgPAAIgJAAIAAAUgAgLACIAKAAQAHAAADgCQADgDAAgHQAAgFgEgDQgDgCgHgBIgJAAg");
	this.shape_91.setTransform(-92.875,-143.1);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f("#FFF000").s().p("AgVAaQgLgKAAgQQAAgOALgLQALgKAOAAQAPAAALAKIgIALQgEgEgEgBQgFgCgEAAQgIAAgGAGQgGAGgBAJQAAAKAHAGQAFAGAHAAQAJAAAFgDIAAgTIAQAAIAAAYQgLAMgSAAQgOAAgLgKg");
	this.shape_92.setTransform(-100.3,-143.125);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.f("#FFF000").s().p("AgRAHIAAgNIAjAAIAAANg");
	this.shape_93.setTransform(-91.75,-158.8);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.f("#FFF000").s().p("AgRAHIAAgNIAjAAIAAANg");
	this.shape_94.setTransform(-96.65,-158.8);

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.f("#FFF000").s().p("AgRAHIAAgNIAjAAIAAANg");
	this.shape_95.setTransform(-101.55,-158.8);

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.f("#FFF000").s().p("AgcAEIAAgHIA5AAIAAAHg");
	this.shape_96.setTransform(-250.25,135.825);

	this.shape_97 = new cjs.Shape();
	this.shape_97.graphics.f("#FFF000").s().p("AgcAEIAAgHIA5AAIAAAHg");
	this.shape_97.setTransform(-257.45,135.825);

	this.shape_98 = new cjs.Shape();
	this.shape_98.graphics.f("#FFF000").s().p("AgcAEIAAgHIA5AAIAAAHg");
	this.shape_98.setTransform(-264.65,135.825);

	this.shape_99 = new cjs.Shape();
	this.shape_99.graphics.f("#FFF000").s().p("AgdAEIAAgHIA6AAIAAAHg");
	this.shape_99.setTransform(-18.25,136.425);

	this.shape_100 = new cjs.Shape();
	this.shape_100.graphics.f("#FFF000").s().p("AgdAEIAAgHIA6AAIAAAHg");
	this.shape_100.setTransform(-25.45,136.425);

	this.shape_101 = new cjs.Shape();
	this.shape_101.graphics.f("#FFF000").s().p("AgdAEIAAgHIA6AAIAAAHg");
	this.shape_101.setTransform(-32.65,136.425);

	this.shape_102 = new cjs.Shape();
	this.shape_102.graphics.f("#FFF000").s().p("AgdAEIAAgHIA6AAIAAAHg");
	this.shape_102.setTransform(39.35,90.225);

	this.shape_103 = new cjs.Shape();
	this.shape_103.graphics.f("#FFF000").s().p("AgdAEIAAgHIA6AAIAAAHg");
	this.shape_103.setTransform(32.15,90.225);

	this.shape_104 = new cjs.Shape();
	this.shape_104.graphics.f("#FFF000").s().p("AgdAEIAAgHIA6AAIAAAHg");
	this.shape_104.setTransform(24.95,90.225);

	this.shape_105 = new cjs.Shape();
	this.shape_105.graphics.f("#FFF000").s().p("AgdAEIAAgHIA6AAIAAAHg");
	this.shape_105.setTransform(17.75,90.225);

	this.shape_106 = new cjs.Shape();
	this.shape_106.graphics.f("#FFF000").s().p("AgdAEIAAgHIA6AAIAAAHg");
	this.shape_106.setTransform(10.55,90.225);

	this.shape_107 = new cjs.Shape();
	this.shape_107.graphics.f("#FFF000").s().p("AgdAEIAAgHIA6AAIAAAHg");
	this.shape_107.setTransform(3.35,90.225);

	this.shape_108 = new cjs.Shape();
	this.shape_108.graphics.f("#FFF000").s().p("AgdAEIAAgHIA6AAIAAAHg");
	this.shape_108.setTransform(-3.85,90.225);

	this.shape_109 = new cjs.Shape();
	this.shape_109.graphics.f("#FFF000").s().p("AgdAEIAAgHIA6AAIAAAHg");
	this.shape_109.setTransform(-11.05,90.225);

	this.shape_110 = new cjs.Shape();
	this.shape_110.graphics.f("#FFF000").s().p("AgdAEIAAgHIA6AAIAAAHg");
	this.shape_110.setTransform(-18.25,90.225);

	this.shape_111 = new cjs.Shape();
	this.shape_111.graphics.f("#FFF000").s().p("AgdAEIAAgHIA6AAIAAAHg");
	this.shape_111.setTransform(-25.45,90.225);

	this.shape_112 = new cjs.Shape();
	this.shape_112.graphics.f("#FFF000").s().p("AgdAEIAAgHIA6AAIAAAHg");
	this.shape_112.setTransform(-32.65,90.225);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_29},{t:this.shape_28},{t:this.shape_27,p:{x:-219.875,y:-47.325}},{t:this.shape_26},{t:this.shape_25,p:{x:-207.65,y:-47.325}},{t:this.shape_24,p:{x:-202.475,y:-47.3}},{t:this.shape_23,p:{x:-234.875,y:-31.125}},{t:this.shape_22},{t:this.shape_21,p:{x:-221.425,y:-31.1}},{t:this.shape_20,p:{x:-214.875,y:-31.1}},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17,p:{x:-195.325,y:-31.125}},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12,p:{x:-168.275,y:-31.125}},{t:this.shape_11,p:{x:-161.175,y:-31.1}},{t:this.shape_10,p:{x:-153.225,y:-31.1}},{t:this.shape_9,p:{x:-145.375,y:-31.075}},{t:this.shape_8,p:{x:-138.5,y:-31.1}}]},33).to({state:[{t:this.shape_95},{t:this.shape_94},{t:this.shape_93},{t:this.shape_92},{t:this.shape_91},{t:this.shape_90},{t:this.shape_89},{t:this.shape_88},{t:this.shape_87},{t:this.shape_86},{t:this.shape_85},{t:this.shape_84},{t:this.shape_83},{t:this.shape_82},{t:this.shape_81},{t:this.shape_80},{t:this.shape_79},{t:this.shape_78},{t:this.shape_77},{t:this.shape_76},{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68,p:{x:-264.65}},{t:this.shape_67,p:{x:-257.45}},{t:this.shape_66,p:{x:-250.25}},{t:this.shape_65,p:{x:-243.05}},{t:this.shape_64,p:{x:-235.85}},{t:this.shape_63,p:{x:-228.65}},{t:this.shape_62,p:{x:-221.45}},{t:this.shape_61,p:{x:-214.25,y:135.825}},{t:this.shape_60,p:{x:-207.05,y:135.825}},{t:this.shape_59,p:{x:-199.85,y:135.825}},{t:this.shape_58,p:{x:-192.65,y:135.825}},{t:this.shape_57,p:{x:-185.45,y:135.825}},{t:this.shape_56,p:{x:-178.25,y:135.825}},{t:this.shape_55,p:{x:-171.05,y:135.825}},{t:this.shape_54,p:{x:-163.85,y:135.825}},{t:this.shape_53,p:{x:-156.65,y:135.825}},{t:this.shape_52,p:{x:-149.45,y:135.825}},{t:this.shape_51,p:{x:-142.25,y:135.825}},{t:this.shape_50},{t:this.shape_49},{t:this.shape_27,p:{x:-120.525,y:131.425}},{t:this.shape_48},{t:this.shape_25,p:{x:-108.3,y:131.425}},{t:this.shape_24,p:{x:-103.125,y:131.45}},{t:this.shape_23,p:{x:-265.125,y:147.625}},{t:this.shape_47},{t:this.shape_21,p:{x:-251.675,y:147.65}},{t:this.shape_20,p:{x:-245.125,y:147.65}},{t:this.shape_46},{t:this.shape_45},{t:this.shape_17,p:{x:-225.575,y:147.625}},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_12,p:{x:-198.525,y:147.625}},{t:this.shape_11,p:{x:-191.425,y:147.65}},{t:this.shape_10,p:{x:-183.475,y:147.65}},{t:this.shape_9,p:{x:-175.625,y:147.675}},{t:this.shape_8,p:{x:-168.75,y:147.65}},{t:this.shape_40,p:{y:-112.775}},{t:this.shape_39,p:{y:-112.775}},{t:this.shape_38,p:{y:-112.775}},{t:this.shape_37,p:{y:-112.775}},{t:this.shape_36,p:{y:-112.775}},{t:this.shape_35,p:{y:-112.775}},{t:this.shape_34,p:{y:-112.775}},{t:this.shape_33,p:{y:-112.775}},{t:this.shape_32,p:{y:-112.775}},{t:this.shape_31,p:{y:-112.775}},{t:this.shape_30,p:{y:-112.775}}]},10).to({state:[{t:this.shape_95},{t:this.shape_94},{t:this.shape_93},{t:this.shape_92},{t:this.shape_91},{t:this.shape_90},{t:this.shape_89},{t:this.shape_88},{t:this.shape_87},{t:this.shape_86},{t:this.shape_85},{t:this.shape_84},{t:this.shape_83},{t:this.shape_82},{t:this.shape_81},{t:this.shape_80},{t:this.shape_79},{t:this.shape_78},{t:this.shape_77},{t:this.shape_76},{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_98},{t:this.shape_97},{t:this.shape_96},{t:this.shape_68,p:{x:-243.05}},{t:this.shape_67,p:{x:-235.85}},{t:this.shape_66,p:{x:-228.65}},{t:this.shape_65,p:{x:-221.45}},{t:this.shape_64,p:{x:-214.25}},{t:this.shape_63,p:{x:-207.05}},{t:this.shape_62,p:{x:-199.85}},{t:this.shape_61,p:{x:-192.65,y:135.825}},{t:this.shape_60,p:{x:-185.45,y:135.825}},{t:this.shape_59,p:{x:-178.25,y:135.825}},{t:this.shape_58,p:{x:-171.05,y:135.825}},{t:this.shape_57,p:{x:-163.85,y:135.825}},{t:this.shape_56,p:{x:-156.65,y:135.825}},{t:this.shape_55,p:{x:-149.45,y:135.825}},{t:this.shape_54,p:{x:-142.25,y:135.825}},{t:this.shape_50},{t:this.shape_49},{t:this.shape_27,p:{x:-120.525,y:131.425}},{t:this.shape_48},{t:this.shape_25,p:{x:-108.3,y:131.425}},{t:this.shape_24,p:{x:-103.125,y:131.45}},{t:this.shape_53,p:{x:-86.8,y:135.825}},{t:this.shape_52,p:{x:-79.6,y:135.825}},{t:this.shape_51,p:{x:-72.4,y:135.825}},{t:this.shape_23,p:{x:-265.125,y:147.625}},{t:this.shape_47},{t:this.shape_21,p:{x:-251.675,y:147.65}},{t:this.shape_20,p:{x:-245.125,y:147.65}},{t:this.shape_46},{t:this.shape_45},{t:this.shape_17,p:{x:-225.575,y:147.625}},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_12,p:{x:-198.525,y:147.625}},{t:this.shape_11,p:{x:-191.425,y:147.65}},{t:this.shape_10,p:{x:-183.475,y:147.65}},{t:this.shape_9,p:{x:-175.625,y:147.675}},{t:this.shape_8,p:{x:-168.75,y:147.65}},{t:this.shape_40,p:{y:87.225}},{t:this.shape_39,p:{y:87.225}},{t:this.shape_38,p:{y:87.225}},{t:this.shape_37,p:{y:87.225}},{t:this.shape_36,p:{y:87.225}},{t:this.shape_35,p:{y:87.225}},{t:this.shape_34,p:{y:87.225}},{t:this.shape_33,p:{y:87.225}},{t:this.shape_32,p:{y:87.225}},{t:this.shape_31,p:{y:87.225}},{t:this.shape_30,p:{y:87.225}}]},8).to({state:[{t:this.shape_61,p:{x:-301.65,y:-65.775}},{t:this.shape_60,p:{x:-294.45,y:-65.775}},{t:this.shape_59,p:{x:-287.25,y:-65.775}},{t:this.shape_58,p:{x:-280.05,y:-65.775}},{t:this.shape_57,p:{x:-272.85,y:-65.775}},{t:this.shape_56,p:{x:-265.65,y:-65.775}},{t:this.shape_55,p:{x:-258.45,y:-65.775}},{t:this.shape_54,p:{x:-251.25,y:-65.775}},{t:this.shape_53,p:{x:-244.05,y:-65.775}},{t:this.shape_52,p:{x:-236.85,y:-65.775}},{t:this.shape_51,p:{x:-229.65,y:-65.775}},{t:this.shape_112},{t:this.shape_111},{t:this.shape_110},{t:this.shape_109},{t:this.shape_108},{t:this.shape_107},{t:this.shape_106},{t:this.shape_105},{t:this.shape_104},{t:this.shape_103},{t:this.shape_102},{t:this.shape_101},{t:this.shape_100},{t:this.shape_99}]},17).to({state:[]},8).wait(45));

	// DECO 2
	this.shape_113 = new cjs.Shape();
	this.shape_113.graphics.f("#FFF000").s().p("AgdAEIAAgHIA7AAIAAAHg");
	this.shape_113.setTransform(50.75,-22.575);

	this.shape_114 = new cjs.Shape();
	this.shape_114.graphics.f("#FFF000").s().p("AgdAEIAAgHIA7AAIAAAHg");
	this.shape_114.setTransform(43.55,-22.575);

	this.shape_115 = new cjs.Shape();
	this.shape_115.graphics.f("#FFF000").s().p("AgdAEIAAgHIA7AAIAAAHg");
	this.shape_115.setTransform(36.35,-22.575);

	this.shape_116 = new cjs.Shape();
	this.shape_116.graphics.f("#FFF000").s().p("AgdAEIAAgHIA7AAIAAAHg");
	this.shape_116.setTransform(-434.6,173.175);

	this.shape_117 = new cjs.Shape();
	this.shape_117.graphics.f("#FFF000").s().p("AgdAEIAAgHIA7AAIAAAHg");
	this.shape_117.setTransform(-441.8,173.175);

	this.shape_118 = new cjs.Shape();
	this.shape_118.graphics.f("#FFF000").s().p("AgdAEIAAgHIA7AAIAAAHg");
	this.shape_118.setTransform(-449,173.175);

	this.shape_119 = new cjs.Shape();
	this.shape_119.graphics.f("#FFF000").s().p("AgdAEIAAgHIA7AAIAAAHg");
	this.shape_119.setTransform(-456.2,173.175);

	this.shape_120 = new cjs.Shape();
	this.shape_120.graphics.f("#FFF000").s().p("AgdAEIAAgHIA7AAIAAAHg");
	this.shape_120.setTransform(-463.4,173.175);

	this.shape_121 = new cjs.Shape();
	this.shape_121.graphics.f("#FFF000").s().p("AgdAEIAAgHIA7AAIAAAHg");
	this.shape_121.setTransform(-470.6,173.175);

	this.shape_122 = new cjs.Shape();
	this.shape_122.graphics.f("#FFF000").s().p("AgdAEIAAgHIA7AAIAAAHg");
	this.shape_122.setTransform(-477.8,173.175);

	this.shape_123 = new cjs.Shape();
	this.shape_123.graphics.f("#FFF000").s().p("AgdAEIAAgHIA7AAIAAAHg");
	this.shape_123.setTransform(-485,173.175);

	this.shape_124 = new cjs.Shape();
	this.shape_124.graphics.f("#FFF000").s().p("AgdAEIAAgHIA7AAIAAAHg");
	this.shape_124.setTransform(-492.2,173.175);

	this.shape_125 = new cjs.Shape();
	this.shape_125.graphics.f("#FFF000").s().p("AgdAEIAAgHIA7AAIAAAHg");
	this.shape_125.setTransform(-499.4,173.175);

	this.shape_126 = new cjs.Shape();
	this.shape_126.graphics.f("#FFF000").s().p("AgdAEIAAgHIA7AAIAAAHg");
	this.shape_126.setTransform(-506.6,173.175);

	this.shape_127 = new cjs.Shape();
	this.shape_127.graphics.f("#FFF000").s().p("AAHATIAAgPIgOAAIAAAPIgIAAIAAglIAIAAIAAAQIAOAAIAAgQIAJAAIAAAlg");
	this.shape_127.setTransform(-169.55,25.25);

	this.shape_128 = new cjs.Shape();
	this.shape_128.graphics.f("#FFF000").s().p("AgPAaIAXgzIAIAAIgXAzg");
	this.shape_128.setTransform(-173.425,25.275);

	this.shape_129 = new cjs.Shape();
	this.shape_129.graphics.f("#FFF000").s().p("AANATIAAgXIgKAUIgFAAIgKgUIAAAXIgJAAIAAglIAMAAIAJAUIAKgUIAMAAIAAAlg");
	this.shape_129.setTransform(-177.825,25.25);

	this.shape_130 = new cjs.Shape();
	this.shape_130.graphics.f("#FFF000").s().p("AAHATIgLgPIgEAFIAAAKIgIAAIAAglIAIAAIAAAQIAPgQIAKAAIgQARIAIAJIAIALg");
	this.shape_130.setTransform(-182.3,25.25);

	this.shape_131 = new cjs.Shape();
	this.shape_131.graphics.f("#FFF000").s().p("AgKARQgEgEAAgFQAAgGAFgDQgDgDAAgFQAAgEADgDQAEgDAFAAQAGAAADADQAEADABAEQAAAFgEADQAFADAAAGQAAAFgEAEQgFADgGAAQgFAAgFgDgAgDAEQgBAAAAABQAAAAgBABQAAAAAAABQAAAAAAABQAAAAAAABQAAAAAAABQABAAAAABQAAAAABABIADABIAEgBQAAgBABAAQAAgBAAAAQABgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQAAgBAAAAIgFgBIgDABgAgDgLQAAAAgBABQAAAAAAAAQAAABAAAAQgBABAAAAQAAABABAAQAAABAAAAQAAAAAAABQABAAAAAAIADACQABAAAAAAQAAAAABgBQAAAAABAAQAAAAAAgBQABAAAAAAQABgBAAAAQAAAAAAgBQABAAAAgBQAAAAgBgBQAAAAAAgBQAAAAgBAAQAAgBgBAAQAAgBAAAAQgBAAAAAAQgBgBAAAAQAAAAgBAAIgDACg");
	this.shape_131.setTransform(-186.25,25.225);

	this.shape_132 = new cjs.Shape();
	this.shape_132.graphics.f("#FFF000").s().p("AgNAOIAFgHIABACIAEACIADABQAAAAABAAQAAAAABgBQAAAAABAAQAAAAABgBQAAAAAAAAQABgBAAAAQAAgBAAAAQABgBAAgBQAAAAgBgBQAAAAAAgBQAAgBgBAAQAAAAAAgBQgDgBgCAAQgCAAgEACIgEgDIAAgTIAXAAIAAAIIgOAAIAAAGIACAAQAGAAAEADQAEADAAAFQAAAGgEAEQgFAEgFAAQgHAAgGgGg");
	this.shape_132.setTransform(-189.75,25.275);

	this.shape_133 = new cjs.Shape();
	this.shape_133.graphics.f("#FFF000").s().p("AABATIAAgJIgOAAIAAgHIANgVIAJAAIgMAUIAEAAIAAgFIAJAAIAAAFIAEAAIAAAIIgEAAIAAAJg");
	this.shape_133.setTransform(-193.025,25.25);

	this.shape_134 = new cjs.Shape();
	this.shape_134.graphics.f("#FFF000").s().p("AgCADQgBAAAAAAQAAgBgBAAQAAgBAAAAQAAgBAAAAQAAAAAAAAQAAAAAAgBQABAAAAgBQAAAAABgBQAAAAAAAAQABAAAAgBQABAAAAAAQAAAAAAAAQABAAAAAAQAAAAABAAQAAABABAAQAAAAABAAQAAABAAAAQAAABABAAQAAABAAAAQAAAAAAAAQAAAAAAABQAAAAAAABQgBAAAAABQAAAAAAAAQgBABAAAAQgBAAAAABQgBAAAAAAQAAAAgBAAQAAAAAAAAQAAAAgBAAQAAgBgBAAQAAAAAAgBg");
	this.shape_134.setTransform(-195.5,26.75);

	this.shape_135 = new cjs.Shape();
	this.shape_135.graphics.f("#FFF000").s().p("AgNAOIAFgHIABACIAEACIADABQAAAAABAAQAAAAABgBQAAAAABAAQAAAAAAgBQABAAAAAAQABgBAAAAQAAgBAAAAQABgBAAgBQAAAAgBgBQAAAAAAgBQAAgBgBAAQAAAAgBgBQgCgBgBAAQgDAAgEACIgEgDIAAgTIAXAAIAAAIIgOAAIAAAGIACAAQAGAAAEADQAEADAAAFQAAAGgEAEQgFAEgFAAQgIAAgFgGg");
	this.shape_135.setTransform(-197.95,25.275);

	this.shape_136 = new cjs.Shape();
	this.shape_136.graphics.f("#FFF000").s().p("AgKARQgEgEAAgFQAAgGAFgDQgDgDgBgFQAAgEAFgDQADgDAFAAQAGAAAEADQADADAAAEQABAFgEADQAFADAAAGQAAAFgEAEQgEADgHAAQgGAAgEgDgAgEAEQAAAAAAABQAAAAgBABQAAAAAAABQAAAAAAABQAAAAAAABQAAAAAAABQABAAAAABQAAAAAAABIAEABIAEgBQABgBAAAAQAAgBABAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQAAgBgBAAIgEgBIgEABgAgDgLQAAAAgBABQAAAAAAAAQAAABAAAAQgBABAAAAQAAABABAAQAAABAAAAQAAAAAAABQABAAAAAAIADACQAAAAABAAQAAAAABgBQAAAAABAAQAAAAAAgBQABAAAAAAQABgBAAAAQAAAAAAgBQAAAAAAgBQAAAAAAgBQAAAAAAgBQAAAAgBAAQAAgBgBAAQAAgBAAAAQgBAAAAAAQgBgBAAAAQgBAAAAAAIgDACg");
	this.shape_136.setTransform(-201.4,25.225);

	this.shape_137 = new cjs.Shape();
	this.shape_137.graphics.f("#FFF000").s().p("AgCADQgBAAAAAAQAAgBgBAAQAAgBAAAAQAAgBAAAAQAAAAAAAAQAAAAAAgBQABAAAAgBQAAAAABgBQAAAAAAAAQABAAAAgBQABAAAAAAQAAAAAAAAQABAAAAAAQAAAAABAAQAAABABAAQAAAAABAAQAAABAAAAQAAABABAAQAAABAAAAQAAAAAAAAQAAAAAAABQAAAAAAABQgBAAAAABQAAAAAAAAQgBABAAAAQgBAAAAABQgBAAAAAAQAAAAgBAAQAAAAAAAAQAAAAgBAAQAAgBgBAAQAAAAAAgBg");
	this.shape_137.setTransform(-205.5,26.75);

	this.shape_138 = new cjs.Shape();
	this.shape_138.graphics.f("#FFF000").s().p("AgRATIAAglIAOAAQAJAAAGAEQAGAFAAAJQAAAJgGAFQgFAGgLgBgAgIAMIAFAAQAFAAAEgDQADgEAAgFQAAgFgDgDQgEgDgFAAIgFAAg");
	this.shape_138.setTransform(-208.325,25.25);

	this.shape_139 = new cjs.Shape();
	this.shape_139.graphics.f("#FFF000").s().p("AgNATIAAglIAbAAIAAAHIgTAAIAAAIIARAAIAAAGIgRAAIAAAJIATAAIAAAHg");
	this.shape_139.setTransform(-212.325,25.25);

	this.shape_140 = new cjs.Shape();
	this.shape_140.graphics.f("#FFF000").s().p("AgNATIAAglIAbAAIAAAHIgTAAIAAAIIARAAIAAAGIgRAAIAAAJIATAAIAAAHg");
	this.shape_140.setTransform(-216.025,25.25);

	this.shape_141 = new cjs.Shape();
	this.shape_141.graphics.f("#FFF000").s().p("AgPATIAAglIAOAAQAIAAAEADQAFAEAAAGQAAAHgFADQgEADgHABIgGAAIAAAKgAgGABIAGAAQAEAAABgBQACgCAAgDQAAgBgBgBQAAAAAAgBQAAAAgBgBQAAAAgBAAQgBgCgEAAIgFAAg");
	this.shape_141.setTransform(-219.65,25.25);

	this.shape_142 = new cjs.Shape();
	this.shape_142.graphics.f("#FFF000").s().p("AgOANIAEgGQAHAFAFAAIADgBQAAAAABAAQAAgBAAAAQAAAAAAgBQAAAAAAAAQAAgBAAAAQAAgBAAAAQAAAAAAgBQgBAAAAAAIgFgCQgGgCgEgCQgDgCAAgGQAAgFAEgDQAEgDAFAAIAIABQAEABADADIgEAGQgFgEgGAAIgDABQAAAAAAABQAAAAgBAAQAAABAAAAQAAABAAAAQAAABAAAAQAAAAAAABQABAAAAAAQAAAAAAABIAHACQAGACACABQADADAAAFQAAAGgDADQgEADgGAAQgJAAgHgHg");
	this.shape_142.setTransform(-223.55,25.225);

	this.shape_143 = new cjs.Shape();
	this.shape_143.graphics.f("#FFF000").s().p("AgPACIAAgEIAfAAIAAAEg");
	this.shape_143.setTransform(-115.65,18.65);

	this.shape_144 = new cjs.Shape();
	this.shape_144.graphics.f("#FFF000").s().p("AgPACIAAgEIAfAAIAAAEg");
	this.shape_144.setTransform(-119.7,18.65);

	this.shape_145 = new cjs.Shape();
	this.shape_145.graphics.f("#FFF000").s().p("AgQACIAAgEIAhAAIAAAEg");
	this.shape_145.setTransform(-123.7,18.65);

	this.shape_146 = new cjs.Shape();
	this.shape_146.graphics.f("#FFF000").s().p("AAAAUIAAgfIgHAAIAAgIIAPAAIAAAng");
	this.shape_146.setTransform(-132.85,16.2);

	this.shape_147 = new cjs.Shape();
	this.shape_147.graphics.f("#FFF000").s().p("AgLAPQgEgGAAgJQAAgIAEgGQAEgFAHAAQAIAAAEAFQAEAGAAAIQAAAJgEAGQgEAFgIAAQgHAAgEgFgAgFgIQgCADAAAFQAAAGACADQACAEADAAQAEAAACgEQACgDAAgGQAAgFgCgDQgCgEgEAAQgDAAgCAEg");
	this.shape_147.setTransform(-135.725,16.175);

	this.shape_148 = new cjs.Shape();
	this.shape_148.graphics.f("#FFF000").s().p("AgDAEQAAgBAAAAQAAgBgBAAQAAgBAAAAQAAAAAAgBQAAAAAAAAQAAAAAAgBQABAAAAgBQAAAAAAAAQABgBAAAAQABAAAAgBQABAAAAAAQAAAAAAAAQAAAAABAAQAAAAABAAQAAABABAAQAAAAABABQAAAAAAAAQAAABABAAQAAABAAAAQAAAAAAAAQAAABAAAAQAAAAAAABQgBAAAAABQAAAAAAABQgBAAAAAAQgBAAAAABQgBAAAAAAQgBAAAAAAQAAAAAAAAQAAAAgBAAQAAgBgBAAQAAAAgBAAg");
	this.shape_148.setTransform(-139.95,17.7);

	this.shape_149 = new cjs.Shape();
	this.shape_149.graphics.f("#FFF000").s().p("AgPANIAGgGQAGAFAEAAIAEgBQAAAAABAAQAAgBAAAAQAAAAAAgBQABAAAAAAQAAgBgBAAQAAgBAAAAQAAAAAAgBQgBAAAAAAIgFgCQgHgCgCgCQgEgCAAgGQAAgFAEgDQAEgDAFAAIAIABQAEABADADIgFAGQgEgEgGAAIgCABQgBABAAAAQAAAAgBABQAAAAAAAAQAAABAAAAQAAABAAAAQAAAAAAABQABAAAAAAQAAABABAAIAGACQAFACADABQAEADAAAFQAAAGgFADQgDADgHAAQgIAAgIgHg");
	this.shape_149.setTransform(-142.6,16.175);

	this.shape_150 = new cjs.Shape();
	this.shape_150.graphics.f("#FFF000").s().p("AgOAOQgFgFAAgJQAAgIAFgFQAHgGAHAAQAJAAAGAGQAFAFAAAIQAAAJgFAFQgGAGgJAAQgHAAgHgGgAgHgIQgEAEABAEQgBAFAEAEQADAEAEAAQAFAAADgEQAEgEAAgFQAAgEgEgEQgDgEgFAAQgEAAgDAEg");
	this.shape_150.setTransform(-146.65,16.175);

	this.shape_151 = new cjs.Shape();
	this.shape_151.graphics.f("#FFF000").s().p("AgPAUIAAgnIAOAAQAIAAAEAEQAFADAAAIQgBAGgEADQgEADgHAAIgGAAIAAAMgAgGABIAGAAQAEAAABgBQABgBABgEQAAgBgBgBQAAAAAAgBQAAAAgBgBQAAAAAAgBQgDgBgDAAIgFAAg");
	this.shape_151.setTransform(-150.7,16.2);

	this.shape_152 = new cjs.Shape();
	this.shape_152.graphics.f("#FFF000").s().p("AgQACIAAgEIAhAAIAAAEg");
	this.shape_152.setTransform(-154.75,18.65);

	this.shape_153 = new cjs.Shape();
	this.shape_153.graphics.f("#FFF000").s().p("AgQACIAAgEIAhAAIAAAEg");
	this.shape_153.setTransform(-158.8,18.65);

	this.shape_154 = new cjs.Shape();
	this.shape_154.graphics.f("#FFF000").s().p("AgPACIAAgEIAfAAIAAAEg");
	this.shape_154.setTransform(-162.8,18.65);

	this.shape_155 = new cjs.Shape();
	this.shape_155.graphics.f("#FFF000").s().p("AgPACIAAgEIAfAAIAAAEg");
	this.shape_155.setTransform(-166.85,18.65);

	this.shape_156 = new cjs.Shape();
	this.shape_156.graphics.f("#FFF000").s().p("AgPACIAAgEIAfAAIAAAEg");
	this.shape_156.setTransform(-170.9,18.65);

	this.shape_157 = new cjs.Shape();
	this.shape_157.graphics.f("#FFF000").s().p("AgQACIAAgEIAgAAIAAAEg");
	this.shape_157.setTransform(-174.9,18.65);

	this.shape_158 = new cjs.Shape();
	this.shape_158.graphics.f("#FFF000").s().p("AgQACIAAgEIAgAAIAAAEg");
	this.shape_158.setTransform(-178.95,18.65);

	this.shape_159 = new cjs.Shape();
	this.shape_159.graphics.f("#FFF000").s().p("AgQACIAAgEIAgAAIAAAEg");
	this.shape_159.setTransform(-183,18.65);

	this.shape_160 = new cjs.Shape();
	this.shape_160.graphics.f("#FFF000").s().p("AgPACIAAgEIAgAAIAAAEg");
	this.shape_160.setTransform(-187,18.65);

	this.shape_161 = new cjs.Shape();
	this.shape_161.graphics.f("#FFF000").s().p("AgPACIAAgEIAgAAIAAAEg");
	this.shape_161.setTransform(-191.05,18.65);

	this.shape_162 = new cjs.Shape();
	this.shape_162.graphics.f("#FFF000").s().p("AgPACIAAgEIAgAAIAAAEg");
	this.shape_162.setTransform(-195.1,18.65);

	this.shape_163 = new cjs.Shape();
	this.shape_163.graphics.f("#FFF000").s().p("AgQACIAAgEIAgAAIAAAEg");
	this.shape_163.setTransform(-199.1,18.65);

	this.shape_164 = new cjs.Shape();
	this.shape_164.graphics.f("#FFF000").s().p("AgQACIAAgEIAgAAIAAAEg");
	this.shape_164.setTransform(-203.15,18.65);

	this.shape_165 = new cjs.Shape();
	this.shape_165.graphics.f("#FFF000").s().p("AgQACIAAgEIAhAAIAAAEg");
	this.shape_165.setTransform(-207.15,18.65);

	this.shape_166 = new cjs.Shape();
	this.shape_166.graphics.f("#FFF000").s().p("AgQACIAAgEIAhAAIAAAEg");
	this.shape_166.setTransform(-211.2,18.65);

	this.shape_167 = new cjs.Shape();
	this.shape_167.graphics.f("#FFF000").s().p("AgQACIAAgEIAhAAIAAAEg");
	this.shape_167.setTransform(-215.25,18.65);

	this.shape_168 = new cjs.Shape();
	this.shape_168.graphics.f("#FFF000").s().p("AgPACIAAgEIAfAAIAAAEg");
	this.shape_168.setTransform(-219.25,18.65);

	this.shape_169 = new cjs.Shape();
	this.shape_169.graphics.f("#FFF000").s().p("AgPACIAAgEIAfAAIAAAEg");
	this.shape_169.setTransform(-223.3,18.65);

	this.shape_170 = new cjs.Shape();
	this.shape_170.graphics.f("#FFF000").s().p("AgcAEIAAgHIA5AAIAAAHg");
	this.shape_170.setTransform(-69.25,-152.575);

	this.shape_171 = new cjs.Shape();
	this.shape_171.graphics.f("#FFF000").s().p("AgcAEIAAgHIA5AAIAAAHg");
	this.shape_171.setTransform(-76.45,-152.575);

	this.shape_172 = new cjs.Shape();
	this.shape_172.graphics.f("#FFF000").s().p("AgcAEIAAgHIA5AAIAAAHg");
	this.shape_172.setTransform(-83.65,-152.575);

	this.shape_173 = new cjs.Shape();
	this.shape_173.graphics.f().s("#FFFFFF").ss(1,1,1).p("AW3ISI4mONI1HzBILk58IcQC+g");
	this.shape_173.setTransform(-22.675,68.825);

	this.shape_174 = new cjs.Shape();
	this.shape_174.graphics.f().s("#FFFFFF").ss(1,1,1).p("AnFmSICWm8ISbU+I7XFfIAAgB");
	this.shape_174.setTransform(-240,-49.65);

	this.shape_175 = new cjs.Shape();
	this.shape_175.graphics.f("#FFFFFF").s().p("AqGJxIGnzhIAAAAINmPeI0MEDg");
	this.shape_175.setTransform(-262.95,-27.55);

	this.shape_176 = new cjs.Shape();
	this.shape_176.graphics.f().s("#FFFFFF").ss(1,1,1).p("ALnNZI3NtWIXJtbg");
	this.shape_176.setTransform(-577,114.55);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_126},{t:this.shape_125},{t:this.shape_124},{t:this.shape_123,p:{x:-485}},{t:this.shape_122,p:{x:-477.8}},{t:this.shape_121,p:{x:-470.6}},{t:this.shape_120,p:{x:-463.4}},{t:this.shape_119,p:{x:-456.2}},{t:this.shape_118,p:{x:-449}},{t:this.shape_117,p:{x:-441.8}},{t:this.shape_116,p:{x:-434.6}},{t:this.shape_115,p:{x:36.35,y:-22.575}},{t:this.shape_114,p:{x:43.55,y:-22.575}},{t:this.shape_113,p:{x:50.75,y:-22.575}}]},59).to({state:[]},6).to({state:[{t:this.shape_123,p:{x:-506.6}},{t:this.shape_122,p:{x:-499.4}},{t:this.shape_121,p:{x:-492.2}},{t:this.shape_120,p:{x:-485}},{t:this.shape_119,p:{x:-477.8}},{t:this.shape_118,p:{x:-470.6}},{t:this.shape_117,p:{x:-463.4}},{t:this.shape_116,p:{x:-456.2}},{t:this.shape_115,p:{x:-449,y:173.175}},{t:this.shape_114,p:{x:-441.8,y:173.175}},{t:this.shape_113,p:{x:-434.6,y:173.175}},{t:this.shape_172},{t:this.shape_171},{t:this.shape_170},{t:this.shape_169},{t:this.shape_168},{t:this.shape_167},{t:this.shape_166},{t:this.shape_165},{t:this.shape_164},{t:this.shape_163},{t:this.shape_162},{t:this.shape_161},{t:this.shape_160},{t:this.shape_159},{t:this.shape_158},{t:this.shape_157},{t:this.shape_156},{t:this.shape_155},{t:this.shape_154},{t:this.shape_153},{t:this.shape_152},{t:this.shape_151},{t:this.shape_150},{t:this.shape_149},{t:this.shape_148},{t:this.shape_147},{t:this.shape_146},{t:this.shape_145},{t:this.shape_144},{t:this.shape_143},{t:this.shape_142},{t:this.shape_141},{t:this.shape_140},{t:this.shape_139},{t:this.shape_138},{t:this.shape_137},{t:this.shape_136},{t:this.shape_135},{t:this.shape_134},{t:this.shape_133},{t:this.shape_132},{t:this.shape_131},{t:this.shape_130},{t:this.shape_129},{t:this.shape_128},{t:this.shape_127}]},11).to({state:[{t:this.shape_173}]},17).to({state:[{t:this.shape_175},{t:this.shape_174}]},4).to({state:[{t:this.shape_176}]},3).to({state:[]},6).to({state:[]},1).wait(14));

	// masque Titre (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_17 = new cjs.Graphics().p("EgpOArJMAAAhWRMAvJAAAMAAABWRg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(17).to({graphics:mask_graphics_17,x:-263.875,y:-6.1095}).wait(24).to({graphics:null,x:0,y:0}).wait(80));

	// _LETTRES_ANARCHIQUES
	this.instance_2 = new lib.lettres_FAT();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-433.85,-73.05,1,1,0,0,0,0,186.8);
	this.instance_2._off = true;

	this.shape_177 = new cjs.Shape();
	this.shape_177.graphics.f("#FFF000").s().p("AjeEDQhohlgBicQABibBqhnQBqhpCaAAQCrAAB1CEIhiBuQhJhchuAAQhYgBg+A6Qg+A6gBBgQAABhA8A8QA6A6BTAAQB0ABBIhcIBkBoQh2CFiiAAQigABhphng");
	this.shape_177.setTransform(-336.15,7.45);

	this.shape_178 = new cjs.Shape();
	this.shape_178.graphics.f("#FFF000").s().p("AByFeIiajfIhsAAIAADfIidAAIAAq6IEKAAQCiAABFA2QBFA4AAB6QAACmiEAyICwD6gAiUgHIBxAAQBPAAAdgaQAegbAAg4QAAg4gfgWQgegUhJAAIh1AAg");
	this.shape_178.setTransform(-495.65,7.85);

	this.shape_179 = new cjs.Shape();
	this.shape_179.graphics.f("#FFF000").s().p("ACYFdIiYnlIiYHlIhzAAIj1q5ICpAAICLGQIB7mQICiAAIB7GQICMmQICoAAIj0K5g");
	this.shape_179.setTransform(-368.65,-116);

	this.shape_180 = new cjs.Shape();
	this.shape_180.graphics.f("#FFF000").s().p("AjeEDQhohlAAicQAAiaBqhoQBrhpCYAAQCtAABzCEIhgBtQhLhchuABQhWAAg/A5Qg/A6ABBgQAABiA6A7QA7A6BTAAQBzAABKhbIBjBoQh3CGihAAQigAAhphng");
	this.shape_180.setTransform(-280.75,-17.55);

	this.shape_181 = new cjs.Shape();
	this.shape_181.graphics.f("#FFF000").s().p("AByFeIiajgIhsAAIAADgIidAAIAAq7IEKAAQCiAABFA3QBFA3AAB7QAACmiFAxICxD7gAiUgIIBxAAQBPAAAdgaQAegZAAg5QgBg4gegWQgegUhJgBIh1AAg");
	this.shape_181.setTransform(-363.45,-17.15);

	this.shape_182 = new cjs.Shape();
	this.shape_182.graphics.f("#FFF000").s().p("ACYFeIiYnlIiYHlIh0AAIj0q7ICpAAICKGSIB8mSICiAAIB7GSICLmSICpAAIj0K7g");
	this.shape_182.setTransform(-466.85,-141);

	var maskedShapeInstanceList = [this.instance_2,this.shape_177,this.shape_178,this.shape_179,this.shape_180,this.shape_181,this.shape_182];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_2}]},17).to({state:[{t:this.instance_2}]},14).to({state:[{t:this.shape_179},{t:this.shape_178},{t:this.shape_177}]},1).to({state:[{t:this.shape_182},{t:this.shape_181},{t:this.shape_180}]},7).to({state:[]},9).to({state:[]},32).wait(41));
	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(17).to({_off:false},0).to({x:-276.85},14,cjs.Ease.quartOut).to({_off:true},1).wait(89));

	// masque Titre
	this.instance_3 = new lib.masque_generique();
	this.instance_3.parent = this;
	this.instance_3.setTransform(221.1,-163.9,0.0128,0.4711,0,0,0,0,17.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({regX:0.5,scaleX:1.9223,x:-75.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.0074,x:-68.7},10,cjs.Ease.quartInOut).to({_off:true},1).wait(101));

	// masque Titre
	this.instance_4 = new lib.masque_generique();
	this.instance_4.parent = this;
	this.instance_4.setTransform(-228.9,-13.9,0.0128,0.4711,0,0,0,0,17.2);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(4).to({_off:false},0).to({regX:0.5,scaleX:1.9223,x:-527.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.0074,x:-528.7},10,cjs.Ease.quartInOut).to({_off:true},1).wait(97));

	// _LETTRES_CLIENT
	this.shape_183 = new cjs.Shape();
	this.shape_183.graphics.f("#5FAFE5").s().p("AgJA0QgEgEAAgGQAAgFAEgFQAEgEAFAAQAGAAAEAEQAEAFAAAFQAAAGgEAEQgEAFgGAAQgFAAgEgFgAgJAVIgEg5IAAgUIAbAAIAAAUIgFA5g");
	this.shape_183.setTransform(-357.925,-14.075);

	this.shape_184 = new cjs.Shape();
	this.shape_184.graphics.f("#5FAFE5").s().p("AAbA4Ig1hFIAABFIgZAAIAAhvIAYAAIA2BHIAAhHIAZAAIAABvg");
	this.shape_184.setTransform(-378.95,-14.15);

	this.shape_185 = new cjs.Shape();
	this.shape_185.graphics.f("#5FAFE5").s().p("AgoA4IAAhvIBPAAIAAAXIg2AAIAAAWIAxAAIAAAUIgxAAIAAAYIA4AAIAAAWg");
	this.shape_185.setTransform(-402.45,-14.15);

	this.shape_186 = new cjs.Shape();
	this.shape_186.graphics.f("#5FAFE5").s().p("AgMA4IAAhvIAZAAIAABvg");
	this.shape_186.setTransform(-422.55,-14.15);

	this.shape_187 = new cjs.Shape();
	this.shape_187.graphics.f("#5FAFE5").s().p("AgCA5IADgYIgNAAIgEAYIgZAAIAFgYIgQAAIAFgWIAPAAIADgTIgRAAIAEgWIARAAIAEgaIAXAAIgDAaIANAAIAEgaIAZAAIgFAaIARAAIgEAWIgRAAIgDATIASAAIgEAWIgSAAIgEAYgAgJALIAOAAIAEgTIgOAAg");
	this.shape_187.setTransform(-459.375,-14.275);

	this.shape_188 = new cjs.Shape();
	this.shape_188.graphics.f("#5FAFE5").s().p("AgjA4IAAhvIAZAAIAABYIAuAAIAAAXg");
	this.shape_188.setTransform(-497.625,-14.15);

	this.shape_189 = new cjs.Shape();
	this.shape_189.graphics.f("#5FAFE5").s().p("AgjApQgRgQAAgYQAAgZARgQQASgRAXAAQAcAAASAVIgPASQgMgPgSAAQgNAAgKAJQgJAJgBAPQAAAPAKAKQAJAKANAAQASAAALgPIAQARQgTAVgZAAQgZAAgRgRg");
	this.shape_189.setTransform(-520.4,-14.2);

	this.shape_190 = new cjs.Shape();
	this.shape_190.graphics.f("#5FAFE5").s().p("AgMA4IAAhZIgfAAIAAgWIBXAAIAAAWIggAAIAABZg");
	this.shape_190.setTransform(-411.925,-14.15);

	this.shape_191 = new cjs.Shape();
	this.shape_191.graphics.f("#5FAFE5").s().p("AAbA4Ig0hFIAABFIgaAAIAAhvIAYAAIA2BHIAAhHIAZAAIAABvg");
	this.shape_191.setTransform(-435.3,-14.15);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_189},{t:this.shape_188},{t:this.shape_187},{t:this.shape_186,p:{x:-422.55}},{t:this.shape_185,p:{x:-402.45}},{t:this.shape_184},{t:this.shape_183,p:{x:-357.925}}]},13).to({state:[{t:this.shape_189},{t:this.shape_188},{t:this.shape_187},{t:this.shape_183,p:{x:-422.675}}]},5).to({state:[{t:this.shape_189},{t:this.shape_188},{t:this.shape_186,p:{x:-478.9}},{t:this.shape_185,p:{x:-458.8}},{t:this.shape_191},{t:this.shape_190}]},5).to({state:[]},20).to({state:[]},37).wait(41));

	// Layer_11
	this.instance_5 = new lib.masqueTexte("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(-328.95,105.85,0.8999,0.85,0,0,0,0.1,108.9);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(37).to({_off:false},0).to({_off:true},40).wait(3).to({_off:false,regX:0.2,regY:108.8,rotation:180,x:-329.05,y:105.8},0).to({_off:true},40).wait(1));

	// _TEXTE_PRESENTATION
	this.shape_192 = new cjs.Shape();
	this.shape_192.graphics.f("#FFFFFF").s().p("AhYBoQgqgpAAg+QAAg9AqgqQArgqA9AAQBFAAAuA1IgmAsQgeglgtAAQghAAgZAXQgZAXAAAmQgBAnAYAYQAYAXAhAAQAtAAAdgkIApApQgwA2hBAAQhAAAgpgpg");
	this.shape_192.setTransform(-213.5,130.05);

	this.shape_193 = new cjs.Shape();
	this.shape_193.graphics.f("#FFFFFF").s().p("AAuCMIg+haIgrAAIAABaIg+AAIAAkXIBqAAQBAAAAcAWQAbAWAAAxQAABCg0AUIBGBkgAg7gCIAuAAQAfAAALgLQAMgKAAgXQAAgWgMgJQgMgIgdAAIgvAAg");
	this.shape_193.setTransform(-241.8,130.225);

	this.shape_194 = new cjs.Shape();
	this.shape_194.graphics.f("#FFFFFF").s().p("AA9CMIg9jCIg8DCIgvAAIhhkXIBDAAIA3CgIAzigIA/AAIAxCgIA4igIBEAAIhiEXg");
	this.shape_194.setTransform(-277.75,130.225);

	this.shape_195 = new cjs.Shape();
	this.shape_195.graphics.f("#FFFFFF").s().p("AgqBmQgagIgVgRIAbgmQAhAaAmAAQALAAAHgEQAGgFAAgHQAAgIgJgGQgKgHgQgFIgcgKQgJgEgLgIQgZgOAAgeQAAgdAYgTQAYgTAlAAQAmAAAmAaIgWAoQgcgUgdAAQgaAAAAAQQAAAIAJAFQAJAEATAHIAdAKQAIADAMAHQAXANAAAgQAAAggYATQgXATgkAAQgXAAgagJg");
	this.shape_195.setTransform(-319.4,133.425);

	this.shape_196 = new cjs.Shape();
	this.shape_196.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAggeQAhgeArAAQAtAAAfAbQAfAbAAAuIAAAfIicAAQADASAPALQAQALATAAQAfAAAVgVIAiAlQglAigxAAQgwAAgggegAgdgyQgPALgCATIBdAAQgCgUgNgKQgMgKgSAAQgQAAgPAKg");
	this.shape_196.setTransform(-341.7,133.425);

	this.shape_197 = new cjs.Shape();
	this.shape_197.graphics.f("#FFFFFF").s().p("AhvB8IAbgsQAQAPAPgBQAJABAGgJQAGgHAAgKQAAgJhUjRIBAAAIA0CGIA1iGIBAAAIhkD9QgJAWgSAMQgSAMgYAAQgfAAgcgag");
	this.shape_197.setTransform(-365.9,137.7);

	this.shape_198 = new cjs.Shape();
	this.shape_198.graphics.f("#FFFFFF").s().p("AgdCUIAAkoIA7AAIAAEog");
	this.shape_198.setTransform(-383.5,129.35);

	this.shape_199 = new cjs.Shape();
	this.shape_199.graphics.f("#FFFFFF").s().p("AgdCUIAAkoIA7AAIAAEog");
	this.shape_199.setTransform(-395.15,129.35);

	this.shape_200 = new cjs.Shape();
	this.shape_200.graphics.f("#FFFFFF").s().p("AhJBbQgXgUAAghQAAghAYgPQAYgQAqAAIArAAIAAgCQAAgigkAAQgPAAgRAGQgSAGgLAKIgbgnQAqgfA2AAQAnAAAZAUQAYATAAArIAACIIg4AAIAAgaQgYAdggAAQgjAAgXgUgAgkAmQAAALAJAHQAIAGAQAAQAQAAALgKQANgKAAgQIAAgLIglAAQgkAAAAAXg");
	this.shape_200.setTransform(-413.35,133.425);

	this.shape_201 = new cjs.Shape();
	this.shape_201.graphics.f("#FFFFFF").s().p("Ag9BtIAAjWIA8AAIAAAdQAKgOASgJQARgJASAAIAAA4IgLAAQgbAAgNATQgMATAAAeIAABdg");
	this.shape_201.setTransform(-432.125,133.275);

	this.shape_202 = new cjs.Shape();
	this.shape_202.graphics.f("#FFFFFF").s().p("AgqBmQgagIgVgRIAagmQAjAaAkAAQAMAAAGgEQAIgFAAgHQAAgIgKgGQgJgHgRgFIgbgKQgKgEgMgIQgYgOAAgeQAAgdAYgTQAYgTAlAAQAmAAAmAaIgXAoQgcgUgcAAQgaAAAAAQQAAAIAJAFQAIAEAUAHIAdAKQAJADAMAHQAWANAAAgQAAAggXATQgYATgkAAQgXAAgagJg");
	this.shape_202.setTransform(-462.45,133.425);

	this.shape_203 = new cjs.Shape();
	this.shape_203.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAggeQAhgeAsAAQAsAAAfAbQAfAbAAAuIAAAfIicAAQADASAPALQAQALATAAQAfAAAVgVIAhAlQgkAigxAAQgwAAgggegAgdgyQgPALgDATIBeAAQgCgUgNgKQgMgKgRAAQgSAAgOAKg");
	this.shape_203.setTransform(-484.75,133.425);

	this.shape_204 = new cjs.Shape();
	this.shape_204.graphics.f("#FFFFFF").s().p("AhQB2QgeggAAgwQAAgwAdgeQAegeAngBQAmAAAZAcIAAhqIA8AAIAAEoIg8AAIAAgcQgaAfglAAQgmAAgegggAgigDQgPARAAAaQAAAaAPARQAQAQATAAQAVAAAPgQQAOgRAAgaQAAgagOgRQgPgSgVAAQgUAAgPASg");
	this.shape_204.setTransform(-511.125,129.5);

	this.shape_205 = new cjs.Shape();
	this.shape_205.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAggeQAhgeArAAQAtAAAfAbQAfAbAAAuIAAAfIicAAQADASAPALQAQALATAAQAfAAAVgVIAiAlQglAigxAAQgwAAgggegAgdgyQgPALgCATIBdAAQgCgUgNgKQgMgKgSAAQgQAAgPAKg");
	this.shape_205.setTransform(-222,84.675);

	this.shape_206 = new cjs.Shape();
	this.shape_206.graphics.f("#FFFFFF").s().p("AgdBsIhWjXIA+AAIA1CHIA2iHIA+AAIhVDXg");
	this.shape_206.setTransform(-246.2,84.675);

	this.shape_207 = new cjs.Shape();
	this.shape_207.graphics.f("#FFFFFF").s().p("AgdCXIAAjWIA7AAIAADWgAgYhaQgKgKAAgPQAAgPAKgKQAKgKAOAAQAPAAAKAKQAKAKAAAPQAAAPgKAKQgKAKgPAAQgOAAgKgKg");
	this.shape_207.setTransform(-263.725,80.325);

	this.shape_208 = new cjs.Shape();
	this.shape_208.graphics.f("#FFFFFF").s().p("AgdCVIAAkoIA7AAIAAEog");
	this.shape_208.setTransform(-275.35,80.6);

	this.shape_209 = new cjs.Shape();
	this.shape_209.graphics.f("#FFFFFF").s().p("AggB5QgVgUAAgkIAAhiIgZAAIAAgrIAZAAIAAhAIA7AAIAABAIA0AAIAAArIg0AAIAABgQAAAMAHAHQAHAHAIAAQARAAALgPIAXAqQgcAZgfAAQgeAAgWgUg");
	this.shape_209.setTransform(-299.95,81.625);

	this.shape_210 = new cjs.Shape();
	this.shape_210.graphics.f("#FFFFFF").s().p("AApBtIAAh4QAAgyglAAQgRAAgNANQgNANAAAZIAAB3Ig9AAIAAjWIA9AAIAAAYQAagbAiAAQAhAAAYAYQAXAYgBAnIAACCg");
	this.shape_210.setTransform(-321.95,84.525);

	this.shape_211 = new cjs.Shape();
	this.shape_211.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAggeQAhgeArAAQAtAAAfAbQAfAbAAAuIAAAfIicAAQADASAPALQAQALATAAQAfAAAVgVIAiAlQglAigxAAQgwAAgggegAgdgyQgPALgCATIBdAAQgCgUgNgKQgMgKgSAAQgQAAgPAKg");
	this.shape_211.setTransform(-347.1,84.675);

	this.shape_212 = new cjs.Shape();
	this.shape_212.graphics.f("#FFFFFF").s().p("ABtBtIAAh0QAAgygkAAQgSAAgNANQgOANAAAZIAABzIg7AAIAAh0QAAgagIgMQgIgMgRAAQgSAAgNANQgNANAAAZIAABzIg8AAIAAjWIA8AAIAAAYQAZgbAfAAQAVAAAQALQAQANAIARQANgUAVgLQAVgKAVAAQAmAAAXAWQAXAXAAAqIAACCg");
	this.shape_212.setTransform(-379.075,84.525);

	this.shape_213 = new cjs.Shape();
	this.shape_213.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAggeQAhgeAsAAQAsAAAfAbQAfAbAAAuIAAAfIicAAQADASAPALQAQALATAAQAfAAAVgVIAiAlQglAigxAAQgwAAgggegAgdgyQgPALgDATIBeAAQgCgUgNgKQgMgKgSAAQgQAAgPAKg");
	this.shape_213.setTransform(-411.1,84.675);

	this.shape_214 = new cjs.Shape();
	this.shape_214.graphics.f("#FFFFFF").s().p("AgqBmQgagIgVgRIAbgmQAhAaAmAAQALAAAHgEQAGgFABgHQAAgIgKgGQgKgHgQgFIgbgKQgKgEgMgIQgYgOAAgeQAAgdAYgTQAYgTAlAAQAmAAAmAaIgXAoQgbgUgcAAQgbAAAAAQQAAAIAJAFQAJAEATAHIAdAKQAJADALAHQAXANAAAgQAAAggYATQgXATgkAAQgXAAgagJg");
	this.shape_214.setTransform(-434.3,84.675);

	this.shape_215 = new cjs.Shape();
	this.shape_215.graphics.f("#FFFFFF").s().p("AgqBmQgagIgVgRIAbgmQAhAaAmAAQALAAAHgEQAGgFABgHQAAgIgKgGQgKgHgQgFIgcgKQgJgEgMgIQgYgOAAgeQAAgdAYgTQAYgTAlAAQAmAAAmAaIgWAoQgcgUgcAAQgbAAAAAQQAAAIAJAFQAJAEATAHIAdAKQAIADAMAHQAXANAAAgQAAAggYATQgXATgkAAQgXAAgagJg");
	this.shape_215.setTransform(-455.1,84.675);

	this.shape_216 = new cjs.Shape();
	this.shape_216.graphics.f("#FFFFFF").s().p("AhJBbQgXgUAAghQAAghAYgPQAYgQApAAIAsAAIAAgCQAAgigkAAQgPAAgRAGQgRAGgNAKIgbgnQArgfA2AAQAmAAAZAUQAZATAAArIAACIIg4AAIAAgaQgXAdgiAAQghAAgYgUgAgkAmQAAALAJAHQAIAGARAAQAPAAAMgKQAMgKAAgQIAAgLIglAAQgkAAAAAXg");
	this.shape_216.setTransform(-477.5,84.675);

	this.shape_217 = new cjs.Shape();
	this.shape_217.graphics.f("#FFFFFF").s().p("AgdCVIAAkoIA7AAIAAEog");
	this.shape_217.setTransform(-494.7,80.6);

	this.shape_218 = new cjs.Shape();
	this.shape_218.graphics.f("#FFFFFF").s().p("AhEBQQgigeAAgyQAAgwAigfQAigfAtAAQAXAAAYAKQAYAJASATIgfApQgLgNgPgGQgOgHgPAAQgXAAgRAPQgRAQAAAaQAAAbARAQQARAPAWAAQAeAAAYgeIAkAoQgqArgwAAQgwAAghgfg");
	this.shape_218.setTransform(-511.975,84.675);

	this.shape_219 = new cjs.Shape();
	this.shape_219.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAhgeQAggeArAAQAtAAAfAbQAfAbAAAuIAAAfIicAAQADASAQALQAPALATAAQAfAAAVgVIAhAlQgkAigxAAQgwAAgggegAgdgyQgPALgDATIBeAAQgCgUgMgKQgNgKgRAAQgRAAgPAKg");
	this.shape_219.setTransform(-313.85,35.925);

	this.shape_220 = new cjs.Shape();
	this.shape_220.graphics.f("#FFFFFF").s().p("AhQB2QgeghAAgwQAAguAdggQAegeAnAAQAmABAZAbIAAhqIA8AAIAAEoIg8AAIAAgdQgaAgglAAQgmAAgegggAgigDQgPARAAAaQAAAaAPAQQAQARATAAQAVAAAPgRQAOgQAAgbQAAgZgOgRQgPgRgVAAQgUAAgPARg");
	this.shape_220.setTransform(-340.225,32);

	this.shape_221 = new cjs.Shape();
	this.shape_221.graphics.f("#FFFFFF").s().p("AhMBXQgYgWAAgsIAAiBIA9AAIAAB0QAAAyAkgBQARABANgNQANgNAAgZIAAhzIA9AAIAADWIg9AAIAAgdQgWAggiAAQgkAAgYgWg");
	this.shape_221.setTransform(-376.375,36.1);

	this.shape_222 = new cjs.Shape();
	this.shape_222.graphics.f("#FFFFFF").s().p("AhJBbQgXgUAAghQAAghAYgPQAYgQAqAAIArAAIAAgCQAAgigkAAQgPAAgRAGQgRAGgMAKIgbgnQAqgfA1AAQAnAAAaAUQAYATAAArIAACIIg4AAIAAgaQgYAdggAAQgiAAgYgUgAgkAmQAAALAIAHQAKAGAPAAQAPAAAMgKQANgKAAgQIAAgLIglAAQgkAAAAAXg");
	this.shape_222.setTransform(-401.4,35.925);

	this.shape_223 = new cjs.Shape();
	this.shape_223.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAggeQAhgeAsAAQAsAAAfAbQAfAbAAAuIAAAfIicAAQADASAPALQAQALATAAQAfAAAVgVIAiAlQglAigxAAQgwAAgggegAgdgyQgPALgDATIBeAAQgCgUgNgKQgMgKgSAAQgQAAgPAKg");
	this.shape_223.setTransform(-425.05,35.925);

	this.shape_224 = new cjs.Shape();
	this.shape_224.graphics.f("#FFFFFF").s().p("AgdCVIAAkoIA7AAIAAEog");
	this.shape_224.setTransform(-443.3,31.85);

	this.shape_225 = new cjs.Shape();
	this.shape_225.graphics.f("#FFFFFF").s().p("AgyB2IAAAdIg8AAIAAkoIA8AAIAABqQAZgbAmgBQAnAAAdAeQAeAgAAAuQAAAwgeAhQgeAggmAAQgmAAgZgggAgkgDQgPARAAAZQAAAbAPAQQAPARAUAAQAUAAAQgRQAPgQAAgaQAAgagPgRQgPgRgUAAQgVAAgPARg");
	this.shape_225.setTransform(-461.875,32);

	this.shape_226 = new cjs.Shape();
	this.shape_226.graphics.f("#FFFFFF").s().p("AhJBbQgXgUAAghQAAghAYgPQAYgQAqAAIArAAIAAgCQAAgigkAAQgPAAgRAGQgSAGgLAKIgbgnQAqgfA2AAQAmAAAaAUQAYATAAArIAACIIg4AAIAAgaQgYAdggAAQgjAAgXgUgAgkAmQAAALAIAHQAKAGAPAAQAPAAAMgKQANgKAAgQIAAgLIglAAQgkAAAAAXg");
	this.shape_226.setTransform(-488.15,35.925);

	this.shape_227 = new cjs.Shape();
	this.shape_227.graphics.f("#FFFFFF").s().p("AgeCMIAAjhIhPAAIAAg2IDbAAIAAA2IhPAAIAADhg");
	this.shape_227.setTransform(-511.7,32.725);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_227},{t:this.shape_226},{t:this.shape_225},{t:this.shape_224},{t:this.shape_223},{t:this.shape_222},{t:this.shape_221},{t:this.shape_220},{t:this.shape_219},{t:this.shape_218},{t:this.shape_217},{t:this.shape_216},{t:this.shape_215},{t:this.shape_214},{t:this.shape_213},{t:this.shape_212},{t:this.shape_211},{t:this.shape_210},{t:this.shape_209},{t:this.shape_208},{t:this.shape_207},{t:this.shape_206},{t:this.shape_205},{t:this.shape_204},{t:this.shape_203},{t:this.shape_202},{t:this.shape_201},{t:this.shape_200},{t:this.shape_199},{t:this.shape_198},{t:this.shape_197},{t:this.shape_196},{t:this.shape_195},{t:this.shape_194},{t:this.shape_193},{t:this.shape_192}]},57).to({state:[]},43).to({state:[]},1).wait(20));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-652.3,-259.8,878.4,570.5);


// stage content:
(lib.FS_projet_michelin_wrc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":150});

	// timeline functions:
	this.frame_99 = function() {
		//var frequency = 60;
		//stage.enableMouseOver(frequency);
		
		// EVENT DE CLIC POUR LA SUITE
		this.next_btn.addEventListener("click", fl_ClickToGoToAndPlayFromFrame.bind(this));
		this.next_btn.addEventListener("mouseover", fl_MouseOverHandler.bind(this));
		this.next_btn.addEventListener("mouseout", fl_MouseOutHandler.bind(this));
		
		
		function fl_ClickToGoToAndPlayFromFrame()
		{
			//this.gotoAndPlay("close");
			
			var event = new Event('next');
			this.dispatchEvent(event);		
			event = null;
			
			this.next_btn.btnNext.gotoAndPlay('rollOut');
			
		}
		function fl_MouseOverHandler(){	this.next_btn.btnNext.gotoAndPlay('rollOver'); }
		function fl_MouseOutHandler(){	this.next_btn.btnNext.gotoAndPlay('rollOut'); }
		
		
		// EVENT DE CLIC POUR LE LIEN
		this.cta_mc.addEventListener("click", projectDetails.bind(this));
		this.cta_mc.addEventListener("mouseover", pDetails_MouseOverHandler.bind(this));
		this.cta_mc.addEventListener("mouseout", pDetails_MouseOutHandler.bind(this));
		
		function projectDetails()
		{	
			var event = new Event('details');
			this.dispatchEvent(event);		
			event = null;
			
			this.cta_mc.btnCta.gotoAndPlay('rollOut');
			
		}
		function pDetails_MouseOverHandler(){	this.cta_mc.btnCta.gotoAndPlay('rollOver'); }
		function pDetails_MouseOutHandler(){	this.cta_mc.btnCta.gotoAndPlay('rollOut'); }
	}
	this.frame_146 = function() {
		//PAUSE
		this.stop();
	}
	this.frame_150 = function() {
		this.textes_mc.gotoAndPlay("close");
		this.visuel_mc.gotoAndPlay("close");
		this.cta_mc.gotoAndPlay("close");
		this.video_mc.gotoAndPlay("close");
		this.next_btn.gotoAndPlay("close");
		this.lignes_mc.gotoAndPlay("close_lines");
	}
	this.frame_169 = function() {
		//this.lignes_mc.gotoAndPlay("close");
	}
	this.frame_211 = function() {
		// EVENT DE FIN D'ANIM
		var event = new Event('closed');
		this.dispatchEvent(event);		
		event = null;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(99).call(this.frame_99).wait(47).call(this.frame_146).wait(4).call(this.frame_150).wait(19).call(this.frame_169).wait(42).call(this.frame_211).wait(1));

	// BLOC INFOS TEXTE
	this.textes_mc = new lib.blocInfostextes();
	this.textes_mc.name = "textes_mc";
	this.textes_mc.parent = this;
	this.textes_mc.setTransform(858.2,182,1,1,0,0,0,222.1,-164);
	this.textes_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.textes_mc).wait(17).to({_off:false},0).wait(195));

	// masque VISUEL
	this.visuel_mc = new lib.bloc_visuel();
	this.visuel_mc.name = "visuel_mc";
	this.visuel_mc.parent = this;
	this.visuel_mc.setTransform(1192.55,391.5,1,1,0,0,0,552.5,26.5);
	this.visuel_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.visuel_mc).wait(17).to({_off:false},0).wait(195));

	// CTA NEXT
	this.next_btn = new lib.bloc_NEXT_btn();
	this.next_btn.name = "next_btn";
	this.next_btn.parent = this;
	this.next_btn.setTransform(753.2,786.9,1,1,0,0,0,-340.5,403.9);
	this.next_btn._off = true;

	this.timeline.addTween(cjs.Tween.get(this.next_btn).wait(89).to({_off:false},0).wait(123));

	// CTA DETAIL
	this.cta_mc = new lib.bloc_CTA();
	this.cta_mc.name = "cta_mc";
	this.cta_mc.parent = this;
	this.cta_mc.setTransform(299.5,787,1,1,0,0,0,-340.5,403.9);
	this.cta_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.cta_mc).wait(99).to({_off:false},0).to({_off:true},74).wait(39));

	// VIDEO
	this.video_mc = new lib.bloc_video();
	this.video_mc.name = "video_mc";
	this.video_mc.parent = this;
	this.video_mc.setTransform(866.7,1147.15,1,1,0,0,0,227.5,764.9);
	this.video_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.video_mc).wait(94).to({_off:false},0).to({_off:true},79).wait(39));

	// lignes
	this.lignes_mc = new lib.lignes_all();
	this.lignes_mc.name = "lignes_mc";
	this.lignes_mc.parent = this;
	this.lignes_mc.setTransform(640,384.3,1,1,0,0,0,0,406.3);

	this.timeline.addTween(cjs.Tween.get(this.lignes_mc).wait(212));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(822.4,296,272.19999999999993,1167.3);
// library properties:
lib.properties = {
	id: 'B83F5258FCC81F4898724F16BBA8FDA7',
	width: 1280,
	height: 768,
	fps: 60,
	color: "#000000",
	opacity: 1.00,
	manifest: [
		{src:"images/WRC_cloud.png", id:"WRC_cloud"},
		{src:"images/WRC_route.png", id:"WRC_route"},
		{src:"images/WRC_voiture.png", id:"WRC_voiture"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['B83F5258FCC81F4898724F16BBA8FDA7'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;