(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.archivesDep_parchemin = function() {
	this.initialize(img.archivesDep_parchemin);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,600,400);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.video = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.008)").s().p("EgtTBLAMAAAiV/MBanAAAMAAACV/g");
	this.shape.setTransform(290,480);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.video, new cjs.Rectangle(0,0,580,960), null);


(lib.txt_hashtag = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgmAiIANgQQAQAOANAAQAGgBADgDQADgCAAgFQAAgEgDgCQgEgDgKgDQgRgDgIgGQgIgGAAgOQAAgPALgHQAKgIANAAQAKAAAKAEQAKADAHAHIgLAQQgNgKgNAAQgFAAgDACQgDADAAAEQAAAEAEADQAEACANAEQAOADAHAGQAIAHAAANQAAANgKAJQgKAHgQABQgWAAgTgRg");
	this.shape.setTransform(60.8,52.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAXAxIgtg9IAAA9IgWAAIAAhhIAUAAIAvA+IAAg+IAWAAIAABhg");
	this.shape_1.setTransform(50.7,52.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgjAkQgPgPAAgVQAAgUAPgPQAOgPAVAAQAWAAAPAPQAOAPAAAUQAAAVgOAPQgPAPgWAAQgVAAgOgPgAgTgVQgJAJAAAMQAAANAJAKQAIAIALABQAMgBAJgIQAIgKAAgNQAAgMgIgJQgJgJgMAAQgLAAgIAJg");
	this.shape_2.setTransform(39.2,52.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgKAxIAAhhIAVAAIAABhg");
	this.shape_3.setTransform(31.2,52.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgKAxIAAhOIgbAAIAAgTIBLAAIAAATIgbAAIAABOg");
	this.shape_4.setTransform(24.6,52.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgeAkQgPgOAAgWQAAgVAPgOQAPgPAUAAQAZAAAQASIgOAQQgKgMgQAAQgLgBgIAJQgKAHABANQAAANAIAJQAJAIAKAAQAQAAAKgMIAPAOQgSASgWABQgVAAgPgPg");
	this.shape_5.setTransform(15.3,52.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAeAxIgKgVIgoAAIgJAVIgXAAIAqhhIAVAAIAqBhgAgLAJIAXAAIgMgcg");
	this.shape_6.setTransform(4.9,52.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AAQAxIgVgfIgPAAIAAAfIgWAAIAAhhIAlAAQAWAAAKAIQAKAIAAARQAAAWgTAHIAZAjgAgUAAIAQAAQAKAAAEgEQAEgEAAgHQAAgIgEgDQgEgDgKAAIgQAAg");
	this.shape_7.setTransform(-5.1,52.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBGAAIAAATIgwAAIAAAVIAqAAIAAASIgqAAIAAAUIAxAAIAAATg");
	this.shape_8.setTransform(-14.9,52.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgKAxIAAhOIgcAAIAAgTIBNAAIAAATIgcAAIAABOg");
	this.shape_9.setTransform(-23.9,52.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AAXAxIgtg9IAAA9IgWAAIAAhhIAUAAIAvA+IAAg+IAWAAIAABhg");
	this.shape_10.setTransform(-33.9,52.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgKAxIAAhhIAVAAIAABhg");
	this.shape_11.setTransform(-41.9,52.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgTIANAAIADgRIgPAAIADgUIAPAAIAEgWIAUAAIgDAWIAMAAIADgWIAWAAIgEAWIAOAAIgDAUIgPAAIgDARIARAAIgEATIgQAAIgDAVgAgHAKIALAAIAEgRIgMAAg");
	this.shape_12.setTransform(-49.5,52.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AAXAxIgtg9IAAA9IgWAAIAAhhIAUAAIAvA+IAAg+IAWAAIAABhg");
	this.shape_13.setTransform(75.3,31.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgeAkQgPgOAAgWQAAgUAQgPQAOgOAUAAQAVAAAQANIgLARQgHgGgGgDQgFgBgHAAQgLAAgIAIQgJAJAAAMQAAAOAIAJQAJAIAKAAQALAAAHgFIAAgaIAWAAIAAAiQgOAQgaABQgUgBgPgOg");
	this.shape_14.setTransform(64.3,31.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgKAxIAAhhIAVAAIAABhg");
	this.shape_15.setTransform(56.8,31.7);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgmAiIANgRQAQAPANAAQAGAAADgEQADgCAAgEQAAgFgDgCQgEgDgKgDQgRgEgIgFQgIgHAAgNQAAgPALgHQAKgIANABQAKAAAKADQAKADAHAGIgLARQgNgKgNAAQgFAAgDACQgDADAAAEQAAAFAEACQAEACANAEQAOADAHAGQAIAHAAANQAAANgKAJQgKAHgQABQgWgBgTgQg");
	this.shape_16.setTransform(49.9,31.7);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBGAAIAAATIgwAAIAAAVIAqAAIAAASIgqAAIAAATIAxAAIAAAUg");
	this.shape_17.setTransform(41.2,31.7);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgrAxIAAhhIAiAAQAZAAAOANQAPAMAAAXQAAAWgPAOQgNANgbAAgAgWAdIAOAAQAPAAAHgHQAJgIgBgOQABgOgJgHQgHgIgQAAIgNAAg");
	this.shape_18.setTransform(31.3,31.7);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgeAkQgPgOAAgWQAAgUAPgPQAPgOAUAAQAYgBARATIgOAPQgKgMgQAAQgLgBgJAJQgJAHAAANQAAANAJAJQAIAIALAAQAQAAAKgNIAPAPQgRATgXAAQgWgBgOgOg");
	this.shape_19.setTransform(16.9,31.7);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgKAxIAAhhIAVAAIAABhg");
	this.shape_20.setTransform(9.4,31.7);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AAUAxIAAgmIgmAAIAAAmIgWAAIAAhhIAWAAIAAApIAmAAIAAgpIAVAAIAABhg");
	this.shape_21.setTransform(1.7,31.7);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgmAxIAAhhIAiAAQAWAAALAJQAKAIAAATQAAARgLAIQgLAJgUgBIgNAAIAAAcgAgQADIAOAAQALAAADgDQAEgFAAgJQABgIgGgDQgFgEgKAAIgMAAg");
	this.shape_22.setTransform(-8,31.7);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AAeAxIgKgVIgoAAIgJAVIgXAAIAqhhIAVAAIAqBhgAgLAJIAXAAIgMgcg");
	this.shape_23.setTransform(-18.3,31.7);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AAQAxIgVgfIgPAAIAAAfIgWAAIAAhhIAlAAQAWAAAKAIQAKAHAAASQAAAWgTAHIAZAjgAgUAAIAQAAQAKAAAEgEQAEgEAAgHQAAgIgEgDQgEgDgKAAIgQAAg");
	this.shape_24.setTransform(-28.3,31.7);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgeAkQgOgOAAgWQAAgUAOgPQAPgOAUAAQAWAAAPANIgMARQgGgGgFgDQgGgBgHAAQgLAAgJAIQgIAJAAAMQAAAOAIAJQAIAIAKAAQAMAAAHgFIAAgaIAXAAIAAAiQgPAQgaABQgUgBgPgOg");
	this.shape_25.setTransform(-39,31.7);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgTIANAAIADgRIgPAAIADgTIAPAAIAEgXIAUAAIgDAXIAMAAIADgXIAWAAIgEAXIAOAAIgDATIgPAAIgDARIARAAIgEATIgQAAIgDAVgAgHAKIALAAIAEgRIgMAAg");
	this.shape_26.setTransform(-49.5,31.6);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBGAAIAAAUIgwAAIAAAUIAqAAIAAASIgqAAIAAATIAxAAIAAAUg");
	this.shape_27.setTransform(67.8,10.7);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AgKAxIAAhhIAVAAIAABhg");
	this.shape_28.setTransform(60.7,10.7);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AAUAxIAAgnIgmAAIAAAnIgWAAIAAhhIAWAAIAAApIAmAAIAAgpIAVAAIAABhg");
	this.shape_29.setTransform(53.1,10.7);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AgmAxIAAhhIAjAAQAVAAAKAJQALAJAAASQAAARgLAIQgKAJgVgBIgNAAIAAAcgAgQADIAPAAQAJAAAFgDQADgFAAgJQAAgIgFgDQgFgEgJAAIgNAAg");
	this.shape_30.setTransform(43.4,10.7);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AAeAxIgKgVIgoAAIgJAVIgXAAIAqhhIAVAAIAqBhgAgLAJIAXAAIgMgcg");
	this.shape_31.setTransform(33,10.7);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AAQAxIgVgfIgPAAIAAAfIgWAAIAAhhIAlAAQAWAAAKAIQAKAHAAARQAAAXgTAHIAZAjgAgUAAIAQAAQAKAAAEgEQAEgDAAgJQAAgHgEgDQgEgDgKAAIgQAAg");
	this.shape_32.setTransform(23.1,10.7);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AgeAkQgPgOAAgWQABgUAPgPQAOgOAVAAQAUAAAQANIgLARQgHgGgGgDQgFgBgGAAQgMAAgIAIQgJAIAAANQAAAOAIAJQAJAIAKAAQALAAAIgFIAAgaIAVAAIAAAiQgOAQgZABQgWgBgOgOg");
	this.shape_33.setTransform(12.3,10.6);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AgjAkQgPgPAAgVQAAgVAPgOQAOgOAVAAQAWAAAPAOQAOAOAAAVQAAAVgOAPQgPAPgWAAQgVAAgOgPgAgTgVQgJAJAAAMQAAANAJAKQAIAIALABQAMgBAJgIQAIgKAAgNQAAgMgIgJQgJgKgMABQgLgBgIAKg");
	this.shape_34.setTransform(1.3,10.6);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AAXAxIgtg9IAAA9IgWAAIAAhhIAUAAIAvA+IAAg+IAWAAIAABhg");
	this.shape_35.setTransform(-10.2,10.7);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBFAAIAAAUIgvAAIAAAUIArAAIAAASIgrAAIAAATIAxAAIAAAUg");
	this.shape_36.setTransform(-20.2,10.7);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AgeAkQgPgOAAgWQAAgUAPgPQAPgOAVAAQAXgBARATIgNAPQgLgMgPAAQgMgBgJAJQgIAHgBANQAAAOAJAIQAJAIAKAAQAQAAALgNIANAPQgQATgXAAQgVgBgPgOg");
	this.shape_37.setTransform(-30.1,10.6);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("AgmAhIANgQQAQAOANABQAGAAADgEQADgCAAgEQAAgFgDgCQgEgDgKgDQgRgEgIgFQgIgHAAgNQAAgOALgIQAKgIANABQAKgBAKAEQAKADAHAGIgLARQgNgLgNABQgFgBgDADQgDADAAAEQAAAFAEACQAEADANADQAOADAHAGQAIAHAAANQAAANgKAIQgKAJgQAAQgWgBgTgRg");
	this.shape_38.setTransform(-39.9,10.6);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgTIANAAIADgRIgPAAIADgTIAPAAIAEgXIAUAAIgDAXIAMAAIADgXIAWAAIgEAXIAOAAIgDATIgPAAIgDARIARAAIgEATIgQAAIgDAVgAgHAKIALAAIAEgRIgMAAg");
	this.shape_39.setTransform(-49.5,10.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.txt_hashtag, new cjs.Rectangle(-56.7,0,139.9,63.2), null);


(lib.masqueTexte_1_ligne = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("EgicAEJIAAoRMBE5AAAIAAIRg");
	this.shape.setTransform(0,24.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.masqueTexte_1_ligne, new cjs.Rectangle(-220.5,-1.9,441,53), null);


(lib.masque_generique = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E41A33").s().p("AsQCqIAAlTIYhAAIAAFTg");
	this.shape.setTransform(78.5,17);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.masque_generique, new cjs.Rectangle(0,0,157,34), null);


(lib.ligne = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(49));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#333333").ss(1,1,1).p("EAAAg/bMAAAB+3");
	this.shape.setTransform(0,406);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#333333").ss(1,1,1).p("EAA/A/cQhDv0ggwoQgdwxAExFQAEwXAhwHQAfufA4to");
	this.shape_1.setTransform(-6,406);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#333333").ss(1,1,1).p("EABwA/cQh4vfg5w8Qg0wqAHxQQAHweA7wDQA5uxBjtQ");
	this.shape_2.setTransform(-10.7,406);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#333333").ss(1,1,1).p("EACVA/cQigvPhLxLQhHwmAKxYQAJwjBPv/QBMvACEs9");
	this.shape_3.setTransform(-14.2,406);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#333333").ss(1,1,1).p("EACwA/cQi+vDhZxWQhTwjAMxeQAKwnBev8QBavKCcsw");
	this.shape_4.setTransform(-16.9,406);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#333333").ss(1,1,1).p("EADEA/cQjTu7hjxdQhdwhANxhQAMwrBov6QBkvSCusm");
	this.shape_5.setTransform(-18.8,406);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#333333").ss(1,1,1).p("EADSA/cQjiu1hqxjQhjwfAOxlQAMwsBvv5QBsvXC6sf");
	this.shape_6.setTransform(-20.1,406);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#333333").ss(1,1,1).p("EADbA/cQjsuxhuxnQhnweAOxnQANwtB0v4QBwvaDCsb");
	this.shape_7.setTransform(-20.9,406);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#333333").ss(1,1,1).p("EADgA/cQjyuuhxxpQhpwdAPxpQANwuB3v3QByvdDHsY");
	this.shape_8.setTransform(-21.4,406);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#333333").ss(1,1,1).p("EADjA/cQj1uthyxqQhrwdAPxpQANwvB5v3QB0vdDJsX");
	this.shape_9.setTransform(-21.7,406);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#333333").ss(1,1,1).p("EADkA/cQj2uthzxqQhswdAPxpQANwvB6v3QB1veDKsW");
	this.shape_10.setTransform(-21.8,406);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#333333").ss(1,1,1).p("EADlA/cQj3ush0xrQhrwdAPxpQANwvB5v3QB2veDLsW");
	this.shape_11.setTransform(-21.9,406);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#333333").ss(1,1,1).p("EADlA/cQj3ushzxrQhswdAPxpQANwvB6v3QB1veDLsW");
	this.shape_12.setTransform(-21.9,406);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#333333").ss(1,1,1).p("EADlg/bQjLMWh1PeQh6P3gNQvQgPRpBsQdQBzRrD3Os");
	this.shape_13.setTransform(-21.9,406);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#333333").ss(1,1,1).p("EADlA/cQj3ushzxrQhswdAPxpQANwvB5v3QB2veDLsW");
	this.shape_14.setTransform(-21.9,406);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#333333").ss(1,1,1).p("EADlA/cQj3ushzxrQhswdAPxpQAOwvB5v3QB1veDLsW");
	this.shape_15.setTransform(-21.9,406);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#333333").ss(1,1,1).p("EADkA/cQj2uthzxqQhrwdAPxpQANwvB5v3QB1veDKsW");
	this.shape_16.setTransform(-21.8,406);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#333333").ss(1,1,1).p("EADjA/cQj0uthzxqQhqwdAOxpQAOwvB4v2QB0veDJsX");
	this.shape_17.setTransform(-21.7,406);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#333333").ss(1,1,1).p("EADhA/cQjyuuhyxpQhpwdAOxpQANwuB4v3QBzvdDHsY");
	this.shape_18.setTransform(-21.5,406);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#333333").ss(1,1,1).p("EADeA/cQjvuvhwxoQhoweAOxoQANwuB2v3QBxvcDFsZ");
	this.shape_19.setTransform(-21.2,406);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#333333").ss(1,1,1).p("EADaA/cQjruxhuxnQhnweAPxmQAMwuB0v4QBwvaDBsb");
	this.shape_20.setTransform(-20.9,406);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#333333").ss(1,1,1).p("EADWA/cQjnuzhrxlQhlweAOxmQANwsBxv5QBtvZC+sd");
	this.shape_21.setTransform(-20.4,406);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#333333").ss(1,1,1).p("EADQA/cQjgu2hpxiQhiwfAOxlQAMwsBuv4QBrvXC4sg");
	this.shape_22.setTransform(-19.9,406);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#333333").ss(1,1,1).p("EADJA/cQjZu4hlxgQhewgANxjQAMwrBqv6QBnvTCysk");
	this.shape_23.setTransform(-19.2,406);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#333333").ss(1,1,1).p("EADAA/cQjPu8hhxcQhawhAMxhQAMwqBlv7QBjvQCqso");
	this.shape_24.setTransform(-18.4,406);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#333333").ss(1,1,1).p("EAC2A/cQjEvAhcxZQhWwiAMxfQALwoBhv8QBdvMChst");
	this.shape_25.setTransform(-17.4,406);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#333333").ss(1,1,1).p("EACqA/cQi3vFhWxUQhRwjAMxdQAKwnBav8QBXvICXsz");
	this.shape_26.setTransform(-16.3,406);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#333333").ss(1,1,1).p("EACdA/cQipvLhPxPQhKwkAKxaQAKwlBTv+QBQvDCLs5");
	this.shape_27.setTransform(-15,406);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#333333").ss(1,1,1).p("EACOA/cQiYvShIxIQhCwnAJxWQAIwiBLwAQBJu9B9tB");
	this.shape_28.setTransform(-13.6,406);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#333333").ss(1,1,1).p("EAB8A/cQiFvZg/xCQg6woAIxSQAHwhBCwBQA/u2ButK");
	this.shape_29.setTransform(-11.9,406);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#333333").ss(1,1,1).p("EABpA/cQhxvig1w5QgxwrAHxPQAGwdA3wDQA2uvBdtT");
	this.shape_30.setTransform(-10,406);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#333333").ss(1,1,1).p("EABYA/cQhevpgswyQgqwuAGxKQAFwbAvwFQAsuoBOtc");
	this.shape_31.setTransform(-8.4,406);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#333333").ss(1,1,1).p("EABIA/cQhNvwgkwsQgiwvAFxHQAEwYAmwHQAkujBAtj");
	this.shape_32.setTransform(-6.9,406);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#333333").ss(1,1,1).p("EAA7A/cQg/v2gdwmQgcwxAExEQADwXAgwIQAdudA0tq");
	this.shape_33.setTransform(-5.6,406);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#333333").ss(1,1,1).p("EAAvA/cQgyv7gYwhQgWwzADxBQADwVAZwJQAXuZAqtw");
	this.shape_34.setTransform(-4.5,406);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#333333").ss(1,1,1).p("EAAlA/cQgnv/gTweQgRwzACxAQACwTAUwKQASuVAht1");
	this.shape_35.setTransform(-3.5,406);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#333333").ss(1,1,1).p("EAAcA/cQgdwDgPwaQgNw1ACw9QABwSAQwLQANuSAZt5");
	this.shape_36.setTransform(-2.7,406);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#333333").ss(1,1,1).p("EAAVA/cQgWwGgLwXQgKw2ACw7QABwSALwMQAKuPATt8");
	this.shape_37.setTransform(-2,406);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#333333").ss(1,1,1).p("EAAQA/cQgQwIgIwVQgHw3ABw6QABwRAIwMQAHuNAOt/");
	this.shape_38.setTransform(-1.5,406);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#333333").ss(1,1,1).p("EAALA/cQgLwLgFwSQgFw3AAw6QABwQAGwMQAFuMAJuB");
	this.shape_39.setTransform(-1.1,406);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#333333").ss(1,1,1).p("EAAIA/cQgIwMgDwSQgEw3ABw5QAAwPAEwNQADuKAHuD");
	this.shape_40.setTransform(-0.7,406);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#333333").ss(1,1,1).p("EAAFA/cQgFwNgBwRQgDw3ABw5QAAwOACwOQACuJAEuE");
	this.shape_41.setTransform(-0.5,406);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#333333").ss(1,1,1).p("EAADA/cQgDwOAAwQQgCw3ABw4QAAwOABwPQAAuIADuF");
	this.shape_42.setTransform(-0.3,406);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#333333").ss(1,1,1).p("EAABA/cQgBwOAAwQQgBw4ABw3QAAwOAAwOQAAuIABuG");
	this.shape_43.setTransform(-0.1,406);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#333333").ss(1,1,1).p("EAAAA/cQAAwPAAwPQAAw4AAw3QAAwOAAwOQAAuIAAuG");
	this.shape_44.setTransform(0,406);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#333333").ss(1,1,1).p("EAAAA/cQAAwPAAwPQAAw4AAw3QAAwOAAwOQAAuHAAuH");
	this.shape_45.setTransform(0,406);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1,-1,2,814);


(lib.lettres_FAT = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E41A33").s().p("AkUDuIBchyQByBkBegBQAsAAAYgSQAZgSAAggQAAgggagSQgagShNgUQh6gcg4guQg4gvgBhjQAAhjBIg3QBHg2BqAAQBGAABHAYQBFAYA0AtIhPBxQhahEhhAAQgmAAgXASQgXASAAAfQABAeAbATQAbARBjAZQBjAZA3AvQA4AxAABeQgBBehGA7QhGA6h0ABQikgBiFh7g");
	this.shape.setTransform(187.4,187.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E41A33").s().p("AixBDIAAiFIFjAAIAACFg");
	this.shape_1.setTransform(119.6,192.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E41A33").s().p("AhBFeIkZq7ICpAAICxG9ICym9ICpAAIkZK7g");
	this.shape_2.setTransform(-66.5,187.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#E41A33").s().p("ACLFdIAAkSIkVAAIAAESIicAAIAAq5ICcAAIAAEjIEVAAIAAkjICcAAIAAK5g");
	this.shape_3.setTransform(107.1,63.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#E41A33").s().p("AjeEEQhohngBibQAAibBrhoQBrhnCYAAQCtAABzCCIhhBvQhJhdhuAAQhYAAg+A6Qg/A5AABhQAABiA8A6QA6A8BUAAQByAABKhbIBkBmQh3CGiiAAQihAAhohlg");
	this.shape_4.setTransform(20.9,63.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#E41A33").s().p("ABxFdIiajfIhsAAIAADfIibAAIAAq5IEIAAQCjAABFA3QBGA2AAB6QAACoiGAwICyD6gAiVgIIBzAAQBPAAAcgZQAdgaAAg5QAAg5gdgUQgegWhLAAIh1AAg");
	this.shape_5.setTransform(-61.8,63.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.lettres_FAT, new cjs.Rectangle(-102.9,0,337,249.8), null);


(lib.img_parchemin = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.archivesDep_parchemin();
	this.instance.parent = this;
	this.instance.setTransform(-250,-194.9,0.879,0.879,4.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.img_parchemin, new cjs.Rectangle(-276,-194.9,552.1,389.8), null);


(lib.hastaggraphicbranche = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AjQAuIAShbIDlAAICqAAIgSBbg");
	this.shape.setTransform(0,4.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.hastaggraphicbranche, new cjs.Rectangle(-20.8,0,41.7,9.2), null);


(lib.Design_archives = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E41A33").s().p("AzyNPIh/iAIAA4dMApkAAAIB/CCIAAYbgAUVMbIhJhMMgnzAAAIBKBMMAnyAAAgAT0KtIBIBKIAA2vIhIhIg");
	this.shape.setTransform(40,164.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E41A33").s().p("AzyNPIh/iAIAA4dMApkAAAIB/CCIAAYbgAzdMbMAnyAAAIhJhMMgnzAAAgAT0KtIBIBKIAA2vIhIhIg");
	this.shape_1.setTransform(40,164.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E41A33").s().p("AzwNQIiDiCIAA4dMAplAAAICBCEIAAYbgAzcMcMAnyAAAIhLhOMgnzAAAgATzKsIBKBMIAA2uIhKhLg");
	this.shape_2.setTransform(39.9,164.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#E41A33").s().p("AzsNVIiLiMIAA4dMApkAAAICLCOIAAYbgAzXMhMAnyAAAIhVhYMgnzAAAgATuKnIBUBWIAA2vIhUhUg");
	this.shape_3.setTransform(39.4,164.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#E41A33").s().p("AzfNiIilimIAA4dMApkAAAIClCnIAAYcgAzKMuMAnyAAAIhvhyMgnzAAAgATiKaIBtBwIAA2vIhthug");
	this.shape_4.setTransform(38.1,162.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#E41A33").s().p("AzEN9IjbjcIAA4dMApkAAAIDbDdIAAYcgAyvNJMAnyAAAIilioMgnzAAAgATHJ/ICjCmIAA2vIijikg");
	this.shape_5.setTransform(35.4,160.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E41A33").s().p("AyTOuIk9k+IAA4dMApkAAAIE9FAIAAYbgAx+N6MAnyAAAIkHkKMgnzAAAgASVJOIEGEIIAA2vIkGkGg");
	this.shape_6.setTransform(30.5,155.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#E41A33").s().p("AxCP/InfngIAA4dMApkAAAIHfHhIAAYcgAwtPLMAnyAAAImpmsMgnzAAAgARFH9IGnGqIAA2vImnmog");
	this.shape_7.setTransform(22.4,147.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#E41A33").s().p("AvGR7IrXrYIAA4dMApkAAAILXLaIAAYbgAuxRHMAnyAAAIqhqkMgnzAAAgAPIGBIKgKiIAA2vIqgqgg");
	this.shape_8.setTransform(10,134.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#E41A33").s().p("AtJT3IvRvQIAA4dMAplAAAIPPPSIAAYbgAs1TDMAnyAAAIuZucMgnzAAAgANMEFIOYOaIAA2uIuYuZg");
	this.shape_9.setTransform(-2.4,122.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#E41A33").s().p("Ar5VIIxxxyIAA4dMApkAAAIRxR0IAAYbgArkUUMAnyAAAIw7w+MgnzAAAgAL7C0IQ6Q8IAA2vIw6w6g");
	this.shape_10.setTransform(-10.5,114.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#E41A33").s().p("ArHV5IzVzUIAA4dMApkAAAITUTWIAAYbgAqzVFMAnyAAAIydygMgnzAAAgALKCDIScSeIAA2uIycydg");
	this.shape_11.setTransform(-15.4,109.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#E41A33").s().p("AqsWUI0L0KIAA4dMApkAAAIUKUMIAAYbgAqYVgMAnyAAAIzTzWMgnzAAAgAKvBoITSTUIAA2uIzSzTg");
	this.shape_12.setTransform(-18.1,106.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#E41A33").s().p("AqgWhI0j0kIAA4dMApkAAAIUjUmIAAYbgAqLVtMAnyAAAIztzwMgnzAAAgAKiBbITsTuIAA2vIzszsg");
	this.shape_13.setTransform(-19.4,105.3);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#E41A33").s().p("AqbWmI0t0uIAA4dMApkAAAIUtUvIAAYcgAqGVyMAnyAAAIz3z6MgnzAAAgAKeBWIT1T4IAA2vIz1z2g");
	this.shape_14.setTransform(-19.9,104.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#E41A33").s().p("AqaWnI0v0wIAA4dMApkAAAIUvUyIAAYbgAqFVzMAnyAAAIz5z8MgnzAAAgAKcBVIT4T6IAA2vIz4z4g");
	this.shape_15.setTransform(-20,104.7);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#E41A33").s().p("AqaWnI0v0wIAA4dMApkAAAIUvUyIAAYbgAdtVzIz5z8MgnzAAAIT6T8MAnyAAAgAKcBVIT4T6IAA2vIz4z4g");
	this.shape_16.setTransform(-20,104.7);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#E41A33").s().p("AqaWnI0v0wIAA4dMApkAAAIUvUxIAAYcgAqFVzMAnyAAAIz5z8MgnzAAAgAKdBVIT3T6IAA2vIz3z4g");
	this.shape_17.setTransform(-20,104.7);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#E41A33").s().p("AqaWmI0v0uIAA4dMApkAAAIUvUwIAAYbgAqFVyMAnyAAAIz5z6MgnyAAAgAKdBWIT3T4IAA2uIz3z4g");
	this.shape_18.setTransform(-20,104.8);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#E41A33").s().p("AqcWkI0q0qIAA4dMApjAAAIUqUsIAAYbgAqIVwMAnyAAAIz0z2MgnyAAAgAKfBYITyT0IAA2uIzyzzg");
	this.shape_19.setTransform(-19.7,105);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#E41A33").s().p("AqfWiI0l0mIAA4dMApkAAAIUlUoIAAYbgAqKVuMAnyAAAIzvzyMgnzAAAgAKhBaITuTwIAA2vIzuzug");
	this.shape_20.setTransform(-19.5,105.2);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#E41A33").s().p("AqiWeI0f0fIAA4dMApkAAAIUfUhIAAYbgAqNVqMAnyAAAIzpzrMgnyAAAgAKlBeITnToIAA2uIznzog");
	this.shape_21.setTransform(-19.2,105.6);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#E41A33").s().p("AqnWZI0V0UIAA4dMApkAAAIUVUWIAAYbgAqSVlMAnyAAAIzfzgMgnyAAAgAKqBjITdTeIAA2uIzdzeg");
	this.shape_22.setTransform(-18.7,106.1);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#E41A33").s().p("AquWSI0G0GIAA4dMApjAAAIUGUIIAAYbgAqaVeMAnyAAAIzQzSMgnyAAAgAKxBqITOTQIAA2uIzOzPg");
	this.shape_23.setTransform(-17.9,106.8);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#E41A33").s().p("Aq4WIIzzzzIAA4dMApkAAAITzT1IAAYbgAqjVUMAnyAAAIy9y/MgnyAAAgAK7B0IS7S8IAA2uIy7y8g");
	this.shape_24.setTransform(-17,107.8);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#E41A33").s().p("ArFV8IzZzaIAA4dMApkAAAITZTcIAAYbgAqwVIMAnyAAAIyjymMgnzAAAgALHCAISiSkIAA2vIyiyig");
	this.shape_25.setTransform(-15.7,109);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#E41A33").s().p("ArVVsIy5y6IAA4dMApkAAAIS5S7IAAYcgArAU4MAnyAAAIyDyGMgnzAAAgALYCQISBSEIAA2vIyByCg");
	this.shape_26.setTransform(-14.1,110.6);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#E41A33").s().p("ArpVXIyQyQIAA4dMApkAAAISPSSIAAYbgArVUjMAnyAAAIxaxcMgnyAAAgALsClIRYRaIAA2uIxYxZg");
	this.shape_27.setTransform(-12,112.7);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#E41A33").s().p("AsDU+IxdxeIAA4dMApkAAAIRdRgIAAYbgAruUKMAnyAAAIwnwqMgnzAAAgAMFC+IQmQoIAA2vIwmwmg");
	this.shape_28.setTransform(-9.5,115.2);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#E41A33").s().p("AsiUfIwfwgIAA4dMApkAAAIQfQiIAAYbgAsNTrMAnyAAAIvpvsMgnzAAAgAMkDdIPoPqIAA2vIvovog");
	this.shape_29.setTransform(-6.4,118.3);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#E41A33").s().p("AtHT6IvVvWIAA4dMApkAAAIPVPXIAAYcgAsyTGMAnyAAAIufuiMgnzAAAgANJECIOeOgIAA2vIueueg");
	this.shape_30.setTransform(-2.7,122);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#E41A33").s().p("AtzTNIt8t8IAA4dMApjAAAIN8N+IAAYbgAtfSZMAnyAAAItFtIMgnzAAAgAN2EvINENGIAA2uItEtFg");
	this.shape_31.setTransform(1.8,126.5);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#E41A33").s().p("AuoSYIsTsSIAA4dMApkAAAIMTMUIAAYbgAuTRkMAnyAAAIrdreMgnyAAAgAOrFkILbLcIAA2uIrbrcg");
	this.shape_32.setTransform(7,131.8);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#E41A33").s().p("AvjRdIqdqcIAA4dMApkAAAIKdKeIAAYbgAvOQpMAnyAAAIpnpoMgnyAAAgAPmGeIJlJnIAA2uIplpmg");
	this.shape_33.setTransform(12.9,137.7);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#E41A33").s().p("AwYQpIozo0IAA4dMApkAAAIIzI1IAAYcgAwDP1MAnyAAAIn9oAMgnyAAAgAQaHTIH8H+IAA2vIn8n8g");
	this.shape_34.setTransform(18.2,142.9);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#E41A33").s().p("AxEP8InbnaIAA4dMAplAAAIHZHcIAAYbgAwwPIMAnyAAAImkmmMgnyAAAgARHIAIGiGkIAA2uImimjg");
	this.shape_35.setTransform(22.7,147.4);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#E41A33").s().p("AxqPXImPmQIAA4dMApkAAAIGPGSIAAYbgAxVOjMAnyAAAIlZlcMgnzAAAgARsIlIFYFaIAA2vIlYlYg");
	this.shape_36.setTransform(26.4,151.1);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#E41A33").s().p("AyJO4IlRlSIAA4dMApkAAAIFRFUIAAYbgAx0OEMAnyAAAIkbkeMgnzAAAgASLJEIEaEcIAA2vIkakag");
	this.shape_37.setTransform(29.5,154.2);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#E41A33").s().p("AyiOfIkfkgIAA4dMApkAAAIEfEhIAAYcgAyNNrMAnyAAAIjpjsMgnyAAAgASkJdIDoDqIAA2vIjojog");
	this.shape_38.setTransform(32,156.7);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#E41A33").s().p("Ay2OKIj3j2IAA4dMAplAAAID1D4IAAYbgAyiNWMAnyAAAIi/jCMgnzAAAgAS5JyIC+DAIAA2uIi+i/g");
	this.shape_39.setTransform(34.1,158.8);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#E41A33").s().p("AzHN6IjVjWIAA4dMApkAAAIDVDYIAAYbgAyyNGMAnyAAAIifiiMgnzAAAgATJKCICeCgIAA2vIieieg");
	this.shape_40.setTransform(35.7,160.4);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#E41A33").s().p("AzTNtIi9i9IAA4dMApkAAAIC9C/IAAYbgAy+M5MAnyAAAIiHiJMgnyAAAgATWKOICFCHIAA2uIiFiFg");
	this.shape_41.setTransform(36.9,161.7);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#E41A33").s().p("AzdNkIipiqIAA4dMApkAAAICpCrIAAYcgAzIMwMAnyAAAIhzh2MgnyAAAgATfKYIByB0IAA2vIhyhyg");
	this.shape_42.setTransform(37.9,162.6);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#E41A33").s().p("AzkNcIibiaIAA4dMApkAAAICbCcIAAYbgAzPMoMAnyAAAIhlhmMgnyAAAgATnKfIBjBlIAA2uIhjhjg");
	this.shape_43.setTransform(38.6,163.4);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#E41A33").s().p("AzpNXIiRiRIAA4dMApkAAAICRCTIAAYbgAzUMjMAnyAAAIhbhdMgnyAAAgATsKkIBZBbIAA2uIhZhZg");
	this.shape_44.setTransform(39.1,163.9);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#E41A33").s().p("AztNUIiJiKIAA4dMApkAAAICJCMIAAYbgAzYMgMAnyAAAIhThWMgnzAAAgATvKoIBSBUIAA2vIhShSg");
	this.shape_45.setTransform(39.5,164.2);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#E41A33").s().p("AzvNSIiFiGIAA4dMApkAAAICFCHIAAYcgAzaMeMAnyAAAIhPhSMgnyAAAgATxKqIBOBQIAA2vIhOhOg");
	this.shape_46.setTransform(39.7,164.4);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#E41A33").s().p("AzxNPIiBiAIAA4dMApkAAAICBCCIAAYbgAzcMbMAnyAAAIhLhMMgnyAAAgAT0KsIBJBLIAA2uIhJhKg");
	this.shape_47.setTransform(39.9,164.7);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#E41A33").s().p("AzxNPIiBiAIAA4dMApkAAAICACCIAAYbgAzdMbMAnyAAAIhJhMMgnzAAAgAT0KtIBIBKIAA2uIhIhJg");
	this.shape_48.setTransform(40,164.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-99.4,80,278.9,169.4);


(lib.Btn_cliquable_area = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E41A33").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape.setTransform(0,30);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Btn_cliquable_area, new cjs.Rectangle(-113,0,226,60), null);


(lib.masqueTexte = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// masqueTexte_1_ligne
	this.instance = new lib.masqueTexte_1_ligne();
	this.instance.parent = this;
	this.instance.setTransform(-218.8,191.5,0.008,1,0,0,0,0,25.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(6).to({_off:false},0).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.01,x:218.8},14).wait(1));

	// masqueTexte_1_ligne
	this.instance_1 = new lib.masqueTexte_1_ligne();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-218.8,136.1,0.008,1,0,0,0,0,25.5);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.01,x:218.8},14).to({_off:true},1).wait(2));

	// masqueTexte_1_ligne
	this.instance_2 = new lib.masqueTexte_1_ligne();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-218.8,80.8,0.008,1,0,0,0,0,25.5);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(2).to({_off:false},0).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.01,x:218.8},14).to({_off:true},1).wait(4));

	// masqueTexte_1_ligne
	this.instance_3 = new lib.masqueTexte_1_ligne();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-218.8,25.5,0.008,1,0,0,0,0,25.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.01,x:218.8},14).to({_off:true},1).wait(6));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-220.5,-1.9,3.5,53);


(lib.lignes_all = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{close:114,close_lines:157});

	// timeline functions:
	this.frame_113 = function() {
		this.stop();
	}
	this.frame_156 = function() {
		this.stop();
	}
	this.frame_174 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(113).call(this.frame_113).wait(43).call(this.frame_156).wait(18).call(this.frame_174).wait(1));

	// ligne
	this.instance = new lib.ligne();
	this.instance.parent = this;
	this.instance.setTransform(-454,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(114).to({x:-883},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:-454},0).wait(18));

	// ligne
	this.instance_1 = new lib.ligne();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-227,406.5,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(114).to({x:-883.2},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:-227},0).wait(18));

	// ligne
	this.instance_2 = new lib.ligne();
	this.instance_2.parent = this;
	this.instance_2.setTransform(0,406.5,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(114).to({x:-883.4},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:0},0).wait(18));

	// ligne
	this.instance_3 = new lib.ligne();
	this.instance_3.parent = this;
	this.instance_3.setTransform(227,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(29).to({y:322},20,cjs.Ease.quartInOut).wait(65).to({y:323},0).to({x:-883,y:406},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:227,y:322},0).to({y:406},16,cjs.Ease.quartInOut).wait(2));

	// ligne
	this.instance_4 = new lib.ligne();
	this.instance_4.parent = this;
	this.instance_4.setTransform(454.1,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(114).to({x:-883.4},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:454.1},0).wait(18));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-454.5,-0.5,909.2,813.5);


(lib.hashtag_graphic = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(64));

	// hastag_graphic_branche
	this.instance = new lib.hastaggraphicbranche();
	this.instance.parent = this;
	this.instance.setTransform(37.4,51.9,1.084,0.995,0,-78.3,102.1,0.1,4.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({x:6.5,y:50.1},17,cjs.Ease.quartOut).to({_off:true},5).wait(3).to({_off:false},0).to({_off:true},4).wait(2).to({_off:false,x:26.5},0).to({_off:true},5).wait(28));

	// hastag_graphic_branche
	this.instance_1 = new lib.hastaggraphicbranche();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-41.6,49.9,1.084,0.995,0,-78.3,102.1,0.1,4.6);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off:false},0).to({x:-7.4,y:49.8},18,cjs.Ease.quartOut).to({_off:true},4).wait(5).to({_off:false},0).to({_off:true},4).wait(31));

	// hastag_graphic_branche
	this.instance_2 = new lib.hastaggraphicbranche();
	this.instance_2.parent = this;
	this.instance_2.setTransform(2.7,21.7,1,1,0,0,0,0,4.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({x:-2.3,y:58.7},13,cjs.Ease.quartOut).to({_off:true},11).wait(3).to({_off:false,scaleX:2.33,scaleY:1.03,skewX:-12.8,x:25.5},0).to({_off:true},4).wait(33));

	// hastag_graphic_branche
	this.instance_3 = new lib.hastaggraphicbranche();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-7.3,77.7,1,1,0,0,0,0,4.6);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({x:0.7,y:41.7},14,cjs.Ease.quartOut).to({_off:true},4).wait(7).to({_off:false,regX:0.1,scaleX:1.98,scaleY:1.01,skewX:-9.8,x:19.9,y:49.7},0).to({_off:true},4).wait(4).to({_off:false,regX:0,scaleX:1,scaleY:1,skewX:0,x:0.7,y:41.7},0).to({_off:true},4).wait(21));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-18.1,17.1,64.6,57.7);


(lib.Btn_next = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{rollOver:55,rollOut:70});

	// timeline functions:
	this.frame_54 = function() {
		this.stop();
	}
	this.frame_69 = function() {
		this.stop();
	}
	this.frame_84 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(54).call(this.frame_54).wait(15).call(this.frame_69).wait(15).call(this.frame_84).wait(1));

	// ZONE
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.008)").s().p("EgjTAEsIAApXMBGnAAAIAAJXg");
	this.shape.setTransform(113,29.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(85));

	// txt
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgIAqIAAhDIgYAAIAAgQIBBAAIAAAQIgYAAIAABDg");
	this.shape_1.setTransform(161.1,29.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AASAqIgSgcIgQAcIgYAAIAdgqIgagpIAWAAIAPAZIAQgZIAWAAIgaApIAcAqg");
	this.shape_2.setTransform(145.2,29.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgeAqIAAhTIA8AAIAAARIgpAAIAAARIAkAAIAAAPIgkAAIAAASIAqAAIAAAQg");
	this.shape_3.setTransform(129.3,29.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAUAqIgngzIAAAzIgTAAIAAhTIASAAIAoA1IAAg1IATAAIAABTg");
	this.shape_4.setTransform(112.3,29.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgKAQIAEgOQgEgCAAgFQAAgEADgDQADgDAEAAQAFAAADADQADADAAAEQAAAEgDAEIgIANg");
	this.shape_5.setTransform(86.5,32.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAPAqIgYghIgIAKIAAAXIgTAAIAAhTIATAAIAAAjIAegjIAYAAIgiAlIAQAWIASAYg");
	this.shape_6.setTransform(77,29.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgeAfQgNgNAAgSQAAgSANgMQANgNARAAQASAAAOANQAMAMAAASQAAASgMANQgOAMgSAAQgRAAgNgMgAgRgSQgHAIAAAKQAAALAHAIQAIAIAJAAQAKAAAIgIQAHgIAAgLQAAgKgHgIQgIgIgKAAQgJAAgIAIg");
	this.shape_7.setTransform(59.3,29.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#333333").ss(1,1,1).p("EgjOAADMBGdgAF");
	this.shape_8.setTransform(113,-0.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(85));

	// Layer_2
	this.instance = new lib.Btn_cliquable_area();
	this.instance.parent = this;
	this.instance.setTransform(113,29.5,2,1,0,0,0,0,30);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("rgba(0,0,0,0.008)").s().p("EgjTAEsIAApXMBGnAAAIAAJXg");
	this.shape_9.setTransform(113,29.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance}]},3).to({state:[{t:this.instance}]},46).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.instance}]},5).to({state:[{t:this.instance}]},14).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},14).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({alpha:0.012},46).to({_off:true},1).wait(5).to({_off:false},0).to({alpha:1},14).wait(1).to({alpha:0.012},14).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-113.5,-1.5,453,61);


(lib.Btn_detail = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"rollOver":55,"rollOut":70});

	// timeline functions:
	this.frame_54 = function() {
		this.stop();
	}
	this.frame_69 = function() {
		this.stop();
	}
	this.frame_84 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(54).call(this.frame_54).wait(15).call(this.frame_69).wait(15).call(this.frame_84).wait(1));

	// ZONE
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.008)").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape.setTransform(0,29.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(85));

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgOAyIAAhkIAdAAIAABkg");
	this.shape_1.setTransform(-85.4,29.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgyAOIAAgcIBlAAIAAAcg");
	this.shape_2.setTransform(-85.4,29.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgaAqIAAhTIASAAIAABDIAjAAIAAAQg");
	this.shape_3.setTransform(38.4,29.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgIAqIAAhTIARAAIAABTg");
	this.shape_4.setTransform(24.7,29.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AAZAqIgHgSIgjAAIgHASIgUAAIAkhTIARAAIAkBTgAgKAIIAUAAIgKgYg");
	this.shape_5.setTransform(10.2,29.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgIAqIAAhDIgYAAIAAgQIBBAAIAAAQIgYAAIAABDg");
	this.shape_6.setTransform(-5.9,29.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgeAqIAAhTIA8AAIAAARIgpAAIAAARIAkAAIAAAPIgkAAIAAASIAqAAIAAAQg");
	this.shape_7.setTransform(-21.3,29.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AglAqIAAhTIAeAAQAVAAAMALQAMALAAATQAAATgMALQgMAMgXAAgAgTAaIANAAQAMAAAGgHQAHgGAAgNQAAgLgHgHQgGgGgOgBIgLAAg");
	this.shape_8.setTransform(-37.8,29.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#333333").ss(1,1,1).p("AxpAAMAjTAAA");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(85));

	// Layer_2
	this.instance = new lib.Btn_cliquable_area();
	this.instance.parent = this;
	this.instance.setTransform(0,29.5,1,1,0,0,0,0,30);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("rgba(0,0,0,0.008)").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape_10.setTransform(0,29.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance}]},3).to({state:[{t:this.instance}]},45).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.instance}]},6).to({state:[{t:this.instance}]},14).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},13).wait(2));
	this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({alpha:0.012},45).to({_off:true},1).wait(6).to({_off:false},0).to({alpha:1},14).wait(1).to({alpha:0.012},13).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-114,-1,228,60.5);


(lib.bloc_visuel = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":83});

	// timeline functions:
	this.frame_82 = function() {
		this.stop();
	}
	this.frame_124 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(82).call(this.frame_82).wait(42).call(this.frame_124).wait(1));

	// Layer_4
	this.instance = new lib.Design_archives();
	this.instance.parent = this;
	this.instance.setTransform(-432.1,-137.4,0.16,0.16,14.1,0,0,-42.9,134.6);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(19).to({_off:false},0).to({x:-611,y:-179.3},15,cjs.Ease.quartOut).wait(51).to({x:-814,y:-239.2},24,cjs.Ease.quartIn).to({_off:true},1).wait(15));

	// Layer_3
	this.instance_1 = new lib.Design_archives();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-416.6,-273.7,0.198,0.198,14.2,0,0,-0.3,124.5);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(23).to({_off:false},0).to({x:-595.6,y:-320.5},19,cjs.Ease.quartOut).wait(55).to({x:-798.6,y:-380.5},24,cjs.Ease.quartIn).wait(4));

	// AS
	this.instance_2 = new lib.Design_archives();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-371.5,-165.3,0.122,0.122,14.2,0,0,-0.6,124.7);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(17).to({_off:false},0).to({x:-550.4,y:-212.2},19,cjs.Ease.quartOut).wait(49).to({x:-753.4,y:-272.1},24,cjs.Ease.quartIn).to({_off:true},1).wait(15));

	// Isolation_Mode
	this.instance_3 = new lib.Design_archives();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-171.3,-249.8,0.116,0.116,14.1,0,0,-1.2,124.2);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(15).to({_off:false},0).to({x:-350.2,y:-296.7},17,cjs.Ease.quartOut).wait(59).to({x:-553.2,y:-356.6},24,cjs.Ease.quartIn).to({_off:true},1).wait(9));

	// Isolation_Mode
	this.instance_4 = new lib.Design_archives();
	this.instance_4.parent = this;
	this.instance_4.setTransform(160.5,45.1,0.36,0.36,14.2,0,0,-0.3,124.5);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(3).to({_off:false},0).to({x:-163.5,y:-37.9},15,cjs.Ease.quartOut).wait(71).to({x:-366.5,y:-97.8},20,cjs.Ease.quartIn).to({_off:true},1).wait(15));

	// masque_VISUEL (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("EgjsBDvMAAAiHdMBHZAAAMAAACHdg");
	mask.setTransform(-228.7,-19);

	// Isolation_Mode
	this.instance_5 = new lib.Design_archives();
	this.instance_5.parent = this;
	this.instance_5.setTransform(304.9,26,0.799,0.799,14.1,0,0,-0.3,124.7);

	var maskedShapeInstanceList = [this.instance_5];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_5).to({x:-19.1,y:-57},14,cjs.Ease.quartOut).wait(71).to({x:-222.1,y:-117},24,cjs.Ease.quartIn).to({_off:true},1).wait(15));

	// Layer_2
	this.instance_6 = new lib.img_parchemin();
	this.instance_6.parent = this;
	this.instance_6.setTransform(247,-189.6);
	this.instance_6._off = true;

	var maskedShapeInstanceList = [this.instance_6];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(5).to({_off:false},0).to({x:-173,y:-139.6},49,cjs.Ease.quartOut).wait(31).to({x:-689,y:17.9},37,cjs.Ease.quartIn).to({_off:true},1).wait(2));

	// Isolation_Mode
	this.instance_7 = new lib.Design_archives();
	this.instance_7.parent = this;
	this.instance_7.setTransform(57,-218.2,0.373,0.373,14.2,0,0,-0.2,124.5);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(7).to({_off:false},0).to({x:-123,y:-288.2},17,cjs.Ease.quartOut).wait(69).to({x:-326,y:-348.2},24,cjs.Ease.quartIn).to({_off:true},1).wait(7));

	// Isolation_Mode
	this.instance_8 = new lib.Design_archives();
	this.instance_8.parent = this;
	this.instance_8.setTransform(-207.5,-66.4,0.122,0.122,14.2,0,0,-0.6,124.7);
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(11).to({_off:false},0).to({x:-387.5,y:-136.4},17,cjs.Ease.quartOut).wait(57).to({x:-590.5,y:-196.3},24,cjs.Ease.quartIn).to({_off:true},1).wait(15));

	// Isolation_Mode
	this.instance_9 = new lib.Design_archives();
	this.instance_9.parent = this;
	this.instance_9.setTransform(-57.7,-224.7,0.198,0.198,14.2,0,0,-0.3,124.5);
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(8).to({_off:false},0).to({x:-237.7,y:-294.7},17,cjs.Ease.quartOut).wait(75).to({x:-440.7,y:-354.7},24,cjs.Ease.quartIn).wait(1));

	// Isolation_Mode
	this.instance_10 = new lib.Design_archives();
	this.instance_10.parent = this;
	this.instance_10.setTransform(-168.4,-184.8,0.4,0.3,14.1,0,0,-0.4,124.5);
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(12).to({_off:false},0).to({x:-348.4,y:-254.8},18,cjs.Ease.quartOut).wait(59).to({x:-551.4,y:-314.8},24,cjs.Ease.quartIn).to({_off:true},1).wait(11));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.bloc_video = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":41});

	// timeline functions:
	this.frame_40 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(40).call(this.frame_40).wait(17));

	// masque_video (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_42 = new cjs.Graphics().p("Ega8A8AMAAAh3/MA7rAAAMAAAB3/g");
	var mask_graphics_43 = new cjs.Graphics().p("EgYrA8AMAAAh3/MA3HAAAMAAAB3/g");
	var mask_graphics_44 = new cjs.Graphics().p("EgWaA8AMAAAh3/MAyiAAAMAAAB3/g");
	var mask_graphics_45 = new cjs.Graphics().p("EgUJA8AMAAAh3/MAt+AAAMAAAB3/g");
	var mask_graphics_46 = new cjs.Graphics().p("EgR4A8AMAAAh3/MApaAAAMAAAB3/g");
	var mask_graphics_47 = new cjs.Graphics().p("EgPnA8AMAAAh3/MAk1AAAMAAAB3/g");
	var mask_graphics_48 = new cjs.Graphics().p("EgNWA8AMAAAh3/MAgRAAAMAAAB3/g");
	var mask_graphics_49 = new cjs.Graphics().p("EgLFA8AMAAAh3/IbtAAMAAAB3/g");
	var mask_graphics_50 = new cjs.Graphics().p("EgI0A8AMAAAh3/IXIAAMAAAB3/g");
	var mask_graphics_51 = new cjs.Graphics().p("EgGjA8AMAAAh3/ISkAAMAAAB3/g");
	var mask_graphics_52 = new cjs.Graphics().p("EgESA8AMAAAh3/IOAAAMAAAB3/g");
	var mask_graphics_53 = new cjs.Graphics().p("EgCBA8AMAAAh3/IJbAAMAAAB3/g");
	var mask_graphics_54 = new cjs.Graphics().p("EAAPA8AMAAAh3/IE4AAMAAAB3/g");
	var mask_graphics_55 = new cjs.Graphics().p("EACgA8AMAAAh3/IAUAAMAAAB3/g");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(42).to({graphics:mask_graphics_42,x:209.5,y:0}).wait(1).to({graphics:mask_graphics_43,x:194.8,y:0}).wait(1).to({graphics:mask_graphics_44,x:180,y:0}).wait(1).to({graphics:mask_graphics_45,x:165.3,y:0}).wait(1).to({graphics:mask_graphics_46,x:150.6,y:0}).wait(1).to({graphics:mask_graphics_47,x:135.8,y:0}).wait(1).to({graphics:mask_graphics_48,x:121.1,y:0}).wait(1).to({graphics:mask_graphics_49,x:106.4,y:0}).wait(1).to({graphics:mask_graphics_50,x:91.6,y:0}).wait(1).to({graphics:mask_graphics_51,x:76.9,y:0}).wait(1).to({graphics:mask_graphics_52,x:62.2,y:0}).wait(1).to({graphics:mask_graphics_53,x:47.4,y:0}).wait(1).to({graphics:mask_graphics_54,x:32.7,y:0}).wait(1).to({graphics:mask_graphics_55,x:18,y:0}).wait(1).to({graphics:null,x:0,y:0}).wait(1));

	// VIDEO
	this.videoContainer = new lib.video();
	this.videoContainer.name = "videoContainer";
	this.videoContainer.parent = this;
	this.videoContainer.setTransform(36.7,765,0.659,0.659,0,0,0,0.3,480.2);

	var maskedShapeInstanceList = [this.videoContainer];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.videoContainer).to({y:-33},40,cjs.Ease.quartInOut).to({_off:true},16).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(36.5,448.7,382.1,632.3);


(lib.bloc_NEXT_btn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":20});

	// timeline functions:
	this.frame_19 = function() {
		this.stop();
	}
	this.frame_36 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(19).call(this.frame_19).wait(17).call(this.frame_36).wait(1));

	// CTA_DETAIL
	this.btnNext = new lib.Btn_next();
	this.btnNext.name = "btnNext";
	this.btnNext.parent = this;
	this.btnNext.setTransform(-340.5,411.5,1,1,0,0,0,0,26.5);

	this.timeline.addTween(cjs.Tween.get(this.btnNext).to({y:350.5},19,cjs.Ease.quartInOut).wait(2).to({y:411.5},14,cjs.Ease.quartInOut).to({_off:true},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-453.5,384,452,60.5);


(lib.bloc_hashtag = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_61 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(61).call(this.frame_61).wait(2));

	// Layer_1
	this.instance = new lib.hashtag_graphic("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(-456,243.2,2.317,2.317,0,0,0,-0.1,50.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true},61).wait(2));

	// Layer_5 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_28 = new cjs.Graphics().p("AtWBpIAAjRIatAAIAADRg");
	var mask_graphics_32 = new cjs.Graphics().p("AtWDSIAAmjIatAAIAAGjg");
	var mask_graphics_40 = new cjs.Graphics().p("AtWGYIAAsvIatAAIAAMvg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(28).to({graphics:mask_graphics_28,x:-400.4,y:265.5}).wait(4).to({graphics:mask_graphics_32,x:-400.4,y:255}).wait(8).to({graphics:mask_graphics_40,x:-400.4,y:255.9}).wait(23));

	// Layer_3
	this.instance_1 = new lib.txt_hashtag();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-421.5,244.1,1,1,0,0,0,0,31.6);
	this.instance_1._off = true;

	var maskedShapeInstanceList = [this.instance_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(28).to({_off:false},0).to({_off:true},34).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-497.8,166.1,149.7,129.4);


(lib.bloc_CTA = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":25});

	// timeline functions:
	this.frame_24 = function() {
		this.stop();
	}
	this.frame_41 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(24).call(this.frame_24).wait(17).call(this.frame_41).wait(1));

	// CTA_DETAIL
	this.btnCta = new lib.Btn_detail();
	this.btnCta.name = "btnCta";
	this.btnCta.parent = this;
	this.btnCta.setTransform(-340.5,411.5,1,1,0,0,0,0,26.5);

	this.timeline.addTween(cjs.Tween.get(this.btnCta).to({y:350.5},24,cjs.Ease.quartInOut).wait(2).to({y:411.5},14,cjs.Ease.quartInOut).to({_off:true},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-454,384.5,227,60);


(lib.blocInfostextes = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":80});

	// timeline functions:
	this.frame_79 = function() {
		this.stop();
	}
	this.frame_120 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(79).call(this.frame_79).wait(41).call(this.frame_120).wait(1));

	// Bloc_Hashtag
	this.instance = new lib.bloc_hashtag();
	this.instance.parent = this;
	this.instance.setTransform(-391.5,261.2,0.9,0.9,0,0,0,-412.9,244.7);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(62).to({_off:false},0).wait(59));

	// masque_Titre
	this.instance_1 = new lib.masque_generique();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-527.9,-13.9,0.007,0.471,0,0,0,0,17.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(34).to({_off:false},0).to({regX:0.5,scaleX:1.92,x:-527.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.01,x:-228.9},10,cjs.Ease.quartInOut).to({_off:true},1).wait(26).to({_off:false},0).to({regX:0.5,scaleX:1.92,x:-527.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.01,x:-528.7},10,cjs.Ease.quartInOut).to({_off:true},1).wait(21));

	// _NOM_du_CLIENT
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E41A33").s().p("AAEA4IAAgaIgsAAIAAgUIAohBIAaAAIglA+IAPAAIAAgSIAYAAIAAASIANAAIAAAXIgNAAIAAAag");
	this.shape.setTransform(-294.2,-14.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E41A33").s().p("AgfA4IAqhaIgbAAIAAAPIgYAAIAAgkIBQAAIAAATIgsBcg");
	this.shape_1.setTransform(-315.3,-14.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E41A33").s().p("AgbAKIAAgUIA3AAIAAAUg");
	this.shape_2.setTransform(-336.1,-13.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#E41A33").s().p("AgrAmIAOgSQATAQAOAAQAHAAAEgDQAEgDAAgGQAAgEgFgDQgEgDgLgDQgUgFgJgGQgJgIAAgQQAAgPAMgKQALgIAQAAQAMAAALAEQALAEAIAHIgMASQgPgLgPAAQgGAAgDADQgEADAAAFQAAAFAEADQAFACAPAFQAQAEAJAGQAIAIAAAPQAAAPgLAJQgLAKgTAAQgZAAgVgUg");
	this.shape_3.setTransform(-357.2,-14.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#E41A33").s().p("AgoA4IAAhvIBPAAIAAAXIg2AAIAAAWIAxAAIAAAUIgxAAIAAAYIA4AAIAAAWg");
	this.shape_4.setTransform(-379.1,-14.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#E41A33").s().p("AgKA4IgthvIAbAAIAcBGIAchGIAbAAIgtBvg");
	this.shape_5.setTransform(-402.1,-14.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E41A33").s().p("AgMA4IAAhvIAYAAIAABvg");
	this.shape_6.setTransform(-422.2,-14.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#E41A33").s().p("AAWA4IAAgsIgrAAIAAAsIgZAAIAAhvIAZAAIAAAvIArAAIAAgvIAZAAIAABvg");
	this.shape_7.setTransform(-443,-14.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#E41A33").s().p("AgjApQgRgQAAgYQAAgZARgQQASgRAXAAQAcAAASAVIgPASQgMgPgSAAQgNAAgKAJQgJAJgBAPQAAAPAKAKQAJAKANAAQASAAALgPIAQARQgTAVgZAAQgZAAgRgRg");
	this.shape_8.setTransform(-466.8,-14.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#E41A33").s().p("AASA4IgYgjIgRAAIAAAjIgZAAIAAhvIAqAAQAaAAALAJQALAJAAATQAAAagWAIIAdAogAgXgBIASAAQAMAAAFgDQAEgFAAgJQAAgIgFgEQgEgEgLAAIgTAAg");
	this.shape_9.setTransform(-490.2,-14.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#E41A33").s().p("AAiA4IgKgYIgvAAIgKAYIgbAAIAxhvIAXAAIAxBvgAgNAKIAbAAIgOgfg");
	this.shape_10.setTransform(-514.4,-14.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},43).to({state:[]},46).wait(32));

	// masque_Titre (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_17 = new cjs.Graphics().p("EgpOArJMAAAhWRMAvJAAAMAAABWRg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(17).to({graphics:mask_graphics_17,x:-263.9,y:-6.1}).wait(25).to({graphics:null,x:0,y:0}).wait(79));

	// _LETTRES_ANARCHIQUES
	this.instance_2 = new lib.lettres_FAT();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-433.8,-73,1,1,0,0,0,0,186.8);
	this.instance_2._off = true;

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#E41A33").s().p("AkUDvIBchzQBxBjBfAAQAsAAAYgSQAZgTAAgfQAAgfgagTQgagThNgSQh6geg4gtQg4gvgBhjQAAhkBIg2QBHg2BrAAQBFAABGAZQBHAYAzArIhPByQhahFhhAAQgnABgWASQgXASAAAfQABAfAbARQAbATBjAYQBjAYA3AwQA3AyAABeQAABehGA6QhGA7h0gBQikAAiFh6g");
	this.shape_11.setTransform(-503.4,7.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#E41A33").s().p("ACLFdIAAkSIkVAAIAAESIicAAIAAq5ICcAAIAAEjIEVAAIAAkjICcAAIAAK5g");
	this.shape_12.setTransform(-179.3,-116);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#E41A33").s().p("AjeEDQhohmAAibQAAibBqhnQBrhpCYAAQCtAABzCEIhgBuQhLhchuAAQhWAAg/A5Qg/A5ABBhQAABhA6A8QA7A6BTAAQBzABBKhcIBjBnQh3CGihAAQigABhphng");
	this.shape_13.setTransform(-265.4,-116.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#E41A33").s().p("AixBCIAAiDIFjAAIAACDg");
	this.shape_14.setTransform(-339.1,-111.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#E41A33").s().p("ABxFdIiZjeIhsAAIAADeIidAAIAAq5IEKAAQChAABGA3QBFA3AAB6QABCmiFAyICwD5gAiUgHIBxAAQBPAAAegaQAdgbAAg4QAAg4gfgWQgegVhJABIh1AAg");
	this.shape_15.setTransform(-409.1,-116);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#E41A33").s().p("ADUFdIhCiWIkjAAIhBCWIinAAIEvq5ICWAAIEtK5gAhXA9ICvAAIhYjJg");
	this.shape_16.setTransform(-497.5,-116);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#E41A33").s().p("AkUDuIBdhyQBwBkBfgBQArAAAZgSQAYgSAAggQAAgfgagTQgZgThNgTQh5gcg5guQg5guABhkQAAhjBHg3QBIg2BqAAQBGAABFAYQBGAYA1AtIhQBxQhbhEhgAAQgnAAgWASQgXASABAfQgBAfAcARQAbATBjAYQBkAYA2AwQA3AxAABeQAABehGA7QhHA7hyAAQimAAiEh8g");
	this.shape_17.setTransform(-199.4,-17.5);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#E41A33").s().p("AkBFeIAAq7IH3AAIAACMIlbAAIAACQIE4AAIAACDIk4AAIAACRIFmAAIAACLg");
	this.shape_18.setTransform(-273.8,-17.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#E41A33").s().p("AhBFeIkZq7ICpAAICxG9ICym9ICpAAIkZK7g");
	this.shape_19.setTransform(-354.6,-17.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#E41A33").s().p("AkkArIAAhVIJJAAIAABVg");
	this.shape_20.setTransform(-437,26.7);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#E41A33").s().p("AhNFeIAAq7ICbAAIAAK7g");
	this.shape_21.setTransform(-501.4,-17.1);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#E41A33").s().p("ACLFeIAAkUIkVAAIAAEUIicAAIAAq7ICcAAIAAEkIEVAAIAAkkICcAAIAAK7g");
	this.shape_22.setTransform(-223.3,-141);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#E41A33").s().p("AjeEDQhphlABicQgBibBrhnQBqhpCaAAQCrAAB1CEIhhBtQhLhchuABQhWgBg/A6Qg/A6ABBgQAABhA6A8QA8A6BSAAQB0AABIhbIBkBoQh3CGihAAQigAAhphng");
	this.shape_23.setTransform(-309.4,-141.4);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#E41A33").s().p("AByFeIiajfIhsAAIAADfIidAAIAAq7IEKAAQCiAABFA3QBFA4AAB6QAACmiEAyICwD6gAiUgHIBxAAQBPAAAdgbQAegZAAg5QAAg4gfgWQgegUhJAAIh1AAg");
	this.shape_24.setTransform(-392.1,-141);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#E41A33").s().p("ADUFeIhCiXIkjAAIhBCXIinAAIEvq7ICVAAIEuK7gAhXA+ICvAAIhYjLg");
	this.shape_25.setTransform(-480.5,-141);

	var maskedShapeInstanceList = [this.instance_2,this.shape_11,this.shape_12,this.shape_13,this.shape_14,this.shape_15,this.shape_16,this.shape_17,this.shape_18,this.shape_19,this.shape_20,this.shape_21,this.shape_22,this.shape_23,this.shape_24,this.shape_25];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_2}]},17).to({state:[{t:this.instance_2}]},14).to({state:[{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11}]},1).to({state:[{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17}]},5).to({state:[]},5).wait(79));
	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(17).to({_off:false},0).to({x:-276.8},14,cjs.Ease.quartOut).to({_off:true},1).wait(89));

	// masque_Titre
	this.instance_3 = new lib.masque_generique();
	this.instance_3.parent = this;
	this.instance_3.setTransform(221.1,-163.9,0.013,0.471,0,0,0,0,17.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({regX:0.5,scaleX:1.92,x:-75.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.01,x:-68.7},10,cjs.Ease.quartInOut).to({_off:true},1).wait(101));

	// masque_Titre
	this.instance_4 = new lib.masque_generique();
	this.instance_4.parent = this;
	this.instance_4.setTransform(-228.9,-13.9,0.013,0.471,0,0,0,0,17.2);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(4).to({_off:false},0).to({regX:0.5,scaleX:1.92,x:-527.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.01,x:-528.7},10,cjs.Ease.quartInOut).to({_off:true},1).wait(97));

	// _LETTRES_CLIENT
	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#E41A33").s().p("AgJA0QgEgEAAgGQAAgFAEgFQAEgEAFAAQAGAAAEAEQAEAFAAAFQAAAGgEAEQgEAFgGAAQgFAAgEgFgAgJAVIgEg5IAAgUIAbAAIAAAUIgFA5g");
	this.shape_26.setTransform(-357.9,-14.1);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#E41A33").s().p("AAbA4Ig1hFIAABFIgZAAIAAhvIAYAAIA2BHIAAhHIAZAAIAABvg");
	this.shape_27.setTransform(-378.9,-14.1);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#E41A33").s().p("AgoA4IAAhvIBPAAIAAAXIg2AAIAAAWIAxAAIAAAUIgxAAIAAAYIA4AAIAAAWg");
	this.shape_28.setTransform(-402.4,-14.1);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#E41A33").s().p("AgMA4IAAhvIAZAAIAABvg");
	this.shape_29.setTransform(-422.5,-14.1);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#E41A33").s().p("AgCA5IADgYIgNAAIgEAYIgZAAIAFgYIgQAAIAFgWIAPAAIADgTIgRAAIAEgWIARAAIAEgaIAXAAIgDAaIANAAIAEgaIAZAAIgFAaIARAAIgEAWIgRAAIgDATIASAAIgEAWIgSAAIgEAYgAgJALIAOAAIAEgTIgOAAg");
	this.shape_30.setTransform(-459.4,-14.3);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#E41A33").s().p("AgjA4IAAhvIAZAAIAABYIAuAAIAAAXg");
	this.shape_31.setTransform(-497.6,-14.1);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#E41A33").s().p("AgjApQgRgQAAgYQAAgZARgQQASgRAXAAQAcAAASAVIgPASQgMgPgSAAQgNAAgKAJQgJAJgBAPQAAAPAKAKQAJAKANAAQASAAALgPIAQARQgTAVgZAAQgZAAgRgRg");
	this.shape_32.setTransform(-520.4,-14.2);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#E41A33").s().p("AgMA4IAAhZIgfAAIAAgWIBXAAIAAAWIggAAIAABZg");
	this.shape_33.setTransform(-411.9,-14.1);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#E41A33").s().p("AAbA4Ig0hFIAABFIgaAAIAAhvIAYAAIA2BHIAAhHIAZAAIAABvg");
	this.shape_34.setTransform(-435.3,-14.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29,p:{x:-422.5}},{t:this.shape_28,p:{x:-402.4}},{t:this.shape_27},{t:this.shape_26,p:{x:-357.9}}]},13).to({state:[{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_26,p:{x:-422.7}}]},5).to({state:[{t:this.shape_32},{t:this.shape_31},{t:this.shape_29,p:{x:-478.9}},{t:this.shape_28,p:{x:-458.8}},{t:this.shape_34},{t:this.shape_33}]},5).to({state:[]},20).wait(78));

	// Layer_11
	this.instance_5 = new lib.masqueTexte("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(-313.6,105.9,0.969,0.85,0,0,0,0.1,108.9);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(37).to({_off:false},0).to({_off:true},40).wait(3).to({_off:false,regX:0.2,regY:108.8,scaleX:0.9,rotation:180,x:-329,y:105.8},0).to({_off:true},40).wait(1));

	// _TEXTE_PRESENTATION
	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AhDBIQgdgbAAgtQAAgtAdgbQAegbAnAAQAoAAAbAYQAcAYAAAqIAAAdIiMAAQACAPAOAKQAPAKARAAQAbAAATgTIAeAhQghAfgrAAQgrAAgdgcgAgagtQgNAKgCARIBTAAQgCgSgKgJQgLgJgQAAQgQAAgNAJg");
	this.shape_35.setTransform(-311.9,121);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AgcBtQgTgTAAgfIAAhZIgXAAIAAgmIAXAAIAAg6IA1AAIAAA6IAvAAIAAAmIgvAAIAABWQAAALAGAHQAFAGAIAAQAQAAAKgOIAUAmQgZAXgcAAQgbAAgTgSg");
	this.shape_36.setTransform(-330.6,118.2);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AgaCIIAAjAIA1AAIAADAgAgWhRQgJgJAAgOQAAgNAJgJQAKgJAMAAQANAAAJAJQAKAJgBANQABAOgKAJQgJAJgNAAQgMAAgKgJg");
	this.shape_37.setTransform(-344.2,117);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("AglBcQgYgHgTgQIAYgiQAeAYAiAAQAKAAAGgFQAGgDAAgIQAAgGgJgGQgIgFgPgFQgQgGgIgDQgJgEgKgHQgXgMAAgbQABgbAVgRQAWgRAhAAQAiAAAiAXIgUAkQgZgRgZAAQgYAAAAANQAAAIAIAFQAIADARAHIAaAJQAIADALAGQAUALAAAcQAAAegVARQgVARggAAQgWAAgWgIg");
	this.shape_38.setTransform(-359.1,121);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AgaCIIAAjAIA1AAIAADAgAgVhRQgJgJAAgOQAAgNAJgJQAIgJANAAQAOAAAJAJQAIAJABANQgBAOgIAJQgJAJgOAAQgNAAgIgJg");
	this.shape_39.setTransform(-373.4,117);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("AgbBhIhMjBIA4AAIAvB5IAxh5IA3AAIhMDBg");
	this.shape_40.setTransform(-389.2,121);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("AgaCIIAAjAIA1AAIAADAgAgWhRQgIgJgBgOQABgNAIgJQAJgJANAAQAOAAAIAJQAJAJAAANQAAAOgJAJQgIAJgOAAQgNAAgJgJg");
	this.shape_41.setTransform(-405,117);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("AAkCGIAAhqQAAgsggAAQgPAAgNAMQgMALABAWIAABpIg2AAIAAkLIA2AAIAABgQAYgZAdAAQAfgBAVAWQAVAWgBAjIAAB2g");
	this.shape_42.setTransform(-421.7,117.3);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("Ag9BIQgegbAAgtQAAgrAegcQAegcApAAQAVAAAWAIQAVAJAQARIgcAlQgJgLgOgHQgNgFgOAAQgUAAgPANQgPAOAAAYQAAAZAPAOQAPANAUAAQAbAAAVgbIAgAkQglAngsAAQgqAAgegcg");
	this.shape_43.setTransform(-443.6,121);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFFFFF").s().p("Ag3BjIAAjBIA2AAIAAAZQAJgLAQgJQAQgIAQAAIAAAyIgKAAQgYAAgMARQgLASAAAaIAABVg");
	this.shape_44.setTransform(-460.9,120.8);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFFFFF").s().p("AhBBSQgWgSAAgeQABgdAVgOQAWgOAlAAIAoAAIAAgBQAAggghAAQgNABgQAFQgPAGgLAHIgZgjQAmgbAxAAQAjAAAWARQAWATABAlIAAB7IgzAAIAAgXQgVAageAAQgeAAgVgSgAggAiQAAAKAHAGQAJAGAOAAQANgBALgIQALgJAAgPIAAgJIghAAQggAAAAAUg");
	this.shape_45.setTransform(-480.2,121);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FFFFFF").s().p("AgVAwIAAhfIAqAAIAABfg");
	this.shape_46.setTransform(-495.1,110.3);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFFFFF").s().p("AhIBqQgbgdAAgsQAAgqAagcQAbgbAjAAQAiAAAWAZIAAhfIA3AAIAAEKIg3AAIAAgaQgWAdgiAAQgiAAgbgdgAgfgDQgOAPAAAXQAAAYAOAPQAOAPASAAQATAAANgPQANgPAAgYQAAgXgNgPQgNgQgTAAQgSAAgOAQg");
	this.shape_47.setTransform(-512.4,117.5);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FFFFFF").s().p("Ag3BjIAAjBIA2AAIAAAZQAJgMAQgIQAQgIAQgBIAAA0IgKAAQgYgBgMASQgLAQAAAcIAABUg");
	this.shape_48.setTransform(-220.7,76.9);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FFFFFF").s().p("AhDBJQgdgcAAgtQAAgtAdgbQAegbAnAAQAoAAAbAYQAcAYAAArIAAAcIiMAAQACAPAOAKQAPAKARAAQAbAAATgTIAeAhQghAfgrAAQgrAAgdgbgAgagsQgNAJgCARIBTAAQgCgRgKgKQgLgJgQAAQgQAAgNAKg");
	this.shape_49.setTransform(-239.9,77.1);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FFFFFF").s().p("AgaCIIAAjAIA1AAIAADAgAgWhRQgJgJAAgOQAAgNAJgJQAKgJAMAAQANAAAJAJQAKAJgBANQABAOgKAJQgJAJgNAAQgMAAgKgJg");
	this.shape_50.setTransform(-256.4,73.1);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FFFFFF").s().p("AgcBtQgUgTAAgfIAAhZIgWAAIAAgmIAWAAIAAg6IA2AAIAAA6IAvAAIAAAmIgvAAIAABWQAAALAGAHQAGAGAHAAQAPAAALgOIAUAmQgZAXgcAAQgbAAgTgSg");
	this.shape_51.setTransform(-269.1,74.3);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FFFFFF").s().p("AhDBJQgdgcAAgtQAAgtAdgbQAegbAnAAQAoAAAbAYQAcAYAAArIAAAcIiMAAQACAPAOAKQAPAKARAAQAbAAATgTIAeAhQghAfgrAAQgrAAgdgbgAgagsQgNAJgCARIBTAAQgCgRgKgKQgLgJgQAAQgQAAgNAKg");
	this.shape_52.setTransform(-288.5,77.1);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FFFFFF").s().p("ABiBjIAAhqQAAgsggAAQgRAAgLAMQgNALAAAXIAABoIg1AAIAAhqQAAgXgHgKQgHgLgPAAQgQAAgMAMQgNALAAAXIAABoIg2AAIAAjBIA2AAIAAAWQAXgaAcAAQATAAAPALQAOALAHAPQALgRAUgKQASgKAUAAQAiAAAUAVQAWAUgBAnIAAB1g");
	this.shape_53.setTransform(-317.3,76.9);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FFFFFF").s().p("AhEBOQgWgUAAgnIAAh0IA2AAIAABoQAAAtAhAAQAPAAAMgMQAMgMAAgWIAAhnIA3AAIAADAIg3AAIAAgZQgUAcgfAAQggAAgVgUg");
	this.shape_54.setTransform(-356.3,77.2);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FFFFFF").s().p("AhBBSQgWgRAAgfQABgdAVgOQAWgOAlAAIAoAAIAAgBQgBggggAAQgNAAgQAGQgPAGgLAHIgZgiQAmgcAxAAQAjAAAWARQAWATABAmIAAB6IgzAAIAAgXQgVAageAAQgfAAgUgSgAggAiQAAAKAHAGQAJAFAOABQANgBAMgIQAKgJAAgPIAAgJIghAAQggAAAAAUg");
	this.shape_55.setTransform(-378.8,77.1);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FFFFFF").s().p("AhEBJQgcgcAAgtQAAgtAdgbQAdgbAnAAQApAAAbAYQAcAYAAArIAAAcIiMAAQADAPAOAKQAOAKAQAAQAdAAASgTIAfAhQgiAfgsAAQgrAAgdgbgAgagsQgOAJgCARIBVAAQgDgRgLgKQgLgJgQAAQgPAAgNAKg");
	this.shape_56.setTransform(-409.6,77.1);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FFFFFF").s().p("AhEB0QgcgbAAguQAAgtAdgbQAegbAnAAQAoAAAbAYQAcAZAAApIAAAdIiMAAQACAPAPAKQAOAKAQAAQAcAAATgTIAfAhQgiAfgsAAQgrAAgdgbgAgagBQgOAIgBARIBTAAQgBgRgLgJQgMgJgPAAQgPAAgOAKgAgghPIAsg/IA4AXIgyAog");
	this.shape_57.setTransform(-431.9,72.7);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#FFFFFF").s().p("AgaCIIAAjAIA1AAIAADAgAgWhRQgIgJgBgOQABgNAIgJQAJgJANAAQAOAAAIAJQAJAJAAANQAAAOgJAJQgIAJgOAAQgNAAgJgJg");
	this.shape_58.setTransform(-448.3,73.1);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#FFFFFF").s().p("AhIBqQgbgdAAgsQAAgqAagbQAbgcAjAAQAiAAAWAZIAAhfIA3AAIAAEKIg3AAIAAgZQgWAcgiAAQgiAAgbgdgAgfgDQgOAPAAAYQAAAXAOAPQAOAPASAAQATAAANgPQANgPAAgXQAAgYgNgPQgNgPgTAAQgSAAgOAPg");
	this.shape_59.setTransform(-466.1,73.6);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#FFFFFF").s().p("AhEB0QgcgbAAguQAAgtAdgbQAdgbAnAAQAoAAAcAYQAcAZAAApIAAAdIiMAAQADAPANAKQAOAKASAAQAcAAASgTIAeAhQggAfgsAAQgrAAgegbgAgagBQgOAIgCARIBVAAQgCgRgMgJQgKgJgRAAQgOAAgOAKgAghhPIAtg/IA4AXIgyAog");
	this.shape_60.setTransform(-488.6,72.7);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#FFFFFF").s().p("AhIBqQgbgdAAgsQAAgqAagbQAbgcAjAAQAiAAAWAZIAAhfIA3AAIAAEKIg3AAIAAgZQgWAcgiAAQgiAAgbgdgAgfgDQgOAPAAAYQAAAXAOAPQAOAPASAAQATAAANgPQANgPAAgXQAAgYgNgPQgNgPgTAAQgSAAgOAPg");
	this.shape_61.setTransform(-512.4,73.6);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#FFFFFF").s().p("AhEBJQgcgbAAguQAAgtAdgbQAdgbAnAAQAoAAAcAYQAcAZAAAqIAAAbIiMAAQADAQANAKQAOAKASAAQAcAAASgTIAeAhQggAfgsAAQgrAAgegbgAgagsQgOAJgCARIBVAAQgCgRgMgKQgKgJgRAAQgOAAgOAKg");
	this.shape_62.setTransform(-133.1,33.2);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#FFFFFF").s().p("AgbBhIhMjBIA4AAIAvB5IAxh5IA3AAIhNDBg");
	this.shape_63.setTransform(-154.9,33.2);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#FFFFFF").s().p("AgaCIIAAjAIA1AAIAADAgAgWhRQgIgJgBgOQABgNAIgJQAJgJANAAQAOAAAIAJQAJAJAAANQAAAOgJAJQgIAJgOAAQgNAAgJgJg");
	this.shape_64.setTransform(-170.7,29.2);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#FFFFFF").s().p("AgcBtQgTgTgBgfIAAhZIgWAAIAAgmIAWAAIAAg6IA2AAIAAA6IAvAAIAAAmIgvAAIAABWQAAALAGAHQAFAGAJAAQAPAAAKgOIAUAmQgZAXgcAAQgbAAgTgSg");
	this.shape_65.setTransform(-183.4,30.4);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#FFFFFF").s().p("Ag9BJQgegcAAgsQAAgsAegcQAegcApAAQAVAAAWAJQAVAIAQARIgcAlQgJgLgOgGQgNgHgOAAQgUABgPAOQgPAOAAAXQAAAYAPAOQAPAOAUAAQAbAAAVgbIAgAkQglAngsAAQgqAAgegbg");
	this.shape_66.setTransform(-202.1,33.2);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#FFFFFF").s().p("AhCBSQgUgSgBgdQAAgeAWgOQAWgPAlAAIAnAAIAAAAQABgfghAAQgNgBgQAGQgQAFgKAJIgYgjQAmgcAwAAQAjAAAWASQAWARAAAnIAAB6IgyAAIAAgXQgVAageAAQgfAAgVgSgAggAiQAAAKAIAGQAHAFAPABQANAAALgKQALgIAAgPIAAgJIghAAQggAAAAAUg");
	this.shape_67.setTransform(-223.9,33.2);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#FFFFFF").s().p("Ag3BiIAAjAIA2AAIAAAZQAJgLAQgJQAQgIAQgBIAAA0IgKAAQgYAAgMARQgLARAAAbIAABTg");
	this.shape_68.setTransform(-240.8,33);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#FFFFFF").s().p("AhEBJQgcgbAAguQAAgtAdgbQAegbAnAAQAoAAAbAYQAcAZAAAqIAAAbIiMAAQACAQAPAKQAOAKAQAAQAcAAATgTIAfAhQgiAfgsAAQgqAAgegbgAgagsQgOAJgBARIBTAAQgBgRgLgKQgMgJgPAAQgPAAgOAKg");
	this.shape_69.setTransform(-260,33.2);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#FFFFFF").s().p("AgcBtQgUgTAAgfIAAhZIgWAAIAAgmIAWAAIAAg6IA2AAIAAA6IAvAAIAAAmIgvAAIAABWQAAALAGAHQAGAGAHAAQAPAAALgOIAUAmQgZAXgcAAQgbAAgTgSg");
	this.shape_70.setTransform(-278.6,30.4);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#FFFFFF").s().p("AAkBiIAAhsQABgtghAAQgPAAgMAMQgNAMAAAWIAABrIg2AAIAAjAIA2AAIAAAVQAZgZAdAAQAfABAVAVQAUAWAAAjIAAB1g");
	this.shape_71.setTransform(-298.4,33);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#FFFFFF").s().p("AgaCIIAAjAIA1AAIAADAgAgVhRQgJgJAAgOQAAgNAJgJQAIgJANAAQAOAAAJAJQAIAJABANQgBAOgIAJQgJAJgOAAQgNAAgIgJg");
	this.shape_72.setTransform(-315.3,29.2);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#FFFFFF").s().p("AAlBiIAAhsQAAgtghAAQgQAAgLAMQgMAMAAAWIAABrIg2AAIAAjAIA2AAIAAAVQAYgZAeAAQAeABAVAVQAUAWABAjIAAB1g");
	this.shape_73.setTransform(-341.4,33);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#FFFFFF").s().p("AhIBHQgdgdAAgqQAAgpAdgdQAdgdArAAQAsAAAdAdQAdAdAAApQAAAqgdAdQgdAdgsAAQgrAAgdgdgAghglQgOAPAAAWQAAAYAOAPQANAPAUAAQAVAAAOgPQANgPAAgYQAAgWgNgPQgOgQgVAAQgUAAgNAQg");
	this.shape_74.setTransform(-364.7,33.2);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#FFFFFF").s().p("AgaCIIAAjAIA1AAIAADAgAgWhRQgIgJgBgOQABgNAIgJQAJgJANAAQAOAAAIAJQAJAJAAANQAAAOgJAJQgIAJgOAAQgNAAgJgJg");
	this.shape_75.setTransform(-381.6,29.2);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#FFFFFF").s().p("AgcBtQgTgTgBgfIAAhZIgWAAIAAgmIAWAAIAAg6IA2AAIAAA6IAvAAIAAAmIgvAAIAABWQAAALAGAHQAFAGAJAAQAOAAALgOIAUAmQgZAXgcAAQgbAAgTgSg");
	this.shape_76.setTransform(-394.3,30.4);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#FFFFFF").s().p("AgaCIIAAjAIA1AAIAADAgAgVhRQgJgJAAgOQAAgNAJgJQAIgJANAAQAOAAAJAJQAIAJABANQgBAOgIAJQgJAJgOAAQgNAAgIgJg");
	this.shape_77.setTransform(-408,29.2);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("#FFFFFF").s().p("AgmBcQgXgIgTgPIAYghQAfAXAgAAQALAAAGgEQAGgEAAgHQAAgHgJgGQgIgGgPgFQgQgFgIgEQgJgDgLgHQgVgNgBgaQAAgbAWgRQAWgRAhAAQAiAAAiAXIgUAkQgZgRgZAAQgYAAAAAOQAAAHAIAEQAIAFARAFIAaAJQAIAEAKAGQAVALAAAdQAAAdgVAQQgVASghAAQgUAAgYgIg");
	this.shape_78.setTransform(-422.9,33.2);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("#FFFFFF").s().p("AhIBHQgdgdAAgqQAAgpAdgdQAdgdArAAQAsAAAdAdQAdAdAAApQAAAqgdAdQgdAdgsAAQgrAAgdgdgAghglQgOAPAAAWQAAAYAOAPQANAPAUAAQAVAAAOgPQANgPAAgYQAAgWgNgPQgOgQgVAAQgUAAgNAQg");
	this.shape_79.setTransform(-443.6,33.2);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f("#FFFFFF").s().p("AhjCGIAAkHIA2AAIAAAVQAZgZAhAAQAiABAaAcQAbAcAAArQAAAqgbAcQgaAdgkAAQgiAAgWgcIAABggAgghIQgNAQAAAYQAAAYANAOQANAOATAAQARAAAOgOQAOgOAAgYQAAgYgNgPQgOgQgSAAQgTAAgNAPg");
	this.shape_80.setTransform(-466.8,36.5);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("#FFFFFF").s().p("AAkBhIglg2IgkA2Ig/AAIBEhiIhAhfIBBAAIAgA0IAig0IA/AAIhCBfIBFBig");
	this.shape_81.setTransform(-489.8,33.2);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f("#FFFFFF").s().p("AhcB+IAAj7IC1AAIAAAyIh9AAIAAA0IBwAAIAAAvIhwAAIAAA0ICBAAIAAAyg");
	this.shape_82.setTransform(-511.5,30.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_82},{t:this.shape_81},{t:this.shape_80},{t:this.shape_79},{t:this.shape_78},{t:this.shape_77},{t:this.shape_76},{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35}]},57).to({state:[]},44).wait(20));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(221.1,-172,2,16);


// stage content:
(lib.FS_projet_archives = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":150});

	// timeline functions:
	this.frame_99 = function() {
		//var frequency = 60;
		//stage.enableMouseOver(frequency);
		
		// EVENT DE CLIC POUR LA SUITE
		this.next_btn.addEventListener("click", fl_ClickToGoToAndPlayFromFrame.bind(this));
		this.next_btn.addEventListener("mouseover", fl_MouseOverHandler.bind(this));
		this.next_btn.addEventListener("mouseout", fl_MouseOutHandler.bind(this));
		
		
		function fl_ClickToGoToAndPlayFromFrame()
		{
			//this.gotoAndPlay("close");
			
			var event = new Event('next');
			this.dispatchEvent(event);		
			event = null;
			
			this.next_btn.btnNext.gotoAndPlay('rollOut');
			
		}
		function fl_MouseOverHandler(){	this.next_btn.btnNext.gotoAndPlay('rollOver'); }
		function fl_MouseOutHandler(){	this.next_btn.btnNext.gotoAndPlay('rollOut'); }
		
		
		// EVENT DE CLIC POUR LE LIEN
		this.cta_mc.addEventListener("click", projectDetails.bind(this));
		this.cta_mc.addEventListener("mouseover", pDetails_MouseOverHandler.bind(this));
		this.cta_mc.addEventListener("mouseout", pDetails_MouseOutHandler.bind(this));
		
		function projectDetails()
		{	
			var event = new Event('details');
			this.dispatchEvent(event);		
			event = null;
			
			this.cta_mc.btnCta.gotoAndPlay('rollOut');
			
		}
		function pDetails_MouseOverHandler(){	this.cta_mc.btnCta.gotoAndPlay('rollOver'); }
		function pDetails_MouseOutHandler(){	this.cta_mc.btnCta.gotoAndPlay('rollOut'); }
	}
	this.frame_146 = function() {
		//PAUSE
		this.stop();
	}
	this.frame_150 = function() {
		this.textes_mc.gotoAndPlay("close");
		this.visuel_mc.gotoAndPlay("close");
		this.cta_mc.gotoAndPlay("close");
		this.video_mc.gotoAndPlay("close");
		this.next_btn.gotoAndPlay("close");
		this.lignes_mc.gotoAndPlay("close_lines");
	}
	this.frame_169 = function() {
		//this.lignes_mc.gotoAndPlay("close");
	}
	this.frame_211 = function() {
		// EVENT DE FIN D'ANIM
		var event = new Event('closed');
		this.dispatchEvent(event);		
		event = null;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(99).call(this.frame_99).wait(47).call(this.frame_146).wait(4).call(this.frame_150).wait(19).call(this.frame_169).wait(42).call(this.frame_211).wait(1));

	// BLOC_INFOS_TEXTE
	this.textes_mc = new lib.blocInfostextes();
	this.textes_mc.name = "textes_mc";
	this.textes_mc.parent = this;
	this.textes_mc.setTransform(858.2,182,1,1,0,0,0,222.1,-164);
	this.textes_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.textes_mc).wait(17).to({_off:false},0).wait(195));

	// VISUEL
	this.visuel_mc = new lib.bloc_visuel();
	this.visuel_mc.name = "visuel_mc";
	this.visuel_mc.parent = this;
	this.visuel_mc.setTransform(1192.6,391.5,1,1,0,0,0,552.5,26.5);
	this.visuel_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.visuel_mc).wait(17).to({_off:false},0).wait(195));

	// CTA_NEXT
	this.next_btn = new lib.bloc_NEXT_btn();
	this.next_btn.name = "next_btn";
	this.next_btn.parent = this;
	this.next_btn.setTransform(753.2,786.9,1,1,0,0,0,-340.5,403.9);
	this.next_btn._off = true;

	this.timeline.addTween(cjs.Tween.get(this.next_btn).wait(29).to({_off:false},0).wait(183));

	// CTA_DETAIL
	this.cta_mc = new lib.bloc_CTA();
	this.cta_mc.name = "cta_mc";
	this.cta_mc.parent = this;
	this.cta_mc.setTransform(299.5,787,1,1,0,0,0,-340.5,403.9);
	this.cta_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.cta_mc).wait(14).to({_off:false},0).to({_off:true},159).wait(39));

	// VIDEO
	this.video_mc = new lib.bloc_video();
	this.video_mc.name = "video_mc";
	this.video_mc.parent = this;
	this.video_mc.setTransform(866.7,1147.2,1,1,0,0,0,227.5,764.9);
	this.video_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.video_mc).wait(94).to({_off:false},0).to({_off:true},79).wait(39));

	// lignes
	this.lignes_mc = new lib.lignes_all();
	this.lignes_mc.name = "lignes_mc";
	this.lignes_mc.parent = this;
	this.lignes_mc.setTransform(640,384.3,1,1,0,0,0,0,406.3);

	this.timeline.addTween(cjs.Tween.get(this.lignes_mc).wait(212));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(825.5,361.5,909.1,813.5);
// library properties:
lib.properties = {
	id: '96AA1A097579EC49ABE7107D2D205A02',
	width: 1280,
	height: 768,
	fps: 60,
	color: "#000000",
	opacity: 1.00,
	manifest: [
		{src:"images/archivesDep_parchemin.png?1561738278484", id:"archivesDep_parchemin"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['96AA1A097579EC49ABE7107D2D205A02'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;