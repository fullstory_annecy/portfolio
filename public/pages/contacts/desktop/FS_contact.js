(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.visuel_equipe_580x960jpgcopy = function() {
	this.initialize(img.visuel_equipe_580x960jpgcopy);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,580,960);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.video = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.2)").s().p("EgtTBLAMAAAiV/MBanAAAMAAACV/g");
	this.shape.setTransform(290,479.975);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.video, new cjs.Rectangle(0,0,580,960), null);


(lib.txt_hashtag = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgKAxIAAgmIgig7IAYAAIAUAkIAVgkIAYAAIgiA7IAAAmg");
	this.shape.setTransform(49.675,73.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgeAlQgPgPAAgWQAAgUAPgPQAPgPAVAAQAYAAAQASIgNAQQgLgMgPAAQgMAAgIAIQgJAHAAANQgBANAJAJQAIAIALAAQAQAAALgMIANAOQgQASgXABQgVAAgPgOg");
	this.shape_1.setTransform(40.3,73.75);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBGAAIAAATIgwAAIAAAVIAqAAIAAASIgqAAIAAAUIAxAAIAAATg");
	this.shape_2.setTransform(30.75,73.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAXAxIgtg9IAAA9IgWAAIAAhhIAUAAIAvA+IAAg+IAWAAIAABhg");
	this.shape_3.setTransform(20.225,73.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAXAxIgtg9IAAA9IgWAAIAAhhIAUAAIAvA+IAAg+IAWAAIAABhg");
	this.shape_4.setTransform(8.775,73.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AAeAxIgKgVIgoAAIgJAVIgXAAIAqhhIAVAAIAqBhgAgLAIIAXAAIgMgbg");
	this.shape_5.setTransform(-2.175,73.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgdAlQgKgOAAgXQAAgWAKgOQAKgOATAAQAUAAAKAOQAKAOAAAWQAAAXgKAOQgKAOgUAAQgTAAgKgOgAgNgXQgFAJAAAOQAAAPAFAJQAEAIAJABQAKgBAFgIQAEgJAAgPQAAgOgEgJQgFgIgKgBQgJABgEAIg");
	this.shape_6.setTransform(-15.925,73.75);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgPAuQgIgEgEgGQgJgPABgUQAAgMADgLQADgKAGgGQALgMAPAAQAIAAAFADIAKADIAHAGIgKAPIgCgCIgHgDQgEgDgFAAQgIAAgFAGQgFAHgBAKQAJgGAJAAQAOAAAJAJQAKAIAAAPQgBAPgJAJQgKAIgPABQgKgBgHgEgAgHAIQgFADAAAHQAAAFAEAFQAEAFAGAAQAGgBAEgEQAFgDgBgHQAAgGgEgEQgDgFgHAAQgFAAgEAFg");
	this.shape_7.setTransform(-25.1,73.75);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgQAwIgKgDIgHgGIALgPIACACIAHADQAFADAEAAQAHAAAFgGQAGgGABgLQgJAGgJAAQgOAAgKgJQgIgIgBgPQAAgOAKgKQAKgIAPgBQAJAAAIAFQAIADAEAHQAJAOgBAVQABAMgEALQgDAKgGAGQgLAMgQAAQgHgBgGgCgAgLgbQgEAEAAAGQAAAGAEAEQADAEAGABQAGAAAEgEQAFgEAAgGQAAgHgFgDQgEgFgFgBQgHAAgDAFg");
	this.shape_8.setTransform(-33.95,73.75);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AAEAxIAAgYIgnAAIAAgQIAjg5IAXAAIggA3IANAAIAAgRIAVAAIAAARIALAAIAAASIgLAAIAAAYg");
	this.shape_9.setTransform(-42.525,73.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgbAxIAkhOIgXAAIAAANIgVAAIAAggIBGAAIAAARIgmBQg");
	this.shape_10.setTransform(-50.5,73.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgKAxIAAhOIgbAAIAAgTIBLAAIAAATIgbAAIAABOg");
	this.shape_11.setTransform(148.9,52.75);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AAeAxIgKgVIgoAAIgJAVIgXAAIAqhhIAVAAIAqBhgAgLAJIAXAAIgMgcg");
	this.shape_12.setTransform(139.425,52.75);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgrAxIAAhhIAiAAQAZAAAOANQAOAMABAXQgBAWgOAOQgNANgbAAgAgWAeIAOAAQAPgBAIgHQAHgIAAgOQAAgOgHgHQgJgIgPAAIgNAAg");
	this.shape_13.setTransform(129.1,52.75);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBGAAIAAATIgwAAIAAAVIAqAAIAAASIgqAAIAAAUIAxAAIAAATg");
	this.shape_14.setTransform(119.1,52.75);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgmAiIANgQQAQAOANAAQAGgBADgDQADgCAAgFQAAgEgDgCQgEgDgKgDQgRgDgIgGQgIgGAAgOQAAgPALgHQAKgIANAAQAKAAAKAEQAKADAHAHIgLAQQgNgKgNAAQgFAAgDACQgDADAAAEQAAAEAEADQAEACANAEQAOADAHAGQAIAHAAANQAAANgKAJQgKAHgQABQgWAAgTgRg");
	this.shape_15.setTransform(109.725,52.7);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgmAiIANgQQAQAOANAAQAGgBADgDQADgCAAgFQAAgEgDgCQgEgDgKgDQgRgDgIgGQgIgGAAgOQAAgPALgHQAKgIANAAQAKAAAKAEQAKADAHAHIgLAQQgNgKgNAAQgFAAgDACQgDADAAAEQAAAEAEADQAEACANAEQAOADAHAGQAIAHAAANQAAANgKAJQgKAHgQABQgWAAgTgRg");
	this.shape_16.setTransform(100.775,52.7);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgdAmQgMgMAAgUIAAg2IAWAAIAAA1QAAAMAGAGQAFAHAIAAQAJAAAGgHQAFgGAAgMIAAg1IAWAAIAAA2QAAAUgMAMQgMAMgSAAQgRAAgMgMg");
	this.shape_17.setTransform(91.15,52.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AAeAxIgKgVIgoAAIgJAVIgXAAIAqhhIAVAAIAqBhgAgLAJIAXAAIgMgcg");
	this.shape_18.setTransform(80.675,52.75);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AAXAxIgtg9IAAA9IgWAAIAAhhIAUAAIAvA+IAAg+IAWAAIAABhg");
	this.shape_19.setTransform(66.025,52.75);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgKAxIAAhhIAVAAIAABhg");
	this.shape_20.setTransform(58,52.75);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgKAxIAAhOIgbAAIAAgTIBLAAIAAATIgbAAIAABOg");
	this.shape_21.setTransform(51.45,52.75);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgmAiIANgQQAQAOANAAQAGgBADgDQADgCAAgFQAAgEgDgCQgEgDgKgDQgRgDgIgGQgIgGAAgOQAAgPALgHQAKgIANAAQAKAAAKAEQAKADAHAHIgLAQQgNgKgNAAQgFAAgDACQgDADAAAEQAAAEAEADQAEACANAEQAOADAHAGQAIAHAAANQAAANgKAJQgKAHgQABQgWAAgTgRg");
	this.shape_22.setTransform(42.625,52.7);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgdAmQgMgMAAgUIAAg2IAWAAIAAA1QAAAMAGAGQAFAHAIAAQAJAAAGgHQAFgGAAgMIAAg1IAWAAIAAA2QAAAUgMAMQgMAMgSAAQgRAAgMgMg");
	this.shape_23.setTransform(33,52.8);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgeAkQgOgOAAgWQgBgVAQgOQAOgPAVAAQAUAAAQAOIgMARQgGgGgGgCQgFgCgGAAQgMgBgJAJQgIAJAAAMQAAAOAIAJQAJAIAJAAQAMAAAIgFIAAgaIAWAAIAAAjQgPAQgZAAQgWAAgOgPg");
	this.shape_24.setTransform(22.45,52.7);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgdAmQgMgMAAgUIAAg2IAWAAIAAA1QAAAMAFAGQAGAHAIAAQAKAAAFgHQAFgGAAgMIAAg1IAWAAIAAA2QAAAUgMAMQgLAMgTAAQgRAAgMgMg");
	this.shape_25.setTransform(12,52.8);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AAeAxIgKgVIgoAAIgJAVIgXAAIAqhhIAVAAIAqBhgAgLAJIAXAAIgMgcg");
	this.shape_26.setTransform(1.525,52.75);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgIAJQgDgEAAgFQAAgEADgEQAEgDAEAAQAFAAAEADQADAEAAAEQAAAFgDAEQgEADgFAAQgEAAgEgDg");
	this.shape_27.setTransform(-13.125,56.525);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AgmAxIAAhhIAjAAQAVAAAKAJQALAIAAATQAAARgLAIQgKAJgVgBIgNAAIAAAcgAgQADIAOAAQAKAAAFgEQADgEAAgJQAAgIgFgDQgFgEgJAAIgNAAg");
	this.shape_28.setTransform(-19.55,52.75);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AgmAiIANgQQAQAOANAAQAGgBADgDQADgCAAgFQAAgEgDgCQgEgDgKgDQgRgDgIgGQgIgGAAgOQAAgPALgHQAKgIANAAQAKAAAKAEQAKADAHAHIgLAQQgNgKgNAAQgFAAgDACQgDADAAAEQAAAEAEADQAEACANAEQAOADAHAGQAIAHAAANQAAANgKAJQgKAHgQABQgWAAgTgRg");
	this.shape_29.setTransform(-29.225,52.7);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBGAAIAAATIgwAAIAAAVIAqAAIAAASIgqAAIAAAUIAxAAIAAATg");
	this.shape_30.setTransform(-37.95,52.75);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AgiAmIAJgRQAOAKALAAQAFgBAEgDQAFgDAAgGQAAgGgFgEQgFgDgHAAQgFAAgJACIAAgPIASgVIgdAAIAAgTIA7AAIAAAPIgVAXQAMABAGAIQAHAIAAAKQAAAPgKAJQgLAJgPAAQgRAAgQgMg");
	this.shape_31.setTransform(-50.7,52.8);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AAQAxIgVgfIgPAAIAAAfIgWAAIAAhhIAlAAQAWAAAKAIQAKAHAAARQAAAXgTAHIAZAjgAgUAAIAQAAQAKAAAEgEQAEgDAAgJQAAgHgEgDQgEgDgKAAIgQAAg");
	this.shape_32.setTransform(105.775,10.65);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AghAxIAAhhIBDAAIAAAUIgsAAIAAAVIAqAAIAAARIgqAAIAAAng");
	this.shape_33.setTransform(96.35,10.65);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AgIAJQgDgEAAgFQAAgEADgEQAEgDAEAAQAFAAAEADQADAEAAAEQAAAFgDAEQgEADgFAAQgEAAgEgDg");
	this.shape_34.setTransform(89.775,14.425);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AgKAxIAAgnIgig6IAYAAIAUAkIAVgkIAYAAIgiA6IAAAng");
	this.shape_35.setTransform(83.425,10.65);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AAQAxIgVgfIgPAAIAAAfIgWAAIAAhhIAlAAQAWAAAKAIQAKAHAAARQAAAXgTAHIAZAjgAgUAAIAQAAQAKAAAEgEQAEgDAAgJQAAgHgEgDQgEgDgKAAIgQAAg");
	this.shape_36.setTransform(74.375,10.65);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AgjAkQgPgPAAgVQAAgVAPgOQAOgOAVAAQAWAAAPAOQAOAOAAAVQAAAVgOAPQgPAPgWAAQgVAAgOgPgAgTgVQgJAJAAAMQAAANAJAKQAIAIALABQAMgBAJgIQAIgKAAgNQAAgMgIgJQgJgKgMABQgLgBgIAKg");
	this.shape_37.setTransform(63.125,10.6);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("AgKAxIAAhOIgcAAIAAgTIBNAAIAAATIgcAAIAABOg");
	this.shape_38.setTransform(53.1,10.65);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AgmAhIANgQQAQAOANABQAGAAADgEQADgCAAgEQAAgFgDgCQgEgDgKgDQgRgEgIgFQgIgHAAgNQAAgOALgIQAKgIANABQAKgBAKAEQAKADAHAGIgLARQgNgLgNABQgFgBgDADQgDADAAAEQAAAFAEACQAEADANADQAOADAHAGQAIAHAAANQAAANgKAIQgKAJgQAAQgWgBgTgRg");
	this.shape_39.setTransform(44.275,10.6);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("AgfAxIAAhhIAWAAIAABNIApAAIAAAUg");
	this.shape_40.setTransform(36.325,10.65);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("AgfAxIAAhhIAWAAIAABNIApAAIAAAUg");
	this.shape_41.setTransform(28.375,10.65);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("AgdAmQgMgLAAgVIAAg2IAWAAIAAA1QAAAMAFAGQAGAHAIAAQAKAAAFgHQAFgGAAgMIAAg1IAWAAIAAA2QAAAVgMALQgLAMgTAAQgRAAgMgMg");
	this.shape_42.setTransform(18.75,10.7);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("AggAxIAAhhIBBAAIAAAUIgsAAIAAAVIAqAAIAAARIgqAAIAAAng");
	this.shape_43.setTransform(9.55,10.65);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFFFFF").s().p("AgnAoQgQgQAAgXQAAgWARgRQARgRAWAAQAXAAAQAQQAQAQAAAXQAAAQgHAJQgGAJgKAAQgGAAgEgDQgEgDgCgFQgIAKgLAAQgMAAgIgJQgJgJAAgOQAAgNAIgJQAIgKANAAQADAAAEACIAGAEIABACIAAgGIASAAIAAAsQAAAHAEAAQAEAAADgGQADgGAAgJQAAgTgMgNQgLgNgTAAQgSAAgNANQgNAOAAASQAAATAMANQAMAMASAAQANAAALgKIAGAKQgGAFgIADQgJADgHAAQgXAAgQgQgAgKgKQgDAFAAAFQAAAHADAFQAEAFAGAAQAFAAAEgFQAEgEAAgHQAAgGgEgFQgEgFgFAAQgGAAgEAFg");
	this.shape_44.setTransform(-1.225,11.675);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFFFFF").s().p("AgjAkQgPgPAAgVQAAgVAPgOQAOgOAVAAQAWAAAPAOQAOAOAAAVQAAAVgOAPQgPAPgWAAQgVAAgOgPgAgTgVQgJAJAAAMQAAANAJAKQAIAIALABQAMgBAJgIQAIgKAAgNQAAgMgIgJQgJgKgMABQgLgBgIAKg");
	this.shape_45.setTransform(-13.225,10.6);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FFFFFF").s().p("AgfAxIAAhhIAWAAIAABNIApAAIAAAUg");
	this.shape_46.setTransform(-22.575,10.65);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFFFFF").s().p("AgfAxIAAhhIAWAAIAABNIApAAIAAAUg");
	this.shape_47.setTransform(-30.525,10.65);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBFAAIAAAUIgvAAIAAAUIArAAIAAASIgrAAIAAATIAxAAIAAAUg");
	this.shape_48.setTransform(-39.25,10.65);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FFFFFF").s().p("AAUAxIAAgnIgmAAIAAAnIgWAAIAAhhIAWAAIAAApIAmAAIAAgpIAVAAIAABhg");
	this.shape_49.setTransform(-49.375,10.65);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.txt_hashtag, new cjs.Rectangle(-56.7,0,215.8,84.2), null);


(lib.project_illustration_placeHolder = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.visuel_equipe_580x960jpgcopy();
	this.instance.setTransform(-262.45,-79.85,0.611,0.611,4.9986);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.project_illustration_placeHolder, new cjs.Rectangle(-313.5,-79.8,404.1,615.1999999999999), null);


(lib.masqueTexte_1_ligne = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("EgicAEJIAAoRMBE5AAAIAAIRg");
	this.shape.setTransform(0,24.55);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.masqueTexte_1_ligne, new cjs.Rectangle(-220.5,-1.9,441,53), null);


(lib.masque_generique = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CC9900").s().p("AsQCqIAAlTIYhAAIAAFTg");
	this.shape.setTransform(78.5,17);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.masque_generique, new cjs.Rectangle(0,0,157,34), null);


(lib.ligne = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(49));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#333333").ss(1,1,1).p("EAAAg/bMAAAB+3");
	this.shape.setTransform(0,406);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#333333").ss(1,1,1).p("EAA/A/cQhDv0ggwoQgdwxAExFQAEwXAhwHQAfufA4to");
	this.shape_1.setTransform(-6.0239,406);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#333333").ss(1,1,1).p("EABwA/cQh4vfg5w8Qg0wqAHxQQAHweA7wDQA5uxBjtQ");
	this.shape_2.setTransform(-10.6719,406);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#333333").ss(1,1,1).p("EACVA/cQigvPhLxLQhHwmAKxYQAJwjBPv/QBMvACEs9");
	this.shape_3.setTransform(-14.2371,406);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#333333").ss(1,1,1).p("EACwA/cQi+vDhZxWQhTwjAMxeQAKwnBev8QBavKCcsw");
	this.shape_4.setTransform(-16.895,406);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#333333").ss(1,1,1).p("EADEA/cQjTu7hjxdQhdwhANxhQAMwrBov6QBkvSCusm");
	this.shape_5.setTransform(-18.7801,406);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#333333").ss(1,1,1).p("EADSA/cQjiu1hqxjQhjwfAOxlQAMwsBvv5QBsvXC6sf");
	this.shape_6.setTransform(-20.0814,406);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#333333").ss(1,1,1).p("EADbA/cQjsuxhuxnQhnweAOxnQANwtB0v4QBwvaDCsb");
	this.shape_7.setTransform(-20.9145,406);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#333333").ss(1,1,1).p("EADgA/cQjyuuhxxpQhpwdAPxpQANwuB3v3QByvdDHsY");
	this.shape_8.setTransform(-21.4184,406);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#333333").ss(1,1,1).p("EADjA/cQj1uthyxqQhrwdAPxpQANwvB5v3QB0vdDJsX");
	this.shape_9.setTransform(-21.7172,406);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#333333").ss(1,1,1).p("EADkA/cQj2uthzxqQhswdAPxpQANwvB6v3QB1veDKsW");
	this.shape_10.setTransform(-21.8418,406);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#333333").ss(1,1,1).p("EADlA/cQj3ush0xrQhrwdAPxpQANwvB5v3QB2veDLsW");
	this.shape_11.setTransform(-21.8918,406);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#333333").ss(1,1,1).p("EADlA/cQj3ushzxrQhswdAPxpQANwvB6v3QB1veDLsW");
	this.shape_12.setTransform(-21.9165,406);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#333333").ss(1,1,1).p("EADlg/bQjLMWh1PeQh6P3gNQvQgPRpBsQdQBzRrD3Os");
	this.shape_13.setTransform(-21.9165,406);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#333333").ss(1,1,1).p("EADlA/cQj3ushzxrQhswdAPxpQANwvB5v3QB2veDLsW");
	this.shape_14.setTransform(-21.8915,406);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#333333").ss(1,1,1).p("EADlA/cQj3ushzxrQhswdAPxpQAOwvB5v3QB1veDLsW");
	this.shape_15.setTransform(-21.8665,406);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#333333").ss(1,1,1).p("EADkA/cQj2uthzxqQhrwdAPxpQANwvB5v3QB1veDKsW");
	this.shape_16.setTransform(-21.7865,406);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#333333").ss(1,1,1).p("EADjA/cQj0uthzxqQhqwdAOxpQAOwvB4v2QB0veDJsX");
	this.shape_17.setTransform(-21.6619,406);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#333333").ss(1,1,1).p("EADhA/cQjyuuhyxpQhpwdAOxpQANwuB4v3QBzvdDHsY");
	this.shape_18.setTransform(-21.4626,406);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#333333").ss(1,1,1).p("EADeA/cQjvuvhwxoQhoweAOxoQANwuB2v3QBxvcDFsZ");
	this.shape_19.setTransform(-21.2133,406);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#333333").ss(1,1,1).p("EADaA/cQjruxhuxnQhnweAPxmQAMwuB0v4QBwvaDBsb");
	this.shape_20.setTransform(-20.8645,406);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#333333").ss(1,1,1).p("EADWA/cQjnuzhrxlQhlweAOxmQANwsBxv5QBtvZC+sd");
	this.shape_21.setTransform(-20.4106,406);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#333333").ss(1,1,1).p("EADQA/cQjgu2hpxiQhiwfAOxlQAMwsBuv4QBrvXC4sg");
	this.shape_22.setTransform(-19.8817,406);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#333333").ss(1,1,1).p("EADJA/cQjZu4hlxgQhewgANxjQAMwrBqv6QBnvTCysk");
	this.shape_23.setTransform(-19.1786,406);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#333333").ss(1,1,1).p("EADAA/cQjPu8hhxcQhawhAMxhQAMwqBlv7QBjvQCqso");
	this.shape_24.setTransform(-18.3758,406);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#333333").ss(1,1,1).p("EAC2A/cQjEvAhcxZQhWwiAMxfQALwoBhv8QBdvMChst");
	this.shape_25.setTransform(-17.4235,406);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#333333").ss(1,1,1).p("EACqA/cQi3vFhWxUQhRwjAMxdQAKwnBav8QBXvICXsz");
	this.shape_26.setTransform(-16.2969,406);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#333333").ss(1,1,1).p("EACdA/cQipvLhPxPQhKwkAKxaQAKwlBTv+QBQvDCLs5");
	this.shape_27.setTransform(-15.0095,406);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#333333").ss(1,1,1).p("EACOA/cQiYvShIxIQhCwnAJxWQAIwiBLwAQBJu9B9tB");
	this.shape_28.setTransform(-13.5536,406);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#333333").ss(1,1,1).p("EAB8A/cQiFvZg/xCQg6woAIxSQAHwhBCwBQA/u2ButK");
	this.shape_29.setTransform(-11.8731,406);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#333333").ss(1,1,1).p("EABpA/cQhxvig1w5QgxwrAHxPQAGwdA3wDQA2uvBdtT");
	this.shape_30.setTransform(-10.0434,406);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#333333").ss(1,1,1).p("EABYA/cQhevpgswyQgqwuAGxKQAFwbAvwFQAsuoBOtc");
	this.shape_31.setTransform(-8.3629,406);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#333333").ss(1,1,1).p("EABIA/cQhNvwgkwsQgiwvAFxHQAEwYAmwHQAkujBAtj");
	this.shape_32.setTransform(-6.9071,406);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#333333").ss(1,1,1).p("EAA7A/cQg/v2gdwmQgcwxAExEQADwXAgwIQAdudA0tq");
	this.shape_33.setTransform(-5.6198,406);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#333333").ss(1,1,1).p("EAAvA/cQgyv7gYwhQgWwzADxBQADwVAZwJQAXuZAqtw");
	this.shape_34.setTransform(-4.493,406);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#333333").ss(1,1,1).p("EAAlA/cQgnv/gTweQgRwzACxAQACwTAUwKQASuVAht1");
	this.shape_35.setTransform(-3.5406,406);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#333333").ss(1,1,1).p("EAAcA/cQgdwDgPwaQgNw1ACw9QABwSAQwLQANuSAZt5");
	this.shape_36.setTransform(-2.7379,406);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#333333").ss(1,1,1).p("EAAVA/cQgWwGgLwXQgKw2ACw7QABwSALwMQAKuPATt8");
	this.shape_37.setTransform(-2.0348,406);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#333333").ss(1,1,1).p("EAAQA/cQgQwIgIwVQgHw3ABw6QABwRAIwMQAHuNAOt/");
	this.shape_38.setTransform(-1.5059,406);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#333333").ss(1,1,1).p("EAALA/cQgLwLgFwSQgFw3AAw6QABwQAGwMQAFuMAJuB");
	this.shape_39.setTransform(-1.0523,406);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#333333").ss(1,1,1).p("EAAIA/cQgIwMgDwSQgEw3ABw5QAAwPAEwNQADuKAHuD");
	this.shape_40.setTransform(-0.7031,406);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#333333").ss(1,1,1).p("EAAFA/cQgFwNgBwRQgDw3ABw5QAAwOACwOQACuJAEuE");
	this.shape_41.setTransform(-0.4542,406);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#333333").ss(1,1,1).p("EAADA/cQgDwOAAwQQgCw3ABw4QAAwOABwPQAAuIADuF");
	this.shape_42.setTransform(-0.2562,406);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#333333").ss(1,1,1).p("EAABA/cQgBwOAAwQQgBw4ABw3QAAwOAAwOQAAuIABuG");
	this.shape_43.setTransform(-0.1333,406);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#333333").ss(1,1,1).p("EAAAA/cQAAwPAAwPQAAw4AAw3QAAwOAAwOQAAuIAAuG");
	this.shape_44.setTransform(-0.05,406);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#333333").ss(1,1,1).p("EAAAA/cQAAwPAAwPQAAw4AAw3QAAwOAAwOQAAuHAAuH");
	this.shape_45.setTransform(-0.025,406);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45,p:{x:-0.025}}]},1).to({state:[{t:this.shape_45,p:{x:0}}]},1).to({state:[{t:this.shape}]},1).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-45.8,-1,47.8,814);


(lib.lettres_FAT = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CC9900").s().p("AkVDvIBehzQBxBjBfAAQAqAAAZgSQAZgTgBgfQAAgfgagTQgagThLgSQh6geg5gtQg4gvAAhjQAAhkBHg2QBIg2BpAAQBHAABFAZQBGAYA1ArIhPByQhchFhfAAQgoABgWASQgWASAAAfQAAAfAbARQAcATBiAYQBkAYA3AwQA3AygBBdQAABfhGA6QhHA7hygBQimAAiFh6g");
	this.shape.setTransform(45.6,311.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#CC9900").s().p("AjvFeIAAq7IHfAAIAACJIlDAAIAACXIEzAAIAACIIkzAAIAAETg");
	this.shape_1.setTransform(-68.35,187.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.lettres_FAT, new cjs.Rectangle(-102.9,0,195.2,373.7), null);


(lib.hastaggraphicbranche = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AjQAuIAShbIDlAAICqAAIgSBbg");
	this.shape.setTransform(0,4.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.hastaggraphicbranche, new cjs.Rectangle(-20.8,0,41.7,9.2), null);


(lib.cta1_back = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CC9900").s().p("Ao1EsIAApXIRrAAIAAJXg");
	this.shape.setTransform(56.6,30);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.cta1_back, new cjs.Rectangle(0,0,113.2,60), null);


(lib.Btn_cliquable_area = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CC9900").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape.setTransform(0,30);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.Btn_cliquable_area, new cjs.Rectangle(-113,0,226,60), null);


(lib.prject_illustration = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.project_illustration_placeHolder();
	this.instance.setTransform(0,316.5,1,1,0,0,0,0,316.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(120));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-313.5,-79.8,404.1,615.1999999999999);


(lib.masqueTexte = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// masqueTexte_1_ligne
	this.instance = new lib.masqueTexte_1_ligne();
	this.instance.setTransform(-218.8,191.5,0.0079,1,0,0,0,0,25.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(6).to({_off:false},0).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.0079,x:218.8},14).wait(1));

	// masqueTexte_1_ligne
	this.instance_1 = new lib.masqueTexte_1_ligne();
	this.instance_1.setTransform(-218.8,136.1,0.0079,1,0,0,0,0,25.5);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.0079,x:218.8},14).to({_off:true},1).wait(2));

	// masqueTexte_1_ligne
	this.instance_2 = new lib.masqueTexte_1_ligne();
	this.instance_2.setTransform(-218.8,80.8,0.0079,1,0,0,0,0,25.5);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(2).to({_off:false},0).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.0079,x:218.8},14).to({_off:true},1).wait(4));

	// masqueTexte_1_ligne
	this.instance_3 = new lib.masqueTexte_1_ligne();
	this.instance_3.setTransform(-218.8,25.5,0.0079,1,0,0,0,0,25.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.0079,x:218.8},14).to({_off:true},1).wait(6));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-220.5,-1.9,441.1,219);


(lib.lignes_all = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{close:114,close_lines:157});

	// timeline functions:
	this.frame_113 = function() {
		this.stop();
	}
	this.frame_156 = function() {
		this.stop();
	}
	this.frame_174 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(113).call(this.frame_113).wait(43).call(this.frame_156).wait(18).call(this.frame_174).wait(1));

	// ligne
	this.instance = new lib.ligne();
	this.instance.setTransform(-454.05,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(114).to({x:-883},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:-454.05},0).wait(18));

	// ligne
	this.instance_1 = new lib.ligne();
	this.instance_1.setTransform(-227.05,406.5,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(114).to({x:-883.2},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:-227.05},0).wait(18));

	// ligne
	this.instance_2 = new lib.ligne();
	this.instance_2.setTransform(-0.05,406.5,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(114).to({x:-883.4},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:-0.05},0).wait(18));

	// ligne
	this.instance_3 = new lib.ligne();
	this.instance_3.setTransform(226.95,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(93).to({x:227,y:322},20,cjs.Ease.quartInOut).wait(1).to({y:323},0).to({x:-883.05,y:406},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:227,y:322},0).to({x:226.95,y:406},16,cjs.Ease.quartInOut).wait(2));

	// ligne
	this.instance_4 = new lib.ligne();
	this.instance_4.setTransform(454.1,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(114).to({x:-883.45},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:454.1},0).wait(18));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-884.4,-85,1339.5,898.5);


(lib.hashtag_graphic = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(64));

	// hastag graphic branche
	this.instance = new lib.hastaggraphicbranche();
	this.instance.setTransform(37.35,51.9,1.0844,0.9952,0,-78.3276,102.1252,0.1,4.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({x:6.45,y:50.05},17,cjs.Ease.quartOut).to({_off:true},5).wait(3).to({_off:false},0).to({_off:true},4).wait(2).to({_off:false,x:26.45},0).to({_off:true},5).wait(28));

	// hastag graphic branche
	this.instance_1 = new lib.hastaggraphicbranche();
	this.instance_1.setTransform(-41.65,49.9,1.0844,0.9952,0,-78.3276,102.1252,0.1,4.6);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off:false},0).to({x:-7.4,y:49.8},18,cjs.Ease.quartOut).to({_off:true},4).wait(5).to({_off:false},0).to({_off:true},4).wait(31));

	// hastag graphic branche
	this.instance_2 = new lib.hastaggraphicbranche();
	this.instance_2.setTransform(2.7,21.7,1,1,0,0,0,0,4.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({x:-2.3,y:58.7},13,cjs.Ease.quartOut).to({_off:true},11).wait(3).to({_off:false,scaleX:2.3297,scaleY:1.0257,skewX:-12.8379,x:25.5},0).to({_off:true},4).wait(33));

	// hastag graphic branche
	this.instance_3 = new lib.hastaggraphicbranche();
	this.instance_3.setTransform(-7.3,77.7,1,1,0,0,0,0,4.6);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({x:0.7,y:41.7},14,cjs.Ease.quartOut).to({_off:true},4).wait(7).to({_off:false,regX:0.1,scaleX:1.9808,scaleY:1.0147,skewX:-9.7728,x:19.9,y:49.7},0).to({_off:true},4).wait(4).to({_off:false,regX:0,scaleX:1,scaleY:1,skewX:0,x:0.7,y:41.7},0).to({_off:true},4).wait(21));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-50.9,0,126.1,82.3);


(lib.cta0 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{rollOver:1,rollOut:16});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_15 = function() {
		this.stop();
	}
	this.frame_30 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(15).call(this.frame_15).wait(15).call(this.frame_30).wait(1));

	// Calque_5
	this.instance = new lib.cta1_back();
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({_off:false},0).to({alpha:1},14).wait(1).to({alpha:0},13).wait(2));

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.008)").s().p("Ao0EsIAApXIRpAAIAAJXg");
	this.shape.setTransform(56.5,30);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(31));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,113.2,60);


(lib.Btn_next = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"rollOver":55,"rollOut":70});

	// timeline functions:
	this.frame_54 = function() {
		this.stop();
	}
	this.frame_69 = function() {
		this.stop();
	}
	this.frame_84 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(54).call(this.frame_54).wait(15).call(this.frame_69).wait(15).call(this.frame_84).wait(1));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgeAqIAAhTIA8AAIAAARIgpAAIAAARIAkAAIAAAPIgkAAIAAASIAqAAIAAAQg");
	this.shape.setTransform(233.675,29.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AggAqIAAhTIAdAAQATAAAIAHQAJAIAAAQQAAAOgJAHQgJAIgSAAIgLAAIAAAXgAgOACIANAAQAIAAAEgCQADgEAAgIQAAgGgEgEQgFgDgIAAIgLAAg");
	this.shape_1.setTransform(217.825,29.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgIAqIAAhTIARAAIAABTg");
	this.shape_2.setTransform(203.475,29.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgYAgQgLgKAAgRIAAguIATAAIAAAtQAAAKAEAGQAFAGAHgBQAIABAFgGQAEgGAAgKIAAgtIATAAIAAAuQAAASgKAJQgKALgQgBQgPABgJgLg");
	this.shape_3.setTransform(189,29.15);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAHAvQgGgEgDgHQgTAAgNgNQgMgMAAgRQAAgTAMgNQANgMATAAQARAAANAMQANANAAATQAAAMgHAKQgGALgMAEQABABAAAAQAAABABAAQAAABABAAQAAAAABAAIAFACIAEgBIAEgCIAFgEIAJALQgKALgPAAQgIAAgHgEgAgUgZQgHAHAAAMQAAAKAHAIQAHAHALABQAJgBAIgHQAHgIAAgKQAAgMgHgHQgIgJgJABQgLgBgHAJg");
	this.shape_4.setTransform(171.875,29.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgeA5IAAhTIA8AAIAAARIgpAAIAAAQIAkAAIAAAQIgkAAIAAARIAqAAIAAARgAgKgjIAOgVIATAIIgRANg");
	this.shape_5.setTransform(154.875,27.625);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgGAQIAAgfIANAAIAAAfg");
	this.shape_6.setTransform(141.225,26.525);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgaAqIAAhTIASAAIAABDIAjAAIAAAQg");
	this.shape_7.setTransform(128.625,29.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AAOAqIgSgbIgNAAIAAAbIgTAAIAAhTIAgAAQASAAAJAHQAJAGAAAPQAAATgQAGIAUAegAgRAAIAOAAQAIAAAEgDQADgDAAgHQAAgHgEgDQgDgCgIAAIgOAAg");
	this.shape_8.setTransform(101.75,29.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgIAqIAAhTIARAAIAABTg");
	this.shape_9.setTransform(87.075,29.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AANAqIgRgbIgNAAIAAAbIgSAAIAAhTIAfAAQASAAAJAHQAIAGAAAPQABATgRAGIAWAegAgRAAIANAAQAKAAADgDQADgDAAgHQAAgHgDgDQgEgCgIAAIgOAAg");
	this.shape_10.setTransform(73.1,29.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgHAqIgihTIAVAAIAUA0IAVg0IAVAAIgiBTg");
	this.shape_11.setTransform(56.275,29.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgZAgQgKgKAAgRIAAguIATAAIAAAtQAAAKAFAGQAEAGAHgBQAIABAEgGQAGgGgBgKIAAgtIATAAIAAAuQAAASgKAJQgKALgQgBQgPABgKgLg");
	this.shape_12.setTransform(39.65,29.15);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgeAfQgNgNAAgSQAAgSANgMQAMgNASAAQATAAANANQAMAMAAASQAAASgMANQgNAMgTAAQgSAAgMgMgAgRgSQgHAIAAAKQAAALAHAIQAIAIAJAAQAKAAAIgIQAHgIAAgLQAAgKgHgIQgIgIgKAAQgJAAgIAIg");
	this.shape_13.setTransform(22.2,29.05);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgaAfQgMgMAAgTQAAgRAMgNQANgNASAAQAUAAAOAQIgMAOQgIgMgOAAQgJABgIAGQgHAIAAAKQAAAMAHAHQAHAHAJAAQAOAAAJgLIAMAMQgPARgTgBQgSAAgNgMg");
	this.shape_14.setTransform(4.925,29.05);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgeA5IAAhTIA8AAIAAARIgpAAIAAAQIAkAAIAAAQIgkAAIAAARIAqAAIAAARgAgKgjIAOgVIATAIIgRANg");
	this.shape_15.setTransform(-11.275,27.625);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AglAqIAAhTIAeAAQAVAAAMALQAMALAAATQAAATgMALQgMAMgWAAgAgTAaIANAAQAMAAAGgHQAHgGAAgNQAAgLgHgHQgGgGgOgBIgLAAg");
	this.shape_16.setTransform(-27.8,29.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#333333").ss(1,1,1).p("EgjOAADMBGdgAF");
	this.shape_17.setTransform(113,-0.25);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(85));

	// Layer_2
	this.instance = new lib.Btn_cliquable_area();
	this.instance.setTransform(113,29.5,2,1,0,0,0,0,30);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({alpha:0},46).wait(6).to({alpha:1},14).wait(1).to({alpha:0},13).to({_off:true},1).wait(1));

	// Layer_3
	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("rgba(0,0,0,0.098)").s().p("EgjTAEsIAApXMBGnAAAIAAJXg");
	this.shape_18.setTransform(113,29.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_18).wait(85));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-113.5,-1.5,453,61);


(lib.Btn_detail = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_54 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(54).call(this.frame_54).wait(1));

	// lines
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#333333").ss(1,1,1).p("AAAkoIDSAAIOYAAAAAEtIAApVAxpkoIRpAAADSkoIAAgE");
	this.shape.setTransform(0,29.7);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(55));

	// Isolation Mode
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#FFFFFF").ss(2,1,1).p("ABYgmIhYBNIhXhN");
	this.shape_1.setTransform(59.2,26.95);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhYhLICxAAIAACXIixAAg");
	this.shape_2.setTransform(58.975,30.65);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AhJBZQgJAAgDgDQgDgEAAgIIAAiTQAAgIADgEQAEgDAIAAICUAAQAIAAADADQADAEAAAIIAACTQAAAIgDAEQgDADgJAAgAgUBAIAbAAIAAgMIACgoQAAgGAEgEQADgDAGAAQALgBABAOIACAzIAcAAIAAg+QgCgZgcgCQgRAAgLAMIgBgJIgZAAgAg+A/IAaAAIAAhWIgaAAgAg9g+QgFAFgBAHQAAAIAFAFQAFAGAIAAQAHAAAFgFQAFgFABgHQAAgIgFgFQgFgGgIAAQgHAAgFAFg");
	this.shape_3.setTransform(-57.475,30.425);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(55));

	// zones
	this.cta1 = new lib.cta0();
	this.cta1.name = "cta1";
	this.cta1.setTransform(56.5,29.5,1,1,0,0,0,56.5,30);

	this.timeline.addTween(cjs.Tween.get(this.cta1).wait(55));

	// zones
	this.cta0 = new lib.cta0();
	this.cta0.name = "cta0";
	this.cta0.setTransform(-56.5,29.5,1,1,0,0,0,56.5,30);

	this.timeline.addTween(cjs.Tween.get(this.cta0).wait(55));

	// Layer_2
	this.instance = new lib.Btn_cliquable_area();
	this.instance.setTransform(0,29.5,1,1,0,0,0,0,30);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({alpha:0.0117},46).wait(6));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-114,-1.4,228,62.199999999999996);


(lib.bloc_visuel = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":63});

	// timeline functions:
	this.frame_62 = function() {
		this.stop();
	}
	this.frame_99 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(62).call(this.frame_62).wait(37).call(this.frame_99).wait(1));

	// masque VISUEL (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_20 = new cjs.Graphics().p("EgjsBDvMAAAiHdMBHZAAAMAAACHdg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(20).to({graphics:mask_graphics_20,x:-228.75,y:-19}).wait(80));

	// VISUEL
	this.instance = new lib.prject_illustration();
	this.instance.setTransform(552.5,26.55,1,1,5.9999,0,0,0.1,316.5);
	this.instance.alpha = 0.0117;

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:-0.1,regY:316.6,rotation:-3.0012,x:-229.45,y:6.6,alpha:1},20,cjs.Ease.cubicOut).to({rotation:0,x:-39.55},28,cjs.Ease.cubicInOut).wait(20).to({regX:0,rotation:-12.71,x:-39.5},0).to({x:-719.5},30,cjs.Ease.quartInOut).to({_off:true},1).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-532.5,-400.3,1216.5,689.4000000000001);


(lib.bloc_video = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":41});

	// timeline functions:
	this.frame_40 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(40).call(this.frame_40).wait(17));

	// masque video (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_42 = new cjs.Graphics().p("Ega8A8AMAAAh3/MA7rAAAMAAAB3/g");
	var mask_graphics_43 = new cjs.Graphics().p("Ega8A8AMAAAh3/MA7qAAAMAAAB3/g");
	var mask_graphics_44 = new cjs.Graphics().p("Ega0A8AMAAAh3/MA7aAAAMAAAB3/g");
	var mask_graphics_45 = new cjs.Graphics().p("EgaSA8AMAAAh3/MA6VAAAMAAAB3/g");
	var mask_graphics_46 = new cjs.Graphics().p("EgY1A8AMAAAh3/MA3bAAAMAAAB3/g");
	var mask_graphics_47 = new cjs.Graphics().p("EgVyA8AMAAAh3/MAxSAAAMAAAB3/g");
	var mask_graphics_48 = new cjs.Graphics().p("EgQQA8AMAAAh3/MAmIAAAMAAAB3/g");
	var mask_graphics_49 = new cjs.Graphics().p("EgIMA8AMAAAh3/IV3AAMAAAB3/g");
	var mask_graphics_50 = new cjs.Graphics().p("EgCpA8AMAAAh3/IKsAAMAAAB3/g");
	var mask_graphics_51 = new cjs.Graphics().p("EAAZA8AMAAAh3/IEkAAMAAAB3/g");
	var mask_graphics_52 = new cjs.Graphics().p("EAB1A8AMAAAh3/IBqAAMAAAB3/g");
	var mask_graphics_53 = new cjs.Graphics().p("EACXA8AMAAAh3/IAlAAMAAAB3/g");
	var mask_graphics_54 = new cjs.Graphics().p("EACfA8AMAAAh3/IAVAAMAAAB3/g");
	var mask_graphics_55 = new cjs.Graphics().p("EACgA8AMAAAh3/IAUAAMAAAB3/g");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(42).to({graphics:mask_graphics_42,x:209.4828,y:0}).wait(1).to({graphics:mask_graphics_43,x:209.4292,y:0}).wait(1).to({graphics:mask_graphics_44,x:208.6245,y:0}).wait(1).to({graphics:mask_graphics_45,x:205.1378,y:0}).wait(1).to({graphics:mask_graphics_46,x:195.7504,y:0}).wait(1).to({graphics:mask_graphics_47,x:175.9563,y:0}).wait(1).to({graphics:mask_graphics_48,x:139.9623,y:0}).wait(1).to({graphics:mask_graphics_49,x:87.4933,y:0}).wait(1).to({graphics:mask_graphics_50,x:51.4993,y:0}).wait(1).to({graphics:mask_graphics_51,x:31.7053,y:0}).wait(1).to({graphics:mask_graphics_52,x:22.3178,y:0}).wait(1).to({graphics:mask_graphics_53,x:18.8311,y:0}).wait(1).to({graphics:mask_graphics_54,x:18.0265,y:0}).wait(1).to({graphics:mask_graphics_55,x:17.973,y:0}).wait(1).to({graphics:null,x:0,y:0}).wait(1));

	// VIDEO
	this.videoContainer = new lib.video();
	this.videoContainer.name = "videoContainer";
	this.videoContainer.setTransform(36.7,765,0.6587,0.6587,0,0,0,0.3,480.2);

	var maskedShapeInstanceList = [this.videoContainer];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.videoContainer).to({y:-33},40,cjs.Ease.quartInOut).to({_off:true},16).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-349.3,418.6,1430.3999999999999);


(lib.bloc_NEXT_btn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":17});

	// timeline functions:
	this.frame_16 = function() {
		this.stop();
	}
	this.frame_33 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(16).call(this.frame_16).wait(17).call(this.frame_33).wait(1));

	// CTA DETAIL
	this.btnNext = new lib.Btn_next();
	this.btnNext.name = "btnNext";
	this.btnNext.setTransform(-340.5,411.5,1,1,0,0,0,0,26.5);

	this.timeline.addTween(cjs.Tween.get(this.btnNext).to({y:350.5},16,cjs.Ease.quartInOut).wait(2).to({y:411.5},14,cjs.Ease.quartInOut).to({_off:true},1).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-454,0,453,444.5);


(lib.bloc_hashtag = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_61 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(61).call(this.frame_61).wait(2));

	// Layer_1
	this.instance = new lib.hashtag_graphic("synched",0);
	this.instance.setTransform(-456.05,243.2,2.3169,2.3169,0,0,0,-0.1,50.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true},61).wait(2));

	// Layer_5 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_28 = new cjs.Graphics().p("A2QBpIAAjRMAshAAAIAADRg");
	var mask_graphics_32 = new cjs.Graphics().p("A1ZDSIAAmjMAqzAAAIAAGjg");
	var mask_graphics_40 = new cjs.Graphics().p("A4NExIAAphMAwbAAAIAAJhg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(28).to({graphics:mask_graphics_28,x:-343.525,y:265.45}).wait(4).to({graphics:mask_graphics_32,x:-348.975,y:255.025}).wait(8).to({graphics:mask_graphics_40,x:-330.975,y:245.55}).wait(23));

	// Layer_3
	this.instance_1 = new lib.txt_hashtag();
	this.instance_1.setTransform(-421.5,244.05,1,1,0,0,0,0,31.6);
	this.instance_1._off = true;

	var maskedShapeInstanceList = [this.instance_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(28).to({_off:false},0).to({_off:true},34).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-573.7,0,311.30000000000007,317.1);


(lib.bloc_CTA = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":17});

	// timeline functions:
	this.frame_16 = function() {
		this.stop();
	}
	this.frame_33 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(16).call(this.frame_16).wait(17).call(this.frame_33).wait(1));

	// CTA DETAIL
	this.ctas = new lib.Btn_detail();
	this.ctas.name = "ctas";
	this.ctas.setTransform(-340.5,411.5,1,1,0,0,0,0,26.5);

	this.timeline.addTween(cjs.Tween.get(this.ctas).to({y:350.5},16,cjs.Ease.quartInOut).wait(2).to({y:411.5},14,cjs.Ease.quartInOut).to({_off:true},1).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-454.5,0,228,445.8);


(lib.blocInfostextes = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":80});

	// timeline functions:
	this.frame_79 = function() {
		this.stop();
	}
	this.frame_120 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(79).call(this.frame_79).wait(41).call(this.frame_120).wait(1));

	// Bloc Hashtag
	this.instance = new lib.bloc_hashtag();
	this.instance.setTransform(-391.5,211.2,0.9,0.9,0,0,0,-412.9,244.7);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(62).to({_off:false},0).to({_off:true},18).wait(41));

	// masque Titre
	this.instance_1 = new lib.masque_generique();
	this.instance_1.setTransform(-527.95,-13.9,0.0075,0.4711,0,0,0,0,17.2);
	this.instance_1._off = true;
	this.instance_1.filters = [new cjs.ColorFilter(0, 0, 0, 1, 204, 153, 0, 0)];
	this.instance_1.cache(-2,-2,161,38);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(34).to({_off:false},0).to({regX:0.5,scaleX:1.9223,x:-527.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.0128,x:-228.9},10,cjs.Ease.quartInOut).to({_off:true},1).wait(26).to({_off:false},0).to({regX:0.5,scaleX:1.9223,x:-527.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.0074,x:-528.7},10,cjs.Ease.quartInOut).to({_off:true},1).wait(21));

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CC9900").s().p("AguA4IAAgRIA6hIIg4AAIAAgWIBaAAIAAARIg6BIIA7AAIAAAWg");
	this.shape.setTransform(-307.975,-14.15);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#CC9900").s().p("AgoA4IAAhvIBPAAIAAAXIg2AAIAAAWIAxAAIAAAUIgxAAIAAAYIA4AAIAAAWg");
	this.shape_1.setTransform(-330.35,-14.15);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#CC9900").s().p("AASA4IgYgjIgRAAIAAAjIgZAAIAAhvIAqAAQAaAAALAJQALAJAAATQAAAagWAIIAdAogAgXgBIASAAQAMAAAFgDQAEgFAAgJQAAgIgFgEQgEgEgLAAIgTAAg");
	this.shape_2.setTransform(-353.225,-14.15);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#CC9900").s().p("AgMA4IAAhZIgfAAIAAgWIBXAAIAAAWIggAAIAABZg");
	this.shape_3.setTransform(-376.375,-14.15);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CC9900").s().p("AAbA4Ig0hFIAABFIgaAAIAAhvIAYAAIA2BHIAAhHIAZAAIAABvg");
	this.shape_4.setTransform(-399.75,-14.15);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#CC9900").s().p("AgpApQgRgQAAgZQAAgXARgRQARgRAYAAQAZAAARARQARARAAAXQAAAZgRAQQgRARgZAAQgYAAgRgRgAgXgYQgJAKAAAOQAAAPAJALQAKAKANAAQAOAAAKgKQAJgLAAgPQAAgOgJgKQgKgLgOAAQgNAAgKALg");
	this.shape_5.setTransform(-424.875,-14.225);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#CC9900").s().p("AgiApQgRgQgBgYQABgZARgQQAQgRAZAAQAbAAATAVIgQASQgMgPgRAAQgNAAgLAJQgKAJABAPQAAAPAJAKQAJAKAMAAQATAAAMgPIAQARQgUAVgZAAQgZAAgQgRg");
	this.shape_6.setTransform(-449.25,-14.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#CC9900").s().p("AAaA4Ig0hFIAABFIgZAAIAAhvIAXAAIA2BHIAAhHIAaAAIAABvg");
	this.shape_7.setTransform(-473.7,-14.15);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#CC9900").s().p("AgoA4IAAhvIBPAAIAAAXIg2AAIAAAWIAxAAIAAAUIgxAAIAAAYIA4AAIAAAWg");
	this.shape_8.setTransform(-497.2,-14.15);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#CC9900").s().p("AASA4IgYgjIgRAAIAAAjIgZAAIAAhvIAqAAQAaAAALAJQALAJAAATQAAAagWAIIAdAogAgXgBIASAAQAMAAAFgDQAEgFAAgJQAAgIgFgEQgEgEgLAAIgTAAg");
	this.shape_9.setTransform(-520.075,-14.15);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},34).to({state:[]},55).to({state:[]},1).wait(31));

	// masque Titre (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_17 = new cjs.Graphics().p("EgpOArJMAAAhWRMAvJAAAMAAABWRg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(17).to({graphics:mask_graphics_17,x:-263.875,y:-6.1095}).wait(24).to({graphics:null,x:0,y:0}).wait(80));

	// Layer_5
	this.instance_2 = new lib.lettres_FAT();
	this.instance_2.setTransform(-433.85,-73.05,1,1,0,0,0,0,186.8);
	this.instance_2._off = true;

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#CC9900").s().p("AjiFdIAAq5ICcAAIAAIvIEpAAIAACKg");
	this.shape_10.setTransform(-234.7,131.75);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#CC9900").s().p("AjiFeIAAq6ICcAAIAAIuIEpAAIAACMg");
	this.shape_11.setTransform(-465.1,7.85);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#CC9900").s().p("AjWEPQhVhSAAiXIAAmGICdAAIAAGBQAABUAnAxQAnAwBBAAQBBAAAngwQAngxgBhUIAAmBICdAAIAAGGQAACYhVBSQhUBRiCAAQiDAAhUhSg");
	this.shape_12.setTransform(-385.65,-115.65);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#CC9900").s().p("AjvFdIAAq5IHfAAIAACIIlDAAIAACYIEzAAIAACHIkzAAIAAESg");
	this.shape_13.setTransform(-502.2,-116);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#CC9900").s().p("AhNFdIAAkTIjymmICpAAICWEEICXkEICpAAIjyGmIAAETg");
	this.shape_14.setTransform(-333.3,106.75);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#CC9900").s().p("AByFeIibjgIhsAAIAADgIicAAIAAq7IEJAAQCjAABFA3QBFA3AAB7QAACmiFAxICyD7gAiVgIIByAAQBQAAAcgaQAdgZAAg5QAAg4gegWQgdgUhLgBIh1AAg");
	this.shape_15.setTransform(-230.45,-17.15);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#CC9900").s().p("AkEECQhqhoAAiaQAAiZBqhoQBqhnCaAAQCbAABqBnQBqBoAACZQAACahqBoQhqBnibAAQiaAAhqhngAiSieQg8BCAABcQAABeA8BCQA8BBBWAAQBYAAA8hBQA8hCAAheQAAhcg8hCQg8hChYAAQhWAAg8BCg");
	this.shape_16.setTransform(-476.5,-17.6);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#CC9900").s().p("AhNFeIAAo0IjGAAIAAiHIInAAIAACHIjGAAIAAI0g");
	this.shape_17.setTransform(-334.8,-141);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#CC9900").s().p("AkUDvIBchzQBxBkBfgBQArAAAZgSQAYgTAAgfQABgfgagTQgagThNgSQh6geg4gtQg4gugBhkQAAhkBIg2QBIg2BqAAQBFAABGAZQBHAYA0ArIhQByQhbhFhgAAQgmABgXASQgXASABAfQgBAfAcARQAbATBjAYQBjAYA3AwQA3AyAABdQAABehGA7QhHA7hyAAQilAAiFh7g");
	this.shape_18.setTransform(-486.45,-141.4);

	var maskedShapeInstanceList = [this.instance_2,this.shape_10,this.shape_11,this.shape_12,this.shape_13,this.shape_14,this.shape_15,this.shape_16,this.shape_17,this.shape_18];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_2}]},17).to({state:[{t:this.instance_2}]},14).to({state:[{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10}]},1).to({state:[{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14}]},5).to({state:[]},4).to({state:[]},39).wait(41));
	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(17).to({_off:false},0).to({x:-276.85},14,cjs.Ease.quartOut).to({_off:true},1).wait(89));

	// masque Titre
	this.instance_3 = new lib.masque_generique();
	this.instance_3.setTransform(221.1,-163.9,0.0128,0.4711,0,0,0,0,17.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({regX:0.5,scaleX:1.9223,x:-75.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.0074,x:-68.7},10,cjs.Ease.quartInOut).to({_off:true},1).wait(101));

	// Layer_7
	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#FFFFFF").ss(1,1,1).p("EAAAAlzMgnugc4MAPLgutMAxHAAAMAPLAutg");
	this.shape_19.setTransform(230.675,-128.075);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#FFFFFF").ss(1,1,1).p("ANsHwI7XFfII86dg");
	this.shape_20.setTransform(47,-93.65);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AkvtNISbU8I7XFfg");
	this.shape_21.setTransform(47,-93.65);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#FFFFFF").p("EAm2AQPIhxjoI5ipDQh7grh7hEQh7hDhnhQQCUgrBnhTQB7hiA1iUQAghcgFhhQgGhdgphVQgohVhFg+QhIhChcghQhcgghgAGQheAGhVAoQhTApg/BFQhCBHggBcQg0CUAhCaQAdCBBYB/QiEgCiJgZQiKgZh6grI5Go4IjrBuIb0J1QCrA9C+AbQDIAbCzgPQCCB8CuBoQClBjCrA9gAIKnPQgtCBiCBLQhjA6iOAXQhghrgohrQg2iNAtiAQAVg5AngtQAngqA1gaQA1gZA5gDQA9gEA5AUQA5AUAsApQArAnAZA1QAZA1AEA6QAEA7gVA5g");
	this.shape_22.setTransform(-158.95,-13.1,1,1,0,0,0,-0.3,-0.2);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#FFFFFF").ss(1,1,1).p("AW3ISI4mONI1HzBILk58IcQC+g");
	this.shape_23.setTransform(-22.675,68.825);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#FFFFFF").ss(1,1,1).p("AnFmSICWm8ISbU+I7XFfIAAgB");
	this.shape_24.setTransform(-240,-49.65);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AqGJxIGnzhIAAAAINmPeI0MEDg");
	this.shape_25.setTransform(-262.95,-27.55);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#FFFFFF").ss(1,1,1).p("ALnNZI3NtWIXJtbg");
	this.shape_26.setTransform(-577,114.55);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_19}]},17).to({state:[{t:this.shape_21},{t:this.shape_20}]},4).to({state:[{t:this.shape_22}]},3).to({state:[]},6).to({state:[{t:this.shape_23}]},63).to({state:[{t:this.shape_25},{t:this.shape_24}]},4).to({state:[{t:this.shape_26}]},3).to({state:[]},6).to({state:[]},1).wait(14));

	// masque Titre
	this.instance_4 = new lib.masque_generique();
	this.instance_4.setTransform(-228.9,-13.9,0.0128,0.4711,0,0,0,0,17.2);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(4).to({_off:false},0).to({regX:0.5,scaleX:1.9223,x:-527.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.0074,x:-528.7},10,cjs.Ease.quartInOut).to({_off:true},1).wait(97));

	// Layer_10
	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#CC9900").s().p("AgLA4IAAgsIgnhDIAbAAIAXAqIAYgqIAbAAIgmBDIAAAsg");
	this.shape_27.setTransform(-450.375,-14.15);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#CC9900").s().p("AgoA4IAAhvIBQAAIAAAXIg3AAIAAAWIAxAAIAAAUIgxAAIAAAYIA4AAIAAAWg");
	this.shape_28.setTransform(-504.65,-14.15);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#CC9900").s().p("AgJA0QgEgEAAgGQAAgFAEgFQAEgEAFAAQAGAAAEAEQAEAFAAAFQAAAGgEAEQgEAFgGAAQgFAAgEgFgAgJAVIgEg5IAAgUIAbAAIAAAUIgFA5g");
	this.shape_29.setTransform(-349.875,-14.075);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#CC9900").s().p("AgpApQgRgQAAgZQAAgXARgRQARgRAYAAQAZAAARARQARARAAAXQAAAZgRAQQgRARgZAAQgYAAgRgRgAgXgYQgJAKAAAOQAAAPAJALQAKAKANAAQAOAAAKgKQAJgLAAgPQAAgOgJgKQgKgLgOAAQgNAAgKALg");
	this.shape_30.setTransform(-387.175,-14.225);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#CC9900").s().p("AgjA4IAAhvIAZAAIAABYIAuAAIAAAXg");
	this.shape_31.setTransform(-442.275,-14.15);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#CC9900").s().p("AAWA4IAAgsIgrAAIAAAsIgZAAIAAhvIAZAAIAAAvIArAAIAAgvIAZAAIAABvg");
	this.shape_32.setTransform(-487.85,-14.15);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#CC9900").s().p("AgjA4IAAhvIAZAAIAABYIAuAAIAAAXg");
	this.shape_33.setTransform(-474.675,-14.15);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_28,p:{x:-504.65}},{t:this.shape_27}]},13).to({state:[{t:this.shape_32,p:{x:-487.85}},{t:this.shape_28,p:{x:-464.25}},{t:this.shape_31,p:{x:-442.275}},{t:this.shape_30,p:{x:-387.175}},{t:this.shape_29,p:{x:-349.875}}]},5).to({state:[{t:this.shape_32,p:{x:-520.25}},{t:this.shape_28,p:{x:-496.65}},{t:this.shape_33},{t:this.shape_31,p:{x:-453.575}},{t:this.shape_30,p:{x:-430.875}},{t:this.shape_29,p:{x:-393.575}}]},5).to({state:[]},11).to({state:[]},46).wait(41));

	// Layer_11
	this.instance_5 = new lib.masqueTexte("synched",0);
	this.instance_5.setTransform(-340,105.8,0.85,0.85,0,0,0,0.1,108.8);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(37).to({_off:false},0).to({_off:true},40).wait(3).to({_off:false,rotation:180},0).to({_off:true},40).wait(1));

	// Layer_12
	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAggeQAhgeAsAAQAsAAAfAbQAfAbAAAuIAAAfIicAAQADASAPALQAQALATAAQAfAAAVgVIAhAlQgkAigxAAQgwAAgggegAgdgyQgPALgDATIBeAAQgCgUgNgKQgMgKgRAAQgSAAgOAKg");
	this.shape_34.setTransform(-238.15,133.425);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AhEBQQgigeAAgyQAAgwAigfQAigfAtAAQAXAAAYAKQAYAJASATIgfApQgLgNgPgGQgOgHgPAAQgXAAgRAPQgRAQAAAaQAAAbARAQQARAPAWAAQAeAAAYgeIAkAoQgqArgwAAQgwAAghgfg");
	this.shape_35.setTransform(-262.025,133.425);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AgdCXIAAjWIA7AAIAADWgAgYhaQgKgKAAgPQAAgPAKgKQAKgKAOAAQAPAAAKAKQAKAKAAAPQAAAPgKAKQgKAKgPAAQgOAAgKgKg");
	this.shape_36.setTransform(-279.725,129.075);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AgdBsIhWjXIA+AAIA1CHIA2iHIA+AAIhVDXg");
	this.shape_37.setTransform(-297.3,133.425);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("Ag9BtIAAjWIA8AAIAAAdQAKgOASgJQARgJASAAIAAA4IgLAAQgbAAgNATQgMATAAAeIAABdg");
	this.shape_38.setTransform(-316.375,133.275);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAhgeQAggeArAAQAtAAAfAbQAfAbAAAuIAAAfIicAAQADASAQALQAPALATAAQAfAAAVgVIAhAlQgkAigxAAQgwAAgggegAgdgyQgPALgDATIBeAAQgCgUgMgKQgNgKgRAAQgRAAgPAKg");
	this.shape_39.setTransform(-337.7,133.425);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("AgqBmQgagIgVgRIAagmQAjAaAkAAQAMAAAGgEQAHgFAAgHQAAgIgJgGQgKgHgQgFIgcgKQgJgEgLgIQgZgOAAgeQAAgdAYgTQAYgTAlAAQAmAAAmAaIgWAoQgcgUgdAAQgaAAAAAQQAAAIAJAFQAIAEAUAHIAdAKQAJADALAHQAXANAAAgQAAAggXATQgYATgkAAQgXAAgagJg");
	this.shape_40.setTransform(-360.9,133.425);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAhgeQAggeArAAQAtAAAfAbQAfAbAAAuIAAAfIicAAQADASAQALQAPALATAAQAfAAAVgVIAhAlQgkAigxAAQgwAAgggegAgdgyQgPALgCATIBdAAQgCgUgMgKQgNgKgSAAQgRAAgOAKg");
	this.shape_41.setTransform(-393.7,133.425);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("Ag9BtIAAjWIA8AAIAAAdQAKgOASgJQARgJASAAIAAA4IgLAAQgbAAgNATQgMATAAAeIAABdg");
	this.shape_42.setTransform(-413.525,133.275);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("AgfB5QgWgUAAgkIAAhiIgZAAIAAgrIAZAAIAAhAIA7AAIAABAIA1AAIAAArIg1AAIAABgQABAMAGAHQAHAHAIAAQARAAAMgPIAWAqQgcAZgfAAQgeAAgVgUg");
	this.shape_43.setTransform(-430.85,130.375);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFFFFF").s().p("AhQBPQghggAAgvQAAguAhggQAgggAwAAQAxAAAgAgQAhAgAAAuQAAAvghAgQggAggxAAQgwAAgggggAglgqQgPARAAAZQAAAbAPAQQAPARAWAAQAXAAAPgRQAPgQAAgbQAAgZgPgRQgPgRgXAAQgWAAgPARg");
	this.shape_44.setTransform(-453.175,133.425);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFFFFF").s().p("AgeBsIhVjXIA/AAIA0CHIA2iHIA+AAIhVDXg");
	this.shape_45.setTransform(-477.9,133.425);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FFFFFF").s().p("AhJCMQgXgUAAghQAAghAYgQQAYgRAqAAIArAAIAAgBQAAghgkAAQgPAAgRAGQgSAGgLAIIgbgmQAqgeA2AAQAmAAAaATQAYAUAAApIAACJIg4AAIAAgaQgYAdggAAQgjAAgXgTgAgkBWQAAAMAIAGQAJAGAQAAQAPAAAMgKQANgJAAgRIAAgKIglAAQgkAAAAAWgAgFhXIg4gtIA9gaIAzBHg");
	this.shape_46.setTransform(-512.45,128.575);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFFFFF").s().p("AgqBmQgagIgVgRIAagmQAjAaAkAAQAMAAAGgEQAIgFAAgHQAAgIgKgGQgJgHgRgFIgbgKQgKgEgMgIQgYgOAAgeQAAgdAYgTQAYgTAlAAQAmAAAmAaIgXAoQgcgUgbAAQgbAAAAAQQAAAIAJAFQAIAEAUAHIAdAKQAIADANAHQAWANAAAgQAAAggYATQgXATgkAAQgXAAgagJg");
	this.shape_47.setTransform(-246.55,84.675);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FFFFFF").s().p("AhLCBQgggeAAgzQAAgyAhgeQAggeArABQAtgBAfAbQAfAbAAAuIAAAfIicAAQADATAQALQAPAKATABQAfgBAVgUIAhAkQgkAigxAAQgwAAgggegAgdgCQgPALgDASIBeAAQgCgTgMgKQgNgKgRAAQgRAAgPAKgAglhXIAzhHIA+AaIg4Atg");
	this.shape_48.setTransform(-268.85,79.85);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FFFFFF").s().p("AApBtIAAh4QAAgyglAAQgRAAgNANQgNANAAAZIAAB3Ig9AAIAAjWIA9AAIAAAYQAagbAiAAQAhAAAXAYQAYAYAAAnIAACCg");
	this.shape_49.setTransform(-293.95,84.525);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FFFFFF").s().p("AAoBtIAAh4QAAgygkAAQgRAAgNANQgOANAAAZIAAB3Ig8AAIAAjWIA8AAIAAAYQAbgbAiAAQAhAAAXAYQAYAYAAAnIAACCg");
	this.shape_50.setTransform(-319.5,84.525);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FFFFFF").s().p("AhQBPQghggAAgvQAAguAhggQAgggAwAAQAxAAAgAgQAhAgAAAuQAAAvghAgQggAggxAAQgwAAgggggAglgqQgPARAAAZQAAAbAPAQQAPARAWAAQAXAAAPgRQAPgQAAgbQAAgZgPgRQgPgRgXAAQgWAAgPARg");
	this.shape_51.setTransform(-345.375,84.675);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FFFFFF").s().p("AgdCXIAAjWIA7AAIAADWgAgYhaQgKgKAAgPQAAgPAKgKQAKgKAOAAQAPAAAKAKQAKAKAAAPQAAAPgKAKQgKAKgPAAQgOAAgKgKg");
	this.shape_52.setTransform(-364.175,80.325);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FFFFFF").s().p("AgqBmQgagIgVgRIAbgmQAhAaAmAAQALAAAHgEQAGgFABgHQAAgIgKgGQgKgHgQgFIgbgKQgKgEgMgIQgYgOAAgeQAAgdAYgTQAYgTAlAAQAmAAAmAaIgXAoQgbgUgcAAQgbAAAAAQQAAAIAJAFQAJAEATAHIAdAKQAJADALAHQAXANAAAgQAAAggYATQgXATgkAAQgXAAgagJg");
	this.shape_53.setTransform(-380.75,84.675);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FFFFFF").s().p("AgqBmQgagIgVgRIAbgmQAhAaAmAAQALAAAHgEQAGgFABgHQAAgIgKgGQgKgHgQgFIgcgKQgJgEgMgIQgYgOAAgeQAAgdAYgTQAYgTAlAAQAmAAAmAaIgWAoQgcgUgcAAQgbAAAAAQQAAAIAJAFQAJAEATAHIAdAKQAIADAMAHQAXANAAAgQAAAggYATQgXATgkAAQgXAAgagJg");
	this.shape_54.setTransform(-401.55,84.675);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FFFFFF").s().p("AhJBbQgXgUAAghQAAghAYgPQAYgQApAAIAsAAIAAgCQAAgigkAAQgPAAgRAGQgRAGgNAKIgbgnQArgfA2AAQAnAAAYAUQAZATAAArIAACIIg4AAIAAgaQgXAdgiAAQghAAgYgUgAgkAmQAAALAJAHQAIAGARAAQAPAAAMgKQAMgKAAgQIAAgLIglAAQgkAAAAAXg");
	this.shape_55.setTransform(-423.95,84.675);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FFFFFF").s().p("AhuCUIAAkjIA8AAIAAAXQAcgbAkAAQAmAAAdAfQAeAgAAAvQAAAvgeAgQgdAfgnABQgngBgYgfIAABqgAgkhPQgPARAAAaQAAAbAPAPQAPAQAUAAQAUAAAQgQQAPgPAAgaQAAgbgPgRQgPgSgUAAQgVAAgPASg");
	this.shape_56.setTransform(-448.075,88.4);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAggeQAhgeAsAAQAsAAAfAbQAfAbAAAuIAAAfIicAAQADASAPALQAQALATAAQAfAAAVgVIAhAlQgkAigxAAQgwAAgggegAgdgyQgPALgDATIBeAAQgCgUgNgKQgMgKgRAAQgSAAgOAKg");
	this.shape_57.setTransform(-484.75,84.675);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#FFFFFF").s().p("AhQB2QgeghAAgvQAAgwAdgfQAegdAnAAQAmgBAZAcIAAhqIA8AAIAAEoIg8AAIAAgcQgaAfglAAQgmAAgegggAgigDQgPARAAAaQAAAaAPARQAQAQATAAQAVAAAPgQQAOgRAAgbQAAgagOgQQgPgSgVAAQgUAAgPASg");
	this.shape_58.setTransform(-511.125,80.75);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAggeQAhgeAsAAQAsAAAfAbQAfAbAAAuIAAAfIicAAQADASAPALQAQALATAAQAfAAAVgVIAiAlQglAigxAAQgwAAgggegAgdgyQgPALgDATIBeAAQgCgUgNgKQgMgKgSAAQgQAAgPAKg");
	this.shape_59.setTransform(-305.35,35.925);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#FFFFFF").s().p("AhuCUIAAkkIA8AAIAAAZQAcgcAkAAQAmAAAdAfQAeAgAAAvQAAAvgeAgQgdAggngBQgnABgYggIAABqgAgkhQQgPASAAAaQAAAaAPAQQAPAQAUABQAUgBAQgQQAPgPAAgbQAAgagPgSQgPgRgUAAQgVAAgPARg");
	this.shape_60.setTransform(-330.525,39.65);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#FFFFFF").s().p("AgdCXIAAjWIA7AAIAADWgAgYhaQgKgKAAgPQAAgPAKgKQAKgKAOAAQAPAAAKAKQAKAKAAAPQAAAPgKAKQgKAKgPAAQgOAAgKgKg");
	this.shape_61.setTransform(-350.275,31.575);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#FFFFFF").s().p("AhMBXQgYgWAAgsIAAiBIA9AAIAAB0QAAAyAkgBQARABANgNQANgNAAgZIAAhzIA9AAIAADWIg9AAIAAgdQgWAggiAAQgkAAgYgWg");
	this.shape_62.setTransform(-369.025,36.1);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#FFFFFF").s().p("AAyCUIAAhfQgZAUggAAQgpABgfgfQgfgfAAgwQAAgwAeggQAegfAlAAQAkAAAbAcIAAgZIA9AAIAAEkgAgihQQgPASAAAaQAAAaAPAQQAQAQATABQAVgBAPgQQAOgQAAgaQAAgagOgSQgPgRgVAAQgUAAgPARg");
	this.shape_63.setTransform(-395.625,39.65);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#FFFFFF").s().p("AhLCBQgggeAAgzQAAgyAhgeQAggdArgBQAtABAfAaQAfAbAAAtIAAAhIicAAQADARAQAMQAPALATgBQAfAAAVgUIAhAkQgkAigxAAQgwAAgggegAgdgBQgPAJgCATIBdAAQgCgUgMgJQgNgKgSAAQgRAAgOALgAglhYIAzhGIA+AZIg4Atg");
	this.shape_64.setTransform(-420.6,31.1);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAhgeQAggeArAAQAtAAAfAbQAfAbAAAuIAAAfIicAAQADASAQALQAPALATAAQAfAAAVgVIAhAlQgkAigxAAQgwAAgggegAgdgyQgPALgCATIBdAAQgCgUgMgKQgNgKgSAAQgRAAgOAKg");
	this.shape_65.setTransform(-455.8,35.925);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#FFFFFF").s().p("AApBtIAAh4QAAgyglAAQgRAAgNANQgNANAAAZIAAB3Ig9AAIAAjWIA9AAIAAAYQAbgbAgAAQAjAAAXAYQAWAYAAAnIAACCg");
	this.shape_66.setTransform(-480.9,35.775);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#FFFFFF").s().p("AhVBtQgighAAg9IAAibIA/AAIAACaQAAAhAPATQAQAUAZAAQAbAAAPgUQAPgTAAghIAAiaIA/AAIAACbQAAA9giAhQgiAgg0AAQgzAAgiggg");
	this.shape_67.setTransform(-508.825,32.85);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34}]},57).to({state:[]},43).to({state:[]},1).wait(20));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-652.3,-370.9,1138.3,631.5999999999999);


// stage content:
(lib.FS_contact = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":150});

	// timeline functions:
	this.frame_99 = function() {
		//var frequency = 60;
		//stage.enableMouseOver(frequency);
		
		// EVENT DE CLIC POUR LA SUITE
		this.next_btn.addEventListener("click", fl_ClickToGoToAndPlayFromFrame.bind(this));
		this.next_btn.addEventListener("mouseover", fl_MouseOverHandler.bind(this));
		this.next_btn.addEventListener("mouseout", fl_MouseOutHandler.bind(this));
		
		function fl_ClickToGoToAndPlayFromFrame() {
			//this.gotoAndPlay("close");
		
			var event = new Event('next');
			this.dispatchEvent(event);
			event = null;
			
			this.next_btn.btnNext.gotoAndPlay('rollOut');
		
		}
		function fl_MouseOverHandler() { this.next_btn.btnNext.gotoAndPlay('rollOver');}
		function fl_MouseOutHandler() {	this.next_btn.btnNext.gotoAndPlay('rollOut');}
		
		
		// EVENT DE CLIC POUR LE LIEN 1
		this.cta_mc.ctas.cta0.addEventListener("click", onCTA0.bind(this));
		this.cta_mc.ctas.cta0.addEventListener("mouseover", onCTA0_MouseOverHandler.bind(this));
		this.cta_mc.ctas.cta0.addEventListener("mouseout", onCTA0_MouseOutHandler.bind(this));
		
		function onCTA0() {
			var event = new Event('cta0');
			this.dispatchEvent(event);
			event = null;
			
			this.cta_mc.ctas.cta0.gotoAndPlay('rollOut');
		}
		
		function onCTA0_MouseOverHandler() {	this.cta_mc.ctas.cta0.gotoAndPlay('rollOver');}
		function onCTA0_MouseOutHandler() {	this.cta_mc.ctas.cta0.gotoAndPlay('rollOut');}
		
		// EVENT DE CLIC POUR LE LIEN 2
		this.cta_mc.ctas.cta1.addEventListener("click", onCTA1.bind(this));
		this.cta_mc.ctas.cta1.addEventListener("mouseover", onCTA1_MouseOverHandler.bind(this));
		this.cta_mc.ctas.cta1.addEventListener("mouseout", onCTA1_MouseOutHandler.bind(this));
		
		function onCTA1() {
			var event = new Event('cta1');
			this.dispatchEvent(event);
			event = null;
		
			this.cta_mc.ctas.cta1.gotoAndPlay('rollOut');
		}
		
		function onCTA1_MouseOverHandler() {	this.cta_mc.ctas.cta1.gotoAndPlay('rollOver');}
		function onCTA1_MouseOutHandler() {	this.cta_mc.ctas.cta1.gotoAndPlay('rollOut');}
	}
	this.frame_149 = function() {
		//PAUSE
		this.stop();
	}
	this.frame_150 = function() {
		this.textes_mc.gotoAndPlay("close");
		this.visuel_mc.gotoAndPlay("close");
		this.cta_mc.gotoAndPlay("close");
		this.video_mc.gotoAndPlay("close");
		this.next_btn.gotoAndPlay("close");
		this.lignes_mc.gotoAndPlay("close_lines");
	}
	this.frame_169 = function() {
		//this.lignes_mc.gotoAndPlay("close");
	}
	this.frame_210 = function() {
		// EVENT DE FIN D'ANIM
		var event = new Event('closed');
		this.dispatchEvent(event);		
		event = null;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(99).call(this.frame_99).wait(50).call(this.frame_149).wait(1).call(this.frame_150).wait(19).call(this.frame_169).wait(41).call(this.frame_210).wait(1));

	// BLOC INFOS TEXTE
	this.textes_mc = new lib.blocInfostextes();
	this.textes_mc.name = "textes_mc";
	this.textes_mc.setTransform(858.2,232,1,1,0,0,0,222.1,-164);
	this.textes_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.textes_mc).wait(17).to({_off:false},0).wait(194));

	// VISUEL
	this.visuel_mc = new lib.bloc_visuel();
	this.visuel_mc.name = "visuel_mc";
	this.visuel_mc.setTransform(1192.55,391.5,1,1,0,0,0,552.5,26.5);
	this.visuel_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.visuel_mc).wait(17).to({_off:false},0).wait(194));

	// CTA NEXT
	this.next_btn = new lib.bloc_NEXT_btn();
	this.next_btn.name = "next_btn";
	this.next_btn.setTransform(753.2,786.9,1,1,0,0,0,-340.5,403.9);
	this.next_btn._off = true;

	this.timeline.addTween(cjs.Tween.get(this.next_btn).wait(89).to({_off:false},0).wait(122));

	// CTA DETAIL
	this.cta_mc = new lib.bloc_CTA();
	this.cta_mc.name = "cta_mc";
	this.cta_mc.setTransform(299,787,1,1,0,0,0,-340.5,403.9);
	this.cta_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.cta_mc).wait(99).to({_off:false},0).to({_off:true},74).wait(38));

	// VIDEO
	this.video_mc = new lib.bloc_video();
	this.video_mc.name = "video_mc";
	this.video_mc.setTransform(866.7,1147.15,1,1,0,0,0,227.5,764.9);
	this.video_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.video_mc).wait(94).to({_off:false},0).to({_off:true},79).wait(38));

	// lignes
	this.lignes_mc = new lib.lignes_all();
	this.lignes_mc.name = "lignes_mc";
	this.lignes_mc.setTransform(640,384.3,1,1,0,0,0,0,406.3);

	this.timeline.addTween(cjs.Tween.get(this.lignes_mc).wait(211));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(825.5,348.7,498.5,1114.6);
// library properties:
lib.properties = {
	id: 'F57725382E344746870FFB37CC9153BB',
	width: 1280,
	height: 768,
	fps: 60,
	color: "#666666",
	opacity: 0.00,
	manifest: [
		{src:"images/visuel_equipe_580x960jpgcopy.jpg?1581606650610", id:"visuel_equipe_580x960jpgcopy"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.StageGL();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['F57725382E344746870FFB37CC9153BB'] = {
	getStage: function() { return exportRoot.stage; },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


an.makeResponsive = function(isResp, respDim, isScale, scaleType, domContainers) {		
	var lastW, lastH, lastS=1;		
	window.addEventListener('resize', resizeCanvas);		
	resizeCanvas();		
	function resizeCanvas() {			
		var w = lib.properties.width, h = lib.properties.height;			
		var iw = window.innerWidth, ih=window.innerHeight;			
		var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;			
		if(isResp) {                
			if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {                    
				sRatio = lastS;                
			}				
			else if(!isScale) {					
				if(iw<w || ih<h)						
					sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==1) {					
				sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==2) {					
				sRatio = Math.max(xRatio, yRatio);				
			}			
		}			
		domContainers[0].width = w * pRatio * sRatio;			
		domContainers[0].height = h * pRatio * sRatio;			
		domContainers.forEach(function(container) {				
			container.style.width = w * sRatio + 'px';				
			container.style.height = h * sRatio + 'px';			
		});			
		stage.scaleX = pRatio*sRatio;			
		stage.scaleY = pRatio*sRatio;			
		lastW = iw; lastH = ih; lastS = sRatio;            
		stage.tickOnUpdate = false;            
		stage.update();            
		stage.tickOnUpdate = true;		
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;