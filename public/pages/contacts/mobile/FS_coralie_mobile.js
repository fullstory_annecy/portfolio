(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.chien = function() {
	this.initialize(img.chien);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,378,553);


(lib.coralie_step1 = function() {
	this.initialize(img.coralie_step1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,800,800);


(lib.coralie_step2 = function() {
	this.initialize(img.coralie_step2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,800,800);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.video_frame_croix = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{close:30});

	// timeline functions:
	this.frame_29 = function() {
		this.stop();
	}
	this.frame_50 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(29).call(this.frame_29).wait(21).call(this.frame_50).wait(1));

	// Layer_7 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("EguphRHIEsihMBYnCkwIksChg");
	var mask_graphics_1 = new cjs.Graphics().p("EgwLhQSIHwkLMBYnCkwInwELg");
	var mask_graphics_2 = new cjs.Graphics().p("EgxuhPdIK2l1MBYnCkwIq2F1g");
	var mask_graphics_3 = new cjs.Graphics().p("EgzQhOoIN6nfMBYnCkwIt6Hfg");
	var mask_graphics_4 = new cjs.Graphics().p("Eg0zhNzIRApJMBYnCkwIxAJJg");
	var mask_graphics_5 = new cjs.Graphics().p("Eg2VhM+IUEqzMBYnCkwI0EKzg");
	var mask_graphics_6 = new cjs.Graphics().p("Eg34hMJIXKsdMBYnCkwI3KMdg");
	var mask_graphics_7 = new cjs.Graphics().p("Eg5ahLUIaPuHMBYmCkwI6POHg");
	var mask_graphics_8 = new cjs.Graphics().p("Eg69hKfIdUvxMBYnCkwI9UPxg");
	var mask_graphics_9 = new cjs.Graphics().p("Eg8fhJqMAgZgRbMBYmCkwMggZARbg");
	var mask_graphics_10 = new cjs.Graphics().p("Eg+ChI1MAjegTFMBYnCkwMgjeATFg");
	var mask_graphics_11 = new cjs.Graphics().p("Eg/khIAMAmjgUvMBYmCkwMgmjAUvg");
	var mask_graphics_12 = new cjs.Graphics().p("EhBHhHLMApogWZMBYnCkwMgpoAWZg");
	var mask_graphics_13 = new cjs.Graphics().p("EhCphGWMAstgYDMBYmCkwMgstAYDg");
	var mask_graphics_14 = new cjs.Graphics().p("EhEMhFhMAvygZtMBYnCkwMgvyAZtg");
	var mask_graphics_15 = new cjs.Graphics().p("EhFuhEsMAy3gbXMBYmCkwMgy3AbXg");
	var mask_graphics_16 = new cjs.Graphics().p("EhHRhD3MA18gdBMBYnCkwMg18AdBg");
	var mask_graphics_17 = new cjs.Graphics().p("EhIzhDCMA5BgerMBYmCkwMg5BAerg");
	var mask_graphics_18 = new cjs.Graphics().p("EhKWhCNMA8GggVMBYnCkwMg8GAgVg");
	var mask_graphics_19 = new cjs.Graphics().p("EhL4hBYMA/Lgh/MBYmCkwMg/LAh/g");
	var mask_graphics_20 = new cjs.Graphics().p("EhNbhAjMBCQgjpMBYnCkwMhCQAjpg");
	var mask_graphics_21 = new cjs.Graphics().p("EhO+g/uMBFWglTMBYnCkwMhFWAlTg");
	var mask_graphics_22 = new cjs.Graphics().p("EhQgg+5MBIagm9MBYnCkwMhIaAm9g");
	var mask_graphics_23 = new cjs.Graphics().p("EhSDg+EMBLggonMBYnCkwMhLgAong");
	var mask_graphics_24 = new cjs.Graphics().p("EhTlg9PMBOkgqRMBYnCkwMhOkAqRg");
	var mask_graphics_25 = new cjs.Graphics().p("EhVIg8aMBRqgr7MBYnCkwMhRqAr7g");
	var mask_graphics_26 = new cjs.Graphics().p("EhWqg7lMBUugtlMBYnCkwMhUuAtlg");
	var mask_graphics_27 = new cjs.Graphics().p("EhYNg6wMBX0gvPMBYnCkwMhX0AvPg");
	var mask_graphics_28 = new cjs.Graphics().p("EhZvg57MBa3gw5MBYoCkwMha3Aw5g");
	var mask_graphics_29 = new cjs.Graphics().p("EhbSg5GMBd9gyjMBYoCkwMhd9Ayjg");
	var mask_graphics_30 = new cjs.Graphics().p("EhZDg6TMBZfgwJMBYoCkwMhZfAwJg");
	var mask_graphics_31 = new cjs.Graphics().p("EhW0g7gMBVCgtvMBYnCkwMhVCAtvg");
	var mask_graphics_32 = new cjs.Graphics().p("EhUlg8tMBQkgrVMBYnCkwMhQkArVg");
	var mask_graphics_33 = new cjs.Graphics().p("EhSWg95MBMHgo8MBYmCkvMhMHAo8g");
	var mask_graphics_34 = new cjs.Graphics().p("EhQHg/GMBHpgmiMBYmCkvMhHpAmig");
	var mask_graphics_35 = new cjs.Graphics().p("EhN5hATMBDMgkJMBYnCkwMhDMAkJg");
	var mask_graphics_36 = new cjs.Graphics().p("EhLqhBgMA+ughvMBYnCkwMg+uAhvg");
	var mask_graphics_37 = new cjs.Graphics().p("EhJbhCtMA6QgfVMBYnCkwMg6QAfVg");
	var mask_graphics_38 = new cjs.Graphics().p("EhHMhD6MA1ygc7MBYnCkwMg1yAc7g");
	var mask_graphics_39 = new cjs.Graphics().p("EhE9hFGMAxUgaiMBYnCkvMgxUAaig");
	var mask_graphics_40 = new cjs.Graphics().p("EhCuhGTMAs3gYIMBYmCkvMgs3AYIg");
	var mask_graphics_41 = new cjs.Graphics().p("EhAfhHgMAoZgVvMBYmCkwMgoZAVvg");
	var mask_graphics_42 = new cjs.Graphics().p("Eg+RhItMAj8gTVMBYnCkwMgj8ATVg");
	var mask_graphics_43 = new cjs.Graphics().p("Eg8ChJ6Ifew7MBYnCkwI/eQ7g");
	var mask_graphics_44 = new cjs.Graphics().p("Eg5zhLHIbAuhMBYnCkwI7AOhg");
	var mask_graphics_45 = new cjs.Graphics().p("Eg3khMTIWisIMBYnCkvI2iMIg");
	var mask_graphics_46 = new cjs.Graphics().p("Eg1VhNgISEpuMBYnCkvIyEJug");
	var mask_graphics_47 = new cjs.Graphics().p("EgzGhOtINnnVMBYmCkwItnHVg");
	var mask_graphics_48 = new cjs.Graphics().p("Egw3hP6IJJk7MBYmCkwIpJE7g");
	var mask_graphics_49 = new cjs.Graphics().p("EguphRHIEsihMBYnCkwIksChg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:-274.5,y:516.6}).wait(1).to({graphics:mask_graphics_1,x:-265.1,y:507.1}).wait(1).to({graphics:mask_graphics_2,x:-255.6,y:497.6}).wait(1).to({graphics:mask_graphics_3,x:-246.2,y:488}).wait(1).to({graphics:mask_graphics_4,x:-236.7,y:478.5}).wait(1).to({graphics:mask_graphics_5,x:-227.3,y:469}).wait(1).to({graphics:mask_graphics_6,x:-217.8,y:459.4}).wait(1).to({graphics:mask_graphics_7,x:-208.4,y:449.9}).wait(1).to({graphics:mask_graphics_8,x:-198.9,y:440.4}).wait(1).to({graphics:mask_graphics_9,x:-189.5,y:430.9}).wait(1).to({graphics:mask_graphics_10,x:-180,y:421.3}).wait(1).to({graphics:mask_graphics_11,x:-170.5,y:411.8}).wait(1).to({graphics:mask_graphics_12,x:-161.1,y:402.3}).wait(1).to({graphics:mask_graphics_13,x:-151.6,y:392.7}).wait(1).to({graphics:mask_graphics_14,x:-142.2,y:383.2}).wait(1).to({graphics:mask_graphics_15,x:-132.7,y:373.7}).wait(1).to({graphics:mask_graphics_16,x:-123.3,y:364.1}).wait(1).to({graphics:mask_graphics_17,x:-113.8,y:354.6}).wait(1).to({graphics:mask_graphics_18,x:-104.4,y:345.1}).wait(1).to({graphics:mask_graphics_19,x:-94.9,y:335.6}).wait(1).to({graphics:mask_graphics_20,x:-85.5,y:326}).wait(1).to({graphics:mask_graphics_21,x:-76,y:316.5}).wait(1).to({graphics:mask_graphics_22,x:-66.5,y:307}).wait(1).to({graphics:mask_graphics_23,x:-57.1,y:297.4}).wait(1).to({graphics:mask_graphics_24,x:-47.6,y:287.9}).wait(1).to({graphics:mask_graphics_25,x:-38.2,y:278.4}).wait(1).to({graphics:mask_graphics_26,x:-28.7,y:268.8}).wait(1).to({graphics:mask_graphics_27,x:-19.3,y:259.3}).wait(1).to({graphics:mask_graphics_28,x:-9.8,y:249.8}).wait(1).to({graphics:mask_graphics_29,x:-18.1,y:249.8}).wait(1).to({graphics:mask_graphics_30,x:-30.9,y:263.1}).wait(1).to({graphics:mask_graphics_31,x:-43.8,y:276.5}).wait(1).to({graphics:mask_graphics_32,x:-56.7,y:289.9}).wait(1).to({graphics:mask_graphics_33,x:-69.5,y:303.2}).wait(1).to({graphics:mask_graphics_34,x:-82.4,y:316.6}).wait(1).to({graphics:mask_graphics_35,x:-95.3,y:330}).wait(1).to({graphics:mask_graphics_36,x:-108.1,y:343.3}).wait(1).to({graphics:mask_graphics_37,x:-121,y:356.7}).wait(1).to({graphics:mask_graphics_38,x:-133.9,y:370.1}).wait(1).to({graphics:mask_graphics_39,x:-146.7,y:383.4}).wait(1).to({graphics:mask_graphics_40,x:-159.6,y:396.8}).wait(1).to({graphics:mask_graphics_41,x:-172.5,y:410.2}).wait(1).to({graphics:mask_graphics_42,x:-185.3,y:423.5}).wait(1).to({graphics:mask_graphics_43,x:-198.2,y:436.9}).wait(1).to({graphics:mask_graphics_44,x:-211.1,y:450.3}).wait(1).to({graphics:mask_graphics_45,x:-223.9,y:463.6}).wait(1).to({graphics:mask_graphics_46,x:-236.8,y:477}).wait(1).to({graphics:mask_graphics_47,x:-249.7,y:490.4}).wait(1).to({graphics:mask_graphics_48,x:-262.6,y:503.7}).wait(1).to({graphics:mask_graphics_49,x:-274.5,y:516.6}).wait(1).to({graphics:null,x:0,y:0}).wait(1));

	// Layer_6
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#CC9900").ss(1,1,1).p("AddwFMg65ghIMA65BibMg65gl1");
	this.shape.setTransform(-1.5,315);

	var maskedShapeInstanceList = [this.shape];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape).to({_off:true},50).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-191,-1,215,632);


(lib.video_frame_angle = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_59 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(59).call(this.frame_59).wait(1));

	// Layer_2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("EguphRHIEsihMBYnCkwIksChg");
	var mask_graphics_1 = new cjs.Graphics().p("EgwLhQSIHwkLMBYnCkwInwELg");
	var mask_graphics_2 = new cjs.Graphics().p("EgxuhPdIK2l1MBYnCkwIq2F1g");
	var mask_graphics_3 = new cjs.Graphics().p("EgzQhOoIN6nfMBYnCkwIt6Hfg");
	var mask_graphics_4 = new cjs.Graphics().p("Eg0zhNzIRApJMBYnCkwIxAJJg");
	var mask_graphics_5 = new cjs.Graphics().p("Eg2VhM+IUEqzMBYnCkwI0EKzg");
	var mask_graphics_6 = new cjs.Graphics().p("Eg34hMJIXKsdMBYnCkwI3KMdg");
	var mask_graphics_7 = new cjs.Graphics().p("Eg5ahLUIaPuHMBYmCkwI6POHg");
	var mask_graphics_8 = new cjs.Graphics().p("Eg69hKfIdUvxMBYnCkwI9UPxg");
	var mask_graphics_9 = new cjs.Graphics().p("Eg8fhJqMAgZgRbMBYmCkwMggZARbg");
	var mask_graphics_10 = new cjs.Graphics().p("Eg+ChI1MAjegTFMBYnCkwMgjeATFg");
	var mask_graphics_11 = new cjs.Graphics().p("Eg/khIAMAmjgUvMBYmCkwMgmjAUvg");
	var mask_graphics_12 = new cjs.Graphics().p("EhBHhHLMApogWZMBYnCkwMgpoAWZg");
	var mask_graphics_13 = new cjs.Graphics().p("EhCphGWMAstgYDMBYmCkwMgstAYDg");
	var mask_graphics_14 = new cjs.Graphics().p("EhEMhFhMAvygZtMBYnCkwMgvyAZtg");
	var mask_graphics_15 = new cjs.Graphics().p("EhFuhEsMAy3gbXMBYmCkwMgy3AbXg");
	var mask_graphics_16 = new cjs.Graphics().p("EhHRhD3MA18gdBMBYnCkwMg18AdBg");
	var mask_graphics_17 = new cjs.Graphics().p("EhIzhDCMA5BgerMBYmCkwMg5BAerg");
	var mask_graphics_18 = new cjs.Graphics().p("EhKWhCNMA8GggVMBYnCkwMg8GAgVg");
	var mask_graphics_19 = new cjs.Graphics().p("EhL4hBYMA/Lgh/MBYmCkwMg/LAh/g");
	var mask_graphics_20 = new cjs.Graphics().p("EhNbhAjMBCQgjpMBYnCkwMhCQAjpg");
	var mask_graphics_21 = new cjs.Graphics().p("EhO+g/uMBFWglTMBYnCkwMhFWAlTg");
	var mask_graphics_22 = new cjs.Graphics().p("EhQgg+5MBIagm9MBYnCkwMhIaAm9g");
	var mask_graphics_23 = new cjs.Graphics().p("EhSDg+EMBLggonMBYnCkwMhLgAong");
	var mask_graphics_24 = new cjs.Graphics().p("EhTlg9PMBOkgqRMBYnCkwMhOkAqRg");
	var mask_graphics_25 = new cjs.Graphics().p("EhVIg8aMBRqgr7MBYnCkwMhRqAr7g");
	var mask_graphics_26 = new cjs.Graphics().p("EhWqg7lMBUugtlMBYnCkwMhUuAtlg");
	var mask_graphics_27 = new cjs.Graphics().p("EhYNg6wMBX0gvPMBYnCkwMhX0AvPg");
	var mask_graphics_28 = new cjs.Graphics().p("EhZvg57MBa3gw5MBYoCkwMha3Aw5g");
	var mask_graphics_29 = new cjs.Graphics().p("EhbSg5GMBd9gyjMBYoCkwMhd9Ayjg");
	var mask_graphics_30 = new cjs.Graphics().p("EhZqg5+MBaugwzMBYnCkwMhauAwzg");
	var mask_graphics_31 = new cjs.Graphics().p("EhYDg61MBXggvEMBYnCkvMhXgAvEg");
	var mask_graphics_32 = new cjs.Graphics().p("EhWbg7tMBURgtVMBYmCkwMhURAtVg");
	var mask_graphics_33 = new cjs.Graphics().p("EhU0g8lMBRCgrlMBYnCkwMhRCArlg");
	var mask_graphics_34 = new cjs.Graphics().p("EhTNg9cMBN0gp2MBYnCkvMhN0Ap2g");
	var mask_graphics_35 = new cjs.Graphics().p("EhRlg+UMBKkgoHMBYnCkwMhKkAoHg");
	var mask_graphics_36 = new cjs.Graphics().p("EhP+g/LMBHWgmYMBYnCkvMhHWAmYg");
	var mask_graphics_37 = new cjs.Graphics().p("EhOWhADMBEHgkpMBYmCkwMhEHAkpg");
	var mask_graphics_38 = new cjs.Graphics().p("EhMvhA7MBA4gi5MBYnCkwMhA4Ai5g");
	var mask_graphics_39 = new cjs.Graphics().p("EhLHhByMA9pghKMBYmCkvMg9pAhKg");
	var mask_graphics_40 = new cjs.Graphics().p("EhJghCqMA6agfbMBYnCkwMg6aAfbg");
	var mask_graphics_41 = new cjs.Graphics().p("EhH5hDiMA3MgdrMBYnCkwMg3MAdrg");
	var mask_graphics_42 = new cjs.Graphics().p("EhGRhEZMAz8gb8MBYnCkvMgz8Ab8g");
	var mask_graphics_43 = new cjs.Graphics().p("EhEqhFRMAwugaNMBYnCkwMgwuAaNg");
	var mask_graphics_44 = new cjs.Graphics().p("EhDChGIMAtfgYeMBYmCkvMgtfAYeg");
	var mask_graphics_45 = new cjs.Graphics().p("EhBbhHAMAqQgWuMBYnCkvMgqQAWug");
	var mask_graphics_46 = new cjs.Graphics().p("Eg/zhH4MAnBgU/MBYmCkwMgnBAU/g");
	var mask_graphics_47 = new cjs.Graphics().p("Eg+MhIvMAjygTQMBYnCkvMgjyATQg");
	var mask_graphics_48 = new cjs.Graphics().p("Eg8lhJnMAgkgRhMBYnCkwMggkARhg");
	var mask_graphics_49 = new cjs.Graphics().p("Eg69hKfIdUvxMBYnCkwI9UPxg");
	var mask_graphics_50 = new cjs.Graphics().p("Eg5WhLWIaGuCMBYnCkvI6GOCg");
	var mask_graphics_51 = new cjs.Graphics().p("Eg3uhMOIW3sTMBYmCkwI23MTg");
	var mask_graphics_52 = new cjs.Graphics().p("Eg2HhNGIToqjMBYnCkwIzoKjg");
	var mask_graphics_53 = new cjs.Graphics().p("Eg0fhN9IQZo0MBYmCkvIwZI0g");
	var mask_graphics_54 = new cjs.Graphics().p("Egy4hO1INKnFMBYnCkwItKHFg");
	var mask_graphics_55 = new cjs.Graphics().p("EgxRhPsIJ8lWMBYnCkvIp8FWg");
	var mask_graphics_56 = new cjs.Graphics().p("EgvphQkIGsjnMBYnCkwImsDng");
	var mask_graphics_57 = new cjs.Graphics().p("EgvDhRcIDeh3MBYnCkwIjeB3g");
	var mask_graphics_58 = new cjs.Graphics().p("EgvDhSTIAPgIMBYnCkvIgPAIg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:268.5,y:95.6}).wait(1).to({graphics:mask_graphics_1,x:259.2,y:100.6}).wait(1).to({graphics:mask_graphics_2,x:249.9,y:105.6}).wait(1).to({graphics:mask_graphics_3,x:240.7,y:110.6}).wait(1).to({graphics:mask_graphics_4,x:231.4,y:115.6}).wait(1).to({graphics:mask_graphics_5,x:222.1,y:120.6}).wait(1).to({graphics:mask_graphics_6,x:212.8,y:125.6}).wait(1).to({graphics:mask_graphics_7,x:203.6,y:130.5}).wait(1).to({graphics:mask_graphics_8,x:194.3,y:135.5}).wait(1).to({graphics:mask_graphics_9,x:185,y:140.5}).wait(1).to({graphics:mask_graphics_10,x:175.8,y:145.5}).wait(1).to({graphics:mask_graphics_11,x:166.5,y:150.5}).wait(1).to({graphics:mask_graphics_12,x:157.2,y:155.5}).wait(1).to({graphics:mask_graphics_13,x:148,y:160.5}).wait(1).to({graphics:mask_graphics_14,x:138.7,y:165.4}).wait(1).to({graphics:mask_graphics_15,x:129.4,y:170.4}).wait(1).to({graphics:mask_graphics_16,x:120.1,y:175.4}).wait(1).to({graphics:mask_graphics_17,x:110.9,y:180.4}).wait(1).to({graphics:mask_graphics_18,x:101.6,y:185.4}).wait(1).to({graphics:mask_graphics_19,x:92.3,y:190.4}).wait(1).to({graphics:mask_graphics_20,x:83.1,y:195.4}).wait(1).to({graphics:mask_graphics_21,x:73.8,y:200.4}).wait(1).to({graphics:mask_graphics_22,x:64.5,y:205.3}).wait(1).to({graphics:mask_graphics_23,x:55.3,y:210.3}).wait(1).to({graphics:mask_graphics_24,x:46,y:215.3}).wait(1).to({graphics:mask_graphics_25,x:36.7,y:220.3}).wait(1).to({graphics:mask_graphics_26,x:27.4,y:225.3}).wait(1).to({graphics:mask_graphics_27,x:18.2,y:230.3}).wait(1).to({graphics:mask_graphics_28,x:8.9,y:235.3}).wait(1).to({graphics:mask_graphics_29,x:-18.1,y:249.8}).wait(1).to({graphics:mask_graphics_30,x:-28.4,y:255.3}).wait(1).to({graphics:mask_graphics_31,x:-38.7,y:260.9}).wait(1).to({graphics:mask_graphics_32,x:-49.1,y:266.5}).wait(1).to({graphics:mask_graphics_33,x:-59.4,y:272}).wait(1).to({graphics:mask_graphics_34,x:-69.8,y:277.6}).wait(1).to({graphics:mask_graphics_35,x:-80.1,y:283.2}).wait(1).to({graphics:mask_graphics_36,x:-90.5,y:288.7}).wait(1).to({graphics:mask_graphics_37,x:-100.8,y:294.3}).wait(1).to({graphics:mask_graphics_38,x:-111.2,y:299.9}).wait(1).to({graphics:mask_graphics_39,x:-121.5,y:305.4}).wait(1).to({graphics:mask_graphics_40,x:-131.8,y:311}).wait(1).to({graphics:mask_graphics_41,x:-142.2,y:316.6}).wait(1).to({graphics:mask_graphics_42,x:-152.5,y:322.1}).wait(1).to({graphics:mask_graphics_43,x:-162.9,y:327.7}).wait(1).to({graphics:mask_graphics_44,x:-173.2,y:333.3}).wait(1).to({graphics:mask_graphics_45,x:-183.6,y:338.8}).wait(1).to({graphics:mask_graphics_46,x:-193.9,y:344.4}).wait(1).to({graphics:mask_graphics_47,x:-204.2,y:350}).wait(1).to({graphics:mask_graphics_48,x:-214.6,y:355.5}).wait(1).to({graphics:mask_graphics_49,x:-224.9,y:361.1}).wait(1).to({graphics:mask_graphics_50,x:-235.3,y:366.7}).wait(1).to({graphics:mask_graphics_51,x:-245.6,y:372.2}).wait(1).to({graphics:mask_graphics_52,x:-256,y:377.8}).wait(1).to({graphics:mask_graphics_53,x:-266.3,y:383.4}).wait(1).to({graphics:mask_graphics_54,x:-276.7,y:388.9}).wait(1).to({graphics:mask_graphics_55,x:-287,y:394.5}).wait(1).to({graphics:mask_graphics_56,x:-297.3,y:400.1}).wait(1).to({graphics:mask_graphics_57,x:-301.2,y:405.6}).wait(1).to({graphics:mask_graphics_58,x:-301.2,y:411.2}).wait(1).to({graphics:null,x:0,y:0}).wait(1));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#CC9900").ss(1,1,1).p("EgdrgxQMAAABikEAdxgxTMg7hAAA");
	this.shape.setTransform(0,315.6);

	var maskedShapeInstanceList = [this.shape];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape).to({_off:true},59).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-30.1,-1,221.6,631.9);


(lib.video = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(51,51,51,0)").s().p("Egu3AjKMAAAhGTMBdvAAAMAAABGTg");
	this.shape.setTransform(300,225);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.video, new cjs.Rectangle(0,0,649.4,487), null);


(lib.txt_hashtag = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgKArQgIgIAAgMIAAghIgJAAIAAgQIAJAAIAAgWIAUAAIAAAWIASAAIAAAQIgSAAIAAAgQAAAEADADQACADADAAQAGAAAEgGIAIAPQgKAIgLABQgKgBgHgGg");
	this.shape.setTransform(-12.7,52.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAOAmIAAgpQAAgSgNAAQgFAAgFAFQgEAEAAAJIAAApIgVAAIAAhKIAVAAIAAAIQAJgJALAAQAMAAAIAIQAIAJAAANIAAAtg");
	this.shape_1.setTransform(-20.4,53.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgJA1IAAhKIAUAAIAABKgAgIgfQgDgDAAgFQAAgGADgDQAEgEAEAAQAFAAAEAEQADADAAAGQAAAFgDADQgEADgFABQgEgBgEgDg");
	this.shape_2.setTransform(-27,52.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgVAmIAAhKIAVAAIAAAKQADgFAGgDQAGgDAHAAIAAATIgEAAQgKAAgEAHQgEAHAAAKIAAAgg");
	this.shape_3.setTransform(-31.6,53.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgmAxIAAhhIAiAAQAWAAALAJQAKAIAAATQAAARgLAIQgLAJgUgBIgNAAIAAAcgAgQADIAOAAQALAAAEgEQADgEAAgJQABgIgGgDQgFgEgKAAIgMAAg");
	this.shape_4.setTransform(-39.1,52.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgTIANAAIADgRIgPAAIADgUIAPAAIAEgWIAUAAIgDAWIAMAAIADgWIAWAAIgEAWIAOAAIgDAUIgPAAIgDARIARAAIgEATIgQAAIgDAVgAgHAKIALAAIAEgRIgMAAg");
	this.shape_5.setTransform(-49.5,52.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgOAkQgJgDgHgGIAJgNQAMAJAMAAQAEAAACgCQABAAAAgBQABAAAAgBQAAAAABgBQAAAAAAgBQAAgDgEgCQgDgCgFgCIgKgDIgHgFQgJgEAAgKQAAgLAJgGQAIgHAMAAQAOAAANAJIgIAOQgKgHgJAAQgJAAAAAGQAAADADABIAJAEIAKAEIAIADQAHAEAAALQAAALgIAHQgIAHgNAAQgHAAgJgDg");
	this.shape_6.setTransform(104.6,32.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgZAcQgMgKAAgSQAAgRAMgKQALgLAPAAQAPAAALAKQALAJAAAQIAAALIg2AAQABAGAFAEQAGADAGAAQALAAAHgHIAMANQgNAMgRAAQgRAAgKgLgAgKgRQgEAEgBAHIAfAAQAAgHgEgEQgFgDgGAAQgFAAgGADg");
	this.shape_7.setTransform(96.8,32.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgXAcQgMgLAAgRQAAgQAMgLQAMgLAPAAQAIAAAIAEQAJADAGAGIgLAPQgEgFgFgCQgFgCgFAAQgIAAgFAFQgHAFAAAJQAAAKAHAFQAGAFAGAAQALAAAIgKIANAOQgPAPgRAAQgQAAgLgLg");
	this.shape_8.setTransform(88.4,32.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AAOAmIAAgpQAAgSgNAAQgFAAgFAFQgEAEAAAJIAAApIgVAAIAAhKIAVAAIAAAIQAJgJALAAQAMAAAIAIQAIAJAAANIAAAtg");
	this.shape_9.setTransform(79.8,32.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgaAcQgLgKAAgSQAAgRALgKQAMgLAOAAQAQAAALAKQALAJAAAQIAAALIg2AAQABAGAFAEQAGADAGAAQALAAAHgHIAMANQgNAMgRAAQgQAAgMgLgAgKgRQgEAEgBAHIAfAAQAAgHgEgEQgFgDgGAAQgFAAgGADg");
	this.shape_10.setTransform(71,32.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgJA1IAAhKIAUAAIAABKgAgIgfQgDgEAAgEQAAgGADgDQAEgEAEAAQAFAAAEAEQADADAAAGQAAAEgDAEQgEADgFAAQgEAAgEgDg");
	this.shape_11.setTransform(64.6,31.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgVAmIAAhKIAVAAIAAAKQADgFAGgDQAGgDAHAAIAAATIgEAAQgKAAgEAHQgEAHAAAKIAAAgg");
	this.shape_12.setTransform(60,32.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgZAcQgMgKAAgSQAAgRAMgKQALgLAPAAQAPAAALAKQALAJAAAQIAAALIg2AAQABAGAGAEQAFADAGAAQALAAAHgHIALANQgMAMgRAAQgRAAgKgLgAgJgRQgGAEgBAHIAhAAQgBgHgFgEQgEgDgGAAQgFAAgFADg");
	this.shape_13.setTransform(52.5,32.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgmA0IAAhmIAVAAIAAAJQAKgKAMAAQANAAAKALQALALAAARQAAAPgLAMQgKALgOAAQgMAAgJgLIAAAlgAgMgbQgFAGAAAJQAAAJAFAFQAFAGAHAAQAHAAAFgGQAGgFAAgJQAAgJgGgGQgFgGgHAAQgHAAgFAGg");
	this.shape_14.setTransform(43.7,34.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AAOAmIgOgVIgOAVIgYAAIAagmIgZglIAZAAIAMAVIANgVIAZAAIgaAlIAbAmg");
	this.shape_15.setTransform(34.8,32.8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBGAAIAAATIgwAAIAAAVIArAAIAAASIgrAAIAAATIAxAAIAAAUg");
	this.shape_16.setTransform(26.3,31.7);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgHATIAAglIAPAAIAAAlg");
	this.shape_17.setTransform(19.7,28.7);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgbAqQgLgMAAgRQAAgPALgMQAKgKANAAQANAAAJAJIAAglIAVAAIAABoIgVAAIAAgKQgJAKgNABQgNgBgKgKgAgLAAQgGAFAAAJQABAJAFAGQAGAFAGABQAHgBAFgFQAFgGAAgJQAAgKgFgEQgFgHgHAAQgHAAgFAHg");
	this.shape_18.setTransform(13,31.5);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AAOAmIAAgpQAAgSgNAAQgFAAgFAFQgEAEAAAJIAAApIgVAAIAAhKIAVAAIAAAIQAJgJALAAQAMAAAIAIQAIAJAAANIAAAtg");
	this.shape_19.setTransform(0.4,32.8);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgSAzQgJgEgGgEIAIgQQAKAHAMABQAKgBAHgFQAHgGAAgMQgKALgOAAQgPAAgKgKQgKgKAAgQQAAgQALgMQAKgKANAAQANAAAJALIAAgKIAVAAIAABBQAAAKgEAIQgDAHgGAFQgMAJgPAAQgIAAgJgCgAgLgdQgGAFAAAJQAAAJAGAGQAFAEAGAAQAIAAAFgEQAFgFAAgKQAAgJgFgFQgFgFgHAAQgHAAgFAFg");
	this.shape_20.setTransform(-9,34.3);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgJA1IAAhKIAUAAIAABKgAgIgfQgDgEAAgEQAAgGADgDQAEgEAEAAQAFAAAEAEQADADAAAGQAAAEgDAEQgEADgFAAQgEAAgEgDg");
	this.shape_21.setTransform(-15.5,31.3);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgOAkQgJgDgHgGIAJgNQAMAJAMAAQAEAAACgCQABAAAAgBQABAAAAgBQAAAAABgBQAAAAAAgBQAAgDgEgCQgDgCgFgCIgKgDIgHgFQgJgEAAgKQAAgLAJgGQAIgHAMAAQAOAAANAJIgIAOQgKgHgJAAQgJAAAAAGQAAADADABIAJAEIAKAEIAIADQAHAEAAALQAAALgIAHQgIAHgNAAQgHAAgJgDg");
	this.shape_22.setTransform(-21.3,32.8);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgZAcQgMgKAAgSQAAgRAMgKQALgLAPAAQAPAAALAKQALAJAAAQIAAALIg2AAQABAGAFAEQAGADAGAAQALAAAHgHIALANQgMAMgRAAQgRAAgKgLgAgKgRQgEAEgBAHIAfAAQAAgHgEgEQgFgDgGAAQgFAAgGADg");
	this.shape_23.setTransform(-29.1,32.8);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgrAxIAAhhIAiAAQAaAAANANQAPAMAAAXQAAAWgPAOQgNANgbAAgAgWAdIAOAAQAPAAAHgHQAJgIgBgOQABgOgJgHQgHgIgRAAIgMAAg");
	this.shape_24.setTransform(-38.6,31.7);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgTIANAAIADgRIgPAAIADgTIAPAAIAEgXIAUAAIgDAXIAMAAIADgXIAWAAIgEAXIAOAAIgDATIgPAAIgDARIARAAIgEATIgQAAIgDAVgAgHAKIALAAIAEgRIgMAAg");
	this.shape_25.setTransform(-49.5,31.6);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AAOAmIAAgpQAAgSgNAAQgFAAgFAFQgEAEAAAJIAAApIgVAAIAAhKIAVAAIAAAIQAJgJALAAQAMAAAIAIQAIAJAAANIAAAtg");
	this.shape_26.setTransform(34,11.7);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgbAcQgMgMAAgQQAAgPAMgMQALgLAQAAQARAAAMALQALAMAAAPQAAAQgLAMQgMALgRAAQgQAAgLgLgAgMgOQgGAGAAAIQAAAJAGAGQAFAGAHAAQAIAAAGgGQAFgGAAgJQAAgIgFgGQgGgGgIAAQgHAAgFAGg");
	this.shape_27.setTransform(24.9,11.8);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AgJA1IAAhKIAUAAIAABKgAgIgfQgDgEAAgEQAAgGADgDQAEgEAEAAQAFAAAEAEQADADAAAGQAAAEgDAEQgEAEgFgBQgEABgEgEg");
	this.shape_28.setTransform(18.3,10.3);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AgKAqQgIgGAAgNIAAgiIgJAAIAAgPIAJAAIAAgWIAUAAIAAAWIASAAIAAAPIgSAAIAAAhQAAAFADACQACACADABQAGgBAEgFIAIAPQgKAJgLAAQgKAAgHgIg");
	this.shape_29.setTransform(13.4,10.7);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AgmA0IAAhmIAVAAIAAAJQAKgKAMAAQANAAAKALQALALAAARQAAAPgLAMQgKALgOAAQgMAAgJgLIAAAlgAgMgbQgFAGAAAJQAAAJAFAFQAFAGAHAAQAHAAAFgGQAGgFAAgJQAAgJgGgGQgFgGgHAAQgHAAgFAGg");
	this.shape_30.setTransform(5.7,13.1);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AgaAcQgLgKAAgSQAAgRALgKQAMgLAOAAQAQAAALAKQALAJAAAQIAAALIg2AAQABAGAFAEQAGADAGAAQALAAAHgHIAMANQgNAMgRAAQgQAAgMgLgAgKgRQgEAEgCAHIAgAAQAAgHgFgEQgEgDgGAAQgGAAgFADg");
	this.shape_31.setTransform(-3.4,11.8);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AgXAcQgMgLAAgRQAAgQAMgLQAMgLAPAAQAIAAAJAEQAIADAGAGIgLAPQgEgFgFgCQgFgCgFAAQgHAAgHAFQgFAFAAAJQAAAKAFAFQAGAFAIAAQAKAAAIgKIANAOQgPAPgQAAQgQAAgMgLg");
	this.shape_32.setTransform(-11.8,11.8);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AAOAmIAAgpQAAgSgNAAQgFAAgFAFQgEAEAAAJIAAApIgVAAIAAhKIAVAAIAAAIQAJgJALAAQAMAAAIAIQAIAJAAANIAAAtg");
	this.shape_33.setTransform(-20.4,11.7);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AgbAcQgMgMAAgQQAAgPAMgMQALgLAQAAQARAAAMALQALAMAAAPQAAAQgLAMQgMALgRAAQgQAAgLgLgAgMgOQgGAGAAAIQAAAJAGAGQAFAGAHAAQAIAAAGgGQAFgGAAgJQAAgIgFgGQgGgGgIAAQgHAAgFAGg");
	this.shape_34.setTransform(-29.5,11.8);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AgeAkQgPgOAAgWQAAgUAPgPQAPgOAUAAQAZgBAQATIgOAPQgKgMgQAAQgLgBgJAJQgJAHAAANQABAOAIAIQAJAIAKAAQAQAAAKgNIAPAPQgSATgWAAQgWgBgOgOg");
	this.shape_35.setTransform(-39,10.6);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgTIANAAIADgRIgPAAIADgTIAPAAIAEgXIAUAAIgDAXIAMAAIADgXIAWAAIgEAXIAOAAIgDATIgPAAIgDARIARAAIgEATIgQAAIgDAVgAgHAKIALAAIAEgRIgMAAg");
	this.shape_36.setTransform(-49.5,10.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.txt_hashtag, new cjs.Rectangle(-56.7,0,171,63.2), null);


(lib.prject_illustration = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_43 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(43).call(this.frame_43).wait(18));

	// Layer_2
	this.instance = new lib.coralie_step1();
	this.instance.parent = this;
	this.instance.setTransform(-666,-208,1.206,1.206);

	this.instance_1 = new lib.coralie_step2();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-666,-208,1.206,1.206);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},43).wait(18));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-666,-208,965,965);


(lib.masqueTexte_1_ligne = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("EgicAEJIAAoRMBE5AAAIAAIRg");
	this.shape.setTransform(0,24.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.masqueTexte_1_ligne, new cjs.Rectangle(-220.5,-1.9,441,53), null);


(lib.masque_generique = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CC9900").s().p("AsQCqIAAlTIYhAAIAAFTg");
	this.shape.setTransform(78.5,17);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.masque_generique, new cjs.Rectangle(0,0,157,34), null);


(lib.ligne = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(49));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#333333").ss(2,1,1).p("EgABhM/MAADCZ/");
	this.shape.setTransform(-0.2,319.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#333333").ss(2,1,1).p("EAAghM7Qg1OzgbRBQgbUYAEVMQADWBAkVVQAlTkBERl");
	this.shape_1.setTransform(-7,319.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#333333").ss(2,1,1).p("EAA7hM5QhfN/gxR5QgxUHAHVhQAGWSA/VCQBDUPB4Qw");
	this.shape_2.setTransform(-12.4,319.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#333333").ss(2,1,1).p("EABQhM3Qh/NXhDSkQhAT5AIVyQAIWfBUU0QBZUwChQG");
	this.shape_3.setTransform(-16.5,320.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#333333").ss(2,1,1).p("EABfhM1QiXM4hPTEQhMTvAKV+QAJWpBjUqQBqVIC+Pn");
	this.shape_4.setTransform(-19.6,320.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#333333").ss(2,1,1).p("EABqhM0QioMjhYTaQhVToALWHQALWwBuUiQB1VaDUPR");
	this.shape_5.setTransform(-21.8,320.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#333333").ss(2,1,1).p("EABxhMzQizMUheTqQhbTjAMWNQALW1B2UdQB9VmDjPB");
	this.shape_6.setTransform(-23.3,320.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#333333").ss(2,1,1).p("EAB2hMzQi7MKhiT1QhfTgAMWRQAMW3B7UaQCDVvDsO3");
	this.shape_7.setTransform(-24.2,320.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#333333").ss(2,1,1).p("EAB5hMyQjAMEhkT7QhhTeAMWTQAMW5B+UYQCGVzDyOx");
	this.shape_8.setTransform(-24.8,320.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#333333").ss(2,1,1).p("EAB7hMyQjCMBhmT+QhjTdANWVQAMW6CAUWQCHV2D2Ou");
	this.shape_9.setTransform(-25.2,320.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#333333").ss(2,1,1).p("EAB8hMyQjEL/hmUAQhjTcAMWWQANW7CAUWQCIV3D3Os");
	this.shape_10.setTransform(-25.3,320.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#333333").ss(2,1,1).p("EAB8hMyQjEL/hnUAQhjTcANWWQAMW7CAUWQCJV3D4Os");
	this.shape_11.setTransform(-25.4,320.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#333333").ss(2,1,1).p("EAB8hMyQjEL/hnUAQhjTcAMWWQANW7CAUWQCJV3D4Os");
	this.shape_12.setTransform(-25.4,320.5);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#333333").ss(2,1,1).p("EAB8hMyQjEL/hnUAQhjTcAMWWQANW7CAUWQCJV4D4Or");
	this.shape_13.setTransform(-25.4,320.5);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#333333").ss(2,1,1).p("EAB8hMyQjEL/hmUAQhkTcANWWQAMW7CBUWQCIV3D4Os");
	this.shape_14.setTransform(-25.4,320.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#333333").ss(2,1,1).p("EAB7hMyQjDMAhmT/QhjTdANWVQAMW6CAUWQCIV3D2Ot");
	this.shape_15.setTransform(-25.2,320.5);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#333333").ss(2,1,1).p("EAB6hMyQjBMChmT9QhiTdAMWVQANW6B/UXQCHV1D1Ou");
	this.shape_16.setTransform(-25.1,320.5);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#333333").ss(2,1,1).p("EAB5hMyQjAMEhkT7QhiTeANWTQAMW5B+UYQCGV0DzOw");
	this.shape_17.setTransform(-24.9,320.5);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#333333").ss(2,1,1).p("EAB4hMzQi+MHhjT5QhgTfAMWRQAMW5B8UZQCFVxDvO0");
	this.shape_18.setTransform(-24.6,320.5);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#333333").ss(2,1,1).p("EAB2hMzQi7MLhhT0QhfTgAMWRQAMW3B6UaQCDVtDrO5");
	this.shape_19.setTransform(-24.2,320.5);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#333333").ss(2,1,1).p("EABzhMzQi2MQhgTuQhcTiALWPQAMW1B4UcQB/VqDnO9");
	this.shape_20.setTransform(-23.7,320.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#333333").ss(2,1,1).p("EABwhMzQiyMWhcToQhbTkAMWMQALWzB1UeQB8VlDgPD");
	this.shape_21.setTransform(-23,320.4);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#333333").ss(2,1,1).p("EABshM0QisMehZTgQhXTmALWJQALWxBwUhQB4VeDZPM");
	this.shape_22.setTransform(-22.2,320.4);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#333333").ss(2,1,1).p("EABohM0QilMnhVTWQhUTpALWFQAKWuBsUlQBzVWDQPV");
	this.shape_23.setTransform(-21.3,320.3);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#333333").ss(2,1,1).p("EABihM1QibMyhSTKQhPTuAKWAQAKWrBnUoQBsVNDFPh");
	this.shape_24.setTransform(-20.2,320.3);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#333333").ss(2,1,1).p("EABchM1QiSM/hMS8QhKTyAKV7QAJWnBgUsQBmVDC4Pt");
	this.shape_25.setTransform(-18.9,320.2);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#333333").ss(2,1,1).p("EABUhM2QiGNNhGStQhET3AJV1QAJWiBYUyQBeU3CpP8");
	this.shape_26.setTransform(-17.4,320.1);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#333333").ss(2,1,1).p("EABMhM3Qh5Neg/ScQg9T8AIVuQAHWdBQU2QBVUqCYQO");
	this.shape_27.setTransform(-15.7,320);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#333333").ss(2,1,1).p("EABChM4QhqNxg3SIQg2UCAHVnQAHWWBGU9QBKUbCGQh");
	this.shape_28.setTransform(-13.8,319.9);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#333333").ss(2,1,1).p("EAA3hM5QhZOGguRxQguUKAGVeQAGWPA7VFQA/UJBxQ3");
	this.shape_29.setTransform(-11.7,319.8);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#333333").ss(2,1,1).p("EAAuhM6QhKOZgnRdQgmUQAFVWQAGWKAxVLQA0T6BfRK");
	this.shape_30.setTransform(-9.8,319.7);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#333333").ss(2,1,1).p("EAAlhM7Qg8OqggRLQgfUWAEVPQAEWFApVQQArTtBORb");
	this.shape_31.setTransform(-8.1,319.7);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#333333").ss(2,1,1).p("EAAehM7QgxO4gaQ8QgZUaADVJQAEWAAhVWQAjTgA/Rq");
	this.shape_32.setTransform(-6.6,319.6);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#333333").ss(2,1,1).p("EAAXhM8QgnPFgUQuQgUUeADVFQADV8AaVaQAcTWAzR3");
	this.shape_33.setTransform(-5.3,319.5);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#333333").ss(2,1,1).p("EAAShM9QgePQgRQjQgPUiACVAQADV4AVVeQAVTNAoSD");
	this.shape_34.setTransform(-4.2,319.5);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#333333").ss(2,1,1).p("EAANhM9QgXPZgMQZQgMUlACU9QACV1AQVhQARTFAeSM");
	this.shape_35.setTransform(-3.2,319.4);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#333333").ss(2,1,1).p("EAAJhM9QgRPggJQRQgJUoACU5QABVyANVkQAMS/AXSU");
	this.shape_36.setTransform(-2.4,319.4);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#333333").ss(2,1,1).p("EAAGhM+QgMPogGQKQgHUpACU3QABVwAJVmQAJS6ARSb");
	this.shape_37.setTransform(-1.8,319.3);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#333333").ss(2,1,1).p("EAADhM+QgIPsgEQFQgEUrABU1QABVuAGVoQAGS2AMSg");
	this.shape_38.setTransform(-1.3,319.3);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#333333").ss(2,1,1).p("EAABhM+QgFPwgCQAQgDUtABUzQABVtAEVqQAESyAISk");
	this.shape_39.setTransform(-0.9,319.3);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#333333").ss(2,1,1).p("EAAAhM+QgCPzgCP9QgBUuAAUxQABVtADVqQACSwAFSn");
	this.shape_40.setTransform(-0.6,319.3);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#333333").ss(2,1,1).p("EAAAhM+QgCP1gBP7QAAUuAAUxQABVsACVrQABSuADSp");
	this.shape_41.setTransform(-0.4,319.3);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#333333").ss(2,1,1).p("EgABhM/QgBP3AAP5QAAUvABUxQAAVsABVrQABStACSr");
	this.shape_42.setTransform(-0.2,319.3);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#333333").ss(2,1,1).p("EgABhM/QAAP4AAP4QAAUwABUvQAAVsAAVrQABStABSs");
	this.shape_43.setTransform(-0.2,319.3);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#333333").ss(2,1,1).p("EgABhM/QABP4AAP4QAAUwAAUvQAAVsABVrQABStAASs");
	this.shape_44.setTransform(-0.2,319.3);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#333333").ss(2,1,1).p("EgABhM/QABP4AAP4QAAUwAAUvQABVsAAVrQABStAASs");
	this.shape_45.setTransform(-0.2,319.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.3,-174.5,2.4,987.5);


(lib.lettres_FAT = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CC9900").s().p("AkVDvIBehzQBxBjBfAAQAqAAAZgSQAZgTgBgfQAAgfgagTQgagThLgSQh6geg5gtQg4gvAAhjQAAhkBHg2QBIg2BpAAQBHAABFAZQBGAYA1ArIhPByQhchFhfAAQgoABgWASQgWASAAAfQAAAfAbARQAcATBiAYQBkAYA3AwQA3AygBBdQAABfhGA6QhHA7hygBQimAAiFh6g");
	this.shape.setTransform(45.6,311.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#CC9900").s().p("AjvFeIAAq7IHfAAIAACJIlDAAIAACXIEzAAIAACIIkzAAIAAETg");
	this.shape_1.setTransform(-68.3,187.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.lettres_FAT, new cjs.Rectangle(-102.9,0,195.2,373.7), null);


(lib.hastaggraphicbranche = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AjQAuIAShbIDlAAICqAAIgSBbg");
	this.shape.setTransform(0,4.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.hastaggraphicbranche, new cjs.Rectangle(-20.8,0,41.7,9.2), null);


(lib.cta1_back = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CC9900").s().p("Ao1EsIAApXIRrAAIAAJXg");
	this.shape.setTransform(56.6,30);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.cta1_back, new cjs.Rectangle(0,0,113.2,60), null);


(lib.chien_coeur = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.chien();
	this.instance.parent = this;
	this.instance.setTransform(-189,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.chien_coeur, new cjs.Rectangle(-189,0,378,553), null);


(lib.Btn_cliquable_area = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CC9900").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape.setTransform(0,30);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Btn_cliquable_area, new cjs.Rectangle(-113,0,226,60), null);


(lib.masqueTexte = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// masqueTexte_1_ligne
	this.instance = new lib.masqueTexte_1_ligne();
	this.instance.parent = this;
	this.instance.setTransform(-218.8,191.5,0.008,1,0,0,0,0,25.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(6).to({_off:false},0).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.01,x:218.8},14).wait(1));

	// masqueTexte_1_ligne
	this.instance_1 = new lib.masqueTexte_1_ligne();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-218.8,136.1,0.008,1,0,0,0,0,25.5);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.01,x:218.8},14).to({_off:true},1).wait(2));

	// masqueTexte_1_ligne
	this.instance_2 = new lib.masqueTexte_1_ligne();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-218.8,80.8,0.008,1,0,0,0,0,25.5);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(2).to({_off:false},0).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.01,x:218.8},14).to({_off:true},1).wait(4));

	// masqueTexte_1_ligne
	this.instance_3 = new lib.masqueTexte_1_ligne();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-218.8,25.5,0.008,1,0,0,0,0,25.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.01,x:218.8},14).to({_off:true},1).wait(6));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-220.5,-1.9,3.5,53);


(lib.hashtag_graphic = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(64));

	// hastag graphic branche
	this.instance = new lib.hastaggraphicbranche();
	this.instance.parent = this;
	this.instance.setTransform(37.4,51.9,1.084,0.995,0,-78.3,102.1,0.1,4.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({x:6.5,y:50.1},17,cjs.Ease.quartOut).to({_off:true},5).wait(3).to({_off:false},0).to({_off:true},4).wait(2).to({_off:false,x:26.5},0).to({_off:true},5).wait(28));

	// hastag graphic branche
	this.instance_1 = new lib.hastaggraphicbranche();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-41.6,49.9,1.084,0.995,0,-78.3,102.1,0.1,4.6);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off:false},0).to({x:-7.4,y:49.8},18,cjs.Ease.quartOut).to({_off:true},4).wait(5).to({_off:false},0).to({_off:true},4).wait(31));

	// hastag graphic branche
	this.instance_2 = new lib.hastaggraphicbranche();
	this.instance_2.parent = this;
	this.instance_2.setTransform(2.7,21.7,1,1,0,0,0,0,4.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({x:-2.3,y:58.7},13,cjs.Ease.quartOut).to({_off:true},11).wait(3).to({_off:false,scaleX:2.33,scaleY:1.03,skewX:-12.8,x:25.5},0).to({_off:true},4).wait(33));

	// hastag graphic branche
	this.instance_3 = new lib.hastaggraphicbranche();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-7.3,77.7,1,1,0,0,0,0,4.6);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({x:0.7,y:41.7},14,cjs.Ease.quartOut).to({_off:true},4).wait(7).to({_off:false,regX:0.1,scaleX:1.98,scaleY:1.01,skewX:-9.8,x:19.9,y:49.7},0).to({_off:true},4).wait(4).to({_off:false,regX:0,scaleX:1,scaleY:1,skewX:0,x:0.7,y:41.7},0).to({_off:true},4).wait(21));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-18.1,17.1,64.6,57.7);


(lib.cta0 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{rollOver:1,rollOut:16});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_15 = function() {
		this.stop();
	}
	this.frame_30 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(15).call(this.frame_15).wait(15).call(this.frame_30).wait(1));

	// Calque_5
	this.instance = new lib.cta1_back();
	this.instance.parent = this;
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({_off:false},0).to({alpha:1},14).wait(1).to({alpha:0},13).wait(2));

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.008)").s().p("Ao0EsIAApXIRpAAIAAJXg");
	this.shape.setTransform(56.5,30);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(31));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,113,60);


(lib.chiencoeuranim = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.chien_coeur();
	this.instance.parent = this;
	this.instance.setTransform(1047.3,77.3,0.556,0.556,0,0,0,0.1,276.5);

	this.instance_1 = new lib.chien_coeur();
	this.instance_1.parent = this;
	this.instance_1.setTransform(767.3,77.3,0.556,0.556,0,0,0,0.1,276.5);

	this.instance_2 = new lib.chien_coeur();
	this.instance_2.parent = this;
	this.instance_2.setTransform(1047.3,77.3,0.556,0.556,0,0,0,0.1,276.5);

	this.instance_3 = new lib.chien_coeur();
	this.instance_3.parent = this;
	this.instance_3.setTransform(767.3,77.3,0.556,0.556,0,0,0,0.1,276.5);

	this.instance_4 = new lib.chien_coeur();
	this.instance_4.parent = this;
	this.instance_4.setTransform(1047.3,77.3,0.556,0.556,0,0,0,0.1,276.5);

	this.instance_5 = new lib.chien_coeur();
	this.instance_5.parent = this;
	this.instance_5.setTransform(767.3,77.3,0.556,0.556,0,0,0,0.1,276.5);

	this.instance_6 = new lib.chien_coeur();
	this.instance_6.parent = this;
	this.instance_6.setTransform(1047.3,77.3,0.556,0.556,0,0,0,0.1,276.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance,p:{x:1047.3,y:77.3}}]}).to({state:[{t:this.instance_2,p:{x:1047.3,y:77.3}},{t:this.instance_1,p:{x:767.3,y:77.3}},{t:this.instance,p:{x:901.3,y:-114.7}}]},3).to({state:[{t:this.instance_4,p:{x:1047.3}},{t:this.instance_3,p:{x:767.3}},{t:this.instance_2,p:{x:493.2,y:77.3}},{t:this.instance_1,p:{x:621.2,y:-114.7}},{t:this.instance,p:{x:901.3,y:-114.7}}]},4).to({state:[{t:this.instance_6},{t:this.instance_5},{t:this.instance_4,p:{x:493.2}},{t:this.instance_3,p:{x:213.2}},{t:this.instance_2,p:{x:621.2,y:-114.7}},{t:this.instance_1,p:{x:341.2,y:-114.7}},{t:this.instance,p:{x:901.3,y:-114.7}}]},5).to({state:[{t:this.instance_3,p:{x:493.2}},{t:this.instance_2,p:{x:213.2,y:77.3}},{t:this.instance_1,p:{x:621.2,y:-114.7}},{t:this.instance,p:{x:341.2,y:-114.7}}]},5).to({state:[{t:this.instance_1,p:{x:213.2,y:77.3}},{t:this.instance,p:{x:341.2,y:-114.7}}]},7).to({state:[]},5).wait(3));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(942.3,-76.3,210,307.2);


(lib.Btn_next = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"rollOver":55,"rollOut":70});

	// timeline functions:
	this.frame_54 = function() {
		this.stop();
	}
	this.frame_69 = function() {
		this.stop();
	}
	this.frame_84 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(54).call(this.frame_54).wait(15).call(this.frame_69).wait(15).call(this.frame_84).wait(1));

	// Calque_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.008)").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape.setTransform(0,29.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(85));

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgIAqIAAhDIgYAAIAAgQIBBAAIAAAQIgXAAIAABDg");
	this.shape_1.setTransform(42.9,29.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AAUAqIgngzIAAAzIgTAAIAAhTIASAAIAoA1IAAg1IASAAIAABTg");
	this.shape_2.setTransform(26.3,29.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAZAqIgHgSIgjAAIgHASIgUAAIAkhTIARAAIAkBTgAgKAIIAUAAIgKgYg");
	this.shape_3.setTransform(8.9,29.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgHAqIgihTIAVAAIAUA0IAVg0IAVAAIgiBTg");
	this.shape_4.setTransform(-7.7,29.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgIAqIAAhTIARAAIAABTg");
	this.shape_5.setTransform(-21.8,29.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgYAgQgLgKAAgRIAAguIATAAIAAAtQAAAKAEAGQAFAGAHgBQAIABAEgGQAFgGABgKIAAgtIASAAIAAAuQAAASgKAJQgKALgQgBQgOABgKgLg");
	this.shape_6.setTransform(-36.2,29.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AggAcIALgNQANAMALAAQAFAAADgCQADgCAAgFQAAgDgDgCQgDgCgJgDQgOgEgHgEQgHgGAAgMQAAgMAJgGQAIgHAMAAQAJABAIADQAIACAHAGIgKANQgLgIgLAAQgEAAgDADQgDABAAAEQAAAEAEACQADACALADQAMADAHAFQAGAGAAALQAAAMgIAGQgJAIgOgBQgSABgQgQg");
	this.shape_7.setTransform(-52.6,29.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#333333").ss(1,1,1).p("AxpAAMAjTAAA");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(85));

	// Layer_2
	this.instance = new lib.Btn_cliquable_area();
	this.instance.parent = this;
	this.instance.setTransform(0,29.5,1,1,0,0,0,0,30);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({alpha:0.012},46).wait(6).to({y:30},0).to({alpha:1},14).wait(1).to({alpha:0.012},13).wait(2));

	// BG
	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("rgba(0,0,0,0.098)").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape_9.setTransform(0,29.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_9).wait(85));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-114,-1,228,60.5);


(lib.Btn_detail = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_54 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(54).call(this.frame_54).wait(1));

	// lines
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#333333").ss(1,1,1).p("AAAkoIDSAAIOYAAAAAEtIAApVAxpkoIRpAAADSkoIAAgE");
	this.shape.setTransform(0,29.7);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(55));

	// Isolation Mode
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#FFFFFF").ss(2,1,1).p("ABYgmIhYBNIhXhN");
	this.shape_1.setTransform(59.2,27);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhYhLICxAAIAACXIixAAg");
	this.shape_2.setTransform(59,30.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AhJBZQgJAAgDgDQgDgEAAgIIAAiTQAAgIADgEQAEgDAIAAICUAAQAIAAADADQADAEAAAIIAACTQAAAIgDAEQgDADgJAAgAgUBAIAbAAIAAgMIACgoQAAgGAEgEQADgDAGAAQALgBABAOIACAzIAcAAIAAg+QgCgZgcgCQgRAAgLAMIgBgJIgZAAgAg+A/IAaAAIAAhWIgaAAgAg9g+QgFAFgBAHQAAAIAFAFQAFAGAIAAQAHAAAFgFQAFgFABgHQAAgIgFgFQgFgGgIAAQgHAAgFAFg");
	this.shape_3.setTransform(-57.5,30.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(55));

	// zones
	this.cta1 = new lib.cta0();
	this.cta1.name = "cta1";
	this.cta1.parent = this;
	this.cta1.setTransform(56.5,29.5,1,1,0,0,0,56.5,30);

	this.timeline.addTween(cjs.Tween.get(this.cta1).wait(55));

	// zones
	this.cta0 = new lib.cta0();
	this.cta0.name = "cta0";
	this.cta0.parent = this;
	this.cta0.setTransform(-56.5,29.5,1,1,0,0,0,56.5,30);

	this.timeline.addTween(cjs.Tween.get(this.cta0).wait(55));

	// Layer_2
	this.instance = new lib.Btn_cliquable_area();
	this.instance.parent = this;
	this.instance.setTransform(0,29.5,1,1,0,0,0,0,30);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({alpha:0.012},46).wait(6));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-114,-1.4,228,62.2);


(lib.bloc_visuel = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":63});

	// timeline functions:
	this.frame_62 = function() {
		this.stop();
	}
	this.frame_89 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(62).call(this.frame_62).wait(27).call(this.frame_89).wait(1));

	// masque VISUEL (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("EgjcBDvMAAAiHdMBG5AAAMAAACHdg");
	mask.setTransform(-227.1,-19);

	// VISUEL
	this.instance = new lib.prject_illustration();
	this.instance.parent = this;
	this.instance.setTransform(552.5,26.6,1,1,6,0,0,0.1,316.5);
	this.instance.alpha = 0.012;

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:-0.1,regY:316.6,rotation:-3,x:-229.4,y:6.6,alpha:0.398},20,cjs.Ease.cubicOut).to({regX:-0.2,regY:316.4,scaleX:0.93,scaleY:0.93,rotation:0,x:107.9,y:14.9,alpha:0.301},28,cjs.Ease.cubicInOut).wait(20).to({regX:0,regY:316.6,scaleX:1,scaleY:1,rotation:-12.7,x:-719.5,y:6.6,alpha:0.012},20).to({_off:true},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-155.9,-452.5,155.7,867);


(lib.bloc_video = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{open:1,"close":28});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_27 = function() {
		this.stop();
	}
	this.frame_28 = function() {
		this.croix_1.gotoAndPlay("close");
		this.croix_2.gotoAndPlay("close");
		//this.gif_mc.gotoAndPlay("close");
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(27).call(this.frame_27).wait(1).call(this.frame_28).wait(25));

	// Layer_3
	this.instance = new lib.masque_generique();
	this.instance.parent = this;
	this.instance.setTransform(3,-0.5,0.038,14.292,0,0,0,78.4,17.1);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(6).to({_off:false},0).wait(2).to({regX:77.9,scaleX:3.7,x:287.9},10,cjs.Ease.quartIn).to({regX:81.8,scaleX:0.01,x:579.6},9,cjs.Ease.quartOut).wait(1).to({regX:77.9,scaleX:3.7,x:287.9},6,cjs.Ease.quartIn).to({regX:78.4,scaleX:0.01,x:1},7,cjs.Ease.quartOut).to({_off:true},1).wait(11));

	// VIDEO
	this.videoContainer = new lib.video();
	this.videoContainer.name = "videoContainer";
	this.videoContainer.parent = this;
	this.videoContainer.setTransform(23.7,158.3,0.659,0.659,0,0,0,0.3,480.2);
	this.videoContainer.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.videoContainer).wait(18).to({alpha:1},0).to({_off:true},25).wait(10));

	// geometrie croix
	this.croix_2 = new lib.video_frame_croix();
	this.croix_2.name = "croix_2";
	this.croix_2.parent = this;
	this.croix_2.setTransform(40.3,180.8,1,1,180,0,0,188,96);

	this.croix_1 = new lib.video_frame_croix();
	this.croix_1.name = "croix_1";
	this.croix_1.parent = this;
	this.croix_1.setTransform(414,180.5,1,1,0,180,0,188,96);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.croix_1},{t:this.croix_2}]},17).to({state:[]},33).wait(3));

	// geometrie angle
	this.instance_1 = new lib.video_frame_angle();
	this.instance_1.parent = this;
	this.instance_1.setTransform(38.6,-257.9,1,1,0,0,180,188.1,96);

	this.instance_2 = new lib.video_frame_angle();
	this.instance_2.parent = this;
	this.instance_2.setTransform(231.1,-42.7,1,1,0,180,0,3.6,319.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_2},{t:this.instance_1}]},3).to({state:[]},40).wait(10));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(23.5,-158,395.2,296.4);


(lib.bloc_NEXT_btn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":40});

	// timeline functions:
	this.frame_39 = function() {
		this.stop();
	}
	this.frame_56 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(39).call(this.frame_39).wait(17).call(this.frame_56).wait(1));

	// CTA DETAIL
	this.btnNext = new lib.Btn_next();
	this.btnNext.name = "btnNext";
	this.btnNext.parent = this;
	this.btnNext.setTransform(-340.5,411.5,1,1,0,0,0,0,26.5);

	this.timeline.addTween(cjs.Tween.get(this.btnNext).to({y:350.5},39,cjs.Ease.quartInOut).wait(2).to({y:411.5},14,cjs.Ease.quartInOut).to({_off:true},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-454,384.5,227,60);


(lib.bloc_lignes = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":166,close_lines:209});

	// timeline functions:
	this.frame_165 = function() {
		this.stop();
	}
	this.frame_208 = function() {
		this.stop();
	}
	this.frame_229 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(165).call(this.frame_165).wait(43).call(this.frame_208).wait(21).call(this.frame_229).wait(1));

	// ligne
	this.instance = new lib.ligne();
	this.instance.parent = this;
	this.instance.setTransform(-454,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(166).to({x:-883},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:-454},0).wait(21));

	// ligne
	this.instance_1 = new lib.ligne();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-227,406.5,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(119).to({y:320.5},40,cjs.Ease.quartInOut).wait(7).to({y:406.5},0).to({x:-883.2},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:-227,y:320.5},0).to({y:406.5},17,cjs.Ease.quartInOut).wait(4));

	// ligne
	this.instance_2 = new lib.ligne();
	this.instance_2.parent = this;
	this.instance_2.setTransform(0,406.5,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(166).to({x:-883.4},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:0},0).wait(21));

	// ligne
	this.instance_3 = new lib.ligne();
	this.instance_3.parent = this;
	this.instance_3.setTransform(227,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(124).to({y:320},40,cjs.Ease.quartInOut).to({_off:true},2).wait(43).to({_off:false},0).to({y:406},17,cjs.Ease.quartInOut).wait(4));

	// ligne
	this.instance_4 = new lib.ligne();
	this.instance_4.parent = this;
	this.instance_4.setTransform(454.1,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(166).to({x:-883.4},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:454.1},0).wait(21));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-455.4,-174.5,910.5,988);


(lib.bloc_hashtag = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_61 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(61).call(this.frame_61).wait(2));

	// Layer_1
	this.instance = new lib.hashtag_graphic("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(-456,243.2,2.317,2.317,0,0,0,-0.1,50.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true},61).wait(2));

	// Layer_5 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_28 = new cjs.Graphics().p("A2QBpIAAjRMAshAAAIAADRg");
	var mask_graphics_32 = new cjs.Graphics().p("A1ZDSIAAmjMAqzAAAIAAGjg");
	var mask_graphics_40 = new cjs.Graphics().p("A4NExIAAphMAwbAAAIAAJhg");
	var mask_graphics_46 = new cjs.Graphics().p("Egl9AXUIAAtBMAwbAAAIAANBg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(28).to({graphics:mask_graphics_28,x:-343.5,y:265.5}).wait(4).to({graphics:mask_graphics_32,x:-349,y:255}).wait(8).to({graphics:mask_graphics_40,x:-331,y:245.6}).wait(6).to({graphics:mask_graphics_46,x:-243,y:149.2}).wait(17));

	// Layer_3
	this.instance_1 = new lib.txt_hashtag();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-421.5,244.1,1,1,0,0,0,0,31.6);
	this.instance_1._off = true;

	var maskedShapeInstanceList = [this.instance_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(28).to({_off:false},0).to({_off:true},34).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-497.8,166.1,149.7,129.4);


(lib.bloc_CTA = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":17});

	// timeline functions:
	this.frame_16 = function() {
		this.stop();
	}
	this.frame_33 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(16).call(this.frame_16).wait(17).call(this.frame_33).wait(1));

	// CTA DETAIL
	this.ctas = new lib.Btn_detail();
	this.ctas.name = "ctas";
	this.ctas.parent = this;
	this.ctas.setTransform(-340.5,411.5,1,1,0,0,0,0,26.5);

	this.timeline.addTween(cjs.Tween.get(this.ctas).to({y:350.5},16,cjs.Ease.quartInOut).wait(2).to({y:411.5},14,cjs.Ease.quartInOut).to({_off:true},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-454,384.1,227,61.2);


(lib.blocInfostextes = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":80});

	// timeline functions:
	this.frame_79 = function() {
		this.stop();
	}
	this.frame_120 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(79).call(this.frame_79).wait(41).call(this.frame_120).wait(1));

	// Bloc Hashtag
	this.instance = new lib.bloc_hashtag();
	this.instance.parent = this;
	this.instance.setTransform(-391.5,211.2,0.9,0.9,0,0,0,-412.9,244.7);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(62).to({_off:false},0).to({_off:true},18).wait(41));

	// masque Titre
	this.instance_1 = new lib.masque_generique();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-527.9,-13.9,0.007,0.471,0,0,0,0,17.2);
	this.instance_1._off = true;
	this.instance_1.filters = [new cjs.ColorFilter(0, 0, 0, 1, 204, 153, 0, 0)];
	this.instance_1.cache(-2,-2,161,38);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(34).to({_off:false},0).to({regX:0.5,scaleX:1.92,x:-527.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.01,x:-228.9},10,cjs.Ease.quartInOut).to({_off:true},1).wait(26).to({_off:false},0).to({regX:0.5,scaleX:1.92,x:-527.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.01,x:-528.7},10,cjs.Ease.quartInOut).to({_off:true},1).wait(21));

	// sous titre coloré
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CC9900").s().p("AAbA4Ig0hFIAABFIgaAAIAAhvIAYAAIA2BHIAAhHIAZAAIAABvg");
	this.shape.setTransform(-308.4,-14.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#CC9900").s().p("AgpApQgRgQAAgZQAAgXARgRQARgRAYAAQAZAAARARQARARAAAXQAAAZgRAQQgRARgZAAQgYAAgRgRgAgXgYQgJAKAAAOQAAAPAJALQAKAKANAAQAOAAAKgKQAJgLAAgPQAAgOgJgKQgKgLgOAAQgNAAgKALg");
	this.shape_1.setTransform(-333.5,-14.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#CC9900").s().p("AgjApIANgSQAKALALAAQAFAAADgEQAEgFAAgHIAAg0IggAAIAAgVIA5AAIAABIQAAATgKAKQgLALgPgBQgUAAgPgPg");
	this.shape_2.setTransform(-357,-14.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#CC9900").s().p("AAaA4Ig0hFIAABFIgYAAIAAhvIAWAAIA2BHIAAhHIAZAAIAABvg");
	this.shape_3.setTransform(-379.5,-14.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CC9900").s().p("AAiA4IgKgYIgvAAIgKAYIgbAAIAxhvIAXAAIAxBvgAgNAKIAbAAIgOgfg");
	this.shape_4.setTransform(-404,-14.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#CC9900").s().p("AglA4IAAhvIBLAAIAAAWIgyAAIAAAYIAwAAIAAAVIgwAAIAAAsg");
	this.shape_5.setTransform(-426.6,-14.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#CC9900").s().p("AglA4IAAhvIBLAAIAAAWIgyAAIAAAYIAwAAIAAAVIgwAAIAAAsg");
	this.shape_6.setTransform(-448.4,-14.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#CC9900").s().p("AAiA4IgKgYIgvAAIgKAYIgbAAIAxhvIAXAAIAxBvgAgNAKIAbAAIgOgfg");
	this.shape_7.setTransform(-471.5,-14.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#CC9900").s().p("AAWA4IAAgsIgrAAIAAAsIgZAAIAAhvIAZAAIAAAvIArAAIAAgvIAZAAIAABvg");
	this.shape_8.setTransform(-495.6,-14.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#CC9900").s().p("AgiApQgRgQgBgYQABgZARgQQAQgRAZAAQAbAAATAVIgQASQgMgPgRAAQgNAAgLAJQgKAJABAPQAAAPAJAKQAJAKAMAAQATAAAMgPIAQARQgUAVgZAAQgZAAgQgRg");
	this.shape_9.setTransform(-519.4,-14.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#CC9900").s().p("AgdAgQgNgMAAgUQAAgTANgNQANgMARABQARAAANAKQAMALAAASIAAANIg+AAQACAGAGAFQAGAFAHAAQANAAAIgJIANAOQgOAOgUABQgTAAgMgNgAgLgTQgGAEgBAIIAlAAQgBgJgFgDQgFgFgHAAQgGAAgGAFg");
	this.shape_10.setTransform(-400.9,-34.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#CC9900").s().p("AgLA8IAAhUIAXAAIAABUgAgJgkQgEgDAAgHQAAgGAEgEQAEgDAFAAQAGAAAEADQAEAEAAAGQAAAHgEADQgEAFgGAAQgFAAgEgFg");
	this.shape_11.setTransform(-420.2,-36.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#CC9900").s().p("AgLA7IAAh1IAXAAIAAB1g");
	this.shape_12.setTransform(-436.8,-36.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#CC9900").s().p("AgcAlQgKgJAAgMQAAgOAKgGQAJgGARAAIARAAIAAAAQAAgOgPgBQgFABgHACQgGADgGADIgLgPQARgMAWAAQAPgBAKAJQAKAHAAARIAAA2IgXAAIAAgLQgJAMgNABQgNgBgJgHgAgNAPQAAAEADADQADADAGAAQAGgBAFgEQAFgDAAgHIAAgEIgPAAQgNAAAAAJg");
	this.shape_13.setTransform(-456.1,-34.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#CC9900").s().p("AgYAsIAAhVIAYAAIAAALQADgFAIgEQAHgEAGAAIABAXIgFAAQgLAAgFAHQgEAIAAALIAAAmg");
	this.shape_14.setTransform(-475.6,-34.9);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#CC9900").s().p("AggAfQgNgMABgTQgBgSANgNQAOgNASABQATgBANANQANANABASQgBATgNAMQgNANgTABQgSgBgOgNgAgPgQQgFAHgBAJQABALAFAGQAHAHAIAAQAJAAAHgHQAFgGAAgLQAAgJgFgHQgHgHgJAAQgIAAgHAHg");
	this.shape_15.setTransform(-496.5,-34.8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#CC9900").s().p("AgiApQgRgQgBgYQABgZARgQQAQgRAZAAQAbAAATAVIgQASQgMgPgRAAQgNAAgLAJQgKAJABAPQAAAPAJAKQAJAKAMAAQATAAAMgPIAQARQgUAVgZAAQgZAAgQgRg");
	this.shape_16.setTransform(-519.4,-36.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},46).to({state:[]},43).to({state:[]},1).wait(31));

	// masque Titre (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_17 = new cjs.Graphics().p("EgpOArJMAAAhWRMAvJAAAMAAABWRg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(17).to({graphics:mask_graphics_17,x:-263.9,y:-6.1}).wait(24).to({graphics:null,x:0,y:0}).wait(80));

	// Layer_5
	this.instance_2 = new lib.lettres_FAT();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-433.8,-73,1,1,0,0,0,0,186.8);
	this.instance_2._off = true;

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#CC9900").s().p("AjiFdIAAq5ICcAAIAAIvIEpAAIAACKg");
	this.shape_17.setTransform(-234.7,131.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#CC9900").s().p("AjiFeIAAq6ICcAAIAAIuIEpAAIAACMg");
	this.shape_18.setTransform(-465.1,7.9);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#CC9900").s().p("AjWEPQhVhSAAiXIAAmGICdAAIAAGBQAABUAnAxQAnAwBBAAQBBAAAngwQAngxgBhUIAAmBICdAAIAAGGQAACYhVBSQhUBRiCAAQiDAAhUhSg");
	this.shape_19.setTransform(-385.6,-115.6);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#CC9900").s().p("AjvFdIAAq5IHfAAIAACIIlDAAIAACYIEzAAIAACHIkzAAIAAESg");
	this.shape_20.setTransform(-502.2,-116);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#CC9900").s().p("AhNFdIAAkTIjymmICpAAICWEEICXkEICpAAIjyGmIAAETg");
	this.shape_21.setTransform(-333.3,106.8);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#CC9900").s().p("AByFeIibjgIhsAAIAADgIicAAIAAq7IEJAAQCjAABFA3QBFA3AAB7QAACmiFAxICyD7gAiVgIIByAAQBQAAAcgaQAdgZAAg5QAAg4gegWQgdgUhLgBIh1AAg");
	this.shape_22.setTransform(-230.4,-17.1);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#CC9900").s().p("AkEECQhqhoAAiaQAAiZBqhoQBqhnCaAAQCbAABqBnQBqBoAACZQAACahqBoQhqBnibAAQiaAAhqhngAiSieQg8BCAABcQAABeA8BCQA8BBBWAAQBYAAA8hBQA8hCAAheQAAhcg8hCQg8hChYAAQhWAAg8BCg");
	this.shape_23.setTransform(-476.5,-17.6);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#CC9900").s().p("AhNFeIAAo0IjGAAIAAiHIInAAIAACHIjGAAIAAI0g");
	this.shape_24.setTransform(-334.8,-141);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#CC9900").s().p("AkUDvIBchzQBxBkBfgBQArAAAZgSQAYgTAAgfQABgfgagTQgagThNgSQh6geg4gtQg4gugBhkQAAhkBIg2QBIg2BqAAQBFAABGAZQBHAYA0ArIhQByQhbhFhgAAQgmABgXASQgXASABAfQgBAfAcARQAbATBjAYQBjAYA3AwQA3AyAABdQAABehGA7QhHA7hyAAQilAAiFh7g");
	this.shape_25.setTransform(-486.4,-141.4);

	var maskedShapeInstanceList = [this.instance_2,this.shape_17,this.shape_18,this.shape_19,this.shape_20,this.shape_21,this.shape_22,this.shape_23,this.shape_24,this.shape_25];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_2}]},17).to({state:[{t:this.instance_2}]},14).to({state:[{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17}]},1).to({state:[{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21}]},5).to({state:[]},4).to({state:[]},39).wait(41));
	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(17).to({_off:false},0).to({x:-276.8},14,cjs.Ease.quartOut).to({_off:true},1).wait(89));

	// masque Titre
	this.instance_3 = new lib.masque_generique();
	this.instance_3.parent = this;
	this.instance_3.setTransform(221.1,-163.9,0.013,0.471,0,0,0,0,17.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({regX:0.5,scaleX:1.92,x:-75.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.01,x:-68.7},10,cjs.Ease.quartInOut).to({_off:true},1).wait(101));

	// Layer_7
	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#FFFFFF").ss(1,1,1).p("EAAAAlzMgnugc4MAPLgutMAxHAAAMAPLAutg");
	this.shape_26.setTransform(230.7,-128.1);

	this.instance_4 = new lib.chien_coeur();
	this.instance_4.parent = this;
	this.instance_4.setTransform(-241.4,-122,0.556,0.556,0,0,0,0.1,276.5);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#FFFFFF").ss(1,1,1).p("AW3ISI4mONI1HzBILk58IcQC+g");
	this.shape_27.setTransform(-22.7,68.8);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#FFFFFF").ss(1,1,1).p("AnFmSICWm8ISbU+I7XFfIAAgB");
	this.shape_28.setTransform(-240,-49.6);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AqGJxIGnzhIAAAAINmPeI0MEDg");
	this.shape_29.setTransform(-262.9,-27.5);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#FFFFFF").ss(1,1,1).p("ALnNZI3NtWIXJtbg");
	this.shape_30.setTransform(-577,114.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_26}]},17).to({state:[{t:this.instance_4}]},4).to({state:[]},9).to({state:[{t:this.shape_27}]},63).to({state:[{t:this.shape_29},{t:this.shape_28}]},4).to({state:[{t:this.shape_30}]},3).to({state:[]},6).to({state:[]},1).wait(14));

	// Layer_8
	this.instance_5 = new lib.chiencoeuranim();
	this.instance_5.parent = this;
	this.instance_5.setTransform(16,35.1,0.324,0.324,0,0,0,1047.2,77.4);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(8).to({_off:false},0).to({_off:true},63).wait(50));

	// masque Titre
	this.instance_6 = new lib.masque_generique();
	this.instance_6.parent = this;
	this.instance_6.setTransform(-228.9,-13.9,0.013,0.471,0,0,0,0,17.2);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(4).to({_off:false},0).to({regX:0.5,scaleX:1.92,x:-527.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.01,x:-528.7},10,cjs.Ease.quartInOut).to({_off:true},1).wait(14).to({_off:false,scaleX:0.01,x:-228.9,y:-33.9},0).to({regX:0.5,scaleX:1.92,x:-527.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.01,x:-528.7},10,cjs.Ease.quartInOut).to({_off:true},1).wait(63));

	// Layer_10
	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#CC9900").s().p("AgLA4IAAgsIgnhDIAbAAIAXAqIAYgqIAbAAIgmBDIAAAsg");
	this.shape_31.setTransform(-450.4,-14.1);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#CC9900").s().p("AgoA4IAAhvIBQAAIAAAXIg3AAIAAAWIAxAAIAAAUIgxAAIAAAYIA4AAIAAAWg");
	this.shape_32.setTransform(-504.6,-14.1);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#CC9900").s().p("AgJA0QgEgEAAgGQAAgFAEgFQAEgEAFAAQAGAAAEAEQAEAFAAAFQAAAGgEAEQgEAFgGAAQgFAAgEgFgAgJAVIgEg5IAAgUIAbAAIAAAUIgFA5g");
	this.shape_33.setTransform(-349.9,-14.1);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#CC9900").s().p("AgpApQgRgQAAgZQAAgXARgRQARgRAYAAQAZAAARARQARARAAAXQAAAZgRAQQgRARgZAAQgYAAgRgRgAgXgYQgJAKAAAOQAAAPAJALQAKAKANAAQAOAAAKgKQAJgLAAgPQAAgOgJgKQgKgLgOAAQgNAAgKALg");
	this.shape_34.setTransform(-387.2,-14.2);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#CC9900").s().p("AgjA4IAAhvIAZAAIAABYIAuAAIAAAXg");
	this.shape_35.setTransform(-442.3,-14.1);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#CC9900").s().p("AAWA4IAAgsIgrAAIAAAsIgZAAIAAhvIAZAAIAAAvIArAAIAAgvIAZAAIAABvg");
	this.shape_36.setTransform(-487.8,-14.1);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#CC9900").s().p("AgjA4IAAhvIAZAAIAABYIAuAAIAAAXg");
	this.shape_37.setTransform(-474.7,-14.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_32,p:{x:-504.6}},{t:this.shape_31}]},13).to({state:[{t:this.shape_36,p:{x:-487.8}},{t:this.shape_32,p:{x:-464.2}},{t:this.shape_35,p:{x:-442.3}},{t:this.shape_34,p:{x:-387.2}},{t:this.shape_33,p:{x:-349.9}}]},5).to({state:[{t:this.shape_36,p:{x:-520.2}},{t:this.shape_32,p:{x:-496.6}},{t:this.shape_37},{t:this.shape_35,p:{x:-453.6}},{t:this.shape_34,p:{x:-430.9}},{t:this.shape_33,p:{x:-393.6}}]},5).to({state:[]},11).to({state:[]},46).wait(41));

	// Layer_11
	this.instance_7 = new lib.masqueTexte("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(-340,105.8,0.85,0.85,0,0,0,0.1,108.8);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(37).to({_off:false},0).to({_off:true},40).wait(3).to({_off:false,rotation:180},0).to({_off:true},40).wait(1));

	// Layer_12
	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("AhJBbQgXgUAAghQAAghAYgPQAYgQApAAIAsAAIAAgCQAAgigkAAQgPAAgRAGQgSAGgMAKIgbgnQArgfA1AAQAnAAAZAUQAZATAAArIAACIIg4AAIAAgaQgXAdgiAAQgiAAgXgUgAgkAmQAAALAIAHQAKAGAQAAQAOAAAMgKQANgKAAgQIAAgLIglAAQgkAAAAAXg");
	this.shape_38.setTransform(-298.5,133.4);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AgdCXIAAjWIA7AAIAADWgAgYhaQgKgKAAgPQAAgPAKgKQAKgKAOAAQAPAAAKAKQAKAKAAAPQAAAPgKAKQgKAKgPAAQgOAAgKgKg");
	this.shape_39.setTransform(-315.8,129.1);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("AhQB2QgeggAAgwQAAgwAdgeQAegeAngBQAmAAAZAcIAAhqIA8AAIAAEoIg8AAIAAgcQgaAfglAAQgmAAgegggAgigDQgPARAAAaQAAAaAPARQAQAQATAAQAVAAAPgQQAOgRAAgaQAAgagOgRQgPgSgVAAQgUAAgPASg");
	this.shape_40.setTransform(-335.5,129.5);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAhgeQAggeArAAQAtAAAfAbQAfAbAAAuIAAAfIicAAQADASAQALQAPALATAAQAfAAAVgVIAhAlQgkAigxAAQgwAAgggegAgdgyQgPALgCATIBdAAQgCgUgMgKQgNgKgSAAQgRAAgOAKg");
	this.shape_41.setTransform(-360.5,133.4);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("ABtBtIAAh0QAAgygkAAQgSAAgNANQgOANAAAZIAABzIg7AAIAAh0QAAgagIgMQgIgMgRAAQgSAAgNANQgNANAAAZIAABzIg8AAIAAjWIA8AAIAAAYQAZgbAfAAQAVAAAQALQAQANAIARQANgUAVgLQAVgKAVAAQAmAAAXAWQAXAXAAAqIAACCg");
	this.shape_42.setTransform(-392.5,133.3);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("AhGAbIAAg1ICNAAIAAA1g");
	this.shape_43.setTransform(-422,132.2);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFFFFF").s().p("AgdCXIAAjWIA7AAIAADWgAgYhaQgKgKAAgPQAAgPAKgKQAKgKAOAAQAPAAAKAKQAKAKAAAPQAAAPgKAKQgKAKgPAAQgOAAgKgKg");
	this.shape_44.setTransform(-437.7,129.1);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFFFFF").s().p("Ag9BtIAAjWIA8AAIAAAdQAKgOASgJQARgJASAAIAAA4IgLAAQgbAAgNATQgMATAAAeIAABdg");
	this.shape_45.setTransform(-450.9,133.3);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FFFFFF").s().p("AhMBXQgYgWAAgsIAAiBIA9AAIAAB0QAAAxAkAAQARAAANgMQANgNAAgZIAAhzIA9AAIAADWIg9AAIAAgcQgWAfgiAAQgkAAgYgWg");
	this.shape_46.setTransform(-472.9,133.6);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFFFFF").s().p("AgdCUIAAkoIA7AAIAAEog");
	this.shape_47.setTransform(-491.3,129.4);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FFFFFF").s().p("AhuCUIAAkkIA8AAIAAAYQAcgbAkAAQAmAAAdAfQAeAfAAAwQAAAvgeAgQgdAggnAAQgnAAgYggIAABqgAgkhQQgPASAAAaQAAAaAPAQQAPARAUAAQAUAAAQgRQAPgQAAgaQAAgZgPgTQgPgRgUAAQgVAAgPARg");
	this.shape_48.setTransform(-509.9,137.2);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FFFFFF").s().p("AgqBmQgagIgVgRIAagmQAjAaAkAAQAMAAAGgEQAIgFgBgHQAAgIgJgGQgKgHgQgFIgcgKQgJgEgLgIQgZgOAAgeQAAgdAYgTQAYgTAlAAQAmAAAmAaIgXAoQgcgUgcAAQgaAAAAAQQAAAIAJAFQAIAEAUAHIAdAKQAJADAMAHQAWANAAAgQAAAggXATQgYATgkAAQgXAAgagJg");
	this.shape_49.setTransform(-253,84.7);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FFFFFF").s().p("AApBtIAAh4QAAgyglAAQgRAAgNANQgNANAAAZIAAB3Ig9AAIAAjWIA9AAIAAAYQAbgbAgAAQAiAAAYAYQAXAYgBAnIAACCg");
	this.shape_50.setTransform(-275.7,84.5);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FFFFFF").s().p("AhQBPQghggAAgvQAAguAhggQAgggAwAAQAxAAAgAgQAhAgAAAuQAAAvghAgQggAggxAAQgwAAgggggAglgqQgPARAAAZQAAAbAPAQQAPARAWAAQAXAAAPgRQAPgQAAgbQAAgZgPgRQgPgRgXAAQgWAAgPARg");
	this.shape_51.setTransform(-301.6,84.7);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FFFFFF").s().p("AgdCXIAAjWIA7AAIAADWgAgYhaQgKgKAAgPQAAgPAKgKQAKgKAOAAQAPAAAKAKQAKAKAAAPQAAAPgKAKQgKAKgPAAQgOAAgKgKg");
	this.shape_52.setTransform(-320.4,80.3);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FFFFFF").s().p("AgfB5QgWgUAAgkIAAhiIgZAAIAAgrIAZAAIAAhAIA7AAIAABAIA1AAIAAArIg1AAIAABgQABAMAGAHQAGAHAJAAQARAAAMgPIAWAqQgcAZgfAAQgeAAgVgUg");
	this.shape_53.setTransform(-334.4,81.6);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FFFFFF").s().p("AhEBQQgigeAAgyQAAgwAigfQAigfAtAAQAXAAAYAKQAYAJASATIgfApQgLgNgPgGQgOgHgPAAQgXAAgRAPQgRAQAAAaQAAAbARAQQARAPAWAAQAeAAAYgeIAkAoQgqArgwAAQgwAAghgfg");
	this.shape_54.setTransform(-355.2,84.7);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FFFFFF").s().p("AhJBbQgXgUAAghQAAghAYgPQAYgQAqAAIArAAIAAgCQAAgigkAAQgPAAgRAGQgSAGgLAKIgbgnQAqgfA1AAQAoAAAYAUQAZATAAArIAACIIg4AAIAAgaQgXAdghAAQgjAAgXgUgAgkAmQAAALAIAHQAKAGAPAAQAPAAAMgKQANgKAAgQIAAgLIglAAQgkAAAAAXg");
	this.shape_55.setTransform(-379.4,84.7);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FFFFFF").s().p("Ag9BtIAAjWIA8AAIAAAdQAKgOASgJQARgJASAAIAAA4IgLAAQgbAAgNATQgMATAAAeIAABdg");
	this.shape_56.setTransform(-398.2,84.5);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAggeQAhgeArAAQAtAAAfAbQAfAbAAAuIAAAfIicAAQADASAPALQAQALATAAQAfAAAVgVIAiAlQglAigxAAQgwAAgggegAgdgyQgPALgCATIBdAAQgCgUgNgKQgMgKgSAAQgQAAgPAKg");
	this.shape_57.setTransform(-419.5,84.7);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#FFFFFF").s().p("AggB5QgVgUAAgkIAAhiIgZAAIAAgrIAZAAIAAhAIA8AAIAABAIAzAAIAAArIgzAAIAABgQgBAMAHAHQAGAHAJAAQARAAALgPIAXAqQgcAZgfAAQgeAAgWgUg");
	this.shape_58.setTransform(-440.2,81.6);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#FFFFFF").s().p("AApBtIAAh4QAAgyglAAQgRAAgNANQgNANAAAZIAAB3Ig9AAIAAjWIA9AAIAAAYQAagbAiAAQAhAAAXAYQAYAYAAAnIAACCg");
	this.shape_59.setTransform(-462.2,84.5);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#FFFFFF").s().p("AgdCXIAAjWIA7AAIAADWgAgYhaQgKgKAAgPQAAgPAKgKQAKgKAOAAQAPAAAKAKQAKAKAAAPQAAAPgKAKQgKAKgPAAQgOAAgKgKg");
	this.shape_60.setTransform(-481,80.3);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#FFFFFF").s().p("AgXA2IAAhqIAvAAIAABqg");
	this.shape_61.setTransform(-492,72.9);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#FFFFFF").s().p("AhQB2QgeghAAgvQAAgwAdgfQAegdAnAAQAmgBAZAcIAAhqIA8AAIAAEoIg8AAIAAgcQgaAfglAAQgmAAgegggAgigDQgPARAAAaQAAAaAPARQAQAQATAAQAVAAAPgQQAOgRAAgbQAAgagOgQQgPgSgVAAQgUAAgPASg");
	this.shape_62.setTransform(-511.1,80.8);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#FFFFFF").s().p("Ag9BtIAAjWIA8AAIAAAdQAKgOASgJQARgJASAAIAAA4IgLAAQgbAAgNATQgMATAAAeIAABdg");
	this.shape_63.setTransform(-349.9,35.8);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAggeQAhgeAsAAQAsAAAfAbQAfAbAAAuIAAAfIicAAQADASAPALQAQALATAAQAfAAAVgVIAiAlQglAigxAAQgwAAgggegAgdgyQgPALgDATIBeAAQgCgUgNgKQgMgKgRAAQgSAAgOAKg");
	this.shape_64.setTransform(-371.2,35.9);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#FFFFFF").s().p("AAoBtIAAh4QAAgygkAAQgRAAgNANQgOANAAAZIAAB3Ig8AAIAAjWIA8AAIAAAYQAbgbAiAAQAiAAAWAYQAYAYAAAnIAACCg");
	this.shape_65.setTransform(-396.3,35.8);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#FFFFFF").s().p("Ag1CQQgZgIgSgOIAXguQAdAXAhAAQAgAAAUgRQATgQAAgkQgdAggpAAQgqAAgdgdQgdgcAAgvQAAgwAegfQAegeAmAAQAlAAAaAgIAAgdIA8AAIAAC7QAAAdgKAWQgKAWgRANQghAbgtAAQgYAAgZgIgAgihVQgQAPAAAZQAAAaAPAQQAPAOAVAAQAWAAAOgOQAPgQAAgZQAAgagPgPQgOgPgVAAQgVAAgPAPg");
	this.shape_66.setTransform(-423.2,40);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#FFFFFF").s().p("AgdCXIAAjWIA7AAIAADWgAgYhaQgKgKAAgPQAAgPAKgKQAKgKAOAAQAPAAAKAKQAKAKAAAPQAAAPgKAKQgKAKgPAAQgOAAgKgKg");
	this.shape_67.setTransform(-441.8,31.6);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#FFFFFF").s().p("AgqBmQgagIgVgRIAbgmQAhAaAmAAQALAAAHgEQAGgFAAgHQABgIgKgGQgKgHgQgFIgcgKQgJgEgMgIQgYgOAAgeQAAgdAYgTQAYgTAlAAQAmAAAmAaIgWAoQgcgUgcAAQgbAAAAAQQAAAIAJAFQAJAEATAHIAdAKQAIADAMAHQAXANAAAgQAAAggYATQgXATgkAAQgXAAgagJg");
	this.shape_68.setTransform(-458.3,35.9);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAhgeQAggeArAAQAtAAAfAbQAfAbAAAuIAAAfIicAAQADASAQALQAPALATAAQAfAAAVgVIAhAlQgkAigxAAQgwAAgggegAgdgyQgPALgCATIBdAAQgCgUgMgKQgNgKgSAAQgQAAgPAKg");
	this.shape_69.setTransform(-480.6,35.9);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#FFFFFF").s().p("Ah+CMIAAkXIBjAAQBKAAAoAlQAoAkAABCQAABAgnAmQgnAmhPAAgAg/BUIAnAAQAqAAAXgVQAXgWAAgpQAAgogXgWQgXgWgvAAIgiAAg");
	this.shape_70.setTransform(-507.7,32.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38}]},57).to({state:[]},43).to({state:[]},1).wait(20));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(221.1,-172,2,16);


// stage content:
(lib.FS_coralie_mobile = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":515});

	// timeline functions:
	this.frame_129 = function() {
		this.video_mc.gotoAndPlay("open");
	}
	this.frame_139 = function() {
		//var frequency = 60;
		//stage.enableMouseOver(frequency);
		
		// EVENT DE CLIC POUR LA SUITE
		this.next_btn.addEventListener("click", fl_ClickToGoToAndPlayFromFrame.bind(this));
		this.next_btn.addEventListener("mouseover", fl_MouseOverHandler.bind(this));
		this.next_btn.addEventListener("mouseout", fl_MouseOutHandler.bind(this));
		
		function fl_ClickToGoToAndPlayFromFrame() {
			//this.gotoAndPlay("close");
		
			var event = new Event('next');
			this.dispatchEvent(event);
			event = null;
			
			this.next_btn.btnNext.gotoAndPlay('rollOut');
		
		}
		function fl_MouseOverHandler() { this.next_btn.btnNext.gotoAndPlay('rollOver');}
		function fl_MouseOutHandler() {	this.next_btn.btnNext.gotoAndPlay('rollOut');}
		
		
		// EVENT DE CLIC POUR LE LIEN 1
		this.cta_mc.ctas.cta0.addEventListener("click", onCTA0.bind(this));
		this.cta_mc.ctas.cta0.addEventListener("mouseover", onCTA0_MouseOverHandler.bind(this));
		this.cta_mc.ctas.cta0.addEventListener("mouseout", onCTA0_MouseOutHandler.bind(this));
		
		function onCTA0() {
			var event = new Event('cta0');
			this.dispatchEvent(event);
			event = null;
			
			this.cta_mc.ctas.cta0.gotoAndPlay('rollOut');
		}
		
		function onCTA0_MouseOverHandler() {	this.cta_mc.ctas.cta0.gotoAndPlay('rollOver');}
		function onCTA0_MouseOutHandler() {	this.cta_mc.ctas.cta0.gotoAndPlay('rollOut');}
		
		// EVENT DE CLIC POUR LE LIEN 2
		this.cta_mc.ctas.cta1.addEventListener("click", onCTA1.bind(this));
		this.cta_mc.ctas.cta1.addEventListener("mouseover", onCTA1_MouseOverHandler.bind(this));
		this.cta_mc.ctas.cta1.addEventListener("mouseout", onCTA1_MouseOutHandler.bind(this));
		
		function onCTA1() {
			var event = new Event('cta1');
			this.dispatchEvent(event);
			event = null;
		
			this.cta_mc.ctas.cta1.gotoAndPlay('rollOut');
		}
		
		function onCTA1_MouseOverHandler() {	this.cta_mc.ctas.cta1.gotoAndPlay('rollOver');}
		function onCTA1_MouseOutHandler() {	this.cta_mc.ctas.cta1.gotoAndPlay('rollOut');}
	}
	this.frame_179 = function() {
		//this.visuel_mc.gotoAndPlay("close");
	}
	this.frame_339 = function() {
		//this.textes_mc.gotoAndPlay("close");
	}
	this.frame_514 = function() {
		//PAUSE
		this.stop();
	}
	this.frame_516 = function() {
		this.textes_mc.gotoAndPlay("close");
		this.cta_mc.gotoAndPlay("close");
		this.next_btn.gotoAndPlay("close");
		this.lignes_mc.gotoAndPlay("close_lines");
		this.visuel_mc.gotoAndPlay("close");
		this.video_mc.gotoAndPlay("close");
	}
	this.frame_562 = function() {
		// EVENT DE FIN D'ANIM
		var event = new Event('closed');
		this.dispatchEvent(event);		
		event = null;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(129).call(this.frame_129).wait(10).call(this.frame_139).wait(40).call(this.frame_179).wait(160).call(this.frame_339).wait(175).call(this.frame_514).wait(2).call(this.frame_516).wait(46).call(this.frame_562).wait(1));

	// CTA NEXT
	this.next_btn = new lib.bloc_NEXT_btn();
	this.next_btn.name = "next_btn";
	this.next_btn.parent = this;
	this.next_btn.setTransform(359.2,861.2,1.06,1.06,0,0,0,-340.4,403.9);
	this.next_btn._off = true;

	this.timeline.addTween(cjs.Tween.get(this.next_btn).wait(128).to({_off:false},0).wait(435));

	// CTA Contact
	this.cta_mc = new lib.bloc_CTA();
	this.cta_mc.name = "cta_mc";
	this.cta_mc.parent = this;
	this.cta_mc.setTransform(121.5,861.4,1.06,1.06,0,0,0,-340.4,404.1);
	this.cta_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.cta_mc).wait(119).to({_off:false},0).to({_off:true},420).wait(24));

	// BLOC INFOS TEXTE
	this.textes_mc = new lib.blocInfostextes();
	this.textes_mc.name = "textes_mc";
	this.textes_mc.parent = this;
	this.textes_mc.setTransform(792,235.5,1,1,0,0,0,222.1,-164);
	this.textes_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.textes_mc).wait(17).to({_off:false},0).wait(546));

	// VIDEO
	this.video_mc = new lib.bloc_video();
	this.video_mc.name = "video_mc";
	this.video_mc.parent = this;
	this.video_mc.setTransform(359.4,1081.7,0.627,0.627,0,0,0,227.5,765.5);

	this.timeline.addTween(cjs.Tween.get(this.video_mc).wait(563));

	// VISUEL
	this.visuel_mc = new lib.bloc_visuel();
	this.visuel_mc.name = "visuel_mc";
	this.visuel_mc.parent = this;
	this.visuel_mc.setTransform(1059.3,471.6,1.05,1.05,0,0,0,552.6,26.7);
	this.visuel_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.visuel_mc).wait(17).to({_off:false},0).wait(546));

	// lignes
	this.lignes_mc = new lib.bloc_lignes();
	this.lignes_mc.name = "lignes_mc";
	this.lignes_mc.parent = this;
	this.lignes_mc.setTransform(240.8,453.9,0.525,1,0,0,0,0.7,405.9);

	this.timeline.addTween(cjs.Tween.get(this.lignes_mc).wait(563));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(241.3,293.5,478,988);
// library properties:
lib.properties = {
	id: '39994B05A8901F48BD55A71AD6A95ED5',
	width: 480,
	height: 840,
	fps: 60,
	color: "#000000",
	opacity: 1.00,
	manifest: [
		{src:"images/chien.png?1540896165643", id:"chien"},
		{src:"images/coralie_step1.jpg?1540896165643", id:"coralie_step1"},
		{src:"images/coralie_step2.jpg?1540896165643", id:"coralie_step2"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['39994B05A8901F48BD55A71AD6A95ED5'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;