(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.step2 = function() {
	this.initialize(img.step2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,239,264);


(lib.steph = function() {
	this.initialize(img.steph);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,239,264);


(lib.stephane = function() {
	this.initialize(img.stephane);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,500,870);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.video_frame_croix = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{close:30});

	// timeline functions:
	this.frame_29 = function() {
		this.stop();
	}
	this.frame_50 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(29).call(this.frame_29).wait(21).call(this.frame_50).wait(1));

	// Layer_7 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("EguphRHIEsihMBYnCkwIksChg");
	var mask_graphics_1 = new cjs.Graphics().p("Eg0fhN+IQYozMBYnCkwIwYIzg");
	var mask_graphics_2 = new cjs.Graphics().p("Eg5vhLJIa4udMBYnCkwI64Odg");
	var mask_graphics_3 = new cjs.Graphics().p("Eg+chInMAkSgThMBYnCkwMgkSAThg");
	var mask_graphics_4 = new cjs.Graphics().p("EhCohGXMAsqgYBMBYnCkwMgsqAYBg");
	var mask_graphics_5 = new cjs.Graphics().p("EhGVhEXMA0FgcBMBYmCkwMg0FAcBg");
	var mask_graphics_6 = new cjs.Graphics().p("EhJnhCmMA6pgfjMBYmCkwMg6pAfjg");
	var mask_graphics_7 = new cjs.Graphics().p("EhMfhBDMBAZgipMBYmCkwMhAZAipg");
	var mask_graphics_8 = new cjs.Graphics().p("EhPAg/tMBFaglVMBYnCkwMhFaAlVg");
	var mask_graphics_9 = new cjs.Graphics().p("EhRLg+iMBJxgnrMBYmCkwMhJxAnrg");
	var mask_graphics_10 = new cjs.Graphics().p("EhTDg9hMBNhgpsMBYmCkvMhNhApsg");
	var mask_graphics_11 = new cjs.Graphics().p("EhUqg8qMBQugrbMBYnCkwMhQuArbg");
	var mask_graphics_12 = new cjs.Graphics().p("EhWAg77MBTbgs4MBYmCkvMhTbAs4g");
	var mask_graphics_13 = new cjs.Graphics().p("EhXJg7UMBVsguGMBYnCkvMhVsAuGg");
	var mask_graphics_14 = new cjs.Graphics().p("EhYFg60MBXlgvHMBYmCkwMhXlAvHg");
	var mask_graphics_15 = new cjs.Graphics().p("EhY3g6ZMBZHgv8MBYoCkvMhZHAv8g");
	var mask_graphics_16 = new cjs.Graphics().p("EhZeg6EMBaWgwnMBYnCkwMhaWAwng");
	var mask_graphics_17 = new cjs.Graphics().p("EhZ+g5zMBbVgxJMBYoCkwMhbVAxJg");
	var mask_graphics_18 = new cjs.Graphics().p("EhaXg5mMBcHgxjMBYoCkwMhcHAxjg");
	var mask_graphics_19 = new cjs.Graphics().p("Ehapg5cMBcsgx3MBYnCkwMhcsAx3g");
	var mask_graphics_20 = new cjs.Graphics().p("Eha3g5UMBdIgyGMBYnCkvMhdIAyGg");
	var mask_graphics_21 = new cjs.Graphics().p("EhbBg5PMBdbgyRMBYoCkwMhdbAyRg");
	var mask_graphics_22 = new cjs.Graphics().p("EhbIg5LMBdpgyYMBYoCkvMhdpAyYg");
	var mask_graphics_23 = new cjs.Graphics().p("EhbMg5JMBdygydMBYnCkwMhdyAydg");
	var mask_graphics_24 = new cjs.Graphics().p("EhbPg5HMBd3gygMBYoCkvMhd3Aygg");
	var mask_graphics_25 = new cjs.Graphics().p("EhbRg5HMBd7gyhMBYoCkwMhd7Ayhg");
	var mask_graphics_26 = new cjs.Graphics().p("EhbRg5GMBd8gyiMBYnCkvMhd8Ayig");
	var mask_graphics_27 = new cjs.Graphics().p("EhbSg5GMBd9gyjMBYoCkwMhd9Ayjg");
	var mask_graphics_28 = new cjs.Graphics().p("EhbSg5GMBd9gyjMBYoCkwMhd9Ayjg");
	var mask_graphics_29 = new cjs.Graphics().p("EhbSg5GMBd9gyjMBYoCkwMhd9Ayjg");
	var mask_graphics_30 = new cjs.Graphics().p("EhbSg5GMBd9gyjMBYoCkwMhd9Ayjg");
	var mask_graphics_31 = new cjs.Graphics().p("EhbPg5HMBd4gygMBYnCkvMhd4Aygg");
	var mask_graphics_32 = new cjs.Graphics().p("EhbGg5MMBdlgyWMBYoCkvMhdlAyWg");
	var mask_graphics_33 = new cjs.Graphics().p("Ehatg5aMBczgx7MBYoCkwMhczAx7g");
	var mask_graphics_34 = new cjs.Graphics().p("EhZ4g52MBbKgxDMBYnCkwMhbKAxDg");
	var mask_graphics_35 = new cjs.Graphics().p("EhYZg6qMBYMgvbMBYnCkwMhYMAvbg");
	var mask_graphics_36 = new cjs.Graphics().p("EhV7g7+MBTQgsyMBYnCkvMhTQAsyg");
	var mask_graphics_37 = new cjs.Graphics().p("EhSJg+BMBLsgotMBYnCkwMhLsAotg");
	var mask_graphics_38 = new cjs.Graphics().p("EhMohA+MBArgiyMBYmCkvMhArAiyg");
	var mask_graphics_39 = new cjs.Graphics().p("EhE9hFGMAxUgaiMBYnCkvMgxUAaig");
	var mask_graphics_40 = new cjs.Graphics().p("Eg9ShJPMAh+gSRMBYnCkwMgh+ASRg");
	var mask_graphics_41 = new cjs.Graphics().p("Eg3yhMMIW+sXMBYnCkwI2+MXg");
	var mask_graphics_42 = new cjs.Graphics().p("Eg0AhOOIPaoSMBYnCkvIvaISg");
	var mask_graphics_43 = new cjs.Graphics().p("EgxihPjIKeloMBYnCkvIqeFog");
	var mask_graphics_44 = new cjs.Graphics().p("EgwChQXIHekBMBYnCkwIneEBg");
	var mask_graphics_45 = new cjs.Graphics().p("EgvNhQzIF0jIMBYnCkvIl0DJg");
	var mask_graphics_46 = new cjs.Graphics().p("Egu0hRBIFCitMBYnCkwIlCCtg");
	var mask_graphics_47 = new cjs.Graphics().p("EgurhRGIEwijMBYnCkwIkwCjg");
	var mask_graphics_48 = new cjs.Graphics().p("EguphRHIEsihMBYnCkwIksChg");
	var mask_graphics_49 = new cjs.Graphics().p("EguphRHIEsihMBYnCkwIksChg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:-274.5388,y:516.6306}).wait(1).to({graphics:mask_graphics_1,x:-238.6331,y:480.4354}).wait(1).to({graphics:mask_graphics_2,x:-206.3752,y:447.9173}).wait(1).to({graphics:mask_graphics_3,x:-177.5091,y:418.8185}).wait(1).to({graphics:mask_graphics_4,x:-151.7885,y:392.8904}).wait(1).to({graphics:mask_graphics_5,x:-128.9759,y:369.8938}).wait(1).to({graphics:mask_graphics_6,x:-108.8435,y:349.5991}).wait(1).to({graphics:mask_graphics_7,x:-91.1727,y:331.7858}).wait(1).to({graphics:mask_graphics_8,x:-75.7541,y:316.2428}).wait(1).to({graphics:mask_graphics_9,x:-62.3877,y:302.7686}).wait(1).to({graphics:mask_graphics_10,x:-50.8828,y:291.1709}).wait(1).to({graphics:mask_graphics_11,x:-41.0579,y:281.2668}).wait(1).to({graphics:mask_graphics_12,x:-32.7409,y:272.8828}).wait(1).to({graphics:mask_graphics_13,x:-25.7691,y:265.8547}).wait(1).to({graphics:mask_graphics_14,x:-19.9889,y:260.0279}).wait(1).to({graphics:mask_graphics_15,x:-15.2562,y:255.257}).wait(1).to({graphics:mask_graphics_16,x:-11.4359,y:251.406}).wait(1).to({graphics:mask_graphics_17,x:-8.4026,y:248.3482}).wait(1).to({graphics:mask_graphics_18,x:-6.0399,y:245.9664}).wait(1).to({graphics:mask_graphics_19,x:-4.2408,y:244.1528}).wait(1).to({graphics:mask_graphics_20,x:-2.9077,y:242.809}).wait(1).to({graphics:mask_graphics_21,x:-1.9522,y:241.8457}).wait(1).to({graphics:mask_graphics_22,x:-1.2951,y:241.1834}).wait(1).to({graphics:mask_graphics_23,x:-0.8668,y:240.7516}).wait(1).to({graphics:mask_graphics_24,x:-0.6067,y:240.4894}).wait(1).to({graphics:mask_graphics_25,x:-0.4636,y:240.3452}).wait(1).to({graphics:mask_graphics_26,x:-0.3958,y:240.2768}).wait(1).to({graphics:mask_graphics_27,x:-0.3706,y:240.2514}).wait(1).to({graphics:mask_graphics_28,x:-0.3648,y:240.2455}).wait(1).to({graphics:mask_graphics_29,x:-18.0517,y:249.7587}).wait(1).to({graphics:mask_graphics_30,x:-18.0646,y:249.7721}).wait(1).to({graphics:mask_graphics_31,x:-18.2577,y:249.9726}).wait(1).to({graphics:mask_graphics_32,x:-19.0941,y:250.8415}).wait(1).to({graphics:mask_graphics_33,x:-21.3461,y:253.1808}).wait(1).to({graphics:mask_graphics_34,x:-26.0945,y:258.1133}).wait(1).to({graphics:mask_graphics_35,x:-34.7292,y:267.0827}).wait(1).to({graphics:mask_graphics_36,x:-48.9488,y:281.8536}).wait(1).to({graphics:mask_graphics_37,x:-70.7607,y:304.5111}).wait(1).to({graphics:mask_graphics_38,x:-102.4812,y:337.4615}).wait(1).to({graphics:mask_graphics_39,x:-146.7356,y:383.4315}).wait(1).to({graphics:mask_graphics_40,x:-190.99,y:429.4016}).wait(1).to({graphics:mask_graphics_41,x:-222.7106,y:462.352}).wait(1).to({graphics:mask_graphics_42,x:-244.5225,y:485.0095}).wait(1).to({graphics:mask_graphics_43,x:-258.7421,y:499.7803}).wait(1).to({graphics:mask_graphics_44,x:-267.3767,y:508.7498}).wait(1).to({graphics:mask_graphics_45,x:-272.1252,y:513.6823}).wait(1).to({graphics:mask_graphics_46,x:-274.3771,y:516.0216}).wait(1).to({graphics:mask_graphics_47,x:-275.2136,y:516.8905}).wait(1).to({graphics:mask_graphics_48,x:-275.4066,y:517.091}).wait(1).to({graphics:mask_graphics_49,x:-274.5388,y:516.6306}).wait(1).to({graphics:null,x:0,y:0}).wait(1));

	// Layer_6
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#CC9900").ss(1,1,1).p("EAdugxOMgRVBidQk+tTk/tRQp96hAAAIQAAAJrGAKQljAFljAD");
	this.shape.setTransform(-0.125,315.125);

	var maskedShapeInstanceList = [this.shape];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape).to({_off:true},50).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-191.3,-0.9,382.4,632.1);


(lib.video_frame_angle = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_59 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(59).call(this.frame_59).wait(1));

	// Layer_2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("EguphRHIEsihMBYnCkwIksChg");
	var mask_graphics_1 = new cjs.Graphics().p("EguphRHIEsihMBYnCkwIksChg");
	var mask_graphics_2 = new cjs.Graphics().p("EguphRHIEsihMBYnCkwIksChg");
	var mask_graphics_3 = new cjs.Graphics().p("EguphRHIEsihMBYnCkwIksChg");
	var mask_graphics_4 = new cjs.Graphics().p("EguqhRGIEuiiMBYnCkvIkuCig");
	var mask_graphics_5 = new cjs.Graphics().p("EgurhRFIEwikMBYnCkvIkwCkg");
	var mask_graphics_6 = new cjs.Graphics().p("EguuhREIE2inMBYnCkwIk2Cng");
	var mask_graphics_7 = new cjs.Graphics().p("EguyhRCIE/irMBYmCkwIk/Crg");
	var mask_graphics_8 = new cjs.Graphics().p("Egu5hQ+IFMizMBYnCkwIlMCzg");
	var mask_graphics_9 = new cjs.Graphics().p("EgvDhQ5IFgi9MBYnCkwIlgC9g");
	var mask_graphics_10 = new cjs.Graphics().p("EgvRhQxIF8jNMBYnCkwIl8DNg");
	var mask_graphics_11 = new cjs.Graphics().p("EgvkhQnIGijhMBYnCkwImiDhg");
	var mask_graphics_12 = new cjs.Graphics().p("Egv8hQaIHTj7MBYmCkwInTD7g");
	var mask_graphics_13 = new cjs.Graphics().p("EgwchQJIISkdMBYnCkwIoSEdg");
	var mask_graphics_14 = new cjs.Graphics().p("EgxEhPzIJilIMBYnCkvIpiFIg");
	var mask_graphics_15 = new cjs.Graphics().p("Egx1hPZILEl9MBYnCkwIrEF9g");
	var mask_graphics_16 = new cjs.Graphics().p("EgyxhO4IM9m+MBYmCkvIs9G+g");
	var mask_graphics_17 = new cjs.Graphics().p("Egz6hORIPOoMMBYnCkvIvOIMg");
	var mask_graphics_18 = new cjs.Graphics().p("Eg1RhNjIR8ppMBYnCkwIx8Jpg");
	var mask_graphics_19 = new cjs.Graphics().p("Eg23hMsIVIrXMBYnCkwI1ILXg");
	var mask_graphics_20 = new cjs.Graphics().p("Eg4vhLrIY4tYMBYnCkvI44NYg");
	var mask_graphics_21 = new cjs.Graphics().p("Eg66hKgIdPvuMBYmCkvI9PPug");
	var mask_graphics_22 = new cjs.Graphics().p("Eg9bhJKMAiQgSbMBYnCkwMgiQASbg");
	var mask_graphics_23 = new cjs.Graphics().p("EhAThHnMAoAgVhMBYnCkwMgoAAVhg");
	var mask_graphics_24 = new cjs.Graphics().p("EhDlhF2MAukgZDMBYnCkwMgukAZDg");
	var mask_graphics_25 = new cjs.Graphics().p("EhHThD2MA2AgdDMBYnCkwMg2AAdDg");
	var mask_graphics_26 = new cjs.Graphics().p("EhLfhBmMA+YghjMBYnCkwMg+YAhjg");
	var mask_graphics_27 = new cjs.Graphics().p("EhQLg/EMBHxgmnMBYmCkwMhHxAmng");
	var mask_graphics_28 = new cjs.Graphics().p("EhVcg8PMBSSgsQMBYnCkvMhSSAsQg");
	var mask_graphics_29 = new cjs.Graphics().p("EhbSg5GMBd9gyjMBYoCkwMhd9Ayjg");
	var mask_graphics_30 = new cjs.Graphics().p("EhZqg5+MBaugwzMBYnCkwMhauAwzg");
	var mask_graphics_31 = new cjs.Graphics().p("EhYDg61MBXggvEMBYnCkvMhXgAvEg");
	var mask_graphics_32 = new cjs.Graphics().p("EhWbg7tMBURgtVMBYmCkwMhURAtVg");
	var mask_graphics_33 = new cjs.Graphics().p("EhU0g8lMBRCgrlMBYnCkwMhRCArlg");
	var mask_graphics_34 = new cjs.Graphics().p("EhTNg9cMBN0gp2MBYnCkvMhN0Ap2g");
	var mask_graphics_35 = new cjs.Graphics().p("EhRlg+UMBKkgoHMBYnCkwMhKkAoHg");
	var mask_graphics_36 = new cjs.Graphics().p("EhP+g/LMBHWgmYMBYnCkvMhHWAmYg");
	var mask_graphics_37 = new cjs.Graphics().p("EhOWhADMBEHgkpMBYmCkwMhEHAkpg");
	var mask_graphics_38 = new cjs.Graphics().p("EhMvhA7MBA4gi5MBYnCkwMhA4Ai5g");
	var mask_graphics_39 = new cjs.Graphics().p("EhLHhByMA9pghKMBYmCkvMg9pAhKg");
	var mask_graphics_40 = new cjs.Graphics().p("EhJghCqMA6agfbMBYnCkwMg6aAfbg");
	var mask_graphics_41 = new cjs.Graphics().p("EhH5hDiMA3MgdrMBYnCkwMg3MAdrg");
	var mask_graphics_42 = new cjs.Graphics().p("EhGRhEZMAz8gb8MBYnCkvMgz8Ab8g");
	var mask_graphics_43 = new cjs.Graphics().p("EhEqhFRMAwugaNMBYnCkwMgwuAaNg");
	var mask_graphics_44 = new cjs.Graphics().p("EhDChGIMAtfgYeMBYmCkvMgtfAYeg");
	var mask_graphics_45 = new cjs.Graphics().p("EhBbhHAMAqQgWuMBYnCkvMgqQAWug");
	var mask_graphics_46 = new cjs.Graphics().p("Eg/zhH4MAnBgU/MBYmCkwMgnBAU/g");
	var mask_graphics_47 = new cjs.Graphics().p("Eg+MhIvMAjygTQMBYnCkvMgjyATQg");
	var mask_graphics_48 = new cjs.Graphics().p("Eg8lhJnMAgkgRhMBYnCkwMggkARhg");
	var mask_graphics_49 = new cjs.Graphics().p("Eg69hKfIdUvxMBYnCkwI9UPxg");
	var mask_graphics_50 = new cjs.Graphics().p("Eg5WhLWIaGuCMBYnCkvI6GOCg");
	var mask_graphics_51 = new cjs.Graphics().p("Eg3uhMOIW3sTMBYmCkwI23MTg");
	var mask_graphics_52 = new cjs.Graphics().p("Eg2HhNGIToqjMBYnCkwIzoKjg");
	var mask_graphics_53 = new cjs.Graphics().p("Eg0fhN9IQZo0MBYmCkvIwZI0g");
	var mask_graphics_54 = new cjs.Graphics().p("Egy4hO1INKnFMBYnCkwItKHFg");
	var mask_graphics_55 = new cjs.Graphics().p("EgxRhPsIJ8lWMBYnCkvIp8FWg");
	var mask_graphics_56 = new cjs.Graphics().p("EgvphQkIGsjnMBYnCkwImsDng");
	var mask_graphics_57 = new cjs.Graphics().p("EgvDhRcIDeh3MBYnCkwIjeB3g");
	var mask_graphics_58 = new cjs.Graphics().p("EgvDhSTIAPgIMBYnCkvIgPAIg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:268.4612,y:95.6306}).wait(1).to({graphics:mask_graphics_1,x:268.4608,y:95.6308}).wait(1).to({graphics:mask_graphics_2,x:268.4551,y:95.6339}).wait(1).to({graphics:mask_graphics_3,x:268.4304,y:95.6472}).wait(1).to({graphics:mask_graphics_4,x:268.3639,y:95.683}).wait(1).to({graphics:mask_graphics_5,x:268.2236,y:95.7584}).wait(1).to({graphics:mask_graphics_6,x:267.9686,y:95.8956}).wait(1).to({graphics:mask_graphics_7,x:267.5486,y:96.1216}).wait(1).to({graphics:mask_graphics_8,x:266.9044,y:96.4681}).wait(1).to({graphics:mask_graphics_9,x:265.9675,y:96.9721}).wait(1).to({graphics:mask_graphics_10,x:264.6604,y:97.6753}).wait(1).to({graphics:mask_graphics_11,x:262.8964,y:98.6242}).wait(1).to({graphics:mask_graphics_12,x:260.5798,y:99.8704}).wait(1).to({graphics:mask_graphics_13,x:257.6056,y:101.4704}).wait(1).to({graphics:mask_graphics_14,x:253.8599,y:103.4854}).wait(1).to({graphics:mask_graphics_15,x:249.2195,y:105.9817}).wait(1).to({graphics:mask_graphics_16,x:243.5521,y:109.0305}).wait(1).to({graphics:mask_graphics_17,x:236.7163,y:112.7078}).wait(1).to({graphics:mask_graphics_18,x:228.5616,y:117.0946}).wait(1).to({graphics:mask_graphics_19,x:218.9284,y:122.2768}).wait(1).to({graphics:mask_graphics_20,x:207.6479,y:128.3451}).wait(1).to({graphics:mask_graphics_21,x:194.5423,y:135.3953}).wait(1).to({graphics:mask_graphics_22,x:179.4244,y:143.5279}).wait(1).to({graphics:mask_graphics_23,x:162.0984,y:152.8484}).wait(1).to({graphics:mask_graphics_24,x:142.3587,y:163.4673}).wait(1).to({graphics:mask_graphics_25,x:119.9912,y:175.4999}).wait(1).to({graphics:mask_graphics_26,x:94.7723,y:189.0664}).wait(1).to({graphics:mask_graphics_27,x:66.4694,y:204.2919}).wait(1).to({graphics:mask_graphics_28,x:34.8408,y:221.3065}).wait(1).to({graphics:mask_graphics_29,x:-18.0517,y:249.7587}).wait(1).to({graphics:mask_graphics_30,x:-28.396,y:255.3254}).wait(1).to({graphics:mask_graphics_31,x:-38.7403,y:260.892}).wait(1).to({graphics:mask_graphics_32,x:-49.0845,y:266.4586}).wait(1).to({graphics:mask_graphics_33,x:-59.4288,y:272.0252}).wait(1).to({graphics:mask_graphics_34,x:-69.7731,y:277.5918}).wait(1).to({graphics:mask_graphics_35,x:-80.1173,y:283.1584}).wait(1).to({graphics:mask_graphics_36,x:-90.4616,y:288.725}).wait(1).to({graphics:mask_graphics_37,x:-100.8058,y:294.2916}).wait(1).to({graphics:mask_graphics_38,x:-111.1501,y:299.8582}).wait(1).to({graphics:mask_graphics_39,x:-121.4943,y:305.4249}).wait(1).to({graphics:mask_graphics_40,x:-131.8386,y:310.9915}).wait(1).to({graphics:mask_graphics_41,x:-142.1828,y:316.5581}).wait(1).to({graphics:mask_graphics_42,x:-152.5271,y:322.1247}).wait(1).to({graphics:mask_graphics_43,x:-162.8713,y:327.6913}).wait(1).to({graphics:mask_graphics_44,x:-173.2156,y:333.258}).wait(1).to({graphics:mask_graphics_45,x:-183.5598,y:338.8246}).wait(1).to({graphics:mask_graphics_46,x:-193.9041,y:344.3912}).wait(1).to({graphics:mask_graphics_47,x:-204.2483,y:349.9578}).wait(1).to({graphics:mask_graphics_48,x:-214.5926,y:355.5245}).wait(1).to({graphics:mask_graphics_49,x:-224.9368,y:361.0911}).wait(1).to({graphics:mask_graphics_50,x:-235.2811,y:366.6577}).wait(1).to({graphics:mask_graphics_51,x:-245.6253,y:372.2243}).wait(1).to({graphics:mask_graphics_52,x:-255.9696,y:377.791}).wait(1).to({graphics:mask_graphics_53,x:-266.3138,y:383.3576}).wait(1).to({graphics:mask_graphics_54,x:-276.6581,y:388.9242}).wait(1).to({graphics:mask_graphics_55,x:-287.0023,y:394.4909}).wait(1).to({graphics:mask_graphics_56,x:-297.3466,y:400.0575}).wait(1).to({graphics:mask_graphics_57,x:-301.1807,y:405.6241}).wait(1).to({graphics:mask_graphics_58,x:-301.1687,y:411.1778}).wait(1).to({graphics:null,x:0,y:0}).wait(1));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#CC9900").ss(1,1,1).p("EgdrgxQMAAABikEAdxgxTMg7hAAA");
	this.shape.setTransform(0,315.575);

	var maskedShapeInstanceList = [this.shape];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape).to({_off:true},59).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-191.5,-1,383,633.2);


(lib.video = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(51,51,51,0)").s().p("Egu3AjKMAAAhGTMBdvAAAMAAABGTg");
	this.shape.setTransform(300,225);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.video, new cjs.Rectangle(0,0,649.4,487), null);


(lib.txt_hashtag = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgsAxIAAhhIAjAAQAaAAAOANQAOAMgBAXQABAWgOAOQgOANgbAAgAgVAeIANAAQAOgBAJgHQAHgIABgOQgBgOgHgHQgJgIgQAAIgLAAg");
	this.shape.setTransform(23.65,52.75);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAEAxIAAgYIgnAAIAAgQIAjg5IAXAAIggA3IANAAIAAgRIAVAAIAAARIALAAIAAASIgLAAIAAAYg");
	this.shape_1.setTransform(13.825,52.75);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgZAgQgIgHAAgMQAAgLAIgFQAJgGAPAAIAOAAQAAgMgNAAQgFAAgFACQgHACgDAEIgKgOQAPgLASAAQAOAAAIAHQAJAHAAAPIAAAvIgUAAIAAgJQgIAKgLAAQgMAAgIgHgAgMANQAAAEADACQADADAGAAQAEAAAEgEQAFgDAAgGIAAgEIgNAAQgMAAAAAIg");
	this.shape_2.setTransform(5.45,53.875);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAmAmIAAgoQAAgRgNAAQgGAAgFAEQgEAFAAAJIAAAnIgUAAIAAgoQAAgJgDgEQgDgEgGAAQgGAAgFAEQgEAFAAAJIAAAnIgVAAIAAhKIAVAAIAAAIQAIgJAMAAQAHAAAGAEQAEAEADAGQAFgHAHgEQAHgDAIAAQANAAAIAIQAIAHAAAPIAAAtg");
	this.shape_3.setTransform(-5.375,53.825);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgaAcQgLgKAAgSQAAgRALgKQAMgLAOAAQAQAAALAKQALAJAAAQIAAALIg2AAQABAGAGAEQAFADAGAAQALAAAHgHIAMANQgNAMgRAAQgRAAgLgLgAgJgRQgFAEgCAHIAhAAQgBgHgFgEQgEgDgGAAQgGAAgEADg");
	this.shape_4.setTransform(-16.6,53.875);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AAOAmIAAgpQAAgSgNAAQgFAAgFAFQgEAEAAAJIAAApIgVAAIAAhKIAVAAIAAAIQAJgJALAAQAMAAAIAIQAIAJAAANIAAAtg");
	this.shape_5.setTransform(-25.375,53.825);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgJA1IAAhKIAUAAIAABKgAgIgfQgDgDAAgFQAAgGADgDQAEgEAEAAQAFAAAEAEQADADAAAGQAAAFgDADQgEADgFABQgEgBgEgDg");
	this.shape_6.setTransform(-31.975,52.35);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgeAkQgPgOAAgWQAAgVAPgOQAPgPAUAAQAZAAAQASIgOAQQgKgMgQAAQgLgBgJAJQgJAHAAANQABANAIAJQAJAIAKAAQAQAAAKgMIAPAOQgSASgWABQgWAAgOgPg");
	this.shape_7.setTransform(-39.05,52.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgTIANAAIADgRIgPAAIADgUIAPAAIAEgWIAUAAIgDAWIAMAAIADgWIAWAAIgEAWIAOAAIgDAUIgPAAIgDARIARAAIgEATIgQAAIgDAVgAgHAKIALAAIAEgRIgMAAg");
	this.shape_8.setTransform(-49.475,52.65);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgOAkQgJgDgHgGIAJgNQAMAJAMAAQAEAAACgCQABAAAAgBQABAAAAgBQAAAAABgBQAAAAAAgBQAAgDgEgCQgDgCgFgCIgKgDIgHgFQgJgEAAgKQAAgLAJgGQAIgHAMAAQAOAAANAJIgIAOQgKgHgJAAQgJAAAAAGQAAADADABIAJAEIAKAEIAIADQAHAEAAALQAAALgIAHQgIAHgNAAQgHAAgJgDg");
	this.shape_9.setTransform(43.025,32.825);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgKArQgIgIAAgMIAAghIgJAAIAAgQIAJAAIAAgWIAUAAIAAAWIASAAIAAAQIgSAAIAAAgQAAAFADACQACACADABQAGgBAEgFIAIAPQgKAIgLABQgKAAgHgHg");
	this.shape_10.setTransform(36.625,31.75);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgXAcQgMgLAAgRQAAgQAMgLQAMgLAPAAQAIAAAJAEQAIADAGAGIgLAPQgDgFgGgCQgFgCgFAAQgIAAgFAFQgHAFABAJQgBAKAHAFQAGAFAGAAQALAAAIgKIANAOQgPAPgRAAQgPAAgMgLg");
	this.shape_11.setTransform(29.35,32.825);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgZAcQgMgKAAgSQAAgRAMgKQALgLAPAAQAPAAALAKQALAJAAAQIAAALIg2AAQABAGAGAEQAFADAGAAQALAAAHgHIALANQgMAMgRAAQgQAAgLgLgAgJgRQgGAEAAAHIAgAAQgBgHgFgEQgEgDgGAAQgFAAgFADg");
	this.shape_12.setTransform(20.9,32.825);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgRA1IAAg6IgJAAIAAgPIAJAAIAAgFQgBgNAIgHQAIgHAKAAQAKAAAKAIIgIAPQgEgFgGgBQgEAAgCADQgCACAAAEIAAAGIASAAIAAAPIgSAAIAAA6g");
	this.shape_13.setTransform(14.15,31.3);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgRA1IAAg6IgJAAIAAgPIAJAAIAAgFQAAgNAHgHQAIgHAKAAQALAAAJAIIgIAPQgFgFgFgBQgDAAgDADQgCACAAAEIAAAGIASAAIAAAPIgSAAIAAA6g");
	this.shape_14.setTransform(8.5,31.3);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgjAxIAAhhIBFAAIAAATIgvAAIAAAVIArAAIAAASIgrAAIAAATIAxAAIAAAUg");
	this.shape_15.setTransform(0.9,31.7);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgVAmIAAhKIAVAAIAAAKQADgFAGgDQAGgDAHAAIAAATIgEAAQgKAAgEAHQgEAHAAAKIAAAgg");
	this.shape_16.setTransform(-10.175,32.775);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgaAcQgLgKAAgSQAAgRALgKQAMgLAOAAQAQAAALAKQALAJAAAQIAAALIg2AAQABAGAFAEQAGADAGAAQALAAAHgHIAMANQgNAMgRAAQgQAAgMgLgAgKgRQgEAEgBAHIAfAAQAAgHgEgEQgFgDgGAAQgFAAgGADg");
	this.shape_17.setTransform(-17.65,32.825);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgKArQgIgIAAgMIAAghIgJAAIAAgQIAJAAIAAgWIAUAAIAAAWIASAAIAAAQIgSAAIAAAgQAAAFADACQACACADABQAGgBAEgFIAIAPQgKAIgLABQgKAAgHgHg");
	this.shape_18.setTransform(-24.875,31.75);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgSA1IAAg6IgJAAIAAgPIAJAAIAAgFQABgNAHgHQAIgHAKAAQAKAAAJAIIgHAPQgFgFgFgBQgEAAgBADQgDACAAAEIAAAGIASAAIAAAPIgSAAIAAA6g");
	this.shape_19.setTransform(-30.55,31.3);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AAeAxIgKgVIgoAAIgJAVIgXAAIAqhhIAVAAIAqBhgAgLAJIAXAAIgMgcg");
	this.shape_20.setTransform(-39.025,31.7);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgTIANAAIADgRIgPAAIADgTIAPAAIAEgXIAUAAIgDAXIAMAAIADgXIAWAAIgEAXIAOAAIgDATIgPAAIgDARIARAAIgEATIgQAAIgDAVgAgHAKIALAAIAEgRIgMAAg");
	this.shape_21.setTransform(-49.475,31.6);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AAOAmIAAgpQAAgSgNAAQgFAAgFAFQgEAEAAAJIAAApIgVAAIAAhKIAVAAIAAAIQAJgJALAAQAMAAAIAIQAIAJAAANIAAAtg");
	this.shape_22.setTransform(54.875,11.725);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgSAzQgJgDgGgFIAIgQQAKAHAMAAQAKAAAHgFQAHgGAAgMQgKALgOAAQgPAAgKgKQgKgKAAgQQAAgQALgMQAKgKANAAQANAAAJALIAAgKIAVAAIAABBQAAAKgEAIQgDAHgGAFQgMAJgPAAQgIAAgJgCgAgLgdQgGAFAAAJQAAAJAGAGQAFAEAGAAQAIAAAFgEQAFgGAAgJQAAgJgFgFQgFgFgHAAQgHAAgFAFg");
	this.shape_23.setTransform(45.475,13.2);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgJA1IAAhKIAUAAIAABKgAgIgfQgDgEAAgEQAAgGADgDQAEgEAEAAQAFAAAEAEQADADAAAGQAAAEgDAEQgEAEgFgBQgEABgEgEg");
	this.shape_24.setTransform(38.925,10.25);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgOAkQgJgDgHgGIAJgNQAMAJAMAAQAEAAACgCQABAAAAgBQABAAAAgBQAAAAABgBQAAAAAAgBQAAgDgEgCQgDgCgFgCIgKgDIgHgFQgJgEAAgKQAAgLAJgGQAIgHAMAAQAOAAANAJIgIAOQgKgHgJAAQgJAAAAAGQAAADADABIAJAEIAKAEIAIADQAHAEAAALQAAALgIAHQgIAHgNAAQgHAAgJgDg");
	this.shape_25.setTransform(33.125,11.775);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgZAcQgMgKAAgSQAAgRAMgKQALgLAPAAQAPAAALAKQALAJAAAQIAAALIg2AAQABAGAFAEQAGADAGAAQALAAAHgHIALANQgMAMgRAAQgRAAgKgLgAgKgRQgEAEgBAHIAfAAQAAgHgEgEQgFgDgGAAQgFAAgGADg");
	this.shape_26.setTransform(25.3,11.775);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgrAxIAAhhIAiAAQAaAAANANQAPAMAAAXQAAAWgPAOQgNANgbAAgAgWAdIAOAAQAPAAAHgHQAJgIgBgOQABgOgJgHQgHgIgRAAIgMAAg");
	this.shape_27.setTransform(15.85,10.65);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AAOAmIAAgpQAAgSgNAAQgFAAgFAFQgEAEAAAJIAAApIgVAAIAAhKIAVAAIAAAIQAJgJALAAQAMAAAIAIQAIAJAAANIAAAtg");
	this.shape_28.setTransform(2.075,11.725);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AgbAcQgMgMAAgQQAAgPAMgMQALgLAQAAQARAAAMALQALAMAAAPQAAAQgLAMQgMALgRAAQgQAAgLgLgAgMgOQgGAGAAAIQAAAJAGAGQAFAGAHAAQAIAAAGgGQAFgGAAgJQAAgIgFgGQgGgGgIAAQgHAAgFAGg");
	this.shape_29.setTransform(-7.025,11.775);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AgJA1IAAhKIAUAAIAABKgAgIgfQgDgEAAgEQAAgGADgDQAEgEAEAAQAFAAAEAEQADADAAAGQAAAEgDAEQgEAEgFgBQgEABgEgEg");
	this.shape_30.setTransform(-13.625,10.25);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AgKAqQgIgGAAgNIAAgiIgJAAIAAgPIAJAAIAAgWIAUAAIAAAWIASAAIAAAPIgSAAIAAAhQAAAFADACQACACADABQAGgBAEgFIAIAPQgKAJgLAAQgKAAgHgIg");
	this.shape_31.setTransform(-18.525,10.7);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AgbAcQgMgMAAgQQAAgPAMgMQALgLAQAAQARAAAMALQALAMAAAPQAAAQgLAMQgMALgRAAQgQAAgLgLgAgMgOQgGAGAAAIQAAAJAGAGQAFAGAHAAQAIAAAGgGQAFgGAAgJQAAgIgFgGQgGgGgIAAQgHAAgFAGg");
	this.shape_32.setTransform(-26.375,11.775);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AAhAxIAAg9IgbA1IgMAAIgag1IAAA9IgWAAIAAhhIAeAAIAYA1IAZg1IAeAAIAABhg");
	this.shape_33.setTransform(-37.575,10.65);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AgBAyIACgVIgMAAIgDAVIgVAAIADgVIgNAAIAEgTIANAAIADgRIgPAAIADgTIAPAAIAEgXIAUAAIgDAXIAMAAIADgXIAWAAIgEAXIAOAAIgDATIgPAAIgDARIARAAIgEATIgQAAIgDAVgAgHAKIALAAIAEgRIgMAAg");
	this.shape_34.setTransform(-49.475,10.55);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.txt_hashtag, new cjs.Rectangle(-56.7,0,118.2,63.2), null);


(lib.steph_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.instance = new lib.stephane();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.steph_1, new cjs.Rectangle(0,0,500,870), null);


(lib.mob = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.instance = new lib.steph();
	this.instance.setTransform(-169,-257);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.mob, new cjs.Rectangle(-169,-257,239,264), null);


(lib.masqueTexte_1_ligne = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("EgicAEJIAAoRMBE5AAAIAAIRg");
	this.shape.setTransform(0,24.55);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.masqueTexte_1_ligne, new cjs.Rectangle(-220.5,-1.9,441,53), null);


(lib.masque_generique = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CC9900").s().p("AsQCqIAAlTIYhAAIAAFTg");
	this.shape.setTransform(78.5,17);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.masque_generique, new cjs.Rectangle(0,0,157,34), null);


(lib.ligne = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(49));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#333333").ss(2,1,1).p("EgABhM/MAADCZ/");
	this.shape.setTransform(-0.175,319.25);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#333333").ss(2,1,1).p("EAAghM7Qg1OzgbRBQgbUYAEVMQADWBAkVVQAlTkBERl");
	this.shape_1.setTransform(-7.0258,319.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#333333").ss(2,1,1).p("EAA7hM5QhfN/gxR5QgxUHAHVhQAGWSA/VCQBDUPB4Qw");
	this.shape_2.setTransform(-12.4134,319.85);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#333333").ss(2,1,1).p("EABQhM3Qh/NXhDSkQhAT5AIVyQAIWfBUU0QBZUwChQG");
	this.shape_3.setTransform(-16.5245,320.05);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#333333").ss(2,1,1).p("EABfhM1QiXM4hPTEQhMTvAKV+QAJWpBjUqQBqVIC+Pn");
	this.shape_4.setTransform(-19.5828,320.225);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#333333").ss(2,1,1).p("EABqhM0QioMjhYTaQhVToALWHQALWwBuUiQB1VaDUPR");
	this.shape_5.setTransform(-21.763,320.325);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#333333").ss(2,1,1).p("EABxhMzQizMUheTqQhbTjAMWNQALW1B2UdQB9VmDjPB");
	this.shape_6.setTransform(-23.2645,320.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#333333").ss(2,1,1).p("EAB2hMzQi7MKhiT1QhfTgAMWRQAMW3B7UaQCDVvDsO3");
	this.shape_7.setTransform(-24.2423,320.45);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#333333").ss(2,1,1).p("EAB5hMyQjAMEhkT7QhhTeAMWTQAMW5B+UYQCGVzDyOx");
	this.shape_8.setTransform(-24.821,320.475);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#333333").ss(2,1,1).p("EAB7hMyQjCMBhmT+QhjTdANWVQAMW6CAUWQCHV2D2Ou");
	this.shape_9.setTransform(-25.1701,320.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#333333").ss(2,1,1).p("EAB8hMyQjEL/hmUAQhjTcAMWWQANW7CAUWQCIV3D3Os");
	this.shape_10.setTransform(-25.3201,320.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#333333").ss(2,1,1).p("EAB8hMyQjEL/hnUAQhjTcANWWQAMW7CAUWQCJV3D4Os");
	this.shape_11.setTransform(-25.3701,320.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#333333").ss(2,1,1).p("EAB8hMyQjEL/hnUAQhjTcAMWWQANW7CAUWQCJV3D4Os");
	this.shape_12.setTransform(-25.3948,320.5);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#333333").ss(2,1,1).p("EAB8hMyQjEL/hnUAQhjTcAMWWQANW7CAUWQCJV4D4Or");
	this.shape_13.setTransform(-25.3948,320.5);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#333333").ss(2,1,1).p("EAB8hMyQjEL/hmUAQhkTcANWWQAMW7CBUWQCIV3D4Os");
	this.shape_14.setTransform(-25.3698,320.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#333333").ss(2,1,1).p("EAB7hMyQjDMAhmT/QhjTdANWVQAMW6CAUWQCIV3D2Ot");
	this.shape_15.setTransform(-25.2451,320.5);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#333333").ss(2,1,1).p("EAB6hMyQjBMChmT9QhiTdAMWVQANW6B/UXQCHV1D1Ou");
	this.shape_16.setTransform(-25.0954,320.475);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#333333").ss(2,1,1).p("EAB5hMyQjAMEhkT7QhiTeANWTQAMW5B+UYQCGV0DzOw");
	this.shape_17.setTransform(-24.871,320.475);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#333333").ss(2,1,1).p("EAB4hMzQi+MHhjT5QhgTfAMWRQAMW5B8UZQCFVxDvO0");
	this.shape_18.setTransform(-24.5917,320.45);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#333333").ss(2,1,1).p("EAB2hMzQi7MLhhT0QhfTgAMWRQAMW3B6UaQCDVtDrO5");
	this.shape_19.setTransform(-24.1923,320.45);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#333333").ss(2,1,1).p("EABzhMzQi2MQhgTuQhcTiALWPQAMW1B4UcQB/VqDnO9");
	this.shape_20.setTransform(-23.6636,320.425);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#333333").ss(2,1,1).p("EABwhMzQiyMWhcToQhbTkAMWMQALWzB1UeQB8VlDgPD");
	this.shape_21.setTransform(-23.0398,320.375);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#333333").ss(2,1,1).p("EABshM0QisMehZTgQhXTmALWJQALWxBwUhQB4VeDZPM");
	this.shape_22.setTransform(-22.2367,320.35);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#333333").ss(2,1,1).p("EABohM0QilMnhVTWQhUTpALWFQAKWuBsUlQBzVWDQPV");
	this.shape_23.setTransform(-21.3086,320.3);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#333333").ss(2,1,1).p("EABihM1QibMyhSTKQhPTuAKWAQAKWrBnUoQBsVNDFPh");
	this.shape_24.setTransform(-20.2062,320.25);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#333333").ss(2,1,1).p("EABchM1QiSM/hMS8QhKTyAKV7QAJWnBgUsQBmVDC4Pt");
	this.shape_25.setTransform(-18.904,320.175);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#333333").ss(2,1,1).p("EABUhM2QiGNNhGStQhET3AJV1QAJWiBYUyQBeU3CpP8");
	this.shape_26.setTransform(-17.4222,320.1);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#333333").ss(2,1,1).p("EABMhM3Qh5Neg/ScQg9T8AIVuQAHWdBQU2QBVUqCYQO");
	this.shape_27.setTransform(-15.7214,320.025);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#333333").ss(2,1,1).p("EABChM4QhqNxg3SIQg2UCAHVnQAHWWBGU9QBKUbCGQh");
	this.shape_28.setTransform(-13.7905,319.925);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#333333").ss(2,1,1).p("EAA3hM5QhZOGguRxQguUKAGVeQAGWPA7VFQA/UJBxQ3");
	this.shape_29.setTransform(-11.685,319.825);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#333333").ss(2,1,1).p("EAAuhM6QhKOZgnRdQgmUQAFVWQAGWKAxVLQA0T6BfRK");
	this.shape_30.setTransform(-9.7541,319.725);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#333333").ss(2,1,1).p("EAAlhM7Qg8OqggRLQgfUWAEVPQAEWFApVQQArTtBORb");
	this.shape_31.setTransform(-8.0535,319.65);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#333333").ss(2,1,1).p("EAAehM7QgxO4gaQ8QgZUaADVJQAEWAAhVWQAjTgA/Rq");
	this.shape_32.setTransform(-6.5715,319.575);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#333333").ss(2,1,1).p("EAAXhM8QgnPFgUQuQgUUeADVFQADV8AaVaQAcTWAzR3");
	this.shape_33.setTransform(-5.2696,319.5);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#333333").ss(2,1,1).p("EAAShM9QgePQgRQjQgPUiACVAQADV4AVVeQAVTNAoSD");
	this.shape_34.setTransform(-4.1674,319.45);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#333333").ss(2,1,1).p("EAANhM9QgXPZgMQZQgMUlACU9QACV1AQVhQARTFAeSM");
	this.shape_35.setTransform(-3.2393,319.4);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#333333").ss(2,1,1).p("EAAJhM9QgRPggJQRQgJUoACU5QABVyANVkQAMS/AXSU");
	this.shape_36.setTransform(-2.4362,319.375);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#333333").ss(2,1,1).p("EAAGhM+QgMPogGQKQgHUpACU3QABVwAJVmQAJS6ARSb");
	this.shape_37.setTransform(-1.8141,319.325);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#333333").ss(2,1,1).p("EAADhM+QgIPsgEQFQgEUrABU1QABVuAGVoQAGS2AMSg");
	this.shape_38.setTransform(-1.285,319.3);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#333333").ss(2,1,1).p("EAABhM+QgFPwgCQAQgDUtABUzQABVtAEVqQAESyAISk");
	this.shape_39.setTransform(-0.8875,319.3);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#333333").ss(2,1,1).p("EAAAhM+QgCPzgCP9QgBUuAAUxQABVtADVqQACSwAFSn");
	this.shape_40.setTransform(-0.6062,319.275);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#333333").ss(2,1,1).p("EAAAhM+QgCP1gBP7QAAUuAAUxQABVsACVrQABSuADSp");
	this.shape_41.setTransform(-0.3875,319.275);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#333333").ss(2,1,1).p("EgABhM/QgBP3AAP5QAAUvABUxQAAVsABVrQABStACSr");
	this.shape_42.setTransform(-0.25,319.25);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#333333").ss(2,1,1).p("EgABhM/QAAP4AAP4QAAUwABUvQAAVsAAVrQABStABSs");
	this.shape_43.setTransform(-0.2,319.25);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#333333").ss(2,1,1).p("EgABhM/QABP4AAP4QAAUwAAUvQAAVsABVrQABStAASs");
	this.shape_44.setTransform(-0.175,319.25);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#333333").ss(2,1,1).p("EgABhM/QABP4AAP4QAAUwAAUvQABVsAAVrQABStAASs");
	this.shape_45.setTransform(-0.175,319.25);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape}]},1).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-52.8,-174.5,54.8,987.5);


(lib.lettres_FAT = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CC9900").s().p("AkVDvIBehzQBxBjBfAAQAqAAAZgSQAZgTgBgfQAAgfgagTQgagThLgSQh6geg5gtQg4gvAAhjQAAhkBHg2QBIg2BpAAQBHAABFAZQBGAYA1ArIhPByQhchFhfAAQgoABgWASQgWASAAAfQAAAfAbARQAcATBiAYQBkAYA3AwQA3AygBBdQAABfhGA6QhHA7hygBQimAAiFh6g");
	this.shape.setTransform(45.6,311.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#CC9900").s().p("AjvFeIAAq7IHfAAIAACJIlDAAIAACXIEzAAIAACIIkzAAIAAETg");
	this.shape_1.setTransform(-68.35,187.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.lettres_FAT, new cjs.Rectangle(-102.9,0,195.2,373.7), null);


(lib.jumper = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.instance = new lib.step2();
	this.instance.setTransform(82.8,315.65,1,1,-110.2677);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.jumper, new cjs.Rectangle(0,0,330.5,315.7), null);


(lib.hastaggraphicbranche = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AjQAuIAShbIDlAAICqAAIgSBbg");
	this.shape.setTransform(0,4.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.hastaggraphicbranche, new cjs.Rectangle(-20.8,0,41.7,9.2), null);


(lib.cta1_back = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CC9900").s().p("Ao1EsIAApXIRrAAIAAJXg");
	this.shape.setTransform(56.6,30);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.cta1_back, new cjs.Rectangle(0,0,113.2,60), null);


(lib.Btn_cliquable_area = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CC9900").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape.setTransform(0,30);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = getMCSymbolPrototype(lib.Btn_cliquable_area, new cjs.Rectangle(-113,0,226,60), null);


(lib.tai_chi = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_126 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(126).call(this.frame_126).wait(1));

	// Calque_3
	this.instance = new lib.jumper();
	this.instance.setTransform(1735.95,-723,1,1,0,0,0,165.2,157.8);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(24).to({_off:false},0).to({scaleX:0.5011,scaleY:0.5011,x:-921.4,y:164},80).to({_off:true},1).wait(22));

	// Calque_2
	this.instance_1 = new lib.mob();
	this.instance_1.setTransform(594.95,174.15,1,1,0,0,0,0.4,-1.8);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(7).to({_off:false},0).to({regY:-1.7,rotation:23.9296,x:366.5,y:174.6},23).to({regX:0.6,regY:-1.8,scaleX:0.9999,scaleY:0.9999,rotation:10.4539,x:330.15,y:174.7},18).to({regY:-1.7,rotation:29.1229,x:128.95,y:175.3},26).to({regX:0.7,regY:-1.6,scaleX:0.9998,scaleY:0.9998,rotation:0.4188,x:64.95,y:175.55},9).to({regX:0.6,regY:-1.5,scaleX:0.9997,scaleY:0.9997,rotation:20.8587,x:-133.6,y:176.05},26).to({regX:0.5,regY:-1.4,scaleX:0.9998,scaleY:0.9998,rotation:-1.6127,x:-209.45,y:176.1},8).to({rotation:20.7983,x:-343.5,y:176.25},7).to({_off:true},1).wait(2));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1004.1,-880.8,2905.3,1123.8999999999999);


(lib.masqueTexte = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// masqueTexte_1_ligne
	this.instance = new lib.masqueTexte_1_ligne();
	this.instance.setTransform(-218.8,191.5,0.0079,1,0,0,0,0,25.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(6).to({_off:false},0).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.0079,x:218.8},14).wait(1));

	// masqueTexte_1_ligne
	this.instance_1 = new lib.masqueTexte_1_ligne();
	this.instance_1.setTransform(-218.8,136.1,0.0079,1,0,0,0,0,25.5);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(4).to({_off:false},0).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.0079,x:218.8},14).to({_off:true},1).wait(2));

	// masqueTexte_1_ligne
	this.instance_2 = new lib.masqueTexte_1_ligne();
	this.instance_2.setTransform(-218.8,80.8,0.0079,1,0,0,0,0,25.5);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(2).to({_off:false},0).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.0079,x:218.8},14).to({_off:true},1).wait(4));

	// masqueTexte_1_ligne
	this.instance_3 = new lib.masqueTexte_1_ligne();
	this.instance_3.setTransform(-218.8,25.5,0.0079,1,0,0,0,0,25.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({scaleX:1,x:0},15).wait(4).to({scaleX:0.0079,x:218.8},14).to({_off:true},1).wait(6));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-220.5,-1.9,441.1,219);


(lib.hashtag_graphic = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(64));

	// hastag graphic branche
	this.instance = new lib.hastaggraphicbranche();
	this.instance.setTransform(37.35,51.9,1.0844,0.9952,0,-78.3276,102.1252,0.1,4.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({x:6.45,y:50.05},17,cjs.Ease.quartOut).to({_off:true},5).wait(3).to({_off:false},0).to({_off:true},4).wait(2).to({_off:false,x:26.45},0).to({_off:true},5).wait(28));

	// hastag graphic branche
	this.instance_1 = new lib.hastaggraphicbranche();
	this.instance_1.setTransform(-41.65,49.9,1.0844,0.9952,0,-78.3276,102.1252,0.1,4.6);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off:false},0).to({x:-7.4,y:49.8},18,cjs.Ease.quartOut).to({_off:true},4).wait(5).to({_off:false},0).to({_off:true},4).wait(31));

	// hastag graphic branche
	this.instance_2 = new lib.hastaggraphicbranche();
	this.instance_2.setTransform(2.7,21.7,1,1,0,0,0,0,4.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({x:-2.3,y:58.7},13,cjs.Ease.quartOut).to({_off:true},11).wait(3).to({_off:false,scaleX:2.3297,scaleY:1.0257,skewX:-12.8379,x:25.5},0).to({_off:true},4).wait(33));

	// hastag graphic branche
	this.instance_3 = new lib.hastaggraphicbranche();
	this.instance_3.setTransform(-7.3,77.7,1,1,0,0,0,0,4.6);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({x:0.7,y:41.7},14,cjs.Ease.quartOut).to({_off:true},4).wait(7).to({_off:false,regX:0.1,scaleX:1.9808,scaleY:1.0147,skewX:-9.7728,x:19.9,y:49.7},0).to({_off:true},4).wait(4).to({_off:false,regX:0,scaleX:1,scaleY:1,skewX:0,x:0.7,y:41.7},0).to({_off:true},4).wait(21));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-50.9,0,126.1,82.3);


(lib.cta0 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{rollOver:1,rollOut:16});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_15 = function() {
		this.stop();
	}
	this.frame_30 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(15).call(this.frame_15).wait(15).call(this.frame_30).wait(1));

	// Calque_5
	this.instance = new lib.cta1_back();
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({_off:false},0).to({alpha:1},14).wait(1).to({alpha:0},13).wait(2));

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.008)").s().p("Ao0EsIAApXIRpAAIAAJXg");
	this.shape.setTransform(56.5,30);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(31));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,113.2,60);


(lib.Btn_next = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"rollOver":55,"rollOut":70});

	// timeline functions:
	this.frame_54 = function() {
		this.stop();
	}
	this.frame_69 = function() {
		this.stop();
	}
	this.frame_84 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(54).call(this.frame_54).wait(15).call(this.frame_69).wait(15).call(this.frame_84).wait(1));

	// Calque_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.008)").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape.setTransform(0,29.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(85));

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgIAqIAAhDIgYAAIAAgQIBBAAIAAAQIgXAAIAABDg");
	this.shape_1.setTransform(42.85,29.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AAUAqIgngzIAAAzIgTAAIAAhTIASAAIAoA1IAAg1IASAAIAABTg");
	this.shape_2.setTransform(26.3,29.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAZAqIgHgSIgjAAIgHASIgUAAIAkhTIARAAIAkBTgAgKAIIAUAAIgKgYg");
	this.shape_3.setTransform(8.925,29.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgHAqIgihTIAVAAIAUA0IAVg0IAVAAIgiBTg");
	this.shape_4.setTransform(-7.675,29.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgIAqIAAhTIARAAIAABTg");
	this.shape_5.setTransform(-21.775,29.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgYAgQgLgKAAgRIAAguIATAAIAAAtQAAAKAEAGQAFAGAHgBQAIABAEgGQAFgGABgKIAAgtIASAAIAAAuQAAASgKAJQgKALgQgBQgOABgKgLg");
	this.shape_6.setTransform(-36.25,29.15);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AggAcIALgNQANAMALAAQAFAAADgCQADgCAAgFQAAgDgDgCQgDgCgJgDQgOgEgHgEQgHgGAAgMQAAgMAJgGQAIgHAMAAQAJABAIADQAIACAHAGIgKANQgLgIgLAAQgEAAgDADQgDABAAAEQAAAEAEACQADACALADQAMADAHAFQAGAGAAALQAAAMgIAGQgJAIgOgBQgSABgQgQg");
	this.shape_7.setTransform(-52.625,29.05);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#333333").ss(1,1,1).p("AxpAAMAjTAAA");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(85));

	// Layer_2
	this.instance = new lib.Btn_cliquable_area();
	this.instance.setTransform(0,29.5,1,1,0,0,0,0,30);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({alpha:0.0117},46).wait(6).to({y:30},0).to({alpha:1},14).wait(1).to({alpha:0.0117},13).wait(2));

	// BG
	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("rgba(0,0,0,0.098)").s().p("AxpEsIAApXMAjTAAAIAAJXg");
	this.shape_9.setTransform(0,29.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_9).wait(85));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-114,-1,228,61);


(lib.Btn_detail = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_54 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(54).call(this.frame_54).wait(1));

	// lines
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#333333").ss(1,1,1).p("AAAkoIDSAAIOYAAAAAEtIAApVAxpkoIRpAAADSkoIAAgE");
	this.shape.setTransform(0,29.7);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(55));

	// Isolation Mode
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#FFFFFF").ss(2,1,1).p("ABYgmIhYBNIhXhN");
	this.shape_1.setTransform(59.2,26.95);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhYhLICxAAIAACXIixAAg");
	this.shape_2.setTransform(58.975,30.65);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AhJBZQgJAAgDgDQgDgEAAgIIAAiTQAAgIADgEQAEgDAIAAICUAAQAIAAADADQADAEAAAIIAACTQAAAIgDAEQgDADgJAAgAgUBAIAbAAIAAgMIACgoQAAgGAEgEQADgDAGAAQALgBABAOIACAzIAcAAIAAg+QgCgZgcgCQgRAAgLAMIgBgJIgZAAgAg+A/IAaAAIAAhWIgaAAgAg9g+QgFAFgBAHQAAAIAFAFQAFAGAIAAQAHAAAFgFQAFgFABgHQAAgIgFgFQgFgGgIAAQgHAAgFAFg");
	this.shape_3.setTransform(-57.475,30.425);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(55));

	// zones
	this.cta1 = new lib.cta0();
	this.cta1.name = "cta1";
	this.cta1.setTransform(56.5,29.5,1,1,0,0,0,56.5,30);

	this.timeline.addTween(cjs.Tween.get(this.cta1).wait(55));

	// zones
	this.cta0 = new lib.cta0();
	this.cta0.name = "cta0";
	this.cta0.setTransform(-56.5,29.5,1,1,0,0,0,56.5,30);

	this.timeline.addTween(cjs.Tween.get(this.cta0).wait(55));

	// Layer_2
	this.instance = new lib.Btn_cliquable_area();
	this.instance.setTransform(0,29.5,1,1,0,0,0,0,30);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({alpha:0.0117},46).wait(6));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-114,-1.4,228,62.199999999999996);


(lib.bloc_visuel = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":63});

	// timeline functions:
	this.frame_62 = function() {
		this.stop();
	}
	this.frame_89 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(62).call(this.frame_62).wait(27).call(this.frame_89).wait(1));

	// masque VISUEL (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("EgjcBDvMAAAiHdMBG5AAAMAAACHdg");
	mask.setTransform(-227.125,-19);

	// Calque_2
	this.instance = new lib.steph_1();
	this.instance.setTransform(-228,841,1,1,0,0,0,250,435);
	this.instance.alpha = 0;

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:21,alpha:0.3984},61,cjs.Ease.quartOut).wait(7).to({rotation:2.741,x:282.05,y:55.05,alpha:1},20,cjs.Ease.quartIn).to({_off:true},1).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1484.6,-570.9,2389.2,1153.6999999999998);


(lib.bloc_video = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{open:1,"close":28});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_27 = function() {
		this.stop();
	}
	this.frame_28 = function() {
		this.croix_1.gotoAndPlay("close");
		this.croix_2.gotoAndPlay("close");
		//this.gif_mc.gotoAndPlay("close");
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(27).call(this.frame_27).wait(1).call(this.frame_28).wait(25));

	// Layer_3
	this.instance = new lib.masque_generique();
	this.instance.setTransform(2.95,-0.55,0.0377,14.292,0,0,0,78.4,17.1);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(6).to({_off:false},0).wait(2).to({regX:77.9,scaleX:3.6959,x:287.9},10,cjs.Ease.quartIn).to({regX:81.8,scaleX:0.0079,x:579.6},9,cjs.Ease.quartOut).wait(1).to({regX:77.9,scaleX:3.6959,x:287.9},6,cjs.Ease.quartIn).to({regX:78.4,scaleX:0.0128,x:1},7,cjs.Ease.quartOut).to({_off:true},1).wait(11));

	// VIDEO
	this.videoContainer = new lib.video();
	this.videoContainer.name = "videoContainer";
	this.videoContainer.setTransform(23.7,158.25,0.6587,0.6587,0,0,0,0.3,480.2);
	this.videoContainer.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.videoContainer).wait(18).to({alpha:1},0).to({_off:true},25).wait(10));

	// geometrie croix
	this.croix_2 = new lib.video_frame_croix();
	this.croix_2.name = "croix_2";
	this.croix_2.setTransform(40.25,180.8,1,1,180,0,0,188,96);

	this.croix_1 = new lib.video_frame_croix();
	this.croix_1.name = "croix_1";
	this.croix_1.setTransform(414,180.5,1,1,0,180,0,188,96);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.croix_1},{t:this.croix_2}]},17).to({state:[]},33).wait(3));

	// geometrie angle
	this.instance_1 = new lib.video_frame_angle();
	this.instance_1.setTransform(38.55,-257.9,1,1,0,0,180,188.1,96);

	this.instance_2 = new lib.video_frame_angle();
	this.instance_2.setTransform(231.1,-42.75,1,1,0,180,0,3.6,319.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_2},{t:this.instance_1}]},3).to({state:[]},40).wait(10));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-347.1,-793.5,1148.5,1510.2);


(lib.bloc_NEXT_btn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":40});

	// timeline functions:
	this.frame_39 = function() {
		this.stop();
	}
	this.frame_56 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(39).call(this.frame_39).wait(17).call(this.frame_56).wait(1));

	// CTA DETAIL
	this.btnNext = new lib.Btn_next();
	this.btnNext.name = "btnNext";
	this.btnNext.setTransform(-340.5,411.5,1,1,0,0,0,0,26.5);

	this.timeline.addTween(cjs.Tween.get(this.btnNext).to({y:350.5},39,cjs.Ease.quartInOut).wait(2).to({y:411.5},14,cjs.Ease.quartInOut).to({_off:true},1).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-454.5,0,228,444.5);


(lib.bloc_lignes = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":166,close_lines:209});

	// timeline functions:
	this.frame_165 = function() {
		this.stop();
	}
	this.frame_208 = function() {
		this.stop();
	}
	this.frame_229 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(165).call(this.frame_165).wait(43).call(this.frame_208).wait(21).call(this.frame_229).wait(1));

	// ligne
	this.instance = new lib.ligne();
	this.instance.setTransform(-454.05,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(166).to({x:-883},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:-454.05},0).wait(21));

	// ligne
	this.instance_1 = new lib.ligne();
	this.instance_1.setTransform(-227.05,406.5,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(119).to({y:320.5},40,cjs.Ease.quartInOut).wait(7).to({y:406.5},0).to({x:-883.2},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:-227.05,y:320.5},0).to({y:406.5},17,cjs.Ease.quartInOut).wait(4));

	// ligne
	this.instance_2 = new lib.ligne();
	this.instance_2.setTransform(-0.05,406.5,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(166).to({x:-883.4},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:-0.05},0).wait(21));

	// ligne
	this.instance_3 = new lib.ligne();
	this.instance_3.setTransform(226.95,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(124).to({y:320},40,cjs.Ease.quartInOut).to({_off:true},2).wait(43).to({_off:false},0).to({y:406},17,cjs.Ease.quartInOut).wait(4));

	// ligne
	this.instance_4 = new lib.ligne();
	this.instance_4.setTransform(454.1,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(166).to({x:-883.45},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:454.1},0).wait(21));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-884.8,-260.5,1339.9,1074);


(lib.bloc_hashtag = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_61 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(61).call(this.frame_61).wait(2));

	// Layer_1
	this.instance = new lib.hashtag_graphic("synched",0);
	this.instance.setTransform(-456.05,243.2,2.3169,2.3169,0,0,0,-0.1,50.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true},61).wait(2));

	// Layer_5 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_28 = new cjs.Graphics().p("A2QBpIAAjRMAshAAAIAADRg");
	var mask_graphics_32 = new cjs.Graphics().p("A1ZDSIAAmjMAqzAAAIAAGjg");
	var mask_graphics_40 = new cjs.Graphics().p("A4NExIAAphMAwbAAAIAAJhg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(28).to({graphics:mask_graphics_28,x:-343.525,y:265.45}).wait(4).to({graphics:mask_graphics_32,x:-348.975,y:255.025}).wait(8).to({graphics:mask_graphics_40,x:-330.975,y:245.55}).wait(23));

	// Layer_3
	this.instance_1 = new lib.txt_hashtag();
	this.instance_1.setTransform(-421.5,244.05,1,1,0,0,0,0,31.6);
	this.instance_1._off = true;

	var maskedShapeInstanceList = [this.instance_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(28).to({_off:false},0).to({_off:true},34).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-573.7,0,287.1,317.1);


(lib.bloc_CTA = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":17});

	// timeline functions:
	this.frame_16 = function() {
		this.stop();
	}
	this.frame_33 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(16).call(this.frame_16).wait(17).call(this.frame_33).wait(1));

	// CTA DETAIL
	this.ctas = new lib.Btn_detail();
	this.ctas.name = "ctas";
	this.ctas.setTransform(-340.5,411.5,1,1,0,0,0,0,26.5);

	this.timeline.addTween(cjs.Tween.get(this.ctas).to({y:350.5},16,cjs.Ease.quartInOut).wait(2).to({y:411.5},14,cjs.Ease.quartInOut).to({_off:true},1).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-454.5,0,228,445.8);


(lib.blocInfostextes = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":80});

	// timeline functions:
	this.frame_79 = function() {
		this.stop();
	}
	this.frame_120 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(79).call(this.frame_79).wait(41).call(this.frame_120).wait(1));

	// Layer_8
	this.instance = new lib.tai_chi();
	this.instance.setTransform(-224,221.05,0.546,0.546,0,0,0,-0.2,0.2);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(14).to({_off:false},0).to({_off:true},66).wait(41));

	// Bloc Hashtag
	this.instance_1 = new lib.bloc_hashtag();
	this.instance_1.setTransform(-391.5,211.2,0.9,0.9,0,0,0,-412.9,244.7);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(62).to({_off:false},0).to({_off:true},18).wait(41));

	// masque Titre
	this.instance_2 = new lib.masque_generique();
	this.instance_2.setTransform(-527.95,-13.9,0.0075,0.4711,0,0,0,0,17.2);
	this.instance_2._off = true;
	this.instance_2.filters = [new cjs.ColorFilter(0, 0, 0, 1, 204, 153, 0, 0)];
	this.instance_2.cache(-2,-2,161,38);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(34).to({_off:false},0).to({regX:0.5,scaleX:1.9223,x:-527.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.0128,x:-228.9},10,cjs.Ease.quartInOut).to({_off:true},1).wait(26).to({_off:false},0).to({regX:0.5,scaleX:1.9223,x:-527.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.0074,x:-528.7},10,cjs.Ease.quartInOut).to({_off:true},1).wait(21));

	// sous titre coloré
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CC9900").s().p("AAaA4IgzhFIAABFIgZAAIAAhvIAWAAIA2BHIAAhHIAZAAIAABvg");
	this.shape.setTransform(-376.65,-14.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#CC9900").s().p("AgpApQgRgQAAgZQAAgXARgRQARgRAYAAQAZAAARARQARARAAAXQAAAZgRAQQgRARgZAAQgYAAgRgRgAgXgYQgJAKAAAOQAAAPAJALQAKAKANAAQAOAAAKgKQAJgLAAgPQAAgOgJgKQgKgLgOAAQgNAAgKALg");
	this.shape_1.setTransform(-401.775,-14.675);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#CC9900").s().p("AgxA4IAAhvIAnAAQAdAAAPAPQAQAOAAAaQAAAZgPAQQgQAPgfAAgAgYAiIAPAAQAQAAAJgJQAJgJAAgQQAAgPgJgJQgJgJgSAAIgNAAg");
	this.shape_2.setTransform(-426.25,-14.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#CC9900").s().p("AghAsQgOgNAAgZIAAg9IAZAAIAAA8QAAAOAHAIQAFAHAKABQALgBAGgHQAGgIAAgOIAAg8IAZAAIAAA9QAAAZgNANQgOANgVgBQgUABgNgNg");
	this.shape_3.setTransform(-450.7,-14.55);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CC9900").s().p("AAiA4IgLgYIguAAIgJAYIgbAAIAwhvIAXAAIAwBvgAgNAKIAbAAIgOgfg");
	this.shape_4.setTransform(-474.65,-14.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#CC9900").s().p("AgjA4IAAhvIAZAAIAABYIAuAAIAAAXg");
	this.shape_5.setTransform(-496.675,-14.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#CC9900").s().p("AgiApQgRgQgBgYQABgZARgQQAQgRAZAAQAbAAATAVIgQASQgMgPgRAAQgNAAgLAJQgKAJABAPQAAAPAJAKQAJAKAMAAQATAAAMgPIAQARQgUAVgZAAQgZAAgQgRg");
	this.shape_6.setTransform(-519.45,-14.65);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#CC9900").s().p("AgdAgQgNgMAAgUQAAgTANgNQANgMARABQARAAANAKQAMALAAASIAAANIg+AAQACAGAGAFQAGAFAHAAQANAAAIgJIANAOQgOAOgUABQgTAAgMgNgAgLgTQgGAEgBAIIAlAAQgBgJgFgDQgFgFgHAAQgGAAgGAFg");
	this.shape_7.setTransform(-368.725,-34.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#CC9900").s().p("AAQAsIAAgwQAAgUgOAAQgHAAgFAFQgFAGAAAKIAAAvIgYAAIAAhVIAYAAIAAAJQALgLAMAAQAOAAAJAKQAJAJAAAQIAAA0g");
	this.shape_8.setTransform(-390.725,-34.875);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#CC9900").s().p("AgdAlQgJgJAAgMQAAgOAKgGQAJgGARAAIARAAIAAAAQAAgOgPgBQgFABgHACQgGADgFADIgMgPQASgMAVAAQAPgBAKAJQAKAHAAARIAAA2IgXAAIAAgLQgJAMgMABQgOgBgKgHgAgNAPQgBAEAEADQADADAGAAQAGgBAFgEQAFgDAAgHIAAgEIgPAAQgNAAAAAJg");
	this.shape_9.setTransform(-412.85,-34.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#CC9900").s().p("AAQA7IAAgvQAAgTgOAAQgHAAgFAGQgFAEAAAKIAAAuIgYAAIAAh1IAYAAIAAAqQALgLAMAAQAOAAAJAJQAJAKAAAPIAAA0g");
	this.shape_10.setTransform(-434.425,-36.425);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#CC9900").s().p("AgrA7IAAh0IAYAAIAAAKQALgLAOAAQAPAAALAMQAMANAAATQAAASgMANQgLANgQAAQgPAAgJgNIAAAqgAgOgfQgGAHAAAKQAAALAGAFQAGAHAIAAQAIAAAGgHQAGgFAAgLQAAgKgGgHQgGgHgIAAQgIAAgGAHg");
	this.shape_11.setTransform(-456.675,-33.325);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#CC9900").s().p("AgdAzQgNgMAAgUQAAgTANgMQANgMARAAQARAAANAKQAMALAAASIAAANIg+AAQACAHAGAEQAGAFAHAAQANAAAIgJIANAPQgOAOgUAAQgTAAgMgNgAgLAAQgGADgBAIIAlAAQgBgIgFgDQgFgEgHAAQgGAAgGAEgAgOgiIATgdIAZALIgWASg");
	this.shape_12.setTransform(-479.175,-36.725);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#CC9900").s().p("AgMAwQgJgIABgOIAAgnIgKAAIAAgRIAKAAIAAgaIAWAAIAAAaIAWAAIAAARIgWAAIAAAmQAAAFADADQADADADAAQAHAAAFgHIAJARQgMALgMAAQgLAAgJgJg");
	this.shape_13.setTransform(-499.45,-36.025);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#CC9900").s().p("AgrAmIAOgSQATAQAOAAQAHgBAEgCQAEgDAAgFQAAgGgFgCQgEgDgLgEQgUgEgJgGQgJgIAAgQQAAgPAMgKQALgIAQAAQAMAAALAEQALAEAIAHIgMASQgPgLgPAAQgGAAgDADQgEADAAAFQAAAFAEADQAFACAPAFQAQAEAJAGQAIAIAAAPQAAAPgLAJQgLAKgTAAQgZAAgVgUg");
	this.shape_14.setTransform(-520.375,-36.15);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},46).to({state:[]},43).to({state:[]},1).wait(31));

	// masque Titre (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_17 = new cjs.Graphics().p("EgpOArJMAAAhWRMAvJAAAMAAABWRg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(17).to({graphics:mask_graphics_17,x:-263.875,y:-6.1095}).wait(24).to({graphics:null,x:0,y:0}).wait(80));

	// Layer_5
	this.instance_3 = new lib.lettres_FAT();
	this.instance_3.setTransform(-433.85,-73.05,1,1,0,0,0,0,186.8);
	this.instance_3._off = true;

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#CC9900").s().p("AjiFdIAAq5ICcAAIAAIvIEpAAIAACKg");
	this.shape_15.setTransform(-234.7,131.75);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#CC9900").s().p("AjiFeIAAq6ICcAAIAAIuIEpAAIAACMg");
	this.shape_16.setTransform(-465.1,7.85);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#CC9900").s().p("AjWEPQhVhSAAiXIAAmGICdAAIAAGBQAABUAnAxQAnAwBBAAQBBAAAngwQAngxgBhUIAAmBICdAAIAAGGQAACYhVBSQhUBRiCAAQiDAAhUhSg");
	this.shape_17.setTransform(-385.65,-115.65);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#CC9900").s().p("AjvFdIAAq5IHfAAIAACIIlDAAIAACYIEzAAIAACHIkzAAIAAESg");
	this.shape_18.setTransform(-502.2,-116);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#CC9900").s().p("AhNFdIAAkTIjymmICpAAICWEEICXkEICpAAIjyGmIAAETg");
	this.shape_19.setTransform(-333.3,106.75);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#CC9900").s().p("AByFeIibjgIhsAAIAADgIicAAIAAq7IEJAAQCjAABFA3QBFA3AAB7QAACmiFAxICyD7gAiVgIIByAAQBQAAAcgaQAdgZAAg5QAAg4gegWQgdgUhLgBIh1AAg");
	this.shape_20.setTransform(-230.45,-17.15);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#CC9900").s().p("AkEECQhqhoAAiaQAAiZBqhoQBqhnCaAAQCbAABqBnQBqBoAACZQAACahqBoQhqBnibAAQiaAAhqhngAiSieQg8BCAABcQAABeA8BCQA8BBBWAAQBYAAA8hBQA8hCAAheQAAhcg8hCQg8hChYAAQhWAAg8BCg");
	this.shape_21.setTransform(-476.5,-17.6);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#CC9900").s().p("AhNFeIAAo0IjGAAIAAiHIInAAIAACHIjGAAIAAI0g");
	this.shape_22.setTransform(-334.8,-141);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#CC9900").s().p("AkUDvIBchzQBxBkBfgBQArAAAZgSQAYgTAAgfQABgfgagTQgagThNgSQh6geg4gtQg4gugBhkQAAhkBIg2QBIg2BqAAQBFAABGAZQBHAYA0ArIhQByQhbhFhgAAQgmABgXASQgXASABAfQgBAfAcARQAbATBjAYQBjAYA3AwQA3AyAABdQAABehGA7QhHA7hyAAQilAAiFh7g");
	this.shape_23.setTransform(-486.45,-141.4);

	var maskedShapeInstanceList = [this.instance_3,this.shape_15,this.shape_16,this.shape_17,this.shape_18,this.shape_19,this.shape_20,this.shape_21,this.shape_22,this.shape_23];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_3}]},17).to({state:[{t:this.instance_3}]},14).to({state:[{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15}]},1).to({state:[{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19}]},5).to({state:[]},4).to({state:[]},39).wait(41));
	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(17).to({_off:false},0).to({x:-276.85},14,cjs.Ease.quartOut).to({_off:true},1).wait(89));

	// masque Titre
	this.instance_4 = new lib.masque_generique();
	this.instance_4.setTransform(221.1,-163.9,0.0128,0.4711,0,0,0,0,17.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).to({regX:0.5,scaleX:1.9223,x:-75.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.0074,x:-68.7},10,cjs.Ease.quartInOut).to({_off:true},1).wait(101));

	// Layer_7
	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#FFFFFF").ss(1,1,1).p("AW3ISI4mONI1HzBILk58IcQC+g");
	this.shape_24.setTransform(-22.675,68.825);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#FFFFFF").ss(1,1,1).p("AnFmSICWm8ISbU+I7XFfIAAgB");
	this.shape_25.setTransform(-240,-49.65);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AqGJxIGnzhIAAAAINmPeI0MEDg");
	this.shape_26.setTransform(-262.95,-27.55);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#FFFFFF").ss(1,1,1).p("ALnNZI3NtWIXJtbg");
	this.shape_27.setTransform(-577,114.55);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_24}]},93).to({state:[{t:this.shape_26},{t:this.shape_25}]},4).to({state:[{t:this.shape_27}]},3).to({state:[]},6).to({state:[]},1).wait(14));

	// masque Titre
	this.instance_5 = new lib.masque_generique();
	this.instance_5.setTransform(-228.9,-13.9,0.0128,0.4711,0,0,0,0,17.2);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(4).to({_off:false},0).to({regX:0.5,scaleX:1.9223,x:-527.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.0074,x:-528.7},10,cjs.Ease.quartInOut).to({_off:true},1).wait(14).to({_off:false,scaleX:0.0128,x:-228.9,y:-33.9},0).to({regX:0.5,scaleX:1.9223,x:-527.8},9,cjs.Ease.quartInOut).to({regX:0,scaleX:0.0074,x:-528.7},10,cjs.Ease.quartInOut).to({_off:true},1).wait(63));

	// Layer_10
	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#CC9900").s().p("AgLA4IAAgsIgnhDIAbAAIAXAqIAYgqIAbAAIgmBDIAAAsg");
	this.shape_28.setTransform(-450.375,-14.15);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#CC9900").s().p("AgoA4IAAhvIBQAAIAAAXIg3AAIAAAWIAxAAIAAAUIgxAAIAAAYIA4AAIAAAWg");
	this.shape_29.setTransform(-504.65,-14.15);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#CC9900").s().p("AgJA0QgEgEAAgGQAAgFAEgFQAEgEAFAAQAGAAAEAEQAEAFAAAFQAAAGgEAEQgEAFgGAAQgFAAgEgFgAgJAVIgEg5IAAgUIAbAAIAAAUIgFA5g");
	this.shape_30.setTransform(-349.875,-14.075);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#CC9900").s().p("AgpApQgRgQAAgZQAAgXARgRQARgRAYAAQAZAAARARQARARAAAXQAAAZgRAQQgRARgZAAQgYAAgRgRgAgXgYQgJAKAAAOQAAAPAJALQAKAKANAAQAOAAAKgKQAJgLAAgPQAAgOgJgKQgKgLgOAAQgNAAgKALg");
	this.shape_31.setTransform(-387.175,-14.225);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#CC9900").s().p("AgjA4IAAhvIAZAAIAABYIAuAAIAAAXg");
	this.shape_32.setTransform(-442.275,-14.15);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#CC9900").s().p("AAWA4IAAgsIgrAAIAAAsIgZAAIAAhvIAZAAIAAAvIArAAIAAgvIAZAAIAABvg");
	this.shape_33.setTransform(-487.85,-14.15);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#CC9900").s().p("AgjA4IAAhvIAZAAIAABYIAuAAIAAAXg");
	this.shape_34.setTransform(-474.675,-14.15);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_29,p:{x:-504.65}},{t:this.shape_28}]},13).to({state:[{t:this.shape_33,p:{x:-487.85}},{t:this.shape_29,p:{x:-464.25}},{t:this.shape_32,p:{x:-442.275}},{t:this.shape_31,p:{x:-387.175}},{t:this.shape_30,p:{x:-349.875}}]},5).to({state:[{t:this.shape_33,p:{x:-520.25}},{t:this.shape_29,p:{x:-496.65}},{t:this.shape_34},{t:this.shape_32,p:{x:-453.575}},{t:this.shape_31,p:{x:-430.875}},{t:this.shape_30,p:{x:-393.575}}]},5).to({state:[]},11).to({state:[]},46).wait(41));

	// Layer_11
	this.instance_6 = new lib.masqueTexte("synched",0);
	this.instance_6.setTransform(-340,105.8,0.85,0.85,0,0,0,0.1,108.8);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(37).to({_off:false},0).to({_off:true},40).wait(3).to({_off:false,rotation:180},0).to({_off:true},40).wait(1));

	// Layer_12
	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AgqBmQgagIgVgRIAbgmQAhAaAmAAQALAAAHgEQAGgFAAgHQABgIgKgGQgKgHgQgFIgcgKQgJgEgLgIQgZgOAAgeQAAgdAYgTQAYgTAlAAQAmAAAmAaIgWAoQgcgUgdAAQgaAAAAAQQAAAIAJAFQAJAEATAHIAdAKQAIADAMAHQAXANAAAgQAAAggYATQgXATgkAAQgXAAgagJg");
	this.shape_35.setTransform(-359.45,133.425);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAggeQAhgeArAAQAtAAAfAbQAfAbAAAuIAAAfIicAAQADASAPALQAQALATAAQAfAAAVgVIAiAlQglAigxAAQgwAAgggegAgdgyQgPALgCATIBdAAQgCgUgNgKQgMgKgSAAQgQAAgPAKg");
	this.shape_36.setTransform(-381.75,133.425);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AgdCUIAAkoIA7AAIAAEog");
	this.shape_37.setTransform(-400,129.35);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("AhJBbQgXgUAAghQAAghAYgPQAYgQApAAIAsAAIAAgCQAAgigkAAQgPAAgRAGQgRAGgNAKIgbgnQArgfA2AAQAnAAAZAUQAYATAAArIAACIIg4AAIAAgaQgXAdgiAAQgiAAgXgUgAgkAmQAAALAJAHQAIAGAQAAQAQAAAMgKQAMgKAAgQIAAgLIglAAQgkAAAAAXg");
	this.shape_38.setTransform(-418.2,133.425);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AgfB5QgWgUAAgkIAAhiIgZAAIAAgrIAZAAIAAhAIA7AAIAABAIA0AAIAAArIg0AAIAABgQAAAMAHAHQAGAHAJAAQARAAAMgPIAWAqQgcAZgfAAQgeAAgVgUg");
	this.shape_39.setTransform(-437.85,130.375);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("AgdCXIAAjWIA7AAIAADWgAgYhaQgKgKAAgPQAAgPAKgKQAKgKAOAAQAPAAAKAKQAKAKAAAPQAAAPgKAKQgKAKgPAAQgOAAgKgKg");
	this.shape_40.setTransform(-453.025,129.075);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("Ag1CQQgZgIgSgOIAXguQAdAXAhAAQAgAAAUgRQATgQAAgkQgdAggpAAQgqAAgdgdQgdgcAAgvQAAgwAegfQAegeAmAAQAlAAAaAgIAAgdIA8AAIAAC7QAAAdgKAWQgKAWgRANQghAbgtAAQgYAAgZgIgAgihVQgQAPAAAZQAAAaAPAQQAPAOAVAAQAWAAAOgOQAPgQAAgZQAAgagPgPQgOgPgVAAQgVAAgPAPg");
	this.shape_41.setTransform(-472.775,137.525);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("AgdCXIAAjWIA7AAIAADWgAgYhaQgKgKAAgPQAAgPAKgKQAKgKAOAAQAPAAAKAKQAKAKAAAPQAAAPgKAKQgKAKgPAAQgOAAgKgKg");
	this.shape_42.setTransform(-491.375,129.075);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("AhQB2QgeggAAgwQAAgwAdgeQAegeAngBQAmAAAZAcIAAhqIA8AAIAAEoIg8AAIAAgcQgaAfglAAQgmAAgegggAgigDQgPARAAAaQAAAaAPARQAQAQATAAQAVAAAPgQQAOgRAAgaQAAgagOgRQgPgSgVAAQgUAAgPASg");
	this.shape_43.setTransform(-511.125,129.5);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFFFFF").s().p("AgqBmQgagIgVgRIAagmQAjAaAkAAQAMAAAGgEQAIgFgBgHQAAgIgJgGQgKgHgQgFIgcgKQgJgEgLgIQgZgOAAgeQAAgdAYgTQAYgTAlAAQAmAAAmAaIgXAoQgcgUgcAAQgaAAAAAQQAAAIAJAFQAIAEAUAHIAdAKQAJADAMAHQAWANAAAgQAAAggXATQgYATgkAAQgXAAgagJg");
	this.shape_44.setTransform(-272.8,84.675);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFFFFF").s().p("AApBtIAAh4QAAgyglAAQgRAAgNANQgNANAAAZIAAB3Ig9AAIAAjWIA9AAIAAAYQAbgbAgAAQAjAAAXAYQAWAYAAAnIAACCg");
	this.shape_45.setTransform(-295.5,84.525);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FFFFFF").s().p("AhQBPQghggAAgvQAAguAhggQAgggAwAAQAxAAAgAgQAhAgAAAuQAAAvghAgQggAggxAAQgwAAgggggAglgqQgPARAAAZQAAAbAPAQQAPARAWAAQAXAAAPgRQAPgQAAgbQAAgZgPgRQgPgRgXAAQgWAAgPARg");
	this.shape_46.setTransform(-321.375,84.675);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFFFFF").s().p("AgdCXIAAjWIA7AAIAADWgAgYhaQgKgKAAgPQAAgPAKgKQAKgKAOAAQAPAAAKAKQAKAKAAAPQAAAPgKAKQgKAKgPAAQgOAAgKgKg");
	this.shape_47.setTransform(-340.175,80.325);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FFFFFF").s().p("AgfB5QgWgUAAgkIAAhiIgZAAIAAgrIAZAAIAAhAIA7AAIAABAIA1AAIAAArIg1AAIAABgQABAMAGAHQAGAHAJAAQARAAAMgPIAWAqQgcAZgfAAQgeAAgVgUg");
	this.shape_48.setTransform(-354.25,81.625);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FFFFFF").s().p("AhJBbQgXgUAAghQAAghAYgPQAYgQApAAIAsAAIAAgCQAAgigkAAQgPAAgRAGQgSAGgMAKIgbgnQArgfA1AAQAnAAAZAUQAZATAAArIAACIIg4AAIAAgaQgXAdgiAAQgiAAgXgUgAgkAmQAAALAIAHQAKAGAQAAQAOAAAMgKQANgKAAgQIAAgLIglAAQgkAAAAAXg");
	this.shape_49.setTransform(-375.95,84.675);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FFFFFF").s().p("ABtBtIAAh0QAAgygkAAQgSAAgNANQgOANAAAZIAABzIg7AAIAAh0QAAgagIgMQgIgMgRAAQgSAAgNANQgNANAAAZIAABzIg8AAIAAjWIA8AAIAAAYQAZgbAfAAQAVAAAQALQAQANAIARQANgUAVgLQAVgKAVAAQAmAAAXAWQAXAXAAAqIAACCg");
	this.shape_50.setTransform(-406.875,84.525);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FFFFFF").s().p("AgdCXIAAjWIA7AAIAADWgAgYhaQgKgKAAgPQAAgPAKgKQAKgKAOAAQAPAAAKAKQAKAKAAAPQAAAPgKAKQgKAKgPAAQgOAAgKgKg");
	this.shape_51.setTransform(-432.475,80.325);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FFFFFF").s().p("AAoBtIAAh4QABgyglAAQgRAAgNANQgOANAAAZIAAB3Ig7AAIAAjWIA7AAIAAAYQAcgbAgAAQAjAAAXAYQAWAYAAAnIAACCg");
	this.shape_52.setTransform(-450.95,84.525);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FFFFFF").s().p("AhJBbQgXgUAAghQAAghAYgPQAYgQAqAAIArAAIAAgCQAAgigkAAQgPAAgRAGQgSAGgLAKIgbgnQAqgfA1AAQAoAAAYAUQAZATAAArIAACIIg4AAIAAgaQgXAdghAAQgjAAgXgUgAgkAmQAAALAIAHQAKAGAPAAQAPAAAMgKQANgKAAgQIAAgLIglAAQgkAAAAAXg");
	this.shape_53.setTransform(-476.2,84.675);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FFFFFF").s().p("AgjAzIASgrQgNgIABgPQAAgQAJgJQAJgKANAAQAPAAAJAJQAKAJgBAOQAAAOgIANIgcAqg");
	this.shape_54.setTransform(-492.6,72.525);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FFFFFF").s().p("AhQB2QgeghAAgvQAAgwAdgfQAegdAnAAQAmgBAZAcIAAhqIA8AAIAAEoIg8AAIAAgcQgaAfglAAQgmAAgegggAgigDQgPARAAAaQAAAaAPARQAQAQATAAQAVAAAPgQQAOgRAAgbQAAgagOgQQgPgSgVAAQgUAAgPASg");
	this.shape_55.setTransform(-511.125,80.75);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FFFFFF").s().p("Ag9BtIAAjWIA8AAIAAAdQAKgOASgJQARgJASAAIAAA4IgLAAQgbAAgNATQgMATAAAeIAABdg");
	this.shape_56.setTransform(-292.925,35.775);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FFFFFF").s().p("AhMBXQgYgWAAgsIAAiBIA9AAIAAB0QAAAyAkgBQARABANgNQANgNAAgZIAAhzIA9AAIAADWIg9AAIAAgdQgWAggiAAQgkAAgYgWg");
	this.shape_57.setTransform(-314.925,36.1);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#FFFFFF").s().p("AhLBRQgggeAAgzQAAgyAggeQAhgeAsAAQAsAAAfAbQAfAbAAAuIAAAfIicAAQADASAPALQAQALATAAQAfAAAVgVIAhAlQgkAigxAAQgwAAgggegAgdgyQgPALgDATIBeAAQgCgUgNgKQgMgKgRAAQgSAAgOAKg");
	this.shape_58.setTransform(-339.85,35.925);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#FFFFFF").s().p("AgfB5QgWgUAAgkIAAhiIgZAAIAAgrIAZAAIAAhAIA7AAIAABAIA1AAIAAArIg1AAIAABgQABAMAGAHQAGAHAJAAQARAAAMgPIAWAqQgcAZgfAAQgeAAgVgUg");
	this.shape_59.setTransform(-360.55,32.875);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#FFFFFF").s().p("AhJBbQgXgUAAghQAAghAYgPQAYgQApAAIAsAAIAAgCQAAgigkAAQgPAAgRAGQgSAGgMAKIgbgnQArgfA1AAQAoAAAYAUQAZATAAArIAACIIg4AAIAAgaQgXAdgiAAQgiAAgXgUgAgkAmQAAALAIAHQAKAGAQAAQAOAAAMgKQANgKAAgQIAAgLIglAAQgkAAAAAXg");
	this.shape_60.setTransform(-382.25,35.925);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#FFFFFF").s().p("Ag9BtIAAjWIA8AAIAAAdQAKgOASgJQARgJASAAIAAA4IgLAAQgbAAgNATQgMATAAAeIAABdg");
	this.shape_61.setTransform(-401.025,35.775);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#FFFFFF").s().p("AhQBPQghggAAgvQAAguAhggQAgggAwAAQAxAAAgAgQAhAgAAAuQAAAvghAgQggAggxAAQgwAAgggggAglgqQgPARAAAZQAAAbAPAQQAPARAWAAQAXAAAPgRQAPgQAAgbQAAgZgPgRQgPgRgXAAQgWAAgPARg");
	this.shape_62.setTransform(-423.075,35.925);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#FFFFFF").s().p("AgdCVIAAkoIA7AAIAAEog");
	this.shape_63.setTransform(-441.85,31.85);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#FFFFFF").s().p("AhuCUIAAkkIA8AAIAAAZQAcgcAkAAQAmAAAdAfQAeAgAAAvQAAAvgeAgQgdAggngBQgnABgYggIAABqgAgkhQQgPASAAAaQAAAaAPAQQAPAQAUABQAUgBAQgQQAPgPAAgbQAAgagPgSQgPgRgUAAQgVAAgPARg");
	this.shape_64.setTransform(-460.425,39.65);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#FFFFFF").s().p("AAoBsIgpg8IgoA8IhGAAIBLhtIhHhqIBIAAIAkA6IAmg6IBGAAIhKBqIBNBtg");
	this.shape_65.setTransform(-486,35.95);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#FFFFFF").s().p("AhmCMIAAkXIDJAAIAAA4IiLAAIAAA5IB8AAIAAA0Ih8AAIAAA6ICPAAIAAA4g");
	this.shape_66.setTransform(-510.1,32.725);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35}]},57).to({state:[]},43).to({state:[]},1).wait(20));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-652.3,-259.8,878.4,520.5);


// stage content:
(lib.FS_stephane_mobile = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":515});

	// timeline functions:
	this.frame_129 = function() {
		this.video_mc.gotoAndPlay("open");
	}
	this.frame_139 = function() {
		//var frequency = 60;
		//stage.enableMouseOver(frequency);
		
		// EVENT DE CLIC POUR LA SUITE
		this.next_btn.addEventListener("click", fl_ClickToGoToAndPlayFromFrame.bind(this));
		this.next_btn.addEventListener("mouseover", fl_MouseOverHandler.bind(this));
		this.next_btn.addEventListener("mouseout", fl_MouseOutHandler.bind(this));
		
		function fl_ClickToGoToAndPlayFromFrame() {
			//this.gotoAndPlay("close");
		
			var event = new Event('next');
			this.dispatchEvent(event);
			event = null;
			
			this.next_btn.btnNext.gotoAndPlay('rollOut');
		
		}
		function fl_MouseOverHandler() { this.next_btn.btnNext.gotoAndPlay('rollOver');}
		function fl_MouseOutHandler() {	this.next_btn.btnNext.gotoAndPlay('rollOut');}
		
		
		// EVENT DE CLIC POUR LE LIEN 1
		this.cta_mc.ctas.cta0.addEventListener("click", onCTA0.bind(this));
		this.cta_mc.ctas.cta0.addEventListener("mouseover", onCTA0_MouseOverHandler.bind(this));
		this.cta_mc.ctas.cta0.addEventListener("mouseout", onCTA0_MouseOutHandler.bind(this));
		
		function onCTA0() {
			var event = new Event('cta0');
			this.dispatchEvent(event);
			event = null;
			
			this.cta_mc.ctas.cta0.gotoAndPlay('rollOut');
		}
		
		function onCTA0_MouseOverHandler() {	this.cta_mc.ctas.cta0.gotoAndPlay('rollOver');}
		function onCTA0_MouseOutHandler() {	this.cta_mc.ctas.cta0.gotoAndPlay('rollOut');}
		
		// EVENT DE CLIC POUR LE LIEN 2
		this.cta_mc.ctas.cta1.addEventListener("click", onCTA1.bind(this));
		this.cta_mc.ctas.cta1.addEventListener("mouseover", onCTA1_MouseOverHandler.bind(this));
		this.cta_mc.ctas.cta1.addEventListener("mouseout", onCTA1_MouseOutHandler.bind(this));
		
		function onCTA1() {
			var event = new Event('cta1');
			this.dispatchEvent(event);
			event = null;
		
			this.cta_mc.ctas.cta1.gotoAndPlay('rollOut');
		}
		
		function onCTA1_MouseOverHandler() {	this.cta_mc.ctas.cta1.gotoAndPlay('rollOver');}
		function onCTA1_MouseOutHandler() {	this.cta_mc.ctas.cta1.gotoAndPlay('rollOut');}
	}
	this.frame_179 = function() {
		//this.visuel_mc.gotoAndPlay("close");
	}
	this.frame_339 = function() {
		//this.textes_mc.gotoAndPlay("close");
	}
	this.frame_514 = function() {
		//PAUSE
		this.stop();
	}
	this.frame_516 = function() {
		this.textes_mc.gotoAndPlay("close");
		this.cta_mc.gotoAndPlay("close");
		this.next_btn.gotoAndPlay("close");
		this.lignes_mc.gotoAndPlay("close_lines");
		this.visuel_mc.gotoAndPlay("close");
		this.video_mc.gotoAndPlay("close");
	}
	this.frame_562 = function() {
		// EVENT DE FIN D'ANIM
		var event = new Event('closed');
		this.dispatchEvent(event);		
		event = null;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(129).call(this.frame_129).wait(10).call(this.frame_139).wait(40).call(this.frame_179).wait(160).call(this.frame_339).wait(175).call(this.frame_514).wait(2).call(this.frame_516).wait(46).call(this.frame_562).wait(1));

	// CTA NEXT
	this.next_btn = new lib.bloc_NEXT_btn();
	this.next_btn.name = "next_btn";
	this.next_btn.setTransform(359.2,861.15,1.06,1.06,0,0,0,-340.4,403.9);
	this.next_btn._off = true;

	this.timeline.addTween(cjs.Tween.get(this.next_btn).wait(128).to({_off:false},0).wait(435));

	// CTA Contact
	this.cta_mc = new lib.bloc_CTA();
	this.cta_mc.name = "cta_mc";
	this.cta_mc.setTransform(121.45,861.35,1.06,1.06,0,0,0,-340.4,404.1);
	this.cta_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.cta_mc).wait(119).to({_off:false},0).to({_off:true},420).wait(24));

	// BLOC INFOS TEXTE
	this.textes_mc = new lib.blocInfostextes();
	this.textes_mc.name = "textes_mc";
	this.textes_mc.setTransform(791.95,235.5,1,1,0,0,0,222.1,-164);
	this.textes_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.textes_mc).wait(17).to({_off:false},0).wait(546));

	// VIDEO
	this.video_mc = new lib.bloc_video();
	this.video_mc.name = "video_mc";
	this.video_mc.setTransform(359.4,1081.65,0.6269,0.6267,0,0,0,227.5,765.5);

	this.timeline.addTween(cjs.Tween.get(this.video_mc).wait(563));

	// VISUEL
	this.visuel_mc = new lib.bloc_visuel();
	this.visuel_mc.name = "visuel_mc";
	this.visuel_mc.setTransform(1059.25,471.55,1.0502,1.0502,0,0,0,552.6,26.7);
	this.visuel_mc._off = true;

	this.timeline.addTween(cjs.Tween.get(this.visuel_mc).wait(17).to({_off:false},0).wait(546));

	// lignes
	this.lignes_mc = new lib.bloc_lignes();
	this.lignes_mc.name = "lignes_mc";
	this.lignes_mc.setTransform(240.75,453.9,0.525,1,0,0,0,0.7,405.9);

	this.timeline.addTween(cjs.Tween.get(this.lignes_mc).wait(563));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(216.9,293.5,576.1,1490.1);
// library properties:
lib.properties = {
	id: '15B796517CE85844BC299421C2929151',
	width: 480,
	height: 840,
	fps: 60,
	color: "#000000",
	opacity: 1.00,
	manifest: [
		{src:"images/step2.png?1581604943961", id:"step2"},
		{src:"images/steph.png?1581604943961", id:"steph"},
		{src:"images/stephane.jpg?1581604943961", id:"stephane"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.StageGL();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['15B796517CE85844BC299421C2929151'] = {
	getStage: function() { return exportRoot.stage; },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


an.makeResponsive = function(isResp, respDim, isScale, scaleType, domContainers) {		
	var lastW, lastH, lastS=1;		
	window.addEventListener('resize', resizeCanvas);		
	resizeCanvas();		
	function resizeCanvas() {			
		var w = lib.properties.width, h = lib.properties.height;			
		var iw = window.innerWidth, ih=window.innerHeight;			
		var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;			
		if(isResp) {                
			if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {                    
				sRatio = lastS;                
			}				
			else if(!isScale) {					
				if(iw<w || ih<h)						
					sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==1) {					
				sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==2) {					
				sRatio = Math.max(xRatio, yRatio);				
			}			
		}			
		domContainers[0].width = w * pRatio * sRatio;			
		domContainers[0].height = h * pRatio * sRatio;			
		domContainers.forEach(function(container) {				
			container.style.width = w * sRatio + 'px';				
			container.style.height = h * sRatio + 'px';			
		});			
		stage.scaleX = pRatio*sRatio;			
		stage.scaleY = pRatio*sRatio;			
		lastW = iw; lastH = ih; lastS = sRatio;            
		stage.tickOnUpdate = false;            
		stage.update();            
		stage.tickOnUpdate = true;		
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;