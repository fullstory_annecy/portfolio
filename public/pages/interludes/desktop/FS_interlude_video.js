(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.video = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("EiV/BUYMAAAiovMEr/AAAMAAACovg");
	this.shape.setTransform(960,540);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.video, new cjs.Rectangle(0,0,1920,1080), null);


(lib.title_txt = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.text = new cjs.Text("MOTION DESIGN REALISE POUR DEGRISOGONO", "bold 15px 'Montserrat'", "#FFFFFF");
	this.text.name = "text";
	this.text.textAlign = "center";
	this.text.lineHeight = 22;
	this.text.lineWidth = 416;
	this.text.parent = this;
	this.text.setTransform(210.1,2);

	this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

}).prototype = getMCSymbolPrototype(lib.title_txt, new cjs.Rectangle(0,0,420.2,54), null);


(lib.mask5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("Egx/A8AMAAAh3/MBj/AAAMAAAB3/g");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.mask5, new cjs.Rectangle(-320,-384,640,768), null);


(lib.mask4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("Egx/A8AMAAAh3/MBj/AAAMAAAB3/g");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.mask4, new cjs.Rectangle(-320,-384,640,768), null);


(lib.mask3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("Egx/A8AMAAAh3/MBj/AAAMAAAB3/g");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.mask3, new cjs.Rectangle(-320,-384,640,768), null);


(lib.mask2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("Egx/A8AMAAAh3/MBj/AAAMAAAB3/g");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.mask2, new cjs.Rectangle(-320,-384,640,768), null);


(lib.mask1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("Egx/A8AMAAAh3/MBj/AAAMAAAB3/g");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.mask1, new cjs.Rectangle(-320,-384,640,768), null);


(lib.mask0 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("Egx/A8AMAAAh3/MBj/AAAMAAAB3/g");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.mask0, new cjs.Rectangle(-320,-384,640,768), null);


(lib.ligne = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#333333").ss(1,1,1).p("EAAAg/bMAAAB+3");
	this.shape.setTransform(0,406);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.ligne, new cjs.Rectangle(-1,-1,2,814), null);


(lib.Btn_cliquable_area = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CC9933").s().p("Ehj/AEsIAApXMDH/AAAIAAJXg");
	this.shape.setTransform(640,30);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Btn_cliquable_area, new cjs.Rectangle(0,0,1280,60), null);


(lib.video_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.videoContainer = new lib.video();
	this.videoContainer.name = "videoContainer";
	this.videoContainer.parent = this;
	this.videoContainer.setTransform(0.1,-0.1,0.711,0.711,0,0,0,960.1,539.9);

	this.timeline.addTween(cjs.Tween.get(this.videoContainer).wait(1));

}).prototype = getMCSymbolPrototype(lib.video_mc, new cjs.Rectangle(-682.6,-384,1365.3,768), null);


(lib.lignes_all = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// ligne
	this.instance = new lib.ligne();
	this.instance.parent = this;
	this.instance.setTransform(-454,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// ligne
	this.instance_1 = new lib.ligne();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-227,406.5,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

	// ligne
	this.instance_2 = new lib.ligne();
	this.instance_2.parent = this;
	this.instance_2.setTransform(0,406.5,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(1));

	// ligne
	this.instance_3 = new lib.ligne();
	this.instance_3.parent = this;
	this.instance_3.setTransform(227,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(1));

	// ligne
	this.instance_4 = new lib.ligne();
	this.instance_4.parent = this;
	this.instance_4.setTransform(454.1,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(1));

}).prototype = getMCSymbolPrototype(lib.lignes_all, new cjs.Rectangle(-454.5,-0.5,909.2,813.5), null);


(lib.Btn_next = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{rollOver:55,rollOut:70});

	// timeline functions:
	this.frame_54 = function() {
		this.stop();
	}
	this.frame_69 = function() {
		this.stop();
	}
	this.frame_84 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(54).call(this.frame_54).wait(15).call(this.frame_69).wait(15).call(this.frame_84).wait(1));

	// ZONE
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.008)").s().p("Ehj/A8AMAAAh3/MDH/AAAMAAAB3/g");
	this.shape.setTransform(640,-324);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(85));

	// txt
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgeAqIAAhTIA8AAIAAARIgpAAIAAARIAkAAIAAAPIgkAAIAAASIAqAAIAAAQg");
	this.shape_1.setTransform(856.2,28.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgJAqIAAhDIgXAAIAAgQIBBAAIAAAQIgYAAIAABDg");
	this.shape_2.setTransform(840.4,28.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgIAqIAAhTIARAAIAABTg");
	this.shape_3.setTransform(826.8,28.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgZAgQgKgKAAgSIAAgtIATAAIAAAtQAAAKAFAGQAEAGAHgBQAIABAEgGQAGgGgBgKIAAgtIATAAIAAAtQAAATgKAJQgKAKgQAAQgPAAgKgKg");
	this.shape_4.setTransform(812.3,28.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AggAdIALgOQANAMALAAQAFAAADgDQADgBAAgEQAAgEgDgCQgDgCgJgDQgOgEgHgEQgHgGAAgMQAAgLAJgHQAIgHAMABQAJAAAIACQAIAEAHAFIgKANQgLgIgLAAQgEAAgDADQgDACAAADQAAAEAEACQADACALADQAMADAHAFQAGAGAAALQAAALgIAIQgJAGgOAAQgSAAgQgOg");
	this.shape_5.setTransform(795.9,28.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAZAqIgHgSIgjAAIgHASIgUAAIAkhTIARAAIAkBTgAgKAIIAUAAIgKgYg");
	this.shape_6.setTransform(768.6,28.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgaAqIAAhTIASAAIAABDIAjAAIAAAQg");
	this.shape_7.setTransform(753.1,28.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AAZA5IgHgSIgjAAIgHASIgUAAIAkhTIARAAIAkBTgAgKAWIAUAAIgKgXgAgHgjIgRgNIATgIIAOAVg");
	this.shape_8.setTransform(725.7,26.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AANAqIgRgbIgNAAIAAAbIgSAAIAAhTIAfAAQASAAAJAGQAIAIAAAOQABATgRAGIAWAegAgRAAIANAAQAKAAADgDQADgDAAgHQAAgHgDgDQgEgCgIAAIgOAAg");
	this.shape_9.setTransform(698.2,28.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgeAqIAAhTIA8AAIAAARIgpAAIAAARIAkAAIAAAPIgkAAIAAASIAqAAIAAAQg");
	this.shape_10.setTransform(681.7,28.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AggAdIALgOQANAMALAAQAFAAADgDQADgBAAgEQAAgEgDgCQgDgCgJgDQgOgEgHgEQgHgGAAgMQAAgLAJgHQAIgHAMABQAJAAAIACQAIAEAHAFIgKANQgLgIgLAAQgEAAgDADQgDACAAADQAAAEAEACQADACALADQAMADAHAFQAGAGAAALQAAALgIAIQgJAGgOAAQgSAAgQgOg");
	this.shape_11.setTransform(665.7,28.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AggAdIALgOQANAMALAAQAFAAADgDQADgBAAgEQAAgEgDgCQgDgCgJgDQgOgEgHgEQgHgGAAgMQAAgLAJgHQAIgHAMABQAJAAAIACQAIAEAHAFIgKANQgLgIgLAAQgEAAgDADQgDACAAADQAAAEAEACQADACALADQAMADAHAFQAGAGAAALQAAALgIAIQgJAGgOAAQgSAAgQgOg");
	this.shape_12.setTransform(650.1,28.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AAZAqIgHgSIgjAAIgHASIgUAAIAkhTIARAAIAkBTgAgKAIIAUAAIgKgYg");
	this.shape_13.setTransform(633.8,28.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AggAqIAAhTIAdAAQATAAAIAHQAJAIAAAQQAAAOgJAHQgJAIgSgBIgLAAIAAAYgAgOACIANAAQAIAAAEgCQADgEAAgHQAAgIgEgCQgFgEgIAAIgLAAg");
	this.shape_14.setTransform(617.6,28.4);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AANAqIgRgbIgNAAIAAAbIgSAAIAAhTIAfAAQASAAAJAGQAIAIAAAOQABATgRAGIAWAegAgRAAIANAAQAKAAADgDQADgDAAgHQAAgHgDgDQgEgCgIAAIgOAAg");
	this.shape_15.setTransform(590.2,28.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgYAgQgLgKAAgSIAAgtIATAAIAAAtQAAAKAEAGQAFAGAHgBQAIABAFgGQAEgGABgKIAAgtIASAAIAAAtQAAATgKAJQgKAKgQAAQgPAAgJgKg");
	this.shape_16.setTransform(573,28.5);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgfAfQgMgMAAgTQAAgSAMgMQAOgMARAAQATAAAMAMQANAMAAASQAAATgNAMQgMAMgTAAQgRAAgOgMgAgQgSQgIAIAAAKQAAAMAIAHQAGAIAKAAQALAAAHgIQAHgHAAgMQAAgKgHgIQgHgIgLAAQgKAAgGAIg");
	this.shape_17.setTransform(555.5,28.4);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AggAqIAAhTIAdAAQATAAAIAHQAJAIAAAQQAAAOgJAHQgJAIgSgBIgLAAIAAAYgAgOACIANAAQAIAAAEgCQADgEAAgHQAAgIgEgCQgFgEgIAAIgLAAg");
	this.shape_18.setTransform(538.8,28.4);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgiAqIAAgNIArg2IgqAAIAAgQIBEAAIAAANIgsA2IAsAAIAAAQg");
	this.shape_19.setTransform(511.3,28.4);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgeAqIAAhTIA8AAIAAARIgpAAIAAARIAkAAIAAAPIgkAAIAAASIAqAAIAAAQg");
	this.shape_20.setTransform(495.5,28.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgZAgQgKgKAAgSIAAgtIATAAIAAAtQAAAKAFAGQAEAGAHgBQAIABAEgGQAGgGAAgKIAAgtIASAAIAAAtQAAATgKAJQgKAKgQAAQgPAAgKgKg");
	this.shape_21.setTransform(478.9,28.5);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AAHAvQgGgEgDgHQgTAAgNgNQgMgMAAgRQAAgTAMgMQANgNATAAQARAAANANQANAMAAATQAAALgHALQgGALgMAEQAAABABAAQAAABABAAQAAABABAAQAAAAABABIAFABIAEgBIAEgBIAFgFIAJALQgKALgPAAQgIAAgHgEgAgUgZQgHAHAAAMQAAAKAHAHQAHAJALgBQAJABAIgJQAHgHAAgKQAAgMgHgHQgIgJgJAAQgLAAgHAJg");
	this.shape_22.setTransform(461.7,29.1);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgIAqIAAhTIARAAIAABTg");
	this.shape_23.setTransform(446.5,28.4);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgaAqIAAhTIASAAIAABDIAjAAIAAAQg");
	this.shape_24.setTransform(433.5,28.4);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgaAfQgMgMAAgSQAAgTAMgMQANgMASAAQAUAAAOAPIgMAOQgIgLgOAAQgJAAgIAGQgHAIAAAKQAAAMAHAHQAHAHAJAAQAOAAAJgLIAMANQgPAPgTAAQgSAAgNgMg");
	this.shape_25.setTransform(417.4,28.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(85));

	// Layer_1
	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("rgba(51,51,51,0.2)").ss(1,1,1).p("Ehj/AADMDH/gAF");
	this.shape_26.setTransform(640,-0.2);

	this.timeline.addTween(cjs.Tween.get(this.shape_26).wait(85));

	// Calque_5
	this.instance = new lib.Btn_cliquable_area();
	this.instance.parent = this;
	this.instance.setTransform(0,29.5,1,1,0,0,0,0,30);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("rgba(0,0,0,0.008)").s().p("Ehj/AEsIAApXMDH/AAAIAAJXg");
	this.shape_27.setTransform(640,29.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance}]},3).to({state:[{t:this.instance}]},46).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.instance}]},5).to({state:[{t:this.instance}]},14).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},14).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({alpha:0.012},46).to({_off:true},1).wait(5).to({_off:false},0).to({alpha:1},14).wait(1).to({alpha:0.012},14).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1,-708,1282,768);


(lib.bloc_video = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{over:1});

	// timeline functions:
	this.frame_83 = function() {
		//PAUSE
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(83).call(this.frame_83).wait(1));

	// masque video copy 5
	this.instance = new lib.mask5();
	this.instance.parent = this;
	this.instance.setTransform(1459.9,390,0.355,1.042);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(11).to({x:1207},31,cjs.Ease.quartInOut).wait(3).to({y:1187.5},28).wait(11));

	// masque video copy 4
	this.instance_1 = new lib.mask4();
	this.instance_1.parent = this;
	this.instance_1.setTransform(980.6,-407.6,0.355,1.042,0,0,0,0.1,0);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1).to({x:980.1,y:384.9},31,cjs.Ease.quartInOut).wait(16).to({y:-440.1},28).wait(8));

	// masque video copy 3
	this.instance_2 = new lib.mask3();
	this.instance_2.parent = this;
	this.instance_2.setTransform(753.2,1187.7,0.355,1.042,0,0,0,0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(6).to({y:384.5},31,cjs.Ease.quartInOut).wait(13).to({x:1468.2},28).wait(6));

	// masque video copy 2
	this.instance_3 = new lib.mask2();
	this.instance_3.parent = this;
	this.instance_3.setTransform(526.3,-449.5,0.355,1.042);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(9).to({y:387.5},31,cjs.Ease.quartInOut).wait(7).to({y:1185},28).wait(9));

	// masque video copy
	this.instance_4 = new lib.mask1();
	this.instance_4.parent = this;
	this.instance_4.setTransform(299.5,1187.7,0.355,1.042,0,0,0,0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(3).to({y:386.6},31,cjs.Ease.quartInOut).wait(16).to({x:-168},28).wait(6));

	// masque video
	this.instance_5 = new lib.mask0();
	this.instance_5.parent = this;
	this.instance_5.setTransform(-137.2,390,0.355,1.042,0,0,0,-0.1,0);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(14).to({x:73},31,cjs.Ease.quartInOut).wait(7).to({y:-407.5},28).wait(4));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-250.9,-849.5,1824.6,2437.1);


(lib.bloc_title = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{open:1,close:20});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_19 = function() {
		this.stop();
	}
	this.frame_37 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(19).call(this.frame_19).wait(18).call(this.frame_37).wait(1));

	// line
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(128,128,128,0.2)").s().p("ArFADIAAgFIWLAAIAAAFg");
	this.shape.setTransform(211,34.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(38));

	// CTA_DETAIL
	this.title_txt = new lib.title_txt();
	this.title_txt.name = "title_txt";
	this.title_txt.parent = this;
	this.title_txt.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.title_txt).wait(1).to({alpha:1},11).wait(13).to({alpha:0},10).to({_off:true},1).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,420.2,99.1);


(lib.bloc_NEXT_btn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":20});

	// timeline functions:
	this.frame_19 = function() {
		this.stop();
	}
	this.frame_37 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(19).call(this.frame_19).wait(18).call(this.frame_37).wait(1));

	// CTA DETAIL
	this.btnNext = new lib.Btn_next();
	this.btnNext.name = "btnNext";
	this.btnNext.parent = this;
	this.btnNext.setTransform(0,411.5,1,1,0,0,0,0,26.5);

	this.timeline.addTween(cjs.Tween.get(this.btnNext).to({y:352.5},19,cjs.Ease.quartInOut).wait(2).to({y:411.5},14,cjs.Ease.quartInOut).to({_off:true},1).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-0.5,-323,1281,768);


// stage content:
(lib.FS_interlude_video = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":110});

	// timeline functions:
	this.frame_29 = function() {
		this.title_mc.gotoAndPlay("open");
	}
	this.frame_109 = function() {
		this.stop();
		
		//var frequency = 60;
		//stage.enableMouseOver(frequency);
		
		// EVENT DE CLIC POUR LA SUITE
		this.next_btn.addEventListener("click", fl_ClickToGoToAndPlayFromFrame.bind(this));
		this.next_btn.addEventListener("mouseover", fl_MouseOverHandler.bind(this));
		this.next_btn.addEventListener("mouseout", fl_MouseOutHandler.bind(this));
		
		
		function fl_ClickToGoToAndPlayFromFrame()
		{
			//this.gotoAndPlay("close");
			
			var event = new Event('next');
			this.dispatchEvent(event);		
			event = null;
			
			this.next_btn.btnNext.gotoAndPlay('rollOut');
			
		}
		function fl_MouseOverHandler(){	this.next_btn.btnNext.gotoAndPlay('rollOver'); }
		function fl_MouseOutHandler(){	this.next_btn.btnNext.gotoAndPlay('rollOut'); }
	}
	this.frame_110 = function() {
		this.next_btn.gotoAndPlay("close");
		this.masks_mc.gotoAndPlay("over");
		this.title_mc.gotoAndPlay("close");
	}
	this.frame_179 = function() {
		this.stop();
		
		// EVENT DE FIN D'ANIM
		var event = new Event('closed');
		this.dispatchEvent(event);		
		event = null;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(29).call(this.frame_29).wait(80).call(this.frame_109).wait(1).call(this.frame_110).wait(69).call(this.frame_179).wait(1));

	// CTA NEXT
	this.next_btn = new lib.bloc_NEXT_btn();
	this.next_btn.name = "next_btn";
	this.next_btn.parent = this;
	this.next_btn.setTransform(-340.5,786.9,1,1,0,0,0,-340.5,403.9);
	this.next_btn._off = true;

	this.timeline.addTween(cjs.Tween.get(this.next_btn).wait(89).to({_off:false},0).wait(91));

	// Calque_1
	this.title_mc = new lib.bloc_title();
	this.title_mc.name = "title_mc";
	this.title_mc.parent = this;
	this.title_mc.setTransform(640.1,87,1,1,0,0,0,210.1,27);

	this.timeline.addTween(cjs.Tween.get(this.title_mc).wait(180));

	// lines
	this.lignes_mc = new lib.lignes_all();
	this.lignes_mc.name = "lignes_mc";
	this.lignes_mc.parent = this;
	this.lignes_mc.setTransform(640,384.3,1,1,0,0,0,0,406.3);

	this.timeline.addTween(cjs.Tween.get(this.lignes_mc).to({alpha:0.199},88).wait(1).to({y:301.3},20,cjs.Ease.quartInOut).wait(1).to({y:384.3,alpha:1},19,cjs.Ease.quartInOut).wait(51));

	// masque
	this.masks_mc = new lib.bloc_video();
	this.masks_mc.name = "masks_mc";
	this.masks_mc.parent = this;
	this.masks_mc.setTransform(227.5,764.9,1,1,0,0,0,227.5,764.9);

	this.timeline.addTween(cjs.Tween.get(this.masks_mc).wait(180));

	// VIDEO
	this.video_mc = new lib.video_mc();
	this.video_mc.name = "video_mc";
	this.video_mc.parent = this;
	this.video_mc.setTransform(640.6,384);
	this.video_mc.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.video_mc).to({_off:true},43).wait(1).to({_off:false,alpha:1},0).to({_off:true},112).wait(24));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(389.1,-465.5,1824.6,2437.1);
// library properties:
lib.properties = {
	id: '257B9F9B73D91D4AB082E9823AC91623',
	width: 1280,
	height: 768,
	fps: 60,
	color: "#000000",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['257B9F9B73D91D4AB082E9823AC91623'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;