(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.action = function() {
	this.initialize(img.action);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,640,335);


(lib.danger = function() {
	this.initialize(img.danger);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1080,1080);


(lib.fire_0000_Layer8 = function() {
	this.initialize(img.fire_0000_Layer8);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,50,79);


(lib.fire_0001_Layer9 = function() {
	this.initialize(img.fire_0001_Layer9);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,50,79);


(lib.fire_0002_Layer10 = function() {
	this.initialize(img.fire_0002_Layer10);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,50,79);


(lib.fire_0003_Layer11 = function() {
	this.initialize(img.fire_0003_Layer11);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,50,79);


(lib.fire_0004_Layer12 = function() {
	this.initialize(img.fire_0004_Layer12);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,50,79);


(lib.fire_0005_Layer13 = function() {
	this.initialize(img.fire_0005_Layer13);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,50,79);


(lib.fire_0006_Layer14 = function() {
	this.initialize(img.fire_0006_Layer14);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,50,79);


(lib.fire_0007_Layer15 = function() {
	this.initialize(img.fire_0007_Layer15);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,50,79);


(lib.fire_0008_Layer16 = function() {
	this.initialize(img.fire_0008_Layer16);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,50,79);


(lib.fire_0009_Layer17 = function() {
	this.initialize(img.fire_0009_Layer17);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,50,79);


(lib.fire_0010_Layer1 = function() {
	this.initialize(img.fire_0010_Layer1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,50,79);


(lib.fire_0011_Layer2 = function() {
	this.initialize(img.fire_0011_Layer2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,50,79);


(lib.fire_0012_Layer3 = function() {
	this.initialize(img.fire_0012_Layer3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,50,79);


(lib.fire_0013_Layer4 = function() {
	this.initialize(img.fire_0013_Layer4);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,50,79);


(lib.fire_0014_Layer5 = function() {
	this.initialize(img.fire_0014_Layer5);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,50,79);


(lib.fire_0015_Layer6 = function() {
	this.initialize(img.fire_0015_Layer6);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,50,79);


(lib.fire_0016_Layer7 = function() {
	this.initialize(img.fire_0016_Layer7);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,50,79);


(lib.vitesse = function() {
	this.initialize(img.vitesse);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,640,640);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.vignette_vitesse = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.vitesse();
	this.instance.parent = this;
	this.instance.setTransform(-320,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.vignette_vitesse, new cjs.Rectangle(-320,0,640,640), null);


(lib.txt_vitesse = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_69 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(69).call(this.frame_69).wait(1));

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("EgimAPRIAA+hMBFNAAAIAAehg");
	this.shape.setTransform(-181.175,198.875);
	this.shape._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(14).to({_off:false},0).to({_off:true},4).wait(35).to({_off:false},0).to({_off:true},4).wait(13));

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("EhJoADpIAAnRMCTRAAAIAAHRg");
	this.shape_1.setTransform(-311.925,-39.35);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("EgupAFkIAArHMBdTAAAIAALHg");
	this.shape_2.setTransform(163.75,242.95);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("EgoIAPRIAA+hMBQRAAAIAAehg");
	this.shape_3.setTransform(223.65,109.525);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("Eg1UAPRIAA+hMBqpAAAIAAehg");
	this.shape_4.setTransform(139.325,109.525);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("EhJoAPRIAA+hMCTRAAAIAAehg");
	this.shape_5.setTransform(9.175,109.525);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AoLLHIAA2NIQAAAIAAEbIrCAAIAAElIJ7AAIAAENIp7AAIAAEnILZAAIAAEZg");
	this.shape_6.setTransform(370.95,105.875);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AozHlIC9joQDmDKDCAAQBXAAAygmQAzgmAAhAQAAhAg2glQg1gmicgnQj3g7hzhdQh0hfAAjKQAAjLCShuQCRhtDZAAQCPAACOAwQCOAxBrBaIihDoQi5iNjFAAQhPAAguAmQguAlAAA+QAAA/A4AkQA5AlDIAyQDLAxBwBjQBvBkAAC/QAADAiPB3QiPB2jqAAQlRAAkOj6g");
	this.shape_7.setTransform(235.025,105.075);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AozHlIC9joQDmDKDCAAQBXAAAygmQAzgmAAhAQAAhAg2glQg1gmicgnQj3g7hzhdQh0hfAAjKQAAjLCShuQCRhtDZAAQCPAACOAwQCOAxBrBaIihDoQi5iNjFAAQhPAAguAmQguAlAAA+QAAA/A4AkQA5AlDIAyQDLAxBwBjQBvBkAAC/QAADAiPB3QiPB2jqAAQlRAAkOj6g");
	this.shape_8.setTransform(105.175,105.075);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AoLLHIAA2NIQAAAIAAEbIrCAAIAAElIJ7AAIAAENIp7AAIAAEnILZAAIAAEZg");
	this.shape_9.setTransform(-21.8,105.875);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AidLHIAAx6ImUAAIAAkTIRjAAIAAETImUAAIAAR6g");
	this.shape_10.setTransform(-153.55,105.875);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AieLHIAA2NIE9AAIAAWNg");
	this.shape_11.setTransform(-248.75,105.875);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AiFLHIo82NIFYAAIFqOJIFpuJIFYAAIo7WNg");
	this.shape_12.setTransform(-352.15,105.875);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1}]}).to({state:[{t:this.shape_2}]},3).to({state:[{t:this.shape_3}]},3).to({state:[{t:this.shape_4}]},3).to({state:[{t:this.shape_5}]},3).to({state:[{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6}]},3).to({state:[{t:this.shape_5}]},40).to({state:[{t:this.shape_4}]},3).to({state:[{t:this.shape_3}]},3).to({state:[{t:this.shape_2}]},3).to({state:[{t:this.shape_1}]},4).to({state:[]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-783.2,-62.6,1263.8000000000002,359.20000000000005);


(lib.txt_violence = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_69 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(69).call(this.frame_69).wait(1));

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("EgimAPRIAA+hMBFNAAAIAAehg");
	this.shape.setTransform(-181.175,198.875);
	this.shape._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(14).to({_off:false},0).to({_off:true},4).wait(35).to({_off:false},0).to({_off:true},4).wait(13));

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("EhJoADpIAAnRMCTRAAAIAAHRg");
	this.shape_1.setTransform(-311.925,-39.35);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("EgupAFkIAArHMBdTAAAIAALHg");
	this.shape_2.setTransform(163.75,242.95);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("EgoIAPRIAA+hMBQRAAAIAAehg");
	this.shape_3.setTransform(223.65,109.525);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("Eg1UAPRIAA+hMBqpAAAIAAehg");
	this.shape_4.setTransform(139.325,109.525);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("EhJoAPRIAA+hMCTRAAAIAAehg");
	this.shape_5.setTransform(9.175,109.525);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AoLLHIAA2NIQAAAIAAEbIrCAAIAAElIJ8AAIAAENIp8AAIAAEnILZAAIAAEZg");
	this.shape_6.setTransform(478.75,105.875);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AnFIPQjVjOABk9QgBk7DZjUQDYjTE6AAQFdAADtEKIjGDgQiXi7jgAAQixAAiAB1Qh+B0gBDGQABDHB4B4QB3B3CrAAQDpAACVi5IDLDSQjxEQlJAAQlJAAjUjQg");
	this.shape_7.setTransform(335.55,105.075);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AFTLHIqkt6IAAN6Ik+AAIAA2NIEpAAIK5OTIAAuTIE9AAIAAWNg");
	this.shape_8.setTransform(176.775,105.875);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AoLLHIAA2NIQAAAIAAEbIrCAAIAAElIJ8AAIAAENIp8AAIAAEnILZAAIAAEZg");
	this.shape_9.setTransform(30.45,105.875);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AnNLHIAA2NIE+AAIAARyIJdAAIAAEbg");
	this.shape_10.setTransform(-91.4,105.875);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AoSIMQjYjTAAk5QAAk4DYjTQDYjSE6AAQE8AADXDSQDYDTgBE4QABE5jYDTQjXDSk8AAQk6AAjYjSgAkqlDQh5CHAAC9QAAC/B5CFQB6CFCxAAQCzAAB6iFQB6iFAAi/QAAi9h6iHQh6iGizAAQixAAh6CGg");
	this.shape_11.setTransform(-239.15,104.975);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AieLHIAA2NIE9AAIAAWNg");
	this.shape_12.setTransform(-356.5,105.875);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AiFLHIo82NIFYAAIFqOJIFpuJIFYAAIo8WNg");
	this.shape_13.setTransform(-459.9,105.875);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1}]}).to({state:[{t:this.shape_2}]},3).to({state:[{t:this.shape_3}]},3).to({state:[{t:this.shape_4}]},3).to({state:[{t:this.shape_5}]},3).to({state:[{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6}]},3).to({state:[{t:this.shape_5}]},40).to({state:[{t:this.shape_4}]},3).to({state:[{t:this.shape_3}]},3).to({state:[{t:this.shape_2}]},3).to({state:[{t:this.shape_1}]},4).to({state:[]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-783.2,-62.6,1329.6,359.20000000000005);


(lib.txt_surInstagram = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_109 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(109).call(this.frame_109).wait(1));

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("EgimAPRIAA+hMBFNAAAIAAehg");
	this.shape.setTransform(-181.175,198.875);
	this.shape._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(14).to({_off:false},0).to({_off:true},4).wait(75).to({_off:false},0).to({_off:true},4).wait(13));

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("EhJoADpIAAnRMCTRAAAIAAHRg");
	this.shape_1.setTransform(-311.925,-39.35);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("EgupAFkIAArHMBdTAAAIAALHg");
	this.shape_2.setTransform(163.75,242.95);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("EgoIAPRIAA+hMBQRAAAIAAehg");
	this.shape_3.setTransform(223.65,109.525);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("Eg1UAPRIAA+hMBqpAAAIAAehg");
	this.shape_4.setTransform(139.325,109.525);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("Ehy5APRIAA+hMDlyAAAIAAehg");
	this.shape_5.setTransform(29.15,109.525);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AHeLHIAAt+ImBMIIi7AAIl/sIIAAN+Ik+AAIAA2NIGtAAIFuMMIFxsMIGrAAIAAWNg");
	this.shape_6.setTransform(1189.675,105.875);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AGvLHIiFkzIpTAAIiEEzIlSAAIJm2NIEzAAIJmWNgAixB9IFjAAIixmcg");
	this.shape_7.setTransform(1016.85,105.875);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("ADnLHIk6nGIjcAAIAAHGIk9AAIAA2NIIbAAQFLAACNBwQCNBwAAD4QAAFVkOBkIFoH8gAkvgQIDoAAQChAAA7g1QA8g1AAhzQAAhzg+grQg8gqiZAAIjtAAg");
	this.shape_8.setTransform(872.75,105.875);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AnBIPQjUjOAAk9QAAk7DYjUQDZjTEyAAQEyAADhDIIilDvQhfhUhQgfQhRgfhdAAQi0AAh8B7Qh8B6AADJQAADKB3B5QB3B5CjAAQClAABuhAIAAmFIE/AAIAAH4QjUDrl1AAQk5AAjVjQg");
	this.shape_9.setTransform(716.075,105.075);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AGuLHIiEkzIpTAAIiEEzIlSAAIJm2NIEzAAIJmWNgAixB9IFjAAIixmcg");
	this.shape_10.setTransform(564.75,105.875);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AieLHIAAx6ImTAAIAAkTIRjAAIAAETImTAAIAAR6g");
	this.shape_11.setTransform(427.1,105.875);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AozHlIC9joQDmDKDCAAQBXAAAygmQAzgmAAhAQAAhAg2glQg1gmicgnQj3g7hzhdQh0hfAAjKQAAjLCShuQCRhtDZAAQCPAACOAwQCOAxBrBaIihDoQi5iNjFAAQhPAAguAmQguAlAAA+QAAA/A4AkQA5AlDIAyQDLAxBwBjQBvBkAAC/QAADAiPB3QiPB2jqAAQlRAAkOj6g");
	this.shape_12.setTransform(299.225,105.075);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AFTLHIqkt6IAAN6Ik+AAIAA2NIEpAAIK5OTIAAuTIE9AAIAAWNg");
	this.shape_13.setTransform(152.275,105.875);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AieLHIAA2NIE9AAIAAWNg");
	this.shape_14.setTransform(35.8,105.875);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AoLLHIAA2NIQAAAIAAEbIrCAAIAAElIJ8AAIAAENIp8AAIAAEnILZAAIAAEZg");
	this.shape_15.setTransform(-114.55,105.875);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("ADnLHIk6nGIjcAAIAAHGIk+AAIAA2NIIcAAQFLAACNBwQCNBwAAD4QAAFVkPBkIFoH8gAkvgQIDoAAQChAAA8g1QA7g1AAhzQAAhzg9grQg+gqiXAAIjuAAg");
	this.shape_16.setTransform(-252.75,105.875);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AidLHIAAx6ImUAAIAAkTIRjAAIAAETImUAAIAAR6g");
	this.shape_17.setTransform(-394.2,105.875);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AoSIMQjYjTABk5QgBk4DYjTQDXjSE8AAQE6AADYDSQDXDTABE4QgBE5jXDTQjYDSk6AAQk8AAjXjSgAkplDQh6CHgBC9QABC/B6CFQB6CFCwAAQCyAAB6iFQB7iFAAi/QAAi9h7iHQh6iGiyAAQiwAAh6CGg");
	this.shape_18.setTransform(-540.15,104.975);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AFTLHIqkt6IAAN6Ik+AAIAA2NIEpAAIK5OTIAAuTIE9AAIAAWNg");
	this.shape_19.setTransform(-707.275,105.875);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("ADnLHIk6nGIjcAAIAAHGIk+AAIAA2NIIcAAQFLAACNBwQCNBwAAD4QAAFVkPBkIFoH8gAkvgQIDoAAQChAAA8g1QA7g1AAhzQAAhzg9grQg+gqiXAAIjuAAg");
	this.shape_20.setTransform(-912.45,105.875);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("Am0InQisinAAkzIAAsaIE9AAIAAMQQAACrBQBjQBPBjCFAAQCGAABOhjQBPhjAAirIAAsQIE9AAIAAMaQAAE2irClQiqCmkLAAQkJAAising");
	this.shape_21.setTransform(-1068.5,106.575);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AozHlIC9joQDmDKDCAAQBXAAAygmQAzgmAAhAQAAhAg2glQg1gmicgnQj3g7hzhdQh0hfAAjKQAAjLCShuQCRhtDZAAQCPAACOAwQCOAxBrBaIihDoQi5iNjFAAQhPAAguAmQguAlAAA+QAAA/A4AkQA5AlDIAyQDLAxBwBjQBvBkAAC/QAADAiPB3QiPB2jqAAQlRAAkOj6g");
	this.shape_22.setTransform(-1210.925,105.075);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1}]}).to({state:[{t:this.shape_2}]},3).to({state:[{t:this.shape_3}]},3).to({state:[{t:this.shape_4}]},3).to({state:[{t:this.shape_5}]},3).to({state:[{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6}]},3).to({state:[{t:this.shape_5}]},80).to({state:[{t:this.shape_4}]},3).to({state:[{t:this.shape_3}]},3).to({state:[{t:this.shape_2}]},3).to({state:[{t:this.shape_1}]},4).to({state:[]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1279.1,-62.6,2570.3,359.20000000000005);


(lib.txt_etbienplus = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_109 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(109).call(this.frame_109).wait(1));

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("EgimAPRIAA+hMBFNAAAIAAehg");
	this.shape.setTransform(-181.175,198.875);
	this.shape._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(14).to({_off:false},0).to({_off:true},4).wait(75).to({_off:false},0).to({_off:true},4).wait(13));

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("EhJoADpIAAnRMCTRAAAIAAHRg");
	this.shape_1.setTransform(-311.925,-39.35);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("EgupAFkIAArHMBdTAAAIAALHg");
	this.shape_2.setTransform(163.75,242.95);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("EgoIAPRIAA+hMBQRAAAIAAehg");
	this.shape_3.setTransform(223.65,109.525);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("Eg1UAPRIAA+hMBqpAAAIAAehg");
	this.shape_4.setTransform(139.325,109.525);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("EhJoAPRIAA+hMCTRAAAIAAehg");
	this.shape_5.setTransform(29.1648,109.525,1.5602,1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AozHlIC9joQDmDKDCAAQBXAAAygmQAzgmAAhAQAAhAg2glQg1gmicgnQj3g7hzhdQh0hfAAjKQAAjLCShuQCRhtDZAAQCPAACOAwQCOAxBrBaIihDoQi5iNjFAAQhPAAguAmQguAlAAA+QAAA/A4AkQA5AlDIAyQDLAxBwBjQBvBkAAC/QAADAiPB3QiPB2jqAAQlRAAkOj6g");
	this.shape_6.setTransform(643.225,105.075);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("Am0InQisinAAkzIAAsaIE9AAIAAMQQAACrBPBjQBQBjCFAAQCGAABPhjQBOhjAAirIAAsQIE9AAIAAMaQAAE2irClQirCmkKAAQkJAAising");
	this.shape_7.setTransform(503.05,106.575);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AnNLHIAA2NIE+AAIAARyIJdAAIAAEbg");
	this.shape_8.setTransform(374.7,105.875);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("Ao0LHIAA2NIH3AAQFHAACVB/QCWB/AAEIQAAEGiaB7QiZB6k7AAIi9AAIAAGMgAj2AoIDWAAQCdAAA5g/QA5hBAAh7QAAh7hLg0QhJgzicAAIi1AAg");
	this.shape_9.setTransform(247.225,105.875);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AFTLHIqkt6IAAN6Ik+AAIAA2NIEpAAIK5OTIAAuTIE9AAIAAWNg");
	this.shape_10.setTransform(36.375,105.875);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AoLLHIAA2NIQAAAIAAEbIrCAAIAAElIJ8AAIAAENIp8AAIAAEnILZAAIAAEZg");
	this.shape_11.setTransform(-109.95,105.875);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AieLHIAA2NIE9AAIAAWNg");
	this.shape_12.setTransform(-213.25,105.875);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("Ao3LHIAA2NIIqAAQCQAABoAiQBpAjA1A7QBfBuAACKQAACnhrBRQglAdgPAHIgyAYQCEAbBOBaQBPBcAACGQAACUhmByQh2CEklAAgAj5G7IDaAAQCHAABEgiQBEgiAAhiQAAhjhJgfQhHggieAAIi7AAgAj5iGICWAAQCDAABBgdQA/gcAAhfQAAhfg6gdQg7gfiMAAIiYAAg");
	this.shape_13.setTransform(-312.575,105.875);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AidLHIAAx6ImUAAIAAkTIRjAAIAAETImTAAIAAR6g");
	this.shape_14.setTransform(-502.4,105.875);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AoLLHIAA2NIQAAAIAAEbIrCAAIAAElIJ8AAIAAENIp8AAIAAEnILZAAIAAEZg");
	this.shape_15.setTransform(-627.45,105.875);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("Ehy5APRIAA+hMDlyAAAIAAehg");
	this.shape_16.setTransform(29.15,109.525);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1}]}).to({state:[{t:this.shape_2}]},3).to({state:[{t:this.shape_3}]},3).to({state:[{t:this.shape_4}]},3).to({state:[{t:this.shape_5}]},3).to({state:[{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6}]},3).to({state:[{t:this.shape_16}]},80).to({state:[{t:this.shape_4}]},3).to({state:[{t:this.shape_3}]},3).to({state:[{t:this.shape_2}]},3).to({state:[{t:this.shape_1}]},4).to({state:[]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-783.2,-62.6,1547.7,359.20000000000005);


(lib.txt_danger = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_69 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(69).call(this.frame_69).wait(1));

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("EgimAPRIAA+hMBFNAAAIAAehg");
	this.shape.setTransform(-181.175,198.875);
	this.shape._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(14).to({_off:false},0).to({_off:true},4).wait(35).to({_off:false},0).to({_off:true},4).wait(13));

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("EhJoADpIAAnRMCTRAAAIAAHRg");
	this.shape_1.setTransform(-311.925,-39.35);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("EgupAFkIAArHMBdTAAAIAALHg");
	this.shape_2.setTransform(163.75,242.95);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("EgoIAPRIAA+hMBQRAAAIAAehg");
	this.shape_3.setTransform(223.65,109.525);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("Eg1UAPRIAA+hMBqpAAAIAAehg");
	this.shape_4.setTransform(139.325,109.525);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("EhJoAPRIAA+hMCTRAAAIAAehg");
	this.shape_5.setTransform(9.175,109.525);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("ADnLHIk6nGIjbAAIAAHGIk+AAIAA2NIIbAAQFLAACNBwQCNBwAAD4QAAFVkOBkIFoH8gAkugQIDnAAQCiAAA6g1QA8g1AAhzQAAhzg9grQg9gqiZAAIjsAAg");
	this.shape_6.setTransform(391.6,105.875);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AoLLHIAA2NIQAAAIAAEbIrCAAIAAElIJ7AAIAAENIp7AAIAAEnILZAAIAAEZg");
	this.shape_7.setTransform(248.75,105.875);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AnBIPQjUjOAAk9QAAk7DYjUQDZjTEyAAQEyAADhDIIilDvQhfhUhQgfQhRgfhdAAQi0AAh8B7Qh8B6AADJQAADKB3B5QB3B5CjAAQClAABuhAIAAmFIE/AAIAAH4QjUDrl1AAQk5AAjVjQg");
	this.shape_8.setTransform(101.825,105.075);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AFTLHIqkt6IAAN6Ik+AAIAA2NIEpAAIK5OTIAAuTIE9AAIAAWNg");
	this.shape_9.setTransform(-56.925,105.875);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AGuLHIiEkzIpTAAIiEEzIlSAAIJm2NIEzAAIJmWNgAixB9IFjAAIixmcg");
	this.shape_10.setTransform(-215.75,105.875);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AqELHIAA2NIH2AAQF8AADMC6QDLC6AAFMQAAFLjGDBQjHDBmWAAgAlGGuIDIAAQDaAAB0htQB1huAAjSQAAjRh1hwQh0hxjyAAIiwAAg");
	this.shape_11.setTransform(-365.975,105.875);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1}]}).to({state:[{t:this.shape_2}]},3).to({state:[{t:this.shape_3}]},3).to({state:[{t:this.shape_4}]},3).to({state:[{t:this.shape_5}]},3).to({state:[{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6}]},3).to({state:[{t:this.shape_5}]},40).to({state:[{t:this.shape_4}]},3).to({state:[{t:this.shape_3}]},3).to({state:[{t:this.shape_2}]},3).to({state:[{t:this.shape_1}]},4).to({state:[]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-783.2,-62.6,1263.8000000000002,359.20000000000005);


(lib.Tween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgEAFQAAgBgBAAQAAgBAAAAQgBgBAAgBQAAAAAAgBQAAAAAAAAQAAgBABgBQAAAAAAgBQABAAAAAAQACgCACAAQABAAAAAAQABAAABAAQAAAAABABQAAAAABABQAAAAABAAQAAABAAAAQABABAAABQAAAAAAAAQAAABAAAAQAAABgBABQAAAAAAABQgBAAAAABQgBAAAAAAQgBABAAAAQgBAAgBAAQAAABgBAAQgCgBgCgBg");
	this.shape.setTransform(68.175,2.05);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgEAFQAAgBgBAAQAAgBAAAAQgBgBAAgBQAAAAAAgBQAAAAAAAAQAAgBABgBQAAAAAAgBQABAAAAAAQACgCACAAQABAAAAAAQABAAABAAQAAAAABABQAAAAABABQAAAAABAAQAAABAAAAQAAABABABQAAAAAAAAQAAABAAAAQgBABAAABQAAAAAAABQgBAAAAABQgBAAAAAAQgBABAAAAQgBAAgBAAQAAABgBAAQgCgBgCgBg");
	this.shape_1.setTransform(66.025,2.05);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgEAFQAAgBgBAAQAAgBAAAAQAAgBgBgBQAAAAAAgBQAAAAAAAAQABgBAAgBQAAAAAAgBQABAAAAAAQACgCACAAQABAAAAAAQABAAABAAQAAAAABABQAAAAABABQAAAAABAAQAAABAAAAQABABAAABQAAAAAAAAQAAABAAAAQAAABgBABQAAAAAAABQgBAAAAABQgBAAAAAAQgBABAAAAQgBAAgBAAQAAABgBAAQgCgBgCgBg");
	this.shape_2.setTransform(63.825,2.05);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgTAaIAAgzIAlAAIAAAKIgZAAIAAALIAXAAIAAAJIgXAAIAAALIAaAAIAAAKg");
	this.shape_3.setTransform(60.45,0.025);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAJAaIgLgRIgIAAIAAARIgMAAIAAgzIAUAAQALAAAGAEQAEAEAAAJQABAMgKADIANATgAgKAAIAJAAQAEAAADgCQACgCAAgEQAAgEgCgCQgDgBgEAAIgJAAg");
	this.shape_4.setTransform(55.3,0.025);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgFAaIAAgpIgPAAIAAgKIApAAIAAAKIgPAAIAAApg");
	this.shape_5.setTransform(50.025,0.025);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgTAjIAAg0IAmAAIAAALIgZAAIAAAJIAWAAIAAALIgWAAIAAALIAaAAIAAAKgAAEgWIgFgGIgGAGIgJAAIAJgMIAMAAIAJAMg");
	this.shape_6.setTransform(45.45,-0.85);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgMAFIAAgJIAaAAIAAAJg");
	this.shape_7.setTransform(41,0.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgFAaIAAgpIgPAAIAAgKIApAAIAAAKIgPAAIAAApg");
	this.shape_8.setTransform(36.825,0.025);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgPAUQgHgGAAgLIAAgdIAMAAIAAAcQAAAHADADQADAEAEAAQAFAAADgEQADgDAAgHIAAgcIAMAAIAAAdQAAAMgHAFQgGAHgKAAQgJAAgGgHg");
	this.shape_9.setTransform(31.725,0.05);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgTAaIAAgzIAmAAIAAAKIgZAAIAAALIAWAAIAAAJIgWAAIAAALIAaAAIAAAKg");
	this.shape_10.setTransform(26.55,0.025);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgUAaIAAgzIASAAQAMAAAFAEQAGAFAAAKQAAAJgGAEQgGAFgLAAIgGAAIAAAOgAgIABIAHAAQAGAAABgBQADgCAAgFQAAgFgDgBQgDgCgFAAIgGAAg");
	this.shape_11.setTransform(21.6,0.025);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgUASIAHgJQAJAIAGAAIAFgBQABgBAAAAQAAgBABAAQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQAAAAgBgBIgHgDQgJgCgEgDQgEgDgBgHQAAgIAGgEQAFgEAIAAQAFAAAFACQAFABAEAEIgGAIQgGgFgIAAQAAAAAAAAQgBAAgBABQAAAAgBAAQAAAAgBABQAAAAAAAAQgBABAAAAQAAABAAAAQAAABAAAAQAAABAAAAQAAABAAAAQAAABABAAQAAABAAAAIAJADQAIACAEADQAEADAAAHQAAAIgGAEQgFAEgIAAQgMAAgKgJg");
	this.shape_12.setTransform(14.4,-0.025);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgFAaIAAgzIALAAIAAAzg");
	this.shape_13.setTransform(10.85,0.025);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgTATQgIgHAAgMQAAgLAIgHQAIgIALAAQAMAAAIAIQAIAHAAALQAAAMgIAHQgIAIgMAAQgLAAgIgIgAgKgLQgFAFAAAGQAAAHAFAFQAEAFAGAAQAHAAAEgFQAFgFAAgHQAAgGgFgFQgEgFgHAAQgGAAgEAFg");
	this.shape_14.setTransform(6.475,-0.025);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgRAaIAAgzIAjAAIAAAKIgXAAIAAALIAWAAIAAAKIgWAAIAAAUg");
	this.shape_15.setTransform(1.2,0.025);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgSAaIAAgzIAlAAIAAAKIgaAAIAAALIAXAAIAAAJIgXAAIAAALIAaAAIAAAKg");
	this.shape_16.setTransform(-5.55,0.025);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AANAaIgYggIAAAgIgMAAIAAgzIALAAIAZAgIAAggIALAAIAAAzg");
	this.shape_17.setTransform(-11.275,0.025);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgFAaIAAgzIALAAIAAAzg");
	this.shape_18.setTransform(-15.55,0.025);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AAQAaIgFgLIgVAAIgFALIgMAAIAWgzIALAAIAXAzgAgGAFIAMAAIgGgPg");
	this.shape_19.setTransform(-19.6,0.025);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AAKAaIAAgUIgTAAIAAAUIgMAAIAAgzIAMAAIAAAWIATAAIAAgWIAMAAIAAAzg");
	this.shape_20.setTransform(-25.275,0.025);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgQAUQgIgIAAgMQABgKAHgIQAJgIAKAAQANAAAJAKIgIAIQgFgHgIAAQgGAAgFAEQgFAFABAGQgBAHAFAFQAEAEAFAAQAJAAAGgHIAIAIQgKAKgMAAQgLAAgIgHg");
	this.shape_21.setTransform(-30.9,-0.025);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgTATQgIgHAAgMQAAgLAIgHQAIgIALAAQAMAAAIAIQAIAHAAALQAAAMgIAHQgIAIgMAAQgLAAgIgIgAgKgLQgFAFAAAGQAAAHAFAFQAEAFAGAAQAHAAAEgFQAFgFAAgHQAAgGgFgFQgEgFgHAAQgGAAgEAFg");
	this.shape_22.setTransform(-36.775,-0.025);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AAJAaIgLgRIgIAAIAAARIgMAAIAAgzIAUAAQAMAAAEAEQAGAEgBAJQABAMgKADIANATgAgKAAIAJAAQAEAAACgCQADgCAAgEQAAgEgDgCQgCgBgEAAIgJAAg");
	this.shape_23.setTransform(-42.45,0.025);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgUAaIAAgzIATAAQAKAAAHAEQAFAFAAAKQAAAJgGAEQgFAFgLAAIgHAAIAAAOgAgIABIAIAAQAFAAACgBQACgCAAgFQAAgFgDgBQgDgCgEAAIgHAAg");
	this.shape_24.setTransform(-47.75,0.025);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgTAaIAAgzIAmAAIAAAKIgZAAIAAALIAWAAIAAAJIgWAAIAAALIAaAAIAAAKg");
	this.shape_25.setTransform(-54.8,0.025);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AANAaIgYggIAAAgIgMAAIAAgzIALAAIAZAgIAAggIALAAIAAAzg");
	this.shape_26.setTransform(-60.525,0.025);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgPAUQgHgGAAgLIAAgdIAMAAIAAAcQAAAHADADQADAEAEAAQAFAAADgEQADgDAAgHIAAgcIAMAAIAAAdQAAAMgHAFQgGAHgKAAQgJAAgGgHg");
	this.shape_27.setTransform(-66.425,0.05);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-69.8,-5.1,139.6,10.2);


(lib.Tween1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgEAFQAAgBgBAAQAAgBAAAAQgBgBAAgBQAAAAAAgBQAAAAAAAAQAAgBABgBQAAAAAAgBQABAAAAAAQACgCACAAQABAAAAAAQABAAABAAQAAAAABABQAAAAABABQAAAAABAAQAAABAAAAQABABAAABQAAAAAAAAQAAABAAAAQAAABgBABQAAAAAAABQgBAAAAABQgBAAAAAAQgBABAAAAQgBAAgBAAQAAABgBAAQgCgBgCgBg");
	this.shape.setTransform(68.175,2.05);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgEAFQAAgBgBAAQAAgBAAAAQgBgBAAgBQAAAAAAgBQAAAAAAAAQAAgBABgBQAAAAAAgBQABAAAAAAQACgCACAAQABAAAAAAQABAAABAAQAAAAABABQAAAAABABQAAAAABAAQAAABAAAAQAAABABABQAAAAAAAAQAAABAAAAQgBABAAABQAAAAAAABQgBAAAAABQgBAAAAAAQgBABAAAAQgBAAgBAAQAAABgBAAQgCgBgCgBg");
	this.shape_1.setTransform(66.025,2.05);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgEAFQAAgBgBAAQAAgBAAAAQAAgBgBgBQAAAAAAgBQAAAAAAAAQABgBAAgBQAAAAAAgBQABAAAAAAQACgCACAAQABAAAAAAQABAAABAAQAAAAABABQAAAAABABQAAAAABAAQAAABAAAAQABABAAABQAAAAAAAAQAAABAAAAQAAABgBABQAAAAAAABQgBAAAAABQgBAAAAAAQgBABAAAAQgBAAgBAAQAAABgBAAQgCgBgCgBg");
	this.shape_2.setTransform(63.825,2.05);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgTAaIAAgzIAlAAIAAAKIgZAAIAAALIAXAAIAAAJIgXAAIAAALIAaAAIAAAKg");
	this.shape_3.setTransform(60.45,0.025);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAJAaIgLgRIgIAAIAAARIgMAAIAAgzIAUAAQALAAAGAEQAEAEAAAJQABAMgKADIANATgAgKAAIAJAAQAEAAADgCQACgCAAgEQAAgEgCgCQgDgBgEAAIgJAAg");
	this.shape_4.setTransform(55.3,0.025);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgFAaIAAgpIgPAAIAAgKIApAAIAAAKIgPAAIAAApg");
	this.shape_5.setTransform(50.025,0.025);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgTAjIAAg0IAmAAIAAALIgZAAIAAAJIAWAAIAAALIgWAAIAAALIAaAAIAAAKgAAEgWIgFgGIgGAGIgJAAIAJgMIAMAAIAJAMg");
	this.shape_6.setTransform(45.45,-0.85);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgMAFIAAgJIAaAAIAAAJg");
	this.shape_7.setTransform(41,0.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgFAaIAAgpIgPAAIAAgKIApAAIAAAKIgPAAIAAApg");
	this.shape_8.setTransform(36.825,0.025);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgPAUQgHgGAAgLIAAgdIAMAAIAAAcQAAAHADADQADAEAEAAQAFAAADgEQADgDAAgHIAAgcIAMAAIAAAdQAAAMgHAFQgGAHgKAAQgJAAgGgHg");
	this.shape_9.setTransform(31.725,0.05);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgTAaIAAgzIAmAAIAAAKIgZAAIAAALIAWAAIAAAJIgWAAIAAALIAaAAIAAAKg");
	this.shape_10.setTransform(26.55,0.025);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgUAaIAAgzIASAAQAMAAAFAEQAGAFAAAKQAAAJgGAEQgGAFgLAAIgGAAIAAAOgAgIABIAHAAQAGAAABgBQADgCAAgFQAAgFgDgBQgDgCgFAAIgGAAg");
	this.shape_11.setTransform(21.6,0.025);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgUASIAHgJQAJAIAGAAIAFgBQABgBAAAAQAAgBABAAQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQAAAAgBgBIgHgDQgJgCgEgDQgEgDgBgHQAAgIAGgEQAFgEAIAAQAFAAAFACQAFABAEAEIgGAIQgGgFgIAAQAAAAAAAAQgBAAgBABQAAAAgBAAQAAAAgBABQAAAAAAAAQgBABAAAAQAAABAAAAQAAABAAAAQAAABAAAAQAAABAAAAQAAABABAAQAAABAAAAIAJADQAIACAEADQAEADAAAHQAAAIgGAEQgFAEgIAAQgMAAgKgJg");
	this.shape_12.setTransform(14.4,-0.025);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgFAaIAAgzIALAAIAAAzg");
	this.shape_13.setTransform(10.85,0.025);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgTATQgIgHAAgMQAAgLAIgHQAIgIALAAQAMAAAIAIQAIAHAAALQAAAMgIAHQgIAIgMAAQgLAAgIgIgAgKgLQgFAFAAAGQAAAHAFAFQAEAFAGAAQAHAAAEgFQAFgFAAgHQAAgGgFgFQgEgFgHAAQgGAAgEAFg");
	this.shape_14.setTransform(6.475,-0.025);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgRAaIAAgzIAjAAIAAAKIgXAAIAAALIAWAAIAAAKIgWAAIAAAUg");
	this.shape_15.setTransform(1.2,0.025);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgSAaIAAgzIAlAAIAAAKIgaAAIAAALIAXAAIAAAJIgXAAIAAALIAaAAIAAAKg");
	this.shape_16.setTransform(-5.55,0.025);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AANAaIgYggIAAAgIgMAAIAAgzIALAAIAZAgIAAggIALAAIAAAzg");
	this.shape_17.setTransform(-11.275,0.025);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgFAaIAAgzIALAAIAAAzg");
	this.shape_18.setTransform(-15.55,0.025);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AAQAaIgFgLIgVAAIgFALIgMAAIAWgzIALAAIAXAzgAgGAFIAMAAIgGgPg");
	this.shape_19.setTransform(-19.6,0.025);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AAKAaIAAgUIgTAAIAAAUIgMAAIAAgzIAMAAIAAAWIATAAIAAgWIAMAAIAAAzg");
	this.shape_20.setTransform(-25.275,0.025);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgQAUQgIgIAAgMQABgKAHgIQAJgIAKAAQANAAAJAKIgIAIQgFgHgIAAQgGAAgFAEQgFAFABAGQgBAHAFAFQAEAEAFAAQAJAAAGgHIAIAIQgKAKgMAAQgLAAgIgHg");
	this.shape_21.setTransform(-30.9,-0.025);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgTATQgIgHAAgMQAAgLAIgHQAIgIALAAQAMAAAIAIQAIAHAAALQAAAMgIAHQgIAIgMAAQgLAAgIgIgAgKgLQgFAFAAAGQAAAHAFAFQAEAFAGAAQAHAAAEgFQAFgFAAgHQAAgGgFgFQgEgFgHAAQgGAAgEAFg");
	this.shape_22.setTransform(-36.775,-0.025);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AAJAaIgLgRIgIAAIAAARIgMAAIAAgzIAUAAQAMAAAEAEQAGAEgBAJQABAMgKADIANATgAgKAAIAJAAQAEAAACgCQADgCAAgEQAAgEgDgCQgCgBgEAAIgJAAg");
	this.shape_23.setTransform(-42.45,0.025);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgUAaIAAgzIATAAQAKAAAHAEQAFAFAAAKQAAAJgGAEQgFAFgLAAIgHAAIAAAOgAgIABIAIAAQAFAAACgBQACgCAAgFQAAgFgDgBQgDgCgEAAIgHAAg");
	this.shape_24.setTransform(-47.75,0.025);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgTAaIAAgzIAmAAIAAAKIgZAAIAAALIAWAAIAAAJIgWAAIAAALIAaAAIAAAKg");
	this.shape_25.setTransform(-54.8,0.025);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AANAaIgYggIAAAgIgMAAIAAgzIALAAIAZAgIAAggIALAAIAAAzg");
	this.shape_26.setTransform(-60.525,0.025);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgPAUQgHgGAAgLIAAgdIAMAAIAAAcQAAAHADADQADAEAEAAQAFAAADgEQADgDAAgHIAAgcIAMAAIAAAdQAAAMgHAFQgGAHgKAAQgJAAgGgHg");
	this.shape_27.setTransform(-66.425,0.05);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-69.8,-5.1,139.6,10.2);


(lib.masque = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#333333").s().p("EgpfApfMAAAhS+MBS/AAAMAAABS+g");
	this.shape.setTransform(0,265.55);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.masque, new cjs.Rectangle(-265.5,0,531.1,531.1), null);


(lib.ligne = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_48 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(48).call(this.frame_48).wait(1));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#333333").ss(1,1,1).p("EAAAg/bMAAAB+3");
	this.shape.setTransform(0,406);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#333333").ss(1,1,1).p("EAA/A/cQhDv0ggwoQgdwxAExFQAEwXAhwHQAfufA4to");
	this.shape_1.setTransform(-6.0239,406);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#333333").ss(1,1,1).p("EABwA/cQh4vfg5w8Qg0wqAHxQQAHweA7wDQA5uxBjtQ");
	this.shape_2.setTransform(-10.6719,406);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#333333").ss(1,1,1).p("EACVA/cQigvPhLxLQhHwmAKxYQAJwjBPv/QBMvACEs9");
	this.shape_3.setTransform(-14.2371,406);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#333333").ss(1,1,1).p("EACwA/cQi+vDhZxWQhTwjAMxeQAKwnBev8QBavKCcsw");
	this.shape_4.setTransform(-16.895,406);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#333333").ss(1,1,1).p("EADEA/cQjTu7hjxdQhdwhANxhQAMwrBov6QBkvSCusm");
	this.shape_5.setTransform(-18.7801,406);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#333333").ss(1,1,1).p("EADSA/cQjiu1hqxjQhjwfAOxlQAMwsBvv5QBsvXC6sf");
	this.shape_6.setTransform(-20.0814,406);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#333333").ss(1,1,1).p("EADbA/cQjsuxhuxnQhnweAOxnQANwtB0v4QBwvaDCsb");
	this.shape_7.setTransform(-20.9145,406);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#333333").ss(1,1,1).p("EADgA/cQjyuuhxxpQhpwdAPxpQANwuB3v3QByvdDHsY");
	this.shape_8.setTransform(-21.4184,406);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#333333").ss(1,1,1).p("EADjA/cQj1uthyxqQhrwdAPxpQANwvB5v3QB0vdDJsX");
	this.shape_9.setTransform(-21.7172,406);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#333333").ss(1,1,1).p("EADkA/cQj2uthzxqQhswdAPxpQANwvB6v3QB1veDKsW");
	this.shape_10.setTransform(-21.8418,406);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#333333").ss(1,1,1).p("EADlA/cQj3ush0xrQhrwdAPxpQANwvB5v3QB2veDLsW");
	this.shape_11.setTransform(-21.8918,406);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#333333").ss(1,1,1).p("EADlA/cQj3ushzxrQhswdAPxpQANwvB6v3QB1veDLsW");
	this.shape_12.setTransform(-21.9165,406);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#333333").ss(1,1,1).p("EADlg/bQjLMWh1PeQh6P3gNQvQgPRpBsQdQBzRrD3Os");
	this.shape_13.setTransform(-21.9165,406);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#333333").ss(1,1,1).p("EADlA/cQj3ushzxrQhswdAPxpQANwvB5v3QB2veDLsW");
	this.shape_14.setTransform(-21.8915,406);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#333333").ss(1,1,1).p("EADlA/cQj3ushzxrQhswdAPxpQAOwvB5v3QB1veDLsW");
	this.shape_15.setTransform(-21.8665,406);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#333333").ss(1,1,1).p("EADkA/cQj2uthzxqQhrwdAPxpQANwvB5v3QB1veDKsW");
	this.shape_16.setTransform(-21.7865,406);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#333333").ss(1,1,1).p("EADjA/cQj0uthzxqQhqwdAOxpQAOwvB4v2QB0veDJsX");
	this.shape_17.setTransform(-21.6619,406);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#333333").ss(1,1,1).p("EADhA/cQjyuuhyxpQhpwdAOxpQANwuB4v3QBzvdDHsY");
	this.shape_18.setTransform(-21.4626,406);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#333333").ss(1,1,1).p("EADeA/cQjvuvhwxoQhoweAOxoQANwuB2v3QBxvcDFsZ");
	this.shape_19.setTransform(-21.2133,406);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#333333").ss(1,1,1).p("EADaA/cQjruxhuxnQhnweAPxmQAMwuB0v4QBwvaDBsb");
	this.shape_20.setTransform(-20.8645,406);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#333333").ss(1,1,1).p("EADWA/cQjnuzhrxlQhlweAOxmQANwsBxv5QBtvZC+sd");
	this.shape_21.setTransform(-20.4106,406);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#333333").ss(1,1,1).p("EADQA/cQjgu2hpxiQhiwfAOxlQAMwsBuv4QBrvXC4sg");
	this.shape_22.setTransform(-19.8817,406);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#333333").ss(1,1,1).p("EADJA/cQjZu4hlxgQhewgANxjQAMwrBqv6QBnvTCysk");
	this.shape_23.setTransform(-19.1786,406);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#333333").ss(1,1,1).p("EADAA/cQjPu8hhxcQhawhAMxhQAMwqBlv7QBjvQCqso");
	this.shape_24.setTransform(-18.3758,406);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#333333").ss(1,1,1).p("EAC2A/cQjEvAhcxZQhWwiAMxfQALwoBhv8QBdvMChst");
	this.shape_25.setTransform(-17.4235,406);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#333333").ss(1,1,1).p("EACqA/cQi3vFhWxUQhRwjAMxdQAKwnBav8QBXvICXsz");
	this.shape_26.setTransform(-16.2969,406);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#333333").ss(1,1,1).p("EACdA/cQipvLhPxPQhKwkAKxaQAKwlBTv+QBQvDCLs5");
	this.shape_27.setTransform(-15.0095,406);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#333333").ss(1,1,1).p("EACOA/cQiYvShIxIQhCwnAJxWQAIwiBLwAQBJu9B9tB");
	this.shape_28.setTransform(-13.5536,406);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#333333").ss(1,1,1).p("EAB8A/cQiFvZg/xCQg6woAIxSQAHwhBCwBQA/u2ButK");
	this.shape_29.setTransform(-11.8731,406);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#333333").ss(1,1,1).p("EABpA/cQhxvig1w5QgxwrAHxPQAGwdA3wDQA2uvBdtT");
	this.shape_30.setTransform(-10.0434,406);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#333333").ss(1,1,1).p("EABYA/cQhevpgswyQgqwuAGxKQAFwbAvwFQAsuoBOtc");
	this.shape_31.setTransform(-8.3629,406);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#333333").ss(1,1,1).p("EABIA/cQhNvwgkwsQgiwvAFxHQAEwYAmwHQAkujBAtj");
	this.shape_32.setTransform(-6.9071,406);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#333333").ss(1,1,1).p("EAA7A/cQg/v2gdwmQgcwxAExEQADwXAgwIQAdudA0tq");
	this.shape_33.setTransform(-5.6198,406);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#333333").ss(1,1,1).p("EAAvA/cQgyv7gYwhQgWwzADxBQADwVAZwJQAXuZAqtw");
	this.shape_34.setTransform(-4.493,406);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#333333").ss(1,1,1).p("EAAlA/cQgnv/gTweQgRwzACxAQACwTAUwKQASuVAht1");
	this.shape_35.setTransform(-3.5406,406);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#333333").ss(1,1,1).p("EAAcA/cQgdwDgPwaQgNw1ACw9QABwSAQwLQANuSAZt5");
	this.shape_36.setTransform(-2.7379,406);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#333333").ss(1,1,1).p("EAAVA/cQgWwGgLwXQgKw2ACw7QABwSALwMQAKuPATt8");
	this.shape_37.setTransform(-2.0348,406);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#333333").ss(1,1,1).p("EAAQA/cQgQwIgIwVQgHw3ABw6QABwRAIwMQAHuNAOt/");
	this.shape_38.setTransform(-1.5059,406);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#333333").ss(1,1,1).p("EAALA/cQgLwLgFwSQgFw3AAw6QABwQAGwMQAFuMAJuB");
	this.shape_39.setTransform(-1.0523,406);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#333333").ss(1,1,1).p("EAAIA/cQgIwMgDwSQgEw3ABw5QAAwPAEwNQADuKAHuD");
	this.shape_40.setTransform(-0.7031,406);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#333333").ss(1,1,1).p("EAAFA/cQgFwNgBwRQgDw3ABw5QAAwOACwOQACuJAEuE");
	this.shape_41.setTransform(-0.4542,406);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#333333").ss(1,1,1).p("EAADA/cQgDwOAAwQQgCw3ABw4QAAwOABwPQAAuIADuF");
	this.shape_42.setTransform(-0.2562,406);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#333333").ss(1,1,1).p("EAABA/cQgBwOAAwQQgBw4ABw3QAAwOAAwOQAAuIABuG");
	this.shape_43.setTransform(-0.1333,406);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#333333").ss(1,1,1).p("EAAAA/cQAAwPAAwPQAAw4AAw3QAAwOAAwOQAAuIAAuG");
	this.shape_44.setTransform(-0.05,406);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#333333").ss(1,1,1).p("EAAAA/cQAAwPAAwPQAAw4AAw3QAAwOAAwOQAAuHAAuH");
	this.shape_45.setTransform(-0.025,406);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45,p:{x:-0.025}}]},1).to({state:[{t:this.shape_45,p:{x:0}}]},1).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-45.8,-1,47.8,814);


(lib.img_danger = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.danger();
	this.instance.parent = this;
	this.instance.setTransform(-369,-41,0.674,0.674);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.img_danger, new cjs.Rectangle(-369,-41,727.9,727.9), null);


(lib.img_action = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.action();
	this.instance.parent = this;
	this.instance.setTransform(-334,118,1.0658,1.0658);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.img_action, new cjs.Rectangle(-334,118,682.1,357.1), null);


(lib.bg_btn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CC9933").s().p("AsqC2IAAlrIZVAAIAAFrg");
	this.shape.setTransform(0.025,18.2);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.bg_btn, new cjs.Rectangle(-81.1,0,162.3,36.4), null);


(lib._200_gif = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.fire_0000_Layer8();
	this.instance.parent = this;
	this.instance.setTransform(199,21);

	this.instance_1 = new lib.fire_0001_Layer9();
	this.instance_1.parent = this;
	this.instance_1.setTransform(199,21);

	this.instance_2 = new lib.fire_0002_Layer10();
	this.instance_2.parent = this;
	this.instance_2.setTransform(199,21);

	this.instance_3 = new lib.fire_0003_Layer11();
	this.instance_3.parent = this;
	this.instance_3.setTransform(199,21);

	this.instance_4 = new lib.fire_0004_Layer12();
	this.instance_4.parent = this;
	this.instance_4.setTransform(199,21);

	this.instance_5 = new lib.fire_0005_Layer13();
	this.instance_5.parent = this;
	this.instance_5.setTransform(199,21);

	this.instance_6 = new lib.fire_0006_Layer14();
	this.instance_6.parent = this;
	this.instance_6.setTransform(199,21);

	this.instance_7 = new lib.fire_0007_Layer15();
	this.instance_7.parent = this;
	this.instance_7.setTransform(199,21);

	this.instance_8 = new lib.fire_0008_Layer16();
	this.instance_8.parent = this;
	this.instance_8.setTransform(199,21);

	this.instance_9 = new lib.fire_0009_Layer17();
	this.instance_9.parent = this;
	this.instance_9.setTransform(199,21);

	this.instance_10 = new lib.fire_0010_Layer1();
	this.instance_10.parent = this;
	this.instance_10.setTransform(199,21);

	this.instance_11 = new lib.fire_0011_Layer2();
	this.instance_11.parent = this;
	this.instance_11.setTransform(199,21);

	this.instance_12 = new lib.fire_0012_Layer3();
	this.instance_12.parent = this;
	this.instance_12.setTransform(199,21);

	this.instance_13 = new lib.fire_0013_Layer4();
	this.instance_13.parent = this;
	this.instance_13.setTransform(199,21);

	this.instance_14 = new lib.fire_0014_Layer5();
	this.instance_14.parent = this;
	this.instance_14.setTransform(199,21);

	this.instance_15 = new lib.fire_0015_Layer6();
	this.instance_15.parent = this;
	this.instance_15.setTransform(199,21);

	this.instance_16 = new lib.fire_0016_Layer7();
	this.instance_16.parent = this;
	this.instance_16.setTransform(199,21);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},4).to({state:[{t:this.instance_2}]},3).to({state:[{t:this.instance_3}]},3).to({state:[{t:this.instance_4}]},3).to({state:[{t:this.instance_5}]},3).to({state:[{t:this.instance_6}]},3).to({state:[{t:this.instance_7}]},3).to({state:[{t:this.instance_8}]},3).to({state:[{t:this.instance_9}]},3).to({state:[{t:this.instance_10}]},3).to({state:[{t:this.instance_11}]},3).to({state:[{t:this.instance_12}]},3).to({state:[{t:this.instance_13}]},3).to({state:[{t:this.instance_14}]},3).to({state:[{t:this.instance_15}]},3).to({state:[{t:this.instance_16}]},3).to({state:[]},3).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,249,100);


(lib.vignette3_danger = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_92 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(92).call(this.frame_92).wait(1));

	// Layer_2
	this.instance = new lib.txt_danger();
	this.instance.parent = this;
	this.instance.setTransform(559.75,1084.95,0.1,0.1,0,0,0,0,255.1);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(2).to({_off:false},0).to({_off:true},90).wait(1));

	// Layer_5 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	var mask_1_graphics_6 = new cjs.Graphics().p("EA6hAqXIhTh4IG9AAIBVB4gEArUAqXIAAh4IFrAAIAAB4gEATkAqXIAAh4IStAAIAAB4gEgITAofIgng3InpAAIAAA3IlqAAIAAg3IgFAAIAAgeISuAAIAAAeIBmAAIAnA3gEgt+AofIAAg3ISuAAIAAA3gEBLKAnoIgUgeIG8AAIAVAegEABeAnoIAAgeIFrAAIAAAegEgeRAnKIg6hUIFGAAIAAhkIAuAAICCC4gEgrQAnKIAAhUIgeAAIhGhkIGMAAIAABkIBDAAIAABUgEhDBAnKIAAhUIsjAAIAAhkIFrAAIAABEINCAAIAAAgIFEAAIAAhkIFqAAIAABkIB2AAIAABUgEAiUAkSIAAhAIAtBAgEAPlAkSIgyhHIG4AAIAFAHIAABAgEAEkAkSIAAhHIFsAAIAABHgEgTLAkSIAAhHIFrAAIAABHgEhIKAi9IAAhwIFrAAIAABwgEhY+Ah4IAAgrILPAAIAAArgEAuqAghIAAg5IvwAAIAAg5INxAAIAAgqIsvAAIAAhjIlkAAIAABjIs8AAIAAhjIgvAAIAAhWIgSAAQAJgNAGgPIiiAAIAAAcIkeAAIAAh/IFrAAIAABYILYAAIAAALIkFAAIgFAcIGwAAIAABKIGtAAIAAhKIFsAAIAABWIBzAAIAAhWIEcAAIAABWQBPgPAngjQASgQANgUIGHAAQgJAugPAoIjAAAIAABjIGuAAIAAhjIFrAAIAAAKIEJAAQBNAAA6gKIIEAAQgVA2gfAtIFVAAIAAAqI1lAAIAAgqIhCAAIAAAqIoEAAQhCBJhmApgAHJfoIAAg5IRDAAIAAAqIrYAAIAAAPgArWfoIAAg5IM6AAIAAA5gEBW8AevIAAgqIFfAAIAAAqgAhzbMIAAgcIFqAAIAAAcgA1GbMIAAgcIMlAAIAAAcgEAr4AawQATgqADg5IFyAAQgCA0gHAvgAdbawIAAhjIFrAAIAABjgEghXAZNIAAgrIFrAAIAAArgEBJlATrIAAg5IkmAAIAAhjISTAAIAABjIE5AAQAaAaAUAfgEAx0ATrIAAg5ISTAAIAAA5gEBcvASyIAAhjIQKAAQBJAbAyApQASAOARARgAK0RPIAAg8IJoAAQEEgBCeA9gAs7RPIAAg8ISSAAIAAA8gEg9JgQHIAAjLITNAAIgQANQjjC+muAAgEBFZgT9QgcgcgYgdIU0AAIAAAiIgVAXgEA0EgT9Igsg5IJRAAIAAA5gEAlOgT9IAAg5IFrAAIAAA5gAamz9IgZg5IGCAAIAZA5gAIhz9IAZg5IGCAAIgZA5gAxvz9IAAg5IksAAQgXgcgTgdIi0AAIhhh/IjfAAQgVgwgOg0IGJAAQAZA3AoAtIIVAAIAAB/IBEAAQBQAiBbAAQB/AABjgiQgqg8gdhDIkNAAIAAhkIFtAAIAABkIFGAAIAVAXQBFBGBTAiIhHAAIAAA5IEeAAQgWAegZAbgEgnogU2Igrg5IA7AAIAAh/ImPAAIA4B/IjYAAIAAA5IlrAAIAAg5IlXAAIAZA5ImDAAIgUgwIqpAAIgUAwImCAAIAZg5IAZAAIAhgNQAbgNAZgQQAegUAZgXQAUgUASgWIpBAAIAxhyIEHAAIAAipIkYAAIAAjVIkAAAIgehHIDJAAQghhIg3g1QhahYicgcIJmAAQBJBqAhCHIjLAAIAeBHIDqAAIAADVIB1AAIAAC3IMNAAIAqBkIldAAQgRAsgWApIgPAdIgIANIF0AAIA3h/IF/AAIAAhkIFrAAIAABkIGTAAIhMhkIG4AAIAHAJIAAgJIFqAAIAABkIDWAAIAAB/IDXAAIAAA5gEhsxgU2IAAg5IFsAAIAAAoIDlAAQCLAABngoIBXAAIAAh/IlRAAQAngyAYhAIF5AAQgNA8gTA2IEkAAIAAB/IBKAAQgSAdgVAcgAFs1vQAwgPApgYIAAhYIFtAAIAAB/gEhzdgXuIAAhyIFrAAIAABygEBtxgZgIAAipIkYAAIAAhLIFtAAIAABLIEYAAIAACpgEhjNgfeIAfhHIGCAAIgfBHgEhxdgfeQgKglgPgiIGCAAQAIAjAGAkgEgncgf6IAAgrIhCAAQgIAVgHAWImAAAIAKgrIjeAAIAAi4IJ7AAICMC4IEJAAIAAArgEg5ugf6IAAgrIFrAAIAAArgEhFvgf6IgFgHIAAAHIlrAAIAAgrICPAAIBQi4IKtAAIBQC4IjVAAIAgArgEhDOgglIBKAAIglhYgEgVFgglQAYhiAuhWIHMAAQgVAQgTASQhBBBgkBVgEggQgglIAAi4IFrAAIAAC4gEho/gglIAAjxIFrAAIAADxgEgC6gjdIB2AAIgrA8QgmgiglgagEBWpgkWIgggOQhcgkhrABQh6gBhkAyIoBAAQAshDA7g9IRzAAIAABaIgaAmgEA6xgkWIAAiAIFqAAIAACAgEAo/gkWIAAiAIgnAAIAPgPQAegeAhgaImAAAIAAi3IhBAAQAfgCAgAAQE7gBDwC6IN9AAIAABHIpfAAIBhCAgEBBfgmWIA+AAIg+Bag");
	var mask_1_graphics_11 = new cjs.Graphics().p("EADyAqXIh6ivInoAAIAACvIlqAAIAAivIgFAAIAAhyIsjAAIAAi5IEmAAIAAhwIDPAAIAAieILTAAIAAgqIsuAAIAAhjIllAAIAABjIs7AAIAAhjIgwAAIAAhyIMlAAIAABmIGuAAIAAhmIFrAAIAAByIBzAAIAAjVIgeAAIABgdIAAgbIFxAAIAAAaIAAAeIAXAAIAABYILXAAIAAALIkFAAQgJA+gUA0Ii/AAIAABjIGtAAIAAhjIFrAAIAAAKIEIAAQBPAAA4gKIIFAAQgVA2gfAtIFWAAIAAAqIGuAAIAAgqITLAAQgQAWgTAUIrSAAQhDBJhmApIwBAAIAAhyImuAAIAAAqIrXAAIAABIIlrAAIAAhyIljAAIAAByIrPAAIAAAsILEAAIgPAGIBLBqIm3AAIgfgtIj7AAIAAAtIkmAAIAACZINCAAIAAAgIFEAAIAAi5IFpAAIAAC5IB2AAIAAByIBmAAIB9CvgAIAevIFlAAIAAgqIllAAgAlpciQBOgPAngjQAcgZARgnIiiAAgEgjLAqXIAAivISuAAIAACvgEAZlAnoIhPhyIFGAAIAAikIDEEWgEAMRAnoIAAhyIgeAAIiAi5IG4AAIAOAVIAACkIBDAAIAABygEgrNAi9IAAhwIFrAAIAABwgEg8BAh4IAAgrILPAAIAAArgAcGawQATgqADg5IFyAAQgCA0gHAvgANpawIAAhjIFrAAIAABjgA5ZZNIAAg4IDhAAQgDgygOgkIF8AAQAGApABAtIjnAAIAAA4gEgxJAZNIAAg4IFrAAIAAA4gEgksAYVIAAhWIjKAAQgSgsgigXQgNgKgSgHIG+AAQALAoAGAsIC5AAIAABWgEg8cAYVIAAhWIFrAAIAABWgEg2ZAW/IAAhUIFsAAIAABUgEhOJAW/IAAhUIFrAAIAABUgAq4VrQhJggiMgBIkQAAIAAAhIlrAAIAAi5IklAAIAAifISTAAIAACfIE5AAQBIBJAeBwgEgv5AVrIAAi5ISUAAIAACkIsoAAIAAAVgAk9SyIAAifIJnAAQF7AAChCAQATAOAQARgAL9yeQgXgRgXgTIgEgDIgQgNIALAAIg/iUIqoAAIg/CUIC+AAIAADLIlqAAIiajLIg8AAIBEidIAZAAIAhgNQAbgNAYgQQAegUAagXQAUgUATgWIpCAAIB6kbIkXAAIBbjVIh6AAIAAhHIFrAAIAABHICRAAIhcDVIEYAAIgsBlIGVAAIgrhlIkXAAIhbjVIA4AAQAFgkAJgjIjdAAIAAm4IDXAAIAAiDIFqAAIAACDIiKAAIFOG4IijAAQgOAigJAlIg0AAIBbDVIEYAAIB7EbIleAAQgRAsgVApIgQAdIgIANIF0AAIA3h/IF/AAIAAkbIkYAAIAAjVIFqAAIAADVIC8AAIihjVIG3AAICiDVICSAAIAAjVIFrAAIAADVIEYAAIAAEbIDVAAIAAB/IDXAAIAACdIoFAAIh3idIA7AAIAAh/ImOAAIA3B/IjYAAIAACdICFAAIgQANQgZAVgaASQiEBdirAkQhtAWh8AAQkfAAjViXgARIzSIEUAAIAAidIlXAAgAUa3uIGUAAIjYkbIi8AAgEAgggZJIAAjAIiSAAgA18wHIAAjLIkVAAIBZDLImDAAIhXjLIhPAAIAAidIFrAAIAAAoIDlAAQCMAABmgoIBXAAIAAh/IlRAAQBWhtAJiuIkYAAIABgpQAAhegVhOIlbAAIAAhHICOAAIC+m4IF5AAIAAiDIFTAAIBkCDIleAAIC9G4IjVAAIA2BHIhrAAQANBNAABWIgBAyIEYAAQgHCcgvB/IElAAIAAB/IBKAAQguBMhBA/IgTASIh9AAIAADLgA7W/eIAaAAIgagjgEgYxgglIBLAAIglhYgEg0TgQHIBYjLIGCAAIhYDLgEhM7gQHIAAjLITNAAIgQANQjjC+muAAgEA2UgTSIgkghQg7g6gshCIi0AAIhhh/IjfAAQg4iCgKiZIkYAAQgCggAAghQAAhMAMhIIF8AAQgRBFAABRQAAAgADAfIEYAAQAPCpBhByIIWAAIAAB/IBEAAQBPAiBcAAQB/AABjgiQgpg8gehDIkNAAIAAkbIkYAAIAAhLIFtAAIAABLIEYAAIAAEbIFGAAIAVAXQBFBGBTAiIhHAAIAABbQgfAjgjAfgEBQ8gVvQAvgPApgYIAAhYIFtAAIAAB/gEgoOgXuIAAkbIkYAAIAAjVIkAAAIgehHIDJAAQgghIg3g1QiFiCkWABIjJAAIAAD+IlsAAIAAm4IuZAAIAAiDII+AAQFWAADYCDIOZAAQA8AlAyAtQCbCOA0DYIjLAAIAfBHIDqAAIAADVIEYAAIAAEbgEg4vgfeIAfhHIGBAAIgfBHgEhHAgfeQgKgmgPghIGCAAQAIAjAGAkgEhYAgfeIAAhHIFsAAIAABHgEAVXgglQA2jbCoilQAegeAhgaIobAAQAaAVAZAXIhRB1IAAihIhDAAIAAG4IlrAAIAAm4IBNAAQDki6E0ABQE7gBDwC6IN9AAIAAChIhsCbQhthfhcgkQhcgkhrABQjOAAiOCMQhCBBgjBVgEgqOgndIA4iDIFgAAIA5CDg");
	var mask_1_graphics_15 = new cjs.Graphics().p("EA9fAqCIh6iwInpAAIAACwIlrAAIAAiwIgEAAIAAkqIEmAAIAAjjImuAAIAAAqIrYAAIAAC5IlrAAIAAjjIlkAAIAACdIs8AAIAAidILTAAIAAiOIlMAAIAAjUIEYAAIAAg5IFrAAIAAA5IkYAAIAABYILXAAIAAB8IBGAAIAACOIFkAAIAAiOIAEAAIAAjUIEYAAIAAg5IDhAAQgHhtg+gsQhGgxiuAAIkQAAIAADKIlrAAIAAliIklAAIAAigISTAAIAACgIE5AAQB4B6AGDoIjnAAIAAA5IkYAAIAADUIGQAAQBPgOAngjQA8g3AHhsIEYAAIABgdIgBgcIFyAAIAAAbIgBAeIkYAAQgEB4giBcIjAAAIAACOIGuAAIAAiOIFrAAIAAALIEIAAQBOAAA5gLIIEAAQgeBSg3A8IrTAAQhIBOhwAqIBMBrIm4AAIgfgtIj7AAIAAAtIkmAAIAACZINDAAIAACRIBmAAIB8CwgEAWhAqCIAAiwIStAAIAACwgEBTRAnSIjOkqIG4AAIDSEqgEBF+AnSIAAkqIFrAAIAAEqgAj2c3IAAgLIM6AAIAAALgAHuYqIAAliISTAAIAACkIsoAAIAAC+gEAypATIIAAigIJoAAQF8AAChCBQASAOAQARgADEyIQg2gmgwgwQg8g5grhCIi0AAIk4mbIi8AAIAAGbIDXAAIAAFoIlrAAIkRloIA6AAIAAmbIkYAAIAArTIDXAAIAAiDIFrAAIAACDIiLAAIInLTICTAAIAArTIBNAAQDji6E0AAQE6AADwC6IOaAAQAbAVAZAXIi8EQQhthfhcglQhcgjhrAAQjOAAiOCNQiOCMAADmQAAAhADAeIEYAAQAQC7B1B3QBGBHBTAiIhIAAIAABbQg8BDhIAzQjYCXlBAAQkeAAjViXgACm70IEYAAIAAGbIBEAAQBQAhBbAAQB/AABjghQh6ixgPjqIkYAAQgCgfAAghQAAlpD4jyQAegeAggaIp8AAgABU40IAAjAIiSAAgAvJ70IC8AAIi8j3gA+2vxIAAloIlXAAICbFoImCAAIiXlfIqpAAIiXFfImCAAICcloIAZAAQBSgfA6g3QB7hzAKjSIkYAAIABgoQAAjwiGiAQiFiCkVAAIjKAAIAAIaIEYAAIAAGbIBKAAQguBMhBA/QjjDdnQAAIosAAIAAloIFrAAIAAAnIDlAAQCMAABmgnIBXAAIAAmbIkYAAIAArTIuZAAIAAiDII+AAQFVAADZCDIOZAAQA8AkAyAuQDpDUAAF8IgBAxIEYAAQgKDvhpCsIF0AAICymbIkYAAIE4rTIF4AAIAAiDIFTAAIBkCDIleAAIE4LTIEYAAICyGbIjYAAIAAFogEgn9gb0IEYAAIgsBmIGXAAIgshmIkYAAIiflzgAcp1ZQAvgQApgXIAAl0IkYAAIAAhKIFtAAIAABKIEYAAIAAGbgEg3ggnHIA5iDIFgAAIA5CDg");
	var mask_1_graphics_21 = new cjs.Graphics().p("EBIXAqCIlooHIj7AAIAAIHIlrAAIAA5aIJoAAQF8AAChCBQChCAAAEcQAAGGk1ByIGbJFgEA+0AdCIEJAAQC6AABDg8QBEg+AAiCQAAiEhGgxQhGgxiuAAIkQAAgEAhZAqCIAA5aISTAAIAAFEIsoAAIAAFPILXAAIAAE0IrXAAIAAFSINCAAIAAFBgEAO5AhiIAAk2IM7AAIAAE2gAJkyIQg2gmgwgwQjzjsAAlqQAAlpD3jyQD4jyFfAAQFdAAECDmIi9EQQhshfhcglQhcgjhrAAQjPAAiOCNQiNCMAADmQAADnCICKQCICKC7AAQC9AAB+hIIAAm+IFsAAIAAJAQg8BDhIAzQjYCXlBAAQkeAAjViXgAmlvxIsGv6IAAP6IlrAAIAA5ZIFTAAIMeQWIAAwWIFrAAIAAZZgEghUgPxIiXlfIqpAAIiXFfImCAAIK+5ZIFgAAIK9ZZgEgsKgaOIGWAAIjKnZgEhPUgPxIAA5ZII+AAQGzAADoDVQDpDUAAF8QAAF7jjDcQjjDdnQAAgEhJpgUyIDlAAQD6AACFh9QCGh+AAjvQAAjwiGiAQiFiCkVAAIjKAAg");
	var mask_1_graphics_70 = new cjs.Graphics().p("EA9fAqCIh6iwInpAAIAACwIlrAAIAAiwIgEAAIAAkqIEmAAIAAjjImuAAIAAAqIrYAAIAAC5IlrAAIAAjjIlkAAIAACdIs8AAIAAidILTAAIAAiOIlMAAIAAjUIEYAAIAAg5IFrAAIAAA5IkYAAIAABYILXAAIAAB8IBGAAIAACOIFkAAIAAiOIAEAAIAAjUIEYAAIAAg5IDhAAQgHhtg+gsQhGgxiuAAIkQAAIAADKIlrAAIAAliIklAAIAAigISTAAIAACgIE5AAQB4B6AGDoIjnAAIAAA5IkYAAIAADUIGQAAQBPgOAngjQA8g3AHhsIEYAAIABgdIgBgcIFyAAIAAAbIgBAeIkYAAQgEB4giBcIjAAAIAACOIGuAAIAAiOIFrAAIAAALIEIAAQBOAAA5gLIIEAAQgeBSg3A8IrTAAQhIBOhwAqIBMBrIm4AAIgfgtIj7AAIAAAtIkmAAIAACZINDAAIAACRIBmAAIB8CwgEAWhAqCIAAiwIStAAIAACwgEBTRAnSIjOkqIG4AAIDSEqgEBF+AnSIAAkqIFrAAIAAEqgAj2c3IAAgLIM6AAIAAALgAHuYqIAAliISTAAIAACkIsoAAIAAC+gEAypATIIAAigIJoAAQF8AAChCBQASAOAQARgADEyIQg2gmgwgwQg8g5grhCIi0AAIk4mbIi8AAIAAGbIDXAAIAAFoIlrAAIkRloIA6AAIAAmbIkYAAIAArTIDXAAIAAiDIFrAAIAACDIiLAAIInLTICTAAIAArTIBNAAQDji6E0AAQE6AADwC6IOaAAQAbAVAZAXIi8EQQhthfhcglQhcgjhrAAQjOAAiOCNQiOCMAADmQAAAhADAeIEYAAQAQC7B1B3QBGBHBTAiIhIAAIAABbQg8BDhIAzQjYCXlBAAQkeAAjViXgACm70IEYAAIAAGbIBEAAQBQAhBbAAQB/AABjghQh6ixgPjqIkYAAQgCgfAAghQAAlpD4jyQAegeAggaIp8AAgABU40IAAjAIiSAAgAvJ70IC8AAIi8j3gA+2vxIAAloIlXAAICbFoImCAAIiXlfIqpAAIiXFfImCAAICcloIAZAAQBSgfA6g3QB7hzAKjSIkYAAIABgoQAAjwiGiAQiFiCkVAAIjKAAIAAIaIEYAAIAAGbIBKAAQguBMhBA/QjjDdnQAAIosAAIAAloIFrAAIAAAnIDlAAQCMAABmgnIBXAAIAAmbIkYAAIAArTIuZAAIAAiDII+AAQFVAADZCDIOZAAQA8AkAyAuQDpDUAAF8IgBAxIEYAAQgKDvhpCsIF0AAICymbIkYAAIE4rTIF4AAIAAiDIFTAAIBkCDIleAAIE4LTIEYAAICyGbIjYAAIAAFogEgn9gb0IEYAAIgsBmIGXAAIgshmIkYAAIiflzgAcp1ZQAvgQApgXIAAl0IkYAAIAAhKIFtAAIAABKIEYAAIAAGbgEg3ggnHIA5iDIFgAAIA5CDg");
	var mask_1_graphics_74 = new cjs.Graphics().p("EADyAqXIh6ivInoAAIAACvIlqAAIAAivIgFAAIAAhyIsjAAIAAi5IEmAAIAAhwIDPAAIAAieILTAAIAAgqIsuAAIAAhjIllAAIAABjIs7AAIAAhjIgwAAIAAhyIMlAAIAABmIGuAAIAAhmIFrAAIAAByIBzAAIAAjVIgeAAIABgdIAAgbIFxAAIAAAaIAAAeIAXAAIAABYILXAAIAAALIkFAAQgJA+gUA0Ii/AAIAABjIGtAAIAAhjIFrAAIAAAKIEIAAQBPAAA4gKIIFAAQgVA2gfAtIFWAAIAAAqIGuAAIAAgqITLAAQgQAWgTAUIrSAAQhDBJhmApIwBAAIAAhyImuAAIAAAqIrXAAIAABIIlrAAIAAhyIljAAIAAByIrPAAIAAAsILEAAIgPAGIBLBqIm3AAIgfgtIj7AAIAAAtIkmAAIAACZINCAAIAAAgIFEAAIAAi5IFpAAIAAC5IB2AAIAAByIBmAAIB9CvgAIAevIFlAAIAAgqIllAAgAlpciQBOgPAngjQAcgZARgnIiiAAgEgjLAqXIAAivISuAAIAACvgEAZlAnoIhPhyIFGAAIAAikIDEEWgEAMRAnoIAAhyIgeAAIiAi5IG4AAIAOAVIAACkIBDAAIAABygEgrNAi9IAAhwIFrAAIAABwgEg8BAh4IAAgrILPAAIAAArgAcGawQATgqADg5IFyAAQgCA0gHAvgANpawIAAhjIFrAAIAABjgA5ZZNIAAg4IDhAAQgDgygOgkIF8AAQAGApABAtIjnAAIAAA4gEgxJAZNIAAg4IFrAAIAAA4gEgksAYVIAAhWIjKAAQgSgsgigXQgNgKgSgHIG+AAQALAoAGAsIC5AAIAABWgEg8cAYVIAAhWIFrAAIAABWgEg2ZAW/IAAhUIFsAAIAABUgEhOJAW/IAAhUIFrAAIAABUgAq4VrQhJggiMgBIkQAAIAAAhIlrAAIAAi5IklAAIAAifISTAAIAACfIE5AAQBIBJAeBwgEgv5AVrIAAi5ISUAAIAACkIsoAAIAAAVgAk9SyIAAifIJnAAQF7AAChCAQATAOAQARgAL9yeQgXgRgXgTIgEgDIgQgNIALAAIg/iUIqoAAIg/CUIC+AAIAADLIlqAAIiajLIg8AAIBEidIAZAAIAhgNQAbgNAYgQQAegUAagXQAUgUATgWIpCAAIB6kbIkXAAIBbjVIh6AAIAAhHIFrAAIAABHICRAAIhcDVIEYAAIgsBlIGVAAIgrhlIkXAAIhbjVIA4AAQAFgkAJgjIjdAAIAAm4IDXAAIAAiDIFqAAIAACDIiKAAIFOG4IijAAQgOAigJAlIg0AAIBbDVIEYAAIB7EbIleAAQgRAsgVApIgQAdIgIANIF0AAIA3h/IF/AAIAAkbIkYAAIAAjVIFqAAIAADVIC8AAIihjVIG3AAICiDVICSAAIAAjVIFrAAIAADVIEYAAIAAEbIDVAAIAAB/IDXAAIAACdIoFAAIh3idIA7AAIAAh/ImOAAIA3B/IjYAAIAACdICFAAIgQANQgZAVgaASQiEBdirAkQhtAWh8AAQkfAAjViXgARIzSIEUAAIAAidIlXAAgAUa3uIGUAAIjYkbIi8AAgEAgggZJIAAjAIiSAAgA18wHIAAjLIkVAAIBZDLImDAAIhXjLIhPAAIAAidIFrAAIAAAoIDlAAQCMAABmgoIBXAAIAAh/IlRAAQBWhtAJiuIkYAAIABgpQAAhegVhOIlbAAIAAhHICOAAIC+m4IF5AAIAAiDIFTAAIBkCDIleAAIC9G4IjVAAIA2BHIhrAAQANBNAABWIgBAyIEYAAQgHCcgvB/IElAAIAAB/IBKAAQguBMhBA/IgTASIh9AAIAADLgA7W/eIAaAAIgagjgEgYxgglIBLAAIglhYgEg0TgQHIBYjLIGCAAIhYDLgEhM7gQHIAAjLITNAAIgQANQjjC+muAAgEA2UgTSIgkghQg7g6gshCIi0AAIhhh/IjfAAQg4iCgKiZIkYAAQgCggAAghQAAhMAMhIIF8AAQgRBFAABRQAAAgADAfIEYAAQAPCpBhByIIWAAIAAB/IBEAAQBPAiBcAAQB/AABjgiQgpg8gehDIkNAAIAAkbIkYAAIAAhLIFtAAIAABLIEYAAIAAEbIFGAAIAVAXQBFBGBTAiIhHAAIAABbQgfAjgjAfgEBQ8gVvQAvgPApgYIAAhYIFtAAIAAB/gEgoOgXuIAAkbIkYAAIAAjVIkAAAIgehHIDJAAQgghIg3g1QiFiCkWABIjJAAIAAD+IlsAAIAAm4IuZAAIAAiDII+AAQFWAADYCDIOZAAQA8AlAyAtQCbCOA0DYIjLAAIAfBHIDqAAIAADVIEYAAIAAEbgEg4vgfeIAfhHIGBAAIgfBHgEhHAgfeQgKgmgPghIGCAAQAIAjAGAkgEhYAgfeIAAhHIFsAAIAABHgEAVXgglQA2jbCoilQAegeAhgaIobAAQAaAVAZAXIhRB1IAAihIhDAAIAAG4IlrAAIAAm4IBNAAQDki6E0ABQE7gBDwC6IN9AAIAAChIhsCbQhthfhcgkQhcgkhrABQjOAAiOCMQhCBBgjBVgEgqOgndIA4iDIFgAAIA5CDg");
	var mask_1_graphics_79 = new cjs.Graphics().p("EA6hAqXIhTh4IG9AAIBVB4gEArUAqXIAAh4IFrAAIAAB4gEATkAqXIAAh4IStAAIAAB4gEgITAofIgng3InpAAIAAA3IlqAAIAAg3IgFAAIAAgeISuAAIAAAeIBmAAIAnA3gEgt+AofIAAg3ISuAAIAAA3gEBLKAnoIgUgeIG8AAIAVAegEABeAnoIAAgeIFrAAIAAAegEgeRAnKIg6hUIFGAAIAAhkIAuAAICCC4gEgrQAnKIAAhUIgeAAIhGhkIGMAAIAABkIBDAAIAABUgEhDBAnKIAAhUIsjAAIAAhkIFrAAIAABEINCAAIAAAgIFEAAIAAhkIFqAAIAABkIB2AAIAABUgEAiUAkSIAAhAIAtBAgEAPlAkSIgyhHIG4AAIAFAHIAABAgEAEkAkSIAAhHIFsAAIAABHgEgTLAkSIAAhHIFrAAIAABHgEhIKAi9IAAhwIFrAAIAABwgEhY+Ah4IAAgrILPAAIAAArgEAuqAghIAAg5IvwAAIAAg5INxAAIAAgqIsvAAIAAhjIlkAAIAABjIs8AAIAAhjIgvAAIAAhWIgSAAQAJgNAGgPIiiAAIAAAcIkeAAIAAh/IFrAAIAABYILYAAIAAALIkFAAIgFAcIGwAAIAABKIGtAAIAAhKIFsAAIAABWIBzAAIAAhWIEcAAIAABWQBPgPAngjQASgQANgUIGHAAQgJAugPAoIjAAAIAABjIGuAAIAAhjIFrAAIAAAKIEJAAQBNAAA6gKIIEAAQgVA2gfAtIFVAAIAAAqI1lAAIAAgqIhCAAIAAAqIoEAAQhCBJhmApgAHJfoIAAg5IRDAAIAAAqIrYAAIAAAPgArWfoIAAg5IM6AAIAAA5gEBW8AevIAAgqIFfAAIAAAqgAhzbMIAAgcIFqAAIAAAcgA1GbMIAAgcIMlAAIAAAcgEAr4AawQATgqADg5IFyAAQgCA0gHAvgAdbawIAAhjIFrAAIAABjgEghXAZNIAAgrIFrAAIAAArgEBJlATrIAAg5IkmAAIAAhjISTAAIAABjIE5AAQAaAaAUAfgEAx0ATrIAAg5ISTAAIAAA5gEBcvASyIAAhjIQKAAQBJAbAyApQASAOARARgAK0RPIAAg8IJoAAQEEgBCeA9gAs7RPIAAg8ISSAAIAAA8gEg9JgQHIAAjLITNAAIgQANQjjC+muAAgEBFZgT9QgcgcgYgdIU0AAIAAAiIgVAXgEA0EgT9Igsg5IJRAAIAAA5gEAlOgT9IAAg5IFrAAIAAA5gAamz9IgZg5IGCAAIAZA5gAIhz9IAZg5IGCAAIgZA5gAxvz9IAAg5IksAAQgXgcgTgdIi0AAIhhh/IjfAAQgVgwgOg0IGJAAQAZA3AoAtIIVAAIAAB/IBEAAQBQAiBbAAQB/AABjgiQgqg8gdhDIkNAAIAAhkIFtAAIAABkIFGAAIAVAXQBFBGBTAiIhHAAIAAA5IEeAAQgWAegZAbgEgnogU2Igrg5IA7AAIAAh/ImPAAIA4B/IjYAAIAAA5IlrAAIAAg5IlXAAIAZA5ImDAAIgUgwIqpAAIgUAwImCAAIAZg5IAZAAIAhgNQAbgNAZgQQAegUAZgXQAUgUASgWIpBAAIAxhyIEHAAIAAipIkYAAIAAjVIkAAAIgehHIDJAAQghhIg3g1QhahYicgcIJmAAQBJBqAhCHIjLAAIAeBHIDqAAIAADVIB1AAIAAC3IMNAAIAqBkIldAAQgRAsgWApIgPAdIgIANIF0AAIA3h/IF/AAIAAhkIFrAAIAABkIGTAAIhMhkIG4AAIAHAJIAAgJIFqAAIAABkIDWAAIAAB/IDXAAIAAA5gEhsxgU2IAAg5IFsAAIAAAoIDlAAQCLAABngoIBXAAIAAh/IlRAAQAngyAYhAIF5AAQgNA8gTA2IEkAAIAAB/IBKAAQgSAdgVAcgAFs1vQAwgPApgYIAAhYIFtAAIAAB/gEhzdgXuIAAhyIFrAAIAABygEBtxgZgIAAipIkYAAIAAhLIFtAAIAABLIEYAAIAACpgEhjNgfeIAfhHIGCAAIgfBHgEhxdgfeQgKglgPgiIGCAAQAIAjAGAkgEgncgf6IAAgrIhCAAQgIAVgHAWImAAAIAKgrIjeAAIAAi4IJ7AAICMC4IEJAAIAAArgEg5ugf6IAAgrIFrAAIAAArgEhFvgf6IgFgHIAAAHIlrAAIAAgrICPAAIBQi4IKtAAIBQC4IjVAAIAgArgEhDOgglIBKAAIglhYgEgVFgglQAYhiAuhWIHMAAQgVAQgTASQhBBBgkBVgEggQgglIAAi4IFrAAIAAC4gEho/gglIAAjxIFrAAIAADxgEgC6gjdIB2AAIgrA8QgmgiglgagEBWpgkWIgggOQhcgkhrABQh6gBhkAyIoBAAQAshDA7g9IRzAAIAABaIgaAmgEA6xgkWIAAiAIFqAAIAACAgEAo/gkWIAAiAIgnAAIAPgPQAegeAhgaImAAAIAAi3IhBAAQAfgCAgAAQE7gBDwC6IN9AAIAABHIpfAAIBhCAgEBBfgmWIA+AAIg+Bag");

	this.timeline.addTween(cjs.Tween.get(mask_1).to({graphics:null,x:0,y:0}).wait(6).to({graphics:mask_1_graphics_6,x:359.85,y:1020}).wait(5).to({graphics:mask_1_graphics_11,x:460.85,y:1020}).wait(4).to({graphics:mask_1_graphics_15,x:382.775,y:1017.825}).wait(6).to({graphics:mask_1_graphics_21,x:313.175,y:1017.825}).wait(49).to({graphics:mask_1_graphics_70,x:382.775,y:1017.825}).wait(4).to({graphics:mask_1_graphics_74,x:460.85,y:1020}).wait(5).to({graphics:mask_1_graphics_79,x:359.85,y:1020}).wait(5).to({graphics:null,x:0,y:0}).wait(9));

	// Layer_8
	this.instance_1 = new lib.masque();
	this.instance_1.parent = this;
	this.instance_1.setTransform(353.7,938.65,2.8542,1.6381,0,0,0,0.2,265.6);
	this.instance_1.alpha = 0.8984;
	this.instance_1._off = true;

	var maskedShapeInstanceList = [this.instance_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(6).to({_off:false},0).to({_off:true},78).wait(9));

	// Layer_6
	this.instance_2 = new lib.img_danger();
	this.instance_2.parent = this;
	this.instance_2.setTransform(373.85,963,2.1186,2.1186,0,0,0,0.1,320.2);
	this.instance_2._off = true;

	var maskedShapeInstanceList = [this.instance_2];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(6).to({_off:false},0).to({y:1003},77).to({_off:true},1).wait(9));

	// Layer_7 (mask)
	var mask_2 = new cjs.Shape();
	mask_2._off = true;
	var mask_2_graphics_0 = new cjs.Graphics().p("AwjRVMAAAgipMAhHAAAMAAAAipg");
	var mask_2_graphics_8 = new cjs.Graphics().p("EgefAvcMAAAhe3MA8/AAAMAAABe3g");
	var mask_2_graphics_81 = new cjs.Graphics().p("AwjRWMAAAgirMAhHAAAMAAAAirg");

	this.timeline.addTween(cjs.Tween.get(mask_2).to({graphics:mask_2_graphics_0,x:203.875,y:1193.1}).wait(8).to({graphics:mask_2_graphics_8,x:261.05,y:1112.225}).wait(73).to({graphics:mask_2_graphics_81,x:203.875,y:1053.1}).wait(12));

	// Layer_9
	this.instance_3 = new lib._200_gif();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-653.6,1419.6,8.7047,8.7047,0,0,0,126.3,100.1);
	this.instance_3._off = true;

	var maskedShapeInstanceList = [this.instance_3];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_2;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(52).to({_off:false},0).to({_off:true},40).wait(1));

	// Layer_3
	this.instance_4 = new lib.img_danger();
	this.instance_4.parent = this;
	this.instance_4.setTransform(158.55,1108.2,0.65,0.65,0,0,0,0.1,320.2);

	var maskedShapeInstanceList = [this.instance_4];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_2;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_4).to({regX:0.3,regY:320.3,x:158.7,y:1101.55},15).to({regX:0.1,regY:320.2,x:158.55,y:1067.2},76).to({_off:true},1).wait(1));

	// Layer_1
	this.instance_5 = new lib.img_danger();
	this.instance_5.parent = this;
	this.instance_5.setTransform(312.35,964,2.1186,2.1186,0,0,0,0.1,320.2);
	this.instance_5.alpha = 0.1094;
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(6).to({_off:false},0).to({y:1005},77).to({_off:true},1).wait(9));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-469.6,0,1568.5,1781.9);


(lib.vignette2_action = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_91 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(91).call(this.frame_91).wait(1));

	// Layer_2
	this.instance = new lib.txt_violence();
	this.instance.parent = this;
	this.instance.setTransform(167.75,902.95,0.1,0.1,0,0,0,0,255.1);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(2).to({_off:false},0).wait(90));

	// Layer_5 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	var mask_1_graphics_6 = new cjs.Graphics().p("EAy1AueIAAkJIIpAAIAAEJgEBlDApCIAAjIIE5AAIAADIgEA9FApCIAGgUQAfhgA6hUIHwAAQhfAbhNBHQgoAlgbAtIgLAUgEAy1ApCIAAjIIA6AAIAADIgEBKWAl6IGDAAIiUCnQhmiAiJgngEgPwAjaIAAguIjTAAIAAAGIk5AAIAAkcIIMAAIAADIIHkAAIAAB8gEg8FAjaQDDibELAAQCrAACPBBIAAjIQCRBBB1CDIgvA2IjXAAIAAAogEhOHAjaIAAh8IE3AAIAAB8gEhdaAjaIAAh8IEkAAIBfB8gAvwQ7IAAjIIouAAIAAh1ImQAAIAAjdIE5AAIAAA9IKFAAIAADIIBIAAIAACgIGQAAIAAB1gEg1UAPGIAAjdIE5AAIAAA7IAeAAIAACigEgvEANzIAAh1Ig5AAIAAiiII1AAIAACiIGQAAIAAB1gEBeRALpIAAm3IOrAAIAADSIpyAAIAADlgEBHrALpIAAm3IE5AAIAAG3gEAmBALCIAAhkIEDAAIAABkgAPbLCIAAhkIE5AAIAABkgEAy7AJnIAAkLILIAAIAAELgAfxJeIAAloILJAAIAAAEIJyAAIAAA4IqoAAIAAA5IlaAAIAADzgAKfJeIAAloIDlAAIAAFogAyOD2IAAhlIKTAAIAAA8IA2AAIAAApgEgngAD2IAAgpIhUAAIAAnLIBUAAIAAGPIDlAAIAABlgA8UCRIAAg9IE5AAIAAA9gAn7glIAAjZICNAAIAAg9IPvAAIAAA9IsfAAIAADZgEgjEgQeIjIAAQgrgZgpgfIAAkIICZAAIAJALQB5CCCuABQCvgBB4iCIAKgLIFrAAQg0BxhfBdQhGBFhRAtIDIAAQijBdjQAAQjQAAikhdgEg2kgVeIAAhQIjIAAIAAlAIB+AAQARgTAOgVIFdAAQgIAUgJAUIiwAAIAAFAIDIAAIAABQgEgnggWjIAAgzIJ+AAIAAAzgEhK6gYmIgghQIjIAAIhAigIHJAAIhACgIDIAAIggBQgEhBwgbuIAAgoIACAAQAOAVARATgEhsqgbuIgRgoIJqAAIgQAogEhBsghXQADgzAJgxIFJAAQgPAwgFA0gEhPkghXIAAifIE5AAIAACfgEhhhghXIBAifIFRAAIhBCfgEhorghXIgohkIFRAAIAoBkgEg7Qgi7QAGgeAJgdIAAAAIA4iNIiUAAIAAh3IDbAAIAwB3IDaAAIg4CNIABAAQgOAdgKAegEhjDgi7IgZg7IBfAAIAAA7gEALYgj2QAjgXAmgSQCShFC0AAQC0AACSBFQAmASAjAXgEgEvgj2QgPgggUgcIiBAAIAAloIEgAAIAAFoIAzAAIAAA8gEg97gj2IgZg8IB3AAIAAA8gEgz5gmDIAwh3IBpAAIAAB3gEg6RgqaIAXg7IFSAAIgXA7gEhIsgqaIhokDIFSAAIBoEDg");
	var mask_1_graphics_11 = new cjs.Graphics().p("EAHvA1rIDIAAQisgwiBh+QhFhDguhPIBBAAQA4ggAxgwIBQAAIBOBQIBdAAQBdA6B2AAQCCAABng6IEHAAIBPBSQidCzjEA7IjIAAQhjAehsAAQh8AAhrgegEAezAwrIAAhQIDgAAIAABQgEAoLAvqIAAgPIDIAAIAAlAIE4AAIAAA6ILOAAIAAEGIjIAAIAAAPgEgOAAvqIgLgPIDIAAIjzlAIGuAAQgXgmgRgqIFvAAQAJALAKAKQAjAiAoAZIkdAAIAAFAIjIAAIAAAPgEANRAvbQATgVATgXIAqAsgEACvAvbIAAgpQAOAVARAUgEgXhAvbIAAjIIg5AAIAAAPIk4AAIAAgPIDIAAIAAlAIhMAAIg9hQIkHAAIAAn0II5AAIAADIIBcAAIE9GhIAAmhIE4AAIAAH0ImQAAIAABQIiyAAIAAFAgEgXhApLIBeAAIheh8gEgbiAmDIEBAAIAAh8IkBlSgEAiTAqbIAAhQIBYAAIAABQgEAlDApLIAAn0ICEAAIAAjIIE4AAIAADIIiEAAIAABTIJyAAIAAEJIpyAAIAACYgEgCgApLQg2iCAAieQAAhxAchjIFTAAQguBZAAB2QAACUBEBoIAAApgEAX1AnnIAAjIIH8AAICZDIgEgmqAnTIAAhQIE4AAIAABQgEBdTAhXIAAjIIE4AAIAADIgEA1VAhXIAGgUQAehhA6hTIHwAAQhfAbhMBHQgpAlgbAtIgLAUgEBCmAePIGCAAIiTCnQhniAiIgngEg6ZAePQg+gShDAAQhFAAg8ASInxAAQAng5A0gyQDUjQE1AAQCrAACQBBIAAjIQCQBBB1CDIgvA2IjWAAIAADIgEhV4AePIAAkcIE4AAIAAEcgEhlKAePIAAkcIEkAAIDYEcgA3heKIAAjJIjTAAIAAAGIk4AAIAAkcIILAAIAADIIHkAAIAAEXgA3hJQIAAjIIotAAIAAh1ImQAAIAAjdIE4AAIAAA9IKFAAIAADIIBJAAIAACgIGQAAIAAB1gEg9EAHbIAAjdIE4AAIAAA7IAfAAIAACigEg20AGIIAAh1Ig5AAIAAiiII1AAIAACiIGQAAIAAB1gEBWhAD+IAAm2IOqAAIAADRIpyAAIAADlgEA/7AD+IAAm2IE4AAIAAG2gAeRDXIAAhkIECAAIAABkgAHrDXIAAhkIE4AAIAABkgEArLAB8IAAkKILHAAIAAEKgAYBBzIAAnMIKSAAIAAA8IA2AAIAAAtIJyAAIAAA4IqoAAIAAA5IlaAAIAADygACvBzIAAmQIhUAAIAAnMIBUAAIAAGQIDkAAIAAHMgAN7lZIAAg9IE4AAIAAA9gEAiTgIQIAAjZICOAAIAAg9IPvAAIAAA9IsgAAIAADZgAHL4JIjIAAQgsgZgogfIAAkIICYAAIAKALQB4CCCvABQCvgBB4iCIAKgLIFrAAQg1BxhfBdQhGBFhQAtIDIAAQikBdjPAAQjQAAikhdgAsU9JIAAhQIjIAAIAAlAIB+AAQA2hAAchMIFIAAQgQBKgfBCIixAAIAAFAIDIAAIAABQgACv+OIAAhDQgjgbgggfQhfhdgzhxIDVAAIAAEEIFGAAIAAjDILHAAIAADDImQAAIAABHgEggrggRIgghQIjIAAIiAlAIJKAAIiBFAIDIAAIggBQgEgXhgjZIAAgrQAPAWASAVgEhCbgjZIg4iMIFRAAIAMAfIANgfIAmAAIiBlBIFSAAICAFBIgnAAIg4CMgEACvgkVIAAgIIhKAAIAAhIIBKAAIAAgpICNAAQAWA6AMA/gEgF8gllQAHgUAGgUQAPg8AAhEQAAhRgXhIIFGAAQAOBJAABPQAABDgKA+QgDAUgEAUgEgXNgllQgEgUgDgUQgKg+AAhDQAAhPAOhJIFJAAQgXBIAABRQAABEAPA8QAGAUAHAUgEAbkgmOQgRgXgUgXQh4iDivAAQivAAh4CDQgVAXgQAXIliAAQA0iHBuhrQBZhXBrgzQCThFC0AAQCzAACTBFQBqAzBZBXQBvBrA1CHgEgcugmhQgfhCgQhKIFLAAQASA0AfAtIAAArgEgrkgmhIAAiMIE4AAIAACMgEglUgotIAAlBIE4AAIAAFBgEg3ZgotICAlBIiUAAIAAh3IDbAAIAwB3IDaAAIiBFBgEAAYgqmQgVhAgmg3IiZAAIAAmjIE3AAIAAGjIA0AAIAAB3gEgRAgqmQAMg+AXg5Ih0AAICnmjIFSAAIioGjICFAAQgoA3gUBAgEg40gqmIgwh3IB3AAIAAB3gEgfEgtuIAAh3IBoAAIiomjIFSAAICoGjIiCAAIAAB3gEgvJgtuIAwh3IFRAAIgwB3g");
	var mask_1_graphics_15 = new cjs.Graphics().p("EgFTAxPIDIAAQisgxiBh9Qhnhkg0h/IFvAAQAJALAKALQB2B0CnABQDIAACKiLIBQAAICdCjQidCyjEA8IjIAAQhiAdhsAAQh8AAhrgdgEAbIAxdIAAgOIDIAAIAAmRIE4AAIAACLILOAAIAAEGIjIAAIAAAOgEgbDAxdIgLgOIDIAAIkwmRIkHAAIAAvYIEkAAIKuOFIAAuFIE4AAIAAPYImQAAIAAGRIjIAAIAAAOgEgcFAq+IFfAAIlfnNgEgqVAxdIAAgOIDIAAIAAmRIE4AAIAAGRIjIAAIAAAOgEAkgAq+IAAvYIPvAAIAAEXIq3AAIAAEgIJyAAIAAEJIpyAAIAACYgEAMuAq+QATgUATgXIAqArgEgDDAq+Qg2iBAAifQAAk3DVjPQDTjRE1ABQFYAADoEFIjCDcQiVi4jbAAQiwAAh9BzQh9BzAADDQAACzBjBxgEAj+ALDIAAh1ImQAAIAAr3IE4AAIAAAtIJyAAIAAEIIpyAAIAAEjILOAAIAACfIGQAAIAAB1gANYLDIAAh1ImQAAIAAr3IE4AAIAAJVIJUAAIAACiIGQAAIAAB1gAtnDvIAAkJILHAAIAAEJgAXeipIAAoKIPvAAIAAEXIq3AAIAADzgAA4ipIAAoKIE4AAIAAIKgAl38mIjIAAQhRgthGhEQiWiUgsjHIFLAAQAeBTA+BEQB4CDCvAAQCvAAB3iDQA+hEAehTIFIAAQgsDHiXCUQhGBEhQAtIDIAAQikBdjOABQjQgBikhdgA5X7WIAAhQIjIAAIAAnMIE4AAIAAHMIDIAAIAABQgEgtugbWIgghQIjIAAIi4nMIFRAAIAMAgIANggIAmAAIiBlAIFSAAICAFAIgnAAIi5HMIDIAAIggBQgEAQqgiqIAAhIIGQAAIAAjCILHAAIAADCImQAAIAABIgEAJIgjyQAchMAAhbQAAhRgXhIIFGAAQAOBJAABPQAABZgRBPgEgIIgjyQgRhPAAhZQAAhPAOhJIFJAAQgXBIAABRQAABbAcBMgEgWPgjyIAAlAIE4AAIAAFAgEgoUgjyICAlAIjbAAIjYoaIFSAAIDYIaIDaAAIiBFAgEAPdgoyQgehchDhLQh4iDivAAQivAAh4CDQhEBLgdBcIlIAAQAojRCciaQDUjPE3AAQE1AADUDPQCeCaAoDRgEgP/goyIAAoaIE4AAIAAIagEggEgoyIDXoaIFSAAIjZIag");
	var mask_1_graphics_21 = new cjs.Graphics().p("EgJDAuhQjRjLAAk4QAAk3DVjPQDUjRE1ABQFXAADoEFIjCDcQiUi4jbAAQiwAAh9BzQh9BzAADDQAADEB3B2QB1B0CoABQDlAACSi2IDHDOQjtELlCAAQlDAAjRjLgEAcFAxdIAA13IPwAAIAAEXIq3AAIAAEgIJxAAIAAEJIpxAAIAAEjILNAAIAAEUgEgaFAxdIqatsIAANsIk5AAIAA13IEkAAIKvOFIAAuFIE3AAIAAV3gAbjLDIAA12IPvAAIAAEXIq3AAIAAEgIJzAAIAAEIIpzAAIAAEjILOAAIAAEUgAE9LDIAA12IE4AAIAARfIJUAAIAAEXgAvyDvIAAkJILHAAIAAEJgACG+XQjSjQAAkzQAAkzDSjQQDVjPE2AAQE2AADTDPQDVDQAAEzQAAEzjVDQQjTDOk2ABQk2gBjVjOgEAFrgrZQh4CFAAC7QAAC7B4CDQB4CDCvAAQCvAAB4iDQB4iDAAi7QAAi7h4iFQh4iDivAAQivAAh4CDgAvC7WIAA12IE4AAIAAV2gEgjYgbWIoy12IFSAAIFkN6IFkt6IFSAAIoyV2gEAeHgiqIAAkKILIAAIAAEKg");
	var mask_1_graphics_70 = new cjs.Graphics().p("EgFTAxPIDIAAQisgxiBh9Qhnhkg0h/IFvAAQAJALAKALQB2B0CnABQDIAACKiLIBQAAICdCjQidCyjEA8IjIAAQhiAdhsAAQh8AAhrgdgEAbIAxdIAAgOIDIAAIAAmRIE4AAIAACLILOAAIAAEGIjIAAIAAAOgEgbDAxdIgLgOIDIAAIkwmRIkHAAIAAvYIEkAAIKuOFIAAuFIE4AAIAAPYImQAAIAAGRIjIAAIAAAOgEgcFAq+IFfAAIlfnNgEgqVAxdIAAgOIDIAAIAAmRIE4AAIAAGRIjIAAIAAAOgEAkgAq+IAAvYIPvAAIAAEXIq3AAIAAEgIJyAAIAAEJIpyAAIAACYgEAMuAq+QATgUATgXIAqArgEgDDAq+Qg2iBAAifQAAk3DVjPQDTjRE1ABQFYAADoEFIjCDcQiVi4jbAAQiwAAh9BzQh9BzAADDQAACzBjBxgEAj+ALDIAAh1ImQAAIAAr3IE4AAIAAAtIJyAAIAAEIIpyAAIAAEjILOAAIAACfIGQAAIAAB1gANYLDIAAh1ImQAAIAAr3IE4AAIAAJVIJUAAIAACiIGQAAIAAB1gAtnDvIAAkJILHAAIAAEJgAXeipIAAoKIPvAAIAAEXIq3AAIAADzgAA4ipIAAoKIE4AAIAAIKgAl38mIjIAAQhRgthGhEQiWiUgsjHIFLAAQAeBTA+BEQB4CDCvAAQCvAAB3iDQA+hEAehTIFIAAQgsDHiXCUQhGBEhQAtIDIAAQikBdjOABQjQgBikhdgA5X7WIAAhQIjIAAIAAnMIE4AAIAAHMIDIAAIAABQgEgtugbWIgghQIjIAAIi4nMIFRAAIAMAgIANggIAmAAIiBlAIFSAAICAFAIgnAAIi5HMIDIAAIggBQgEAQqgiqIAAhIIGQAAIAAjCILHAAIAADCImQAAIAABIgEAJIgjyQAchMAAhbQAAhRgXhIIFGAAQAOBJAABPQAABZgRBPgEgIIgjyQgRhPAAhZQAAhPAOhJIFJAAQgXBIAABRQAABbAcBMgEgWPgjyIAAlAIE4AAIAAFAgEgoUgjyICAlAIjbAAIjYoaIFSAAIDYIaIDaAAIiBFAgEAPdgoyQgehchDhLQh4iDivAAQivAAh4CDQhEBLgdBcIlIAAQAojRCciaQDUjPE3AAQE1AADUDPQCeCaAoDRgEgP/goyIAAoaIE4AAIAAIagEggEgoyIDXoaIFSAAIjZIag");
	var mask_1_graphics_74 = new cjs.Graphics().p("EAHvA1rIDIAAQisgwiBh+QhFhDguhPIBBAAQA4ggAxgwIBQAAIBOBQIBdAAQBdA6B2AAQCCAABng6IEHAAIBPBSQidCzjEA7IjIAAQhjAehsAAQh8AAhrgegEAezAwrIAAhQIDgAAIAABQgEAoLAvqIAAgPIDIAAIAAlAIE4AAIAAA6ILOAAIAAEGIjIAAIAAAPgEgOAAvqIgLgPIDIAAIjzlAIGuAAQgXgmgRgqIFvAAQAJALAKAKQAjAiAoAZIkdAAIAAFAIjIAAIAAAPgEANRAvbQATgVATgXIAqAsgEACvAvbIAAgpQAOAVARAUgEgXhAvbIAAjIIg5AAIAAAPIk4AAIAAgPIDIAAIAAlAIhMAAIg9hQIkHAAIAAn0II5AAIAADIIBcAAIE9GhIAAmhIE4AAIAAH0ImQAAIAABQIiyAAIAAFAgEgXhApLIBeAAIheh8gEgbiAmDIEBAAIAAh8IkBlSgEAiTAqbIAAhQIBYAAIAABQgEAlDApLIAAn0ICEAAIAAjIIE4AAIAADIIiEAAIAABTIJyAAIAAEJIpyAAIAACYgEgCgApLQg2iCAAieQAAhxAchjIFTAAQguBZAAB2QAACUBEBoIAAApgEAX1AnnIAAjIIH8AAICZDIgEgmqAnTIAAhQIE4AAIAABQgEBdTAhXIAAjIIE4AAIAADIgEA1VAhXIAGgUQAehhA6hTIHwAAQhfAbhMBHQgpAlgbAtIgLAUgEBCmAePIGCAAIiTCnQhniAiIgngEg6ZAePQg+gShDAAQhFAAg8ASInxAAQAng5A0gyQDUjQE1AAQCrAACQBBIAAjIQCQBBB1CDIgvA2IjWAAIAADIgEhV4AePIAAkcIE4AAIAAEcgEhlKAePIAAkcIEkAAIDYEcgA3heKIAAjJIjTAAIAAAGIk4AAIAAkcIILAAIAADIIHkAAIAAEXgA3hJQIAAjIIotAAIAAh1ImQAAIAAjdIE4AAIAAA9IKFAAIAADIIBJAAIAACgIGQAAIAAB1gEg9EAHbIAAjdIE4AAIAAA7IAfAAIAACigEg20AGIIAAh1Ig5AAIAAiiII1AAIAACiIGQAAIAAB1gEBWhAD+IAAm2IOqAAIAADRIpyAAIAADlgEA/7AD+IAAm2IE4AAIAAG2gAeRDXIAAhkIECAAIAABkgAHrDXIAAhkIE4AAIAABkgEArLAB8IAAkKILHAAIAAEKgAYBBzIAAnMIKSAAIAAA8IA2AAIAAAtIJyAAIAAA4IqoAAIAAA5IlaAAIAADygACvBzIAAmQIhUAAIAAnMIBUAAIAAGQIDkAAIAAHMgAN7lZIAAg9IE4AAIAAA9gEAiTgIQIAAjZICOAAIAAg9IPvAAIAAA9IsgAAIAADZgAHL4JIjIAAQgsgZgogfIAAkIICYAAIAKALQB4CCCvABQCvgBB4iCIAKgLIFrAAQg1BxhfBdQhGBFhQAtIDIAAQikBdjPAAQjQAAikhdgAsU9JIAAhQIjIAAIAAlAIB+AAQA2hAAchMIFIAAQgQBKgfBCIixAAIAAFAIDIAAIAABQgACv+OIAAhDQgjgbgggfQhfhdgzhxIDVAAIAAEEIFGAAIAAjDILHAAIAADDImQAAIAABHgEggrggRIgghQIjIAAIiAlAIJKAAIiBFAIDIAAIggBQgEgXhgjZIAAgrQAPAWASAVgEhCbgjZIg4iMIFRAAIAMAfIANgfIAmAAIiBlBIFSAAICAFBIgnAAIg4CMgEACvgkVIAAgIIhKAAIAAhIIBKAAIAAgpICNAAQAWA6AMA/gEgF8gllQAHgUAGgUQAPg8AAhEQAAhRgXhIIFGAAQAOBJAABPQAABDgKA+QgDAUgEAUgEgXNgllQgEgUgDgUQgKg+AAhDQAAhPAOhJIFJAAQgXBIAABRQAABEAPA8QAGAUAHAUgEAbkgmOQgRgXgUgXQh4iDivAAQivAAh4CDQgVAXgQAXIliAAQA0iHBuhrQBZhXBrgzQCThFC0AAQCzAACTBFQBqAzBZBXQBvBrA1CHgEgcugmhQgfhCgQhKIFLAAQASA0AfAtIAAArgEgrkgmhIAAiMIE4AAIAACMgEglUgotIAAlBIE4AAIAAFBgEg3ZgotICAlBIiUAAIAAh3IDbAAIAwB3IDaAAIiBFBgEAAYgqmQgVhAgmg3IiZAAIAAmjIE3AAIAAGjIA0AAIAAB3gEgRAgqmQAMg+AXg5Ih0AAICnmjIFSAAIioGjICFAAQgoA3gUBAgEg40gqmIgwh3IB3AAIAAB3gEgfEgtuIAAh3IBoAAIiomjIFSAAICoGjIiCAAIAAB3gEgvJgtuIAwh3IFRAAIgwB3g");
	var mask_1_graphics_79 = new cjs.Graphics().p("EAy1AueIAAkJIIpAAIAAEJgEBlDApCIAAjIIE5AAIAADIgEA9FApCIAGgUQAfhgA6hUIHwAAQhfAbhNBHQgoAlgbAtIgLAUgEAy1ApCIAAjIIA6AAIAADIgEBKWAl6IGDAAIiUCnQhmiAiJgngEgPwAjaIAAguIjTAAIAAAGIk5AAIAAkcIIMAAIAADIIHkAAIAAB8gEg8FAjaQDDibELAAQCrAACPBBIAAjIQCRBBB1CDIgvA2IjXAAIAAAogEhOHAjaIAAh8IE3AAIAAB8gEhdaAjaIAAh8IEkAAIBfB8gAvwQ7IAAjIIouAAIAAh1ImQAAIAAjdIE5AAIAAA9IKFAAIAADIIBIAAIAACgIGQAAIAAB1gEg1UAPGIAAjdIE5AAIAAA7IAeAAIAACigEgvEANzIAAh1Ig5AAIAAiiII1AAIAACiIGQAAIAAB1gEBeRALpIAAm3IOrAAIAADSIpyAAIAADlgEBHrALpIAAm3IE5AAIAAG3gEAmBALCIAAhkIEDAAIAABkgAPbLCIAAhkIE5AAIAABkgEAy7AJnIAAkLILIAAIAAELgAfxJeIAAloILJAAIAAAEIJyAAIAAA4IqoAAIAAA5IlaAAIAADzgAKfJeIAAloIDlAAIAAFogAyOD2IAAhlIKTAAIAAA8IA2AAIAAApgEgngAD2IAAgpIhUAAIAAnLIBUAAIAAGPIDlAAIAABlgA8UCRIAAg9IE5AAIAAA9gAn7glIAAjZICNAAIAAg9IPvAAIAAA9IsfAAIAADZgEgjEgQeIjIAAQgrgZgpgfIAAkIICZAAIAJALQB5CCCuABQCvgBB4iCIAKgLIFrAAQg0BxhfBdQhGBFhRAtIDIAAQijBdjQAAQjQAAikhdgEg2kgVeIAAhQIjIAAIAAlAIB+AAQARgTAOgVIFdAAQgIAUgJAUIiwAAIAAFAIDIAAIAABQgEgnggWjIAAgzIJ+AAIAAAzgEhK6gYmIgghQIjIAAIhAigIHJAAIhACgIDIAAIggBQgEhBwgbuIAAgoIACAAQAOAVARATgEhsqgbuIgRgoIJqAAIgQAogEhBsghXQADgzAJgxIFJAAQgPAwgFA0gEhPkghXIAAifIE5AAIAACfgEhhhghXIBAifIFRAAIhBCfgEhorghXIgohkIFRAAIAoBkgEg7Qgi7QAGgeAJgdIAAAAIA4iNIiUAAIAAh3IDbAAIAwB3IDaAAIg4CNIABAAQgOAdgKAegEhjDgi7IgZg7IBfAAIAAA7gEALYgj2QAjgXAmgSQCShFC0AAQC0AACSBFQAmASAjAXgEgEvgj2QgPgggUgcIiBAAIAAloIEgAAIAAFoIAzAAIAAA8gEg97gj2IgZg8IB3AAIAAA8gEgz5gmDIAwh3IBpAAIAAB3gEg6RgqaIAXg7IFSAAIgXA7gEhIsgqaIhokDIFSAAIBoEDg");

	this.timeline.addTween(cjs.Tween.get(mask_1).to({graphics:null,x:0,y:0}).wait(6).to({graphics:mask_1_graphics_6,x:799.0375,y:921.7}).wait(5).to({graphics:mask_1_graphics_11,x:528.675,y:970.8}).wait(4).to({graphics:mask_1_graphics_15,x:612.175,y:959.25}).wait(6).to({graphics:mask_1_graphics_21,x:606.05,y:959.25}).wait(49).to({graphics:mask_1_graphics_70,x:612.175,y:959.25}).wait(4).to({graphics:mask_1_graphics_74,x:528.675,y:970.8}).wait(5).to({graphics:mask_1_graphics_79,x:799.0375,y:921.7}).wait(5).to({graphics:null,x:0,y:0}).wait(8));

	// Layer_8
	this.instance_1 = new lib.masque();
	this.instance_1.parent = this;
	this.instance_1.setTransform(353.7,938.65,2.8542,1.6381,0,0,0,0.2,265.6);
	this.instance_1.alpha = 0.8984;
	this.instance_1._off = true;

	var maskedShapeInstanceList = [this.instance_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(6).to({_off:false},0).to({_off:true},78).wait(8));

	// Layer_6
	this.instance_2 = new lib.img_action();
	this.instance_2.parent = this;
	this.instance_2.setTransform(373.85,963,2.1186,2.1186,0,0,0,0.1,320.2);
	this.instance_2._off = true;

	var maskedShapeInstanceList = [this.instance_2];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(6).to({_off:false},0).to({y:1003},77).to({_off:true},1).wait(8));

	// Layer_7 (mask)
	var mask_2 = new cjs.Shape();
	mask_2._off = true;
	var mask_2_graphics_0 = new cjs.Graphics().p("A4ORVMAAAgipMAwdAAAMAAAAipg");
	var mask_2_graphics_8 = new cjs.Graphics().p("EhCNA8oMAAAh5PMCEbAAAMAAAB5Pg");
	var mask_2_graphics_81 = new cjs.Graphics().p("A4ORVMAAAgipMAwdAAAMAAAAipg");

	this.timeline.addTween(cjs.Tween.get(mask_2).to({graphics:mask_2_graphics_0,x:633.2,y:862.35}).wait(8).to({graphics:mask_2_graphics_8,x:485.95,y:869.8}).wait(73).to({graphics:mask_2_graphics_81,x:633.2,y:862.35}).wait(11));

	// Layer_3
	this.instance_3 = new lib.img_action();
	this.instance_3.parent = this;
	this.instance_3.setTransform(528.4,849.45,0.875,0.875,0,0,0,0.1,320.1);

	var maskedShapeInstanceList = [this.instance_3];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_2;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({y:869.45},91).wait(1));

	// Layer_1
	this.instance_4 = new lib.img_action();
	this.instance_4.parent = this;
	this.instance_4.setTransform(312.35,964,2.1186,2.1186,0,0,0,0.1,320.2);
	this.instance_4.alpha = 0.1094;
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off:false},0).to({y:943.5},77).to({scaleX:2.1187,scaleY:2.1187,x:410.5,y:923.05},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-395.4,494.7,1543.1999999999998,822.5999999999999);


(lib.vignette1_vitesse = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_91 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(91).call(this.frame_91).wait(1));

	// Layer_2
	this.instance = new lib.txt_vitesse();
	this.instance.parent = this;
	this.instance.setTransform(486.25,1055.45,0.1,0.1,0,0,0,0,255.1);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(2).to({_off:false},0).wait(90));

	// Layer_5 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	var mask_1_graphics_6 = new cjs.Graphics().p("EgCzAxVIAAhaIQFAAIAABagEgkkAv7IAAikIqQAAIAAkVIE5AAIAAD/ILNAAIAAAWIKQAAIAACkgEgB+AqBIAAg/ILGAAIAAA/gEgWIAqBIAAg/IJ8AAIAAgfIE5AAIAAAfIjtAAIAAA/gEAgpApCIAAgfILIAAIAAAfgEAMgApCIAAgfIJEAAIAAhIIOrAAIAABEIpyAAIAAAEIi2AAIAAAfgEBCaAojIAAhIILIAAIAABIgEAuQAojIAAhIILIAAIAABIgA3AbjIAAgEIWEAAIAAAEgEAwqAKyIAAAJIk6AAIAAhWINpAAQiJBmjYAAQhqAAhkgZgAShK7IAAAAIrNAAIAAhWITSAAQiIBmjYAAQhVAAhQgQgEhF6AHVIAAhDQBZAlBTAAQBVAAAxglQAKgHAIgIIDoAAIB2iSQBuBgBlAyIjaAAQgJArgSAngEgyeAGDIAIgJIAAAJgEgyyADKIgRgKIAtAAIAAAkQgMgOgQgMgEg5xADAIAAjWIHbAAIAACbIiiAAIAAA7gAoDixQgGgTgDgWIDJAAIAAApgEAkagEDQgCgaAAgdQAAg7AMgyIFpAAIgGAEQgtAlAAA9QAAAjASAbgAvXkDIAAjDICUAAIAAj0IKuAAQBfgfByAAQCNAACLAvQCMAwBoBYIhUB7IkJAAQhEgZhGgGIhRAAQgxAGghAZIkSAAIhKBqQifh6irgPIhRAAQg1AHgjAcQgtAlAAA9QAAAjATAbgEAu8gGnIEHAAIhJBqQhehIhggigA3QnGQgXgCgYAAQgSAAgQACImzAAQAihlBZhDQCOhrDWAAQCMAACMAvQCMAwBpBYIhABcgEheBgifIAAjXIhmAAIAADXIgtAAIAAj2IE5AAIAAAfICRAAIAADXgEgCogjBIAAkLIE3AAIAAELgEgE7gjBIAAkLIAtAAIAAELgEgXAgjBIAAg5IBnkBIAAgLIkMAAIAAjYIE5AAIAAA7IAWAAIANgfIFQAAIiUFzIAACOgEgrygjBIjOoBIFSAAIBLC8IAdAAIBWjYIFSAAIhXDYIjdAAIAABxIhkAAIBLC7IANggIAAA5gEhM2gl2IAAgfIhAAAIAthxIFSAAIguBxIAmAAIAAAfgEg+ngmVIAAhxIE4AAIAABxgEhZOgmVIiElJIFSAAIAqBqIAABuIAsAAIAuBxgEgGHgoGIAAgTImNAAIAAjFIRQAAIAADFImMAAIAAATgEgwTgoGIAAhuIAsBugEBPEgwLIAAgKIRRAAIAAAKgEBBzgwLIAAgKIE5AAIAAAKgEA0ogwLIAEgKIqqAAIAAg/IRRAAIAAA/IhWAAIgDAKgEAAGgwLIgdhJIFRAAIAeBJgEAcxgwVIAAg/IE4AAIAAA/gEAPpgwVIAZg/IFTAAIgaA/g");
	var mask_1_graphics_11 = new cjs.Graphics().p("EgQAAxVIAAj+IqQAAIAAkVIE5AAIAAD/ILNAAIAAAWIKPAAIAAD+gEASlAqBIAAg/ILIAAIAAA/gEgBkAqBIAAg/IJ8AAIAAhnIOqAAIAABEIpyAAIAAAjIjtAAIAAA/gEA1OApCIAAhnILHAAIAABngEAhEApCIAAhnILHAAIAABngEArRAlHIAAhkILIAAIAABkgEAXHAlHIAAhkILIAAIAABkgEgBkAlHIAAjoIE4AAIAAAiIJyAAIAADGgEAfzAhfIAAl8IZsAAIAACQIvwAAIAAgSIlEAAIAAD+gEgkMAbjIAAgEIZsAAIAAAEgA1WKyIAAAJIk6AAIAAilQgngegmgjIQvAAQgjBJhEA5QiMB0jnAAQhqAAhkgZgEgzgAK7IAAAAIrMAAIAAkUIE1AAIAAjnIE5AAIAADDIA7AAIAjAPIAABDIA0AAIAAAAIC5jkQDjDGC/AAQBWAAAxglQAyglAAg/QAAg/g1glQg0gliagmQidglhng0IAAg8IOSAAQAVAOARAPQAVATARAWIAPAVQAhA1AOBGIjdAAQAJAvAAA2QAABigmBOInjAAQgjBJhEA5QiNB0jmAAQhVAAhRgQgEhTHAHVIAAhDQBZAlBTAAQBVAAAxglQAKgHAIgIIDoAAIB2iSQBuBgBmAyIjaAAQgKArgSAngEg/qAGDQAggiAAgzQAAg/g1glIgRgKIF+AAQAJAvAAA2QAAAxgKAtgEhG9ADAIAAjWIOqAAIAACbIpyAAIAAA7gAzwgWQhUhGgVh+IDKAAIAAEAQg5gcgogggAXOkDQgDgaAAgdQAAjICQhsQCOhrDWAAQCNAACLAvQCMAwBpBYIifDlQi1iLjDAAQhOAAgtAlQgtAlAAA9QAAAjATAbgA8kkDIAAjDICUAAIAAj0IPwAAIAAD0InsAAIhfCJQifh6iqgPIhRAAQg2AHgjAcQgtAlAAA9QAAAjATAbgAGynGQgXgCgYAAQgSAAgQACImyAAQAihlBYhDQCOhrDWAAQCNAACLAvQCMAwBpBYIhABcgAR37eIAAmbIE3AAIAAGbgAPk7eIAAmbIAtAAIAAGbgAky7eIilmbIKoAAIAAGbgEgP1gh5IAAoqIhmAAIAAIqIgtAAIAApJIE5AAIAAAfICSAAIAAIqgEgkMgh5IAAiBIBmkBIAAioIBDAAIANgfIFRAAIiVFzIAADWgEg4igh5IjrpJIFSAAIDDHoIANggIAACBgEABVgqjIAAgfIhAAAIAthxIFSAAIguBxIAnAAIAAAfgEAjCgrCIAAhxIDDAAIBajiIqqAAIAAg/IRRAAIAAA/IhVAAIhbDiIjdAAIAABxgEAPkgrCIAAhxIE4AAIAABxgEgLCgrCIihmSIFSAAIBIC0IAABtIAsAAIAtBxgEBIEgszIAAgTImMAAIAAjPIRQAAIAADPImMAAIAAATgEA0mgszIAAjiIE5AAIAADigEAd4gszIAAhtIAsBtgEAPkgwVIAAg/IE4AAIAAA/gEACdgwVIAZg/IFSAAIgaA/g");
	var mask_1_graphics_15 = new cjs.Graphics().p("EgZPAxVIAAj+IqQAAIAAl8IOqAAIAABEIpyAAIAAEiILNAAIAAAWIKQAAIAAD+gEAJWAqBIAAimILHAAIAACmgEgK0AqBIAAimILHAAIAACmgEgAmAlHIAAhkILHAAIAABkgEgUwAlHIAAhkILIAAIAABkgEgtcAlHIAApoIZsAAIAACUIvvAAIAAgSIlEAAIAAEgIJxAAIAADGgAEoHVIIZAAIgBAAIC5jkQDjDGDAAAQBVAAAxglQAyglAAg/QAAg/g1glQg0gliagmQidglhmg0IAAkAIjLAAQgIgsAAg0QAAjICQhsQCOhrDXAAQCMAACMAvQCLAwBpBYIifDlQi1iLjCAAQhPAAgtAlQgsAlgBA9QABA9A2AkIAKAGIDeAAQA9AjC3AsQDHAxBuBhQAVATASAWIAOAVQAhA1AOBGIjdAAQAJAvAAA2QAABigmBOIoYAAQgjBJhDA5QiNB0jnAAQlLAAkKj2gAzaK7IAAAAIwFAAIAAn7IE4AAIAADnIFDAAIAegkIFJAAQBsA0BjAAQBVAAAxglQALgHAHgIIDoAAIB2iSQBuBgBmAyIjaAAQgbB9hoBXQiMB0jnAAQhVAAhRgQgAACGDQAggiAAgzQAAg/g0glIgRgKIF9AAQAJAvAAA2QAAAxgJAtgAnQDAIAAmaIjeAAQgIgsAAg0QABjICPhsQCPhrDWAAQCMAACLAvQCMAwBoBYIifDlQi0iLjDAAQhNAAgtAlQguAlABA9QAAA9A3AkIAKAGICaAAIAABWIJyAAIAAEJIpyAAIAAA7gEAr8gAWQhThGgWh+IDKAAIAAEAQg4gcgpgggEgjfgDaIAAngIPvAAIAAEWIq3AAIAADKgAm37eIAAvFIhmAAIAAPFIgtAAIAA12IE5AAIAAGxICSAAIAAPFgA9g7eIox12IFRAAIFlN6IB0khIAAioIBCAAICumxIFRAAIk2MFIAAJxgEAKTgqjIAAijImMAAIAAkOIRQAAIAAEOImMAAIAACjg");
	var mask_1_graphics_21 = new cjs.Graphics().p("EgfhAxVIAA12IPwAAIAAEWIq3AAIAAEgIJyAAIAAEKIpyAAIAAEiILNAAIAAEUgEANUAqBIAAkKILIAAIAAEKgEgG1AqBIAAkKILHAAIAAEKgAQ/HVIC5jkQDjDGC/AAQBWAAAxglQAyglAAg/QAAg/g1glQg0gliagmQjzg6hyhbQhxheAAjGQAAjICQhsQCOhrDWAAQCNAACLAvQCMAwBpBYIifDlQi1iLjDAAQhOAAgtAlQgtAlAAA9QAAA9A3AkQA4AlDGAwQDHAxBvBhQBtBjAAC7QAAC8iNB2QiMB0jnAAQlMAAkJj2gAnyHVIC5jkQDjDGC+AAQBWAAAxglQAyglAAg/QAAg/g1glQg0gliagmQjyg6hyhbQhxheAAjGQAAjICQhsQCOhrDVAAQCNAACLAvQCMAwBpBYIifDlQi1iLjDAAQhNAAgtAlQgtAlAAA9QAAA9A3AkQA4AlDFAwQDHAxBvBhQBtBjAAC7QAAC8iNB2QiMB0jnAAQlLAAkJj2gA/hK7IAA11IPwAAIAAEWIq3AAIAAEgIJyAAIAAEJIpyAAIAAEiILNAAIAAEUgAOS7eIAAxoImMAAIAAkOIRQAAIAAEOImMAAIAARogAlL7eIAA12IE5AAIAAV2gA5h7eIoy12IFSAAIFlN6IFjt6IFSAAIoxV2g");
	var mask_1_graphics_65 = new cjs.Graphics().p("EgZPAxVIAAj+IqQAAIAAl8IOqAAIAABEIpyAAIAAEiILNAAIAAAWIKQAAIAAD+gEAJWAqBIAAimILHAAIAACmgEgK0AqBIAAimILHAAIAACmgEgAmAlHIAAhkILHAAIAABkgEgUwAlHIAAhkILIAAIAABkgEgtcAlHIAApoIZsAAIAACUIvvAAIAAgSIlEAAIAAEgIJxAAIAADGgAEoHVIIZAAIgBAAIC5jkQDjDGDAAAQBVAAAxglQAyglAAg/QAAg/g1glQg0gliagmQidglhmg0IAAkAIjLAAQgIgsAAg0QAAjICQhsQCOhrDXAAQCMAACMAvQCLAwBpBYIifDlQi1iLjCAAQhPAAgtAlQgsAlgBA9QABA9A2AkIAKAGIDeAAQA9AjC3AsQDHAxBuBhQAVATASAWIAOAVQAhA1AOBGIjdAAQAJAvAAA2QAABigmBOIoYAAQgjBJhDA5QiNB0jnAAQlLAAkKj2gAzaK7IAAAAIwFAAIAAn7IE4AAIAADnIFDAAIAegkIFJAAQBsA0BjAAQBVAAAxglQALgHAHgIIDoAAIB2iSQBuBgBmAyIjaAAQgbB9hoBXQiMB0jnAAQhVAAhRgQgAACGDQAggiAAgzQAAg/g0glIgRgKIF9AAQAJAvAAA2QAAAxgJAtgAnQDAIAAmaIjeAAQgIgsAAg0QABjICPhsQCPhrDWAAQCMAACLAvQCMAwBoBYIifDlQi0iLjDAAQhNAAgtAlQguAlABA9QAAA9A3AkIAKAGICaAAIAABWIJyAAIAAEJIpyAAIAAA7gEAr8gAWQhThGgWh+IDKAAIAAEAQg4gcgpgggEgjfgDaIAAngIPvAAIAAEWIq3AAIAADKgAm37eIAAvFIhmAAIAAPFIgtAAIAA12IE5AAIAAGxICSAAIAAPFgA9g7eIox12IFRAAIFlN6IB0khIAAioIBCAAICumxIFRAAIk2MFIAAJxgEAKTgqjIAAijImMAAIAAkOIRQAAIAAEOImMAAIAACjg");
	var mask_1_graphics_69 = new cjs.Graphics().p("EgZPAxVIAAj+IqQAAIAAl8IOqAAIAABEIpyAAIAAEiILNAAIAAAWIKQAAIAAD+gEAJWAqBIAAimILHAAIAACmgEgK0AqBIAAimILHAAIAACmgEgAmAlHIAAhkILHAAIAABkgEgUwAlHIAAhkILIAAIAABkgEgtcAlHIAApoIZsAAIAACUIvvAAIAAgSIlEAAIAAEgIJxAAIAADGgAEoHVIIZAAIgBAAIC5jkQDjDGDAAAQBVAAAxglQAyglAAg/QAAg/g1glQg0gliagmQidglhmg0IAAkAIjLAAQgIgsAAg0QAAjICQhsQCOhrDXAAQCMAACMAvQCLAwBpBYIifDlQi1iLjCAAQhPAAgtAlQgsAlgBA9QABA9A2AkIAKAGIDeAAQA9AjC3AsQDHAxBuBhQAVATASAWIAOAVQAhA1AOBGIjdAAQAJAvAAA2QAABigmBOIoYAAQgjBJhDA5QiNB0jnAAQlLAAkKj2gAzaK7IAAAAIwFAAIAAn7IE4AAIAADnIFDAAIAegkIFJAAQBsA0BjAAQBVAAAxglQALgHAHgIIDoAAIB2iSQBuBgBmAyIjaAAQgbB9hoBXQiMB0jnAAQhVAAhRgQgAACGDQAggiAAgzQAAg/g0glIgRgKIF9AAQAJAvAAA2QAAAxgJAtgAnQDAIAAmaIjeAAQgIgsAAg0QABjICPhsQCPhrDWAAQCMAACLAvQCMAwBoBYIifDlQi0iLjDAAQhNAAgtAlQguAlABA9QAAA9A3AkIAKAGICaAAIAABWIJyAAIAAEJIpyAAIAAA7gEAr8gAWQhThGgWh+IDKAAIAAEAQg4gcgpgggEgjfgDaIAAngIPvAAIAAEWIq3AAIAADKgAm37eIAAvFIhmAAIAAPFIgtAAIAA12IE5AAIAAGxICSAAIAAPFgA9g7eIox12IFRAAIFlN6IB0khIAAioIBCAAICumxIFRAAIk2MFIAAJxgEAKTgqjIAAijImMAAIAAkOIRQAAIAAEOImMAAIAACjg");
	var mask_1_graphics_74 = new cjs.Graphics().p("EgQAAxVIAAj+IqQAAIAAkVIE5AAIAAD/ILNAAIAAAWIKPAAIAAD+gEASlAqBIAAg/ILIAAIAAA/gEgBkAqBIAAg/IJ8AAIAAhnIOqAAIAABEIpyAAIAAAjIjtAAIAAA/gEA1OApCIAAhnILHAAIAABngEAhEApCIAAhnILHAAIAABngEArRAlHIAAhkILIAAIAABkgEAXHAlHIAAhkILIAAIAABkgEgBkAlHIAAjoIE4AAIAAAiIJyAAIAADGgEAfzAhfIAAl8IZsAAIAACQIvwAAIAAgSIlEAAIAAD+gEgkMAbjIAAgEIZsAAIAAAEgA1WKyIAAAJIk6AAIAAilQgngegmgjIQvAAQgjBJhEA5QiMB0jnAAQhqAAhkgZgEgzgAK7IAAAAIrMAAIAAkUIE1AAIAAjnIE5AAIAADDIA7AAIAjAPIAABDIA0AAIAAAAIC5jkQDjDGC/AAQBWAAAxglQAyglAAg/QAAg/g1glQg0gliagmQidglhng0IAAg8IOSAAQAVAOARAPQAVATARAWIAPAVQAhA1AOBGIjdAAQAJAvAAA2QAABigmBOInjAAQgjBJhEA5QiNB0jmAAQhVAAhRgQgEhTHAHVIAAhDQBZAlBTAAQBVAAAxglQAKgHAIgIIDoAAIB2iSQBuBgBmAyIjaAAQgKArgSAngEg/qAGDQAggiAAgzQAAg/g1glIgRgKIF+AAQAJAvAAA2QAAAxgKAtgEhG9ADAIAAjWIOqAAIAACbIpyAAIAAA7gAzwgWQhUhGgVh+IDKAAIAAEAQg5gcgogggAXOkDQgDgaAAgdQAAjICQhsQCOhrDWAAQCNAACLAvQCMAwBpBYIifDlQi1iLjDAAQhOAAgtAlQgtAlAAA9QAAAjATAbgA8kkDIAAjDICUAAIAAj0IPwAAIAAD0InsAAIhfCJQifh6iqgPIhRAAQg2AHgjAcQgtAlAAA9QAAAjATAbgAGynGQgXgCgYAAQgSAAgQACImyAAQAihlBYhDQCOhrDWAAQCNAACLAvQCMAwBpBYIhABcgAR37eIAAmbIE3AAIAAGbgAPk7eIAAmbIAtAAIAAGbgAky7eIilmbIKoAAIAAGbgEgP1gh5IAAoqIhmAAIAAIqIgtAAIAApJIE5AAIAAAfICSAAIAAIqgEgkMgh5IAAiBIBmkBIAAioIBDAAIANgfIFRAAIiVFzIAADWgEg4igh5IjrpJIFSAAIDDHoIANggIAACBgEABVgqjIAAgfIhAAAIAthxIFSAAIguBxIAnAAIAAAfgEAjCgrCIAAhxIDDAAIBajiIqqAAIAAg/IRRAAIAAA/IhVAAIhbDiIjdAAIAABxgEAPkgrCIAAhxIE4AAIAABxgEgLCgrCIihmSIFSAAIBIC0IAABtIAsAAIAtBxgEBIEgszIAAgTImMAAIAAjPIRQAAIAADPImMAAIAAATgEA0mgszIAAjiIE5AAIAADigEAd4gszIAAhtIAsBtgEAPkgwVIAAg/IE4AAIAAA/gEACdgwVIAZg/IFSAAIgaA/g");
	var mask_1_graphics_79 = new cjs.Graphics().p("EgCzAxVIAAhaIQFAAIAABagEgkkAv7IAAikIqQAAIAAkVIE5AAIAAD/ILNAAIAAAWIKQAAIAACkgEgB+AqBIAAg/ILGAAIAAA/gEgWIAqBIAAg/IJ8AAIAAgfIE5AAIAAAfIjtAAIAAA/gEAgpApCIAAgfILIAAIAAAfgEAMgApCIAAgfIJEAAIAAhIIOrAAIAABEIpyAAIAAAEIi2AAIAAAfgEBCaAojIAAhIILIAAIAABIgEAuQAojIAAhIILIAAIAABIgA3AbjIAAgEIWEAAIAAAEgEAwqAKyIAAAJIk6AAIAAhWINpAAQiJBmjYAAQhqAAhkgZgAShK7IAAAAIrNAAIAAhWITSAAQiIBmjYAAQhVAAhQgQgEhF6AHVIAAhDQBZAlBTAAQBVAAAxglQAKgHAIgIIDoAAIB2iSQBuBgBlAyIjaAAQgJArgSAngEgyeAGDIAIgJIAAAJgEgyyADKIgRgKIAtAAIAAAkQgMgOgQgMgEg5xADAIAAjWIHbAAIAACbIiiAAIAAA7gAoDixQgGgTgDgWIDJAAIAAApgEAkagEDQgCgaAAgdQAAg7AMgyIFpAAIgGAEQgtAlAAA9QAAAjASAbgAvXkDIAAjDICUAAIAAj0IKuAAQBfgfByAAQCNAACLAvQCMAwBoBYIhUB7IkJAAQhEgZhGgGIhRAAQgxAGghAZIkSAAIhKBqQifh6irgPIhRAAQg1AHgjAcQgtAlAAA9QAAAjATAbgEAu8gGnIEHAAIhJBqQhehIhggigA3QnGQgXgCgYAAQgSAAgQACImzAAQAihlBZhDQCOhrDWAAQCMAACMAvQCMAwBpBYIhABcgEheBgifIAAjXIhmAAIAADXIgtAAIAAj2IE5AAIAAAfICRAAIAADXgEgCogjBIAAkLIE3AAIAAELgEgE7gjBIAAkLIAtAAIAAELgEgXAgjBIAAg5IBnkBIAAgLIkMAAIAAjYIE5AAIAAA7IAWAAIANgfIFQAAIiUFzIAACOgEgrygjBIjOoBIFSAAIBLC8IAdAAIBWjYIFSAAIhXDYIjdAAIAABxIhkAAIBLC7IANggIAAA5gEhM2gl2IAAgfIhAAAIAthxIFSAAIguBxIAmAAIAAAfgEg+ngmVIAAhxIE4AAIAABxgEhZOgmVIiElJIFSAAIAqBqIAABuIAsAAIAuBxgEgGHgoGIAAgTImNAAIAAjFIRQAAIAADFImMAAIAAATgEgwTgoGIAAhuIAsBugEBPEgwLIAAgKIRRAAIAAAKgEBBzgwLIAAgKIE5AAIAAAKgEA0ogwLIAEgKIqqAAIAAg/IRRAAIAAA/IhWAAIgDAKgEAAGgwLIgdhJIFRAAIAeBJgEAcxgwVIAAg/IE4AAIAAA/gEAPpgwVIAZg/IFTAAIgaA/g");

	this.timeline.addTween(cjs.Tween.get(mask_1).to({graphics:null,x:0,y:0}).wait(6).to({graphics:mask_1_graphics_6,x:46.4,y:963.175}).wait(5).to({graphics:mask_1_graphics_11,x:130.875,y:963.175}).wait(4).to({graphics:mask_1_graphics_15,x:190.05,y:963.175}).wait(6).to({graphics:mask_1_graphics_21,x:164.575,y:963.175}).wait(44).to({graphics:mask_1_graphics_65,x:190.05,y:963.175}).wait(4).to({graphics:mask_1_graphics_69,x:190.05,y:963.175}).wait(5).to({graphics:mask_1_graphics_74,x:130.875,y:963.175}).wait(5).to({graphics:mask_1_graphics_79,x:46.4,y:963.175}).wait(5).to({graphics:null,x:0,y:0}).wait(8));

	// Layer_8
	this.instance_1 = new lib.masque();
	this.instance_1.parent = this;
	this.instance_1.setTransform(353.7,938.65,2.8542,1.6381,0,0,0,0.2,265.6);
	this.instance_1.alpha = 0.8984;
	this.instance_1._off = true;

	var maskedShapeInstanceList = [this.instance_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(6).to({_off:false},0).to({_off:true},78).wait(8));

	// Layer_6
	this.instance_2 = new lib.vignette_vitesse();
	this.instance_2.parent = this;
	this.instance_2.setTransform(373.85,923,2.1186,2.1186,0,0,0,0.1,320.2);
	this.instance_2._off = true;

	var maskedShapeInstanceList = [this.instance_2];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(6).to({_off:false},0).to({x:401.2},77).to({_off:true},1).wait(8));

	// Layer_7 (mask)
	var mask_2 = new cjs.Shape();
	mask_2._off = true;
	var mask_2_graphics_0 = new cjs.Graphics().p("A4ORVMAAAgipMAwdAAAMAAAAipg");
	var mask_2_graphics_8 = new cjs.Graphics().p("EhCNAYgMAAAgw/MCEbAAAMAAAAw/g");
	var mask_2_graphics_81 = new cjs.Graphics().p("A4ORVMAAAgipMAwdAAAMAAAAipg");

	this.timeline.addTween(cjs.Tween.get(mask_2).to({graphics:mask_2_graphics_0,x:633.2,y:862.35}).wait(8).to({graphics:mask_2_graphics_8,x:484.45,y:852.8}).wait(73).to({graphics:mask_2_graphics_81,x:633.2,y:862.35}).wait(11));

	// Layer_3
	this.instance_3 = new lib.vignette_vitesse();
	this.instance_3.parent = this;
	this.instance_3.setTransform(528.4,849.45,0.875,0.875,0,0,0,0.1,320.1);

	var maskedShapeInstanceList = [this.instance_3];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_2;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({x:508.3},91).wait(1));

	// Layer_1
	this.instance_4 = new lib.vignette_vitesse();
	this.instance_4.parent = this;
	this.instance_4.setTransform(312.35,923,2.1186,2.1186,0,0,0,0.1,320.2);
	this.instance_4.alpha = 0.1094;
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off:false},0).to({x:360.2},77).to({_off:true},1).wait(8));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-404.8,244.6,1442.8,1356);


(lib.lignes_all = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{close:58});

	// timeline functions:
	this.frame_42 = function() {
		this.stop();
	}
	this.frame_119 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(42).call(this.frame_42).wait(77).call(this.frame_119).wait(3));

	// ligne
	this.instance = new lib.ligne();
	this.instance.parent = this;
	this.instance.setTransform(-454.05,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({x:-883},40,cjs.Ease.quartInOut).to({_off:true},1).wait(44).to({_off:false,regX:0.1,regY:405.9,rotation:19.9997,x:505.1,y:405.95},0).to({regX:0,regY:406,rotation:0,x:-454.05,y:406},33,cjs.Ease.quartOut).wait(4));

	// ligne
	this.instance_1 = new lib.ligne();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-227.05,406.5,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({x:-883.2},40,cjs.Ease.quartInOut).to({_off:true},1).wait(36).to({_off:false,regX:0.1,rotation:15,x:505.1},0).to({regX:0,rotation:0,x:-227.05},33,cjs.Ease.quartOut).wait(12));

	// ligne
	this.instance_2 = new lib.ligne();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-0.05,406.5,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({x:-883.4},40,cjs.Ease.quartInOut).to({_off:true},1).wait(30).to({_off:false,regX:0.1,rotation:9.9999,x:505.05},0).to({regX:0,rotation:0,x:-0.05},33,cjs.Ease.quartOut).wait(18));

	// ligne
	this.instance_3 = new lib.ligne();
	this.instance_3.parent = this;
	this.instance_3.setTransform(226.95,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({x:-883.05},40,cjs.Ease.quartInOut).to({_off:true},1).wait(24).to({_off:false,regX:0.1,rotation:5.0001,x:505.05},0).to({regX:0,rotation:0,x:226.95},33,cjs.Ease.quartOut).wait(24));

	// ligne
	this.instance_4 = new lib.ligne();
	this.instance_4.parent = this;
	this.instance_4.setTransform(454.1,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).to({x:-883.45},40,cjs.Ease.quartInOut).to({_off:true},1).wait(18).to({_off:false,x:505},0).to({x:454.1},35,cjs.Ease.quartOut).wait(28));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-884.4,-1,1528.8,814.6);


(lib.BTN_follow = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_205 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(205).call(this.frame_205).wait(1));

	// bg_btn (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("AmVC2IAAlrIAEAAIAAFrg");
	var mask_graphics_1 = new cjs.Graphics().p("AmVC2IAAlrIASAAIAAFrg");
	var mask_graphics_2 = new cjs.Graphics().p("AmVC2IAAlrIAfAAIAAFrg");
	var mask_graphics_3 = new cjs.Graphics().p("AmVC2IAAlrIAtAAIAAFrg");
	var mask_graphics_4 = new cjs.Graphics().p("AmVC2IAAlrIA7AAIAAFrg");
	var mask_graphics_5 = new cjs.Graphics().p("AmVC2IAAlrIBIAAIAAFrg");
	var mask_graphics_6 = new cjs.Graphics().p("AmVC2IAAlrIBWAAIAAFrg");
	var mask_graphics_7 = new cjs.Graphics().p("AmVC2IAAlrIBkAAIAAFrg");
	var mask_graphics_8 = new cjs.Graphics().p("AmVC2IAAlrIByAAIAAFrg");
	var mask_graphics_9 = new cjs.Graphics().p("AmVC2IAAlrIB/AAIAAFrg");
	var mask_graphics_10 = new cjs.Graphics().p("AmVC2IAAlrICNAAIAAFrg");
	var mask_graphics_11 = new cjs.Graphics().p("AmVC2IAAlrICbAAIAAFrg");
	var mask_graphics_12 = new cjs.Graphics().p("AmVC2IAAlrICpAAIAAFrg");
	var mask_graphics_13 = new cjs.Graphics().p("AmVC2IAAlrIC2AAIAAFrg");
	var mask_graphics_14 = new cjs.Graphics().p("AmVC2IAAlrIDEAAIAAFrg");
	var mask_graphics_15 = new cjs.Graphics().p("AmVC2IAAlrIDSAAIAAFrg");
	var mask_graphics_16 = new cjs.Graphics().p("AmVC2IAAlrIDfAAIAAFrg");
	var mask_graphics_17 = new cjs.Graphics().p("AmVC2IAAlrIDtAAIAAFrg");
	var mask_graphics_18 = new cjs.Graphics().p("AmVC2IAAlrID7AAIAAFrg");
	var mask_graphics_19 = new cjs.Graphics().p("AmVC2IAAlrIEJAAIAAFrg");
	var mask_graphics_20 = new cjs.Graphics().p("AmVC2IAAlrIEWAAIAAFrg");
	var mask_graphics_21 = new cjs.Graphics().p("AmVC2IAAlrIEkAAIAAFrg");
	var mask_graphics_22 = new cjs.Graphics().p("AmVC2IAAlrIEyAAIAAFrg");
	var mask_graphics_23 = new cjs.Graphics().p("AmVC2IAAlrIE/AAIAAFrg");
	var mask_graphics_24 = new cjs.Graphics().p("AmVC2IAAlrIFNAAIAAFrg");
	var mask_graphics_25 = new cjs.Graphics().p("AmVC2IAAlrIFbAAIAAFrg");
	var mask_graphics_26 = new cjs.Graphics().p("AmVC2IAAlrIFpAAIAAFrg");
	var mask_graphics_27 = new cjs.Graphics().p("AmVC2IAAlrIF2AAIAAFrg");
	var mask_graphics_28 = new cjs.Graphics().p("AmVC2IAAlrIGEAAIAAFrg");
	var mask_graphics_29 = new cjs.Graphics().p("AmVC2IAAlrIGSAAIAAFrg");
	var mask_graphics_30 = new cjs.Graphics().p("AmVC2IAAlrIGfAAIAAFrg");
	var mask_graphics_31 = new cjs.Graphics().p("AmVC2IAAlrIGsAAIAAFrg");
	var mask_graphics_32 = new cjs.Graphics().p("AmVC2IAAlrIG6AAIAAFrg");
	var mask_graphics_33 = new cjs.Graphics().p("AmVC2IAAlrIHIAAIAAFrg");
	var mask_graphics_34 = new cjs.Graphics().p("AmVC2IAAlrIHVAAIAAFrg");
	var mask_graphics_35 = new cjs.Graphics().p("AmVC2IAAlrIHjAAIAAFrg");
	var mask_graphics_36 = new cjs.Graphics().p("AmVC2IAAlrIHxAAIAAFrg");
	var mask_graphics_37 = new cjs.Graphics().p("AmVC2IAAlrIH/AAIAAFrg");
	var mask_graphics_38 = new cjs.Graphics().p("AmVC2IAAlrIIMAAIAAFrg");
	var mask_graphics_39 = new cjs.Graphics().p("AmVC2IAAlrIIaAAIAAFrg");
	var mask_graphics_40 = new cjs.Graphics().p("AmVC2IAAlrIIoAAIAAFrg");
	var mask_graphics_41 = new cjs.Graphics().p("AmVC2IAAlrII1AAIAAFrg");
	var mask_graphics_42 = new cjs.Graphics().p("AmVC2IAAlrIJDAAIAAFrg");
	var mask_graphics_43 = new cjs.Graphics().p("AmVC2IAAlrIJRAAIAAFrg");
	var mask_graphics_44 = new cjs.Graphics().p("AmVC2IAAlrIJfAAIAAFrg");
	var mask_graphics_45 = new cjs.Graphics().p("AmVC2IAAlrIJsAAIAAFrg");
	var mask_graphics_46 = new cjs.Graphics().p("AmVC2IAAlrIJ6AAIAAFrg");
	var mask_graphics_47 = new cjs.Graphics().p("AmVC2IAAlrIKIAAIAAFrg");
	var mask_graphics_48 = new cjs.Graphics().p("AmVC2IAAlrIKVAAIAAFrg");
	var mask_graphics_49 = new cjs.Graphics().p("AmVC2IAAlrIKjAAIAAFrg");
	var mask_graphics_50 = new cjs.Graphics().p("AmVC2IAAlrIKxAAIAAFrg");
	var mask_graphics_51 = new cjs.Graphics().p("AmVC2IAAlrIK/AAIAAFrg");
	var mask_graphics_52 = new cjs.Graphics().p("AmVC2IAAlrILMAAIAAFrg");
	var mask_graphics_53 = new cjs.Graphics().p("AmVC2IAAlrILaAAIAAFrg");
	var mask_graphics_54 = new cjs.Graphics().p("AmVC2IAAlrILoAAIAAFrg");
	var mask_graphics_55 = new cjs.Graphics().p("AmVC2IAAlrIL2AAIAAFrg");
	var mask_graphics_56 = new cjs.Graphics().p("AmVC2IAAlrIMDAAIAAFrg");
	var mask_graphics_57 = new cjs.Graphics().p("AmVC2IAAlrIMRAAIAAFrg");
	var mask_graphics_58 = new cjs.Graphics().p("AmVC2IAAlrIMfAAIAAFrg");
	var mask_graphics_59 = new cjs.Graphics().p("AmWC2IAAlrIMsAAIAAFrg");
	var mask_graphics_60 = new cjs.Graphics().p("AmcC2IAAlrIM5AAIAAFrg");
	var mask_graphics_61 = new cjs.Graphics().p("AmjC2IAAlrINHAAIAAFrg");
	var mask_graphics_62 = new cjs.Graphics().p("AmqC2IAAlrINVAAIAAFrg");
	var mask_graphics_63 = new cjs.Graphics().p("AmxC2IAAlrINjAAIAAFrg");
	var mask_graphics_64 = new cjs.Graphics().p("Am4C2IAAlrINxAAIAAFrg");
	var mask_graphics_65 = new cjs.Graphics().p("Am/C2IAAlrIN/AAIAAFrg");
	var mask_graphics_66 = new cjs.Graphics().p("AnGC2IAAlrIONAAIAAFrg");
	var mask_graphics_67 = new cjs.Graphics().p("AnMC2IAAlrIOZAAIAAFrg");
	var mask_graphics_68 = new cjs.Graphics().p("AnTC2IAAlrIOnAAIAAFrg");
	var mask_graphics_69 = new cjs.Graphics().p("AnaC2IAAlrIO1AAIAAFrg");
	var mask_graphics_70 = new cjs.Graphics().p("AnhC2IAAlrIPDAAIAAFrg");
	var mask_graphics_71 = new cjs.Graphics().p("AnoC2IAAlrIPRAAIAAFrg");
	var mask_graphics_72 = new cjs.Graphics().p("AnvC2IAAlrIPfAAIAAFrg");
	var mask_graphics_73 = new cjs.Graphics().p("An2C2IAAlrIPtAAIAAFrg");
	var mask_graphics_74 = new cjs.Graphics().p("An8C2IAAlrIP5AAIAAFrg");
	var mask_graphics_75 = new cjs.Graphics().p("AoDC2IAAlrIQHAAIAAFrg");
	var mask_graphics_76 = new cjs.Graphics().p("AoKC2IAAlrIQVAAIAAFrg");
	var mask_graphics_77 = new cjs.Graphics().p("AoRC2IAAlrIQjAAIAAFrg");
	var mask_graphics_78 = new cjs.Graphics().p("AoYC2IAAlrIQxAAIAAFrg");
	var mask_graphics_79 = new cjs.Graphics().p("AofC2IAAlrIQ/AAIAAFrg");
	var mask_graphics_80 = new cjs.Graphics().p("AomC2IAAlrIRNAAIAAFrg");
	var mask_graphics_81 = new cjs.Graphics().p("AosC2IAAlrIRZAAIAAFrg");
	var mask_graphics_82 = new cjs.Graphics().p("AozC2IAAlrIRnAAIAAFrg");
	var mask_graphics_83 = new cjs.Graphics().p("Ao6C2IAAlrIR1AAIAAFrg");
	var mask_graphics_84 = new cjs.Graphics().p("ApBC2IAAlrISDAAIAAFrg");
	var mask_graphics_85 = new cjs.Graphics().p("ApIC2IAAlrISRAAIAAFrg");
	var mask_graphics_86 = new cjs.Graphics().p("ApPC2IAAlrISfAAIAAFrg");
	var mask_graphics_87 = new cjs.Graphics().p("ApWC2IAAlrIStAAIAAFrg");
	var mask_graphics_88 = new cjs.Graphics().p("ApcC2IAAlrIS5AAIAAFrg");
	var mask_graphics_89 = new cjs.Graphics().p("ApjC2IAAlrITHAAIAAFrg");
	var mask_graphics_90 = new cjs.Graphics().p("ApqC2IAAlrITVAAIAAFrg");
	var mask_graphics_91 = new cjs.Graphics().p("ApxC2IAAlrITjAAIAAFrg");
	var mask_graphics_92 = new cjs.Graphics().p("Ap4C2IAAlrITxAAIAAFrg");
	var mask_graphics_93 = new cjs.Graphics().p("Ap/C2IAAlrIT/AAIAAFrg");
	var mask_graphics_94 = new cjs.Graphics().p("AqGC2IAAlrIUNAAIAAFrg");
	var mask_graphics_95 = new cjs.Graphics().p("AqMC2IAAlrIUZAAIAAFrg");
	var mask_graphics_96 = new cjs.Graphics().p("AqTC2IAAlrIUnAAIAAFrg");
	var mask_graphics_97 = new cjs.Graphics().p("AqaC2IAAlrIU1AAIAAFrg");
	var mask_graphics_98 = new cjs.Graphics().p("AqhC2IAAlrIVDAAIAAFrg");
	var mask_graphics_99 = new cjs.Graphics().p("AqoC2IAAlrIVRAAIAAFrg");
	var mask_graphics_100 = new cjs.Graphics().p("AqvC2IAAlrIVfAAIAAFrg");
	var mask_graphics_101 = new cjs.Graphics().p("Aq2C2IAAlrIVtAAIAAFrg");
	var mask_graphics_102 = new cjs.Graphics().p("Aq8C2IAAlrIV5AAIAAFrg");
	var mask_graphics_103 = new cjs.Graphics().p("ArDC2IAAlrIWHAAIAAFrg");
	var mask_graphics_104 = new cjs.Graphics().p("ArKC2IAAlrIWVAAIAAFrg");
	var mask_graphics_105 = new cjs.Graphics().p("ArRC2IAAlrIWjAAIAAFrg");
	var mask_graphics_106 = new cjs.Graphics().p("ArYC2IAAlrIWxAAIAAFrg");
	var mask_graphics_107 = new cjs.Graphics().p("ArfC2IAAlrIW/AAIAAFrg");
	var mask_graphics_108 = new cjs.Graphics().p("ArmC2IAAlrIXNAAIAAFrg");
	var mask_graphics_109 = new cjs.Graphics().p("ArtC2IAAlrIXbAAIAAFrg");
	var mask_graphics_110 = new cjs.Graphics().p("ArzC2IAAlrIXnAAIAAFrg");
	var mask_graphics_111 = new cjs.Graphics().p("Ar6C2IAAlrIX1AAIAAFrg");
	var mask_graphics_112 = new cjs.Graphics().p("AsBC2IAAlrIYDAAIAAFrg");
	var mask_graphics_113 = new cjs.Graphics().p("AsIC2IAAlrIYRAAIAAFrg");
	var mask_graphics_114 = new cjs.Graphics().p("AsPC2IAAlrIYfAAIAAFrg");
	var mask_graphics_115 = new cjs.Graphics().p("AsWC2IAAlrIYtAAIAAFrg");
	var mask_graphics_116 = new cjs.Graphics().p("AsdC2IAAlrIY7AAIAAFrg");
	var mask_graphics_117 = new cjs.Graphics().p("AsjC2IAAlrIZHAAIAAFrg");
	var mask_graphics_118 = new cjs.Graphics().p("AsqC2IAAlrIZVAAIAAFrg");
	var mask_graphics_119 = new cjs.Graphics().p("AsqCtIAAlHIZVAAIAAFHg");
	var mask_graphics_120 = new cjs.Graphics().p("AsqCkIAAkjIZVAAIAAEjg");
	var mask_graphics_121 = new cjs.Graphics().p("AsqCbIAAj/IZVAAIAAD/g");
	var mask_graphics_122 = new cjs.Graphics().p("AsqCSIAAjbIZVAAIAADbg");
	var mask_graphics_123 = new cjs.Graphics().p("AsqCJIAAi3IZVAAIAAC3g");
	var mask_graphics_124 = new cjs.Graphics().p("AsqCAIAAiTIZVAAIAACTg");
	var mask_graphics_125 = new cjs.Graphics().p("AsqB3IAAhwIZVAAIAABwg");
	var mask_graphics_126 = new cjs.Graphics().p("AsqBuIAAhMIZVAAIAABMg");
	var mask_graphics_127 = new cjs.Graphics().p("AsqBlIAAgoIZVAAIAAAog");
	var mask_graphics_128 = new cjs.Graphics().p("AsqBcIAAgEIZVAAIAAAEg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:-40.5622,y:18.2}).wait(1).to({graphics:mask_graphics_1,x:-40.5621,y:18.2}).wait(1).to({graphics:mask_graphics_2,x:-40.562,y:18.2}).wait(1).to({graphics:mask_graphics_3,x:-40.5619,y:18.2}).wait(1).to({graphics:mask_graphics_4,x:-40.5618,y:18.2}).wait(1).to({graphics:mask_graphics_5,x:-40.5617,y:18.2}).wait(1).to({graphics:mask_graphics_6,x:-40.5616,y:18.2}).wait(1).to({graphics:mask_graphics_7,x:-40.5615,y:18.2}).wait(1).to({graphics:mask_graphics_8,x:-40.5614,y:18.2}).wait(1).to({graphics:mask_graphics_9,x:-40.5613,y:18.2}).wait(1).to({graphics:mask_graphics_10,x:-40.5612,y:18.2}).wait(1).to({graphics:mask_graphics_11,x:-40.5611,y:18.2}).wait(1).to({graphics:mask_graphics_12,x:-40.561,y:18.2}).wait(1).to({graphics:mask_graphics_13,x:-40.5609,y:18.2}).wait(1).to({graphics:mask_graphics_14,x:-40.5608,y:18.2}).wait(1).to({graphics:mask_graphics_15,x:-40.5607,y:18.2}).wait(1).to({graphics:mask_graphics_16,x:-40.5606,y:18.2}).wait(1).to({graphics:mask_graphics_17,x:-40.5605,y:18.2}).wait(1).to({graphics:mask_graphics_18,x:-40.5604,y:18.2}).wait(1).to({graphics:mask_graphics_19,x:-40.5603,y:18.2}).wait(1).to({graphics:mask_graphics_20,x:-40.5602,y:18.2}).wait(1).to({graphics:mask_graphics_21,x:-40.5601,y:18.2}).wait(1).to({graphics:mask_graphics_22,x:-40.56,y:18.2}).wait(1).to({graphics:mask_graphics_23,x:-40.5599,y:18.2}).wait(1).to({graphics:mask_graphics_24,x:-40.5598,y:18.2}).wait(1).to({graphics:mask_graphics_25,x:-40.5596,y:18.2}).wait(1).to({graphics:mask_graphics_26,x:-40.5595,y:18.2}).wait(1).to({graphics:mask_graphics_27,x:-40.5594,y:18.2}).wait(1).to({graphics:mask_graphics_28,x:-40.5593,y:18.2}).wait(1).to({graphics:mask_graphics_29,x:-40.5592,y:18.2}).wait(1).to({graphics:mask_graphics_30,x:-40.5591,y:18.2}).wait(1).to({graphics:mask_graphics_31,x:-40.559,y:18.2}).wait(1).to({graphics:mask_graphics_32,x:-40.5589,y:18.2}).wait(1).to({graphics:mask_graphics_33,x:-40.5588,y:18.2}).wait(1).to({graphics:mask_graphics_34,x:-40.5587,y:18.2}).wait(1).to({graphics:mask_graphics_35,x:-40.5586,y:18.2}).wait(1).to({graphics:mask_graphics_36,x:-40.5585,y:18.2}).wait(1).to({graphics:mask_graphics_37,x:-40.5584,y:18.2}).wait(1).to({graphics:mask_graphics_38,x:-40.5583,y:18.2}).wait(1).to({graphics:mask_graphics_39,x:-40.5582,y:18.2}).wait(1).to({graphics:mask_graphics_40,x:-40.5581,y:18.2}).wait(1).to({graphics:mask_graphics_41,x:-40.558,y:18.2}).wait(1).to({graphics:mask_graphics_42,x:-40.5579,y:18.2}).wait(1).to({graphics:mask_graphics_43,x:-40.5578,y:18.2}).wait(1).to({graphics:mask_graphics_44,x:-40.5577,y:18.2}).wait(1).to({graphics:mask_graphics_45,x:-40.5576,y:18.2}).wait(1).to({graphics:mask_graphics_46,x:-40.5575,y:18.2}).wait(1).to({graphics:mask_graphics_47,x:-40.5574,y:18.2}).wait(1).to({graphics:mask_graphics_48,x:-40.5573,y:18.2}).wait(1).to({graphics:mask_graphics_49,x:-40.5572,y:18.2}).wait(1).to({graphics:mask_graphics_50,x:-40.5571,y:18.2}).wait(1).to({graphics:mask_graphics_51,x:-40.557,y:18.2}).wait(1).to({graphics:mask_graphics_52,x:-40.5568,y:18.2}).wait(1).to({graphics:mask_graphics_53,x:-40.5567,y:18.2}).wait(1).to({graphics:mask_graphics_54,x:-40.5566,y:18.2}).wait(1).to({graphics:mask_graphics_55,x:-40.5565,y:18.2}).wait(1).to({graphics:mask_graphics_56,x:-40.5564,y:18.2}).wait(1).to({graphics:mask_graphics_57,x:-40.5563,y:18.2}).wait(1).to({graphics:mask_graphics_58,x:-40.5562,y:18.2}).wait(1).to({graphics:mask_graphics_59,x:-40.4625,y:18.2}).wait(1).to({graphics:mask_graphics_60,x:-39.7762,y:18.2}).wait(1).to({graphics:mask_graphics_61,x:-39.09,y:18.2}).wait(1).to({graphics:mask_graphics_62,x:-38.4038,y:18.2}).wait(1).to({graphics:mask_graphics_63,x:-37.7176,y:18.2}).wait(1).to({graphics:mask_graphics_64,x:-37.0313,y:18.2}).wait(1).to({graphics:mask_graphics_65,x:-36.3451,y:18.2}).wait(1).to({graphics:mask_graphics_66,x:-35.6589,y:18.2}).wait(1).to({graphics:mask_graphics_67,x:-34.9726,y:18.2}).wait(1).to({graphics:mask_graphics_68,x:-34.2864,y:18.2}).wait(1).to({graphics:mask_graphics_69,x:-33.6002,y:18.2}).wait(1).to({graphics:mask_graphics_70,x:-32.914,y:18.2}).wait(1).to({graphics:mask_graphics_71,x:-32.2277,y:18.2}).wait(1).to({graphics:mask_graphics_72,x:-31.5415,y:18.2}).wait(1).to({graphics:mask_graphics_73,x:-30.8553,y:18.2}).wait(1).to({graphics:mask_graphics_74,x:-30.169,y:18.2}).wait(1).to({graphics:mask_graphics_75,x:-29.4828,y:18.2}).wait(1).to({graphics:mask_graphics_76,x:-28.7966,y:18.2}).wait(1).to({graphics:mask_graphics_77,x:-28.1104,y:18.2}).wait(1).to({graphics:mask_graphics_78,x:-27.4241,y:18.2}).wait(1).to({graphics:mask_graphics_79,x:-26.7379,y:18.2}).wait(1).to({graphics:mask_graphics_80,x:-26.0517,y:18.2}).wait(1).to({graphics:mask_graphics_81,x:-25.3654,y:18.2}).wait(1).to({graphics:mask_graphics_82,x:-24.6792,y:18.2}).wait(1).to({graphics:mask_graphics_83,x:-23.993,y:18.2}).wait(1).to({graphics:mask_graphics_84,x:-23.3068,y:18.2}).wait(1).to({graphics:mask_graphics_85,x:-22.6205,y:18.2}).wait(1).to({graphics:mask_graphics_86,x:-21.9343,y:18.2}).wait(1).to({graphics:mask_graphics_87,x:-21.2481,y:18.2}).wait(1).to({graphics:mask_graphics_88,x:-20.5619,y:18.2}).wait(1).to({graphics:mask_graphics_89,x:-19.8756,y:18.2}).wait(1).to({graphics:mask_graphics_90,x:-19.1894,y:18.2}).wait(1).to({graphics:mask_graphics_91,x:-18.5032,y:18.2}).wait(1).to({graphics:mask_graphics_92,x:-17.8169,y:18.2}).wait(1).to({graphics:mask_graphics_93,x:-17.1307,y:18.2}).wait(1).to({graphics:mask_graphics_94,x:-16.4445,y:18.2}).wait(1).to({graphics:mask_graphics_95,x:-15.7583,y:18.2}).wait(1).to({graphics:mask_graphics_96,x:-15.072,y:18.2}).wait(1).to({graphics:mask_graphics_97,x:-14.3858,y:18.2}).wait(1).to({graphics:mask_graphics_98,x:-13.6996,y:18.2}).wait(1).to({graphics:mask_graphics_99,x:-13.0133,y:18.2}).wait(1).to({graphics:mask_graphics_100,x:-12.3271,y:18.2}).wait(1).to({graphics:mask_graphics_101,x:-11.6409,y:18.2}).wait(1).to({graphics:mask_graphics_102,x:-10.9547,y:18.2}).wait(1).to({graphics:mask_graphics_103,x:-10.2684,y:18.2}).wait(1).to({graphics:mask_graphics_104,x:-9.5822,y:18.2}).wait(1).to({graphics:mask_graphics_105,x:-8.896,y:18.2}).wait(1).to({graphics:mask_graphics_106,x:-8.2097,y:18.2}).wait(1).to({graphics:mask_graphics_107,x:-7.5235,y:18.2}).wait(1).to({graphics:mask_graphics_108,x:-6.8373,y:18.2}).wait(1).to({graphics:mask_graphics_109,x:-6.1511,y:18.2}).wait(1).to({graphics:mask_graphics_110,x:-5.4648,y:18.2}).wait(1).to({graphics:mask_graphics_111,x:-4.7786,y:18.2}).wait(1).to({graphics:mask_graphics_112,x:-4.0924,y:18.2}).wait(1).to({graphics:mask_graphics_113,x:-3.4061,y:18.2}).wait(1).to({graphics:mask_graphics_114,x:-2.7199,y:18.2}).wait(1).to({graphics:mask_graphics_115,x:-2.0337,y:18.2}).wait(1).to({graphics:mask_graphics_116,x:-1.3475,y:18.2}).wait(1).to({graphics:mask_graphics_117,x:-0.6612,y:18.2}).wait(1).to({graphics:mask_graphics_118,x:0.025,y:18.2}).wait(1).to({graphics:mask_graphics_119,x:0.025,y:17.3026}).wait(1).to({graphics:mask_graphics_120,x:0.025,y:16.4051}).wait(1).to({graphics:mask_graphics_121,x:0.025,y:15.5077}).wait(1).to({graphics:mask_graphics_122,x:0.025,y:14.6102}).wait(1).to({graphics:mask_graphics_123,x:0.025,y:13.7128}).wait(1).to({graphics:mask_graphics_124,x:0.025,y:12.8153}).wait(1).to({graphics:mask_graphics_125,x:0.025,y:11.9179}).wait(1).to({graphics:mask_graphics_126,x:0.025,y:11.0204}).wait(1).to({graphics:mask_graphics_127,x:0.025,y:10.123}).wait(1).to({graphics:mask_graphics_128,x:0.025,y:9.2261}).wait(78));

	// JE FOLLOW DIRECT
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgHAkIAAg5IgVAAIAAgOIA5AAIAAAOIgVAAIAAA5g");
	this.shape.setTransform(50.025,17.925);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgWAbQgLgLAAgQQAAgPALgLQALgLAQAAQARAAAMAOIgKALQgIgJgLAAQgIAAgHAGQgGAGAAAJQAAAKAGAGQAGAGAIAAQAMAAAIgJIAKALQgNAOgQAAQgQAAgLgLg");
	this.shape_1.setTransform(43.1,17.875);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgaAkIAAhHIA0AAIAAAOIgkAAIAAAPIAgAAIAAANIggAAIAAAPIAlAAIAAAOg");
	this.shape_2.setTransform(36.025,17.925);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAMAkIgPgXIgMAAIAAAXIgQAAIAAhHIAcAAQAQAAAHAFQAHAGAAANQAAAQgOAFIATAagAgPAAIAMAAQAIAAADgDQADgCAAgGQAAgGgEgCQgDgCgGAAIgNAAg");
	this.shape_3.setTransform(28.975,17.925);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgHAkIAAhHIAPAAIAABHg");
	this.shape_4.setTransform(23.2,17.925);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AggAkIAAhHIAZAAQATAAALAJQAKAKAAAQQAAAQgKAKQgLAKgTAAgAgQAWIAKAAQALAAAFgGQAHgFAAgLQAAgKgHgGQgFgFgMAAIgJAAg");
	this.shape_5.setTransform(17.7,17.925);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAQAkIgQgxIgPAxIgMAAIgZhHIASAAIANAoIAOgoIAQAAIAMAoIAPgoIASAAIgaBHg");
	this.shape_6.setTransform(5.5,17.925);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgaAbQgLgLAAgQQAAgPALgKQALgLAPAAQAQAAALALQALAKAAAPQAAAQgLALQgLAKgQABQgPgBgLgKgAgPgQQgFAIAAAIQAAAKAFAHQAHAHAIgBQAJABAHgHQAFgHAAgKQAAgIgFgIQgHgGgJAAQgIAAgHAGg");
	this.shape_7.setTransform(-4.1,17.85);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgXAkIAAhHIARAAIAAA5IAdAAIAAAOg");
	this.shape_8.setTransform(-11.05,17.925);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgWAkIAAhHIAPAAIAAA5IAfAAIAAAOg");
	this.shape_9.setTransform(-16.95,17.925);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgaAbQgLgLAAgQQAAgPALgKQALgLAPAAQAQAAALALQALAKAAAPQAAAQgLALQgLAKgQABQgPgBgLgKgAgOgQQgHAIABAIQgBAKAHAHQAGAHAIgBQAJABAGgHQAGgHABgKQgBgIgGgIQgGgGgJAAQgIAAgGAGg");
	this.shape_10.setTransform(-24.5,17.85);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgYAkIAAhHIAxAAIAAAOIghAAIAAAPIAfAAIAAAOIgfAAIAAAcg");
	this.shape_11.setTransform(-31.75,17.925);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgaAkIAAhHIA0AAIAAAOIgkAAIAAAPIAgAAIAAANIggAAIAAAPIAlAAIAAAOg");
	this.shape_12.setTransform(-41.125,17.925);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgXAbIAJgMQAHAHAHAAQACAAACgDQADgCAAgGIAAghIgVAAIAAgNIAlAAIAAAtQABANgHAHQgHAGgKABQgMAAgLgKg");
	this.shape_13.setTransform(-47.85,17.95);

	var maskedShapeInstanceList = [this.shape,this.shape_1,this.shape_2,this.shape_3,this.shape_4,this.shape_5,this.shape_6,this.shape_7,this.shape_8,this.shape_9,this.shape_10,this.shape_11,this.shape_12,this.shape_13];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},118).to({state:[]},21).wait(67));

	// bg_btn
	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#CC9933").ss(1,1,1).p("Asqi1IZVAAIAAFrI5VAAg");
	this.shape_14.setTransform(0.025,18.2);

	this.timeline.addTween(cjs.Tween.get(this.shape_14).to({_off:true},119).wait(87));

	// bg_btn
	this.instance = new lib.bg_btn();
	this.instance.parent = this;
	this.instance.setTransform(-80.95,18.2,0.0022,1,0,0,0,0,18.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1,x:0},118).to({regY:18.1,scaleY:0.011,y:18.25},10,cjs.Ease.quartOut).to({_off:true},11).wait(67));

	// JE FOLLOW DIRECT
	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#CC9933").s().p("AgHAkIAAg5IgVAAIAAgOIA5AAIAAAOIgVAAIAAA5g");
	this.shape_15.setTransform(50.025,17.925);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#CC9933").s().p("AgWAbQgLgLAAgQQAAgPALgLQALgLAQAAQARAAAMAOIgKALQgIgJgLAAQgIAAgHAGQgGAGAAAJQAAAKAGAGQAGAGAIAAQAMAAAIgJIAKALQgNAOgQAAQgQAAgLgLg");
	this.shape_16.setTransform(43.1,17.875);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#CC9933").s().p("AgaAkIAAhHIA0AAIAAAOIgkAAIAAAPIAgAAIAAANIggAAIAAAPIAlAAIAAAOg");
	this.shape_17.setTransform(36.025,17.925);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#CC9933").s().p("AAMAkIgPgXIgMAAIAAAXIgQAAIAAhHIAcAAQAQAAAHAFQAHAGAAANQAAAQgOAFIATAagAgPAAIAMAAQAIAAADgDQADgCAAgGQAAgGgEgCQgDgCgGAAIgNAAg");
	this.shape_18.setTransform(28.975,17.925);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#CC9933").s().p("AgHAkIAAhHIAPAAIAABHg");
	this.shape_19.setTransform(23.2,17.925);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#CC9933").s().p("AggAkIAAhHIAZAAQATAAALAJQAKAKAAAQQAAAQgKAKQgLAKgTAAgAgQAWIAKAAQALAAAFgGQAHgFAAgLQAAgKgHgGQgFgFgMAAIgJAAg");
	this.shape_20.setTransform(17.7,17.925);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#CC9933").s().p("AAQAkIgQgxIgPAxIgMAAIgZhHIASAAIANAoIAOgoIAQAAIAMAoIAPgoIASAAIgaBHg");
	this.shape_21.setTransform(5.5,17.925);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#CC9933").s().p("AgaAbQgLgLAAgQQAAgPALgKQALgLAPAAQAQAAALALQALAKAAAPQAAAQgLALQgLAKgQABQgPgBgLgKgAgPgQQgFAIAAAIQAAAKAFAHQAHAHAIgBQAJABAHgHQAFgHAAgKQAAgIgFgIQgHgGgJAAQgIAAgHAGg");
	this.shape_22.setTransform(-4.1,17.85);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#CC9933").s().p("AgXAkIAAhHIARAAIAAA5IAdAAIAAAOg");
	this.shape_23.setTransform(-11.05,17.925);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#CC9933").s().p("AgWAkIAAhHIAPAAIAAA5IAfAAIAAAOg");
	this.shape_24.setTransform(-16.95,17.925);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#CC9933").s().p("AgaAbQgLgLAAgQQAAgPALgKQALgLAPAAQAQAAALALQALAKAAAPQAAAQgLALQgLAKgQABQgPgBgLgKgAgOgQQgHAIABAIQgBAKAHAHQAGAHAIgBQAJABAGgHQAGgHABgKQgBgIgGgIQgGgGgJAAQgIAAgGAGg");
	this.shape_25.setTransform(-24.5,17.85);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#CC9933").s().p("AgYAkIAAhHIAxAAIAAAOIghAAIAAAPIAfAAIAAAOIgfAAIAAAcg");
	this.shape_26.setTransform(-31.75,17.925);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#CC9933").s().p("AgaAkIAAhHIA0AAIAAAOIgkAAIAAAPIAgAAIAAANIggAAIAAAPIAlAAIAAAOg");
	this.shape_27.setTransform(-41.125,17.925);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#CC9933").s().p("AgXAbIAJgMQAHAHAHAAQACAAACgDQADgCAAgGIAAghIgVAAIAAgNIAlAAIAAAtQABANgHAHQgHAGgKABQgMAAgLgKg");
	this.shape_28.setTransform(-47.85,17.95);

	this.instance_1 = new lib.Tween1("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-1.8,17.85);
	this.instance_1.alpha = 0.5;
	this.instance_1._off = true;

	this.instance_2 = new lib.Tween2("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-1.8,17.85);
	this.instance_2.alpha = 0;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15}]}).to({state:[{t:this.instance_1}]},118).to({state:[{t:this.instance_1}]},30).to({state:[{t:this.instance_2}]},56).to({state:[]},1).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(118).to({_off:false},0).to({alpha:1},30).to({_off:true,alpha:0},56).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-82.1,-1,164.3,38.4);


// stage content:
(lib.FS_interlude_instagram = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.lignes_mc.gotoAndPlay(1);
	}
	this.frame_339 = function() {
		this.lignes_mc.gotoAndPlay("close");
	}
	this.frame_439 = function() {
		var event = new Event('next');
		this.dispatchEvent(event);		
		event = null;
	}
	this.frame_499 = function() {
		// EVENT DE FIN D'ANIM
		var event = new Event('closed');
		this.dispatchEvent(event);		
		event = null;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(339).call(this.frame_339).wait(100).call(this.frame_439).wait(60).call(this.frame_499).wait(1));

	// Layer_15
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AmTH9QifiaAAkbIAArfIElAAIAALVQAACeBKBbQBJBcB7AAQB8AABIhcQBJhbAAieIAArVIElAAIAALfQAAEeieCYQieCaj2AAQj1AAifibg");
	this.shape.setTransform(686.825,597.975);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AoJKRIAA0hIHRAAQEuAACJB1QCLB2AAD0QAADyiOBxQiOBxkjAAIiuAAIAAFugAjjAlIDFAAQCRAAA1g6QA1g8AAhxQAAhyhFgwQhEgwiQAAIinAAg");
	this.shape_1.setTransform(641.875,364.225);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AoLKRIAA0hIH/AAQCFAABgAgQBhAgAxA2QBYBmAACAQAACahjBLQgiAbgNAGIgvAWQB5AZBJBUQBIBUAAB8QAACJhdBpQhtB6kPAAgAjmGZIDJAAQB9AABAgfQA9gfABhbQAAhchEgcQhCgdiRAAIitAAgAjmh8ICLAAQB5AAA8gaQA6gaABhYQAAhXg3gcQg2gciBAAIiNAAg");
	this.shape_2.setTransform(846.05,364.225);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AlOB9IAAj5IKdAAIAAD5g");
	this.shape_3.setTransform(731.375,373.475);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AoLKRIAA0hIH/AAQCFAABgAgQBiAgAwA2QBYBmAACAQAACahjBLQgiAbgNAGIgwAWQB6AZBJBUQBIBUAAB8QAACJhdBpQhtB6kPAAgAjmGZIDJAAQB9AABAgfQA9gfAAhbQAAhchDgcQhBgdiSAAIitAAgAjmh8ICLAAQB5AAA8gaQA6gaAAhYQAAhXg2gcQg2gciBAAIiNAAg");
	this.shape_4.setTransform(910.2,364.225);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AhpE8IANieIiGBcIhgiqICehPIighOIBiioICGBYIgNieIDTAAIgNCeICGhYIBiCoIigBOICeBPIhgCqIiGhcIANCeg");
	this.shape_5.setTransform(657.5,330.125);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AlOB9IAAj5IKdAAIAAD5g");
	this.shape_6.setTransform(349.175,373.475);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AoLKRIAA0hIH/AAQCFAABgAgQBhAgAxA2QBYBmAACAQAACahjBLQgiAbgNAGIgvAWQB5AZBJBUQBIBUAAB8QAACJhdBpQhtB6kPAAgAjmGZIDJAAQB9AABAgfQA9gfABhbQAAhchEgcQhCgdiRAAIitAAgAjmh8ICLAAQB5AAA8gaQA6gaAAhYQAAhXg2gcQg2gciBAAIiNAAg");
	this.shape_7.setTransform(476.05,364.225);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1,p:{x:641.875}},{t:this.shape,p:{x:686.825,y:597.975}}]}).to({state:[{t:this.shape_1,p:{x:435.575}},{t:this.shape,p:{x:565.125,y:364.875}},{t:this.shape_3,p:{x:731.375}},{t:this.shape_2}]},7).to({state:[{t:this.shape_6},{t:this.shape_3,p:{x:441.275}},{t:this.shape_1,p:{x:555.725}},{t:this.shape_5},{t:this.shape,p:{x:770.975,y:364.875}},{t:this.shape_4}]},7).to({state:[{t:this.shape_1,p:{x:207.325}},{t:this.shape,p:{x:336.825,y:364.875}},{t:this.shape_7}]},9).to({state:[]},9).to({state:[]},467).wait(1));

	// Layer_12
	this.instance = new lib.BTN_follow();
	this.instance.parent = this;
	this.instance.setTransform(640.1,454.05,1.15,1.15,0,0,0,0.1,18.2);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(260).to({_off:false},0).to({_off:true},239).wait(1));

	// Layer_11
	this.instance_1 = new lib.txt_surInstagram();
	this.instance_1.parent = this;
	this.instance_1.setTransform(707.5,383.15,0.12,0.12);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(236).to({_off:false},0).to({_off:true},263).wait(1));

	// Layer_4
	this.instance_2 = new lib.txt_etbienplus();
	this.instance_2.parent = this;
	this.instance_2.setTransform(498.3,343,0.1,0.1,0,0,0,-311.6,-38.5);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(224).to({_off:false},0).to({_off:true},275).wait(1));

	// Layer_3
	this.instance_3 = new lib.vignette3_danger();
	this.instance_3.parent = this;
	this.instance_3.setTransform(266.15,-254.3,1,1,0,0,0,0.5,320.4);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(161).to({_off:false},0).to({_off:true},73).wait(266));

	// Layer_2
	this.instance_4 = new lib.vignette2_action();
	this.instance_4.parent = this;
	this.instance_4.setTransform(266.15,-254.3,1,1,0,0,0,0.5,320.4);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(90).to({_off:false},0).to({_off:true},76).wait(334));

	// Layer_1
	this.instance_5 = new lib.vignette1_vitesse();
	this.instance_5.parent = this;
	this.instance_5.setTransform(266.15,-254.3,1,1,0,0,0,0.5,320.4);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(21).to({_off:false},0).to({_off:true},96).wait(383));

	// Ligne
	this.lignes_mc = new lib.lignes_all();
	this.lignes_mc.name = "lignes_mc";
	this.lignes_mc.parent = this;
	this.lignes_mc.setTransform(640,384.3,1,1,0,0,0,0,406.3);

	this.timeline.addTween(cjs.Tween.get(this.lignes_mc).to({_off:true},499).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,1098.6,791);
// library properties:
lib.properties = {
	id: '8FC2756ADD874D4CB70E4CC078A82AAA',
	width: 1280,
	height: 768,
	fps: 60,
	color: "#000000",
	opacity: 1.00,
	manifest: [
		{src:"images/action.jpg?1540365672414", id:"action"},
		{src:"images/danger.jpg?1540365672414", id:"danger"},
		{src:"images/fire_0000_Layer8.png?1540365672414", id:"fire_0000_Layer8"},
		{src:"images/fire_0001_Layer9.png?1540365672414", id:"fire_0001_Layer9"},
		{src:"images/fire_0002_Layer10.png?1540365672414", id:"fire_0002_Layer10"},
		{src:"images/fire_0003_Layer11.png?1540365672414", id:"fire_0003_Layer11"},
		{src:"images/fire_0004_Layer12.png?1540365672414", id:"fire_0004_Layer12"},
		{src:"images/fire_0005_Layer13.png?1540365672414", id:"fire_0005_Layer13"},
		{src:"images/fire_0006_Layer14.png?1540365672414", id:"fire_0006_Layer14"},
		{src:"images/fire_0007_Layer15.png?1540365672414", id:"fire_0007_Layer15"},
		{src:"images/fire_0008_Layer16.png?1540365672414", id:"fire_0008_Layer16"},
		{src:"images/fire_0009_Layer17.png?1540365672414", id:"fire_0009_Layer17"},
		{src:"images/fire_0010_Layer1.png?1540365672414", id:"fire_0010_Layer1"},
		{src:"images/fire_0011_Layer2.png?1540365672414", id:"fire_0011_Layer2"},
		{src:"images/fire_0012_Layer3.png?1540365672414", id:"fire_0012_Layer3"},
		{src:"images/fire_0013_Layer4.png?1540365672414", id:"fire_0013_Layer4"},
		{src:"images/fire_0014_Layer5.png?1540365672414", id:"fire_0014_Layer5"},
		{src:"images/fire_0015_Layer6.png?1540365672414", id:"fire_0015_Layer6"},
		{src:"images/fire_0016_Layer7.png?1540365672414", id:"fire_0016_Layer7"},
		{src:"images/vitesse.jpg?1540365672414", id:"vitesse"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['8FC2756ADD874D4CB70E4CC078A82AAA'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;