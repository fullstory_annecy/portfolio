(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.video = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#333333").s().p("EiV/BUYMAAAiovMEr/AAAMAAACovg");
	this.shape.setTransform(960,540);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.video, new cjs.Rectangle(0,0,1920,1080), null);


(lib.title_txt = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.text = new cjs.Text("MOTION DESIGN REALISE POUR DEGRISOGONO", "bold 15px 'Montserrat'", "#FFFFFF");
	this.text.name = "text";
	this.text.textAlign = "center";
	this.text.lineHeight = 22;
	this.text.lineWidth = 416;
	this.text.parent = this;
	this.text.setTransform(210.1,2);

	this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

}).prototype = getMCSymbolPrototype(lib.title_txt, new cjs.Rectangle(0,0,420.2,54), null);


(lib.playArrow = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(2,1,1).p("AiOiMIAAEZIEdiIg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AiOiMIEdCRIkdCIg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.playArrow, new cjs.Rectangle(-15.3,-15.1,30.7,30.2), null);


(lib.mask5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("Egx/A8AMAAAh3/MBj/AAAMAAAB3/g");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.mask5, new cjs.Rectangle(-320,-384,640,768), null);


(lib.mask4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("Egx/A8AMAAAh3/MBj/AAAMAAAB3/g");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.mask4, new cjs.Rectangle(-320,-384,640,768), null);


(lib.mask3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("Egx/A8AMAAAh3/MBj/AAAMAAAB3/g");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.mask3, new cjs.Rectangle(-320,-384,640,768), null);


(lib.mask2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("Egx/A8AMAAAh3/MBj/AAAMAAAB3/g");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.mask2, new cjs.Rectangle(-320,-384,640,768), null);


(lib.mask1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("Egx/A8AMAAAh3/MBj/AAAMAAAB3/g");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.mask1, new cjs.Rectangle(-320,-384,640,768), null);


(lib.mask0 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("Egx/A8AMAAAh3/MBj/AAAMAAAB3/g");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.mask0, new cjs.Rectangle(-320,-384,640,768), null);


(lib.line_explode = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_15 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(15).call(this.frame_15).wait(1));

	// Calque_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(1,1,1).p("AgHAAIAPAA");
	this.shape.setTransform(0.8,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#FFFFFF").ss(1,1,1).p("AgRAAIAjAA");
	this.shape_1.setTransform(2.9,0);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#FFFFFF").ss(1,1,1).p("AgaAAIA2AA");
	this.shape_2.setTransform(5.1,0);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#FFFFFF").ss(1,1,1).p("AglAAIBLAA");
	this.shape_3.setTransform(7.3,0);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#FFFFFF").ss(1,1,1).p("AgnAAIBPAA");
	this.shape_4.setTransform(8.8,0);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#FFFFFF").ss(1,1,1).p("AgpAAIBTAA");
	this.shape_5.setTransform(10.4,0);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#FFFFFF").ss(1,1,1).p("AgrAAIBXAA");
	this.shape_6.setTransform(12,0);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#FFFFFF").ss(1,1,1).p("AgtAAIBbAA");
	this.shape_7.setTransform(13.6,0);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#FFFFFF").ss(1,1,1).p("AgvAAIBfAA");
	this.shape_8.setTransform(15.1,0);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#FFFFFF").ss(1,1,1).p("AgxAAIBjAA");
	this.shape_9.setTransform(16.7,0);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#FFFFFF").ss(1,1,1).p("Ag0AAIBpAA");
	this.shape_10.setTransform(18.3,0);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#FFFFFF").ss(1,1,1).p("AgWAAIAtAA");
	this.shape_11.setTransform(25.8,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape,p:{x:0.8}}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3,p:{x:7.3}}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_3,p:{x:22}}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape,p:{x:29.5}}]},1).to({state:[]},1).to({state:[]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1,-1,13,2);


(lib.ligne = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#333333").ss(2,1,1).p("EgABhM/MAADCZ/");
	this.shape.setTransform(-0.2,319.3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.ligne, new cjs.Rectangle(-1.3,-174.5,2.4,987.5), null);


(lib.Btn_cliquable_area = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CC9933").s().p("Ehj/AEsIAApXMDH/AAAIAAJXg");
	this.shape.setTransform(640,30);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Btn_cliquable_area, new cjs.Rectangle(0,0,1280,60), null);


(lib.btn_playVideo = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{open:1,close:97});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_96 = function() {
		this.stop();
	}
	this.frame_149 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(96).call(this.frame_96).wait(53).call(this.frame_149).wait(1));

	// ZONE
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.008)").s().p("AsPMPIAA4dIYeAAIAAYdg");
	this.shape.setTransform(0.1,0);

	this.timeline.addTween(cjs.Tween.get(this.shape).to({_off:true},149).wait(1));

	// Calque_16
	this.instance = new lib.line_explode();
	this.instance.parent = this;
	this.instance.setTransform(2.7,0,1,1,120);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(127).to({_off:false},0).to({_off:true},22).wait(1));

	// Calque_15
	this.instance_1 = new lib.line_explode();
	this.instance_1.parent = this;
	this.instance_1.setTransform(2.8,0,0.778,1,-155.3);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(125).to({_off:false},0).to({_off:true},24).wait(1));

	// Calque_14
	this.instance_2 = new lib.line_explode();
	this.instance_2.parent = this;
	this.instance_2.setTransform(2.8,0.1,1,1,-80.3);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(126).to({_off:false},0).to({_off:true},23).wait(1));

	// Calque_13
	this.instance_3 = new lib.line_explode();
	this.instance_3.parent = this;
	this.instance_3.setTransform(2.9,0.1,0.807,1,69.7,0,0,0.1,-0.1);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(124).to({_off:false},0).to({_off:true},25).wait(1));

	// Calque_12
	this.instance_4 = new lib.line_explode();
	this.instance_4.parent = this;
	this.instance_4.setTransform(2.8,0);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(123).to({_off:false},0).to({_off:true},26).wait(1));

	// Calque_5
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgGADIANgF");
	this.shape_1.setTransform(-10.5,13.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgVALIArgU");
	this.shape_2.setTransform(-8.9,12.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgkASIBJgj");
	this.shape_3.setTransform(-7.4,12);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgzAZIBngx");
	this.shape_4.setTransform(-5.9,11.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhDAgICHg/");
	this.shape_5.setTransform(-4.4,10.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhSAnIClhN");
	this.shape_6.setTransform(-2.9,9.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhhAvIDDhc");
	this.shape_7.setTransform(-1.4,9.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhwA2IDhhr");
	this.shape_8.setTransform(0.2,8.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#FFFFFF").ss(2,1,1).p("Ah/A9ID/h5");
	this.shape_9.setTransform(1.7,7.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#FFFFFF").ss(2,1,1).p("AiOBEIEdiH");
	this.shape_10.setTransform(3.2,7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_1}]},54).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[]},34).to({state:[]},52).wait(1));

	// Calque_6
	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAAgFIAAAL");
	this.shape_11.setTransform(-11.1,-13.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAAATIAAgl");
	this.shape_12.setTransform(-11.1,-12.5);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAAAhIAAhB");
	this.shape_13.setTransform(-11.1,-11.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAAAvIAAhc");
	this.shape_14.setTransform(-11.1,-9.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAAA8IAAh3");
	this.shape_15.setTransform(-11.1,-8.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAABKIAAiT");
	this.shape_16.setTransform(-11.1,-7.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAABXIAAit");
	this.shape_17.setTransform(-11.1,-5.7);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAABlIAAjJ");
	this.shape_18.setTransform(-11.1,-4.4);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAAByIAAjj");
	this.shape_19.setTransform(-11.1,-3);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAACAIAAj/");
	this.shape_20.setTransform(-11.1,-1.7);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAAiMIAAEZ");
	this.shape_21.setTransform(-11.1,-0.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_11}]},44).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[]},43).to({state:[]},52).wait(1));

	// Calque_4
	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#FFFFFF").ss(2,1,1).p("AANAHIgZgM");
	this.shape_22.setTransform(16.2,-0.5);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgbgNIA3Ab");
	this.shape_23.setTransform(14.8,-1.2);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgpgUIBTAp");
	this.shape_24.setTransform(13.3,-2);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#FFFFFF").ss(2,1,1).p("Ag3gcIBwA5");
	this.shape_25.setTransform(11.9,-2.7);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhGgjICNBH");
	this.shape_26.setTransform(10.4,-3.4);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhUgrICpBX");
	this.shape_27.setTransform(9,-4.2);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhjgyIDHBl");
	this.shape_28.setTransform(7.5,-4.9);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#FFFFFF").ss(2,1,1).p("Ahxg5IDjBz");
	this.shape_29.setTransform(6.1,-5.7);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#FFFFFF").ss(2,1,1).p("AiAhBIEBCD");
	this.shape_30.setTransform(4.6,-6.4);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#FFFFFF").ss(2,1,1).p("ACPBJIkdiR");
	this.shape_31.setTransform(3.2,-7.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_22}]},35).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[]},53).to({state:[]},52).wait(1));

	// Calque_3
	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#FFFFFF").ss(2,1,1).p("AAVAAIgpAA");
	this.shape_32.setTransform(-318.9,0);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgVAAIArAA");
	this.shape_33.setTransform(-318.6,0);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgZAAIAzAA");
	this.shape_34.setTransform(-317.5,0);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgfAAIA/AA");
	this.shape_35.setTransform(-315.8,0);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgnAAIBPAA");
	this.shape_36.setTransform(-313.3,0);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgyAAIBkAA");
	this.shape_37.setTransform(-310.1,0);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#FFFFFF").ss(2,1,1).p("Ag/AAIB+AA");
	this.shape_38.setTransform(-306.3,0);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhOAAICdAA");
	this.shape_39.setTransform(-301.7,0);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhfAAIC/AA");
	this.shape_40.setTransform(-296.5,0);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhzAAIDnAA");
	this.shape_41.setTransform(-290.5,0);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#FFFFFF").ss(2,1,1).p("AiJAAIETAA");
	this.shape_42.setTransform(-283.8,0);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#FFFFFF").ss(2,1,1).p("AiiAAIFFAA");
	this.shape_43.setTransform(-276.4,0);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#FFFFFF").ss(2,1,1).p("Ai9AAIF7AA");
	this.shape_44.setTransform(-268.4,0);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#FFFFFF").ss(2,1,1).p("AjaAAIG2AA");
	this.shape_45.setTransform(-259.6,0);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().s("#FFFFFF").ss(2,1,1).p("Aj6AAIH1AA");
	this.shape_46.setTransform(-250.1,0);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().s("#FFFFFF").ss(2,1,1).p("AkcAAII5AA");
	this.shape_47.setTransform(-239.9,0);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f().s("#FFFFFF").ss(2,1,1).p("AlAAAIKBAA");
	this.shape_48.setTransform(-229.1,0);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f().s("#FFFFFF").ss(2,1,1).p("AlnAAILPAA");
	this.shape_49.setTransform(-217.5,0);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f().s("#FFFFFF").ss(2,1,1).p("AmQAAIMhAA");
	this.shape_50.setTransform(-205.2,0);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f().s("#FFFFFF").ss(2,1,1).p("AmUAAIMpAA");
	this.shape_51.setTransform(-194.2,0);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f().s("#FFFFFF").ss(2,1,1).p("AmZAAIMzAA");
	this.shape_52.setTransform(-183.3,0);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f().s("#FFFFFF").ss(2,1,1).p("AmdAAIM7AA");
	this.shape_53.setTransform(-172.3,0);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f().s("#FFFFFF").ss(2,1,1).p("AmhAAINDAA");
	this.shape_54.setTransform(-161.3,0);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f().s("#FFFFFF").ss(2,1,1).p("AmmAAINNAA");
	this.shape_55.setTransform(-150.4,0);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f().s("#FFFFFF").ss(2,1,1).p("AmqAAINVAA");
	this.shape_56.setTransform(-139.4,0);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f().s("#FFFFFF").ss(2,1,1).p("AmvAAINeAA");
	this.shape_57.setTransform(-128.4,0);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f().s("#FFFFFF").ss(2,1,1).p("AmzAAINnAA");
	this.shape_58.setTransform(-117.5,0);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f().s("#FFFFFF").ss(2,1,1).p("Am4AAINxAA");
	this.shape_59.setTransform(-106.5,0);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f().s("#FFFFFF").ss(2,1,1).p("Am8AAIN5AA");
	this.shape_60.setTransform(-95.5,0);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f().s("#FFFFFF").ss(2,1,1).p("AnAAAIOBAA");
	this.shape_61.setTransform(-84.6,0);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f().s("#FFFFFF").ss(2,1,1).p("AnFAAIOLAA");
	this.shape_62.setTransform(-73.6,0);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f().s("#FFFFFF").ss(2,1,1).p("AnJAAIOTAA");
	this.shape_63.setTransform(-62.6,0);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f().s("#FFFFFF").ss(2,1,1).p("AnOAAIOdAA");
	this.shape_64.setTransform(-51.7,0);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f().s("#FFFFFF").ss(2,1,1).p("AnSAAIOlAA");
	this.shape_65.setTransform(-40.7,0);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f().s("#FFFFFF").ss(2,1,1).p("AHXAAIutAA");
	this.shape_66.setTransform(-29.8,0);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f().s("#FFFFFF").ss(2,1,1).p("AmfAAIM/AA");
	this.shape_67.setTransform(-24.2,0);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f().s("#FFFFFF").ss(2,1,1).p("AlrAAILXAA");
	this.shape_68.setTransform(-19,0);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f().s("#FFFFFF").ss(2,1,1).p("Ak7AAIJ3AA");
	this.shape_69.setTransform(-14.2,0);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f().s("#FFFFFF").ss(2,1,1).p("AkOAAIIdAA");
	this.shape_70.setTransform(-9.7,0);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f().s("#FFFFFF").ss(2,1,1).p("AjlAAIHLAA");
	this.shape_71.setTransform(-5.6,0);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f().s("#FFFFFF").ss(2,1,1).p("Ai/AAIF/AA");
	this.shape_72.setTransform(-1.8,0);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f().s("#FFFFFF").ss(2,1,1).p("AidAAIE7AA");
	this.shape_73.setTransform(1.6,0);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f().s("#FFFFFF").ss(2,1,1).p("Ah/AAID/AA");
	this.shape_74.setTransform(4.6,0);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhkAAIDJAA");
	this.shape_75.setTransform(7.3,0);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f().s("#FFFFFF").ss(2,1,1).p("AhNAAICbAA");
	this.shape_76.setTransform(9.6,0);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f().s("#FFFFFF").ss(2,1,1).p("Ag5AAIBzAA");
	this.shape_77.setTransform(11.6,0);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgpAAIBTAA");
	this.shape_78.setTransform(13.2,0);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgcAAIA5AA");
	this.shape_79.setTransform(14.5,0);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgTAAIAnAA");
	this.shape_80.setTransform(15.3,0);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f().s("#FFFFFF").ss(2,1,1).p("AgOAAIAdAA");
	this.shape_81.setTransform(15.9,0);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f().s("#FFFFFF").ss(2,1,1).p("AANAAIgZAA");
	this.shape_82.setTransform(16.1,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_51}]},1).to({state:[{t:this.shape_52}]},1).to({state:[{t:this.shape_53}]},1).to({state:[{t:this.shape_54}]},1).to({state:[{t:this.shape_55}]},1).to({state:[{t:this.shape_56}]},1).to({state:[{t:this.shape_57}]},1).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_59}]},1).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_63}]},1).to({state:[{t:this.shape_64}]},1).to({state:[{t:this.shape_65}]},1).to({state:[{t:this.shape_66}]},1).to({state:[{t:this.shape_67}]},1).to({state:[{t:this.shape_68}]},1).to({state:[{t:this.shape_69}]},1).to({state:[{t:this.shape_70}]},1).to({state:[{t:this.shape_71}]},1).to({state:[{t:this.shape_72}]},1).to({state:[{t:this.shape_73}]},1).to({state:[{t:this.shape_74}]},1).to({state:[{t:this.shape_75}]},1).to({state:[{t:this.shape_76}]},1).to({state:[{t:this.shape_77}]},1).to({state:[{t:this.shape_78}]},1).to({state:[{t:this.shape_79}]},1).to({state:[{t:this.shape_80}]},1).to({state:[{t:this.shape_81}]},1).to({state:[{t:this.shape_82}]},1).to({state:[]},1).to({state:[]},97).wait(1));

	// Calque_8 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_62 = new cjs.Graphics().p("Ai/DUIAAmoIF/AAIAAGog");
	var mask_graphics_63 = new cjs.Graphics().p("Ai/DUIAAmoIF/AAIAAGog");
	var mask_graphics_64 = new cjs.Graphics().p("Ai/DUIAAmoIF/AAIAAGog");
	var mask_graphics_65 = new cjs.Graphics().p("Ai/DUIAAmoIF/AAIAAGog");
	var mask_graphics_66 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_67 = new cjs.Graphics().p("AjADUIAAmoIGAAAIAAGog");
	var mask_graphics_68 = new cjs.Graphics().p("Ai/DUIAAmoIGAAAIAAGog");
	var mask_graphics_69 = new cjs.Graphics().p("Ai/DUIAAmoIF/AAIAAGog");
	var mask_graphics_70 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_71 = new cjs.Graphics().p("Ai/DUIAAmoIGAAAIAAGog");
	var mask_graphics_72 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_73 = new cjs.Graphics().p("Ai/DUIAAmoIF/AAIAAGog");
	var mask_graphics_74 = new cjs.Graphics().p("AjADUIAAmoIGAAAIAAGog");
	var mask_graphics_75 = new cjs.Graphics().p("AjADUIAAmoIGAAAIAAGog");
	var mask_graphics_76 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_77 = new cjs.Graphics().p("Ai/DUIAAmoIF/AAIAAGog");
	var mask_graphics_78 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_79 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_80 = new cjs.Graphics().p("Ai/DUIAAmoIF/AAIAAGog");
	var mask_graphics_81 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_82 = new cjs.Graphics().p("Ai/DUIAAmoIF/AAIAAGog");
	var mask_graphics_83 = new cjs.Graphics().p("AjADUIAAmoIGAAAIAAGog");
	var mask_graphics_84 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_85 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_86 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_87 = new cjs.Graphics().p("Ai/DUIAAmoIGAAAIAAGog");
	var mask_graphics_88 = new cjs.Graphics().p("AjADUIAAmoIGAAAIAAGog");
	var mask_graphics_89 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_90 = new cjs.Graphics().p("Ai/DUIAAmoIGAAAIAAGog");
	var mask_graphics_91 = new cjs.Graphics().p("AjADUIAAmoIGAAAIAAGog");
	var mask_graphics_92 = new cjs.Graphics().p("Ai/DUIAAmoIF/AAIAAGog");
	var mask_graphics_93 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_94 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_95 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_96 = new cjs.Graphics().p("AjADUIAAmoIGBAAIAAGog");
	var mask_graphics_97 = new cjs.Graphics().p("Ai/DUIAAmoIGAAAIAAGog");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(62).to({graphics:mask_graphics_62,x:38.2,y:0.7}).wait(1).to({graphics:mask_graphics_63,x:38.2,y:0.7}).wait(1).to({graphics:mask_graphics_64,x:38.2,y:0.7}).wait(1).to({graphics:mask_graphics_65,x:38.2,y:0.7}).wait(1).to({graphics:mask_graphics_66,x:38.2,y:0.7}).wait(1).to({graphics:mask_graphics_67,x:38.1,y:0.7}).wait(1).to({graphics:mask_graphics_68,x:38,y:0.7}).wait(1).to({graphics:mask_graphics_69,x:37.8,y:0.7}).wait(1).to({graphics:mask_graphics_70,x:37.5,y:0.7}).wait(1).to({graphics:mask_graphics_71,x:37.1,y:0.7}).wait(1).to({graphics:mask_graphics_72,x:36.5,y:0.7}).wait(1).to({graphics:mask_graphics_73,x:35.6,y:0.7}).wait(1).to({graphics:mask_graphics_74,x:34.5,y:0.7}).wait(1).to({graphics:mask_graphics_75,x:33.2,y:0.7}).wait(1).to({graphics:mask_graphics_76,x:31.4,y:0.7}).wait(1).to({graphics:mask_graphics_77,x:29.2,y:0.7}).wait(1).to({graphics:mask_graphics_78,x:26.6,y:0.7}).wait(1).to({graphics:mask_graphics_79,x:23.4,y:0.7}).wait(1).to({graphics:mask_graphics_80,x:20.2,y:0.7}).wait(1).to({graphics:mask_graphics_81,x:17.6,y:0.7}).wait(1).to({graphics:mask_graphics_82,x:15.4,y:0.7}).wait(1).to({graphics:mask_graphics_83,x:13.6,y:0.7}).wait(1).to({graphics:mask_graphics_84,x:12.3,y:0.7}).wait(1).to({graphics:mask_graphics_85,x:11.2,y:0.7}).wait(1).to({graphics:mask_graphics_86,x:10.4,y:0.7}).wait(1).to({graphics:mask_graphics_87,x:9.7,y:0.7}).wait(1).to({graphics:mask_graphics_88,x:9.3,y:0.7}).wait(1).to({graphics:mask_graphics_89,x:9,y:0.7}).wait(1).to({graphics:mask_graphics_90,x:8.8,y:0.7}).wait(1).to({graphics:mask_graphics_91,x:8.7,y:0.7}).wait(1).to({graphics:mask_graphics_92,x:8.6,y:0.7}).wait(1).to({graphics:mask_graphics_93,x:8.6,y:0.7}).wait(1).to({graphics:mask_graphics_94,x:8.6,y:0.7}).wait(1).to({graphics:mask_graphics_95,x:8.6,y:0.7}).wait(1).to({graphics:mask_graphics_96,x:8.6,y:0.7}).wait(1).to({graphics:mask_graphics_97,x:4.8,y:0.7}).wait(53));

	// Calque_7
	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("#FFFFFF").s().p("AiOiMIEdCRIkdCIg");
	this.shape_83.setTransform(3.2,-0.3);

	this.instance_5 = new lib.playArrow();
	this.instance_5.parent = this;
	this.instance_5.setTransform(3.2,-0.3);
	this.instance_5._off = true;

	var maskedShapeInstanceList = [this.shape_83,this.instance_5];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_83}]},62).to({state:[{t:this.instance_5}]},35).to({state:[{t:this.instance_5}]},3).to({state:[{t:this.instance_5}]},18).to({state:[]},1).to({state:[]},30).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(97).to({_off:false},0).wait(3).to({scaleX:0.07,scaleY:0.07},18,cjs.Ease.quartIn).to({_off:true},1).wait(31));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-78.3,-78.4,156.7,156.7);


(lib.Btn_next = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{rollOver:55,rollOut:70});

	// timeline functions:
	this.frame_54 = function() {
		this.stop();
	}
	this.frame_69 = function() {
		this.stop();
	}
	this.frame_84 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(54).call(this.frame_54).wait(15).call(this.frame_69).wait(15).call(this.frame_84).wait(1));

	// ZONE
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.008)").s().p("Ehj/AEtIAApZMDH/AAAIAAJZg");
	this.shape.setTransform(640,29.9);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(85));

	// txt
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgeAqIAAhTIA8AAIAAARIgpAAIAAARIAkAAIAAAPIgkAAIAAASIAqAAIAAAQg");
	this.shape_1.setTransform(860.2,28.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgIAqIAAhDIgYAAIAAgQIBBAAIAAAQIgXAAIAABDg");
	this.shape_2.setTransform(844.4,28.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgIAqIAAhTIARAAIAABTg");
	this.shape_3.setTransform(830.8,28.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgYAgQgLgKAAgSIAAgtIATAAIAAAtQAAAKAEAGQAFAGAHgBQAIABAFgGQAEgGAAgKIAAgtIATAAIAAAtQAAATgKAJQgKAKgQAAQgPAAgJgKg");
	this.shape_4.setTransform(816.3,28.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AggAdIALgOQANAMALAAQAFAAADgDQADgBAAgEQAAgEgDgCQgDgCgJgDQgOgEgHgEQgHgGAAgMQAAgLAJgHQAIgHAMABQAJAAAIACQAIAEAHAFIgKANQgLgIgLAAQgEAAgDADQgDACAAADQAAAEAEACQADACALADQAMADAHAFQAGAGAAALQAAALgIAIQgJAGgOAAQgSAAgQgOg");
	this.shape_5.setTransform(799.9,28.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAZAqIgHgSIgjAAIgHASIgUAAIAkhTIARAAIAkBTgAgKAIIAUAAIgKgYg");
	this.shape_6.setTransform(772.6,28.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgaAqIAAhTIASAAIAABDIAjAAIAAAQg");
	this.shape_7.setTransform(757.1,28.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AAZA5IgHgSIgjAAIgHASIgUAAIAkhTIARAAIAkBTgAgKAWIAUAAIgKgXgAgHgjIgRgNIATgIIAOAVg");
	this.shape_8.setTransform(729.7,26.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AAOAqIgSgbIgNAAIAAAbIgTAAIAAhTIAgAAQATAAAIAGQAJAIAAAOQAAATgQAGIAUAegAgRAAIAOAAQAIAAAEgDQAEgDAAgHQgBgHgEgDQgDgCgIAAIgOAAg");
	this.shape_9.setTransform(702.2,28.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgeAqIAAhTIA8AAIAAARIgpAAIAAARIAkAAIAAAPIgkAAIAAASIAqAAIAAAQg");
	this.shape_10.setTransform(685.7,28.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AggAdIALgOQANAMALAAQAFAAADgDQADgBAAgEQAAgEgDgCQgDgCgJgDQgOgEgHgEQgHgGAAgMQAAgLAJgHQAIgHAMABQAJAAAIACQAIAEAHAFIgKANQgLgIgLAAQgEAAgDADQgDACAAADQAAAEAEACQADACALADQAMADAHAFQAGAGAAALQAAALgIAIQgJAGgOAAQgSAAgQgOg");
	this.shape_11.setTransform(669.7,28.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AggAdIALgOQANAMALAAQAFAAADgDQADgBAAgEQAAgEgDgCQgDgCgJgDQgOgEgHgEQgHgGAAgMQAAgLAJgHQAIgHAMABQAJAAAIACQAIAEAHAFIgKANQgLgIgLAAQgEAAgDADQgDACAAADQAAAEAEACQADACALADQAMADAHAFQAGAGAAALQAAALgIAIQgJAGgOAAQgSAAgQgOg");
	this.shape_12.setTransform(654.1,28.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AAZAqIgHgSIgjAAIgHASIgUAAIAkhTIARAAIAkBTgAgKAIIAUAAIgKgYg");
	this.shape_13.setTransform(637.8,28.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AggAqIAAhTIAdAAQATAAAIAHQAJAIAAAQQAAAOgJAHQgJAIgSgBIgLAAIAAAYgAgOACIANAAQAIAAAEgCQADgEAAgHQAAgIgEgCQgFgEgIAAIgLAAg");
	this.shape_14.setTransform(621.6,28.4);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AAOAqIgSgbIgNAAIAAAbIgTAAIAAhTIAgAAQATAAAIAGQAJAIAAAOQAAATgQAGIAUAegAgRAAIAOAAQAIAAAEgDQAEgDAAgHQgBgHgEgDQgDgCgIAAIgOAAg");
	this.shape_15.setTransform(594.2,28.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgZAgQgKgKAAgSIAAgtIATAAIAAAtQAAAKAFAGQAEAGAHgBQAIABAEgGQAGgGAAgKIAAgtIASAAIAAAtQAAATgKAJQgKAKgQAAQgPAAgKgKg");
	this.shape_16.setTransform(577,28.5);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgeAfQgNgMAAgTQAAgSANgMQAMgMASAAQASAAAOAMQAMAMAAASQAAATgMAMQgOAMgSAAQgSAAgMgMgAgRgSQgHAIAAAKQAAAMAHAHQAIAIAJAAQAKAAAIgIQAHgHAAgMQAAgKgHgIQgIgIgKAAQgJAAgIAIg");
	this.shape_17.setTransform(559.5,28.4);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AggAqIAAhTIAdAAQATAAAIAHQAJAIAAAQQAAAOgJAHQgJAIgSgBIgLAAIAAAYgAgOACIANAAQAIAAAEgCQADgEAAgHQAAgIgEgCQgFgEgIAAIgLAAg");
	this.shape_18.setTransform(542.8,28.4);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgiAqIAAgNIArg2IgpAAIAAgQIBCAAIAAANIgrA2IAsAAIAAAQg");
	this.shape_19.setTransform(515.3,28.4);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgeAqIAAhTIA8AAIAAARIgpAAIAAARIAkAAIAAAPIgkAAIAAASIAqAAIAAAQg");
	this.shape_20.setTransform(499.5,28.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgYAgQgLgKAAgSIAAgtIATAAIAAAtQAAAKAEAGQAFAGAHgBQAIABAFgGQAEgGAAgKIAAgtIATAAIAAAtQAAATgKAJQgKAKgQAAQgPAAgJgKg");
	this.shape_21.setTransform(482.9,28.5);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AAHAvQgGgEgDgHQgTAAgNgNQgMgMAAgRQAAgTAMgMQANgNATAAQARAAANANQANAMAAATQAAALgHALQgGALgMAEQABABAAAAQAAABABAAQAAABABAAQAAAAABABIAFABIAEgBIAEgBIAFgFIAJALQgKALgPAAQgIAAgHgEgAgUgZQgHAHAAAMQAAAKAHAHQAHAJALgBQAJABAIgJQAHgHAAgKQAAgMgHgHQgIgJgJAAQgLAAgHAJg");
	this.shape_22.setTransform(465.7,29.1);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgIAqIAAhTIARAAIAABTg");
	this.shape_23.setTransform(450.5,28.4);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgaAqIAAhTIASAAIAABDIAjAAIAAAQg");
	this.shape_24.setTransform(437.5,28.4);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgaAfQgMgMAAgSQAAgTAMgMQANgMASAAQAUAAAOAPIgMAOQgIgLgOAAQgJAAgIAGQgHAIAAAKQAAAMAHAHQAHAHAJAAQAOAAAJgLIAMANQgPAPgTAAQgSAAgNgMg");
	this.shape_25.setTransform(421.4,28.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(85));

	// Layer_1
	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("rgba(51,51,51,0.2)").ss(1,1,1).p("Ehj/AADMDH/gAF");
	this.shape_26.setTransform(640,-0.2);

	this.timeline.addTween(cjs.Tween.get(this.shape_26).wait(85));

	// Calque_5
	this.instance = new lib.Btn_cliquable_area();
	this.instance.parent = this;
	this.instance.setTransform(0,29.5,1,1,0,0,0,0,30);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("rgba(0,0,0,0.008)").s().p("Ehj/AEsIAApXMDH/AAAIAAJXg");
	this.shape_27.setTransform(640,29.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance}]},3).to({state:[{t:this.instance}]},46).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.instance}]},5).to({state:[{t:this.instance}]},14).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},14).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({alpha:0.012},46).to({_off:true},1).wait(5).to({_off:false},0).to({alpha:1},14).wait(1).to({alpha:0.012},14).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1,-1.5,1282,61.5);


(lib.bloc_video = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{over:1});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_83 = function() {
		//PAUSE
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(83).call(this.frame_83).wait(1));

	// masque_video_copy_5
	this.instance = new lib.mask5();
	this.instance.parent = this;
	this.instance.setTransform(1459.9,390,0.355,1.042);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(11).to({x:1207},31,cjs.Ease.quartInOut).wait(3).to({y:1187.5},28).to({_off:true},10).wait(1));

	// masque_video_copy_4
	this.instance_1 = new lib.mask4();
	this.instance_1.parent = this;
	this.instance_1.setTransform(980.6,-407.6,0.355,1.042,0,0,0,0.1,0);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1).to({x:980.1,y:384.9},31,cjs.Ease.quartInOut).wait(16).to({y:-440.1},28).to({_off:true},7).wait(1));

	// masque_video_copy_3
	this.instance_2 = new lib.mask3();
	this.instance_2.parent = this;
	this.instance_2.setTransform(753.2,1187.7,0.355,1.042,0,0,0,0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(6).to({y:384.5},31,cjs.Ease.quartInOut).wait(13).to({x:1468.2},28).to({_off:true},5).wait(1));

	// masque_video_copy_2
	this.instance_3 = new lib.mask2();
	this.instance_3.parent = this;
	this.instance_3.setTransform(526.3,-449.5,0.355,1.042);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(9).to({y:387.5},31,cjs.Ease.quartInOut).wait(7).to({y:1185},28).to({_off:true},8).wait(1));

	// masque_video_copy
	this.instance_4 = new lib.mask1();
	this.instance_4.parent = this;
	this.instance_4.setTransform(299.5,1187.7,0.355,1.042,0,0,0,0.1,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(3).to({y:386.6},31,cjs.Ease.quartInOut).wait(16).to({x:-168},28).to({_off:true},5).wait(1));

	// masque_video
	this.instance_5 = new lib.mask0();
	this.instance_5.parent = this;
	this.instance_5.setTransform(-137.2,390,0.355,1.042,0,0,0,-0.1,0);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(14).to({x:73},31,cjs.Ease.quartInOut).wait(7).to({y:-407.5},28).to({_off:true},3).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-250.9,-849.5,1824.6,2437.1);


(lib.bloc_title = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"open":1,"close":20});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_19 = function() {
		this.stop();
	}
	this.frame_37 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(19).call(this.frame_19).wait(18).call(this.frame_37).wait(1));

	// line
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(128,128,128,0.2)").s().p("ArFADIAAgFIWLAAIAAAFg");
	this.shape.setTransform(211,34.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(38));

	// CTA_DETAIL
	this.title_txt = new lib.title_txt();
	this.title_txt.name = "title_txt";
	this.title_txt.parent = this;
	this.title_txt.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.title_txt).wait(1).to({alpha:1},11).wait(13).to({alpha:0},10).to({_off:true},1).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,420.2,99.1);


(lib.bloc_NEXT_btn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":20});

	// timeline functions:
	this.frame_19 = function() {
		this.stop();
	}
	this.frame_37 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(19).call(this.frame_19).wait(18).call(this.frame_37).wait(1));

	// CTA_DETAIL
	this.btnNext = new lib.Btn_next();
	this.btnNext.name = "btnNext";
	this.btnNext.parent = this;
	this.btnNext.setTransform(0,411.5,1,1,0,0,0,0,26.5);

	this.timeline.addTween(cjs.Tween.get(this.btnNext).to({y:352.5},19,cjs.Ease.quartInOut).wait(2).to({y:411.5},14,cjs.Ease.quartInOut).to({_off:true},1).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-0.5,384,1281,61);


(lib.bloc_lignes = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":166});

	// timeline functions:
	this.frame_165 = function() {
		this.stop();
	}
	this.frame_186 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(165).call(this.frame_165).wait(21).call(this.frame_186).wait(1));

	// ligne
	this.instance = new lib.ligne();
	this.instance.parent = this;
	this.instance.setTransform(-454,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(187));

	// ligne
	this.instance_1 = new lib.ligne();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-227,406.5,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(119).to({y:320.5,alpha:0.199},40,cjs.Ease.quartInOut).wait(7).to({y:406.5,alpha:1},17,cjs.Ease.quartInOut).wait(4));

	// ligne
	this.instance_2 = new lib.ligne();
	this.instance_2.parent = this;
	this.instance_2.setTransform(0,406.5,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(121).to({y:320,alpha:0.199},41,cjs.Ease.quartInOut).wait(4).to({y:406.5,alpha:1},17,cjs.Ease.quartInOut).wait(4));

	// ligne
	this.instance_3 = new lib.ligne();
	this.instance_3.parent = this;
	this.instance_3.setTransform(227,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(124).to({y:320,alpha:0.199},40,cjs.Ease.quartInOut).wait(2).to({y:406,alpha:1},17,cjs.Ease.quartInOut).wait(4));

	// ligne
	this.instance_4 = new lib.ligne();
	this.instance_4.parent = this;
	this.instance_4.setTransform(454.1,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(187));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-455.4,-174.5,910.5,988);


(lib.video_bloc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// btn_playVideo
	this.btn_playVideo = new lib.btn_playVideo();
	this.btn_playVideo.name = "btn_playVideo";
	this.btn_playVideo.parent = this;
	this.btn_playVideo.setTransform(688,390);

	this.timeline.addTween(cjs.Tween.get(this.btn_playVideo).wait(1));

	// videoContainer
	this.videoContainer = new lib.video();
	this.videoContainer.name = "videoContainer";
	this.videoContainer.parent = this;
	this.videoContainer.setTransform(682.7,383.9,0.711,0.711,0,0,0,960.1,539.9);

	this.timeline.addTween(cjs.Tween.get(this.videoContainer).wait(1));

}).prototype = getMCSymbolPrototype(lib.video_bloc, new cjs.Rectangle(0,0,1365.3,768), null);


// stage content:
(lib.FS_interlude_video_mobile = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{"close":110});

	// timeline functions:
	this.frame_4 = function() {
		this.masks_mc.gotoAndPlay("over");
	}
	this.frame_29 = function() {
		this.video_mc.btn_playVideo.gotoAndPlay("open");
		this.title_mc.gotoAndPlay("open");
		
		
		
		// EVENT DE CLIC POUR LA VIDEO
		this.video_mc.btn_playVideo.addEventListener("click", playVideo.bind(this));
		
		function playVideo()
		{	
			var event = new Event('video');
			this.dispatchEvent(event);		
			event = null;
			
			this.video_mc.btn_playVideo.gotoAndPlay('close');
		}
	}
	this.frame_44 = function() {
		//On envoie l'information de l'affichage de la video
		var event = new Event('videoStarter');
		this.dispatchEvent(event);		
		event = null;
		
		
		//var frequency = 60;
		//stage.enableMouseOver(frequency);
		
		// EVENT DE CLIC POUR LA SUITE
		this.next_btn.addEventListener("click", fl_ClickToGoToAndPlayFromFrame.bind(this));
		this.next_btn.addEventListener("mouseover", fl_MouseOverHandler.bind(this));
		this.next_btn.addEventListener("mouseout", fl_MouseOutHandler.bind(this));
		
		
		function fl_ClickToGoToAndPlayFromFrame()
		{
			//this.gotoAndPlay("close");
			
			var event = new Event('next');
			this.dispatchEvent(event);		
			event = null;
			
			this.next_btn.btnNext.gotoAndPlay('rollOut');
			
		}
		function fl_MouseOverHandler(){	this.next_btn.btnNext.gotoAndPlay('rollOver'); }
		function fl_MouseOutHandler(){	this.next_btn.btnNext.gotoAndPlay('rollOut'); }
	}
	this.frame_95 = function() {
		this.stop();
	}
	this.frame_109 = function() {
		this.stop();
	}
	this.frame_110 = function() {
		this.next_btn.gotoAndPlay("close");
		this.masks_mc.gotoAndPlay("over");
		this.title_mc.gotoAndPlay("close");
	}
	this.frame_179 = function() {
		this.stop();
		
		// EVENT DE FIN D'ANIM
		var event = new Event('closed');
		this.dispatchEvent(event);		
		event = null;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(4).call(this.frame_4).wait(25).call(this.frame_29).wait(15).call(this.frame_44).wait(51).call(this.frame_95).wait(14).call(this.frame_109).wait(1).call(this.frame_110).wait(69).call(this.frame_179).wait(1));

	// cta_btn
	this.next_btn = new lib.bloc_NEXT_btn();
	this.next_btn.name = "next_btn";
	this.next_btn.parent = this;
	this.next_btn.setTransform(-740.5,860.9,1,1,0,0,0,-340.5,403.9);
	this.next_btn._off = true;

	this.timeline.addTween(cjs.Tween.get(this.next_btn).wait(43).to({_off:false},0).wait(137));

	// title_mc
	this.title_mc = new lib.bloc_title();
	this.title_mc.name = "title_mc";
	this.title_mc.parent = this;
	this.title_mc.setTransform(-310.5,463.9,1,1,0,0,0,-340.5,403.9);

	this.timeline.addTween(cjs.Tween.get(this.title_mc).wait(180));

	// lignes
	this.lignes_mc = new lib.bloc_lignes();
	this.lignes_mc.name = "lignes_mc";
	this.lignes_mc.parent = this;
	this.lignes_mc.setTransform(240.8,453.9,0.525,1,0,0,0,0.7,405.9);

	this.timeline.addTween(cjs.Tween.get(this.lignes_mc).wait(180));

	// masque
	this.masks_mc = new lib.bloc_video();
	this.masks_mc.name = "masks_mc";
	this.masks_mc.parent = this;
	this.masks_mc.setTransform(24.3,835.8,0.525,1.1,0,0,0,227.2,764.9);

	this.timeline.addTween(cjs.Tween.get(this.masks_mc).to({_off:true},79).wait(101));

	// video
	this.video_mc = new lib.video_bloc();
	this.video_mc.name = "video_mc";
	this.video_mc.parent = this;
	this.video_mc.setTransform(-510.8,-2.4,1.1,1.1);
	this.video_mc.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.video_mc).wait(44).to({alpha:1},0).to({_off:true},106).wait(30));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-270.8,-520,1501.8,2680.8);
// library properties:
lib.properties = {
	id: '6647B5CB2B9BD44C90AAEB3DDED4EC53',
	width: 480,
	height: 840,
	fps: 60,
	color: "#000000",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['6647B5CB2B9BD44C90AAEB3DDED4EC53'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;