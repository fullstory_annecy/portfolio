(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.action = function() {
	this.initialize(img.action);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,640,335);


(lib.danger = function() {
	this.initialize(img.danger);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1080,1080);


(lib.fire_0000_Layer8 = function() {
	this.initialize(img.fire_0000_Layer8);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,50,79);


(lib.fire_0001_Layer9 = function() {
	this.initialize(img.fire_0001_Layer9);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,50,79);


(lib.fire_0002_Layer10 = function() {
	this.initialize(img.fire_0002_Layer10);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,50,79);


(lib.fire_0003_Layer11 = function() {
	this.initialize(img.fire_0003_Layer11);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,50,79);


(lib.fire_0004_Layer12 = function() {
	this.initialize(img.fire_0004_Layer12);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,50,79);


(lib.fire_0005_Layer13 = function() {
	this.initialize(img.fire_0005_Layer13);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,50,79);


(lib.fire_0006_Layer14 = function() {
	this.initialize(img.fire_0006_Layer14);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,50,79);


(lib.fire_0007_Layer15 = function() {
	this.initialize(img.fire_0007_Layer15);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,50,79);


(lib.fire_0008_Layer16 = function() {
	this.initialize(img.fire_0008_Layer16);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,50,79);


(lib.fire_0009_Layer17 = function() {
	this.initialize(img.fire_0009_Layer17);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,50,79);


(lib.fire_0010_Layer1 = function() {
	this.initialize(img.fire_0010_Layer1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,50,79);


(lib.fire_0011_Layer2 = function() {
	this.initialize(img.fire_0011_Layer2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,50,79);


(lib.fire_0012_Layer3 = function() {
	this.initialize(img.fire_0012_Layer3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,50,79);


(lib.fire_0013_Layer4 = function() {
	this.initialize(img.fire_0013_Layer4);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,50,79);


(lib.fire_0014_Layer5 = function() {
	this.initialize(img.fire_0014_Layer5);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,50,79);


(lib.fire_0015_Layer6 = function() {
	this.initialize(img.fire_0015_Layer6);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,50,79);


(lib.fire_0016_Layer7 = function() {
	this.initialize(img.fire_0016_Layer7);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,50,79);


(lib.vitesse = function() {
	this.initialize(img.vitesse);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,640,640);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.vignette_vitesse = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.vitesse();
	this.instance.parent = this;
	this.instance.setTransform(-320,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.vignette_vitesse, new cjs.Rectangle(-320,0,640,640), null);


(lib.txt_vitesse = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_69 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(69).call(this.frame_69).wait(1));

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("EgimAPRIAA+hMBFNAAAIAAehg");
	this.shape.setTransform(-181.2,198.9);
	this.shape._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(14).to({_off:false},0).to({_off:true},4).wait(35).to({_off:false},0).to({_off:true},4).wait(13));

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("EhJoADpIAAnRMCTRAAAIAAHRg");
	this.shape_1.setTransform(-311.9,-39.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("EgupAFkIAArHMBdTAAAIAALHg");
	this.shape_2.setTransform(163.8,243);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("EgoIAPRIAA+hMBQRAAAIAAehg");
	this.shape_3.setTransform(223.7,109.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("Eg1UAPRIAA+hMBqpAAAIAAehg");
	this.shape_4.setTransform(139.3,109.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("EhJoAPRIAA+hMCTRAAAIAAehg");
	this.shape_5.setTransform(9.2,109.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AoLLHIAA2NIQAAAIAAEbIrCAAIAAElIJ7AAIAAENIp7AAIAAEnILZAAIAAEZg");
	this.shape_6.setTransform(371,105.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AozHlIC9joQDmDKDCAAQBXAAAygmQAzgmAAhAQAAhAg2glQg1gmicgnQj3g7hzhdQh0hfAAjKQAAjLCShuQCRhtDZAAQCPAACOAwQCOAxBrBaIihDoQi5iNjFAAQhPAAguAmQguAlAAA+QAAA/A4AkQA5AlDIAyQDLAxBwBjQBvBkAAC/QAADAiPB3QiPB2jqAAQlRAAkOj6g");
	this.shape_7.setTransform(235,105.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AozHlIC9joQDmDKDCAAQBXAAAygmQAzgmAAhAQAAhAg2glQg1gmicgnQj3g7hzhdQh0hfAAjKQAAjLCShuQCRhtDZAAQCPAACOAwQCOAxBrBaIihDoQi5iNjFAAQhPAAguAmQguAlAAA+QAAA/A4AkQA5AlDIAyQDLAxBwBjQBvBkAAC/QAADAiPB3QiPB2jqAAQlRAAkOj6g");
	this.shape_8.setTransform(105.2,105.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AoLLHIAA2NIQAAAIAAEbIrCAAIAAElIJ7AAIAAENIp7AAIAAEnILZAAIAAEZg");
	this.shape_9.setTransform(-21.8,105.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AidLHIAAx6ImUAAIAAkTIRjAAIAAETImUAAIAAR6g");
	this.shape_10.setTransform(-153.5,105.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AieLHIAA2NIE9AAIAAWNg");
	this.shape_11.setTransform(-248.7,105.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AiFLHIo82NIFYAAIFqOJIFpuJIFYAAIo7WNg");
	this.shape_12.setTransform(-352.1,105.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1}]}).to({state:[{t:this.shape_2}]},3).to({state:[{t:this.shape_3}]},3).to({state:[{t:this.shape_4}]},3).to({state:[{t:this.shape_5}]},3).to({state:[{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6}]},3).to({state:[{t:this.shape_5}]},40).to({state:[{t:this.shape_4}]},3).to({state:[{t:this.shape_3}]},3).to({state:[{t:this.shape_2}]},3).to({state:[{t:this.shape_1}]},4).to({state:[]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-783.2,-62.6,942.7,46.6);


(lib.txt_violence = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_69 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(69).call(this.frame_69).wait(1));

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("EgimAPRIAA+hMBFNAAAIAAehg");
	this.shape.setTransform(-181.2,198.9);
	this.shape._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(14).to({_off:false},0).to({_off:true},4).wait(35).to({_off:false},0).to({_off:true},4).wait(13));

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("EhJoADpIAAnRMCTRAAAIAAHRg");
	this.shape_1.setTransform(-311.9,-39.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("EgupAFkIAArHMBdTAAAIAALHg");
	this.shape_2.setTransform(163.8,243);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("EgoIAPRIAA+hMBQRAAAIAAehg");
	this.shape_3.setTransform(223.7,109.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("Eg1UAPRIAA+hMBqpAAAIAAehg");
	this.shape_4.setTransform(139.3,109.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("EhJoAPRIAA+hMCTRAAAIAAehg");
	this.shape_5.setTransform(9.2,109.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AoLLHIAA2NIQAAAIAAEbIrCAAIAAElIJ8AAIAAENIp8AAIAAEnILZAAIAAEZg");
	this.shape_6.setTransform(478.8,105.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AnFIPQjVjOABk9QgBk7DZjUQDYjTE6AAQFdAADtEKIjGDgQiXi7jgAAQixAAiAB1Qh+B0gBDGQABDHB4B4QB3B3CrAAQDpAACVi5IDLDSQjxEQlJAAQlJAAjUjQg");
	this.shape_7.setTransform(335.6,105.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AFTLHIqkt6IAAN6Ik+AAIAA2NIEpAAIK5OTIAAuTIE9AAIAAWNg");
	this.shape_8.setTransform(176.8,105.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AoLLHIAA2NIQAAAIAAEbIrCAAIAAElIJ8AAIAAENIp8AAIAAEnILZAAIAAEZg");
	this.shape_9.setTransform(30.5,105.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AnNLHIAA2NIE+AAIAARyIJdAAIAAEbg");
	this.shape_10.setTransform(-91.4,105.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AoSIMQjYjTAAk5QAAk4DYjTQDYjSE6AAQE8AADXDSQDYDTgBE4QABE5jYDTQjXDSk8AAQk6AAjYjSgAkqlDQh5CHAAC9QAAC/B5CFQB6CFCxAAQCzAAB6iFQB6iFAAi/QAAi9h6iHQh6iGizAAQixAAh6CGg");
	this.shape_11.setTransform(-239.1,105);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AieLHIAA2NIE9AAIAAWNg");
	this.shape_12.setTransform(-356.5,105.9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AiFLHIo82NIFYAAIFqOJIFpuJIFYAAIo8WNg");
	this.shape_13.setTransform(-459.9,105.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1}]}).to({state:[{t:this.shape_2}]},3).to({state:[{t:this.shape_3}]},3).to({state:[{t:this.shape_4}]},3).to({state:[{t:this.shape_5}]},3).to({state:[{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6}]},3).to({state:[{t:this.shape_5}]},40).to({state:[{t:this.shape_4}]},3).to({state:[{t:this.shape_3}]},3).to({state:[{t:this.shape_2}]},3).to({state:[{t:this.shape_1}]},4).to({state:[]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-783.2,-62.6,942.7,46.6);


(lib.txt_surInstagram = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_109 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(109).call(this.frame_109).wait(1));

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("EgimAPRIAA+hMBFNAAAIAAehg");
	this.shape.setTransform(-181.2,198.9);
	this.shape._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(14).to({_off:false},0).to({_off:true},4).wait(75).to({_off:false},0).to({_off:true},4).wait(13));

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("EhJoADpIAAnRMCTRAAAIAAHRg");
	this.shape_1.setTransform(-311.9,-39.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("EgupAFkIAArHMBdTAAAIAALHg");
	this.shape_2.setTransform(163.8,243);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("EgoIAPRIAA+hMBQRAAAIAAehg");
	this.shape_3.setTransform(223.7,109.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("Eg1UAPRIAA+hMBqpAAAIAAehg");
	this.shape_4.setTransform(139.3,109.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("Ehy5APRIAA+hMDlyAAAIAAehg");
	this.shape_5.setTransform(29.2,109.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AHeLHIAAt+ImBMIIi7AAIl/sIIAAN+Ik+AAIAA2NIGtAAIFuMMIFxsMIGrAAIAAWNg");
	this.shape_6.setTransform(1189.7,105.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AGvLHIiFkzIpTAAIiEEzIlSAAIJm2NIEzAAIJmWNgAixB9IFjAAIixmcg");
	this.shape_7.setTransform(1016.9,105.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("ADnLHIk6nGIjcAAIAAHGIk9AAIAA2NIIbAAQFLAACNBwQCNBwAAD4QAAFVkOBkIFoH8gAkvgQIDoAAQChAAA7g1QA8g1AAhzQAAhzg+grQg8gqiZAAIjtAAg");
	this.shape_8.setTransform(872.8,105.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AnBIPQjUjOAAk9QAAk7DYjUQDZjTEyAAQEyAADhDIIilDvQhfhUhQgfQhRgfhdAAQi0AAh8B7Qh8B6AADJQAADKB3B5QB3B5CjAAQClAABuhAIAAmFIE/AAIAAH4QjUDrl1AAQk5AAjVjQg");
	this.shape_9.setTransform(716.1,105.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AGuLHIiEkzIpTAAIiEEzIlSAAIJm2NIEzAAIJmWNgAixB9IFjAAIixmcg");
	this.shape_10.setTransform(564.8,105.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AieLHIAAx6ImTAAIAAkTIRjAAIAAETImTAAIAAR6g");
	this.shape_11.setTransform(427.1,105.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AozHlIC9joQDmDKDCAAQBXAAAygmQAzgmAAhAQAAhAg2glQg1gmicgnQj3g7hzhdQh0hfAAjKQAAjLCShuQCRhtDZAAQCPAACOAwQCOAxBrBaIihDoQi5iNjFAAQhPAAguAmQguAlAAA+QAAA/A4AkQA5AlDIAyQDLAxBwBjQBvBkAAC/QAADAiPB3QiPB2jqAAQlRAAkOj6g");
	this.shape_12.setTransform(299.2,105.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AFTLHIqkt6IAAN6Ik+AAIAA2NIEpAAIK5OTIAAuTIE9AAIAAWNg");
	this.shape_13.setTransform(152.3,105.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AieLHIAA2NIE9AAIAAWNg");
	this.shape_14.setTransform(35.8,105.9);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AoLLHIAA2NIQAAAIAAEbIrCAAIAAElIJ8AAIAAENIp8AAIAAEnILZAAIAAEZg");
	this.shape_15.setTransform(-114.5,105.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("ADnLHIk6nGIjcAAIAAHGIk+AAIAA2NIIcAAQFLAACNBwQCNBwAAD4QAAFVkPBkIFoH8gAkvgQIDoAAQChAAA8g1QA7g1AAhzQAAhzg9grQg+gqiXAAIjuAAg");
	this.shape_16.setTransform(-252.7,105.9);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AidLHIAAx6ImUAAIAAkTIRjAAIAAETImUAAIAAR6g");
	this.shape_17.setTransform(-394.2,105.9);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AoSIMQjYjTABk5QgBk4DYjTQDXjSE8AAQE6AADYDSQDXDTABE4QgBE5jXDTQjYDSk6AAQk8AAjXjSgAkplDQh6CHgBC9QABC/B6CFQB6CFCwAAQCyAAB6iFQB7iFAAi/QAAi9h7iHQh6iGiyAAQiwAAh6CGg");
	this.shape_18.setTransform(-540.1,105);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AFTLHIqkt6IAAN6Ik+AAIAA2NIEpAAIK5OTIAAuTIE9AAIAAWNg");
	this.shape_19.setTransform(-707.3,105.9);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("ADnLHIk6nGIjcAAIAAHGIk+AAIAA2NIIcAAQFLAACNBwQCNBwAAD4QAAFVkPBkIFoH8gAkvgQIDoAAQChAAA8g1QA7g1AAhzQAAhzg9grQg+gqiXAAIjuAAg");
	this.shape_20.setTransform(-912.4,105.9);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("Am0InQisinAAkzIAAsaIE9AAIAAMQQAACrBQBjQBPBjCFAAQCGAABOhjQBPhjAAirIAAsQIE9AAIAAMaQAAE2irClQiqCmkLAAQkJAAising");
	this.shape_21.setTransform(-1068.5,106.6);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AozHlIC9joQDmDKDCAAQBXAAAygmQAzgmAAhAQAAhAg2glQg1gmicgnQj3g7hzhdQh0hfAAjKQAAjLCShuQCRhtDZAAQCPAACOAwQCOAxBrBaIihDoQi5iNjFAAQhPAAguAmQguAlAAA+QAAA/A4AkQA5AlDIAyQDLAxBwBjQBvBkAAC/QAADAiPB3QiPB2jqAAQlRAAkOj6g");
	this.shape_22.setTransform(-1210.9,105.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1}]}).to({state:[{t:this.shape_2}]},3).to({state:[{t:this.shape_3}]},3).to({state:[{t:this.shape_4}]},3).to({state:[{t:this.shape_5}]},3).to({state:[{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6}]},3).to({state:[{t:this.shape_5}]},80).to({state:[{t:this.shape_4}]},3).to({state:[{t:this.shape_3}]},3).to({state:[{t:this.shape_2}]},3).to({state:[{t:this.shape_1}]},4).to({state:[]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-783.2,-62.6,942.7,46.6);


(lib.txt_etbienplus = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_109 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(109).call(this.frame_109).wait(1));

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("EgimAPRIAA+hMBFNAAAIAAehg");
	this.shape.setTransform(-181.2,198.9);
	this.shape._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(14).to({_off:false},0).to({_off:true},4).wait(75).to({_off:false},0).to({_off:true},4).wait(13));

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("EhJoADpIAAnRMCTRAAAIAAHRg");
	this.shape_1.setTransform(-311.9,-39.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("EgupAFkIAArHMBdTAAAIAALHg");
	this.shape_2.setTransform(163.8,243);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("EgoIAPRIAA+hMBQRAAAIAAehg");
	this.shape_3.setTransform(223.7,109.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("Eg1UAPRIAA+hMBqpAAAIAAehg");
	this.shape_4.setTransform(139.3,109.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("Ehy5APRIAA+hMDlyAAAIAAehg");
	this.shape_5.setTransform(29.2,109.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AozHlIC9joQDmDKDCAAQBXAAAygmQAzgmAAhAQAAhAg2glQg1gmicgnQj3g7hzhdQh0hfAAjKQAAjLCShuQCRhtDZAAQCPAACOAwQCOAxBrBaIihDoQi5iNjFAAQhPAAguAmQguAlAAA+QAAA/A4AkQA5AlDIAyQDLAxBwBjQBvBkAAC/QAADAiPB3QiPB2jqAAQlRAAkOj6g");
	this.shape_6.setTransform(643.2,105.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("Am0InQisinAAkzIAAsaIE9AAIAAMQQAACrBPBjQBQBjCFAAQCGAABPhjQBOhjAAirIAAsQIE9AAIAAMaQAAE2irClQirCmkKAAQkJAAising");
	this.shape_7.setTransform(503.1,106.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AnNLHIAA2NIE+AAIAARyIJdAAIAAEbg");
	this.shape_8.setTransform(374.7,105.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("Ao0LHIAA2NIH3AAQFHAACVB/QCWB/AAEIQAAEGiaB7QiZB6k7AAIi9AAIAAGMgAj2AoIDWAAQCdAAA5g/QA5hBAAh7QAAh7hLg0QhJgzicAAIi1AAg");
	this.shape_9.setTransform(247.2,105.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AFTLHIqkt6IAAN6Ik+AAIAA2NIEpAAIK5OTIAAuTIE9AAIAAWNg");
	this.shape_10.setTransform(36.4,105.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AoLLHIAA2NIQAAAIAAEbIrCAAIAAElIJ8AAIAAENIp8AAIAAEnILZAAIAAEZg");
	this.shape_11.setTransform(-109.9,105.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AieLHIAA2NIE9AAIAAWNg");
	this.shape_12.setTransform(-213.2,105.9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("Ao3LHIAA2NIIqAAQCQAABoAiQBpAjA1A7QBfBuAACKQAACnhrBRQglAdgPAHIgyAYQCEAbBOBaQBPBcAACGQAACUhmByQh2CEklAAgAj5G7IDaAAQCHAABEgiQBEgiAAhiQAAhjhJgfQhHggieAAIi7AAgAj5iGICWAAQCDAABBgdQA/gcAAhfQAAhfg6gdQg7gfiMAAIiYAAg");
	this.shape_13.setTransform(-312.6,105.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AidLHIAAx6ImUAAIAAkTIRjAAIAAETImTAAIAAR6g");
	this.shape_14.setTransform(-502.4,105.9);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AoLLHIAA2NIQAAAIAAEbIrCAAIAAElIJ8AAIAAENIp8AAIAAEnILZAAIAAEZg");
	this.shape_15.setTransform(-627.4,105.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1}]}).to({state:[{t:this.shape_2}]},3).to({state:[{t:this.shape_3}]},3).to({state:[{t:this.shape_4}]},3).to({state:[{t:this.shape_5}]},3).to({state:[{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6}]},3).to({state:[{t:this.shape_5}]},80).to({state:[{t:this.shape_4}]},3).to({state:[{t:this.shape_3}]},3).to({state:[{t:this.shape_2}]},3).to({state:[{t:this.shape_1}]},4).to({state:[]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-783.2,-62.6,942.7,46.6);


(lib.txt_danger = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_69 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(69).call(this.frame_69).wait(1));

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("EgimAPRIAA+hMBFNAAAIAAehg");
	this.shape.setTransform(-181.2,198.9);
	this.shape._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(14).to({_off:false},0).to({_off:true},4).wait(35).to({_off:false},0).to({_off:true},4).wait(13));

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("EhJoADpIAAnRMCTRAAAIAAHRg");
	this.shape_1.setTransform(-311.9,-39.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("EgupAFkIAArHMBdTAAAIAALHg");
	this.shape_2.setTransform(163.8,243);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("EgoIAPRIAA+hMBQRAAAIAAehg");
	this.shape_3.setTransform(223.7,109.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("Eg1UAPRIAA+hMBqpAAAIAAehg");
	this.shape_4.setTransform(139.3,109.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("EhJoAPRIAA+hMCTRAAAIAAehg");
	this.shape_5.setTransform(9.2,109.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("ADnLHIk6nGIjbAAIAAHGIk+AAIAA2NIIbAAQFLAACNBwQCNBwAAD4QAAFVkOBkIFoH8gAkugQIDnAAQCiAAA6g1QA8g1AAhzQAAhzg9grQg9gqiZAAIjsAAg");
	this.shape_6.setTransform(391.6,105.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AoLLHIAA2NIQAAAIAAEbIrCAAIAAElIJ7AAIAAENIp7AAIAAEnILZAAIAAEZg");
	this.shape_7.setTransform(248.8,105.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AnBIPQjUjOAAk9QAAk7DYjUQDZjTEyAAQEyAADhDIIilDvQhfhUhQgfQhRgfhdAAQi0AAh8B7Qh8B6AADJQAADKB3B5QB3B5CjAAQClAABuhAIAAmFIE/AAIAAH4QjUDrl1AAQk5AAjVjQg");
	this.shape_8.setTransform(101.8,105.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AFTLHIqkt6IAAN6Ik+AAIAA2NIEpAAIK5OTIAAuTIE9AAIAAWNg");
	this.shape_9.setTransform(-56.9,105.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AGuLHIiEkzIpTAAIiEEzIlSAAIJm2NIEzAAIJmWNgAixB9IFjAAIixmcg");
	this.shape_10.setTransform(-215.7,105.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AqELHIAA2NIH2AAQF8AADMC6QDLC6AAFMQAAFLjGDBQjHDBmWAAgAlGGuIDIAAQDaAAB0htQB1huAAjSQAAjRh1hwQh0hxjyAAIiwAAg");
	this.shape_11.setTransform(-366,105.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1}]}).to({state:[{t:this.shape_2}]},3).to({state:[{t:this.shape_3}]},3).to({state:[{t:this.shape_4}]},3).to({state:[{t:this.shape_5}]},3).to({state:[{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6}]},3).to({state:[{t:this.shape_5}]},40).to({state:[{t:this.shape_4}]},3).to({state:[{t:this.shape_3}]},3).to({state:[{t:this.shape_2}]},3).to({state:[{t:this.shape_1}]},4).to({state:[]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-783.2,-62.6,942.7,46.6);


(lib.Tween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgEAFQAAgBgBAAQAAgBAAAAQgBgBAAgBQAAAAAAgBQAAAAAAAAQAAgBABgBQAAAAAAgBQABAAAAAAQACgCACAAQABAAAAAAQABAAABAAQAAAAABABQAAAAABABQAAAAABAAQAAABAAAAQABABAAABQAAAAAAAAQAAABAAAAQAAABgBABQAAAAAAABQgBAAAAABQgBAAAAAAQgBABAAAAQgBAAgBAAQAAABgBAAQgCgBgCgBg");
	this.shape.setTransform(68.2,2.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgEAFQAAgBgBAAQAAgBAAAAQgBgBAAgBQAAAAAAgBQAAAAAAAAQAAgBABgBQAAAAAAgBQABAAAAAAQACgCACAAQABAAAAAAQABAAABAAQAAAAABABQAAAAABABQAAAAABAAQAAABAAAAQAAABABABQAAAAAAAAQAAABAAAAQgBABAAABQAAAAAAABQgBAAAAABQgBAAAAAAQgBABAAAAQgBAAgBAAQAAABgBAAQgCgBgCgBg");
	this.shape_1.setTransform(66,2.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgEAFQAAgBgBAAQAAgBAAAAQAAgBgBgBQAAAAAAgBQAAAAAAAAQABgBAAgBQAAAAAAgBQABAAAAAAQACgCACAAQABAAAAAAQABAAABAAQAAAAABABQAAAAABABQAAAAABAAQAAABAAAAQABABAAABQAAAAAAAAQAAABAAAAQAAABgBABQAAAAAAABQgBAAAAABQgBAAAAAAQgBABAAAAQgBAAgBAAQAAABgBAAQgCgBgCgBg");
	this.shape_2.setTransform(63.8,2.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgTAaIAAgzIAlAAIAAAKIgZAAIAAALIAXAAIAAAJIgXAAIAAALIAaAAIAAAKg");
	this.shape_3.setTransform(60.5,0);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAJAaIgLgRIgIAAIAAARIgMAAIAAgzIAUAAQALAAAGAEQAEAEAAAJQABAMgKADIANATgAgKAAIAJAAQAEAAADgCQACgCAAgEQAAgEgCgCQgDgBgEAAIgJAAg");
	this.shape_4.setTransform(55.3,0);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgFAaIAAgpIgPAAIAAgKIApAAIAAAKIgPAAIAAApg");
	this.shape_5.setTransform(50,0);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgTAjIAAg0IAmAAIAAALIgZAAIAAAJIAWAAIAAALIgWAAIAAALIAaAAIAAAKgAAEgWIgFgGIgGAGIgJAAIAJgMIAMAAIAJAMg");
	this.shape_6.setTransform(45.5,-0.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgMAFIAAgJIAaAAIAAAJg");
	this.shape_7.setTransform(41,0.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgFAaIAAgpIgPAAIAAgKIApAAIAAAKIgPAAIAAApg");
	this.shape_8.setTransform(36.8,0);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgPAUQgHgGAAgLIAAgdIAMAAIAAAcQAAAHADADQADAEAEAAQAFAAADgEQADgDAAgHIAAgcIAMAAIAAAdQAAAMgHAFQgGAHgKAAQgJAAgGgHg");
	this.shape_9.setTransform(31.7,0.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgTAaIAAgzIAmAAIAAAKIgZAAIAAALIAWAAIAAAJIgWAAIAAALIAaAAIAAAKg");
	this.shape_10.setTransform(26.6,0);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgUAaIAAgzIASAAQAMAAAFAEQAGAFAAAKQAAAJgGAEQgGAFgLAAIgGAAIAAAOgAgIABIAHAAQAGAAABgBQADgCAAgFQAAgFgDgBQgDgCgFAAIgGAAg");
	this.shape_11.setTransform(21.6,0);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgUASIAHgJQAJAIAGAAIAFgBQABgBAAAAQAAgBABAAQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQAAAAgBgBIgHgDQgJgCgEgDQgEgDgBgHQAAgIAGgEQAFgEAIAAQAFAAAFACQAFABAEAEIgGAIQgGgFgIAAQAAAAAAAAQgBAAgBABQAAAAgBAAQAAAAgBABQAAAAAAAAQgBABAAAAQAAABAAAAQAAABAAAAQAAABAAAAQAAABAAAAQAAABABAAQAAABAAAAIAJADQAIACAEADQAEADAAAHQAAAIgGAEQgFAEgIAAQgMAAgKgJg");
	this.shape_12.setTransform(14.4,0);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgFAaIAAgzIALAAIAAAzg");
	this.shape_13.setTransform(10.9,0);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgTATQgIgHAAgMQAAgLAIgHQAIgIALAAQAMAAAIAIQAIAHAAALQAAAMgIAHQgIAIgMAAQgLAAgIgIgAgKgLQgFAFAAAGQAAAHAFAFQAEAFAGAAQAHAAAEgFQAFgFAAgHQAAgGgFgFQgEgFgHAAQgGAAgEAFg");
	this.shape_14.setTransform(6.5,0);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgRAaIAAgzIAjAAIAAAKIgXAAIAAALIAWAAIAAAKIgWAAIAAAUg");
	this.shape_15.setTransform(1.2,0);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgSAaIAAgzIAlAAIAAAKIgaAAIAAALIAXAAIAAAJIgXAAIAAALIAaAAIAAAKg");
	this.shape_16.setTransform(-5.5,0);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AANAaIgYggIAAAgIgMAAIAAgzIALAAIAZAgIAAggIALAAIAAAzg");
	this.shape_17.setTransform(-11.3,0);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgFAaIAAgzIALAAIAAAzg");
	this.shape_18.setTransform(-15.5,0);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AAQAaIgFgLIgVAAIgFALIgMAAIAWgzIALAAIAXAzgAgGAFIAMAAIgGgPg");
	this.shape_19.setTransform(-19.6,0);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AAKAaIAAgUIgTAAIAAAUIgMAAIAAgzIAMAAIAAAWIATAAIAAgWIAMAAIAAAzg");
	this.shape_20.setTransform(-25.3,0);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgQAUQgIgIAAgMQABgKAHgIQAJgIAKAAQANAAAJAKIgIAIQgFgHgIAAQgGAAgFAEQgFAFABAGQgBAHAFAFQAEAEAFAAQAJAAAGgHIAIAIQgKAKgMAAQgLAAgIgHg");
	this.shape_21.setTransform(-30.9,0);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgTATQgIgHAAgMQAAgLAIgHQAIgIALAAQAMAAAIAIQAIAHAAALQAAAMgIAHQgIAIgMAAQgLAAgIgIgAgKgLQgFAFAAAGQAAAHAFAFQAEAFAGAAQAHAAAEgFQAFgFAAgHQAAgGgFgFQgEgFgHAAQgGAAgEAFg");
	this.shape_22.setTransform(-36.8,0);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AAJAaIgLgRIgIAAIAAARIgMAAIAAgzIAUAAQAMAAAEAEQAGAEgBAJQABAMgKADIANATgAgKAAIAJAAQAEAAACgCQADgCAAgEQAAgEgDgCQgCgBgEAAIgJAAg");
	this.shape_23.setTransform(-42.4,0);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgUAaIAAgzIATAAQAKAAAHAEQAFAFAAAKQAAAJgGAEQgFAFgLAAIgHAAIAAAOgAgIABIAIAAQAFAAACgBQACgCAAgFQAAgFgDgBQgDgCgEAAIgHAAg");
	this.shape_24.setTransform(-47.7,0);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgTAaIAAgzIAmAAIAAAKIgZAAIAAALIAWAAIAAAJIgWAAIAAALIAaAAIAAAKg");
	this.shape_25.setTransform(-54.8,0);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AANAaIgYggIAAAgIgMAAIAAgzIALAAIAZAgIAAggIALAAIAAAzg");
	this.shape_26.setTransform(-60.5,0);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgPAUQgHgGAAgLIAAgdIAMAAIAAAcQAAAHADADQADAEAEAAQAFAAADgEQADgDAAgHIAAgcIAMAAIAAAdQAAAMgHAFQgGAHgKAAQgJAAgGgHg");
	this.shape_27.setTransform(-66.4,0.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-69.8,-5.1,139.6,10.2);


(lib.Tween1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgEAFQAAgBgBAAQAAgBAAAAQgBgBAAgBQAAAAAAgBQAAAAAAAAQAAgBABgBQAAAAAAgBQABAAAAAAQACgCACAAQABAAAAAAQABAAABAAQAAAAABABQAAAAABABQAAAAABAAQAAABAAAAQABABAAABQAAAAAAAAQAAABAAAAQAAABgBABQAAAAAAABQgBAAAAABQgBAAAAAAQgBABAAAAQgBAAgBAAQAAABgBAAQgCgBgCgBg");
	this.shape.setTransform(68.2,2.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgEAFQAAgBgBAAQAAgBAAAAQgBgBAAgBQAAAAAAgBQAAAAAAAAQAAgBABgBQAAAAAAgBQABAAAAAAQACgCACAAQABAAAAAAQABAAABAAQAAAAABABQAAAAABABQAAAAABAAQAAABAAAAQAAABABABQAAAAAAAAQAAABAAAAQgBABAAABQAAAAAAABQgBAAAAABQgBAAAAAAQgBABAAAAQgBAAgBAAQAAABgBAAQgCgBgCgBg");
	this.shape_1.setTransform(66,2.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgEAFQAAgBgBAAQAAgBAAAAQAAgBgBgBQAAAAAAgBQAAAAAAAAQABgBAAgBQAAAAAAgBQABAAAAAAQACgCACAAQABAAAAAAQABAAABAAQAAAAABABQAAAAABABQAAAAABAAQAAABAAAAQABABAAABQAAAAAAAAQAAABAAAAQAAABgBABQAAAAAAABQgBAAAAABQgBAAAAAAQgBABAAAAQgBAAgBAAQAAABgBAAQgCgBgCgBg");
	this.shape_2.setTransform(63.8,2.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgTAaIAAgzIAlAAIAAAKIgZAAIAAALIAXAAIAAAJIgXAAIAAALIAaAAIAAAKg");
	this.shape_3.setTransform(60.5,0);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAJAaIgLgRIgIAAIAAARIgMAAIAAgzIAUAAQALAAAGAEQAEAEAAAJQABAMgKADIANATgAgKAAIAJAAQAEAAADgCQACgCAAgEQAAgEgCgCQgDgBgEAAIgJAAg");
	this.shape_4.setTransform(55.3,0);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgFAaIAAgpIgPAAIAAgKIApAAIAAAKIgPAAIAAApg");
	this.shape_5.setTransform(50,0);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgTAjIAAg0IAmAAIAAALIgZAAIAAAJIAWAAIAAALIgWAAIAAALIAaAAIAAAKgAAEgWIgFgGIgGAGIgJAAIAJgMIAMAAIAJAMg");
	this.shape_6.setTransform(45.5,-0.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgMAFIAAgJIAaAAIAAAJg");
	this.shape_7.setTransform(41,0.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgFAaIAAgpIgPAAIAAgKIApAAIAAAKIgPAAIAAApg");
	this.shape_8.setTransform(36.8,0);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgPAUQgHgGAAgLIAAgdIAMAAIAAAcQAAAHADADQADAEAEAAQAFAAADgEQADgDAAgHIAAgcIAMAAIAAAdQAAAMgHAFQgGAHgKAAQgJAAgGgHg");
	this.shape_9.setTransform(31.7,0.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgTAaIAAgzIAmAAIAAAKIgZAAIAAALIAWAAIAAAJIgWAAIAAALIAaAAIAAAKg");
	this.shape_10.setTransform(26.6,0);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgUAaIAAgzIASAAQAMAAAFAEQAGAFAAAKQAAAJgGAEQgGAFgLAAIgGAAIAAAOgAgIABIAHAAQAGAAABgBQADgCAAgFQAAgFgDgBQgDgCgFAAIgGAAg");
	this.shape_11.setTransform(21.6,0);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgUASIAHgJQAJAIAGAAIAFgBQABgBAAAAQAAgBABAAQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQAAAAgBgBIgHgDQgJgCgEgDQgEgDgBgHQAAgIAGgEQAFgEAIAAQAFAAAFACQAFABAEAEIgGAIQgGgFgIAAQAAAAAAAAQgBAAgBABQAAAAgBAAQAAAAgBABQAAAAAAAAQgBABAAAAQAAABAAAAQAAABAAAAQAAABAAAAQAAABAAAAQAAABABAAQAAABAAAAIAJADQAIACAEADQAEADAAAHQAAAIgGAEQgFAEgIAAQgMAAgKgJg");
	this.shape_12.setTransform(14.4,0);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgFAaIAAgzIALAAIAAAzg");
	this.shape_13.setTransform(10.9,0);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgTATQgIgHAAgMQAAgLAIgHQAIgIALAAQAMAAAIAIQAIAHAAALQAAAMgIAHQgIAIgMAAQgLAAgIgIgAgKgLQgFAFAAAGQAAAHAFAFQAEAFAGAAQAHAAAEgFQAFgFAAgHQAAgGgFgFQgEgFgHAAQgGAAgEAFg");
	this.shape_14.setTransform(6.5,0);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgRAaIAAgzIAjAAIAAAKIgXAAIAAALIAWAAIAAAKIgWAAIAAAUg");
	this.shape_15.setTransform(1.2,0);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgSAaIAAgzIAlAAIAAAKIgaAAIAAALIAXAAIAAAJIgXAAIAAALIAaAAIAAAKg");
	this.shape_16.setTransform(-5.5,0);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AANAaIgYggIAAAgIgMAAIAAgzIALAAIAZAgIAAggIALAAIAAAzg");
	this.shape_17.setTransform(-11.3,0);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgFAaIAAgzIALAAIAAAzg");
	this.shape_18.setTransform(-15.5,0);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AAQAaIgFgLIgVAAIgFALIgMAAIAWgzIALAAIAXAzgAgGAFIAMAAIgGgPg");
	this.shape_19.setTransform(-19.6,0);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AAKAaIAAgUIgTAAIAAAUIgMAAIAAgzIAMAAIAAAWIATAAIAAgWIAMAAIAAAzg");
	this.shape_20.setTransform(-25.3,0);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgQAUQgIgIAAgMQABgKAHgIQAJgIAKAAQANAAAJAKIgIAIQgFgHgIAAQgGAAgFAEQgFAFABAGQgBAHAFAFQAEAEAFAAQAJAAAGgHIAIAIQgKAKgMAAQgLAAgIgHg");
	this.shape_21.setTransform(-30.9,0);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgTATQgIgHAAgMQAAgLAIgHQAIgIALAAQAMAAAIAIQAIAHAAALQAAAMgIAHQgIAIgMAAQgLAAgIgIgAgKgLQgFAFAAAGQAAAHAFAFQAEAFAGAAQAHAAAEgFQAFgFAAgHQAAgGgFgFQgEgFgHAAQgGAAgEAFg");
	this.shape_22.setTransform(-36.8,0);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AAJAaIgLgRIgIAAIAAARIgMAAIAAgzIAUAAQAMAAAEAEQAGAEgBAJQABAMgKADIANATgAgKAAIAJAAQAEAAACgCQADgCAAgEQAAgEgDgCQgCgBgEAAIgJAAg");
	this.shape_23.setTransform(-42.4,0);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgUAaIAAgzIATAAQAKAAAHAEQAFAFAAAKQAAAJgGAEQgFAFgLAAIgHAAIAAAOgAgIABIAIAAQAFAAACgBQACgCAAgFQAAgFgDgBQgDgCgEAAIgHAAg");
	this.shape_24.setTransform(-47.7,0);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgTAaIAAgzIAmAAIAAAKIgZAAIAAALIAWAAIAAAJIgWAAIAAALIAaAAIAAAKg");
	this.shape_25.setTransform(-54.8,0);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AANAaIgYggIAAAgIgMAAIAAgzIALAAIAZAgIAAggIALAAIAAAzg");
	this.shape_26.setTransform(-60.5,0);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgPAUQgHgGAAgLIAAgdIAMAAIAAAcQAAAHADADQADAEAEAAQAFAAADgEQADgDAAgHIAAgcIAMAAIAAAdQAAAMgHAFQgGAHgKAAQgJAAgGgHg");
	this.shape_27.setTransform(-66.4,0.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-69.8,-5.1,139.6,10.2);


(lib.Scene_1_Layer_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AmTH9QifiaAAkbIAArfIElAAIAALVQAACeBKBbQBJBcB7AAQB8AABIhcQBJhbAAieIAArVIElAAIAALfQAAEeieCYQieCaj2AAQj1AAifibg");
	this.shape.setTransform(416.8,598);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AoJKRIAA0hIHRAAQEuAACJB1QCLB2AAD0QAADyiOBxQiOBxkjAAIiuAAIAAFugAjjAlIDFAAQCRAAA1g6QA1g8AAhxQAAhyhFgwQhEgwiQAAIinAAg");
	this.shape_1.setTransform(371.9,364.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AoLKRIAA0hIH/AAQCFAABgAgQBhAgAxA2QBYBmAACAQAACahjBLQgiAbgNAGIgvAWQB5AZBJBUQBIBUAAB8QAACJhdBpQhtB6kPAAgAjmGZIDJAAQB9AABAgfQA9gfABhbQAAhchEgcQhCgdiRAAIitAAgAjmh8ICLAAQB5AAA8gaQA6gaABhYQAAhXg3gcQg2gciBAAIiNAAg");
	this.shape_2.setTransform(576.1,364.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AlOB9IAAj5IKdAAIAAD5g");
	this.shape_3.setTransform(461.4,373.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AoLKRIAA0hIH/AAQCFAABgAgQBiAgAwA2QBYBmAACAQAACahjBLQgiAbgNAGIgwAWQB6AZBJBUQBIBUAAB8QAACJhdBpQhtB6kPAAgAjmGZIDJAAQB9AABAgfQA9gfAAhbQAAhchDgcQhBgdiSAAIitAAgAjmh8ICLAAQB5AAA8gaQA6gaAAhYQAAhXg2gcQg2gciBAAIiNAAg");
	this.shape_4.setTransform(640.2,364.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AhpE8IANieIiGBcIhgiqICehPIighOIBiioICGBYIgNieIDTAAIgNCeICGhYIBiCoIigBOICeBPIhgCqIiGhcIANCeg");
	this.shape_5.setTransform(387.5,330.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AlOB9IAAj5IKdAAIAAD5g");
	this.shape_6.setTransform(79.2,373.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AoLKRIAA0hIH/AAQCFAABgAgQBhAgAxA2QBYBmAACAQAACahjBLQgiAbgNAGIgvAWQB5AZBJBUQBIBUAAB8QAACJhdBpQhtB6kPAAgAjmGZIDJAAQB9AABAgfQA9gfABhbQAAhchEgcQhCgdiRAAIitAAgAjmh8ICLAAQB5AAA8gaQA6gaAAhYQAAhXg2gcQg2gciBAAIiNAAg");
	this.shape_7.setTransform(341.1,364.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1,p:{x:371.9}},{t:this.shape,p:{x:416.8,y:598}}]}).to({state:[{t:this.shape_1,p:{x:165.6}},{t:this.shape,p:{x:295.1,y:364.9}},{t:this.shape_3,p:{x:461.4}},{t:this.shape_2}]},7).to({state:[{t:this.shape_6},{t:this.shape_3,p:{x:171.3}},{t:this.shape_1,p:{x:285.7}},{t:this.shape_5},{t:this.shape,p:{x:501,y:364.9}},{t:this.shape_4}]},7).to({state:[{t:this.shape_1,p:{x:72.3}},{t:this.shape,p:{x:201.8,y:364.9}},{t:this.shape_7}]},9).to({state:[]},9).wait(467));

}).prototype = p = new cjs.MovieClip();


(lib.masque = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#333333").s().p("EgpfApfMAAAhS+MBS/AAAMAAABS+g");
	this.shape.setTransform(0,265.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.masque, new cjs.Rectangle(-265.5,0,531.1,531.1), null);


(lib.ligne = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(49));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#333333").ss(2,1,1).p("EgABhM/MAADCZ/");
	this.shape.setTransform(-0.2,319.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#333333").ss(2,1,1).p("EAAghM7Qg1OzgbRBQgbUYAEVMQADWBAkVVQAlTkBERl");
	this.shape_1.setTransform(-7,319.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#333333").ss(2,1,1).p("EAA7hM5QhfN/gxR5QgxUHAHVhQAGWSA/VCQBDUPB4Qw");
	this.shape_2.setTransform(-12.4,319.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#333333").ss(2,1,1).p("EABQhM3Qh/NXhDSkQhAT5AIVyQAIWfBUU0QBZUwChQG");
	this.shape_3.setTransform(-16.5,320.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#333333").ss(2,1,1).p("EABfhM1QiXM4hPTEQhMTvAKV+QAJWpBjUqQBqVIC+Pn");
	this.shape_4.setTransform(-19.6,320.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#333333").ss(2,1,1).p("EABqhM0QioMjhYTaQhVToALWHQALWwBuUiQB1VaDUPR");
	this.shape_5.setTransform(-21.8,320.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#333333").ss(2,1,1).p("EABxhMzQizMUheTqQhbTjAMWNQALW1B2UdQB9VmDjPB");
	this.shape_6.setTransform(-23.3,320.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#333333").ss(2,1,1).p("EAB2hMzQi7MKhiT1QhfTgAMWRQAMW3B7UaQCDVvDsO3");
	this.shape_7.setTransform(-24.2,320.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#333333").ss(2,1,1).p("EAB5hMyQjAMEhkT7QhhTeAMWTQAMW5B+UYQCGVzDyOx");
	this.shape_8.setTransform(-24.8,320.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#333333").ss(2,1,1).p("EAB7hMyQjCMBhmT+QhjTdANWVQAMW6CAUWQCHV2D2Ou");
	this.shape_9.setTransform(-25.2,320.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#333333").ss(2,1,1).p("EAB8hMyQjEL/hmUAQhjTcAMWWQANW7CAUWQCIV3D3Os");
	this.shape_10.setTransform(-25.3,320.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#333333").ss(2,1,1).p("EAB8hMyQjEL/hnUAQhjTcANWWQAMW7CAUWQCJV3D4Os");
	this.shape_11.setTransform(-25.4,320.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#333333").ss(2,1,1).p("EAB8hMyQjEL/hnUAQhjTcAMWWQANW7CAUWQCJV3D4Os");
	this.shape_12.setTransform(-25.4,320.5);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#333333").ss(2,1,1).p("EAB8hMyQjEL/hnUAQhjTcAMWWQANW7CAUWQCJV4D4Or");
	this.shape_13.setTransform(-25.4,320.5);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#333333").ss(2,1,1).p("EAB8hMyQjEL/hmUAQhkTcANWWQAMW7CBUWQCIV3D4Os");
	this.shape_14.setTransform(-25.4,320.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#333333").ss(2,1,1).p("EAB7hMyQjDMAhmT/QhjTdANWVQAMW6CAUWQCIV3D2Ot");
	this.shape_15.setTransform(-25.2,320.5);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#333333").ss(2,1,1).p("EAB6hMyQjBMChmT9QhiTdAMWVQANW6B/UXQCHV1D1Ou");
	this.shape_16.setTransform(-25.1,320.5);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#333333").ss(2,1,1).p("EAB5hMyQjAMEhkT7QhiTeANWTQAMW5B+UYQCGV0DzOw");
	this.shape_17.setTransform(-24.9,320.5);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#333333").ss(2,1,1).p("EAB4hMzQi+MHhjT5QhgTfAMWRQAMW5B8UZQCFVxDvO0");
	this.shape_18.setTransform(-24.6,320.5);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#333333").ss(2,1,1).p("EAB2hMzQi7MLhhT0QhfTgAMWRQAMW3B6UaQCDVtDrO5");
	this.shape_19.setTransform(-24.2,320.5);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#333333").ss(2,1,1).p("EABzhMzQi2MQhgTuQhcTiALWPQAMW1B4UcQB/VqDnO9");
	this.shape_20.setTransform(-23.7,320.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#333333").ss(2,1,1).p("EABwhMzQiyMWhcToQhbTkAMWMQALWzB1UeQB8VlDgPD");
	this.shape_21.setTransform(-23,320.4);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#333333").ss(2,1,1).p("EABshM0QisMehZTgQhXTmALWJQALWxBwUhQB4VeDZPM");
	this.shape_22.setTransform(-22.2,320.4);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#333333").ss(2,1,1).p("EABohM0QilMnhVTWQhUTpALWFQAKWuBsUlQBzVWDQPV");
	this.shape_23.setTransform(-21.3,320.3);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#333333").ss(2,1,1).p("EABihM1QibMyhSTKQhPTuAKWAQAKWrBnUoQBsVNDFPh");
	this.shape_24.setTransform(-20.2,320.3);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#333333").ss(2,1,1).p("EABchM1QiSM/hMS8QhKTyAKV7QAJWnBgUsQBmVDC4Pt");
	this.shape_25.setTransform(-18.9,320.2);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#333333").ss(2,1,1).p("EABUhM2QiGNNhGStQhET3AJV1QAJWiBYUyQBeU3CpP8");
	this.shape_26.setTransform(-17.4,320.1);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#333333").ss(2,1,1).p("EABMhM3Qh5Neg/ScQg9T8AIVuQAHWdBQU2QBVUqCYQO");
	this.shape_27.setTransform(-15.7,320);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#333333").ss(2,1,1).p("EABChM4QhqNxg3SIQg2UCAHVnQAHWWBGU9QBKUbCGQh");
	this.shape_28.setTransform(-13.8,319.9);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#333333").ss(2,1,1).p("EAA3hM5QhZOGguRxQguUKAGVeQAGWPA7VFQA/UJBxQ3");
	this.shape_29.setTransform(-11.7,319.8);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#333333").ss(2,1,1).p("EAAuhM6QhKOZgnRdQgmUQAFVWQAGWKAxVLQA0T6BfRK");
	this.shape_30.setTransform(-9.8,319.7);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#333333").ss(2,1,1).p("EAAlhM7Qg8OqggRLQgfUWAEVPQAEWFApVQQArTtBORb");
	this.shape_31.setTransform(-8.1,319.7);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#333333").ss(2,1,1).p("EAAehM7QgxO4gaQ8QgZUaADVJQAEWAAhVWQAjTgA/Rq");
	this.shape_32.setTransform(-6.6,319.6);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#333333").ss(2,1,1).p("EAAXhM8QgnPFgUQuQgUUeADVFQADV8AaVaQAcTWAzR3");
	this.shape_33.setTransform(-5.3,319.5);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#333333").ss(2,1,1).p("EAAShM9QgePQgRQjQgPUiACVAQADV4AVVeQAVTNAoSD");
	this.shape_34.setTransform(-4.2,319.5);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#333333").ss(2,1,1).p("EAANhM9QgXPZgMQZQgMUlACU9QACV1AQVhQARTFAeSM");
	this.shape_35.setTransform(-3.2,319.4);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#333333").ss(2,1,1).p("EAAJhM9QgRPggJQRQgJUoACU5QABVyANVkQAMS/AXSU");
	this.shape_36.setTransform(-2.4,319.4);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#333333").ss(2,1,1).p("EAAGhM+QgMPogGQKQgHUpACU3QABVwAJVmQAJS6ARSb");
	this.shape_37.setTransform(-1.8,319.3);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#333333").ss(2,1,1).p("EAADhM+QgIPsgEQFQgEUrABU1QABVuAGVoQAGS2AMSg");
	this.shape_38.setTransform(-1.3,319.3);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#333333").ss(2,1,1).p("EAABhM+QgFPwgCQAQgDUtABUzQABVtAEVqQAESyAISk");
	this.shape_39.setTransform(-0.9,319.3);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#333333").ss(2,1,1).p("EAAAhM+QgCPzgCP9QgBUuAAUxQABVtADVqQACSwAFSn");
	this.shape_40.setTransform(-0.6,319.3);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#333333").ss(2,1,1).p("EAAAhM+QgCP1gBP7QAAUuAAUxQABVsACVrQABSuADSp");
	this.shape_41.setTransform(-0.4,319.3);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#333333").ss(2,1,1).p("EgABhM/QgBP3AAP5QAAUvABUxQAAVsABVrQABStACSr");
	this.shape_42.setTransform(-0.2,319.3);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#333333").ss(2,1,1).p("EgABhM/QAAP4AAP4QAAUwABUvQAAVsAAVrQABStABSs");
	this.shape_43.setTransform(-0.2,319.3);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#333333").ss(2,1,1).p("EgABhM/QABP4AAP4QAAUwAAUvQAAVsABVrQABStAASs");
	this.shape_44.setTransform(-0.2,319.3);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#333333").ss(2,1,1).p("EgABhM/QABP4AAP4QAAUwAAUvQABVsAAVrQABStAASs");
	this.shape_45.setTransform(-0.2,319.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.3,-174.5,2.4,987.5);


(lib.img_danger = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.danger();
	this.instance.parent = this;
	this.instance.setTransform(-369,-41,0.674,0.674);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.img_danger, new cjs.Rectangle(-369,-41,727.9,727.9), null);


(lib.img_action = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.action();
	this.instance.parent = this;
	this.instance.setTransform(-334,118,1.066,1.066);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.img_action, new cjs.Rectangle(-334,118,682.1,357.1), null);


(lib.bg_btn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#CC9933").s().p("AsqC2IAAlrIZVAAIAAFrg");
	this.shape.setTransform(0,18.2);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.bg_btn, new cjs.Rectangle(-81.1,0,162.3,36.4), null);


(lib._200_gif = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.instance = new lib.fire_0000_Layer8();
	this.instance.parent = this;
	this.instance.setTransform(199,21);

	this.instance_1 = new lib.fire_0001_Layer9();
	this.instance_1.parent = this;
	this.instance_1.setTransform(199,21);

	this.instance_2 = new lib.fire_0002_Layer10();
	this.instance_2.parent = this;
	this.instance_2.setTransform(199,21);

	this.instance_3 = new lib.fire_0003_Layer11();
	this.instance_3.parent = this;
	this.instance_3.setTransform(199,21);

	this.instance_4 = new lib.fire_0004_Layer12();
	this.instance_4.parent = this;
	this.instance_4.setTransform(199,21);

	this.instance_5 = new lib.fire_0005_Layer13();
	this.instance_5.parent = this;
	this.instance_5.setTransform(199,21);

	this.instance_6 = new lib.fire_0006_Layer14();
	this.instance_6.parent = this;
	this.instance_6.setTransform(199,21);

	this.instance_7 = new lib.fire_0007_Layer15();
	this.instance_7.parent = this;
	this.instance_7.setTransform(199,21);

	this.instance_8 = new lib.fire_0008_Layer16();
	this.instance_8.parent = this;
	this.instance_8.setTransform(199,21);

	this.instance_9 = new lib.fire_0009_Layer17();
	this.instance_9.parent = this;
	this.instance_9.setTransform(199,21);

	this.instance_10 = new lib.fire_0010_Layer1();
	this.instance_10.parent = this;
	this.instance_10.setTransform(199,21);

	this.instance_11 = new lib.fire_0011_Layer2();
	this.instance_11.parent = this;
	this.instance_11.setTransform(199,21);

	this.instance_12 = new lib.fire_0012_Layer3();
	this.instance_12.parent = this;
	this.instance_12.setTransform(199,21);

	this.instance_13 = new lib.fire_0013_Layer4();
	this.instance_13.parent = this;
	this.instance_13.setTransform(199,21);

	this.instance_14 = new lib.fire_0014_Layer5();
	this.instance_14.parent = this;
	this.instance_14.setTransform(199,21);

	this.instance_15 = new lib.fire_0015_Layer6();
	this.instance_15.parent = this;
	this.instance_15.setTransform(199,21);

	this.instance_16 = new lib.fire_0016_Layer7();
	this.instance_16.parent = this;
	this.instance_16.setTransform(199,21);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},4).to({state:[{t:this.instance_2}]},3).to({state:[{t:this.instance_3}]},3).to({state:[{t:this.instance_4}]},3).to({state:[{t:this.instance_5}]},3).to({state:[{t:this.instance_6}]},3).to({state:[{t:this.instance_7}]},3).to({state:[{t:this.instance_8}]},3).to({state:[{t:this.instance_9}]},3).to({state:[{t:this.instance_10}]},3).to({state:[{t:this.instance_11}]},3).to({state:[{t:this.instance_12}]},3).to({state:[{t:this.instance_13}]},3).to({state:[{t:this.instance_14}]},3).to({state:[{t:this.instance_15}]},3).to({state:[{t:this.instance_16}]},3).to({state:[]},3).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(199,21,50,79);


(lib.vignette3_danger = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_92 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(92).call(this.frame_92).wait(1));

	// Layer_2
	this.instance = new lib.txt_danger();
	this.instance.parent = this;
	this.instance.setTransform(-40.8,1492.6,0.15,0.15,0,0,0,0,255.1);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(2).to({_off:false},0).to({_off:true},90).wait(1));

	// Layer_5 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	var mask_1_graphics_6 = new cjs.Graphics().p("EAQoA0HIhTh5IG9AAIBVB5gEABbA0HIAAh5IFrAAIAAB5gEgWVA0HIAAh5ISuAAIAAB5gEgyMAyOIgng3InpAAIAAA3IlqAAIAAg3IgFAAIAAgeISuAAIAAAeIBmAAIAnA3gEhX3AyOIAAg3IStAAIAAA3gEAhRAxXIgUgeIG8AAIAVAegEgoaAxXIAAgeIFrAAIAAAegEhIKAw5Ig6hUIFGAAIAAhkIAuAAICCC4gEhVJAw5IAAhUIgeAAIhGhkIGMAAIAABkIBDAAIAABUgEhs6Aw5IAAhUIsjAAIAAhkIFrAAIAABFINCAAIAAAfIFDAAIAAhkIFrAAIAABkIB2AAIAABUgEgHkAuBIAAhAIAtBAgEgaTAuBIgyhHIG4AAIAFAHIAABAgEglUAuBIAAhHIFsAAIAABHgEg9EAuBIAAhHIFrAAIAABHgEhyDAstIAAhxIFrAAIAABxgEiC3ArnIAAgrILPAAIAAArgEAExAqQIAAg5IvvAAIAAg4INwAAIAAgrIsuAAIAAhjIlkAAIAABjIs7AAIAAhjIgwAAIAAhVIgSAAQAJgNAGgPIiiAAIAAAcIkeAAIAAiAIFsAAIAABYILXAAIAAAMIkFAAIgEAcIGvAAIAABKIGuAAIAAhKIFrAAIAABVIBzAAIAAhVIEbAAIAABVQBPgPAngjQARgPAOgUIGHAAQgJAtgPAoIjAAAIAABjIGuAAIAAhjIFrAAIAAAKIEJAAQBOAAA5gKIIDAAQgUA2gfAtIFVAAIAAArI1lAAIAAgrIhBAAIAAArIoFAAQhCBIhmApgEgivApXIAAg4IRDAAIAAAqIrYAAIAAAOgEg1PApXIAAg4IM7AAIAAA4gEAtCAofIAAgrIFgAAIAAArgEgrsAk8IAAgcIFrAAIAAAcgEg+/Ak8IAAgcIMlAAIAAAcgEAB/AkgQATgqADg6IFyAAQgBA1gIAvgEgMdAkgIAAhkIFrAAIAABkgEhLQAi8IAAgqIFrAAIAAAqgAfrdaIAAg5IklAAIAAhjISTAAIAABjIE5AAQAaAaAVAfgAH7daIAAg5ISTAAIAAA5gEAy2AchIAAhjIQKAAQBJAcAyAoQASAPARAQgA/Ea+IAAg8IJoAAQEFAACdA8gEg20Aa+IAAg8ISSAAIAAA8gEgtwgZ2IAAjMITNAAIgQAOQjjC+muAAgEBUygdsQgbgcgYgdIU0AAIAAAiIgWAXgEBDdgdsIgrg5IJRAAIAAA5gEA0ngdsIAAg5IFrAAIAAA5gEAqAgdsIgZg5IGCAAIAYA5gAX79sIAZg5IGBAAIgYA5gAiW9sIAAg5IkrAAQgYgcgTgdIi0AAIhhh/IjeAAQgWgwgOg0IGJAAQAaA3AnAtIIWAAIAAB/IBCAAQBQAhBbAAQCAAABighQgpg8gdhDIkNAAIAAhkIFsAAIAABkIFHAAIAVAWQBFBHBTAiIhHAAIAAA5IEfAAQgWAdgaAcgA4O+lIgrg5IA6AAIAAh/ImOAAIA3B/IjYAAIAAA5IlqAAIAAg5IlXAAIAYA5ImCAAIgVgwIqoAAIgVAwImCAAIAZg5IAZAAIAigOQAbgMAYgQQAegUAagYQAUgTASgWIpBAAIAwhyIEHAAIAAiqIkYAAIAAjUIj/AAIgfhHIDKAAQghhIg4g1QhahYibgcIJmAAQBJBqAhCHIjMAAIAfBHIDqAAIAADUIB1AAIAAC4IMMAAIArBkIleAAQgRAsgVApIgQAcIgIAOIF1AAIA3h/IF+AAIAAhkIFrAAIAABkIGTAAIhLhkIG4AAIAGAIIAAgIIFrAAIAABkIDVAAIAAB/IDXAAIAAA5gEhdXgelIAAg5IFrAAIAAAnIDmAAQCLAABngnIBWAAIAAh/IlQAAQAngyAYhAIF5AAQgNA8gUA2IElAAIAAB/IBKAAQgTAdgUAcgAVG/eQAwgQApgXIAAhYIFsAAIAAB/gEhkEghdIAAhyIFsAAIAABygEB9LgjPIAAiqIkYAAIAAhKIFsAAIAABKIEYAAIAACqgEhTzgpNIAehHIGCAAIgeBHgEhiEgpNQgJgmgPghIGCAAQAHAiAGAlgEgYCgpqIAAgqIhCAAQgJAUgGAWImAAAIAJgqIjdAAIAAi5IJ6AAICNC5IEIAAIAAAqgEgqUgpqIAAgqIFqAAIAAAqgEg2WgpqIgEgGIAAAGIlrAAIAAgqICOAAIBQi5IKtAAIBQC5IjUAAIAgAqgEgz1gqUIBLAAIgmhYgEgFsgqUQAYhiAvhXIHKAAQgUARgTASQhCBBgjBVgEgQ2gqUIAAi5IFqAAIAAC5gEhZlgqUIAAjxIFqAAIAADxgEAMegtNIB2AAIgqA9QgngiglgbgEBmDguFIgggPQhcgjhrAAQh7AAhkAyIoBAAQAshEA7g8IRzAAIAABaIgaAmgEBKKguFIAAiAIFqAAIAACAgEA4ZguFIAAiAIgoAAIAPgPQAfgeAggaImAAAIAAi4IhBAAQAggCAfAAQE7AADxC6IN8AAIAABHIpeAAIBhCAgEBQ5gwFIA+AAIg+Bag");
	var mask_1_graphics_11 = new cjs.Graphics().p("EgQCA0HIh5iwInpAAIAACwIlrAAIAAiwIgEAAIAAhyIskAAIAAi4IEmAAIAAhxIDQAAIAAidILSAAIAAgrIsuAAIAAhjIllAAIAABjIs7AAIAAhjIgvAAIAAhxIMlAAIAABmIGuAAIAAhmIFrAAIAABxIBzAAIAAjVIgeAAIABgdIgBgbIFxAAIAAAaIAAAeIAYAAIAABYILXAAIAAAMIkEAAQgKA9gUA0Ii/AAIAABjIGuAAIAAhjIFrAAIAAAKIEIAAQBOAAA5gKIIDAAQgUA2gfAtIFVAAIAAArIGuAAIAAgrITLAAQgQAXgSAUIrTAAQhCBIhnApIwAAAIAAhxImtAAIAAAqIrXAAIAABHIlsAAIAAhxIlkAAIAABxIrPAAIAAAsILFAAIgQAGIBMBrIm3AAIgggtIj7AAIAAAtIklAAIAACZINCAAIAAAfIFDAAIAAi4IFrAAIAAC4IB2AAIAAByIBmAAIB8CwgEgLzAofIFlAAIAAgrIllAAgEgZeAmRQBOgPAngjQAdgZAQgmIiiAAgEg2/A0HIAAiwIStAAIAACwgEAFwAxXIhPhyIFHAAIAAikIDEEWgEgHjAxXIAAhyIgeAAIh/i4IG3AAIAOAUIAACkIBEAAIAABygEg/BAstIAAhxIFqAAIAABxgEhP1ArnIAAgrILPAAIAAArgEAISAkgQATgqADg6IFyAAQgCA1gHAvgEgGLAkgIAAhkIFsAAIAABkgEgtNAi8IAAg4IDhAAQgDgxgOgkIF8AAQAFAoACAtIjoAAIAAA4gEhE9Ai8IAAg4IFqAAIAAA4gEg4gAiEIAAhVIjKAAQgSgsgjgYQgNgKgRgHIG9AAQAMAoAFAtIC6AAIAABVgEhQRAiEIAAhVIFsAAIAABVgEhKNAgvIAAhVIFrAAIAABVgEhh+AgvIAAhVIFsAAIAABVgA+sfaQhJggiNAAIkPAAIAAAgIlrAAIAAi5IkmAAIAAifISTAAIAACfIE6AAQBHBJAfBwgEhDtAfaIAAi5ISTAAIAACkIsoAAIAAAVgA4xchIAAifIJnAAQF8AAChCAQASAPAQAQgAV78NQgXgRgXgTIgEgDIgQgOIALAAIg/iTIqpAAIg/CTIC/AAIAADMIlrAAIiajMIg8AAIBEicIAZAAIAhgOQAbgMAYgQQAegUAagYQAUgTATgWIpBAAIB6kcIkXAAIBbjUIh6AAIAAhHIFqAAIAABHICRAAIhcDUIEYAAIgsBmIGWAAIgrhmIkYAAIhbjUIA4AAQAFgkAJgjIjdAAIAAm4IDXAAIAAiDIFrAAIAACDIiLAAIFPG4IijAAQgOAigJAlIg0AAIBbDUIEYAAIB7EcIleAAQgRAsgVApIgQAcIgIAOIF0AAIA3h/IF/AAIAAkcIkYAAIAAjUIFqAAIAADUIC8AAIihjUIG3AAICiDUICSAAIAAjUIFrAAIAADUIEYAAIAAEcIDVAAIAAB/IDXAAIAACcIoFAAIh3icIA7AAIAAh/ImOAAIA3B/IjYAAIAACcICFAAIgQAOQgZAVgaASQiEBcirAkQhtAXh8AAQkfAAjViXgAbG9CIEUAAIAAicIlXAAgEAeYghdIGUAAIjYkcIi8AAgEAqegi5IAAjAIiSAAgAr+52IAAjMIkVAAIBZDMImDAAIhXjMIhPAAIAAicIFrAAIAAAnIDlAAQCMAABmgnIBXAAIAAh/IlRAAQBWhtAJivIkYAAIABgoQAAhfgVhNIlbAAIAAhHICOAAIC+m4IF5AAIAAiDIFTAAIBkCDIleAAIC9G4IjVAAIA2BHIhrAAQANBNAABWIgBAxIEYAAQgHCcgvCAIElAAIAAB/IBKAAQguBMhBA/IgTARIh9AAIAADMgEgRYgpNIAaAAIgagjgEgOzgqUIBLAAIglhYgEgqVgZ2IBYjMIGCAAIhYDMgEhC9gZ2IAAjMITNAAIgQAOQjjC+muAAgEBASgdCIgkghQg7g5gshCIi0AAIhhh/IjfAAQg4iCgKiaIkYAAQgCgfAAghQAAhNAMhHIF8AAQgRBFAABQQAAAhADAeIEYAAQAPCqBhByIIWAAIAAB/IBEAAQBPAhBcAAQB/AABjghQgpg8gehDIkNAAIAAkcIkYAAIAAhKIFtAAIAABKIEYAAIAAEcIFGAAIAVAWQBFBHBTAiIhHAAIAABbQgfAjgjAegEBa6gfeQAvgQApgXIAAhYIFtAAIAAB/gEgeQghdIAAkcIkYAAIAAjUIkAAAIgehHIDJAAQgghIg3g1QiFiCkWAAIjJAAIAAD/IlsAAIAAm4IuZAAIAAiDII+AAQFWAADYCDIOZAAQA8AkAyAuQCbCOA0DYIjLAAIAfBHIDqAAIAADUIEYAAIAAEcgEguxgpNIAfhHIGBAAIgfBHgEg9CgpNQgKgmgPghIGCAAQAIAiAGAlgEhOCgpNIAAhHIFsAAIAABHgEAfVgqUQA2jcCoikQAegeAhgaIobAAQAaAVAZAXIhRB1IAAihIhDAAIAAG4IlrAAIAAm4IBNAAQDki6E0AAQE7AADwC6IN9AAIAAChIhsCbQhthfhcglQhcgjhrAAQjOAAiOCNQhCBBgjBVgEggQgxMIA4iDIFgAAIA5CDg");
	var mask_1_graphics_15 = new cjs.Graphics().p("EAR7A0HIh6iwInoAAIAACwIlsAAIAAiwIgDAAIAAkqIElAAIAAjjImuAAIAAAqIrWAAIAAC5IlrAAIAAjjIllAAIAACdIs8AAIAAidILTAAIAAiOIlMAAIAAjUIEYAAIAAg5IFrAAIAAA5IkYAAIAABYILYAAIAAB8IBFAAIAACOIFlAAIAAiOIADAAIAAjUIEYAAIAAg5IDgAAQgGhtg/gsQhFgxiuAAIkQAAIAADKIlrAAIAAliIklAAIAAigISSAAIAACgIE5AAQB5B6AGDoIjnAAIAAA5IkYAAIAADUIGPAAQBPgOAngjQA8g3AHhsIEYAAIABgdIAAgcIFxAAIAAAbIAAAeIkYAAQgFB4giBcIjAAAIAACOIGuAAIAAiOIFrAAIAAALIEJAAQBNAAA6gLIIEAAQgfBSg3A8IrTAAQhIBOhwAqIBMBrIm4AAIgfgtIj7AAIAAAtIkmAAIAACZINDAAIAACRIBmAAIB8CwgEgVCA0HIAAiwIStAAIAACwgEAntAxXIjNkqIG3AAIDTEqgEAaaAxXIAAkqIFrAAIAAEqgEgvaAm8IAAgLIM8AAIAAALgEgj1AivIAAliISTAAIAACkIsoAAIAAC+gAHFdNIAAigIJpAAQF7AAChCBQASAOARARgAeU8NQg2gmgxgwQg7g5gthCIizAAIk5mbIi8AAIAAGbIDYAAIAAFoIlrAAIkSloIA7AAIAAmbIkYAAIAArTIDXAAIAAiDIFrAAIAACDIiLAAIInLTICSAAIAArTIBOAAQDki6EzAAQE7AADwC6IOaAAQAbAVAZAXIi8EQQhthfhcglQhcgjhrAAQjPAAiNCNQiOCMAADmQAAAhADAeIEYAAQAQC7B1B3QBFBHBTAiIhHAAIAABbQg8BDhIAzQjZCXlAAAQkfAAjUiXgEAd2gl5IEYAAIAAGbIBEAAQBPAhBcAAQB/AABjghQh7ixgOjqIkYAAQgCgfAAghQAAlpD4jyQAegeAggaIp8AAgEAcjgi5IAAjAIiSAAgEAMGgl5IC8AAIi8j3gAjm52IAAloIlXAAICbFoImCAAIiXlfIqpAAIiXFfImCAAICcloIAZAAQBRgfA7g3QB7hzAKjSIkYAAIABgoQAAjwiGiAQiFiCkVAAIjKAAIAAIaIEYAAIAAGbIBKAAQguBMhBA/QjjDdnRAAIosAAIAAloIFsAAIAAAnIDlAAQCLAABngnIBXAAIAAmbIkYAAIAArTIuaAAIAAiDII+AAQFWAADYCDIOaAAQA7AkAzAuQDpDUAAF8IgCAxIEZAAQgKDvhpCsIF0AAICymbIkYAAIE3rTIF5AAIAAiDIFTAAIBkCDIleAAIE4LTIEXAAICyGbIjYAAIAAFogEgMtgl5IEYAAIgsBmIGWAAIgrhmIkYAAIiflzgEA34gfeQAwgQApgXIAAl0IkYAAIAAhKIFtAAIAABKIEYAAIAAGbgEgcQgxMIA4iDIFgAAIA5CDg");
	var mask_1_graphics_21 = new cjs.Graphics().p("EAumAzTIlnoHIj7AAIAAIHIlrAAIAA5aIJoAAQF7AAChCBQChCAAAEcQAAGGk0ByIGbJFgEAlEAmTIEIAAQC6AABDg8QBEg+AAiCQAAiEhGgxQhFgxivAAIkPAAgEAHoAzTIAA5aISTAAIAAFEIsoAAIAAFPILYAAIAAE0IrYAAIAAFSINDAAIAAFBgEgK3AqzIAAk2IM7AAIAAE2gEAjUgbZQg1gmgxgwQjzjsAAlqQAAlpD4jyQD3jyFfAAQFeAAEBDmIi8EQQhthfhcglQhcgjhrAAQjOAAiOCNQiOCMAADmQAADnCICKQCICKC8AAQC8AAB+hIIAAm+IFtAAIAAJAQg9BDhHAzQjZCXlAAAQkfAAjViXgATL5CIsGv6IAAP6IlrAAIAA5ZIFTAAIMeQWIAAwWIFqAAIAAZZgAnj5CIiXlfIqpAAIiXFfImCAAIK95ZIFgAAIK+ZZgEgSagjfIGXAAIjLnZgEg1kgZCIAA5ZII+AAQGzAADpDVQDpDUAAF8QAAF7jjDcQjjDdnRAAgEgv5geDIDmAAQD5AACFh9QCGh+AAjvQAAjwiGiAQiFiCkVAAIjKAAg");
	var mask_1_graphics_70 = new cjs.Graphics().p("EAc7AyOIh7ixInoAAIAACxIlrAAIAAixIgEAAIAAkpIEmAAIAAjjImuAAIAAAqIrYAAIAAC5IlqAAIAAjjIllAAIAACdIs7AAIAAidILTAAIAAiOIlMAAIAAjVIEYAAIAAg4IFqAAIAAA4IkXAAIAABYILXAAIAAB9IBEAAIAACOIFlAAIAAiOIAEAAIAAjVIEYAAIAAg4IDhAAQgHhug+grQhGgxiuAAIkQAAIAADKIlqAAIAAliIklAAIAAigISSAAIAACgIE5AAQB4B5AHDpIjoAAIAAA4IkYAAIAADVIGPAAQBPgPAngjQA8g2AIhtIEYAAIAAgdIAAgbIFxAAIAAAaIAAAeIkYAAQgEB4giBdIjBAAIAACOIGvAAIAAiOIFqAAIAAAKIEJAAQBOAAA5gKIIEAAQgfBSg2A8IrUAAQhHBOhwApIBLBsIm3AAIgfgtIj7AAIAAAtIkmAAIAACYINCAAIAACRIBmAAIB9CxgEgKCAyOIAAixISsAAIAACxgEAytAvdIjOkpIG4AAIDSEpgEAlaAvdIAAkpIFqAAIAAEpgEgkbAlDIAAgLIM8AAIAAALgEgY2Ag2IAAliISTAAIAACjIsnAAIAAC/gASFbUIAAigIJoAAQF8AACgCAQASAPARARgAeU6UQg2gmgxgvQg7g6gthCIizAAIk5maIi8AAIAAGaIDYAAIAAFoIlrAAIkSloIA7AAIAAmaIkYAAIAArUIDXAAIAAiDIFrAAIAACDIiLAAIInLUICSAAIAArUIBOAAQDki6EzABQE7gBDwC6IOaAAQAbAVAZAXIi8EQQhthfhcgkQhcgkhrABQjPAAiNCMQiOCMAADnQAAAgADAfIEYAAQAQC6B1B4QBFBGBTAiIhHAAIAABbQg8BDhIAzQjZCXlAAAQkfAAjUiXgEAd2gj/IEYAAIAAGaIBEAAQBPAiBcAAQB/AABjgiQh7ixgOjpIkYAAQgCggAAghQAAlpD4jyQAegeAggaIp8AAgEAcjgg/IAAjAIiSAAgEAMGgj/IC8AAIi8j4gAjm39IAAloIlXAAICbFoImCAAIiXlfIqpAAIiXFfImCAAICcloIAZAAQBRgeA7g3QB7h0AKjRIkYAAIABgpQAAjwiGiAQiFiCkVABIjKAAIAAIaIEYAAIAAGaIBKAAQguBMhBA/QjjDenRgBIosAAIAAloIFsAAIAAAoIDlAAQCLAABngoIBXAAIAAmaIkYAAIAArUIuaAAIAAiDII+AAQFWAADYCDIOaAAQA7AlAzAtQDpDUAAF8IgCAyIEZAAQgKDuhpCsIF0AAICymaIkYAAIE3rUIF5AAIAAiDIFTAAIBkCDIleAAIE4LUIEXAAICyGaIjYAAIAAFogEgMtgj/IEYAAIgsBlIGWAAIgrhlIkYAAIifl0gEA34gdlQAwgPApgYIAAlzIkYAAIAAhLIFtAAIAABLIEYAAIAAGagEgcQgvTIA4iDIFgAAIA5CDg");
	var mask_1_graphics_74 = new cjs.Graphics().p("EgJ8A0VIh6iwInpAAIAACwIlrAAIAAiwIgDAAIAAhxIskAAIAAi5IEmAAIAAhxIDPAAIAAidILTAAIAAgqIsuAAIAAhkIllAAIAABkIs8AAIAAhkIgvAAIAAhxIMlAAIAABmIGuAAIAAhmIFsAAIAABxIByAAIAAjUIgeAAIABgdIAAgcIFxAAIAAAbIAAAeIAYAAIAABYILXAAIAAALIkEAAQgLA9gTA0IjAAAIAABkIGuAAIAAhkIFrAAIAAALIEHAAQBOAAA5gLIIFAAQgVA3gfAtIFVAAIAAAqIGvAAIAAgqITKAAQgPAWgTAUIrTAAQhCBJhmApIwBAAIAAhyImtAAIAAAqIrXAAIAABIIlrAAIAAhyIlkAAIAAByIrQAAIAAArILFAAIgQAGIBMBrIm3AAIgggtIj7AAIAAAtIklAAIAACZINCAAIAAAgIFDAAIAAi5IFsAAIAAC5IB1AAIAABxIBmAAIB9CwgEgFuAotIFlAAIAAgqIllAAgEgTYAmfQBOgOAngjQAcgaARgmIiiAAgEgw6A0VIAAiwIStAAIAACwgEAL1AxlIhOhxIFHAAIAAikIDDEVgEgBdAxlIAAhxIgfAAIh/i5IG3AAIAOAVIAACkIBDAAIAABxgEg48As7IAAhxIFrAAIAABxgEhJwAr2IAAgsILPAAIAAAsgEAOXAkuQATgqAEg5IFxAAQgBA0gIAvgEgAFAkuIAAhjIFqAAIAABjgEgnHAjLIAAg5IDhAAQgEgxgOgkIF8AAQAGApABAsIjoAAIAAA5gEg+4AjLIAAg5IFrAAIAAA5gEgybAiSIAAhVIjKAAQgSgsgigYQgNgJgRgIIG9AAQALAoAGAtIC6AAIAABVgEhKLAiSIAAhVIFrAAIAABVgEhEIAg9IAAhVIFsAAIAABVgEhb4Ag9IAAhVIFrAAIAABVgA4nfoQhIggiOAAIkPAAIAAAgIlrAAIAAi4IklAAIAAigISSAAIAACgIE6AAQBIBIAeBwgEg9nAfoIAAi4ISTAAIAACkIspAAIAAAUgAyscwIAAigIJoAAQF8AACgCBQATAOAQARgAP28cQgYgRgXgSIgEgDIgQgOIAMAAIhAiTIqoAAIg/CTIC/AAIAADMIlqAAIibjMIg7AAIBDicIAZAAIAigOQAagNAZgQQAegTAagYQATgUASgWIpAAAIB6kbIkYAAIBcjVIh7AAIAAhHIFrAAIAABHICRAAIhcDVIEYAAIgsBlIGWAAIgrhlIkYAAIhajVIA3AAQAGgkAIgjIjcAAIAAm3IDXAAIAAiEIFqAAIAACEIiLAAIFPG3IijAAQgPAigJAlIg0AAIBbDVIEZAAIB6EbIleAAQgQAsgWApIgQAdIgIAOIF1AAIA3iAIF+AAIAAkbIkYAAIAAjVIFrAAIAADVIC8AAIihjVIG2AAICiDVICTAAIAAjVIFrAAIAADVIEXAAIAAEbIDWAAIAACAIDXAAIAACcIoGAAIh2icIA6AAIAAiAImOAAIA3CAIjYAAIAACcICFAAIgQAOQgZAUgZASQiFBdirAkQhsAXh9AAQkfAAjUiYgAVA9QIEUAAIAAicIlWAAgEAYTghsIGUAAIjYkbIi8AAgEAkZgjHIAAjAIiTAAgAyE6EIAAjMIkUAAIBYDMImCAAIhXjMIhQAAIAAicIFrAAIAAAnIDmAAQCLAABngnIBWAAIAAiAIlQAAQBWhtAJiuIkZAAIACgpQAAhegWhOIlaAAIAAhHICOAAIC+m3IF4AAIAAiEIFTAAIBkCEIleAAIC9G3IjUAAIA2BHIhrAAQAMBNAABWIgBAyIEYAAQgGCcgvB/IElAAIAACAIBJAAQguBLhAA/IgTASIh9AAIAADMgEgXdgpcIAaAAIgagigEgU5gqjIBMAAIgmhXgEgwbgaEIBYjMIGCAAIhYDMgEhJDgaEIAAjMITNAAIgQAOQjjC+muAAgEA6NgdQIgkghQg7g6gthBIi0AAIhgiAIjfAAQg5iBgKiaIkYAAQgCggAAghQAAhMAMhIIF9AAQgRBFAABRQAAAgACAfIEYAAQAPCqBiBxIIVAAIAACAIBFAAQBPAhBcAAQB+AABkghQgqg9gdhDIkNAAIAAkbIkYAAIAAhLIFsAAIAABLIEYAAIAAEbIFHAAIAUAXQBGBGBTAjIhHAAIAABbQggAigiAfgEBU0gfsQAvgQAqgYIAAhYIFsAAIAACAgEgkWghsIAAkbIkYAAIAAjVIj/AAIgehHIDJAAQgghIg4g1QiFiBkVAAIjKAAIAAD+IlrAAIAAm3IuaAAIAAiEII+AAQFWAADYCEIOaAAQA8AkAyAuQCbCNA0DYIjMAAIAfBHIDrAAIAADVIEYAAIAAEbgEg03gpcIAfhHIGBAAIgeBHgEhDHgpcQgKglgPgiIGCAAQAIAjAFAkgEhUHgpcIAAhHIFrAAIAABHgEAZPgqjQA3jbCnikQAfgeAggaIobAAQAbAUAZAXIhRB1IAAigIhEAAIAAG3IlrAAIAAm3IBOAAQDki6EzAAQE7AADxC6IN8AAIAACgIhsCcQhshghcgkQhcgjhrAAQjPAAiOCNQhCBBgiBUgEgmWgxaIA4iEIFhAAIA5CEg");
	var mask_1_graphics_79 = new cjs.Graphics().p("EAV0A2pIhUh4IG9AAIBWB4gEAGmA2pIAAh4IFrAAIAAB4gEgRJA2pIAAh4ISsAAIAAB4gEgtBA0xIgmg4InpAAIAAA4IlrAAIAAg4IgEAAIAAgeISuAAIAAAeIBlAAIAoA4gEhSsA0xIAAg4ISuAAIAAA4gEAmdAz5IgVgeIG9AAIAUAegEgjOAz5IAAgeIFrAAIAAAegEhC+AzbIg7hUIFHAAIAAhjIAuAAICBC3gEhP+AzbIAAhUIgeAAIhFhjIGLAAIAABjIBEAAIAABUgEhnuAzbIAAhUIskAAIAAhjIFsAAIAABEINCAAIAAAfIFDAAIAAhjIFrAAIAABjIB2AAIAABUgEgCZAwkIAAhAIAtBAgEgVIAwkIgxhIIG3AAIAGAIIAABAgEggIAwkIAAhIIFrAAIAABIgEg34AwkIAAhIIFqAAIAABIgEhs3AvPIAAhxIFqAAIAABxgEh9rAuKIAAgsILPAAIAAAsgEAJ9AsyIAAg4IvwAAIAAg5INxAAIAAgqIsuAAIAAhkIlkAAIAABkIs8AAIAAhkIgwAAIAAhVIgRAAQAIgNAGgPIiiAAIAAAcIkdAAIAAiAIFrAAIAABZILYAAIAAALIkFAAIgFAcIGvAAIAABKIGuAAIAAhKIFqAAIAABVIBzAAIAAhVIEdAAIAABVQBPgOAngkQARgPANgUIGIAAQgKAtgOAoIjBAAIAABkIGuAAIAAhkIFsAAIAAALIEIAAQBOAAA5gLIIEAAQgVA2geAuIFVAAIAAAqI1mAAIAAgqIhBAAIAAAqIoEAAQhDBIhlApgEgdjAr6IAAg5IRCAAIAAAqIrYAAIAAAPgEgwEAr6IAAg5IM8AAIAAA5gEAyOArBIAAgqIFgAAIAAAqgEgmgAneIAAgcIFrAAIAAAcgEg50AneIAAgcIMmAAIAAAcgEAHLAnCQATgqADg6IFyAAQgCA1gHAvgEgHSAnCIAAhkIFsAAIAABkgEhGEAleIAAgqIFqAAIAAAqgEAk3Af9IAAg6IklAAIAAhjISTAAIAABjIE5AAQAaAaAUAggANGf9IAAg6ISTAAIAAA6gEA4CAfDIAAhjIQKAAQBIAcAyAoQASAPARAQgA55dgIAAg8IJoAAQEFAACeA8gEgxpAdgIAAg8ISTAAIAAA8gEgy7gcYIAAjMITNAAIgQAOQjjC+muAAgEBPnggPQgcgbgYgdIU0AAIAAAiIgVAWgEA+SggPIgsg4IJRAAIAAA4gEAvcggPIAAg4IFrAAIAAA4gEAk0ggPIgZg4IGCAAIAZA4gEASvggPIAZg4IGCAAIgZA4gEgHhggPIAAg4IksAAQgXgcgTgdIi0AAIhhh/IjfAAQgVgxgOgzIGJAAQAZA2AoAuIIVAAIAAB/IBEAAQBQAhBbAAQB/AABighQgqg9gdhCIkMAAIAAhkIFsAAIAABkIFGAAIAVAWQBFBHBTAiIhHAAIAAA5IEfAAQgWAdgZAbgEgdaghHIgrg5IA7AAIAAh/ImPAAIA4B/IjYAAIAAA5IlrAAIAAg5IlXAAIAZA5ImDAAIgUgwIqpAAIgUAwImCAAIAZg5IAZAAIAhgOQAbgNAZgQQAegTAZgYQAUgUASgVIpBAAIAxhyIEHAAIAAiqIkYAAIAAjVIkAAAIgehGIDJAAQghhJg3g0QhahYicgcIJmAAQBJBqAhCHIjLAAIAeBGIDqAAIAADVIB1AAIAAC4IMNAAIAqBkIldAAQgRAsgWAoIgPAdIgIAOIF0AAIA3h/IF/AAIAAhkIFrAAIAABkIGTAAIhMhkIG4AAIAHAIIAAgIIFqAAIAABkIDWAAIAAB/IDXAAIAAA5gEhijghHIAAg5IFsAAIAAAnIDlAAQCLAABngnIBXAAIAAh/IlRAAQAngyAYhAIF5AAQgNA8gTA2IEkAAIAAB/IBKAAQgSAdgVAcgEAP6giAQAwgQApgYIAAhXIFtAAIAAB/gEhpPgj/IAAhyIFrAAIAABygEB3/glxIAAiqIkYAAIAAhKIFtAAIAABKIEYAAIAACqgEhY/grwIAfhGIGCAAIgfBGgEhnPgrwQgKglgPghIGCAAQAIAiAGAkgEgdOgsMIAAgqIhCAAQgIAUgHAWImAAAIAKgqIjeAAIAAi5IJ7AAICMC5IEJAAIAAAqgEgvggsMIAAgqIFrAAIAAAqgEg7hgsMIgFgGIAAAGIlrAAIAAgqICPAAIBQi5IKtAAIBQC5IjVAAIAgAqgEg5Ags2IBKAAIglhYgEgK3gs2QAYhiAuhXIHMAAQgVAQgTATQhBBBgkBVgEgWCgs2IAAi5IFrAAIAAC5gEhexgs2IAAjxIFrAAIAADxgEAHTgvvIB2AAIgrA9QgmgiglgbgEBg3gwnIgggPQhcgjhrAAQh6AAhkAyIoBAAQAshEA7g8IRzAAIAABaIgaAmgEBE/gwnIAAiAIFqAAIAACAgEAzNgwnIAAiAIgnAAIAPgPQAegeAhgaImAAAIAAi4IhBAAQAfgCAgAAQE7AADwC6IN9AAIAABHIpfAAIBhCAgEBLtgynIA+AAIg+Bag");

	this.timeline.addTween(cjs.Tween.get(mask_1).to({graphics:null,x:0,y:0}).wait(6).to({graphics:mask_1_graphics_6,x:261.3,y:1082.3}).wait(5).to({graphics:mask_1_graphics_11,x:397.1,y:1082.3}).wait(4).to({graphics:mask_1_graphics_15,x:208.4,y:1082.3}).wait(6).to({graphics:mask_1_graphics_21,x:148.3,y:1077.1}).wait(49).to({graphics:mask_1_graphics_70,x:208.4,y:1070.2}).wait(4).to({graphics:mask_1_graphics_74,x:436,y:1083.8}).wait(5).to({graphics:mask_1_graphics_79,x:294.5,y:1098.6}).wait(5).to({graphics:null,x:0,y:0}).wait(9));

	// Layer_8
	this.instance_1 = new lib.masque();
	this.instance_1.parent = this;
	this.instance_1.setTransform(353.7,1130.4,2.854,1.638,0,0,0,0.2,265.6);
	this.instance_1.alpha = 0.898;
	this.instance_1._off = true;

	var maskedShapeInstanceList = [this.instance_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(6).to({_off:false},0).to({_off:true},78).wait(9));

	// Layer_6
	this.instance_2 = new lib.img_danger();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-232.5,1133.1,2.119,2.119,0,0,0,0.1,320.2);
	this.instance_2._off = true;

	var maskedShapeInstanceList = [this.instance_2];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(6).to({_off:false},0).to({x:-221.6,y:1254.1},77).to({_off:true},1).wait(9));

	// Layer_7 (mask)
	var mask_2 = new cjs.Shape();
	mask_2._off = true;
	var mask_2_graphics_0 = new cjs.Graphics().p("AwjRVMAAAgipMAhHAAAMAAAAipg");
	var mask_2_graphics_8 = new cjs.Graphics().p("EgefAvcMAAAhe3MA8/AAAMAAABe3g");
	var mask_2_graphics_81 = new cjs.Graphics().p("AwjRWMAAAgirMAhHAAAMAAAAirg");

	this.timeline.addTween(cjs.Tween.get(mask_2).to({graphics:mask_2_graphics_0,x:203.9,y:1193.1}).wait(8).to({graphics:mask_2_graphics_8,x:261.1,y:1112.2}).wait(73).to({graphics:mask_2_graphics_81,x:203.9,y:1053.1}).wait(12));

	// Layer_9
	this.instance_3 = new lib._200_gif();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-653.6,1419.6,8.705,8.705,0,0,0,126.3,100.1);
	this.instance_3._off = true;

	var maskedShapeInstanceList = [this.instance_3];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_2;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(52).to({_off:false},0).to({_off:true},40).wait(1));

	// Layer_3
	this.instance_4 = new lib.img_danger();
	this.instance_4.parent = this;
	this.instance_4.setTransform(158.6,1108.2,0.65,0.65,0,0,0,0.1,320.2);

	var maskedShapeInstanceList = [this.instance_4];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_2;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_4).to({regX:0.3,regY:320.3,x:158.7,y:1101.6},15).to({regX:0.1,regY:320.2,x:158.6,y:1067.2},76).to({_off:true},1).wait(1));

	// Layer_1
	this.instance_5 = new lib.img_danger();
	this.instance_5.parent = this;
	this.instance_5.setTransform(312.4,964,2.119,2.119,0,0,0,0.1,320.2);
	this.instance_5.alpha = 0.109;
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(6).to({_off:false},0).to({y:1005},77).to({_off:true},1).wait(9));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(97.9,1082.2,212,221.9);


(lib.vignette2_action = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_91 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(91).call(this.frame_91).wait(1));

	// Layer_2
	this.instance = new lib.txt_violence();
	this.instance.parent = this;
	this.instance.setTransform(440.9,1084.7,0.15,0.15,0,0,0,0.4,255.4);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(2).to({_off:false},0).wait(90));

	// Layer_5 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	var mask_1_graphics_6 = new cjs.Graphics().p("EAy1AueIAAkJIIpAAIAAEJgEBlDApCIAAjIIE5AAIAADIgEA9FApCIAGgUQAfhgA6hUIHwAAQhfAbhNBHQgoAlgbAtIgLAUgEAy1ApCIAAjIIA6AAIAADIgEBKWAl6IGDAAIiUCnQhmiAiJgngEgPwAjaIAAguIjTAAIAAAGIk5AAIAAkcIIMAAIAADIIHkAAIAAB8gEg8FAjaQDDibELAAQCrAACPBBIAAjIQCRBBB1CDIgvA2IjXAAIAAAogEhOHAjaIAAh8IE3AAIAAB8gEhdaAjaIAAh8IEkAAIBfB8gAvwQ7IAAjIIouAAIAAh1ImQAAIAAjdIE5AAIAAA9IKFAAIAADIIBIAAIAACgIGQAAIAAB1gEg1UAPGIAAjdIE5AAIAAA7IAeAAIAACigEgvEANzIAAh1Ig5AAIAAiiII1AAIAACiIGQAAIAAB1gEBeRALpIAAm3IOrAAIAADSIpyAAIAADlgEBHrALpIAAm3IE5AAIAAG3gEAmBALCIAAhkIEDAAIAABkgAPbLCIAAhkIE5AAIAABkgEAy7AJnIAAkLILIAAIAAELgAfxJeIAAloILJAAIAAAEIJyAAIAAA4IqoAAIAAA5IlaAAIAADzgAKfJeIAAloIDlAAIAAFogAyOD2IAAhlIKTAAIAAA8IA2AAIAAApgEgngAD2IAAgpIhUAAIAAnLIBUAAIAAGPIDlAAIAABlgA8UCRIAAg9IE5AAIAAA9gAn7glIAAjZICNAAIAAg9IPvAAIAAA9IsfAAIAADZgEgjEgQeIjIAAQgrgZgpgfIAAkIICZAAIAJALQB5CCCuABQCvgBB4iCIAKgLIFrAAQg0BxhfBdQhGBFhRAtIDIAAQijBdjQAAQjQAAikhdgEg2kgVeIAAhQIjIAAIAAlAIB+AAQARgTAOgVIFdAAQgIAUgJAUIiwAAIAAFAIDIAAIAABQgEgnggWjIAAgzIJ+AAIAAAzgEhK6gYmIgghQIjIAAIhAigIHJAAIhACgIDIAAIggBQgEhBwgbuIAAgoIACAAQAOAVARATgEhsqgbuIgRgoIJqAAIgQAogEhBsghXQADgzAJgxIFJAAQgPAwgFA0gEhPkghXIAAifIE5AAIAACfgEhhhghXIBAifIFRAAIhBCfgEhorghXIgohkIFRAAIAoBkgEg7Qgi7QAGgeAJgdIAAAAIA4iNIiUAAIAAh3IDbAAIAwB3IDaAAIg4CNIABAAQgOAdgKAegEhjDgi7IgZg7IBfAAIAAA7gEALYgj2QAjgXAmgSQCShFC0AAQC0AACSBFQAmASAjAXgEgEvgj2QgPgggUgcIiBAAIAAloIEgAAIAAFoIAzAAIAAA8gEg97gj2IgZg8IB3AAIAAA8gEgz5gmDIAwh3IBpAAIAAB3gEg6RgqaIAXg7IFSAAIgXA7gEhIsgqaIhokDIFSAAIBoEDg");
	var mask_1_graphics_11 = new cjs.Graphics().p("EAHvA1rIDIAAQisgwiBh+QhFhDguhPIBBAAQA4ggAxgwIBQAAIBOBQIBdAAQBdA6B2AAQCCAABng6IEHAAIBPBSQidCzjEA7IjIAAQhjAehsAAQh8AAhrgegEAezAwrIAAhQIDgAAIAABQgEAoLAvqIAAgPIDIAAIAAlAIE4AAIAAA6ILOAAIAAEGIjIAAIAAAPgEgOAAvqIgLgPIDIAAIjzlAIGuAAQgXgmgRgqIFvAAQAJALAKAKQAjAiAoAZIkdAAIAAFAIjIAAIAAAPgEANRAvbQATgVATgXIAqAsgEACvAvbIAAgpQAOAVARAUgEgXhAvbIAAjIIg5AAIAAAPIk4AAIAAgPIDIAAIAAlAIhMAAIg9hQIkHAAIAAn0II5AAIAADIIBcAAIE9GhIAAmhIE4AAIAAH0ImQAAIAABQIiyAAIAAFAgEgXhApLIBeAAIheh8gEgbiAmDIEBAAIAAh8IkBlSgEAiTAqbIAAhQIBYAAIAABQgEAlDApLIAAn0ICEAAIAAjIIE4AAIAADIIiEAAIAABTIJyAAIAAEJIpyAAIAACYgEgCgApLQg2iCAAieQAAhxAchjIFTAAQguBZAAB2QAACUBEBoIAAApgEAX1AnnIAAjIIH8AAICZDIgEgmqAnTIAAhQIE4AAIAABQgEBdTAhXIAAjIIE4AAIAADIgEA1VAhXIAGgUQAehhA6hTIHwAAQhfAbhMBHQgpAlgbAtIgLAUgEBCmAePIGCAAIiTCnQhniAiIgngEg6ZAePQg+gShDAAQhFAAg8ASInxAAQAng5A0gyQDUjQE1AAQCrAACQBBIAAjIQCQBBB1CDIgvA2IjWAAIAADIgEhV4AePIAAkcIE4AAIAAEcgEhlKAePIAAkcIEkAAIDYEcgA3heKIAAjJIjTAAIAAAGIk4AAIAAkcIILAAIAADIIHkAAIAAEXgA3hJQIAAjIIotAAIAAh1ImQAAIAAjdIE4AAIAAA9IKFAAIAADIIBJAAIAACgIGQAAIAAB1gEg9EAHbIAAjdIE4AAIAAA7IAfAAIAACigEg20AGIIAAh1Ig5AAIAAiiII1AAIAACiIGQAAIAAB1gEBWhAD+IAAm2IOqAAIAADRIpyAAIAADlgEA/7AD+IAAm2IE4AAIAAG2gAeRDXIAAhkIECAAIAABkgAHrDXIAAhkIE4AAIAABkgEArLAB8IAAkKILHAAIAAEKgAYBBzIAAnMIKSAAIAAA8IA2AAIAAAtIJyAAIAAA4IqoAAIAAA5IlaAAIAADygACvBzIAAmQIhUAAIAAnMIBUAAIAAGQIDkAAIAAHMgAN7lZIAAg9IE4AAIAAA9gEAiTgIQIAAjZICOAAIAAg9IPvAAIAAA9IsgAAIAADZgAHL4JIjIAAQgsgZgogfIAAkIICYAAIAKALQB4CCCvABQCvgBB4iCIAKgLIFrAAQg1BxhfBdQhGBFhQAtIDIAAQikBdjPAAQjQAAikhdgAsU9JIAAhQIjIAAIAAlAIB+AAQA2hAAchMIFIAAQgQBKgfBCIixAAIAAFAIDIAAIAABQgACv+OIAAhDQgjgbgggfQhfhdgzhxIDVAAIAAEEIFGAAIAAjDILHAAIAADDImQAAIAABHgEggrggRIgghQIjIAAIiAlAIJKAAIiBFAIDIAAIggBQgEgXhgjZIAAgrQAPAWASAVgEhCbgjZIg4iMIFRAAIAMAfIANgfIAmAAIiBlBIFSAAICAFBIgnAAIg4CMgEACvgkVIAAgIIhKAAIAAhIIBKAAIAAgpICNAAQAWA6AMA/gEgF8gllQAHgUAGgUQAPg8AAhEQAAhRgXhIIFGAAQAOBJAABPQAABDgKA+QgDAUgEAUgEgXNgllQgEgUgDgUQgKg+AAhDQAAhPAOhJIFJAAQgXBIAABRQAABEAPA8QAGAUAHAUgEAbkgmOQgRgXgUgXQh4iDivAAQivAAh4CDQgVAXgQAXIliAAQA0iHBuhrQBZhXBrgzQCThFC0AAQCzAACTBFQBqAzBZBXQBvBrA1CHgEgcugmhQgfhCgQhKIFLAAQASA0AfAtIAAArgEgrkgmhIAAiMIE4AAIAACMgEglUgotIAAlBIE4AAIAAFBgEg3ZgotICAlBIiUAAIAAh3IDbAAIAwB3IDaAAIiBFBgEAAYgqmQgVhAgmg3IiZAAIAAmjIE3AAIAAGjIA0AAIAAB3gEgRAgqmQAMg+AXg5Ih0AAICnmjIFSAAIioGjICFAAQgoA3gUBAgEg40gqmIgwh3IB3AAIAAB3gEgfEgtuIAAh3IBoAAIiomjIFSAAICoGjIiCAAIAAB3gEgvJgtuIAwh3IFRAAIgwB3g");
	var mask_1_graphics_15 = new cjs.Graphics().p("EgFTAxPIDIAAQisgxiBh9Qhnhkg0h/IFvAAQAJALAKALQB2B0CnABQDIAACKiLIBQAAICdCjQidCyjEA8IjIAAQhiAdhsAAQh8AAhrgdgEAbIAxdIAAgOIDIAAIAAmRIE4AAIAACLILOAAIAAEGIjIAAIAAAOgEgbDAxdIgLgOIDIAAIkwmRIkHAAIAAvYIEkAAIKuOFIAAuFIE4AAIAAPYImQAAIAAGRIjIAAIAAAOgEgcFAq+IFfAAIlfnNgEgqVAxdIAAgOIDIAAIAAmRIE4AAIAAGRIjIAAIAAAOgEAkgAq+IAAvYIPvAAIAAEXIq3AAIAAEgIJyAAIAAEJIpyAAIAACYgEAMuAq+QATgUATgXIAqArgEgDDAq+Qg2iBAAifQAAk3DVjPQDTjRE1ABQFYAADoEFIjCDcQiVi4jbAAQiwAAh9BzQh9BzAADDQAACzBjBxgEAj+ALDIAAh1ImQAAIAAr3IE4AAIAAAtIJyAAIAAEIIpyAAIAAEjILOAAIAACfIGQAAIAAB1gANYLDIAAh1ImQAAIAAr3IE4AAIAAJVIJUAAIAACiIGQAAIAAB1gAtnDvIAAkJILHAAIAAEJgAXeipIAAoKIPvAAIAAEXIq3AAIAADzgAA4ipIAAoKIE4AAIAAIKgAl38mIjIAAQhRgthGhEQiWiUgsjHIFLAAQAeBTA+BEQB4CDCvAAQCvAAB3iDQA+hEAehTIFIAAQgsDHiXCUQhGBEhQAtIDIAAQikBdjOABQjQgBikhdgA5X7WIAAhQIjIAAIAAnMIE4AAIAAHMIDIAAIAABQgEgtugbWIgghQIjIAAIi4nMIFRAAIAMAgIANggIAmAAIiBlAIFSAAICAFAIgnAAIi5HMIDIAAIggBQgEAQqgiqIAAhIIGQAAIAAjCILHAAIAADCImQAAIAABIgEAJIgjyQAchMAAhbQAAhRgXhIIFGAAQAOBJAABPQAABZgRBPgEgIIgjyQgRhPAAhZQAAhPAOhJIFJAAQgXBIAABRQAABbAcBMgEgWPgjyIAAlAIE4AAIAAFAgEgoUgjyICAlAIjbAAIjYoaIFSAAIDYIaIDaAAIiBFAgEAPdgoyQgehchDhLQh4iDivAAQivAAh4CDQhEBLgdBcIlIAAQAojRCciaQDUjPE3AAQE1AADUDPQCeCaAoDRgEgP/goyIAAoaIE4AAIAAIagEggEgoyIDXoaIFSAAIjZIag");
	var mask_1_graphics_21 = new cjs.Graphics().p("EgJDAuhQjRjLAAk4QAAk3DVjPQDUjRE1ABQFXAADoEFIjCDcQiUi4jbAAQiwAAh9BzQh9BzAADDQAADEB3B2QB1B0CoABQDlAACSi2IDHDOQjtELlCAAQlDAAjRjLgEAcFAxdIAA13IPwAAIAAEXIq3AAIAAEgIJxAAIAAEJIpxAAIAAEjILNAAIAAEUgEgaFAxdIqatsIAANsIk5AAIAA13IEkAAIKvOFIAAuFIE3AAIAAV3gAbjLDIAA12IPvAAIAAEXIq3AAIAAEgIJzAAIAAEIIpzAAIAAEjILOAAIAAEUgAE9LDIAA12IE4AAIAARfIJUAAIAAEXgAvyDvIAAkJILHAAIAAEJgACG+XQjSjQAAkzQAAkzDSjQQDVjPE2AAQE2AADTDPQDVDQAAEzQAAEzjVDQQjTDOk2ABQk2gBjVjOgEAFrgrZQh4CFAAC7QAAC7B4CDQB4CDCvAAQCvAAB4iDQB4iDAAi7QAAi7h4iFQh4iDivAAQivAAh4CDgAvC7WIAA12IE4AAIAAV2gEgjYgbWIoy12IFSAAIFkN6IFkt6IFSAAIoyV2gEAeHgiqIAAkKILIAAIAAEKg");
	var mask_1_graphics_70 = new cjs.Graphics().p("EgFTAxPIDIAAQisgxiBh9Qhnhkg0h/IFvAAQAJALAKALQB2B0CnABQDIAACKiLIBQAAICdCjQidCyjEA8IjIAAQhiAdhsAAQh8AAhrgdgEAbIAxdIAAgOIDIAAIAAmRIE4AAIAACLILOAAIAAEGIjIAAIAAAOgEgbDAxdIgLgOIDIAAIkwmRIkHAAIAAvYIEkAAIKuOFIAAuFIE4AAIAAPYImQAAIAAGRIjIAAIAAAOgEgcFAq+IFfAAIlfnNgEgqVAxdIAAgOIDIAAIAAmRIE4AAIAAGRIjIAAIAAAOgEAkgAq+IAAvYIPvAAIAAEXIq3AAIAAEgIJyAAIAAEJIpyAAIAACYgEAMuAq+QATgUATgXIAqArgEgDDAq+Qg2iBAAifQAAk3DVjPQDTjRE1ABQFYAADoEFIjCDcQiVi4jbAAQiwAAh9BzQh9BzAADDQAACzBjBxgEAj+ALDIAAh1ImQAAIAAr3IE4AAIAAAtIJyAAIAAEIIpyAAIAAEjILOAAIAACfIGQAAIAAB1gANYLDIAAh1ImQAAIAAr3IE4AAIAAJVIJUAAIAACiIGQAAIAAB1gAtnDvIAAkJILHAAIAAEJgAXeipIAAoKIPvAAIAAEXIq3AAIAADzgAA4ipIAAoKIE4AAIAAIKgAl38mIjIAAQhRgthGhEQiWiUgsjHIFLAAQAeBTA+BEQB4CDCvAAQCvAAB3iDQA+hEAehTIFIAAQgsDHiXCUQhGBEhQAtIDIAAQikBdjOABQjQgBikhdgA5X7WIAAhQIjIAAIAAnMIE4AAIAAHMIDIAAIAABQgEgtugbWIgghQIjIAAIi4nMIFRAAIAMAgIANggIAmAAIiBlAIFSAAICAFAIgnAAIi5HMIDIAAIggBQgEAQqgiqIAAhIIGQAAIAAjCILHAAIAADCImQAAIAABIgEAJIgjyQAchMAAhbQAAhRgXhIIFGAAQAOBJAABPQAABZgRBPgEgIIgjyQgRhPAAhZQAAhPAOhJIFJAAQgXBIAABRQAABbAcBMgEgWPgjyIAAlAIE4AAIAAFAgEgoUgjyICAlAIjbAAIjYoaIFSAAIDYIaIDaAAIiBFAgEAPdgoyQgehchDhLQh4iDivAAQivAAh4CDQhEBLgdBcIlIAAQAojRCciaQDUjPE3AAQE1AADUDPQCeCaAoDRgEgP/goyIAAoaIE4AAIAAIagEggEgoyIDXoaIFSAAIjZIag");
	var mask_1_graphics_74 = new cjs.Graphics().p("EAHvA1rIDIAAQisgwiBh+QhFhDguhPIBBAAQA4ggAxgwIBQAAIBOBQIBdAAQBdA6B2AAQCCAABng6IEHAAIBPBSQidCzjEA7IjIAAQhjAehsAAQh8AAhrgegEAezAwrIAAhQIDgAAIAABQgEAoLAvqIAAgPIDIAAIAAlAIE4AAIAAA6ILOAAIAAEGIjIAAIAAAPgEgOAAvqIgLgPIDIAAIjzlAIGuAAQgXgmgRgqIFvAAQAJALAKAKQAjAiAoAZIkdAAIAAFAIjIAAIAAAPgEANRAvbQATgVATgXIAqAsgEACvAvbIAAgpQAOAVARAUgEgXhAvbIAAjIIg5AAIAAAPIk4AAIAAgPIDIAAIAAlAIhMAAIg9hQIkHAAIAAn0II5AAIAADIIBcAAIE9GhIAAmhIE4AAIAAH0ImQAAIAABQIiyAAIAAFAgEgXhApLIBeAAIheh8gEgbiAmDIEBAAIAAh8IkBlSgEAiTAqbIAAhQIBYAAIAABQgEAlDApLIAAn0ICEAAIAAjIIE4AAIAADIIiEAAIAABTIJyAAIAAEJIpyAAIAACYgEgCgApLQg2iCAAieQAAhxAchjIFTAAQguBZAAB2QAACUBEBoIAAApgEAX1AnnIAAjIIH8AAICZDIgEgmqAnTIAAhQIE4AAIAABQgEBdTAhXIAAjIIE4AAIAADIgEA1VAhXIAGgUQAehhA6hTIHwAAQhfAbhMBHQgpAlgbAtIgLAUgEBCmAePIGCAAIiTCnQhniAiIgngEg6ZAePQg+gShDAAQhFAAg8ASInxAAQAng5A0gyQDUjQE1AAQCrAACQBBIAAjIQCQBBB1CDIgvA2IjWAAIAADIgEhV4AePIAAkcIE4AAIAAEcgEhlKAePIAAkcIEkAAIDYEcgA3heKIAAjJIjTAAIAAAGIk4AAIAAkcIILAAIAADIIHkAAIAAEXgA3hJQIAAjIIotAAIAAh1ImQAAIAAjdIE4AAIAAA9IKFAAIAADIIBJAAIAACgIGQAAIAAB1gEg9EAHbIAAjdIE4AAIAAA7IAfAAIAACigEg20AGIIAAh1Ig5AAIAAiiII1AAIAACiIGQAAIAAB1gEBWhAD+IAAm2IOqAAIAADRIpyAAIAADlgEA/7AD+IAAm2IE4AAIAAG2gAeRDXIAAhkIECAAIAABkgAHrDXIAAhkIE4AAIAABkgEArLAB8IAAkKILHAAIAAEKgAYBBzIAAnMIKSAAIAAA8IA2AAIAAAtIJyAAIAAA4IqoAAIAAA5IlaAAIAADygACvBzIAAmQIhUAAIAAnMIBUAAIAAGQIDkAAIAAHMgAN7lZIAAg9IE4AAIAAA9gEAiTgIQIAAjZICOAAIAAg9IPvAAIAAA9IsgAAIAADZgAHL4JIjIAAQgsgZgogfIAAkIICYAAIAKALQB4CCCvABQCvgBB4iCIAKgLIFrAAQg1BxhfBdQhGBFhQAtIDIAAQikBdjPAAQjQAAikhdgAsU9JIAAhQIjIAAIAAlAIB+AAQA2hAAchMIFIAAQgQBKgfBCIixAAIAAFAIDIAAIAABQgACv+OIAAhDQgjgbgggfQhfhdgzhxIDVAAIAAEEIFGAAIAAjDILHAAIAADDImQAAIAABHgEggrggRIgghQIjIAAIiAlAIJKAAIiBFAIDIAAIggBQgEgXhgjZIAAgrQAPAWASAVgEhCbgjZIg4iMIFRAAIAMAfIANgfIAmAAIiBlBIFSAAICAFBIgnAAIg4CMgEACvgkVIAAgIIhKAAIAAhIIBKAAIAAgpICNAAQAWA6AMA/gEgF8gllQAHgUAGgUQAPg8AAhEQAAhRgXhIIFGAAQAOBJAABPQAABDgKA+QgDAUgEAUgEgXNgllQgEgUgDgUQgKg+AAhDQAAhPAOhJIFJAAQgXBIAABRQAABEAPA8QAGAUAHAUgEAbkgmOQgRgXgUgXQh4iDivAAQivAAh4CDQgVAXgQAXIliAAQA0iHBuhrQBZhXBrgzQCThFC0AAQCzAACTBFQBqAzBZBXQBvBrA1CHgEgcugmhQgfhCgQhKIFLAAQASA0AfAtIAAArgEgrkgmhIAAiMIE4AAIAACMgEglUgotIAAlBIE4AAIAAFBgEg3ZgotICAlBIiUAAIAAh3IDbAAIAwB3IDaAAIiBFBgEAAYgqmQgVhAgmg3IiZAAIAAmjIE3AAIAAGjIA0AAIAAB3gEgRAgqmQAMg+AXg5Ih0AAICnmjIFSAAIioGjICFAAQgoA3gUBAgEg40gqmIgwh3IB3AAIAAB3gEgfEgtuIAAh3IBoAAIiomjIFSAAICoGjIiCAAIAAB3gEgvJgtuIAwh3IFRAAIgwB3g");
	var mask_1_graphics_79 = new cjs.Graphics().p("EAy1AueIAAkJIIpAAIAAEJgEBlDApCIAAjIIE5AAIAADIgEA9FApCIAGgUQAfhgA6hUIHwAAQhfAbhNBHQgoAlgbAtIgLAUgEAy1ApCIAAjIIA6AAIAADIgEBKWAl6IGDAAIiUCnQhmiAiJgngEgPwAjaIAAguIjTAAIAAAGIk5AAIAAkcIIMAAIAADIIHkAAIAAB8gEg8FAjaQDDibELAAQCrAACPBBIAAjIQCRBBB1CDIgvA2IjXAAIAAAogEhOHAjaIAAh8IE3AAIAAB8gEhdaAjaIAAh8IEkAAIBfB8gAvwQ7IAAjIIouAAIAAh1ImQAAIAAjdIE5AAIAAA9IKFAAIAADIIBIAAIAACgIGQAAIAAB1gEg1UAPGIAAjdIE5AAIAAA7IAeAAIAACigEgvEANzIAAh1Ig5AAIAAiiII1AAIAACiIGQAAIAAB1gEBeRALpIAAm3IOrAAIAADSIpyAAIAADlgEBHrALpIAAm3IE5AAIAAG3gEAmBALCIAAhkIEDAAIAABkgAPbLCIAAhkIE5AAIAABkgEAy7AJnIAAkLILIAAIAAELgAfxJeIAAloILJAAIAAAEIJyAAIAAA4IqoAAIAAA5IlaAAIAADzgAKfJeIAAloIDlAAIAAFogAyOD2IAAhlIKTAAIAAA8IA2AAIAAApgEgngAD2IAAgpIhUAAIAAnLIBUAAIAAGPIDlAAIAABlgA8UCRIAAg9IE5AAIAAA9gAn7glIAAjZICNAAIAAg9IPvAAIAAA9IsfAAIAADZgEgjEgQeIjIAAQgrgZgpgfIAAkIICZAAIAJALQB5CCCuABQCvgBB4iCIAKgLIFrAAQg0BxhfBdQhGBFhRAtIDIAAQijBdjQAAQjQAAikhdgEg2kgVeIAAhQIjIAAIAAlAIB+AAQARgTAOgVIFdAAQgIAUgJAUIiwAAIAAFAIDIAAIAABQgEgnggWjIAAgzIJ+AAIAAAzgEhK6gYmIgghQIjIAAIhAigIHJAAIhACgIDIAAIggBQgEhBwgbuIAAgoIACAAQAOAVARATgEhsqgbuIgRgoIJqAAIgQAogEhBsghXQADgzAJgxIFJAAQgPAwgFA0gEhPkghXIAAifIE5AAIAACfgEhhhghXIBAifIFRAAIhBCfgEhorghXIgohkIFRAAIAoBkgEg7Qgi7QAGgeAJgdIAAAAIA4iNIiUAAIAAh3IDbAAIAwB3IDaAAIg4CNIABAAQgOAdgKAegEhjDgi7IgZg7IBfAAIAAA7gEALYgj2QAjgXAmgSQCShFC0AAQC0AACSBFQAmASAjAXgEgEvgj2QgPgggUgcIiBAAIAAloIEgAAIAAFoIAzAAIAAA8gEg97gj2IgZg8IB3AAIAAA8gEgz5gmDIAwh3IBpAAIAAB3gEg6RgqaIAXg7IFSAAIgXA7gEhIsgqaIhokDIFSAAIBoEDg");

	this.timeline.addTween(cjs.Tween.get(mask_1).to({graphics:null,x:0,y:0}).wait(6).to({graphics:mask_1_graphics_6,x:799,y:921.7}).wait(5).to({graphics:mask_1_graphics_11,x:528.7,y:970.8}).wait(4).to({graphics:mask_1_graphics_15,x:612.2,y:959.3}).wait(6).to({graphics:mask_1_graphics_21,x:606.1,y:959.3}).wait(49).to({graphics:mask_1_graphics_70,x:612.2,y:959.3}).wait(4).to({graphics:mask_1_graphics_74,x:528.7,y:970.8}).wait(5).to({graphics:mask_1_graphics_79,x:799,y:921.7}).wait(5).to({graphics:null,x:0,y:0}).wait(8));

	// Layer_8
	this.instance_1 = new lib.masque();
	this.instance_1.parent = this;
	this.instance_1.setTransform(353.7,938.7,2.854,1.638,0,0,0,0.2,265.6);
	this.instance_1.alpha = 0.898;
	this.instance_1._off = true;

	var maskedShapeInstanceList = [this.instance_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(6).to({_off:false},0).to({_off:true},78).wait(8));

	// Layer_6
	this.instance_2 = new lib.img_action();
	this.instance_2.parent = this;
	this.instance_2.setTransform(373.9,963,2.119,2.119,0,0,0,0.1,320.2);
	this.instance_2._off = true;

	var maskedShapeInstanceList = [this.instance_2];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(6).to({_off:false},0).to({y:1003},77).to({_off:true},1).wait(8));

	// Layer_7 (mask)
	var mask_2 = new cjs.Shape();
	mask_2._off = true;
	var mask_2_graphics_0 = new cjs.Graphics().p("A4ORVMAAAgipMAwdAAAMAAAAipg");
	var mask_2_graphics_8 = new cjs.Graphics().p("EhCNA8oMAAAh5PMCEbAAAMAAAB5Pg");
	var mask_2_graphics_81 = new cjs.Graphics().p("A4ORVMAAAgipMAwdAAAMAAAAipg");

	this.timeline.addTween(cjs.Tween.get(mask_2).to({graphics:mask_2_graphics_0,x:633.2,y:862.4}).wait(8).to({graphics:mask_2_graphics_8,x:486,y:869.8}).wait(73).to({graphics:mask_2_graphics_81,x:633.2,y:862.4}).wait(11));

	// Layer_3
	this.instance_3 = new lib.img_action();
	this.instance_3.parent = this;
	this.instance_3.setTransform(528.4,849.5,0.875,0.875,0,0,0,0.1,320.1);

	var maskedShapeInstanceList = [this.instance_3];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_2;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({y:869.5},91).wait(1));

	// Layer_1
	this.instance_4 = new lib.img_action();
	this.instance_4.parent = this;
	this.instance_4.setTransform(312.4,629.8,2.119,2.119,0,0,0,0.1,320.2);
	this.instance_4.alpha = 0.109;
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off:false},0).to({y:581.5},77).to({y:505.3},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(478.1,751.4,310.2,221.9);


(lib.vignette1_vitesse = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_91 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(91).call(this.frame_91).wait(1));

	// Layer_2
	this.instance = new lib.txt_vitesse();
	this.instance.parent = this;
	this.instance.setTransform(370.3,1326.6,0.15,0.15,0,0,0,0.4,255.4);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(2).to({_off:false},0).wait(90));

	// Layer_5 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	var mask_1_graphics_6 = new cjs.Graphics().p("EgCzAxVIAAhaIQFAAIAABagEgkkAv7IAAikIqQAAIAAkVIE5AAIAAD/ILNAAIAAAWIKQAAIAACkgEgB+AqBIAAg/ILGAAIAAA/gEgWIAqBIAAg/IJ8AAIAAgfIE5AAIAAAfIjtAAIAAA/gEAgpApCIAAgfILIAAIAAAfgEAMgApCIAAgfIJEAAIAAhIIOrAAIAABEIpyAAIAAAEIi2AAIAAAfgEBCaAojIAAhIILIAAIAABIgEAuQAojIAAhIILIAAIAABIgA3AbjIAAgEIWEAAIAAAEgEAwqAKyIAAAJIk6AAIAAhWINpAAQiJBmjYAAQhqAAhkgZgAShK7IAAAAIrNAAIAAhWITSAAQiIBmjYAAQhVAAhQgQgEhF6AHVIAAhDQBZAlBTAAQBVAAAxglQAKgHAIgIIDoAAIB2iSQBuBgBlAyIjaAAQgJArgSAngEgyeAGDIAIgJIAAAJgEgyyADKIgRgKIAtAAIAAAkQgMgOgQgMgEg5xADAIAAjWIHbAAIAACbIiiAAIAAA7gAoDixQgGgTgDgWIDJAAIAAApgEAkagEDQgCgaAAgdQAAg7AMgyIFpAAIgGAEQgtAlAAA9QAAAjASAbgAvXkDIAAjDICUAAIAAj0IKuAAQBfgfByAAQCNAACLAvQCMAwBoBYIhUB7IkJAAQhEgZhGgGIhRAAQgxAGghAZIkSAAIhKBqQifh6irgPIhRAAQg1AHgjAcQgtAlAAA9QAAAjATAbgEAu8gGnIEHAAIhJBqQhehIhggigA3QnGQgXgCgYAAQgSAAgQACImzAAQAihlBZhDQCOhrDWAAQCMAACMAvQCMAwBpBYIhABcgEheBgifIAAjXIhmAAIAADXIgtAAIAAj2IE5AAIAAAfICRAAIAADXgEgCogjBIAAkLIE3AAIAAELgEgE7gjBIAAkLIAtAAIAAELgEgXAgjBIAAg5IBnkBIAAgLIkMAAIAAjYIE5AAIAAA7IAWAAIANgfIFQAAIiUFzIAACOgEgrygjBIjOoBIFSAAIBLC8IAdAAIBWjYIFSAAIhXDYIjdAAIAABxIhkAAIBLC7IANggIAAA5gEhM2gl2IAAgfIhAAAIAthxIFSAAIguBxIAmAAIAAAfgEg+ngmVIAAhxIE4AAIAABxgEhZOgmVIiElJIFSAAIAqBqIAABuIAsAAIAuBxgEgGHgoGIAAgTImNAAIAAjFIRQAAIAADFImMAAIAAATgEgwTgoGIAAhuIAsBugEBPEgwLIAAgKIRRAAIAAAKgEBBzgwLIAAgKIE5AAIAAAKgEA0ogwLIAEgKIqqAAIAAg/IRRAAIAAA/IhWAAIgDAKgEAAGgwLIgdhJIFRAAIAeBJgEAcxgwVIAAg/IE4AAIAAA/gEAPpgwVIAZg/IFTAAIgaA/g");
	var mask_1_graphics_11 = new cjs.Graphics().p("EgQAAxVIAAj+IqQAAIAAkVIE5AAIAAD/ILNAAIAAAWIKPAAIAAD+gEASlAqBIAAg/ILIAAIAAA/gEgBkAqBIAAg/IJ8AAIAAhnIOqAAIAABEIpyAAIAAAjIjtAAIAAA/gEA1OApCIAAhnILHAAIAABngEAhEApCIAAhnILHAAIAABngEArRAlHIAAhkILIAAIAABkgEAXHAlHIAAhkILIAAIAABkgEgBkAlHIAAjoIE4AAIAAAiIJyAAIAADGgEAfzAhfIAAl8IZsAAIAACQIvwAAIAAgSIlEAAIAAD+gEgkMAbjIAAgEIZsAAIAAAEgA1WKyIAAAJIk6AAIAAilQgngegmgjIQvAAQgjBJhEA5QiMB0jnAAQhqAAhkgZgEgzgAK7IAAAAIrMAAIAAkUIE1AAIAAjnIE5AAIAADDIA7AAIAjAPIAABDIA0AAIAAAAIC5jkQDjDGC/AAQBWAAAxglQAyglAAg/QAAg/g1glQg0gliagmQidglhng0IAAg8IOSAAQAVAOARAPQAVATARAWIAPAVQAhA1AOBGIjdAAQAJAvAAA2QAABigmBOInjAAQgjBJhEA5QiNB0jmAAQhVAAhRgQgEhTHAHVIAAhDQBZAlBTAAQBVAAAxglQAKgHAIgIIDoAAIB2iSQBuBgBmAyIjaAAQgKArgSAngEg/qAGDQAggiAAgzQAAg/g1glIgRgKIF+AAQAJAvAAA2QAAAxgKAtgEhG9ADAIAAjWIOqAAIAACbIpyAAIAAA7gAzwgWQhUhGgVh+IDKAAIAAEAQg5gcgogggAXOkDQgDgaAAgdQAAjICQhsQCOhrDWAAQCNAACLAvQCMAwBpBYIifDlQi1iLjDAAQhOAAgtAlQgtAlAAA9QAAAjATAbgA8kkDIAAjDICUAAIAAj0IPwAAIAAD0InsAAIhfCJQifh6iqgPIhRAAQg2AHgjAcQgtAlAAA9QAAAjATAbgAGynGQgXgCgYAAQgSAAgQACImyAAQAihlBYhDQCOhrDWAAQCNAACLAvQCMAwBpBYIhABcgAR37eIAAmbIE3AAIAAGbgAPk7eIAAmbIAtAAIAAGbgAky7eIilmbIKoAAIAAGbgEgP1gh5IAAoqIhmAAIAAIqIgtAAIAApJIE5AAIAAAfICSAAIAAIqgEgkMgh5IAAiBIBmkBIAAioIBDAAIANgfIFRAAIiVFzIAADWgEg4igh5IjrpJIFSAAIDDHoIANggIAACBgEABVgqjIAAgfIhAAAIAthxIFSAAIguBxIAnAAIAAAfgEAjCgrCIAAhxIDDAAIBajiIqqAAIAAg/IRRAAIAAA/IhVAAIhbDiIjdAAIAABxgEAPkgrCIAAhxIE4AAIAABxgEgLCgrCIihmSIFSAAIBIC0IAABtIAsAAIAtBxgEBIEgszIAAgTImMAAIAAjPIRQAAIAADPImMAAIAAATgEA0mgszIAAjiIE5AAIAADigEAd4gszIAAhtIAsBtgEAPkgwVIAAg/IE4AAIAAA/gEACdgwVIAZg/IFSAAIgaA/g");
	var mask_1_graphics_15 = new cjs.Graphics().p("EgZPAxVIAAj+IqQAAIAAl8IOqAAIAABEIpyAAIAAEiILNAAIAAAWIKQAAIAAD+gEAJWAqBIAAimILHAAIAACmgEgK0AqBIAAimILHAAIAACmgEgAmAlHIAAhkILHAAIAABkgEgUwAlHIAAhkILIAAIAABkgEgtcAlHIAApoIZsAAIAACUIvvAAIAAgSIlEAAIAAEgIJxAAIAADGgAEoHVIIZAAIgBAAIC5jkQDjDGDAAAQBVAAAxglQAyglAAg/QAAg/g1glQg0gliagmQidglhmg0IAAkAIjLAAQgIgsAAg0QAAjICQhsQCOhrDXAAQCMAACMAvQCLAwBpBYIifDlQi1iLjCAAQhPAAgtAlQgsAlgBA9QABA9A2AkIAKAGIDeAAQA9AjC3AsQDHAxBuBhQAVATASAWIAOAVQAhA1AOBGIjdAAQAJAvAAA2QAABigmBOIoYAAQgjBJhDA5QiNB0jnAAQlLAAkKj2gAzaK7IAAAAIwFAAIAAn7IE4AAIAADnIFDAAIAegkIFJAAQBsA0BjAAQBVAAAxglQALgHAHgIIDoAAIB2iSQBuBgBmAyIjaAAQgbB9hoBXQiMB0jnAAQhVAAhRgQgAACGDQAggiAAgzQAAg/g0glIgRgKIF9AAQAJAvAAA2QAAAxgJAtgAnQDAIAAmaIjeAAQgIgsAAg0QABjICPhsQCPhrDWAAQCMAACLAvQCMAwBoBYIifDlQi0iLjDAAQhNAAgtAlQguAlABA9QAAA9A3AkIAKAGICaAAIAABWIJyAAIAAEJIpyAAIAAA7gEAr8gAWQhThGgWh+IDKAAIAAEAQg4gcgpgggEgjfgDaIAAngIPvAAIAAEWIq3AAIAADKgAm37eIAAvFIhmAAIAAPFIgtAAIAA12IE5AAIAAGxICSAAIAAPFgA9g7eIox12IFRAAIFlN6IB0khIAAioIBCAAICumxIFRAAIk2MFIAAJxgEAKTgqjIAAijImMAAIAAkOIRQAAIAAEOImMAAIAACjg");
	var mask_1_graphics_21 = new cjs.Graphics().p("EgfhAxVIAA12IPwAAIAAEWIq3AAIAAEgIJyAAIAAEKIpyAAIAAEiILNAAIAAEUgEANUAqBIAAkKILIAAIAAEKgEgG1AqBIAAkKILHAAIAAEKgAQ/HVIC5jkQDjDGC/AAQBWAAAxglQAyglAAg/QAAg/g1glQg0gliagmQjzg6hyhbQhxheAAjGQAAjICQhsQCOhrDWAAQCNAACLAvQCMAwBpBYIifDlQi1iLjDAAQhOAAgtAlQgtAlAAA9QAAA9A3AkQA4AlDGAwQDHAxBvBhQBtBjAAC7QAAC8iNB2QiMB0jnAAQlMAAkJj2gAnyHVIC5jkQDjDGC+AAQBWAAAxglQAyglAAg/QAAg/g1glQg0gliagmQjyg6hyhbQhxheAAjGQAAjICQhsQCOhrDVAAQCNAACLAvQCMAwBpBYIifDlQi1iLjDAAQhNAAgtAlQgtAlAAA9QAAA9A3AkQA4AlDFAwQDHAxBvBhQBtBjAAC7QAAC8iNB2QiMB0jnAAQlLAAkJj2gA/hK7IAA11IPwAAIAAEWIq3AAIAAEgIJyAAIAAEJIpyAAIAAEiILNAAIAAEUgAOS7eIAAxoImMAAIAAkOIRQAAIAAEOImMAAIAARogAlL7eIAA12IE5AAIAAV2gA5h7eIoy12IFSAAIFlN6IFjt6IFSAAIoxV2g");
	var mask_1_graphics_65 = new cjs.Graphics().p("EgZPAxVIAAj+IqQAAIAAl8IOqAAIAABEIpyAAIAAEiILNAAIAAAWIKQAAIAAD+gEAJWAqBIAAimILHAAIAACmgEgK0AqBIAAimILHAAIAACmgEgAmAlHIAAhkILHAAIAABkgEgUwAlHIAAhkILIAAIAABkgEgtcAlHIAApoIZsAAIAACUIvvAAIAAgSIlEAAIAAEgIJxAAIAADGgAEoHVIIZAAIgBAAIC5jkQDjDGDAAAQBVAAAxglQAyglAAg/QAAg/g1glQg0gliagmQidglhmg0IAAkAIjLAAQgIgsAAg0QAAjICQhsQCOhrDXAAQCMAACMAvQCLAwBpBYIifDlQi1iLjCAAQhPAAgtAlQgsAlgBA9QABA9A2AkIAKAGIDeAAQA9AjC3AsQDHAxBuBhQAVATASAWIAOAVQAhA1AOBGIjdAAQAJAvAAA2QAABigmBOIoYAAQgjBJhDA5QiNB0jnAAQlLAAkKj2gAzaK7IAAAAIwFAAIAAn7IE4AAIAADnIFDAAIAegkIFJAAQBsA0BjAAQBVAAAxglQALgHAHgIIDoAAIB2iSQBuBgBmAyIjaAAQgbB9hoBXQiMB0jnAAQhVAAhRgQgAACGDQAggiAAgzQAAg/g0glIgRgKIF9AAQAJAvAAA2QAAAxgJAtgAnQDAIAAmaIjeAAQgIgsAAg0QABjICPhsQCPhrDWAAQCMAACLAvQCMAwBoBYIifDlQi0iLjDAAQhNAAgtAlQguAlABA9QAAA9A3AkIAKAGICaAAIAABWIJyAAIAAEJIpyAAIAAA7gEAr8gAWQhThGgWh+IDKAAIAAEAQg4gcgpgggEgjfgDaIAAngIPvAAIAAEWIq3AAIAADKgAm37eIAAvFIhmAAIAAPFIgtAAIAA12IE5AAIAAGxICSAAIAAPFgA9g7eIox12IFRAAIFlN6IB0khIAAioIBCAAICumxIFRAAIk2MFIAAJxgEAKTgqjIAAijImMAAIAAkOIRQAAIAAEOImMAAIAACjg");
	var mask_1_graphics_69 = new cjs.Graphics().p("EgZPAxVIAAj+IqQAAIAAl8IOqAAIAABEIpyAAIAAEiILNAAIAAAWIKQAAIAAD+gEAJWAqBIAAimILHAAIAACmgEgK0AqBIAAimILHAAIAACmgEgAmAlHIAAhkILHAAIAABkgEgUwAlHIAAhkILIAAIAABkgEgtcAlHIAApoIZsAAIAACUIvvAAIAAgSIlEAAIAAEgIJxAAIAADGgAEoHVIIZAAIgBAAIC5jkQDjDGDAAAQBVAAAxglQAyglAAg/QAAg/g1glQg0gliagmQidglhmg0IAAkAIjLAAQgIgsAAg0QAAjICQhsQCOhrDXAAQCMAACMAvQCLAwBpBYIifDlQi1iLjCAAQhPAAgtAlQgsAlgBA9QABA9A2AkIAKAGIDeAAQA9AjC3AsQDHAxBuBhQAVATASAWIAOAVQAhA1AOBGIjdAAQAJAvAAA2QAABigmBOIoYAAQgjBJhDA5QiNB0jnAAQlLAAkKj2gAzaK7IAAAAIwFAAIAAn7IE4AAIAADnIFDAAIAegkIFJAAQBsA0BjAAQBVAAAxglQALgHAHgIIDoAAIB2iSQBuBgBmAyIjaAAQgbB9hoBXQiMB0jnAAQhVAAhRgQgAACGDQAggiAAgzQAAg/g0glIgRgKIF9AAQAJAvAAA2QAAAxgJAtgAnQDAIAAmaIjeAAQgIgsAAg0QABjICPhsQCPhrDWAAQCMAACLAvQCMAwBoBYIifDlQi0iLjDAAQhNAAgtAlQguAlABA9QAAA9A3AkIAKAGICaAAIAABWIJyAAIAAEJIpyAAIAAA7gEAr8gAWQhThGgWh+IDKAAIAAEAQg4gcgpgggEgjfgDaIAAngIPvAAIAAEWIq3AAIAADKgAm37eIAAvFIhmAAIAAPFIgtAAIAA12IE5AAIAAGxICSAAIAAPFgA9g7eIox12IFRAAIFlN6IB0khIAAioIBCAAICumxIFRAAIk2MFIAAJxgEAKTgqjIAAijImMAAIAAkOIRQAAIAAEOImMAAIAACjg");
	var mask_1_graphics_74 = new cjs.Graphics().p("EgQAAxVIAAj+IqQAAIAAkVIE5AAIAAD/ILNAAIAAAWIKPAAIAAD+gEASlAqBIAAg/ILIAAIAAA/gEgBkAqBIAAg/IJ8AAIAAhnIOqAAIAABEIpyAAIAAAjIjtAAIAAA/gEA1OApCIAAhnILHAAIAABngEAhEApCIAAhnILHAAIAABngEArRAlHIAAhkILIAAIAABkgEAXHAlHIAAhkILIAAIAABkgEgBkAlHIAAjoIE4AAIAAAiIJyAAIAADGgEAfzAhfIAAl8IZsAAIAACQIvwAAIAAgSIlEAAIAAD+gEgkMAbjIAAgEIZsAAIAAAEgA1WKyIAAAJIk6AAIAAilQgngegmgjIQvAAQgjBJhEA5QiMB0jnAAQhqAAhkgZgEgzgAK7IAAAAIrMAAIAAkUIE1AAIAAjnIE5AAIAADDIA7AAIAjAPIAABDIA0AAIAAAAIC5jkQDjDGC/AAQBWAAAxglQAyglAAg/QAAg/g1glQg0gliagmQidglhng0IAAg8IOSAAQAVAOARAPQAVATARAWIAPAVQAhA1AOBGIjdAAQAJAvAAA2QAABigmBOInjAAQgjBJhEA5QiNB0jmAAQhVAAhRgQgEhTHAHVIAAhDQBZAlBTAAQBVAAAxglQAKgHAIgIIDoAAIB2iSQBuBgBmAyIjaAAQgKArgSAngEg/qAGDQAggiAAgzQAAg/g1glIgRgKIF+AAQAJAvAAA2QAAAxgKAtgEhG9ADAIAAjWIOqAAIAACbIpyAAIAAA7gAzwgWQhUhGgVh+IDKAAIAAEAQg5gcgogggAXOkDQgDgaAAgdQAAjICQhsQCOhrDWAAQCNAACLAvQCMAwBpBYIifDlQi1iLjDAAQhOAAgtAlQgtAlAAA9QAAAjATAbgA8kkDIAAjDICUAAIAAj0IPwAAIAAD0InsAAIhfCJQifh6iqgPIhRAAQg2AHgjAcQgtAlAAA9QAAAjATAbgAGynGQgXgCgYAAQgSAAgQACImyAAQAihlBYhDQCOhrDWAAQCNAACLAvQCMAwBpBYIhABcgAR37eIAAmbIE3AAIAAGbgAPk7eIAAmbIAtAAIAAGbgAky7eIilmbIKoAAIAAGbgEgP1gh5IAAoqIhmAAIAAIqIgtAAIAApJIE5AAIAAAfICSAAIAAIqgEgkMgh5IAAiBIBmkBIAAioIBDAAIANgfIFRAAIiVFzIAADWgEg4igh5IjrpJIFSAAIDDHoIANggIAACBgEABVgqjIAAgfIhAAAIAthxIFSAAIguBxIAnAAIAAAfgEAjCgrCIAAhxIDDAAIBajiIqqAAIAAg/IRRAAIAAA/IhVAAIhbDiIjdAAIAABxgEAPkgrCIAAhxIE4AAIAABxgEgLCgrCIihmSIFSAAIBIC0IAABtIAsAAIAtBxgEBIEgszIAAgTImMAAIAAjPIRQAAIAADPImMAAIAAATgEA0mgszIAAjiIE5AAIAADigEAd4gszIAAhtIAsBtgEAPkgwVIAAg/IE4AAIAAA/gEACdgwVIAZg/IFSAAIgaA/g");
	var mask_1_graphics_79 = new cjs.Graphics().p("EgCzAxVIAAhaIQFAAIAABagEgkkAv7IAAikIqQAAIAAkVIE5AAIAAD/ILNAAIAAAWIKQAAIAACkgEgB+AqBIAAg/ILGAAIAAA/gEgWIAqBIAAg/IJ8AAIAAgfIE5AAIAAAfIjtAAIAAA/gEAgpApCIAAgfILIAAIAAAfgEAMgApCIAAgfIJEAAIAAhIIOrAAIAABEIpyAAIAAAEIi2AAIAAAfgEBCaAojIAAhIILIAAIAABIgEAuQAojIAAhIILIAAIAABIgA3AbjIAAgEIWEAAIAAAEgEAwqAKyIAAAJIk6AAIAAhWINpAAQiJBmjYAAQhqAAhkgZgAShK7IAAAAIrNAAIAAhWITSAAQiIBmjYAAQhVAAhQgQgEhF6AHVIAAhDQBZAlBTAAQBVAAAxglQAKgHAIgIIDoAAIB2iSQBuBgBlAyIjaAAQgJArgSAngEgyeAGDIAIgJIAAAJgEgyyADKIgRgKIAtAAIAAAkQgMgOgQgMgEg5xADAIAAjWIHbAAIAACbIiiAAIAAA7gAoDixQgGgTgDgWIDJAAIAAApgEAkagEDQgCgaAAgdQAAg7AMgyIFpAAIgGAEQgtAlAAA9QAAAjASAbgAvXkDIAAjDICUAAIAAj0IKuAAQBfgfByAAQCNAACLAvQCMAwBoBYIhUB7IkJAAQhEgZhGgGIhRAAQgxAGghAZIkSAAIhKBqQifh6irgPIhRAAQg1AHgjAcQgtAlAAA9QAAAjATAbgEAu8gGnIEHAAIhJBqQhehIhggigA3QnGQgXgCgYAAQgSAAgQACImzAAQAihlBZhDQCOhrDWAAQCMAACMAvQCMAwBpBYIhABcgEheBgifIAAjXIhmAAIAADXIgtAAIAAj2IE5AAIAAAfICRAAIAADXgEgCogjBIAAkLIE3AAIAAELgEgE7gjBIAAkLIAtAAIAAELgEgXAgjBIAAg5IBnkBIAAgLIkMAAIAAjYIE5AAIAAA7IAWAAIANgfIFQAAIiUFzIAACOgEgrygjBIjOoBIFSAAIBLC8IAdAAIBWjYIFSAAIhXDYIjdAAIAABxIhkAAIBLC7IANggIAAA5gEhM2gl2IAAgfIhAAAIAthxIFSAAIguBxIAmAAIAAAfgEg+ngmVIAAhxIE4AAIAABxgEhZOgmVIiElJIFSAAIAqBqIAABuIAsAAIAuBxgEgGHgoGIAAgTImNAAIAAjFIRQAAIAADFImMAAIAAATgEgwTgoGIAAhuIAsBugEBPEgwLIAAgKIRRAAIAAAKgEBBzgwLIAAgKIE5AAIAAAKgEA0ogwLIAEgKIqqAAIAAg/IRRAAIAAA/IhWAAIgDAKgEAAGgwLIgdhJIFRAAIAeBJgEAcxgwVIAAg/IE4AAIAAA/gEAPpgwVIAZg/IFTAAIgaA/g");

	this.timeline.addTween(cjs.Tween.get(mask_1).to({graphics:null,x:0,y:0}).wait(6).to({graphics:mask_1_graphics_6,x:46.4,y:963.2}).wait(5).to({graphics:mask_1_graphics_11,x:130.9,y:963.2}).wait(4).to({graphics:mask_1_graphics_15,x:190.1,y:963.2}).wait(6).to({graphics:mask_1_graphics_21,x:164.6,y:963.2}).wait(44).to({graphics:mask_1_graphics_65,x:190.1,y:963.2}).wait(4).to({graphics:mask_1_graphics_69,x:190.1,y:963.2}).wait(5).to({graphics:mask_1_graphics_74,x:130.9,y:963.2}).wait(5).to({graphics:mask_1_graphics_79,x:46.4,y:963.2}).wait(5).to({graphics:null,x:0,y:0}).wait(8));

	// Layer_8
	this.instance_1 = new lib.masque();
	this.instance_1.parent = this;
	this.instance_1.setTransform(353.7,938.7,2.854,1.638,0,0,0,0.2,265.6);
	this.instance_1.alpha = 0.898;
	this.instance_1._off = true;

	var maskedShapeInstanceList = [this.instance_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(6).to({_off:false},0).to({_off:true},78).wait(8));

	// Layer_6
	this.instance_2 = new lib.vignette_vitesse();
	this.instance_2.parent = this;
	this.instance_2.setTransform(373.9,923,2.119,2.119,0,0,0,0.1,320.2);
	this.instance_2._off = true;

	var maskedShapeInstanceList = [this.instance_2];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(6).to({_off:false},0).to({x:401.2},77).to({_off:true},1).wait(8));

	// Layer_7 (mask)
	var mask_2 = new cjs.Shape();
	mask_2._off = true;
	var mask_2_graphics_0 = new cjs.Graphics().p("A4ORVMAAAgipMAwdAAAMAAAAipg");
	var mask_2_graphics_8 = new cjs.Graphics().p("EhCNAYgMAAAgw/MCEbAAAMAAAAw/g");
	var mask_2_graphics_81 = new cjs.Graphics().p("A4ORVMAAAgipMAwdAAAMAAAAipg");

	this.timeline.addTween(cjs.Tween.get(mask_2).to({graphics:mask_2_graphics_0,x:633.2,y:862.4}).wait(8).to({graphics:mask_2_graphics_8,x:484.5,y:852.8}).wait(73).to({graphics:mask_2_graphics_81,x:633.2,y:862.4}).wait(11));

	// Layer_3
	this.instance_3 = new lib.vignette_vitesse();
	this.instance_3.parent = this;
	this.instance_3.setTransform(361.3,849.5,0.875,0.875,0,0,0,0.1,320.1);

	var maskedShapeInstanceList = [this.instance_3];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_2;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({x:341.2},91).wait(1));

	// Layer_1
	this.instance_4 = new lib.vignette_vitesse();
	this.instance_4.parent = this;
	this.instance_4.setTransform(312.4,923,2.119,2.119,0,0,0,0.1,320.2);
	this.instance_4.alpha = 0.109;
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({_off:false},0).to({x:360.2},77).to({_off:true},1).wait(8));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(478.1,751.4,163.1,221.9);


(lib.Scene_1_Layer_8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_8
	this.instance = new lib.vignette1_vitesse();
	this.instance.parent = this;
	this.instance.setTransform(59.9,-125.8,0.7,0.7,0,0,0,0.4,320.4);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(21).to({_off:false},0).to({_off:true},96).wait(382));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_7
	this.instance = new lib.vignette2_action();
	this.instance.parent = this;
	this.instance.setTransform(-165.1,63.2,0.7,0.7,0,0,0,0.5,320.4);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(90).to({_off:false},0).to({_off:true},76).wait(333));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_6
	this.instance = new lib.vignette3_danger();
	this.instance.parent = this;
	this.instance.setTransform(158.4,-103.3,0.6,0.6,0,0,0,0.6,320.3);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(161).to({_off:false},0).to({_off:true},73).wait(265));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_5
	this.instance = new lib.txt_etbienplus();
	this.instance.parent = this;
	this.instance.setTransform(98.1,306.2,0.1,0.1,0,0,0,-311.6,-38.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(224).to({_off:false},0).wait(275));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.instance = new lib.txt_surInstagram();
	this.instance.parent = this;
	this.instance.setTransform(307.3,346.3,0.12,0.12);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(236).to({_off:false},0).wait(263));

}).prototype = p = new cjs.MovieClip();


(lib.BTN_follow = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{open:0});

	// timeline functions:
	this.frame_205 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(205).call(this.frame_205).wait(1));

	// bg_btn (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("AmVC2IAAlrIAEAAIAAFrg");
	var mask_graphics_1 = new cjs.Graphics().p("AmVC2IAAlrIASAAIAAFrg");
	var mask_graphics_2 = new cjs.Graphics().p("AmVC2IAAlrIAfAAIAAFrg");
	var mask_graphics_3 = new cjs.Graphics().p("AmVC2IAAlrIAtAAIAAFrg");
	var mask_graphics_4 = new cjs.Graphics().p("AmVC2IAAlrIA7AAIAAFrg");
	var mask_graphics_5 = new cjs.Graphics().p("AmVC2IAAlrIBIAAIAAFrg");
	var mask_graphics_6 = new cjs.Graphics().p("AmVC2IAAlrIBWAAIAAFrg");
	var mask_graphics_7 = new cjs.Graphics().p("AmVC2IAAlrIBkAAIAAFrg");
	var mask_graphics_8 = new cjs.Graphics().p("AmVC2IAAlrIByAAIAAFrg");
	var mask_graphics_9 = new cjs.Graphics().p("AmVC2IAAlrIB/AAIAAFrg");
	var mask_graphics_10 = new cjs.Graphics().p("AmVC2IAAlrICNAAIAAFrg");
	var mask_graphics_11 = new cjs.Graphics().p("AmVC2IAAlrICbAAIAAFrg");
	var mask_graphics_12 = new cjs.Graphics().p("AmVC2IAAlrICpAAIAAFrg");
	var mask_graphics_13 = new cjs.Graphics().p("AmVC2IAAlrIC2AAIAAFrg");
	var mask_graphics_14 = new cjs.Graphics().p("AmVC2IAAlrIDEAAIAAFrg");
	var mask_graphics_15 = new cjs.Graphics().p("AmVC2IAAlrIDSAAIAAFrg");
	var mask_graphics_16 = new cjs.Graphics().p("AmVC2IAAlrIDfAAIAAFrg");
	var mask_graphics_17 = new cjs.Graphics().p("AmVC2IAAlrIDtAAIAAFrg");
	var mask_graphics_18 = new cjs.Graphics().p("AmVC2IAAlrID7AAIAAFrg");
	var mask_graphics_19 = new cjs.Graphics().p("AmVC2IAAlrIEJAAIAAFrg");
	var mask_graphics_20 = new cjs.Graphics().p("AmVC2IAAlrIEWAAIAAFrg");
	var mask_graphics_21 = new cjs.Graphics().p("AmVC2IAAlrIEkAAIAAFrg");
	var mask_graphics_22 = new cjs.Graphics().p("AmVC2IAAlrIEyAAIAAFrg");
	var mask_graphics_23 = new cjs.Graphics().p("AmVC2IAAlrIE/AAIAAFrg");
	var mask_graphics_24 = new cjs.Graphics().p("AmVC2IAAlrIFNAAIAAFrg");
	var mask_graphics_25 = new cjs.Graphics().p("AmVC2IAAlrIFbAAIAAFrg");
	var mask_graphics_26 = new cjs.Graphics().p("AmVC2IAAlrIFpAAIAAFrg");
	var mask_graphics_27 = new cjs.Graphics().p("AmVC2IAAlrIF2AAIAAFrg");
	var mask_graphics_28 = new cjs.Graphics().p("AmVC2IAAlrIGEAAIAAFrg");
	var mask_graphics_29 = new cjs.Graphics().p("AmVC2IAAlrIGSAAIAAFrg");
	var mask_graphics_30 = new cjs.Graphics().p("AmVC2IAAlrIGfAAIAAFrg");
	var mask_graphics_31 = new cjs.Graphics().p("AmVC2IAAlrIGsAAIAAFrg");
	var mask_graphics_32 = new cjs.Graphics().p("AmVC2IAAlrIG6AAIAAFrg");
	var mask_graphics_33 = new cjs.Graphics().p("AmVC2IAAlrIHIAAIAAFrg");
	var mask_graphics_34 = new cjs.Graphics().p("AmVC2IAAlrIHVAAIAAFrg");
	var mask_graphics_35 = new cjs.Graphics().p("AmVC2IAAlrIHjAAIAAFrg");
	var mask_graphics_36 = new cjs.Graphics().p("AmVC2IAAlrIHxAAIAAFrg");
	var mask_graphics_37 = new cjs.Graphics().p("AmVC2IAAlrIH/AAIAAFrg");
	var mask_graphics_38 = new cjs.Graphics().p("AmVC2IAAlrIIMAAIAAFrg");
	var mask_graphics_39 = new cjs.Graphics().p("AmVC2IAAlrIIaAAIAAFrg");
	var mask_graphics_40 = new cjs.Graphics().p("AmVC2IAAlrIIoAAIAAFrg");
	var mask_graphics_41 = new cjs.Graphics().p("AmVC2IAAlrII1AAIAAFrg");
	var mask_graphics_42 = new cjs.Graphics().p("AmVC2IAAlrIJDAAIAAFrg");
	var mask_graphics_43 = new cjs.Graphics().p("AmVC2IAAlrIJRAAIAAFrg");
	var mask_graphics_44 = new cjs.Graphics().p("AmVC2IAAlrIJfAAIAAFrg");
	var mask_graphics_45 = new cjs.Graphics().p("AmVC2IAAlrIJsAAIAAFrg");
	var mask_graphics_46 = new cjs.Graphics().p("AmVC2IAAlrIJ6AAIAAFrg");
	var mask_graphics_47 = new cjs.Graphics().p("AmVC2IAAlrIKIAAIAAFrg");
	var mask_graphics_48 = new cjs.Graphics().p("AmVC2IAAlrIKVAAIAAFrg");
	var mask_graphics_49 = new cjs.Graphics().p("AmVC2IAAlrIKjAAIAAFrg");
	var mask_graphics_50 = new cjs.Graphics().p("AmVC2IAAlrIKxAAIAAFrg");
	var mask_graphics_51 = new cjs.Graphics().p("AmVC2IAAlrIK/AAIAAFrg");
	var mask_graphics_52 = new cjs.Graphics().p("AmVC2IAAlrILMAAIAAFrg");
	var mask_graphics_53 = new cjs.Graphics().p("AmVC2IAAlrILaAAIAAFrg");
	var mask_graphics_54 = new cjs.Graphics().p("AmVC2IAAlrILoAAIAAFrg");
	var mask_graphics_55 = new cjs.Graphics().p("AmVC2IAAlrIL2AAIAAFrg");
	var mask_graphics_56 = new cjs.Graphics().p("AmVC2IAAlrIMDAAIAAFrg");
	var mask_graphics_57 = new cjs.Graphics().p("AmVC2IAAlrIMRAAIAAFrg");
	var mask_graphics_58 = new cjs.Graphics().p("AmVC2IAAlrIMfAAIAAFrg");
	var mask_graphics_59 = new cjs.Graphics().p("AmWC2IAAlrIMsAAIAAFrg");
	var mask_graphics_60 = new cjs.Graphics().p("AmcC2IAAlrIM5AAIAAFrg");
	var mask_graphics_61 = new cjs.Graphics().p("AmjC2IAAlrINHAAIAAFrg");
	var mask_graphics_62 = new cjs.Graphics().p("AmqC2IAAlrINVAAIAAFrg");
	var mask_graphics_63 = new cjs.Graphics().p("AmxC2IAAlrINjAAIAAFrg");
	var mask_graphics_64 = new cjs.Graphics().p("Am4C2IAAlrINxAAIAAFrg");
	var mask_graphics_65 = new cjs.Graphics().p("Am/C2IAAlrIN/AAIAAFrg");
	var mask_graphics_66 = new cjs.Graphics().p("AnGC2IAAlrIONAAIAAFrg");
	var mask_graphics_67 = new cjs.Graphics().p("AnMC2IAAlrIOZAAIAAFrg");
	var mask_graphics_68 = new cjs.Graphics().p("AnTC2IAAlrIOnAAIAAFrg");
	var mask_graphics_69 = new cjs.Graphics().p("AnaC2IAAlrIO1AAIAAFrg");
	var mask_graphics_70 = new cjs.Graphics().p("AnhC2IAAlrIPDAAIAAFrg");
	var mask_graphics_71 = new cjs.Graphics().p("AnoC2IAAlrIPRAAIAAFrg");
	var mask_graphics_72 = new cjs.Graphics().p("AnvC2IAAlrIPfAAIAAFrg");
	var mask_graphics_73 = new cjs.Graphics().p("An2C2IAAlrIPtAAIAAFrg");
	var mask_graphics_74 = new cjs.Graphics().p("An8C2IAAlrIP5AAIAAFrg");
	var mask_graphics_75 = new cjs.Graphics().p("AoDC2IAAlrIQHAAIAAFrg");
	var mask_graphics_76 = new cjs.Graphics().p("AoKC2IAAlrIQVAAIAAFrg");
	var mask_graphics_77 = new cjs.Graphics().p("AoRC2IAAlrIQjAAIAAFrg");
	var mask_graphics_78 = new cjs.Graphics().p("AoYC2IAAlrIQxAAIAAFrg");
	var mask_graphics_79 = new cjs.Graphics().p("AofC2IAAlrIQ/AAIAAFrg");
	var mask_graphics_80 = new cjs.Graphics().p("AomC2IAAlrIRNAAIAAFrg");
	var mask_graphics_81 = new cjs.Graphics().p("AosC2IAAlrIRZAAIAAFrg");
	var mask_graphics_82 = new cjs.Graphics().p("AozC2IAAlrIRnAAIAAFrg");
	var mask_graphics_83 = new cjs.Graphics().p("Ao6C2IAAlrIR1AAIAAFrg");
	var mask_graphics_84 = new cjs.Graphics().p("ApBC2IAAlrISDAAIAAFrg");
	var mask_graphics_85 = new cjs.Graphics().p("ApIC2IAAlrISRAAIAAFrg");
	var mask_graphics_86 = new cjs.Graphics().p("ApPC2IAAlrISfAAIAAFrg");
	var mask_graphics_87 = new cjs.Graphics().p("ApWC2IAAlrIStAAIAAFrg");
	var mask_graphics_88 = new cjs.Graphics().p("ApcC2IAAlrIS5AAIAAFrg");
	var mask_graphics_89 = new cjs.Graphics().p("ApjC2IAAlrITHAAIAAFrg");
	var mask_graphics_90 = new cjs.Graphics().p("ApqC2IAAlrITVAAIAAFrg");
	var mask_graphics_91 = new cjs.Graphics().p("ApxC2IAAlrITjAAIAAFrg");
	var mask_graphics_92 = new cjs.Graphics().p("Ap4C2IAAlrITxAAIAAFrg");
	var mask_graphics_93 = new cjs.Graphics().p("Ap/C2IAAlrIT/AAIAAFrg");
	var mask_graphics_94 = new cjs.Graphics().p("AqGC2IAAlrIUNAAIAAFrg");
	var mask_graphics_95 = new cjs.Graphics().p("AqMC2IAAlrIUZAAIAAFrg");
	var mask_graphics_96 = new cjs.Graphics().p("AqTC2IAAlrIUnAAIAAFrg");
	var mask_graphics_97 = new cjs.Graphics().p("AqaC2IAAlrIU1AAIAAFrg");
	var mask_graphics_98 = new cjs.Graphics().p("AqhC2IAAlrIVDAAIAAFrg");
	var mask_graphics_99 = new cjs.Graphics().p("AqoC2IAAlrIVRAAIAAFrg");
	var mask_graphics_100 = new cjs.Graphics().p("AqvC2IAAlrIVfAAIAAFrg");
	var mask_graphics_101 = new cjs.Graphics().p("Aq2C2IAAlrIVtAAIAAFrg");
	var mask_graphics_102 = new cjs.Graphics().p("Aq8C2IAAlrIV5AAIAAFrg");
	var mask_graphics_103 = new cjs.Graphics().p("ArDC2IAAlrIWHAAIAAFrg");
	var mask_graphics_104 = new cjs.Graphics().p("ArKC2IAAlrIWVAAIAAFrg");
	var mask_graphics_105 = new cjs.Graphics().p("ArRC2IAAlrIWjAAIAAFrg");
	var mask_graphics_106 = new cjs.Graphics().p("ArYC2IAAlrIWxAAIAAFrg");
	var mask_graphics_107 = new cjs.Graphics().p("ArfC2IAAlrIW/AAIAAFrg");
	var mask_graphics_108 = new cjs.Graphics().p("ArmC2IAAlrIXNAAIAAFrg");
	var mask_graphics_109 = new cjs.Graphics().p("ArtC2IAAlrIXbAAIAAFrg");
	var mask_graphics_110 = new cjs.Graphics().p("ArzC2IAAlrIXnAAIAAFrg");
	var mask_graphics_111 = new cjs.Graphics().p("Ar6C2IAAlrIX1AAIAAFrg");
	var mask_graphics_112 = new cjs.Graphics().p("AsBC2IAAlrIYDAAIAAFrg");
	var mask_graphics_113 = new cjs.Graphics().p("AsIC2IAAlrIYRAAIAAFrg");
	var mask_graphics_114 = new cjs.Graphics().p("AsPC2IAAlrIYfAAIAAFrg");
	var mask_graphics_115 = new cjs.Graphics().p("AsWC2IAAlrIYtAAIAAFrg");
	var mask_graphics_116 = new cjs.Graphics().p("AsdC2IAAlrIY7AAIAAFrg");
	var mask_graphics_117 = new cjs.Graphics().p("AsjC2IAAlrIZHAAIAAFrg");
	var mask_graphics_118 = new cjs.Graphics().p("AsqC2IAAlrIZVAAIAAFrg");
	var mask_graphics_119 = new cjs.Graphics().p("AsqCtIAAlHIZVAAIAAFHg");
	var mask_graphics_120 = new cjs.Graphics().p("AsqCkIAAkjIZVAAIAAEjg");
	var mask_graphics_121 = new cjs.Graphics().p("AsqCbIAAj/IZVAAIAAD/g");
	var mask_graphics_122 = new cjs.Graphics().p("AsqCSIAAjbIZVAAIAADbg");
	var mask_graphics_123 = new cjs.Graphics().p("AsqCJIAAi3IZVAAIAAC3g");
	var mask_graphics_124 = new cjs.Graphics().p("AsqCAIAAiTIZVAAIAACTg");
	var mask_graphics_125 = new cjs.Graphics().p("AsqB3IAAhwIZVAAIAABwg");
	var mask_graphics_126 = new cjs.Graphics().p("AsqBuIAAhMIZVAAIAABMg");
	var mask_graphics_127 = new cjs.Graphics().p("AsqBlIAAgoIZVAAIAAAog");
	var mask_graphics_128 = new cjs.Graphics().p("AsqBcIAAgEIZVAAIAAAEg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_1,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_2,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_3,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_4,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_5,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_6,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_7,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_8,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_9,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_10,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_11,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_12,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_13,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_14,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_15,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_16,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_17,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_18,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_19,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_20,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_21,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_22,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_23,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_24,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_25,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_26,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_27,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_28,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_29,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_30,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_31,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_32,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_33,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_34,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_35,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_36,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_37,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_38,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_39,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_40,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_41,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_42,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_43,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_44,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_45,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_46,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_47,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_48,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_49,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_50,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_51,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_52,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_53,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_54,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_55,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_56,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_57,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_58,x:-40.6,y:18.2}).wait(1).to({graphics:mask_graphics_59,x:-40.5,y:18.2}).wait(1).to({graphics:mask_graphics_60,x:-39.8,y:18.2}).wait(1).to({graphics:mask_graphics_61,x:-39.1,y:18.2}).wait(1).to({graphics:mask_graphics_62,x:-38.4,y:18.2}).wait(1).to({graphics:mask_graphics_63,x:-37.7,y:18.2}).wait(1).to({graphics:mask_graphics_64,x:-37,y:18.2}).wait(1).to({graphics:mask_graphics_65,x:-36.3,y:18.2}).wait(1).to({graphics:mask_graphics_66,x:-35.7,y:18.2}).wait(1).to({graphics:mask_graphics_67,x:-35,y:18.2}).wait(1).to({graphics:mask_graphics_68,x:-34.3,y:18.2}).wait(1).to({graphics:mask_graphics_69,x:-33.6,y:18.2}).wait(1).to({graphics:mask_graphics_70,x:-32.9,y:18.2}).wait(1).to({graphics:mask_graphics_71,x:-32.2,y:18.2}).wait(1).to({graphics:mask_graphics_72,x:-31.5,y:18.2}).wait(1).to({graphics:mask_graphics_73,x:-30.9,y:18.2}).wait(1).to({graphics:mask_graphics_74,x:-30.2,y:18.2}).wait(1).to({graphics:mask_graphics_75,x:-29.5,y:18.2}).wait(1).to({graphics:mask_graphics_76,x:-28.8,y:18.2}).wait(1).to({graphics:mask_graphics_77,x:-28.1,y:18.2}).wait(1).to({graphics:mask_graphics_78,x:-27.4,y:18.2}).wait(1).to({graphics:mask_graphics_79,x:-26.7,y:18.2}).wait(1).to({graphics:mask_graphics_80,x:-26.1,y:18.2}).wait(1).to({graphics:mask_graphics_81,x:-25.4,y:18.2}).wait(1).to({graphics:mask_graphics_82,x:-24.7,y:18.2}).wait(1).to({graphics:mask_graphics_83,x:-24,y:18.2}).wait(1).to({graphics:mask_graphics_84,x:-23.3,y:18.2}).wait(1).to({graphics:mask_graphics_85,x:-22.6,y:18.2}).wait(1).to({graphics:mask_graphics_86,x:-21.9,y:18.2}).wait(1).to({graphics:mask_graphics_87,x:-21.2,y:18.2}).wait(1).to({graphics:mask_graphics_88,x:-20.6,y:18.2}).wait(1).to({graphics:mask_graphics_89,x:-19.9,y:18.2}).wait(1).to({graphics:mask_graphics_90,x:-19.2,y:18.2}).wait(1).to({graphics:mask_graphics_91,x:-18.5,y:18.2}).wait(1).to({graphics:mask_graphics_92,x:-17.8,y:18.2}).wait(1).to({graphics:mask_graphics_93,x:-17.1,y:18.2}).wait(1).to({graphics:mask_graphics_94,x:-16.4,y:18.2}).wait(1).to({graphics:mask_graphics_95,x:-15.8,y:18.2}).wait(1).to({graphics:mask_graphics_96,x:-15.1,y:18.2}).wait(1).to({graphics:mask_graphics_97,x:-14.4,y:18.2}).wait(1).to({graphics:mask_graphics_98,x:-13.7,y:18.2}).wait(1).to({graphics:mask_graphics_99,x:-13,y:18.2}).wait(1).to({graphics:mask_graphics_100,x:-12.3,y:18.2}).wait(1).to({graphics:mask_graphics_101,x:-11.6,y:18.2}).wait(1).to({graphics:mask_graphics_102,x:-11,y:18.2}).wait(1).to({graphics:mask_graphics_103,x:-10.3,y:18.2}).wait(1).to({graphics:mask_graphics_104,x:-9.6,y:18.2}).wait(1).to({graphics:mask_graphics_105,x:-8.9,y:18.2}).wait(1).to({graphics:mask_graphics_106,x:-8.2,y:18.2}).wait(1).to({graphics:mask_graphics_107,x:-7.5,y:18.2}).wait(1).to({graphics:mask_graphics_108,x:-6.8,y:18.2}).wait(1).to({graphics:mask_graphics_109,x:-6.2,y:18.2}).wait(1).to({graphics:mask_graphics_110,x:-5.5,y:18.2}).wait(1).to({graphics:mask_graphics_111,x:-4.8,y:18.2}).wait(1).to({graphics:mask_graphics_112,x:-4.1,y:18.2}).wait(1).to({graphics:mask_graphics_113,x:-3.4,y:18.2}).wait(1).to({graphics:mask_graphics_114,x:-2.7,y:18.2}).wait(1).to({graphics:mask_graphics_115,x:-2,y:18.2}).wait(1).to({graphics:mask_graphics_116,x:-1.3,y:18.2}).wait(1).to({graphics:mask_graphics_117,x:-0.7,y:18.2}).wait(1).to({graphics:mask_graphics_118,x:0,y:18.2}).wait(1).to({graphics:mask_graphics_119,x:0,y:17.3}).wait(1).to({graphics:mask_graphics_120,x:0,y:16.4}).wait(1).to({graphics:mask_graphics_121,x:0,y:15.5}).wait(1).to({graphics:mask_graphics_122,x:0,y:14.6}).wait(1).to({graphics:mask_graphics_123,x:0,y:13.7}).wait(1).to({graphics:mask_graphics_124,x:0,y:12.8}).wait(1).to({graphics:mask_graphics_125,x:0,y:11.9}).wait(1).to({graphics:mask_graphics_126,x:0,y:11}).wait(1).to({graphics:mask_graphics_127,x:0,y:10.1}).wait(1).to({graphics:mask_graphics_128,x:0,y:9.2}).wait(78));

	// JE_FOLLOW_DIRECT
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgHAkIAAg5IgVAAIAAgOIA5AAIAAAOIgVAAIAAA5g");
	this.shape.setTransform(50,17.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgWAbQgLgLAAgQQAAgPALgLQALgLAQAAQARAAAMAOIgKALQgIgJgLAAQgIAAgHAGQgGAGAAAJQAAAKAGAGQAGAGAIAAQAMAAAIgJIAKALQgNAOgQAAQgQAAgLgLg");
	this.shape_1.setTransform(43.1,17.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgaAkIAAhHIA0AAIAAAOIgkAAIAAAPIAgAAIAAANIggAAIAAAPIAlAAIAAAOg");
	this.shape_2.setTransform(36,17.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAMAkIgPgXIgMAAIAAAXIgQAAIAAhHIAcAAQAQAAAHAFQAHAGAAANQAAAQgOAFIATAagAgPAAIAMAAQAIAAADgDQADgCAAgGQAAgGgEgCQgDgCgGAAIgNAAg");
	this.shape_3.setTransform(29,17.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgHAkIAAhHIAPAAIAABHg");
	this.shape_4.setTransform(23.2,17.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AggAkIAAhHIAZAAQATAAALAJQAKAKAAAQQAAAQgKAKQgLAKgTAAgAgQAWIAKAAQALAAAFgGQAHgFAAgLQAAgKgHgGQgFgFgMAAIgJAAg");
	this.shape_5.setTransform(17.7,17.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAQAkIgQgxIgPAxIgMAAIgZhHIASAAIANAoIAOgoIAQAAIAMAoIAPgoIASAAIgaBHg");
	this.shape_6.setTransform(5.5,17.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgaAbQgLgLAAgQQAAgPALgKQALgLAPAAQAQAAALALQALAKAAAPQAAAQgLALQgLAKgQABQgPgBgLgKgAgPgQQgFAIAAAIQAAAKAFAHQAHAHAIgBQAJABAHgHQAFgHAAgKQAAgIgFgIQgHgGgJAAQgIAAgHAGg");
	this.shape_7.setTransform(-4.1,17.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgXAkIAAhHIARAAIAAA5IAdAAIAAAOg");
	this.shape_8.setTransform(-11,17.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgWAkIAAhHIAPAAIAAA5IAfAAIAAAOg");
	this.shape_9.setTransform(-16.9,17.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgaAbQgLgLAAgQQAAgPALgKQALgLAPAAQAQAAALALQALAKAAAPQAAAQgLALQgLAKgQABQgPgBgLgKgAgOgQQgHAIABAIQgBAKAHAHQAGAHAIgBQAJABAGgHQAGgHABgKQgBgIgGgIQgGgGgJAAQgIAAgGAGg");
	this.shape_10.setTransform(-24.5,17.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgYAkIAAhHIAxAAIAAAOIghAAIAAAPIAfAAIAAAOIgfAAIAAAcg");
	this.shape_11.setTransform(-31.7,17.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgaAkIAAhHIA0AAIAAAOIgkAAIAAAPIAgAAIAAANIggAAIAAAPIAlAAIAAAOg");
	this.shape_12.setTransform(-41.1,17.9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgXAbIAJgMQAHAHAHAAQACAAACgDQADgCAAgGIAAghIgVAAIAAgNIAlAAIAAAtQABANgHAHQgHAGgKABQgMAAgLgKg");
	this.shape_13.setTransform(-47.8,18);

	var maskedShapeInstanceList = [this.shape,this.shape_1,this.shape_2,this.shape_3,this.shape_4,this.shape_5,this.shape_6,this.shape_7,this.shape_8,this.shape_9,this.shape_10,this.shape_11,this.shape_12,this.shape_13];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},118).to({state:[]},21).wait(67));

	// bg_btn
	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#CC9933").ss(1,1,1).p("Asqi1IZVAAIAAFrI5VAAg");
	this.shape_14.setTransform(0,18.2);

	this.timeline.addTween(cjs.Tween.get(this.shape_14).to({_off:true},119).wait(87));

	// bg_btn
	this.instance = new lib.bg_btn();
	this.instance.parent = this;
	this.instance.setTransform(-80.9,18.2,0.002,1,0,0,0,0,18.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1,x:0},118).to({regY:18.1,scaleY:0.01,y:18.3},10,cjs.Ease.quartOut).to({_off:true},11).wait(67));

	// JE_FOLLOW_DIRECT
	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("rgba(0,0,0,0.008)").s().p("AsoC4IAAlvIZRAAIAAFvg");
	this.shape_15.setTransform(-0.1,18.1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#CC9933").s().p("AgHAkIAAg5IgVAAIAAgOIA5AAIAAAOIgVAAIAAA5g");
	this.shape_16.setTransform(50,17.9);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#CC9933").s().p("AgWAbQgLgLAAgQQAAgPALgLQALgLAQAAQARAAAMAOIgKALQgIgJgLAAQgIAAgHAGQgGAGAAAJQAAAKAGAGQAGAGAIAAQAMAAAIgJIAKALQgNAOgQAAQgQAAgLgLg");
	this.shape_17.setTransform(43.1,17.9);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#CC9933").s().p("AgaAkIAAhHIA0AAIAAAOIgkAAIAAAPIAgAAIAAANIggAAIAAAPIAlAAIAAAOg");
	this.shape_18.setTransform(36,17.9);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#CC9933").s().p("AAMAkIgPgXIgMAAIAAAXIgQAAIAAhHIAcAAQAQAAAHAFQAHAGAAANQAAAQgOAFIATAagAgPAAIAMAAQAIAAADgDQADgCAAgGQAAgGgEgCQgDgCgGAAIgNAAg");
	this.shape_19.setTransform(29,17.9);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#CC9933").s().p("AgHAkIAAhHIAPAAIAABHg");
	this.shape_20.setTransform(23.2,17.9);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#CC9933").s().p("AggAkIAAhHIAZAAQATAAALAJQAKAKAAAQQAAAQgKAKQgLAKgTAAgAgQAWIAKAAQALAAAFgGQAHgFAAgLQAAgKgHgGQgFgFgMAAIgJAAg");
	this.shape_21.setTransform(17.7,17.9);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#CC9933").s().p("AAQAkIgQgxIgPAxIgMAAIgZhHIASAAIANAoIAOgoIAQAAIAMAoIAPgoIASAAIgaBHg");
	this.shape_22.setTransform(5.5,17.9);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#CC9933").s().p("AgaAbQgLgLAAgQQAAgPALgKQALgLAPAAQAQAAALALQALAKAAAPQAAAQgLALQgLAKgQABQgPgBgLgKgAgPgQQgFAIAAAIQAAAKAFAHQAHAHAIgBQAJABAHgHQAFgHAAgKQAAgIgFgIQgHgGgJAAQgIAAgHAGg");
	this.shape_23.setTransform(-4.1,17.9);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#CC9933").s().p("AgXAkIAAhHIARAAIAAA5IAdAAIAAAOg");
	this.shape_24.setTransform(-11,17.9);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#CC9933").s().p("AgWAkIAAhHIAPAAIAAA5IAfAAIAAAOg");
	this.shape_25.setTransform(-16.9,17.9);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#CC9933").s().p("AgaAbQgLgLAAgQQAAgPALgKQALgLAPAAQAQAAALALQALAKAAAPQAAAQgLALQgLAKgQABQgPgBgLgKgAgOgQQgHAIABAIQgBAKAHAHQAGAHAIgBQAJABAGgHQAGgHABgKQgBgIgGgIQgGgGgJAAQgIAAgGAGg");
	this.shape_26.setTransform(-24.5,17.9);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#CC9933").s().p("AgYAkIAAhHIAxAAIAAAOIghAAIAAAPIAfAAIAAAOIgfAAIAAAcg");
	this.shape_27.setTransform(-31.7,17.9);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#CC9933").s().p("AgaAkIAAhHIA0AAIAAAOIgkAAIAAAPIAgAAIAAANIggAAIAAAPIAlAAIAAAOg");
	this.shape_28.setTransform(-41.1,17.9);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#CC9933").s().p("AgXAbIAJgMQAHAHAHAAQACAAACgDQADgCAAgGIAAghIgVAAIAAgNIAlAAIAAAtQABANgHAHQgHAGgKABQgMAAgLgKg");
	this.shape_29.setTransform(-47.8,18);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#000000").ss(1,1,1).p("Asoi3IZRAAIAAFvI5RAAg");
	this.shape_30.setTransform(-0.1,18.1);

	this.instance_1 = new lib.Tween1("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-1.8,17.9);
	this.instance_1.alpha = 0.5;
	this.instance_1._off = true;

	this.instance_2 = new lib.Tween2("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-1.8,17.9);
	this.instance_2.alpha = 0;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15}]}).to({state:[{t:this.instance_1}]},118).to({state:[{t:this.instance_1}]},30).to({state:[{t:this.instance_2}]},56).to({state:[]},1).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(118).to({_off:false},0).to({alpha:1},30).to({_off:true,alpha:0},56).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-82.1,-1.3,164.3,38.8);


(lib.bloc_lignes = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{close_OLD:166,close:209});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_165 = function() {
		this.stop();
	}
	this.frame_208 = function() {
		this.stop();
	}
	this.frame_229 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(165).call(this.frame_165).wait(43).call(this.frame_208).wait(21).call(this.frame_229).wait(1));

	// ligne
	this.instance = new lib.ligne();
	this.instance.parent = this;
	this.instance.setTransform(-454,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(166).to({x:-883},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:-454},0).wait(21));

	// ligne
	this.instance_1 = new lib.ligne();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-227,406.5,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(119).to({y:320.5},40,cjs.Ease.quartInOut).wait(7).to({y:406.5},0).to({x:-883.2},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:-227,y:320.5},0).to({y:406.5},17,cjs.Ease.quartInOut).wait(4));

	// ligne
	this.instance_2 = new lib.ligne();
	this.instance_2.parent = this;
	this.instance_2.setTransform(0,406.5,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(166).to({x:-883.4},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:0},0).wait(21));

	// ligne
	this.instance_3 = new lib.ligne();
	this.instance_3.parent = this;
	this.instance_3.setTransform(227,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(124).to({y:320},40,cjs.Ease.quartInOut).to({_off:true},2).wait(43).to({_off:false},0).to({y:406},17,cjs.Ease.quartInOut).wait(4));

	// ligne
	this.instance_4 = new lib.ligne();
	this.instance_4.parent = this;
	this.instance_4.setTransform(454.1,406,1,1,0,0,0,0,406);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(166).to({x:-883.4},40,cjs.Ease.quartInOut).to({_off:true},1).wait(2).to({_off:false,x:454.1},0).wait(21));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-455.4,-174.5,910.5,988);


(lib.Scene_1_Ligne = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Ligne
	this.lignes_mc = new lib.bloc_lignes();
	this.lignes_mc.name = "lignes_mc";
	this.lignes_mc.parent = this;
	this.lignes_mc.setTransform(240.8,453.9,0.525,1,0,0,0,0.7,405.9);

	this.timeline.addTween(cjs.Tween.get(this.lignes_mc).wait(499));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.cta_mc = new lib.BTN_follow();
	this.cta_mc.name = "cta_mc";
	this.cta_mc.parent = this;
	this.cta_mc.setTransform(774.3,444.4,1.15,1.15,0,0,0,0.1,18.2);

	this.timeline.addTween(cjs.Tween.get(this.cta_mc).wait(260).to({x:239.9,y:417.2},0).wait(239));

}).prototype = p = new cjs.MovieClip();


// stage content:
(lib.FS_interlude_instagram_mobile = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	this.___GetDepth___ = function(obj) {
		var depth = obj.depth;
		var cameraObj = this.___camera___instance;
		if(cameraObj && cameraObj.depth && obj.isAttachedToCamera)
		{
			depth += depth + cameraObj.depth;
		}
		return depth;
		}
	this.___needSorting___ = function() {
		for (var i = 0; i < this.getNumChildren() - 1; i++)
		{
			var prevDepth = this.___GetDepth___(this.getChildAt(i));
			var nextDepth = this.___GetDepth___(this.getChildAt(i + 1));
			if (prevDepth < nextDepth)
				return true;
		}
		return false;
	}
	this.___sortFunction___ = function(obj1, obj2) {
		return (this.exportRoot.___GetDepth___(obj2) - this.exportRoot.___GetDepth___(obj1));
	}
	this.on('tick', function (event){
		var curTimeline = event.currentTarget;
		if (curTimeline.___needSorting___()){
			this.sortChildren(curTimeline.___sortFunction___);
		}
	});

	// timeline functions:
	this.frame_0 = function() {
		this.cta_mc = this.Layer_3.cta_mc;
		this.lignes_mc = this.Ligne.lignes_mc;
		this.lignes_mc.gotoAndPlay(1);
		this.cta_mc.addEventListener("click", fl_ClickToGoToWebPage);
		
		function fl_ClickToGoToWebPage() {
			window.open("https://www.instagram.com/collectif_fullstory/", "_blank");
		}
	}
	this.frame_260 = function() {
		this.cta_mc = undefined;this.cta_mc = this.Layer_3.cta_mc;
		/* Click to Go to Web Page
		Clicking on the specified symbol instance loads the URL in a new browser window.
		
		Instructions:
		1. Replace http://www.adobe.com with the desired URL address.
		   Keep the quotation marks ("").
		*/
		
		
		this.cta_mc.gotoAndPlay("open");
	}
	this.frame_339 = function() {
		this.lignes_mc.gotoAndPlay("close");
	}
	this.frame_439 = function() {
		var event = new Event('next');
		this.dispatchEvent(event);		
		event = null;
	}
	this.frame_498 = function() {
		this.___loopingOver___ = true;
		// EVENT DE FIN D'ANIM
		var event = new Event('closed');
		this.dispatchEvent(event);		
		event = null;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(260).call(this.frame_260).wait(79).call(this.frame_339).wait(100).call(this.frame_439).wait(59).call(this.frame_498).wait(1));

	// Layer_2_obj_
	this.Layer_2 = new lib.Scene_1_Layer_2();
	this.Layer_2.name = "Layer_2";
	this.Layer_2.parent = this;
	this.Layer_2.setTransform(396.4,481.4,1,1,0,0,0,396.4,481.4);
	this.Layer_2.depth = 0;
	this.Layer_2.isAttachedToCamera = 0
	this.Layer_2.isAttachedToMask = 0
	this.Layer_2.layerDepth = 0
	this.Layer_2.layerIndex = 0
	this.Layer_2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_2).wait(499));

	// Layer_3_obj_
	this.Layer_3 = new lib.Scene_1_Layer_3();
	this.Layer_3.name = "Layer_3";
	this.Layer_3.parent = this;
	this.Layer_3.setTransform(774.2,444.3,1,1,0,0,0,774.2,444.3);
	this.Layer_3.depth = 0;
	this.Layer_3.isAttachedToCamera = 0
	this.Layer_3.isAttachedToMask = 0
	this.Layer_3.layerDepth = 0
	this.Layer_3.layerIndex = 1
	this.Layer_3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_3).wait(499));

	// Layer_4_obj_
	this.Layer_4 = new lib.Scene_1_Layer_4();
	this.Layer_4.name = "Layer_4";
	this.Layer_4.parent = this;
	this.Layer_4.depth = 0;
	this.Layer_4.isAttachedToCamera = 0
	this.Layer_4.isAttachedToMask = 0
	this.Layer_4.layerDepth = 0
	this.Layer_4.layerIndex = 2
	this.Layer_4.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_4).wait(499));

	// Layer_5_obj_
	this.Layer_5 = new lib.Scene_1_Layer_5();
	this.Layer_5.name = "Layer_5";
	this.Layer_5.parent = this;
	this.Layer_5.depth = 0;
	this.Layer_5.isAttachedToCamera = 0
	this.Layer_5.isAttachedToMask = 0
	this.Layer_5.layerDepth = 0
	this.Layer_5.layerIndex = 3
	this.Layer_5.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_5).wait(499));

	// Layer_1 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("EglLBBoMAAAiDPMBKXAAAMAAACDPg");
	mask.setTransform(240,420);

	// Layer_6_obj_
	this.Layer_6 = new lib.Scene_1_Layer_6();
	this.Layer_6.name = "Layer_6";
	this.Layer_6.parent = this;
	this.Layer_6.depth = 0;
	this.Layer_6.isAttachedToCamera = 0
	this.Layer_6.isAttachedToMask = 0
	this.Layer_6.layerDepth = 0
	this.Layer_6.layerIndex = 4
	this.Layer_6.maskLayerName = 0

	var maskedShapeInstanceList = [this.Layer_6];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.Layer_6).wait(499));

	// Layer_7_obj_
	this.Layer_7 = new lib.Scene_1_Layer_7();
	this.Layer_7.name = "Layer_7";
	this.Layer_7.parent = this;
	this.Layer_7.depth = 0;
	this.Layer_7.isAttachedToCamera = 0
	this.Layer_7.isAttachedToMask = 0
	this.Layer_7.layerDepth = 0
	this.Layer_7.layerIndex = 5
	this.Layer_7.maskLayerName = 0

	var maskedShapeInstanceList = [this.Layer_7];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.Layer_7).wait(499));

	// Layer_8_obj_
	this.Layer_8 = new lib.Scene_1_Layer_8();
	this.Layer_8.name = "Layer_8";
	this.Layer_8.parent = this;
	this.Layer_8.depth = 0;
	this.Layer_8.isAttachedToCamera = 0
	this.Layer_8.isAttachedToMask = 0
	this.Layer_8.layerDepth = 0
	this.Layer_8.layerIndex = 6
	this.Layer_8.maskLayerName = 0

	var maskedShapeInstanceList = [this.Layer_8];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.Layer_8).wait(499));

	// Ligne_obj_
	this.Ligne = new lib.Scene_1_Ligne();
	this.Ligne.name = "Ligne";
	this.Ligne.parent = this;
	this.Ligne.setTransform(240.3,367.5,1,1,0,0,0,240.3,367.5);
	this.Ligne.depth = 0;
	this.Ligne.isAttachedToCamera = 0
	this.Ligne.isAttachedToMask = 0
	this.Ligne.layerDepth = 0
	this.Ligne.layerIndex = 7
	this.Ligne.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Ligne).wait(499));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(241.3,293.5,866.8,988);
// library properties:
lib.properties = {
	id: '8FA82CDC141CFF49AE0BFC91675C14F9',
	width: 480,
	height: 840,
	fps: 60,
	color: "#000000",
	opacity: 1.00,
	manifest: [
		{src:"images/action.jpg?1540471576009", id:"action"},
		{src:"images/danger.jpg?1540471576009", id:"danger"},
		{src:"images/fire_0000_Layer8.png?1540471576009", id:"fire_0000_Layer8"},
		{src:"images/fire_0001_Layer9.png?1540471576009", id:"fire_0001_Layer9"},
		{src:"images/fire_0002_Layer10.png?1540471576009", id:"fire_0002_Layer10"},
		{src:"images/fire_0003_Layer11.png?1540471576009", id:"fire_0003_Layer11"},
		{src:"images/fire_0004_Layer12.png?1540471576009", id:"fire_0004_Layer12"},
		{src:"images/fire_0005_Layer13.png?1540471576009", id:"fire_0005_Layer13"},
		{src:"images/fire_0006_Layer14.png?1540471576009", id:"fire_0006_Layer14"},
		{src:"images/fire_0007_Layer15.png?1540471576009", id:"fire_0007_Layer15"},
		{src:"images/fire_0008_Layer16.png?1540471576009", id:"fire_0008_Layer16"},
		{src:"images/fire_0009_Layer17.png?1540471576009", id:"fire_0009_Layer17"},
		{src:"images/fire_0010_Layer1.png?1540471576009", id:"fire_0010_Layer1"},
		{src:"images/fire_0011_Layer2.png?1540471576009", id:"fire_0011_Layer2"},
		{src:"images/fire_0012_Layer3.png?1540471576009", id:"fire_0012_Layer3"},
		{src:"images/fire_0013_Layer4.png?1540471576009", id:"fire_0013_Layer4"},
		{src:"images/fire_0014_Layer5.png?1540471576009", id:"fire_0014_Layer5"},
		{src:"images/fire_0015_Layer6.png?1540471576009", id:"fire_0015_Layer6"},
		{src:"images/fire_0016_Layer7.png?1540471576009", id:"fire_0016_Layer7"},
		{src:"images/vitesse.jpg?1540471576009", id:"vitesse"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['8FA82CDC141CFF49AE0BFC91675C14F9'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


// Layer depth API : 

AdobeAn.Layer = new function() {
	this.getLayerZDepth = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth; else 0;";
		return eval(script);
	}
	this.setLayerZDepth = function(timeline, layerName, zDepth)
	{
		const MAX_zDepth = 10000;
		const MIN_zDepth = -5000;
		if(zDepth > MAX_zDepth)
			zDepth = MAX_zDepth;
		else if(zDepth < MIN_zDepth)
			zDepth = MIN_zDepth;
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth = " + zDepth + ";";
		eval(script);
	}
	this.removeLayer = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline.removeChild(timeline." + layerName + ");";
		eval(script);
	}
	this.addNewLayer = function(timeline, layerName, zDepth)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		zDepth = typeof zDepth !== 'undefined' ? zDepth : 0;
		var layer = new createjs.MovieClip();
		layer.name = layerName;
		layer.depth = zDepth;
		layer.layerIndex = 0;
		timeline.addChild(layer);
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;