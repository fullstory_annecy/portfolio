// vue.config.js
const path = require("path");

module.exports = {
    baseUrl: '/',
    devServer: {
        index: 'index.html'
    },
    pwa : {
        themeColor : "#000000"
    },
    pages: {
        index: {
            entry: 'src/main.js',
            template: 'public/index.html',
            filename: 'index.html',
            title: 'fullstory',
            chunks: ['chunk-vendors', 'chunk-common', 'index']
        },
        bestof: {
            entry: 'src/single-page.js',
            template: 'public/best-of/2019.html',
            filename: 'best-of/2019.html',
            title: 'fullstory',
            chunks: ['chunk-vendors', 'chunk-common','bestof']
        }
    },
    chainWebpack: config => {

    }, configureWebpack: {
        resolve: {
            alias: {
                assets: path.resolve(__dirname, 'src/assets/'),
                createjs : path.resolve(__dirname, 'src/vendor/create.js')
            }
        },
        module: {
            rules: [
                {
                    test: require.resolve('./src/vendor/create.js'),
                    loaders: [
                        'imports-loader?this=>window',
                        'exports-loader?window.createjs'
                    ]
                },
            ]
        },
        plugins: [
        ]
    }
};