import createjs from "createjs";
import MobileDetect from "mobile-detect";

class Playlist {
    constructor(config) {
        this.isFirstTime = true;
        this.config = config;
        this.currentInterlude = 0;
        this.mobileDetect = new MobileDetect(window.navigator.userAgent);
        this.createPlaylist();
        this.lastPrefix = null;
        window.addEventListener("orientationchange", this.onOrientationChanged.bind(this), false);
    }

    shuffle(arra1) {
        let ctr = arra1.length;
        let temp;
        let index;

        while (ctr > 0) {
            index = Math.floor(Math.random() * ctr);
            ctr--;
            temp = arra1[ctr];
            arra1[ctr] = arra1[index];
            arra1[index] = temp;
        }
        return arra1;
    }

    onOrientationChanged(e) {
        setTimeout(() => {
            if(this.getPrefix(true) !== this.lastPrefix) {
                window.location.reload();
            }
        },500);
    }

    createPlaylist(type = "explore") {
        this.currentPageIndex = -1;
        this.list = [];

        if (type === "explore") {
            this.list.push(this.config.static.intro);
            let statics = [...this.config.static.members];
            statics.push({path : "interludes/0"});
            statics.push({path : "interludes/1"});
            statics = this.shuffle(statics);
            let projects = this.shuffle(this.config.explore);

            let index = 0;
            projects.forEach(page => {
                this.list.push(page);
                if (index % 2 === 1 && statics.length) {
                    this.list.push(statics.pop());
                }
                index++;
            });

            if (statics.length) {
                this.list = this.list.concat(statics);
            }
        } else if (type === "contact") {
            this.list.push(this.config.static.contact);
            this.config.static.members.forEach(memberPage => {
                this.list.push(memberPage);
            });
        }

    }

    getPrefix(readOnly = false) {
        let isMobile = this.mobileDetect.phone();
        let isTablet = this.mobileDetect.tablet();

        let prefix = isMobile || (isTablet && window.innerHeight > window.innerWidth)
            ? "mobile"
            : "desktop";

        if(!readOnly) {
            this.lastPrefix = prefix;
        }

        return prefix;
    }

    getAnimationURL(data) {
        return (
            data[this.getPrefix()].url + "?r=" + (Math.random() * 9999).toFixed(0)
        );
    }

    loadManifest(data) {
        let comp = AdobeAn.getComposition(data.id);
        let lib = comp.getLibrary();

        return new Promise(resolve => {
            let queue = new createjs.LoadQueue();
            queue.on("fileload", evt => {
                let images = comp.getImages();
                if (evt && evt.item.type === "image") {
                    images[evt.item.id] = evt.result;
                }
            });

            queue.on("complete", evt => {
                let ss = comp.getSpriteSheet();
                let queue = evt.target;
                let ssMetadata = lib.ssMetadata;
                for (let i = 0; i < ssMetadata.length; i++) {
                    ss[ssMetadata[i].name] = new createjs.SpriteSheet({
                        images: [queue.getResult(ssMetadata[i].name)],
                        frames: ssMetadata[i].frames
                    });
                }
                resolve();
                queue.destroy();
            });

            if (lib.properties.manifest && lib.properties.manifest.length) {
                lib.properties.manifest.forEach(item => {
                    item.src = item.src.split("images/").join("");
                    item.src =
                        "/img/" + item.src + "?r=" + (Math.random() * 9999).toFixed(0);
                });
                queue.loadManifest(lib.properties.manifest);
            } else {
                resolve();
            }
        });
    }

    getNext() {
        return this.list[(this.currentPageIndex + 1) % this.list.length];
    }

    loadURL(route) {
        return new Promise(resolve => {
            let selectedPage = null;
            if (route.length > 1) {
                if (route[0] === "explore") {
                    this.config[route[0]].forEach(page => {
                        if (page.path === route.join("/")) {
                            selectedPage = page;
                        }
                    });
                }else if (route[0] === 'interludes') {

                    selectedPage = this.config.static.interludes[this.currentInterlude%this.config.static.interludes.length];
                    selectedPage.path = 'interludes/' + this.currentInterlude%2;


                } else {
                    this.config.static[route[0]].forEach(page => {
                        if (page.path === route.join("/")) {
                            selectedPage = page;
                        }
                    });
                }
                if (selectedPage) {
                    this.loadPage(selectedPage).then((data, animation) => {
                        resolve(data, animation);
                    });
                } else {
                    resolve(null);
                }
            } else {

                if (this.config.static[route[0]]) {
                    this.loadPage(this.config.static[route[0]]).then(
                        (data, animation) => {
                            resolve(data, animation);
                        }
                    );
                } else {
                    resolve(null);
                }
            }
        });
    }

    loadPage(page) {

        return new Promise(resolve => {
            let queue = new createjs.LoadQueue();
            queue.on("error", () => {
                console.log("error");
            });
            queue.on(
                "complete",
                () => {
                    for (let i = 0; i < this.list.length; i++) {
                        if (this.list[i].path === page.path) {
                            if(!this.isFirstTime || page.path === 'intro') {
                                this.currentPageIndex = i;
                            }else {
                                this.currentPageIndex = -1;
                            }

                            this.isFirstTime = false;

                            if(page.path.indexOf('interludes') > -1) {
                                this.currentInterlude++;
                            }

                            break;
                        }
                    }
                    let data = {};
                    Object.assign(data, page);
                    Object.assign(data, page[this.getPrefix(true)]);
                    this.loadManifest(data).then(() => {
                        resolve(data, queue.getResult("page"));
                        queue.destroy();
                    });
                },
                this
            );

            queue.loadManifest([
                {
                    id: "page",
                    src: "/pages" + this.getAnimationURL(page),
                    type: createjs.Types.JAVASCRIPT
                }
            ]);
        });
    }
}

export default Playlist;
