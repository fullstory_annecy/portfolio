'use strict';
import Player from './Player';

import 'assets/main.scss';
import MobileDetect from 'mobile-detect';
let mobileDetect = new MobileDetect(window.navigator.userAgent);
let configJson;
let player = new Player();


if (mobileDetect.mobile() || mobileDetect.tablet()) {
    document.querySelector('html').classList.add('mobile');
}

document.querySelector("#burger").addEventListener("click", () => {
    document.querySelector("#menu").classList.toggle('open');
});

document.querySelectorAll("#menu nav button").forEach((button) => {
    button.addEventListener("click", (e) => {
        player.goPage(e.currentTarget.dataset.target);
        document.querySelector("#menu").classList.toggle('open');
        player.burgerSprite.goToState('close');
    });
});



function buildMenu() {
    let ul = document.querySelector('footer ul');
    for (let i = 0 ; i < 3 ; i++) {
        let li = document.createElement('li');
        let a = document.createElement('a');
        a.href = configJson.explore[i].path;
        a.appendChild(document.createTextNode(configJson.explore[i].title));
        li.appendChild(a);
        ul.appendChild(li);
    }
}

function buildLink() {
    document.querySelectorAll("#menu footer a").forEach(link => {
        link.addEventListener("click", (e) => {
            history.pushState(e.currentTarget.getAttribute('href'), null, e.currentTarget.getAttribute('href'));
            player.checkUrl(e.currentTarget.getAttribute('href'));
            document.querySelector("#menu").classList.toggle('open');
            player.burgerSprite.goToState('close');
            e.preventDefault();
            e.stopPropagation();
        });
    });
}

fetch('/data/config.json').then(function (response) {
    let contentType = response.headers.get("content-type");
    if (contentType && contentType.indexOf("application/json") !== -1) {
        return response.json().then(function (json) {
            configJson = json;
            buildMenu();
            player.init(configJson);
            setTimeout(buildLink);
        });
    } else {
        console.log("Oops, nous n'avons pas du JSON!");
    }
});

window.addEventListener('popstate', function (e) {
    player.checkUrl(e.state);
});






