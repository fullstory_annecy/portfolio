import MobileDetect from 'mobile-detect';
import 'assets/main.scss';

let mobileDetect = new MobileDetect(window.navigator.userAgent);
import Sprite from "./Sprite";
document.body.classList.add('loaded');

let configJson;
const burgerSprite = new Sprite('#burger', 126, 'menu');
burgerSprite.play();

console.log('burgerSprite',burgerSprite)

if (mobileDetect.mobile() || mobileDetect.tablet()) {
    document.querySelector('html').classList.add('mobile');
}

document.querySelector("#burger").addEventListener("click", () => {
    document.querySelector("#menu").classList.toggle('open');
});

document.querySelectorAll("#menu nav button").forEach((button) => {
    button.addEventListener("click", (e) => {
        player.goPage(e.currentTarget.dataset.target);
        document.querySelector("#menu").classList.toggle('open');
        player.burgerSprite.goToState('close');
    });
});

function buildMenu() {
    let ul = document.querySelector('footer ul');
    for (let i = 0 ; i < 3 ; i++) {
        let li = document.createElement('li');
        let a = document.createElement('a');
        a.href = '/' + configJson.explore[i].path;
        a.appendChild(document.createTextNode(configJson.explore[i].title));
        li.appendChild(a);
        ul.appendChild(li);
    }
}

function buildLink() {
    return;
}

fetch('/data/config.json').then(function (response) {
    let contentType = response.headers.get("content-type");
    if (contentType && contentType.indexOf("application/json") !== -1) {
        return response.json().then(function (json) {
            configJson = json;
            buildMenu();
            setTimeout(buildLink);
        });
    } else {
        console.log("Oops, nous n'avons pas du JSON!");
    }
});
