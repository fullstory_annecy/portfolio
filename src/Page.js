import createjs from "createjs";
import MobileDetect from 'mobile-detect';

class Page extends createjs.Container {
    constructor(data) {
        super();
        this.mobileDetect = new MobileDetect(window.navigator.userAgent);
        this.data = data;
        let comp = AdobeAn.getComposition(this.data.id);
        this.lib = comp.getLibrary();
        this.animation = new this.lib[this.data.name]();
        this.addChild(this.animation);

        if (this.animation.next_btn && this.animation.next_btn.btnNext) {
            this.animation.next_btn.btnNext.cursor = "pointer";
        }

        if (this.animation.cta_mc) {
            if (this.animation.cta_mc.btnCta) {
                this.animation.cta_mc.btnCta.cursor = "pointer";
            }
            if (this.animation.cta_mc.ctas && this.animation.cta_mc.ctas.cta1) {
                this.animation.cta_mc.ctas.cta1.cursor = "pointer";
            }

            if (this.animation.cta_mc.ctas && this.animation.cta_mc.ctas.cta0) {
                this.animation.cta_mc.ctas.cta0.cursor = "pointer";
            }
        }

        this.bindedClosed = this.onClosed.bind(this);
        this.bindedNext = this.onNext.bind(this);
        this.bindedComplete = this.onComplete.bind(this);
        this.bindedDetails = this.onDetails.bind(this);
        this.bindedCTA0 = this.onCTA0.bind(this);
        this.bindedCTA1 = this.onCTA1.bind(this);
        this.bindedVideoStart = this.onVideoStart.bind(this);
        this.bindedPlayVideo = this.onPlayVideo.bind(this);
        this.closing = false;

        if (this.data.hasOwnProperty("video")) {
            this.setVideo();
        }

        if(this.data.hasOwnProperty("infos") && this.animation.title_mc) {
            this.animation.title_mc.title_txt.text.text = this.data.infos;
            this.animation.title_mc.title_txt.text.font = "Bold 18px Montserrat";
        }

        this.addEvents();
    }

    setVideo() {

        this.animation.video_mc.videoContainer.removeAllChildren();
        this.video = document.createElement("video");
        this.video.setAttribute('playsinline', 1);
        this.video.setAttribute('muted', 1);
        this.video.setAttribute('autoplay', 1);
        this.video.setAttribute('loop', 1);
        this.video.autoplay = true;
        this.video.loop = true;
        this.video.muted = true;
        this.video.src = this.data.video;
        this.videoBuffer =  new createjs.VideoBuffer(this.video);
        this.videoBitmap = new createjs.Bitmap(this.videoBuffer);

        if (this.data.poster) {
            let poster = new createjs.Bitmap(this.data.poster);
            this.animation.video_mc.videoContainer.addChildAt(poster, 0);
        }

        if (!this.mobileDetect.mobile() && !this.mobileDetect.tablet()) {
            this.animation.video_mc.videoContainer.addChild(this.videoBitmap);
        }

        this.video.play().then((e) => {
            if (this.animation.video_mc.btn_playVideo) {
                this.animation.video_mc.btn_playVideo.alpha = 0;
            }
            this.animation.video_mc.videoContainer.addChild(this.videoBitmap);

        }, (e) => {

        });

    }

    start() {
        this.animation.gotoAndPlay("open");
    }

    stop() {
        this.animation.stop();
    }

    close() {
        if (!this.closing) {
            this.closing = true;
            this.animation.gotoAndPlay("close");
        }
    }

    onDetails() {
        if (this.data.details) {
            window.open(this.data.details, "_blank");
        }
    }

    checkLink(link) {
        if (link) {
            if (link.indexOf("http") > -1) {
                window.open(link, "_blank");
            } else if (link.indexOf("mailto") > -1) {
                window.location.href = link;
            }
        }
    }

    onCTA0() {
        this.checkLink(this.data.cta0);
    }

    onCTA1() {
        this.checkLink(this.data.cta1);
    }

    onVideoStart() {
        let isPlaying =
            this.video.currentTime > 0 &&
            !this.video.paused &&
            !this.video.ended &&
            this.video.readyState > 2;

        if (this.mobileDetect.os() === 'iOS' && !isPlaying) {
            this.animation.stop();
        }
    }

    onPlayVideo() {
        this.animation.video_mc.videoContainer.addChild(this.videoBitmap);
        this.animation.play();
        this.video.play().then((e) => {
            if (this.animation.video_mc.btn_playVideo) {
                this.animation.video_mc.btn_playVideo.alpha = 0;
            }
        }, (e) => {

        });
    }

    onClosed() {
        this.closing = false;
        let event = new createjs.Event("closed");
        this.dispatchEvent(event);
    }

    onNext() {
        let event = new createjs.Event("next");
        this.dispatchEvent(event);
    }

    onComplete() {
        let event = new createjs.Event("complete");
        event.set({template: constants.ANIMATION});
        this.dispatchEvent(event);
    }

    addEvents() {
        this.animation.on("closed", this.bindedClosed);
        this.animation.on("next", this.bindedNext);
        this.animation.on("complete", this.bindedComplete);
        this.animation.on("details", this.bindedDetails);
        this.animation.on("cta0", this.bindedCTA0);
        this.animation.on("cta1", this.bindedCTA1);
        this.animation.on("videoStarter", this.bindedVideoStart);
        this.animation.on("video", this.bindedPlayVideo);

    }

    removeEvents() {
        this.animation.off("closed", this.bindedClosed);
        this.animation.off("complete", this.bindedComplete);
        this.animation.off("next", this.bindedNext);
        this.animation.off("details", this.bindedDetails);
        this.animation.off("cta0", this.bindedCTA0);
        this.animation.off("cta1", this.bindedCTA1);
        this.animation.off("videoStarter", this.bindedVideoStart);
        this.animation.off("video", this.bindedPlayVideo);
    }

    cleanChildren(container) {
        let l = container.children.length;
        while (l--) {
            createjs.Tween.removeTweens(container.children[l]);
            container.children[l].removeAllEventListeners();
            this.childToDestroy.push(container.children[l]);
            if (container.children[l].children) {
                this.cleanChildren(container.children[l]);
            }
        }
    }

    destroy() {
        if (this.video) {
            this.animation.video_mc.videoContainer.removeAllChildren();
            this.videoBitmap = null;
            let isPlaying =
                this.video.currentTime > 0 &&
                !this.video.paused &&
                !this.video.ended &&
                this.video.readyState > 2;
            if (isPlaying) {
                this.video.pause();
            }
            this.video.remove();
            this.video = null;
        }

        this.animation.tickChildren = false;
        this.animation.tickEnabled = false;
        this.animation.gotoAndStop(0);
        createjs.Tween.removeTweens(this.animation);
        this.removeEvents();
        this.childToDestroy = [];
        this.cleanChildren(this.animation);

        this.animation.removeAllChildren();
        this.animation.removeAllEventListeners();

        let n = this.childToDestroy.length;
        while (n--) {
            if (this.childToDestroy[n] && this.childToDestroy[n].image) {
                this.childToDestroy[n].image.src = "";
                this.childToDestroy[n].image = null;
            }

            if (this.childToDestroy[n].parent) {
                this.childToDestroy[n].parent.removeChild(this.childToDestroy[n]);
            }
            this.childToDestroy[n] = null;
        }

        this.childToDestroy = [];
        this.childToDestroy = null;
        this.animation = null;
        this.lib = null;
        this.data = null;

        this.bindedClosed = null;
        this.bindedNext = null;
        this.bindedComplete = null;
        this.bindedDetails = null;
        this.bindedCTA0 = null;
        this.bindedCTA1 = null;
        this.bindedVideoStart = null;
        this.bindedPlayVideo = null;

        this.mobileDetect = null;

        this.videoBuffer = null;

        this.removeChild(this.animation);
        this.animation = null;
    }
}

export default Page;
