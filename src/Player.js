import createjs from "createjs";
import Playlist from "./Playlist";
import Page from "./Page";
import MobileDetect from "mobile-detect";
import Sprite from "./Sprite";

class Player {
    constructor() {
        this.burgerSprite = new Sprite('#burger', 126, 'menu');
        this.isFirstTime = true;
        this.mobileDetect = new MobileDetect(window.navigator.userAgent);
        createjs.Ticker.framerate = 60;
        createjs.Ticker.timingMode = createjs.Ticker.RAF_SYNCHED;
        this.canvas = document.querySelector("canvas");
        this.stage = new createjs.Stage(this.canvas, {transparent: false});
        //
        this.container = new createjs.Container();
        this.stage.addChild(this.container);

        createjs.Ticker.addEventListener("tick", this.stage);
        window.addEventListener("resize", this.resize.bind(this));

        this.onPageClosedBinded = this.onPageClosed.bind(this);
        this.onPageNextBindex = this.onPageNext.bind(this);
        this.currentPage = null;
        this.nextPage = null;
        this.loader = null;
        this.stage.update();


    }

    init(config) {
        this.playlist = new Playlist(config);

        if(!this.mobileDetect.mobile()) {
            this.stage.enableMouseOver(60);
        }

        let isMobile =  this.playlist.getPrefix(true) === "mobile";
        let preload = new createjs.LoadQueue();
        preload.addEventListener("fileload", () => {
            let comp = AdobeAn.getComposition(
                this.playlist.getPrefix(true) === "mobile"
                    ? "44B3830CFA671C42B9BE7262DEA84C87"
                    : "BB31E8F48565D749B0A887649043155E"
            );
            let lib = comp.getLibrary();
            this.loader = new lib[
                this.playlist.getPrefix(true) === "mobile" ? "loader_fullscreen_mobile" : "loader_fullscreen"
                ]();

            this.checkUrl(window.location.pathname);
            this.resize();

        });

        let loaderFile = isMobile ? "/loaderMobile.js" : "/loader.js";

        preload.loadFile({
            id: "loader",
            src: loaderFile,
            type: createjs.Types.JAVASCRIPT
        });
    }

    checkUrl(path) {
        let route = path.split("/");
        route = route.filter(item => {
            return item && item !== "";
        });

        if (route.length) {
            if (
                route[0] === "interludes" ||
                route[0] === "intro" ||
                route[0] === "contact" ||
                route[0] === "members" ||
                route[0] === "explore" ||
                route[0] === "interludes"
            ) {
                this.playlist.loadURL(route).then((pageData, pageAnimation) => {
                    if (pageData) {
                        this.show(new Page(pageData, pageAnimation));
                        this.stopLoading();
                        if(this.isFirstTime) {
                            this.isFirstTime = false;
                            document.body.classList.add('loaded');
                            setTimeout(() => {
                                this.burgerSprite.play();
                            }, 3000);

                        }
                    }
                });
            } else {
                this.goPage("explore");
            }
        } else {
            this.goPage("explore");
        }
    }

    goPage(pageType) {
        this.playlist.createPlaylist(pageType);
        this.goNext();
    }

    goNext() {
        if (this.currentPage) {
            this.nextPage = true;
            this.hideCurrent();
        } else {
            this.startLoading();
            history.pushState(
                "/" + this.playlist.getNext().path,
                null,
                "/" + this.playlist.getNext().path
            );
            this.checkUrl("/" + this.playlist.getNext().path);
        }
    }

    startLoading() {
        if (this.loader) {
            this.loader.gotoAndPlay(0);
            this.stage.addChild(this.loader);
        }
    }

    stopLoading() {
        if (this.loader) {
            this.loader.stop();
            this.stage.removeChild(this.loader);
        }
    }

    onPageClosed() {
        this.container.removeChild(this.currentPage);
        this.currentPage.removeEventListener("closed", this.onPageClosedBinded);
        this.currentPage.removeEventListener("next", this.onPageNextBindex);
        this.currentPage.destroy();
        this.currentPage = null;

        if (this.nextPage) {
            if (typeof this.nextPage === "boolean") {
                this.nextPage = false;
                this.goNext();
            } else {
                this.show(this.nextPage);
                this.nextPage = false;
            }
        }
    }

    onPageNext() {
        setTimeout(() => {
            this.goNext();
        }, 0);
    }

    hideCurrent() {
        this.currentPage.close();
    }

    show(page) {
        if (this.currentPage) {
            this.nextPage = page;
            this.hideCurrent();
        } else {
            this.currentPage = page;
            this.currentPage.addEventListener("closed", this.onPageClosedBinded);
            this.currentPage.addEventListener("next", this.onPageNextBindex);
            this.container.addChild(this.currentPage);
            this.currentPage.start();
        }
    }

    resize() {

        let w = 1280,
            h = 768;
        if (this.playlist.getPrefix(true) === "mobile") {
            w = 480;
            h = 840;
        }

        let iw = window.innerWidth,
            ih = window.innerHeight;
        let pRatio = window.devicePixelRatio || 1,
            xRatio = iw / w,
            yRatio = ih / h,
            sRatio = 1;

        pRatio = Math.min(1.4, pRatio);
        sRatio = Math.min(xRatio, yRatio);

        this.canvas.width = w * pRatio * sRatio;
        this.canvas.height = h * pRatio * sRatio;
        this.canvas.style.width = w * sRatio + "px";
        this.canvas.style.height = h * sRatio + "px";
        this.stage.scaleX = pRatio * sRatio;
        this.stage.scaleY = pRatio * sRatio;
        this.stage.tickOnUpdate = false;
        this.stage.update();
        this.stage.tickOnUpdate = true;



    }
}

export default Player;
