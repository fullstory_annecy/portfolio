class Sprite {
    constructor(selector, numFrames, prefix) {
        this.sprite = document.querySelector(selector);
        this.currentFrame = 0;
        this.numFrames = numFrames;
        this.prefix = prefix;
        this.targetFrame = 0;

        this.frames = {
            close: {
                start: 0,
                end: 18
            },
            close_hover: {
                start: 19,
                end: 50
            },
            close_out: {
                start: 51,
                end: 59
            },
            close_click: {
                start: 60,
                end: 88
            },
            open_hover: {
                start: 89,
                end: 104
            },
            open_out: {
                start: 105,
                end: 112
            },
            open: {
                start: 113,
                end: 144
            }
        };

        this.sprite.addEventListener('mouseover', this.onOver.bind(this));
        this.sprite.addEventListener('mouseout', this.onOut.bind(this));
        this.sprite.addEventListener('click', this.onClick.bind(this));
        this.mouseState = 'out';
        this.state = 'close';
        this.targetFrame = 18;
    }

    onOut() {
        if (this.mouseState !== 'out') {
            this.mouseState = 'out';
        }
        this.targetFrame =  this.frames[this.state + '_hover'].start;
        this.play();
    }

    goToState(state) {
        this.state = state;

        if (this.mouseState !== 'click') {
            this.mouseState = 'click';
        }

        this.currentFrame = this.frames[this.state  + '_out'].start;
        this.targetFrame =  this.frames[this.state  + '_out'].end;
        this.play();
    }

    onOver() {
        if (this.mouseState !== 'over') {
            this.mouseState = 'over';
        }
        this.currentFrame = this.frames[this.state + '_hover'].start;
        this.targetFrame =  this.frames[this.state + '_hover'].end;
        this.play();
    }

    onClick() {
        if (this.mouseState !== 'click') {
            this.mouseState = 'click';
        }
        if(this.state === 'close') {
            this.state = 'open';
        }else {
            this.state = 'close';
        }

        this.currentFrame = this.frames[this.state  + '_out'].start;
        this.targetFrame =  this.frames[this.state  + '_out'].end;

        setTimeout(() => {
            this.play();
        },500);

    }

    play() {
        if(this.currentFrame !== this.targetFrame) {
            if(this.targetFrame > this.currentFrame) {
                this.currentFrame++;
            }else {
                this.currentFrame--;
            }

            const index = "00" + this.currentFrame;
            this.sprite.setAttribute('class','sprite ' + this.prefix + index.slice(-3));
            requestAnimationFrame(this.play.bind(this));
        }
    }
}
export default Sprite;